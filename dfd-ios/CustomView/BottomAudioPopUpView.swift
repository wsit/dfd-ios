//
//  BottomAudioPopUpView.swift
//  Rockerz
//
//  Created by Sunny Chowdhury on 2/20/20.
//  Copyright © 2020 Sunny Chowdhury. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

protocol LoadListButtonDelegate: class {
    func handleLoadListAction(_ bottomView: BottomAudioPopUpView)
}

protocol PreviousDriveButtonDelegate : class{
    func handlepreviousDrivesAction(_ bottomView: BottomAudioPopUpView)
}

class BottomAudioPopUpView: UIView {
    
    @IBOutlet var contentView: UIView! 
    @IBOutlet weak var previousDriveView: UIView!
    @IBOutlet weak var loadListView: UIView!
    @IBOutlet weak var listImg: UIImageView!
    @IBOutlet weak var listTitle: UIButton!
    
    weak var listDelegate: LoadListButtonDelegate?
    weak var previousDrivesDelegate: PreviousDriveButtonDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    /// It is used when you create the view programmatically.
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("BottomAudioPopUpView", owner: self, options: nil )
        addSubview(contentView)
        contentView.frame = self.bounds
    }
    
    func setUpButtonViews(view:UIView) {
        view.layer.cornerRadius = 9
        view.backgroundColor = UIColor.white
        view.layer.shadowOffset = CGSize(width: 0, height: 2)
        view.layer.shadowColor = UIColor.white.cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowRadius = 6
        // view.layer.borderWidth = 1
    }
    
    @IBAction func loadListButtonClicked(_ sender: Any) {
        listDelegate?.handleLoadListAction(self)
    }
    
    @IBAction func previousDrivesButtonClicked(_ sender: Any) {
        previousDrivesDelegate?.handlepreviousDrivesAction(self)
    }
    
    func configureView(){
        contentView.backgroundColor = .white
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.setUpButtonViews(view: previousDriveView)
        self.setUpButtonViews(view: loadListView)
        self.contentView.roundCorners(corners: [.topLeft, .topRight], radius: 10)
    }
    
}
