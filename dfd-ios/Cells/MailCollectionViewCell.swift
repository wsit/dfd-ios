//
//  MailCollectionViewCell.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 1/10/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//

import UIKit

class MailCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var isPremiumContent: UIImageView!
    @IBOutlet weak var mailImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
