//
//  MenuTableViewCell.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 5/2/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

    @IBOutlet weak var upView: UIView!
    @IBOutlet weak var titleImageView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var arrowImageView: UIImageView!
    @IBOutlet weak var lblSeperator: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
