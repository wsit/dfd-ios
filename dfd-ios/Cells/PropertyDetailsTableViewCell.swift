//
//  PropertyDetailsTableViewCell.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 12/19/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit

class PropertyDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var lblItemTitle: UILabel!
    @IBOutlet weak var lblItemCount: UILabel!
    @IBOutlet weak var itemIsActive: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
