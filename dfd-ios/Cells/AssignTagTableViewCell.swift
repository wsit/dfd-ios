//
//  AssignTagTableViewCell.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 7/19/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit

class AssignTagTableViewCell: UITableViewCell {

    @IBOutlet weak var leftImageView: UIImageView!
    @IBOutlet weak var lblTagTitle: UILabel!
    @IBOutlet weak var btnTagColor: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
