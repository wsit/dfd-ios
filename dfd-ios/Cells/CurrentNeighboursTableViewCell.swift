//
//  CurrentNeighboursTableViewCell.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 1/9/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//

import UIKit

class CurrentNeighboursTableViewCell: UITableViewCell {

    @IBOutlet weak var areaView: UIView!
    @IBOutlet weak var nameImageView: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var locationImageView: UIImageView!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var emailImageVIew: UIImageView!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var socialView: UIView!
    @IBOutlet weak var heightForSocialView: NSLayoutConstraint!
    @IBOutlet weak var mobileNumberView: UIView!
    @IBOutlet weak var heightForMobileNumberView: NSLayoutConstraint!
    @IBOutlet weak var mobileImageView: UIImageView!
    @IBOutlet weak var heightForArea: NSLayoutConstraint!
    
    var mobileNumbers : [String] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
//            
//        heightForMobileNumberView.constant = 0
//        mobileNumberView.layoutIfNeeded()
//        
//        lblLocation.text = ""
//        lblEmail.text = "" 
//        mobileNumbers.removeAll()
//        setNeedsDisplay()
    }
    
    
}
