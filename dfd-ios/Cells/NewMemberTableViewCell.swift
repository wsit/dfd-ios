//
//  NewMemberTableViewCell.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 1/15/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//

import UIKit

class NewMemberTableViewCell: UITableViewCell {
    @IBOutlet weak var upView: UIView!
    @IBOutlet weak var memberImageView: UIImageView!
    @IBOutlet weak var memberNameLabel: UILabel!
    @IBOutlet weak var separatorLine: UILabel!
    @IBOutlet weak var pendingImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //FontAndColorConfigs.setLabelFontSize(labels: [memberNameLabel], type: FontAndColorConfigs.getTextTypeHeader())
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        self.selectedBackgroundView?.isHidden = true
        // Configure the view for the selected state
    }
    
}
