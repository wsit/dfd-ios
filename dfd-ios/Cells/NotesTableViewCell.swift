//
//  NotesTableViewCell.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 6/27/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit

class NotesTableViewCell: UITableViewCell {

    @IBOutlet weak var upView: UIView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblNoteTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblNoteSeprator: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
