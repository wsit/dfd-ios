//
//  PropertyListTableViewCell.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 6/15/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit

class PropertyListTableViewCell: UITableViewCell {

    @IBOutlet weak var upView: UIView!
    @IBOutlet weak var propertyImage: UIImageView!
    @IBOutlet weak var lblProperttAddress: UILabel!
    @IBOutlet weak var lblNoOfPhotosOfProperty: UILabel!
    @IBOutlet weak var lblNoOfNotes: UILabel!
    @IBOutlet weak var visitedImageView: UIImageView!
    @IBOutlet weak var lblDateAdded: UILabel!
    @IBOutlet weak var lblPowerTraced: UILabel!
    @IBOutlet weak var lblSeparator: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
