//
//  HistoryTableViewCell.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 6/15/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit

class HistoryTableViewCell: UITableViewCell {

    @IBOutlet weak var upView: UIView!
    @IBOutlet weak var routeImageView: UIImageView!
    @IBOutlet weak var visitingDate: UILabel!
    @IBOutlet weak var lblTotalVisitTitle: UILabel!
    @IBOutlet weak var lblTotalVisitDistance: UILabel!
    @IBOutlet weak var lblNoOfProperties: UILabel!
    @IBOutlet weak var lblDrivingDuration: UILabel!
    @IBOutlet weak var lblDurationTitle: UILabel!
    @IBOutlet weak var lblDistanceTitle: UILabel!
    @IBOutlet weak var lblSeperator: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
