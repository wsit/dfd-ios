//
//  ScoutsTableViewCell.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 8/27/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit

class ScoutsTableViewCell: UITableViewCell {

    @IBOutlet weak var upView: UIView!
    @IBOutlet weak var lblScoutName: UILabel!
    @IBOutlet weak var lblUrl: UILabel!
    @IBOutlet weak var lblSeperator: UILabel!
    @IBOutlet weak var scoutImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
