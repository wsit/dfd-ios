//
//  PhotosCollectionViewCell.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 7/1/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit

class PhotosCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var photoImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
