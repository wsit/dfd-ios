//
//  CardsTableViewCell.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 1/14/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//

import UIKit

class CardsTableViewCell: UITableViewCell {

    
    @IBOutlet weak var upView: UIView!
    @IBOutlet weak var cardImageView: UIImageView!
    @IBOutlet weak var lblCardNumber: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCardType: UILabel!
    @IBOutlet weak var lblExpDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
