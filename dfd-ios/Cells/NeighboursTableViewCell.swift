//
//  NeighboursTableViewCell.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 7/19/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit

class NeighboursTableViewCell: UITableViewCell {

    @IBOutlet weak var upView: UIView!
    @IBOutlet weak var lblNeighboursAddress: UILabel!
    @IBOutlet weak var lblSeperator: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
