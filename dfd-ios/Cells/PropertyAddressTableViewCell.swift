//
//  PropertyAddressTableViewCell.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 11/5/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit

class PropertyAddressTableViewCell: UITableViewCell {

    @IBOutlet weak var lblPropertyAddress: UILabel!
    @IBOutlet weak var lblSeparatorLine: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
