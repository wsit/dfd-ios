//
//  TeamVisitListViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 6/14/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit
import GoogleMaps
import SnapKit
import TTGSnackbar
import Alamofire

class TeamVisitListViewController: UIViewController {
    
    let tableView = UITableView()
    
    
    var allPropertyList: [ListedProperty] = []
    var urlForProperty : String = ""
    var nextUrlForProperty : String = ""
    var previousUrlForProperty : String = ""
    var jsonResponseForProperty : ListOfPropertyResponse?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(tableView)
        tableView.dataSource = self
        tableView.delegate = self
        AppConfigs.showSnacbar(message: "In Team visit Listview", textColor: UIColor.red)
        let nib = UINib(nibName: "PropertyListTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "PropertyListTableViewCell")
        self.tableView.tableFooterView = UIView()
        self.tableView.backgroundColor = .groupTableViewBackground
        self.tableView.separatorStyle = .none
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setUpConstraint()
        if Network.reachability.status == .unreachable{
            AppConfigs.updateUserInterface(view: view)
        }else{
            self.allPropertyList.removeAll()
            let urlString = UrlManager.baseURL() + UrlManager.apiString() + "/history/team/visited-properties/"
            getAllMyTeamVisitedProperties(urlString: urlString)
            
        }//        }
        
    }
    
    func setUpConstraint(){
        
        tableView.snp.makeConstraints { (make) -> Void in
            
            make.edges.equalTo(view).inset(UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
        }
    }
    
    
    func getAllMyTeamVisitedProperties(urlString: String) {
        
        self.showSpinner(onView: view)
        
        print(urlString)
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success",data)
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    
                    let listOfPropertyResponse =  try? JSONDecoder().decode(ListOfPropertyResponse.self, from: response.data!)
                    self.jsonResponseForProperty = listOfPropertyResponse
                    
                    var items: [ListedProperty] = self.allPropertyList
                    items.append(contentsOf: ((listOfPropertyResponse?.results)!))
                    
                    self.allPropertyList.removeAll()
                    self.allPropertyList = items
                    
                    DispatchQueue.main.async {
                        print("All Properties------------\n",self.allPropertyList)
                        globalPropertyList =  self.allPropertyList
                        self.removeSpinner()//self.view.activityStopAnimating()
                        self.tableView.reloadData()
                    }
                }
                    
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)

                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
            //self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
}


extension TeamVisitListViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if allPropertyList.count == 0 {
            tableView.setEmptyMessage("Nothing yet to show")
        } else {
            tableView.restore()
        }
        return allPropertyList.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PropertyListTableViewCell", for: indexPath) as! PropertyListTableViewCell
        cell.backgroundColor = .white
        cell.upView.backgroundColor = .white
        cell.upView.cornerRadius = 8
        cell.lblProperttAddress.textColor = self.listTitleColor
        cell.lblNoOfNotes.textColor = UIColor.white
        cell.lblNoOfPhotosOfProperty.textColor = UIColor.white
        cell.lblDateAdded.textColor = self.listSubTitleColor
        
        cell.lblNoOfNotes.backgroundColor = self.greenCapsuleColor
        cell.lblNoOfPhotosOfProperty.backgroundColor = self.blueCapsuleColor
        
        // cell.photosImageView.tintColor = UIColor(red:0.35, green:0.78, blue:0.98, alpha:1)
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.accessoryType = .none
        cell.lblPowerTraced.backgroundColor = self.powerTraceFlagColor
        
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [cell.lblProperttAddress], type: TextType.ListTitle.rawValue)
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [cell.lblNoOfPhotosOfProperty, cell.lblNoOfNotes], type: TextType.Capsule.rawValue)
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [cell.lblDateAdded], type: TextType.ListSubTitle.rawValue)
        
        
        //        cell.backgroundColor = .groupTableViewBackground
        //        cell.upView.backgroundColor = .white
        //        cell.upView.cornerRadius = 8
        //        cell.lblProperttAddress.textColor = .black//UIColor(red:0.2, green:0.3, blue:0.41, alpha:1)
        //        cell.lblVisitedFlag.textColor = UIColor(red:0.2, green:0.3, blue:0.41, alpha:1)
        //        cell.lblNoOfPhotosOfProperty.textColor = UIColor(red:0.2, green:0.3, blue:0.41, alpha:1)
        //        cell.photosImageView.tintColor = UIColor(red:0.35, green:0.78, blue:0.98, alpha:1)
        //
        //        cell.lblDateAdded.textColor = UIColor(red:0.2, green:0.3, blue:0.41, alpha:1)
        //        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        //        cell.accessoryType = .none
        //        FontAndColorConfigs.setLabelFontSize(labels: [cell.lblProperttAddress], type: FontAndColorConfigs.getTextTypeHeader())
        //
        //        FontAndColorConfigs.setLabelFontSize(labels: [cell.lblDateAdded, cell.lblNoOfPhotosOfProperty, cell.lblVisitedFlag], type: FontAndColorConfigs.getTextTypeParagraph())
        
        
        if allPropertyList.count > indexPath.row{
            
            let property = allPropertyList[indexPath.row]
            
            let street = property.street
            let city = property.city
            let state = property.state
            let zip = property.zip
            
            let tmp3 = (street ?? "") + " " + (city ?? "")
            let tmp2 = tmp3 + " " + (state ?? "") + " "
            cell.lblProperttAddress.text = tmp2 + (zip ?? "")
            cell.lblNoOfPhotosOfProperty.text =  "\(property.photoCount ?? 0)"
            cell.lblNoOfNotes.text = "\(property.noteCount ?? 0)"
            
            let dateString = property.createdAt ?? ""
            let tmp = String(dateString.prefix(10))
            print(tmp)
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.ssssss'Z'"
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "MMM dd yyyy h:mm a"  //"MMM d, h:mm a" for  Sep 12, 2:11 PM
            let datee = dateFormatterGet.date(from: dateString)
            let dateStringInNewFormat =  dateFormatterPrint.string(from: datee ?? Date())
            print(dateStringInNewFormat)
            
            cell.lblDateAdded.text = dateStringInNewFormat // String(dateString.prefix(10)) //dateStringInNewFormat
            
            if datee == nil{
                cell.lblDateAdded.text = tmp
            }
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row == allPropertyList.count - 1 {
            // we are at last cell load more content
            if allPropertyList.count < (jsonResponseForProperty?.count)! {
                self.getAllMyTeamVisitedProperties(urlString: (jsonResponseForProperty?.next)!)
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if allPropertyList.count > indexPath.row{
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "PropertyDetailsViewController") as! PropertyDetailsViewController
            let property = allPropertyList[indexPath.row]
            let sp = SelectedProperty()
            sp.propertyID = property.id!
            sp.propertyAddress = property.street ?? " "
            sp.latitude = (Double( property.latitude ?? 0.0)) //as! Double
            sp.longitude = (Double( property.longitude ?? 0.0)) //as! Double
            
            
            globalSelectedProperty = sp
            globalSelectedPropertyFromList = property
            //nextViewController.selectedPropertyFromList = property
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
    }
}



