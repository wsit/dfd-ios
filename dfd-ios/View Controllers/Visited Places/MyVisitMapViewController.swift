//
//  MyVisitMapViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 6/14/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit

import GoogleMaps
import SnapKit
import TTGSnackbar


class MyVisitMapViewController: UIViewController {
    
    var mapView = GMSMapView()
    var currentLocationMarker: GMSMarker?
    let geocoder = GMSGeocoder()
    var tappedMarker = GMSMarker()
    let marker = GMSMarker()
    var infoWindow = UIView(frame: CGRect.init(x: 0, y: 0, width: 250, height: 80))
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppConfigs.showSnacbar(message: "In My visit Map view", textColor: UIColor.red)
        self.view.addSubview(mapView)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setUpConstraint()
        self.putMarkersOnMap()
    }
    
    func setUpConstraint(){
        
        mapView.snp.makeConstraints { (make) -> Void in
            
            make.edges.equalTo(view).inset(UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
        }
    }
    
    
    func putMarkersOnMap() {
        
        print(globalPropertyList )
        
        if globalPropertyList.count > 0{
            
            let middleProperty = globalPropertyList[globalPropertyList.count/2]
            let camera = GMSCameraPosition.camera(withLatitude: middleProperty.latitude!, longitude:  middleProperty.longitude!, zoom: 17.0)
            // mapView.camera = camera
            mapView.animate(to: camera)
        }
        
        mapView.delegate = self
        
        for property in globalPropertyList{
            
            let address = property.id
            let lat = property.latitude
            let lon = property.longitude
            
            if lat != nil || lon != nil{
                
                showMarkerWithTitleSnippet(title: address!, snippet: " ", position: CLLocationCoordinate2DMake (CLLocationDegrees(Double(lat!)), CLLocationDegrees(Double(lon!))))
            }
        }
    }
    
    func showMarkerWithTitleSnippet(title: Int, snippet:String, position: CLLocationCoordinate2D){
        let marker = GMSMarker()
        marker.isDraggable = false
        marker.position = position
        marker.title = String(title)
        marker.icon = UIImage(named: "memberlocations")
        marker.snippet = snippet
        marker.map = mapView
    }
}


extension MyVisitMapViewController: GMSMapViewDelegate{
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        print("didTapInfoWindowOf")
    }
    
    /* handles Info Window long press */
    func mapView(_ mapView: GMSMapView, didLongPressInfoWindowOf marker: GMSMarker) {
        print("didLongPressInfoWindowOf")
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        
    }
    
    /* set a custom Info Window */
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        
        return UIView()
    }
    
    //MARK - GMSMarker Dragging
    func mapView(_ mapView: GMSMapView, didBeginDragging marker: GMSMarker) {
        print("didBeginDragging")
    }
    func mapView(_ mapView: GMSMapView, didDrag marker: GMSMarker) {
        print("didDrag")
    }
    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
        print("didEndDragging")
        mapView.selectedMarker = marker
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        print("did tap a marker")
        
        // Remember to return false
        // so marker event is still handled by delegate
        print(marker.title)
        
        print("viewDetailsButtonAction")
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "PropertyDetailsViewController") as! PropertyDetailsViewController
        nextViewController.parentVC = "LoadHouzes"
        nextViewController.propertyIDFromList = marker.title!
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
        return false
        
    }
    
    
    // let the custom infowindow follows the camera
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        if (tappedMarker.userData != nil){
            let location = CLLocationCoordinate2D(latitude: marker.position.latitude, longitude: marker.position.longitude)
            infoWindow.center = mapView.projection.point(for: location)
            
        }
    }
}

