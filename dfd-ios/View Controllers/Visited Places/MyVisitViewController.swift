//
//  MyVisitViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 6/14/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit
import JJFloatingActionButton

class MyVisitViewController: UIViewController {
    
    @IBOutlet weak var changeVIewButton: UIButton!
    @IBOutlet weak var myvisitMapView: UIView!
    @IBOutlet weak var myvisitListView: UIView!
    let actionButton = JJFloatingActionButton(frame: CGRect(x: 0, y: 16, width: 40, height: 40))
    @IBOutlet weak var containerView: UIView!
    
    let myVisitedPlacesArray  = ["MyVisitListViewController", "MyVisitMapViewController"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //AppConfigs.showSnacbar(message: "In My visit", textColor: UIColor.red)
        
        //changeVIewButton.backgroundColor = UIColor(red:0, green:0.84, blue:0.37, alpha:1)
        changeVIewButton.setImage(UIImage(named: "world")!.withRenderingMode(.alwaysTemplate), for: .normal)
        changeVIewButton.tintColor = UIColor.white
        self.loadViewController(id: self.myVisitedPlacesArray[0])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //        self.myvisitMapView.alpha = 1
        //        self.myvisitListView.alpha = 0
    }
    
    @IBAction func changeViewAction(_ sender: Any) {
        
        if changeVIewButton.currentImage == UIImage(named: "world"){
            changeVIewButton.setImage(UIImage(named: "list")!.withRenderingMode(.alwaysTemplate), for: .normal)
            self.loadViewController(id: self.myVisitedPlacesArray[1])
            changeVIewButton.tintColor = UIColor.white
        }else{
            changeVIewButton.setImage(UIImage(named: "world")!.withRenderingMode(.alwaysTemplate), for: .normal)
            self.loadViewController(id: self.myVisitedPlacesArray[0])
        }
        //changeVIewButton.backgroundColor = UIColor(red:0, green:0.84, blue:0.37, alpha:1)
        //changeVIewButton.tintColor = UIColor.white
    }
    
    func loadViewController(id: String) {
        //["MyVisitListViewController", "TeamVisitListViewController"]
        let storyBoard : UIStoryboard = UIStoryboard(name: "VisitedPlaces", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: id)
        self.addChild(vc)
        vc.view.frame = CGRect(x: 0, y: 0, width: self.containerView.frame.size.width, height: self.containerView.frame.size.height)
        self.containerView.addSubview(vc.view)
    }
    
    func addFloatyButton() {
        actionButton.buttonImage = UIImage(named: "Oval")
        actionButton.buttonAnimationConfiguration = .transition(toImage: UIImage(named: "close")!)
        actionButton.itemAnimationConfiguration = .slideIn(withInterItemSpacing: 14)
        actionButton.contentVerticalAlignment = .fill
        actionButton.contentHorizontalAlignment = .fill
        actionButton.buttonColor = .red//UIColor(red:0.07, green:0.82, blue:0.41, alpha:1)
        
        let mapView = actionButton.addItem()
        mapView.titleLabel.text = "    Map View    "
        mapView.titleLabel.textColor = .black
        mapView.titleLabel.backgroundColor = .white
        mapView.imageView.image = UIImage(named: "world")
        mapView.buttonColor = .white
        mapView.action = { item in
            //            self.myvisitMapView.alpha = 1
            //            self.myvisitListView.alpha = 0
        }
        
        let listView = actionButton.addItem()
        listView.titleLabel.text = "    List View    "
        listView.titleLabel.textColor = .black
        listView.titleLabel.backgroundColor = .white
        listView.imageView.image = UIImage(named: "Group 4-2")
        listView.buttonColor = .white
        listView.action = { item in
            
            //            self.myvisitMapView.alpha = 0
            //            self.myvisitListView.alpha = 1
            
        }
        
        view.addSubview(actionButton)
        actionButton.translatesAutoresizingMaskIntoConstraints = false
        actionButton.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: -20).isActive = true
        actionButton.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -30).isActive = true
        
    }
    
    
}
