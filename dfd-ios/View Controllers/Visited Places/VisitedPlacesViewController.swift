//
//  VisitedPlacesViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 5/3/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit
import JJFloatingActionButton
import Sheeeeeeeeet

class VisitedPlacesViewController: UIViewController {
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var MyVisitView: UIView!
    @IBOutlet weak var TeamVisitView: UIView!
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var menuBar: UINavigationBar!
    
    @IBOutlet weak var label: UILabel!
    //Adding the Selected Segment Bar
    let buttonBar = UIView()
    let actionButton = JJFloatingActionButton()
    let actionButton2 = JJFloatingActionButton()
    
    let visitedPlacesArray  = ["MyVisitViewController", "TeamVisitViewController"]
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.addFloatyButton()
        actionButton2.frame = CGRect(x: self.view.frame.width - 72, y: 32, width: 40, height: 40)
        
        //setUpCustomSegmentedController()
        //        MyVisitView.alpha = 1
        //        TeamVisitView.alpha = 0
        //        self.view.backgroundColor = UIColor(red:0.09, green:0.15, blue:0.22, alpha:1)
        //        segmentedControl.setTitleTextAttributes([
        //            NSAttributedString.Key.font: UIFont(name: "ProximaNova-Semibold", size: 16)!,
        //            NSAttributedString.Key.foregroundColor: UIColor(red:0.07, green:0.82, blue:0.41, alpha:1)
        //            ], for: .selected)
        self.loadViewController(id: self.visitedPlacesArray[0])
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // FIXME: Remove code below if u're using your own menu
        //        setupNavForDefaultMenu()
        //        // Add left bar button item
        //        let leftBarItem = UIBarButtonItem(image: UIImage(named: "burger"), style: .plain, target: self, action: #selector(toggleSideMenu))
        //        navigationItem.leftBarButtonItem = leftBarItem
        //        leftBarItem.tintColor = .white
        //
        //        self.title = "Visited Places"
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - PagerTabStripDataSource
    
    private func setupNavForDefaultMenu() {
        // Revert navigation bar translucent style to default
        //navigationBarNonTranslecentStyle()
        // Update side menu after reverted navigation bar style
        sideMenuManager?.instance()?.menu?.isNavbarHiddenOrTransparent = true
        navigationItem.hidesBackButton = true
        
    }
    
    @objc func toggleSideMenu() {
        sideMenuManager?.toggleSideMenuView()
    }
    
    func addFloatyButton() {
        
        //        actionButton2.buttonImage = UIImage(named: "Oval")
        //        actionButton2.buttonAnimationConfiguration = .transition(toImage: UIImage(named: "close")!)
        //        actionButton2.itemAnimationConfiguration = .slideIn(withInterItemSpacing: 14)
        //        actionButton2.contentVerticalAlignment = .fill
        //        actionButton2.contentHorizontalAlignment = .fill
        //        //actionButton2.buttonColor = UIColor(red:0.07, green:0.82, blue:0.41, alpha:1)
        //        actionButton2.setGradientBackground(colorOne: UIColor(red:0.07, green:0.82, blue:0.41, alpha:1.0), colorTwo: UIColor(red:0.12, green:0.74, blue:0.84, alpha:1.0))
        //        actionButton2.addTarget(self, action: #selector(showDialog ), for: .touchUpInside)
        
        actionButton.buttonImage = UIImage(named: "Oval")
        actionButton.buttonAnimationConfiguration = .transition(toImage: UIImage(named: "close")!)
        actionButton.itemAnimationConfiguration = .slideIn(withInterItemSpacing: 14)
        
        actionButton.contentVerticalAlignment = .fill
        actionButton.contentHorizontalAlignment = .fill
        actionButton.buttonColor = UIColor(patternImage: UIImage(named: "ic_round_gradiant_back")!)  //UIColor(red:0.07, green:0.82, blue:0.41, alpha:1)
        actionButton.backgroundColor = UIColor(patternImage: UIImage(named: "ic_round_gradiant_back")!)
        actionButton.buttonDiameter  = 52
        actionButton.addTarget(self, action: #selector(showDialog ), for: .touchUpInside)
        //        let mapView = actionButton.addItem()
        //        mapView.titleLabel.text = "    MyVisitListViewController    "
        //        mapView.titleLabel.textColor = .black
        //        mapView.titleLabel.backgroundColor = .white
        //        mapView.imageView.image = UIImage(named: "world")
        //        mapView.buttonColor = .white
        //        mapView.action = { item in
        ////            self.myvisitMapView.alpha = 1
        ////            self.myvisitListView.alpha = 0
        //            self.loadViewController(id: self.visitedPlacesArray[0])
        //        }
        //        
        //        let listView = actionButton.addItem()
        //        listView.titleLabel.text = "    TeamVisitListViewController   "
        //        listView.titleLabel.textColor = .black
        //        listView.titleLabel.backgroundColor = .white
        //        listView.imageView.image = UIImage(named: "Group 4-2")
        //        listView.buttonColor = .white
        //        listView.action = { item in
        //            
        ////            self.myvisitMapView.alpha = 0
        ////            self.myvisitListView.alpha = 1
        //            self.loadViewController(id: self.visitedPlacesArray[1])
        //        }
        //        
        view.addSubview(actionButton)
        actionButton.translatesAutoresizingMaskIntoConstraints = false
        actionButton.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: -20).isActive = true
        actionButton.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -30).isActive = true
        
    }
    
    @objc func showDialog(){
        print("show dialog")
        
        let title = ActionSheetTitle(title: "Select an  Option")
        //        let item1 = ActionSheetItem(title: "Int", value: 1)
        //        let item2 = ActionSheetItem(title: "String", value: "Hi!")
        let cancelButton = ActionSheetCancelButton(title: "Cancel")
        let myHouzes = ActionSheetItem(title: "My Houzes", subtitle: "", value: 3, image: UIImage(named: "my_houzes"), tapBehavior: .dismiss)
        
        let teamHouzes = ActionSheetItem(title: "Team Houzes", subtitle: "", value: 4, image: UIImage(named: "team_houzes"), tapBehavior: .dismiss)
        
        let items = [title, myHouzes, teamHouzes, cancelButton]
        let sheet = ActionSheet(items: items) { sheet, item in
            if let value = item.value as? Int { print("You selected an int: \(value)")
                
                if value == 3{
                    print("My Houzes button tapped")
                    self.loadViewController(id: self.visitedPlacesArray[0])
                }
                if value == 4{
                    print("Team Houzes button tapped")
                    self.loadViewController(id: self.visitedPlacesArray[1])
                }
            }
            if let value = item.value as? String { print("You selected a string: \(value)") }
            if item.isCancelButton { print("You tapped the OK button") }
        }
        
        sheet.present(in: self, from: view) 
        
    }
    
    func loadViewController(id: String) {
        //["MyVisitListViewController", "TeamVisitListViewController"]
        let storyBoard : UIStoryboard = UIStoryboard(name: "VisitedPlaces", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: id)
        self.addChild(vc)
        vc.view.frame = CGRect(x: 0, y: 0, width: self.containerView.frame.size.width, height:
            
            self.containerView.frame.size.height)
        self.containerView.addSubview(vc.view)
    }
}
