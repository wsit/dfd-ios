//
//  Socket + HomeViewController.swift
//  HouZes
//
//  Created by Mahadhi Hassan Chowdhury on 3/11/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//


import Foundation

import UIKit
import LNSideMenu
import GoogleMaps
import SnapKit
import TTGSnackbar
import Floaty
import JJFloatingActionButton
//import GooglePlaces
//import GooglePlacePicker
import Alamofire
import CoreFoundation
import DropDown
import RealmSwift
import Kingfisher


extension HomeViewController: LocationReceiverDelegate, UserDisconnectedDelegate {

        func didDisconnectedAUser(disconnectedUserID: Int) {
            
            for (index, element) in activeUserIDs.enumerated() {
                print("Item \(index): \(element)")
                
                print(disconnectedUserID)
                
                if element  == disconnectedUserID{
                    if activeUserIDs.count > 0{
                        activeUserIDs.remove(at: index)
                    }
                    if markerArray.count > 0{
                        //markerArray.remove(at: index)
                        let marker = markerArray[index]
                        print(markerArray.count)
                        marker.map = mapView
                        marker.map = nil
                        markerArray.remove(at: index)
                    }
                }
            }
        }
        
        func didReceiveResponse(nearByUser: [String : Any]) {
            //mapView.clear()
            //  self.zoomToCoordinates(myLastLocation!.coordinate)
            // { user_id:1, angle:45, latitude: 23.831735, longitude: 90.416504, drive_type: "PRIVATE" }
            var latitude : Double = 0.0
            var longitude: Double = 0.0
            var angle: Double = 0.0
            
            let  lat = nearByUser["latitude"]
            if lat is Double {
                print("Double type")
                latitude = nearByUser["latitude"] as! Double
            }
            else if lat is String{
                print("String type")
                if let data = Double(nearByUser["latitude"] as! String) {
                    print("The user entered a value price of \(data)")
                    latitude = data
                } else {
                    print("Not a valid number: \(String(describing: lat))")
                }
            }
            else {
                print("Unkown type")
            }
            
            let long = nearByUser["longitude"]
            print(long!)
            if long is Double {
                print("Double type")
                longitude = nearByUser["longitude"] as! Double
            }
            else if long is String{
                print("String type")
                if let data = Double(nearByUser["longitude"] as! String) {
                    print("The user entered a value price of \(data)")
                    longitude = data
                } else {
                    print("Not a valid number: \(String(describing: long))")
                }
            }
            else {
                print("Unkown type")
                return
            }
            
            let ang = nearByUser["angle"]
            print(ang!)
            if ang is Double {
                print("Double type")
                angle = nearByUser["angle"] as! Double
            }
            else if ang is String{
                print("String type")
                if let data = Double(nearByUser["angle"] as! String) {
                    print("The user entered a value price of \(data)")
                    angle = data
                } else {
                    print("Not a valid number: \(String(describing: long))")
                }
            }
            else {
                print("Unkown type")
                return
            }
            
            let driveType = nearByUser["drive_type"] as! String
            print(driveType)
            
            print(latitude, longitude, angle)
            
            var nid : Int = 0
            let tmp = nearByUser["user_id"]// as! Int
            
            if tmp is String {
                print("String type")
                let tmp2 = nearByUser["user_id"] as! String
                
                if tmp2 != nil{
                    
                    nid = Int(tmp2)!
                }
                
            }else if tmp is Int{
                print("Int type")
                let tmp2  = nearByUser["user_id"] as! Int
                if tmp2 != nil{
                    
                    nid = Int(tmp2)
                }
            }
            else
            {
                print("Unkown type")
                nid = 0
            }
            
            print(nid)
            
            //check if found user is me or not
            if nid == AppConfigs.getCurrentUserInfo().id{
                print("ITS ME")
            }else{
                //check if user marker already exists or not
                if activeUserIDs.count == 0{
                    activeUserIDs.append(nid)
                    
                    let marker = GMSMarker()
                    marker.position = CLLocationCoordinate2D(latitude:  CLLocationDegrees(latitude) , longitude: CLLocationDegrees(longitude))
                    marker.rotation = CLLocationDegrees(angle)
                    marker.title = "\(nid)"
                    marker.snippet = "Australia"
                    
                    let markerImage = UIImage(named: "ic_car")!.withRenderingMode(.alwaysTemplate)//marker_user_selected_tags_multi
                    //creating a marker view
                    let markerView = UIImageView(image: markerImage)
                    //testdrive
                     //marker.icon = UIImage(named: "ic_car")
                    if driveType == "PRIVATE"{
                        //ic_private_car
                        //                    markerView.tintColor =  UIColor.red
                        //                    marker.iconView = markerView
                        //                    marker.icon = markerImage
                        marker.icon = UIImage(named: "ic_private_car")
                    }else{
                        marker.icon = UIImage(named: "ic_car")
                        //                    markerView.tintColor =  UIColor.black
                        //                    marker.iconView = markerView
                        //                    marker.icon = markerImage
                    }
    //---------------------------------------------------------------------------------------
                    marker.map = self.mapView
                    markerArray.append(marker)
                }else{
                    
                    if activeUserIDs.contains(nid) {
                        print("YES activeUserIDs.contains")
                        
                        let indexes = activeUserIDs.indexes(of:nid)
                        print("indexes count", indexes.count)
                        
                        if indexes.count > 0{
                            let i = indexes[0]
                            print(" is at index \(i)")
                            
                            let marker = markerArray[i]
                            // Keep Rotation Short
                            CATransaction.begin()
                            CATransaction.setAnimationDuration(3.0)
                            marker.rotation = angle
                            CATransaction.commit()
                            
                            // Movement
                            CATransaction.begin()
                            CATransaction.setAnimationDuration(3.0)
                            marker.position = CLLocationCoordinate2D(latitude:  CLLocationDegrees(latitude) , longitude: CLLocationDegrees(longitude))
                            CATransaction.commit()
                            marker.map = mapView
                        }else {
                            print(" isn't in the array")
                        }
                        
                        
                    }else{
                        print("NO activeUserIDs. do not contains")
                        activeUserIDs.append(nid)
                        
                        let marker = GMSMarker()
                        marker.position = CLLocationCoordinate2D(latitude:  CLLocationDegrees(latitude) , longitude: CLLocationDegrees(longitude))
                        marker.rotation = CLLocationDegrees(angle)
                        marker.title = "\(nid)"
                        marker.snippet = "Australia"
                        let markerImage = UIImage(named: "ic_car")!.withRenderingMode(.alwaysTemplate)//marker_user_selected_tags_multi
                        //creating a marker view
                        let markerView = UIImageView(image: markerImage)
                        
                        if driveType == "PRIVATE"{
                            //ic_private_car
                            //                    markerView.tintColor =  UIColor.red
                            //                    marker.iconView = markerView
                            //                    marker.icon = markerImage
                            marker.icon = UIImage(named: "ic_private_car")
                        }else{
                            marker.icon = UIImage(named: "ic_car")
                            //                    markerView.tintColor =  UIColor.black
                            //                    marker.iconView = markerView
                            //                    marker.icon = markerImage
                        }
                        
                        marker.map = self.mapView
                        markerArray.append(marker)
                    }
                }
                
            }
        }

}
