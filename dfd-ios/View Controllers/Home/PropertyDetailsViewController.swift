    //
    //  PropertyDetailsViewController.swift
    //  dfd-ios
    //
    //  Created by Mahadhi Hassan Chowdhury on 5/15/19.
    //  Copyright © 2019 Workspace IT. All rights reserved.
    //
    
    import UIKit
    import JJFloatingActionButton
    import Alamofire
    import GoogleMaps
    import SwiftyJSON
    
    //globalSelectedPropertyFromList
    //var globalParentVc: String = ""
    
    var globalListedPropertiesInCurrentDrive: [ListedProperty] = []
    
    class PropertyDetailsViewController: UIViewController {
        
        @IBOutlet weak var titleImageView: UIImageView!
        @IBOutlet weak var proppertyDetailsTV: UITableView!
        @IBOutlet weak var lblPropertyAddress: UILabel!
        @IBOutlet weak var myMapView: GMSMapView!
        @IBOutlet weak var lbVerticalLine: UILabel!
        @IBOutlet weak var lblHorizontalLine: UILabel!
        @IBOutlet weak var adrressHeightConstraint: NSLayoutConstraint!
        @IBOutlet weak var lblNotice: UILabel!
        @IBOutlet weak var btnAddress: UIButton!
        @IBOutlet weak var btnCross: UIButton!
        @IBOutlet weak var btnOK: UIButton!
        @IBOutlet weak var okButtonHeight: NSLayoutConstraint!
        @IBOutlet weak var crossButtonHeight: NSLayoutConstraint!
        @IBOutlet weak var btnListTitle: UIButton!
        @IBOutlet weak var lblListName: UILabel!
        
        @IBOutlet weak var verticalLineHeight: NSLayoutConstraint!
        @IBOutlet weak var heightForListTitleButton: NSLayoutConstraint!
        @IBOutlet weak var heightForListImageView: NSLayoutConstraint!
        @IBOutlet weak var heightForListNameLabel: NSLayoutConstraint!
        @IBOutlet weak var listImageView: UIImageView!
        @IBOutlet weak var tagView: UIView!
        
        
        let actionButton = JJFloatingActionButton()
        var propertyIDFromList : String = ""
        // var selectedPropertyFromList : ListedProperty?
        var parentVC: String = ""
        let kCellIdentifier = "PropertyDetailsTableViewCell"
        var items = ["Add To List", "Notes", "Photos", "Assign Tag", "Owner Details", "Mailer Wizard", "Power Trace", "Get Neighbor Details"]
        
        var currentPropertyDetails: PropertyDetailsResponse?
        
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            myMapView.mapType = .satellite
            myMapView.delegate = self
            myMapView.isUserInteractionEnabled = false
            
            print(propertyIDFromList)
            self.title = "Property Details"
            //self.addFloatyButton()
            
            let nib = UINib(nibName: "PropertyDetailsTableViewCell", bundle: nil)
            proppertyDetailsTV.register(nib, forCellReuseIdentifier: kCellIdentifier)
            
            self.proppertyDetailsTV.tableFooterView = UIView()
            
            NotificationCenter.default.addObserver(self, selector: #selector(self.reloadPropertyData(_:)), name: NSNotification.Name(rawValue: "PowerTracedNotif"), object: nil)
            
            let labelTap = UITapGestureRecognizer(target: self, action: #selector(self.labelTapped(_:)))
            self.lblNotice.isUserInteractionEnabled = true
            self.lblNotice.addGestureRecognizer(labelTap)
            
            setUpFontsAndColor()
            
        }
        
        
        override func viewWillAppear(_ animated: Bool) {

            super.viewWillAppear(true)
            
            tagView.subviews.forEach { $0.removeFromSuperview() }

            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()

            lblNotice.layer.masksToBounds = true
            lblNotice.cornerRadius = 8
            lblNotice.isHidden = true
            
            checkIfListIsSelected()
            self.proppertyDetailsTV.reloadData()
            
            
            if globalSelectedList != nil{
                globalIsListedProperty = true
            }else
            {
                globalIsListedProperty = false
            }
            
            if self.parentVC == "LoadHouzes"{
                //print((globalSelectedPropertyFromList?.id)!)
                self.propertyIDFromList = "\(String(describing: (globalSelectedPropertyFromList?.id)!))"
                print(self.propertyIDFromList)
                self.callPropertyDetailsAPI()
            }
            else if self.parentVC == "LoadHouzesFromMap"{
                //                self.propertyIDFromList = "\(String(describing: (globalSelectedPropertyFromList?.id)!))"
                print(self.propertyIDFromList)
                self.callPropertyDetailsAPI()
            }
            else if self.parentVC == "History"{
                self.propertyIDFromList = "\(String(describing: (globalSelectedPropertyFromList?.id)!))"
                print(self.propertyIDFromList)
                self.callPropertyDetailsAPI()
            }
                
            else if self.parentVC == "AddProperty"{
                self.propertyIDFromList = "\(String(describing: (globalSelectedPropertyFromList?.id)!))"
                print(self.propertyIDFromList)
                self.callPropertyDetailsAPI()
            }
                
            else if  self.parentVC == "Home"{
                
                placeTappedMarker()
                
                if globalSelectedPropertyFromList?.id != nil && globalSelectedPropertyFromList?.id != 0{
                    self.propertyIDFromList = "\(String(describing: (globalSelectedPropertyFromList?.id)!))"
                    print(self.propertyIDFromList)
                    self.callPropertyDetailsAPI()
                }else{
                    print("from home but property not available")
                    globalIsListedProperty = false
                    //self.showTopBannerNoticeWithDefaultList()
                }
                
            }
            else if self.parentVC == "DrivedPropertyFromHome"{
                print(self.propertyIDFromList)
                self.callPropertyDetailsAPI()
            }
            else{
                //not load houze or home
                globalIsListedProperty = true
                placeListedMarker()
                
                if globalSelectedPropertyFromList != nil{
                    globalIsListedProperty = true
                }
            }
            
            if self.currentPropertyDetails != nil{
                print("self.currentPropertyDetails not nil")
                if self.currentPropertyDetails?.userList != nil{
                    print("self.currentPropertyDetails?.userList", self.currentPropertyDetails?.userList!)
                }
                
            }else{
                print("self.currentPropertyDetails == nil")
                
            }
            
            //set it true from parent if you have property details
            if globalIsPropertyDetailsAvailable == false{
                self.items =  ["Add To List","Notes", "Photos", "Assign Tag", "Owner Details", "Mailer Wizard", "Power Trace", "Get Neighbor Details"]
                if globalSelectedList?.name != nil && globalSelectedList?.name != ""{
                    showTopBannerNotice()
                    //showTopBannerNoticeWithDefaultList()
                    self.lblNotice.isHidden = false
                }
                globalIsListedProperty = false
                self.showBottomButtons()
                self.proppertyDetailsTV.reloadData()
            }else
            {
                self.hideBottomButtons()
                self.items =  ["Notes", "Photos", "Assign Tag", "Owner Details", "Mailer Wizard", "Power Trace", "Get Neighbor Details"]
                self.lblNotice.isHidden = true
                self.proppertyDetailsTV.reloadData()
            }
            
            let height = AppConfigs.heightForView(text: lblPropertyAddress.text!, font: lblPropertyAddress.font, width: lblPropertyAddress.frame.size.width)
            print(height)
            self.adrressHeightConstraint.constant = height
            self.lblPropertyAddress.layoutIfNeeded()
        }
        
        @objc func labelTapped(_ sender: UITapGestureRecognizer) {
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "ListOfListViewController") as! ListOfListViewController
            vc.modalPresentationStyle = .fullScreen
            vc.parentString = "PropertyDetails"
            self.present(vc, animated: true, completion: nil)
        }
        
        @objc func reloadPropertyData(_ notification: NSNotification) {
            globalIsListedProperty = true
            let url : String = UrlManager.baseURL() + UrlManager.apiString() + "/property/\((globalSelectedPropertyFromList?.id)!)/"
            print(url)
            //            self.showSpinner(onView: view)
            getlistedPropertyDetails(urlString: url)
        }
        
        func setUpFontsAndColor(){
            
            FontAndColorConfigs.setDynamicLabelFontSize(labels: [lblNotice], type: TextType.ListTitle.rawValue)
            FontAndColorConfigs.setDynamicButtonsFontSize(buttons: [self.btnAddress], type: TextType.MainTitle.rawValue)
            FontAndColorConfigs.setDynamicLabelFontSize(labels: [lblPropertyAddress], type: TextType.MainSubTitle.rawValue)
            lblPropertyAddress.textColor = UIColor(red:0.25, green:0.25, blue:0.25, alpha:1)
            
            lblNotice.backgroundColor = UIColor(red:0.51, green:0.69, blue:0.96, alpha:1)
            lblNotice.font = UIFont(name: "ProximaNova-Semibold", size: 14)!
            
            lbVerticalLine.backgroundColor = self.listSeperatorColor
            lblHorizontalLine.backgroundColor = self.listSeperatorColor
        }
        
        func callPropertyDetailsAPI() {
            globalIsListedProperty = true
            let url : String = UrlManager.baseURL() + UrlManager.apiString() + "/property/\(propertyIDFromList)/"
            print(url)
            // self.showSpinner(onView: view)
            getlistedPropertyDetails(urlString: url)
            //            }
        }
        
        
        func checkIfListIsSelected(){
            if globalSelectedList == nil{
                hideBottomButtons()
            }else{
                showBottomButtons()
            }
        }
        
        func showTopBannerNotice(){
            lblNotice.isHidden = false
            lblNotice.text = "Add Property to '\(globalSelectedList?.name ?? "")' \n To change your list, Click here"
            globalIsListedProperty = true
        }
        
        
        func showTopBannerNoticeWithDefaultList(){
            lblNotice.isHidden = false
            let defaultListName = AppConfigs.getCurrentUserInfo().defaultList?.name
            lblNotice.text = "Add Property to '\(defaultListName!)' \n To change your list, Click here"
             
        }
        
        func showBottomButtons(){
            
            self.lblHorizontalLine.isHidden = false
            self.lbVerticalLine.isHidden = false
            self.btnOK.isHidden = false
            self.btnCross.isHidden = false
            
            self.okButtonHeight.constant = 48
            self.crossButtonHeight.constant = 48
            self.verticalLineHeight.constant = 48
            
            self.btnOK.layoutIfNeeded()
            self.btnCross.layoutIfNeeded()
            self.lbVerticalLine.layoutIfNeeded()
        }
        
        func hideBottomButtons(){
            
            self.lblHorizontalLine.isHidden = true
            self.lbVerticalLine.isHidden = true
            self.btnOK.isHidden = true
            self.btnCross.isHidden = true
            
            self.okButtonHeight.constant = 0
            self.crossButtonHeight.constant = 0
            self.verticalLineHeight.constant = 0
            self.verticalLineHeight.constant = 0
            
            self.btnOK.layoutIfNeeded()
            self.btnCross.layoutIfNeeded()
            self.lbVerticalLine.layoutIfNeeded()
        }
        
        
        func placeTappedMarker() {
            
            let street = globalTappedPlace.street
            let city = globalTappedPlace.city
            let state = globalTappedPlace.state
            let zip = globalTappedPlace.zip
            let lat = globalTappedPlace.latitude
            let long = globalTappedPlace.longitude
            let country = globalTappedPlace.country
            
            let tmp = (street ) + " " + (city )
            let tmp2 = tmp + " " + (state ) + " "
            let tmp3 = tmp2 + (zip )
            self.lblPropertyAddress.text = tmp3 + " " + country
            
            var marker = GMSMarker()
            marker = GMSMarker(position: CLLocationCoordinate2D(latitude: globalTappedPlace.latitude, longitude: globalTappedPlace.longitude))
            marker.icon = UIImage(named: "Shape")
            marker.map = self.myMapView
            let camera = GMSCameraPosition.camera(withTarget: CLLocationCoordinate2D(latitude: lat, longitude: long), zoom: 20, bearing: 45, viewingAngle: 0)
            self.myMapView.animate(to: camera)
        }
        
        func placeListedMarker(){
            
            let street = globalSelectedPropertyFromList?.street
            let city = globalSelectedPropertyFromList?.city
            let state = globalSelectedPropertyFromList?.state
            let zip = globalSelectedPropertyFromList?.zip
            let lat = globalSelectedPropertyFromList?.latitude
            let long = globalSelectedPropertyFromList?.longitude
            
            let tmp = (street ?? "") + " " + (city ?? "")
            let tmp2 = tmp + " " + (state ?? "") + " "
            self.lblPropertyAddress.text = tmp2 + (zip ?? "")
            
            
            var marker = GMSMarker()
            if globalSelectedPropertyFromList?.latitude != nil{
                //property already listed
                marker = GMSMarker(position: CLLocationCoordinate2D(latitude: lat!, longitude: long!))
                marker.icon = UIImage(named: "Shape")
                marker.map = self.myMapView
                let camera = GMSCameraPosition.camera(withTarget: CLLocationCoordinate2D(latitude: (lat)!, longitude: (long)!), zoom: 17, bearing: 45, viewingAngle: 0)
                self.myMapView.animate(to: camera)
            }else{
                //not sure when this get called
                //property not listed, just tapped.
                placeTappedMarker()
            }
        }
        
        func getlistedPropertyDetails(urlString: String)  {
            let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
            print(headers)
            
            Alamofire.request(urlString, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
                switch(response.result) {
                    
                case.success(let data):
                    
                    self.removeSpinner()
                    print("success",data)
                    let statusCode = response.response?.statusCode
                    print(statusCode!)
                    
                    if statusCode == 200{
                        
                        let singlePropertyDetailsResponse = try? JSONDecoder().decode(PropertyDetailsResponse.self, from: response.data!)
                        
                        if singlePropertyDetailsResponse == nil{
                            print("singlePropertyDetailsResponse == nil")
                            
                            do {
                                
                                let data = try JSON(data: response.data!)
                                let propertyID = data["id"].intValue
                                let propertyList = data["user_list"].intValue
                                let propertyStreet = data["street"].stringValue
                                let propertyCity = data["city"].stringValue
                                let propertyZip = data["zip"].stringValue
                                
                                let propertyState = data["state"].stringValue
                                let propertyLatitude = data["latitude"].doubleValue
                                let propertyLongitude = data["longitude"].doubleValue
                                
                                let propertyPT = data["power_trace_request_id"].intValue
                                
                                let propertyTags = data["property_tags"].arrayValue
                                
                                var tagArray : [PropertyTag] = []
                                var photoArray : [PropertyPhoto] = []
                                var noteArray : [Note] = []
                                
                                if propertyTags.count > 0{

                                    for item in propertyTags{
                                        
                                        let id = item["id"].intValue
                                        let name = item["name"].stringValue
                                        let user = item["user"].intValue
                                        
                                        let pcolor = item["color"]
                                        let colorName = pcolor["color_name"].stringValue
                                        let colorCode = pcolor["color_code"].stringValue
                                        
                                        let color : TagColor = TagColor(colorName: colorName, colorCode: colorCode)
                                        
                                        let tag : PropertyTag = PropertyTag(id: id, name: name, color: color, user: user, createdAt: "", updatedAt: "")
                                        tagArray.append(tag)
                                    }
                                }
                                print(tagArray)
                                
                                let ownerInfo = data["owner_info"].arrayValue
                                
                                let ownerData = ownerInfo[0]
                                let full_name = data["full_name"].stringValue
                                let full_address = data["full_address"].stringValue
                                let ownerDataObject: OwnerInfo = OwnerInfo(fullName: full_name, fullAddress: full_address)
                                
                                let photos =  data["photos"].arrayValue
                                
                                for photo in photos{
                                    
                                    let pPhoto : PropertyPhoto = PropertyPhoto(id: 1, photoURL: "", thumbPhotoUrl: "", createdAt: "", updatedAt: "", user: 1, property: 1)
                                    photoArray.append(pPhoto)
                                }
                                print(photoArray)
                                
                                let notes =  data["notes"].arrayValue
                                
                                for note in notes{
                                    
                                    let nNote : Note = Note(id: 1, title: "1", notes: "", createdAt: "", updatedAt: "", user: 1, property: 1)
                                    noteArray.append(nNote)
                                }
                                print(noteArray)
                                
                                
                                let historyID = data["history"].intValue
                                
                                let user_list_details =  data["user_list_details"]
                                let lID = data["user_list_details"].intValue
                                let leads_count = data["leads_count"].intValue
                                let user = data["user"].intValue
                                let uname = data["name"].stringValue
                                let fetch_lat_lng = data["fetch_lat_lng"].boolValue
                                
                                let userListDetails : UserListDetails = UserListDetails(id: lID, name: uname, user: user, createdAt: "", updatedAt: "", leadsCount: leads_count, fetchLatLong: fetch_lat_lng)
                                
                                let singlePropertyDetailsResponse : PropertyDetailsResponse = PropertyDetailsResponse(id: propertyID, userList: propertyList, street: propertyStreet, city: propertyCity, state: propertyState, zip: propertyZip, cadAcct: "", gmaTag: 0, latitude: propertyLatitude, longitude: propertyLongitude, propertyTags: tagArray, ownerInfo: [ownerDataObject], photos: photoArray, notes: noteArray, history: historyID, createdAt: "", updatedAt: "", powerTraceRequestID: propertyPT, userListDetails: userListDetails)
                                
                                print(singlePropertyDetailsResponse)
                                
                                globalCurrentPropertyDetails = singlePropertyDetailsResponse
                                self.currentPropertyDetails = singlePropertyDetailsResponse
                                self.updateViewAccordingToData(singlePropertyDetailsResponse: singlePropertyDetailsResponse)

                            }catch let error{
                                self.showAlert(message: " ", title: error.localizedDescription)
                                self.removeSpinner()
                            }
                            
                            
                        }else{
                            
                            globalCurrentPropertyDetails = singlePropertyDetailsResponse
                            self.currentPropertyDetails = singlePropertyDetailsResponse
                            
                            self.updateViewAccordingToData(singlePropertyDetailsResponse: singlePropertyDetailsResponse)
                        }
                        
                    }else{
                        self.showAlert(message: " ", title: "Some Error Occured")
                    }
                    
                case.failure(let error):
                    print("Not Success",error)
                    
                    let statusCode = response.response?.statusCode
                    self.showMessageForServerError(code: statusCode ?? 500)
                    
                    //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                    self.removeSpinner()
                }
            }
        }
        
        func updateViewAccordingToData(singlePropertyDetailsResponse: PropertyDetailsResponse?){

            //replace current drive property info with latest info by matching id
            self.view.layoutIfNeeded()
            self.view.setNeedsLayout()
            
            DispatchQueue.main.async {
                
                self.lblHorizontalLine.isHidden = true
                self.lbVerticalLine.isHidden = true
                self.btnOK.isHidden = true
                self.btnCross.isHidden = true
                
                self.okButtonHeight.constant = 0
                self.crossButtonHeight.constant = 0
                self.verticalLineHeight.constant = 0
                
                self.btnOK.layoutIfNeeded()
                self.btnCross.layoutIfNeeded()
                self.lbVerticalLine.layoutIfNeeded()
                
                let street = singlePropertyDetailsResponse?.street
                let city = singlePropertyDetailsResponse?.city
                let state = singlePropertyDetailsResponse?.state
                let zip = singlePropertyDetailsResponse?.zip
                let lat = singlePropertyDetailsResponse?.latitude
                let long = singlePropertyDetailsResponse?.longitude
                
                globalSelectedPropertyFromList = ListedProperty(
                    id: singlePropertyDetailsResponse?.id,
                    userList: singlePropertyDetailsResponse?.userList,
                    street: singlePropertyDetailsResponse?.street,
                    city: singlePropertyDetailsResponse?.city,
                    state: singlePropertyDetailsResponse?.state,
                    zip: singlePropertyDetailsResponse?.zip,
                    cadAcct: singlePropertyDetailsResponse?.cadAcct,
                    gmaTag: singlePropertyDetailsResponse?.gmaTag,
                    latitude: singlePropertyDetailsResponse?.latitude,
                    longitude: singlePropertyDetailsResponse?.longitude,
                    propertyTags: singlePropertyDetailsResponse?.propertyTags,
                    ownerInfo: singlePropertyDetailsResponse?.ownerInfo,
                    photoCount: singlePropertyDetailsResponse?.photos?.count,
                    noteCount: singlePropertyDetailsResponse?.notes?.count,
                    createdAt: singlePropertyDetailsResponse?.createdAt,
                    updatedAt: singlePropertyDetailsResponse?.updatedAt,
                    powerTraceRequestID:singlePropertyDetailsResponse?.powerTraceRequestID ?? 0,
                    history: singlePropertyDetailsResponse?.history ?? 0
                )
                
                
                for i in 0..<globalListedPropertiesInCurrentDrive.count{
                    
                    let property = globalListedPropertiesInCurrentDrive[i]
                    
                    print("property.id", property.id)
                    print("globalSelectedPropertyFromList.id", globalSelectedPropertyFromList?.id)
                    
                    print("property.street", property.street)
                    print("globalSelectedPropertyFromList.street", globalSelectedPropertyFromList?.street)
                    
                    if property.id == globalSelectedPropertyFromList?.id{
                        globalListedPropertiesInCurrentDrive[i] = globalSelectedPropertyFromList!
                    }else{
                        print("property didn't match")
                    }
                }
                
                self.removeSpinner() //self.view.activityStopAnimating()
                let tmp = (street ?? "") + " " + (city ?? "")
                let tmp2 = tmp + " " + (state ?? "") + " "
                self.lblPropertyAddress.text = tmp2 + (zip ?? "")
                
                var marker = GMSMarker()
                marker = GMSMarker(position: CLLocationCoordinate2D(latitude: lat ?? 0.0, longitude: long ?? 0.0))
                marker.icon = UIImage(named: "Shape")
                marker.map = self.myMapView
                
                let camera = GMSCameraPosition.camera(withTarget: CLLocationCoordinate2D(latitude: lat ?? 0.0, longitude: long ?? 0.0), zoom: 20, bearing: 45, viewingAngle: 0)
                self.myMapView.animate(to: camera)
                
                globalIsListedProperty = true
                globalIsPropertyDetailsAvailable = true //for checking
                
                //set it true from parent if you have property details
                if globalIsPropertyDetailsAvailable == false{
                    self.items =  ["Add To List","Notes", "Photos", "Assign Tag", "Owner Details", "Mailer Wizard", "Power Trace", "Get Neighbor Details"]
                    if globalSelectedList?.name != nil && globalSelectedList?.name != ""{
                        self.showTopBannerNotice()
                        //self.showTopBannerNoticeWithDefaultList()
                        self.lblNotice.isHidden = false
                    }
                    self.showBottomButtons()
                }else
                {
                    self.lblNotice.isHidden = true
                    self.hideBottomButtons()
                    self.items =  ["Notes", "Photos", "Assign Tag", "Owner Details", "Mailer Wizard", "Power Trace", "Get Neighbor Details"]
                    self.lblNotice.isHidden = true
                    
                    self.heightForListTitleButton.constant = 24
                    self.btnListTitle.setTitle("List Name", for: .normal)
                    self.btnListTitle.layoutIfNeeded()
                    
                    self.heightForListImageView.constant = 24
                    self.listImageView.layoutIfNeeded()
                    
                    //to do
                    self.lblListName.text =  singlePropertyDetailsResponse?.userListDetails?.name
                    
                    let heightOfListNameLabel = AppConfigs.heightForView(text: self.lblListName.text!, font: self.lblListName.font, width: self.lblListName.frame.size.width)
                    print(heightOfListNameLabel)
                    
                    if heightOfListNameLabel > 0  && heightOfListNameLabel < 30{
                        self.heightForListNameLabel.constant = 30
                    }else{
                        self.heightForListNameLabel.constant = heightOfListNameLabel
                    }
                    self.lblListName.layoutIfNeeded()
                }
                self.view.layoutIfNeeded()
                self.showTagNameForCurrentProperty()
                self.proppertyDetailsTV.reloadData()
                
            }
        }
        
        func showTagNameForCurrentProperty(){
            
            var yPos =  160
            var xPos = 8
            let tags = self.currentPropertyDetails?.propertyTags
            
            if tags != nil {
                if tags!.count > 0{
                    
                    print(tags!.count)
                    
                    for i in 0..<tags!.count {
                        
                        let element = tags![i]
                        let labelNum = UILabel()
                        
                        labelNum.text = element.name
                        labelNum.textColor = .white
                        labelNum.textAlignment = .center
                        labelNum.numberOfLines = 0
                        labelNum.lineBreakMode = NSLineBreakMode.byWordWrapping
                        labelNum.cornerRadius = 8
                        
                        labelNum.layer.masksToBounds = true
                        labelNum.sizeToFit()
                        labelNum.backgroundColor =  UIColor(hexString: (element.color?.colorCode!)!)
                        FontAndColorConfigs.setDynamicLabelFontSize(labels: [labelNum], type: TextType.ListSubTitle.rawValue)
                        
                        labelNum.frame = CGRect( x:xPos, y:Int(yPos), width:Int(self.tagView.frame.width/2) - 24 , height: 26)
                        
                        self.tagView.addSubview(labelNum)
                        self.tagView.layoutIfNeeded()
                        self.tagView.setNeedsLayout()
                        
                        if (i%2) == 0{
                            xPos += Int(self.tagView.frame.width/2) + 8
                        }else{
                            xPos = 8
                            yPos -= 30
                        }
                    }
                }
            }
        }
        
        @IBAction func backAction(_ sender: Any) {
            
            _ = self.navigationController?.popViewController(animated: true)
        }
        
        
        @IBAction func crossButtonCLicked(_ sender: Any) {
            _ = self.navigationController?.popViewController(animated: true)
        }
        
        @IBAction func rightButtonClicked(_ sender: Any) {
            
            //if AppConfigs.getCurrentUserInfo().defaultList != nil{
            if globalSelectedList?.name != nil && globalSelectedList?.name != ""{
                self.addPropertyToPreviousList()
            }else{
                
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "ListOfListViewController") as! ListOfListViewController
                vc.modalPresentationStyle = .fullScreen
                vc.parentString = "PropertyDetails"
                self.present(vc, animated: true, completion: nil)
            }
        }
        
        func addPropertyToPreviousList() {
            let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
            let defaultListID = AppConfigs.getCurrentUserInfo().defaultList?.id
            print("defaultListID---->", defaultListID!)
//            self.showTopBannerNoticeWithDefaultList()
            var parameters =  [
                "street": globalTappedPlace.street,
                "city": globalTappedPlace.city,
                "state": globalTappedPlace.state,
                "zip": globalTappedPlace.zip,
                "cad_acct": "12345",
                "gma_tag": 12346,
                "latitude": globalTappedPlace.latitude,
                "longitude": globalTappedPlace.longitude,
                "user_list" : (globalSelectedList?.id)!//defaultListID!
                ]  as [String : Any]
            
            if globalDrivingInfo != nil{
                parameters =  [
                    "street": globalTappedPlace.street,
                    "city": globalTappedPlace.city,
                    "state": globalTappedPlace.state,
                    "zip": globalTappedPlace.zip,
                    "cad_acct": "12345",
                    "gma_tag": 12346,
                    "latitude": globalTappedPlace.latitude,
                    "longitude": globalTappedPlace.longitude,
                    "user_list" : (globalSelectedList?.id)!,//defaultListID!,
                    "history" : (globalDrivingInfo?.id)!
                    ]  as [String : Any]
            }
            print(parameters)
            
            let url = UrlManager.baseURL() + UrlManager.apiString() + "/property/" //UrlManager.AddPropertyToListURL()
            print(url)
            Alamofire.request(url, method: .post, parameters: parameters as Parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case.success(let data):
                    //print("success",data)
                    
                    self.removeSpinner()
                    let statusCode = response.response?.statusCode
                    print(statusCode!)
                    
                    if statusCode == 200{
                        
                        let addedToListResponse = try? JSONDecoder().decode(AddedToListResponse.self, from: response.data!)
                        
                        if addedToListResponse?.status == true{
                            
                            if addedToListResponse?.data != nil {
                                globalSelectedPropertyFromList = addedToListResponse?.data!
                                
                                //adding to array of properties for current drive
                                if globalSelectedPropertyFromList != nil{
                                    globalListedPropertiesInCurrentDrive.append(globalSelectedPropertyFromList!)
                                }
                               
                                print(globalSelectedPropertyFromList?.id ?? 0)
                                self.propertyIDFromList = "\(String(describing: (globalSelectedPropertyFromList?.id)!))"
                                globalIsListedProperty = true
                                
                                DispatchQueue.main.async {
                                    
                                    self.removeSpinner()
                                    self.showAlert(message: "Property Added to '\(String(describing: (globalSelectedList?.name)!))'")
                                    print(self.propertyIDFromList)
                                    self.callPropertyDetailsAPI()
                                    
                                }
                            }else{
                                self.removeSpinner()
                                AppConfigs.showSnacbar(message: (addedToListResponse?.message)!, textColor: .green)
                            }
                            
                        }else{
                            
                        }
                    }
                    else if statusCode == 401{
                        self.removeSpinner()
                        let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                        print("error with response status: \(String(describing: statusCode))")
                        //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                    }
                        
                    else{
                        self.removeSpinner()
                        print("error with response status: \(String(describing: statusCode))")
                        AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                    }
                case.failure(let error):
                    print("Not Success",error)
                    
                    let statusCode = response.response?.statusCode
                    self.showMessageForServerError(code: statusCode ?? 500)
                    
                    //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                    self.removeSpinner()
                }
                self.removeSpinner()
            }
        }
        
    }
    
    extension PropertyDetailsViewController: UITableViewDataSource {
        // MARK: - Table view data source
        
        func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return items.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier, for: indexPath) as! PropertyDetailsTableViewCell
            
            
            cell.lblItemTitle.text = items[indexPath.row]
            FontAndColorConfigs.setDynamicLabelFontSize(labels: [cell.lblItemTitle], type: TextType.ListTitle.rawValue)
            cell.lblItemCount.layer.masksToBounds = true
            
            if items[indexPath.row] == "Add To List"{
                cell.lblItemTitle?.textColor = UIColor(red:0, green:0, blue:0, alpha:0.8)
                cell.lblItemCount.isHidden = false
                cell.isUserInteractionEnabled = true
            }else{
                if globalIsListedProperty == true{
                    
                    cell.lblItemTitle?.textColor = UIColor(red:0, green:0, blue:0, alpha:0.8)
                    cell.lblItemCount.isHidden = false
                    cell.isUserInteractionEnabled = true
                }else{
                    cell.lblItemTitle?.textColor = UIColor.lightGray
                    cell.lblItemCount.isHidden = true
                    cell.isUserInteractionEnabled = false
                }
            }
            
            
            switch items[indexPath.row] {
            case "Property Info":
                if let myImage = UIImage(named: "ic_property_info") {
                    let tintableImage = myImage.withRenderingMode(.alwaysOriginal)
                    cell.itemImageView.image = tintableImage
                }
                cell.lblItemCount.isHidden = true
                break
                
            case "Notes":
                if let myImage = UIImage(named: "ic_notes") {
                    let tintableImage = myImage.withRenderingMode(.alwaysOriginal)
                    cell.itemImageView.image = tintableImage
                }
                
                if self.currentPropertyDetails?.notes != nil{
                    cell.lblItemCount.isHidden = false
                    if self.currentPropertyDetails?.notes?.count ?? 0 > 0{
                        
                        cell.lblItemCount.text = "\(String(describing: (self.currentPropertyDetails?.notes?.count)!))"
                        cell.lblItemCount.backgroundColor = self.greenCapsuleColor
                    }else{
                        cell.lblItemCount.isHidden = true
                    }
                }else{
                    cell.lblItemCount.isHidden = true
                }
                break
                
                
            case "Photos":
                if let myImage = UIImage(named: "ic_photos") {
                    let tintableImage = myImage.withRenderingMode(.alwaysOriginal)
                    cell.itemImageView.image = tintableImage
                }
                if self.currentPropertyDetails?.notes != nil{
                    cell.lblItemCount.isHidden = false
                    if self.currentPropertyDetails?.photos?.count ?? 0 > 0{
                        
                        cell.lblItemCount.text = "\(String(describing: (self.currentPropertyDetails?.photos?.count)!))"
                        cell.lblItemCount.backgroundColor = self.blueCapsuleColor
                    }else{
                        cell.lblItemCount.isHidden = true
                    }
                    
                }else{
                    cell.lblItemCount.isHidden = true
                }
                break
                
            case "Get Neighbor Details":
                if let myImage = UIImage(named: "ic_get_neighbours") {
                    let tintableImage = myImage.withRenderingMode(.alwaysOriginal)
                    cell.itemImageView.image = tintableImage
                }
                cell.itemIsActive.isHidden = true
                cell.lblItemCount.isHidden = true
                
                break
                
            case "Owner Details":
                if let myImage = UIImage(named: "ic_owner_details") {
                    let tintableImage = myImage.withRenderingMode(.alwaysOriginal)
                    cell.itemImageView.image = tintableImage
                }
                cell.lblItemCount.isHidden = true
                
                if currentPropertyDetails?.ownerInfo != nil{
                    cell.itemIsActive.isHidden = false
                    if currentPropertyDetails?.ownerInfo?.count == 0{
                        cell.itemIsActive.isHidden = true
                    }
                }else{
                    cell.itemIsActive.isHidden = true
                }
                
                break
                
            case "Mailer Wizard":
                if let myImage = UIImage(named: "new_ic_mailer_wizard") {
                    let tintableImage = myImage.withRenderingMode(.alwaysOriginal)
                    cell.itemImageView.image = tintableImage
                }
                cell.lblItemCount.isHidden = true
                if self.currentPropertyDetails?.ownerInfo != nil {
                    if self.currentPropertyDetails?.ownerInfo?.count ?? 0 > 0 {
                        cell.lblItemTitle?.textColor = UIColor(red:0, green:0, blue:0, alpha:0.8)
                        cell.isUserInteractionEnabled = true
                    }else{
                        cell.lblItemTitle?.textColor = UIColor.lightGray
                        cell.isUserInteractionEnabled = false
                    }
                } else{
                    cell.lblItemTitle?.textColor = UIColor.lightGray
                    cell.isUserInteractionEnabled = false
                }
                cell.itemIsActive.isHidden = true
                break
                
            case "Power Trace":
                if let myImage = UIImage(named: "ic_powertrace") {
                    let tintableImage = myImage.withRenderingMode(.alwaysOriginal)
                    cell.itemImageView.image = tintableImage
                }
                cell.lblItemCount.isHidden = true
                if self.currentPropertyDetails?.ownerInfo != nil {
                    if self.currentPropertyDetails?.ownerInfo?.count ?? 0 > 0 {
                        cell.lblItemTitle?.textColor = UIColor(red:0, green:0, blue:0, alpha:0.8)
                        cell.isUserInteractionEnabled = true
                    }else{
                        cell.lblItemTitle?.textColor = UIColor.lightGray
                        cell.isUserInteractionEnabled = false
                    }
                } else{
                    cell.lblItemTitle?.textColor = UIColor.lightGray
                    cell.isUserInteractionEnabled = false
                }
                
                
                if currentPropertyDetails?.powerTraceRequestID != nil{
                    cell.itemIsActive.isHidden = false
                }
                
                break
                
                
            case "Assign Tag":
                if let myImage = UIImage(named: "utags") {
                    let tintableImage = myImage.withRenderingMode(.alwaysOriginal)
                    cell.itemImageView.image = tintableImage
                }
                cell.lblItemCount.isHidden = true
                break
                
                
            case "Add To List":
                if let myImage = UIImage(named: "new_ic_list") {
                    let tintableImage = myImage.withRenderingMode(.alwaysOriginal)
                    cell.itemImageView.image = tintableImage
                }
                cell.lblItemCount.isHidden = true
                break
                
                
            case "Billing":
                if let myImage = UIImage(named: "ic_load_houzes") {
                    let tintableImage = myImage.withRenderingMode(.alwaysOriginal)
                    cell.itemImageView.image = tintableImage
                }
                cell.lblItemCount.isHidden = true
                break
                
                
            default: break
                // cell.titleImageView.image = UIImage(imageLiteralResourceName: "home.png")
            }
            cell.itemImageView.tintColor = UIColor(red:0.2, green:0.3, blue:0.41, alpha:1)
            
            return cell
        }
        
    }
    
    extension PropertyDetailsViewController: UITableViewDelegate {
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 60
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
            let cell = tableView.cellForRow(at: indexPath) as! PropertyDetailsTableViewCell
            
            if globalIsListedProperty == true{
                
                cell.lblItemTitle?.textColor = UIColor(red:0, green:0, blue:0, alpha:0.8)
                cell.lblItemCount.isHidden = false
                cell.isUserInteractionEnabled = true
                
                if globalSelectedPropertyFromList?.id == nil || globalSelectedPropertyFromList?.id == 0{
                    
                    cell.lblItemTitle?.textColor = UIColor.lightGray
                    cell.lblItemCount.isHidden = true
                    cell.isUserInteractionEnabled = false
                }
            }else{
                cell.lblItemTitle?.textColor = UIColor.lightGray
                cell.lblItemCount.isHidden = true
                cell.isUserInteractionEnabled = false
            }
            
            cell.backgroundColor = UIColor.white
            cell.contentView.backgroundColor = UIColor.white
            tableView.separatorStyle = .singleLine
            cell.itemImageView.tintColor = UIColor(red:0.07, green:0.82, blue:0.41, alpha:1)
            cell.lblItemCount.isHidden = true
            //cell.accessoryView?.backgroundColor = .white
            //cell.isHighlighted = false
            
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            
            switch items[indexPath.row] {
                
            case "Property Info":
                if globalIsListedProperty == true{
                    
                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "PropertyInfoViewController") as! PropertyInfoViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }else{
                    AppConfigs.showSnacbar(message: "This property is not added to any List. Please add to a List first", textColor: .red)
                }
                break
                
            case "Notes":
                cell.lblItemCount.isHidden = false
                if globalIsListedProperty == true{
                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "PropertyNotesViewController") as! PropertyNotesViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    AppConfigs.showSnacbar(message: "This property is not added to any List. Please add to a List first", textColor: .red)
                }
                break
                
                
            case "Photos":
                cell.lblItemCount.isHidden = false
                if globalIsListedProperty == true{
                    
                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "PropertyPhotosViewController") as! PropertyPhotosViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }else{
                    AppConfigs.showSnacbar(message: "This property is not added to any List. Please add to a List first", textColor: .red)
                }
                
                break
                
                
            case "Get Neighbor Details":
                if globalIsListedProperty == true{
                    
                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "NeighboursViewController") as! NeighboursViewController
                    
                    vc.currentPropertyDetails = self.currentPropertyDetails
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }else{
                    AppConfigs.showSnacbar(message: "This property is not added to any List. Please add to a List first", textColor: .red)
                }
                break
                
                
            case "Owner Details":
                
                if globalIsListedProperty == true{
                    
                    if self.currentPropertyDetails?.ownerInfo == nil || self.currentPropertyDetails?.ownerInfo?.count == 0{
                        let vc = mainStoryboard.instantiateViewController(withIdentifier: "PopUpForOwnerAndPowerTraceViewController") as! PopUpForOwnerAndPowerTraceViewController
                        //self.navigationController?.pushViewController(vc, animated: true)
                        vc.modalPresentationStyle = .overCurrentContext
                        vc.parentString = "OIandPT"
                        self.present(vc, animated: true, completion: nil)
                    }else{
                        let vc = mainStoryboard.instantiateViewController(withIdentifier: "OwnerDetailsViewController") as! OwnerDetailsViewController
                        vc.propertyID = currentPropertyDetails?.id ?? 0
                        vc.titleForNav = "Owner Details"
                        vc.currentPropertyDetails = self.currentPropertyDetails
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }else{
                    AppConfigs.showSnacbar(message: "This property is not added to any List. Please add to a List first", textColor: .red)
                }
                cell.lblItemCount.isHidden = true
                
                break
                
                
            case "Mailer Wizard":
                if globalIsListedProperty == true{
                    
                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "ChooseMailerWizardViewController") as! ChooseMailerWizardViewController
                    vc.propertyID = (self.currentPropertyDetails?.id)!
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    AppConfigs.showSnacbar(message: "This property is not added to any List. Please add to a List first", textColor: .red)
                }
                break
                
            case "Power Trace":
                if globalIsListedProperty == true{
                    
                    if self.currentPropertyDetails?.powerTraceRequestID == nil{
                        let vc = mainStoryboard.instantiateViewController(withIdentifier: "PopUpForOwnerAndPowerTraceViewController") as! PopUpForOwnerAndPowerTraceViewController
                        vc.parentString = "pt"
                        //self.navigationController?.pushViewController(vc, animated: true)
                        vc.modalPresentationStyle = .overCurrentContext
                        self.present(vc, animated: true, completion: nil)
                    }else{
                        let vc = mainStoryboard.instantiateViewController(withIdentifier: "OwnerDetailsViewController") as! OwnerDetailsViewController
                        vc.propertyID = currentPropertyDetails?.id ?? 0
                        vc.titleForNav = "Power Trace"
                        vc.currentPropertyDetails = self.currentPropertyDetails
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    
                }
                
                
                break
                
            case "Assign Tag":
                if globalIsListedProperty == true{
                    
                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "AssignTagsViewController") as! AssignTagsViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    AppConfigs.showSnacbar(message: "This property is not added to any List. Please add to a List first", textColor: .red)
                }
                
                
                break
                
            case "Add To List":
                
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "ListOfListViewController") as! ListOfListViewController
                vc.modalPresentationStyle = .fullScreen
                vc.parentString = "PropertyDetails"
                self.present(vc, animated: true, completion: nil)
                
                break
                
                
            case "Billing":
                
                break
                
            default: break
            }
            
        }
        
        func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
            if self.items.count > indexPath.row{
                
                
            }
            
        }
    }
    
    extension PropertyDetailsViewController: GMSMapViewDelegate{
        
    }
