//
//  PolicyPpoUpViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 11/26/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit

import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit
import Alamofire


class PolicyPpoUpViewController: UIViewController, GIDSignInDelegate {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var btnPrivacyPolicy: UIButton!
    @IBOutlet weak var btnTermsCondition: UIButton!
    
    var type: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblTitle.textColor = UIColor(red:0, green:0, blue:0, alpha:0.8)
        lblSubTitle.textColor = UIColor(red:0.41, green:0.41, blue:0.41, alpha:1)
        
        btnPrivacyPolicy.underlined()
        btnTermsCondition.underlined()
        
        GIDSignIn.sharedInstance()?.delegate = self
        GIDSignIn.sharedInstance().presentingViewController = self
        
        if GIDSignIn.sharedInstance()?.currentUser != nil{
            print("Google User Found", GIDSignIn.sharedInstance()?.currentUser! as Any)
        }
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.view.addGestureRecognizer(tap)
        
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func showTermsAndCondition(_ sender: Any) {
        guard let url = URL(string: UrlManager.getTermsandServicesURL()) else {
            return
        }
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func showPrivacyPolicy(_ sender: Any) {
        guard let url = URL(string: UrlManager.getPrivacyPolicyURL()) else {
            return
        }
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func btnAgreeClicked(_ sender: Any) {
        
        if self.type == "fb"{
            //loginWithFb() //to do
                    if Network.reachability.status == .unreachable{
                        AppConfigs.updateUserInterface(view: view)
                    }else{
                        loginWithFb()
                    }
        }
        else if self.type == "google"{
            GIDSignIn.sharedInstance().signIn()
        }else{
            
            let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
            guard let registerVC = storyBoard.instantiateViewController(withIdentifier: "RegistrationNavigationController") as? RegistrationNavigationController  else
            { return  }
            registerVC.modalPresentationStyle = .fullScreen
            present(registerVC, animated: true, completion: nil)
        }
    }
    
    
    func loginWithFb() {
        
        let loginManager = LoginManager()
        //UIApplication.shared.statusBarStyle = .default  // remove this line if not required
        loginManager.logIn(permissions: ["public_profile", "email"], from: self) { (loginResult, error) in
            
            print(loginResult!)
            
            if AccessToken.current != nil{
                AppConfigs.registerSocialUser(view: self.view, vc: self, type: "fb") { (response,responseCode, status,error)  in
                    
                    if responseCode == 200{
                        AppConfigs.saveAccessToken(token: (response?.accessToken)!)
                        AppConfigs.saveRefreshToken(token: (response?.refreshToken)!)
                        
                        let url : String = UrlManager.getCurrentUserInfoURL()
                        self.getCurrentUserInfo(urlString: url)
                        
                    }else{
                        if response != nil{
                            
                            AppConfigs.showSnacbar(message: (response?.errorDescription)!, textColor: .red)
                        }else{
                            
                            AppConfigs.showSnacbar(message: error!, textColor: .red)
                        }
                    }
                }
            }
        }
    }
    
    // Stop the UIActivityIndicatorView animation that was started when the user
    // pressed the Sign In button
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error?) {
        //myActivityIndicator.stopAnimating()
    }
    
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
                print("The user has not signed in before or they have since signed out.")
            } else {
                print("\(error.localizedDescription)")
            }
            return
        }
        let email = user.profile.email
        // ...
        
        print("\(String(describing: email))")
        
        AppConfigs.registerSocialUser(view: self.view, vc: self, type: "google") { (response,responseCode, status,error)  in
            
            if responseCode == 200{
                AppConfigs.saveAccessToken(token: (response?.accessToken)!)
                AppConfigs.saveRefreshToken(token: (response?.refreshToken)!)
                
                
                let url : String = UrlManager.getCurrentUserInfoURL()
                self.getCurrentUserInfo(urlString: url)
                
            }else{
                if response != nil{
                    
                    AppConfigs.showSnacbar(message: (response?.errorDescription)!, textColor: .red)
                }else{
                    
                    AppConfigs.showSnacbar(message: error!, textColor: .red)
                }
            }
        }
    }
    
    
    func getCurrentUserInfo(urlString:String) {
        print(urlString)
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success",data)
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    
                    DispatchQueue.main.async {
                        let currentUser = try? JSONDecoder().decode(CurrentUserInfoResponse.self, from: response.data!)
                        AppConfigs.saveCurrentUserInfo(user: currentUser!)
                        print(AppConfigs.getCurrentUserInfo().email!)
                        


                        if self.isKeyPresentInUserDefaults(key: AppConfigs.isFirstLogin) == true{
                            //key exist
                            AppConfigs.saveFirstLoginFlag(flag:false)
                        }else{
                            //key doesn't exist
                            AppConfigs.saveFirstLoginFlag(flag:true)
                        }
                        
                        
                        //                        SocketIOManager.shared.establishConnection()
                        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
                        guard let vc = storyBoard.instantiateViewController(withIdentifier: "SMNavigationController") as? SMNavigationController  else
                        { return  }
                        vc.modalPresentationStyle = .fullScreen
                        self.present(vc, animated: true, completion: nil)
                        // UIApplication.shared.keyWindow?.rootViewController = vc
                    }
                }
                    
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)

                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
    
    
}
