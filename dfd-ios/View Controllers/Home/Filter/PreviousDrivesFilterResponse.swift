//
//  PreviousDrivesFilterResponse.swift
//  HouZes
//
//  Created by Mahadhi Hassan Chowdhury on 4/20/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//

import Foundation
//   let previousDrivesFilterResponse = try? newJSONDecoder().decode(PreviousDrivesFilterResponse.self, from: jsonData)

import Foundation

// MARK: - PreviousDrivesFilterResponse
class PreviousDrivesFilterResponse: Codable {
    var histories: [MyDrive]?
    var properties: [LoadListFilterResponseElement]?

    init(histories: [MyDrive]?, properties: [LoadListFilterResponseElement]?) {
        self.histories = histories
        self.properties = properties
    }
}
