//
//  AllTeamResponse.swift
//  HouZes
//
//  Created by Mahadhi Hassan Chowdhury on 4/14/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//

import Foundation
//   let allTeamFilterResponse = try? newJSONDecoder().decode(AllTeamFilterResponse.self, from: jsonData)

import Foundation

// MARK: - AllTeamFilterResponse
class AllTeamFilterResponse: Codable {
    var users: [CurrentUserInfoResponse]?
    var unregisteredInvitations: [UnregisteredInvitation]?

    enum CodingKeys: String, CodingKey {
        case users
        case unregisteredInvitations
    }

    init(users: [CurrentUserInfoResponse]?, unregisteredInvitations: [UnregisteredInvitation]?) {
        self.users = users
        self.unregisteredInvitations = unregisteredInvitations
    }
}
