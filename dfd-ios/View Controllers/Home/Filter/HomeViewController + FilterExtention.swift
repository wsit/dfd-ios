//
//  HomeViewController + FilterExtention.swift
//  HouZes
//
//  Created by Mahadhi Hassan Chowdhury on 4/13/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

var keyArray : [String] = []
var valueArray : [MList] = []

var selectedTeamMemberID: Int = 0
var selectedListID: Int = 0
var globalAllPathsForFilter: [GMSMutablePath] = []
var globalAllPolylineForFilter: [GMSPolyline] = []

extension HomeViewController{
    
    func handleClearFilter(){
        previouslySelected = false
        self.teamDropDown.text = "Choose Team"
        self.mySegmentedControl.selectedSegmentIndex = 0
        selectedSegmentIndexAfterFilter = 0
        self.lblDurationOrList.text = "Choose List"
        self.listDropDown.text = "Choose List"

        listDropDown.selectedIndex = nil
        teamDropDown.selectedIndex = nil
        
        deleteAllPropertiesFromFilteredOldList()
        deleteAllDrivesFromFilteredDrives()
        selectedListID = 0
        selectedTeamMemberID = 0
    }
    
    func configureDataForFilter(){
        
        
        self.leftFilterView.isHidden = true
        
        self.teamDropDown.didSelect{(selectedText , index ,id) in
            
            self.teamDropDown.text = selectedText
            print("Selected String: \(selectedText) \n index: \(index) \n id: \(id)")
            selectedTeamMemberID = self.teamDropDownIDs[index]
            
            if self.mySegmentedControl.selectedSegmentIndex == 0{
                
                let url: String = UrlManager.baseURL() + UrlManager.apiString() + "/list/load-list/user/\(selectedTeamMemberID)/"
                print(url)
                self.getLoadListData(urlString: url)
                
            }else{
                self.listDropDown.optionArray = ["3 Months", "6 Months", "9 Months", "12 Months"]
                self.listDropDown.optionIds = [3,6,9,12]
                self.listDropDownIDs =  [3,6,9,12]
                self.listDropDown.selectedIndex = 0
                self.listDropDown.text  = self.listDropDown.optionArray[self.listDropDown.selectedIndex!]
                selectedListID = self.listDropDownIDs[0]
                
            }
        }
        
        self.listDropDown.didSelect{(selectedText , index ,id) in
            
            self.listDropDown.text = selectedText
            print("Selected String: \(selectedText) \n index: \(index)")
            selectedListID = self.listDropDownIDs[index]
            print("selectedListID:", selectedListID)
           // self.listDropDown.selectedIndex
        }
        
        
    }
    
    @IBAction func segmentedValueChanged(_ sender: UISegmentedControl) {
        
        selectedSegmentIndexAfterFilter = sender.selectedSegmentIndex
        
        if sender.selectedSegmentIndex == 0{
            if selectedTeamMemberID != 0{
                self.listDropDown.optionArray.removeAll()
                self.listDropDownIDs.removeAll()
                
                listDropDown.placeholder = "Choose List"
                lblDurationOrList.text = "Choose List"
                self.listDropDown.text  = "Choose List"
                self.listDropDown.selectedIndex = nil
                
                let url: String = UrlManager.baseURL() + UrlManager.apiString() + "/list/load-list/user/\(selectedTeamMemberID)/"
                self.getLoadListData(urlString: url)
                
            }else{
                
                self.listDropDown.optionArray.removeAll()
                self.listDropDownIDs.removeAll()
                listDropDown.placeholder = "Choose List"
                lblDurationOrList.text = "Choose List"
                self.listDropDown.text  = "Choose List"
                self.listDropDown.selectedIndex = nil
                self.listDropDown.optionIds = []
                self.listDropDownIDs =  []
            }
        }else{
            lblDurationOrList.text = "Duration"
            listDropDown.placeholder = "Choose Duration"
            self.listDropDown.optionArray = ["3 Months", "6 Months", "9 Months", "12 Months"]
            self.listDropDown.optionIds = [3,6,9,12]
            self.listDropDownIDs =  [3,6,9,12]
            self.listDropDown.selectedIndex = 0
            self.listDropDown.text  = self.listDropDown.optionArray[self.listDropDown.selectedIndex!]
            selectedListID = self.listDropDownIDs[0]
            
        }
    }
    
    func setUpLeftViews (){
        
        btnApply.backgroundColor = UIColor(red:0.47, green:0.67, blue:0.98, alpha:1)
        mySegmentedControl.selectedSegmentIndex = selectedSegmentIndexAfterFilter
        listDropDown.placeholder = "Choose List"
        lblDurationOrList.text = "Choose List"
        
        btnDrag.backgroundColor = UIColor(red:0.62, green:0.6, blue:0.58, alpha:1)
        
        teamDropDown.backgroundColor = listDropDownBackgroundColor
        teamDropDown.borderColor = listDropDownBorderColor
        
        listDropDown.backgroundColor =  listDropDownBackgroundColor
        listDropDown.borderColor = listDropDownBorderColor
        
        //segmented controls
        mySegmentedControl.backgroundColor = segmentedControlBackgroundColor
        mySegmentedControl.borderColor = UIColor(red:0.89, green:0.91, blue:0.95, alpha:1)
        
        mySegmentedControl.setTitleTextAttributes([.foregroundColor: UIColor(red:0.48, green:0.56, blue:0.72, alpha:1), .font: ProNovaR14], for: .selected)//UIColor(red:0.48, green:0.56, blue:0.72, alpha:1)
        mySegmentedControl.setTitleTextAttributes([.foregroundColor: UIColor(red:0.32, green:0.38, blue:0.52, alpha:1), .font: ProNovaR14], for: .normal)
        
    }
    
    @objc func handleDismiss(sender: UIPanGestureRecognizer) {
        // The rest of the code we will write here
        switch sender.state {
            
        case .changed:
            viewTranslation = sender.translation(in: leftFilterView)
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.leftFilterView.transform = CGAffineTransform(translationX: 0, y: self.viewTranslation.y)
            })
            
        case .ended:
            if viewTranslation.y < 200 {
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    self.leftFilterView.transform = .identity
                })
            } else {
                dismiss(animated: true, completion: nil)
            }
        default:
            break
        }
    }
    
    
    
    func getAllTeamMembers(urlString:String)  {
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success",data)
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    
                    let teamMembers = try? JSONDecoder().decode(AllTeamFilterResponse.self, from: response.data!)
                    //self.jsonResponse = teamMembers
                    let userList = teamMembers?.users
                  //  print("userList------\n",userList)
                    self.removeSpinner()
                    self.teamMembers.removeAll()
                    self.teamMembers = userList ?? []
                    
                    var selectedIndex : Int = 0
                    
                    DispatchQueue.main.async {
                        //print("userList------------\n",self.teamMembers)
                        self.removeSpinner()//self.view.activityStopAnimating()
                        //self.myTeamsTableView.reloadData()
                        self.teamDropDown.optionArray.removeAll()
                        self.teamDropDownIDs.removeAll()
                        
                        for (index,user) in self.teamMembers.enumerated(){
                            var  name = (user.firstName ?? "") + " " + (user.lastName ?? "")
                            let id = user.id
                            if id == AppConfigs.getCurrentUserInfo().id{
                                name = "Myself"
                                selectedIndex = index
                            }
                            self.teamDropDown.optionArray.append(name)
                            self.teamDropDownIDs.append(id!)
                            
                            if previouslySelected == false{
                                if id == AppConfigs.getCurrentUserInfo().id{
                                    self.teamDropDown.text = self.teamDropDown.optionArray[index]
                                    selectedTeamMemberID = self.teamDropDownIDs[index]
                                    self.teamDropDown.selectedIndex = selectedIndex
                                }
                            }
                        }
                        
                        self.teamDropDown.optionIds = self.teamDropDownIDs
//                        print("optionArray------------\n",self.teamDropDown.optionArray)
//                        print("optionIds------------\n",self.teamDropDownIDs)

                         if self.mySegmentedControl.selectedSegmentIndex == 0{
                             
                             let url: String = UrlManager.baseURL() + UrlManager.apiString() + "/list/load-list/user/\(selectedTeamMemberID)/"
                             print(url)
                             self.getLoadListData(urlString: url)
                             
                         }
                    }
                }
                    
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                self.removeSpinner()
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                
                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
    func callAPIForApplyFilterOnList()  {
        if selectedListID != 0{

            let url: String = UrlManager.baseURL() + UrlManager.apiString() +  "/list/\(selectedListID)/load-list/properties/?latitude=\((myLastLocationGlobal?.coordinate.latitude)!)&longitude=\((myLastLocationGlobal?.coordinate.longitude)!)"
            print(url)
            self.loadAllPropertiesFromListGeoFence(urlString: url)
        }
    }
    
    func callAPIForApplyFilterOnPreviuosDrives()  {
        
        if selectedTeamMemberID != 0 && selectedListID != 0{

            let url: String = UrlManager.baseURL() + UrlManager.apiString() +  "/history/previous-drives/user/\(selectedTeamMemberID)/?latitude=\((myLastLocationGlobal?.coordinate.latitude)!)&longitude=\((myLastLocationGlobal?.coordinate.longitude)!)&month=\(selectedListID)"
            print(url)
            self.loadAllPreviousDrivesFromGeoFence(urlString: url)
        }
    }
    
    func getLoadListData(urlString:String)  {
        
       //self.showSpinner(onView: self.listDropDown)
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success",data)
                let statusCode = response.response?.statusCode
                self.removeSpinner()
                if statusCode == 200{
                    
                    let allListsRes = try? JSONDecoder().decode([MList].self, from: response.data!)
                    self.allLists.removeAll()
                    self.allLists = allListsRes!
                    
                    DispatchQueue.main.async {
                        self.removeSpinner()
                        print("allLists------------\n",self.allLists)
                        self.removeSpinner()//self.view.activityStopAnimating()
                        
                        self.listDropDown.optionArray.removeAll()
                        self.listDropDownIDs.removeAll()
                        
                        for list in self.allLists{
                            let name = list.name
                            let id = list.id
                            self.listDropDown.optionArray.append(name!)
                            self.listDropDownIDs.append(id!)
                            self.listDropDown.optionIds?.append(id!)
                        }
//                        print("optionArray------------\n",self.listDropDown.optionArray)
//                        print("optionIds------------\n",self.listDropDownIDs)
//
                        if previouslySelected == false{

                            self.listDropDown.text = self.listDropDown.optionArray[0]
                            selectedListID = self.listDropDownIDs[0]
                        }
                    }
                }
                    
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                self.removeSpinner()
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                
                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
    func deleteAllPropertiesFromFilteredOldList(){
        
        allPropertyFromFilteredOldList.removeAll()
        for marker in self.allPropertyMarkersFromFilteredList{
            marker.map = nil
        }
        self.allPropertyMarkersFromFilteredList.removeAll()
        print(allPropertyFromFilteredOldList.count)
        print(allPropertyMarkersFromFilteredList.count)
        
    }
    
    func deleteAllDrivesFromFilteredDrives(){
        
        allOldDrivesFromFilteredPreDrives.removeAll()
        
        for path in globalAllPathsForFilter{
            path.removeAllCoordinates()
        }
        
        for polyline in globalAllPolylineForFilter{
            polyline.map = nil
        }
        
        globalAllPathsForFilter.removeAll()
        globalAllPolylineForFilter.removeAll()
        
        print("globalAllPolylineForFilter.count",globalAllPolylineForFilter.count)
        print("globalAllPathsForFilter.count",globalAllPathsForFilter.count)
        
    }
    
    func loadAllPreviousDrivesFromGeoFence(urlString:String){
        //MyDrive
        print(urlString)
        //self.showSpinner(onView: self.userImageView)
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success",data)
                let statusCode = response.response?.statusCode
                print(statusCode!)
                if statusCode == 200{
                    
                    DispatchQueue.main.async {
                        self.removeSpinner()
                        let driveFilterData = try? JSONDecoder().decode(PreviousDrivesFilterResponse.self, from: response.data!)
                        if driveFilterData != nil{
                            self.allDrivesFromFilteredPreDrives.removeAll()
                            self.allDrivesFromFilteredPreDrives = driveFilterData?.histories ?? []

                            //self.deleteAllPropertiesFromFilteredOldList()
                            self.deleteAllDrivesFromFilteredDrives()

                            allOldDrivesFromFilteredPreDrives = self.allDrivesFromFilteredPreDrives
                            print("allOldDrivesFromFilteredPreDrives --->", allOldDrivesFromFilteredPreDrives.count)
                            self.removeSpinner()
                            self.showAllDrivesFromFilteredDrives()
                            

                            if driveFilterData?.properties != nil{
                                self.allPropertyFromFilteredNewList.removeAll()
                                self.allPropertyFromFilteredNewList = driveFilterData!.properties ?? []
                                
                                self.deleteAllPropertiesFromFilteredOldList()
                                //self.deleteAllDrivesFromFilteredDrives()
                                
                                allPropertyFromFilteredOldList = self.allPropertyFromFilteredNewList
                                print("driveFilterData allPropertyFromFilteredOldList --->", allPropertyFromFilteredOldList.count)
                                
                                self.showAllPropertiesFromFilteredList()
                            }else{
                                print("driveFilterData properties is nil")
                                AppConfigs.showSnacbar(message: "No Data Found", textColor: .red)
                            }
                            
                        }else{
                            self.removeSpinner()
                            print("driveFilterData properties is nil")
                            AppConfigs.showSnacbar(message: "No Data Found", textColor: .red)
                        }
                        
                        
//                        if driveFilterData != nil{
//                            self.allDrivesFromFilteredPreDrives.removeAll()
//                            self.allDrivesFromFilteredPreDrives = driveFilterData!
//
//                            self.deleteAllPropertiesFromFilteredOldList()
//                            self.deleteAllDrivesFromFilteredDrives()
//
//                            allOldDrivesFromFilteredPreDrives = self.allDrivesFromFilteredPreDrives
//                            print("allOldDrivesFromFilteredPreDrives --->", allOldDrivesFromFilteredPreDrives.count)
//                            self.removeSpinner()
//                            self.showAllDrivesFromFilteredDrives()
//                        }else{
//                            self.removeSpinner()
//                            print("loadListFilterData is nil")
//                            AppConfigs.showSnacbar(message: "No Data Found", textColor: .red)
//                        }
                    }
                }
                else if statusCode == 403{
                    self.removeSpinner()                    
                }
                else if statusCode == 401{
                    self.removeSpinner()
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                    
                }
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
    func loadAllPropertiesFromListGeoFence(urlString:String) {
        print(urlString)
        //self.showSpinner(onView: self.userImageView)
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success",data)
                let statusCode = response.response?.statusCode
                print(statusCode!)
                if statusCode == 200{
                    
                    DispatchQueue.main.async {
                        
                        let loadListFilterData = try? JSONDecoder().decode([LoadListFilterResponseElement].self, from: response.data!)
                        if loadListFilterData != nil{
                            self.allPropertyFromFilteredNewList.removeAll()
                            self.allPropertyFromFilteredNewList = loadListFilterData!
                            
                            self.deleteAllPropertiesFromFilteredOldList()
                            self.deleteAllDrivesFromFilteredDrives()
                            
                            allPropertyFromFilteredOldList = self.allPropertyFromFilteredNewList
                            print("allPropertyFromFilteredOldList --->", allPropertyFromFilteredOldList.count)
                            
                            self.showAllPropertiesFromFilteredList()
                        }else{
                            print("loadListFilterData is nil")
                            AppConfigs.showSnacbar(message: "No Data Found", textColor: .red)
                        }
                    }
                }
                else if statusCode == 403{
                    self.removeSpinner()
                    
                    
                }
                else if statusCode == 401{
                    self.removeSpinner()
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                    
                }
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
    
    func showAllDrivesFromFilteredDrives(){
        
        print("showAllDrivesFromFilteredDrives")
        globalAllPathsForFilter.removeAll()
        globalAllPolylineForFilter.removeAll()
        
        if allOldDrivesFromFilteredPreDrives.count > 0{
            
            for drive in allOldDrivesFromFilteredPreDrives{
               
                var polylinesWithPosition: [LocationWithIndexPosition] = []
                
                let string = (drive.polylines) ?? ""
                let data = string.data(using: .utf8)!
                
                do {
                    if let polylines = try? JSONDecoder().decode([Polyline].self, from: data)
                    {
                        print("polylines.count", polylines.count)
                        
                        for (index,polyline) in polylines.enumerated(){
                            
                            
                            let pwp = LocationWithIndexPosition()
                            pwp.latitude = polyline.latitude!
                            pwp.longitude = polyline.longitude!
                            
                            if polyline.position != nil{
                                pwp.position = polyline.position!
                            }else{
                                pwp.position = 0
                            }
                            polylinesWithPosition.append(pwp)
                        }
                        //------------------start handle pause/resume polyline draw while driving
                        let grouped = polylinesWithPosition.group(by: { $0.position })
                        print("grouped.count", grouped.count)
                        
                        for (index,element) in grouped.enumerated(){
                            
//                            print("index--->",index)
//                            print("element--->",element)
//                            print(element.key)
//                            print(element.value)
//
                            let locations = element.value
                            let lPath = GMSMutablePath()
                            
                            for (lindex, lelement) in locations.enumerated(){
                                let lat = lelement.latitude
                                let long = lelement.longitude
                                lPath.add(CLLocationCoordinate2D(latitude: lat, longitude: long))
                                //path.add(CLLocationCoordinate2D(latitude: lat, longitude: long))
                                globalAllPathsForFilter.append(lPath)
                                //self.timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(animatePolylinePath), userInfo: nil, repeats: true)
                            }
                            let lPolyline = GMSPolyline(path: lPath) // GMSPolyline(path: path)
                            lPolyline.strokeWidth = 3
                            lPolyline.strokeColor = UIColor.blue
                            lPolyline.map = self.mapView
                            globalAllPolylineForFilter.append(lPolyline)
                            //oldPolylineArr2.append(lPolyline)
                        }
                        //------------------start handle pause/resume polyline draw while driving
                        
                        
//                        let bounds = GMSCoordinateBounds(path: path)
//                        mapview.animate(toZoom: 10)
//                        self.myMap!.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 50.0))
                        
                    }else{
                        
                        print("very bad json")
                    }
                }catch let error  {
                    print(error.localizedDescription)
                }
            }
        }
        
    }
    
    func showAllPropertiesFromFilteredList(){
        print( allPropertyFromFilteredOldList.count )
        if allPropertyFromFilteredOldList.count > 0{
            
            allPropertyMarkersFromFilteredList.removeAll()
            for property in allPropertyFromFilteredOldList{
                let marker = GMSMarker()
                let lat = property.latitude
                let long = property.longitude
                marker.position = CLLocationCoordinate2D(latitude: lat!, longitude: long!)
                marker.title = "\((property.id)!)"
                
                let markerImage = UIImage(named: "v2_filtered_marker")!.withRenderingMode(.alwaysTemplate)
                //creating a marker view
                let markerView = UIImageView(image: markerImage)
                let tags = property.propertyTags
                
                if tags != nil{
                    if tags?.count == 0{
                        
                        markerView.tintColor =  UIColor.black
                        marker.iconView = markerView
                        marker.icon = markerImage
                        
                    }else if tags!.count > 1{
                        
                        markerView.tintColor =  UIColor.gray
                        marker.iconView = markerView
                        marker.icon = markerImage
                        
                    }else if tags?.count == 1{
                        
                        let tag = tags?[0]
                        let color = tag?.color?.colorCode
                        print(color ?? "")
                        
                        let propertyTagID = tag?.id ?? 0
                        print(propertyTagID)
                        
                        
                        for tag in globalAllTags{
                            
                            let id = tag.id
                            if id == propertyTagID{
                                let color = tag.color?.colorCode
                                print(color ?? "")
                                let uiColor =  UIColor(hexString: color!)
                                markerView.tintColor = uiColor
                                marker.iconView = markerView
                                marker.icon = markerImage
                            }
                        }
                    }else{
                        print("unknown case")
                        markerView.tintColor =   UIColor.black
                        marker.iconView = markerView
                        marker.icon = markerImage
                    }
                }else{
                    markerView.tintColor = UIColor.black//.systemBlue
                    marker.iconView = markerView
                    marker.icon = markerImage
                }
                
                marker.map = self.mapView
                self.allPropertyMarkersFromFilteredList.append(marker)
            }
            print("allPropertyMarkersFromFilteredList--->",self.allPropertyMarkersFromFilteredList.count)
        }
        
    }
    
    
    func getPreviousDrivesData(urlString:String)  {
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success",data)
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    DispatchQueue.main.async {
                        
                        
                    }
                }
                    
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                self.removeSpinner()
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                
                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
}

class HalfSizePresentationController : UIPresentationController {
    
    override var frameOfPresentedViewInContainerView: CGRect{
        
        return CGRect(x: 0, y: containerView!.bounds.height/4, width: containerView!.bounds.width, height: containerView!.bounds.height/2)
    }
    
    
}
