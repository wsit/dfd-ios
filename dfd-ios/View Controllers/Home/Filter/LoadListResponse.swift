//
//  LoadListResponse.swift
//  HouZes
//
//  Created by Mahadhi Hassan Chowdhury on 4/14/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//
///   let loadListResponse = try? newJSONDecoder().decode(LoadListResponse.self, from: jsonData)

import Foundation

// MARK: - LoadListResponseElement
class LoadListResponseElement: Codable {
    var id: Int?
    var name: String?
    var leadsCount: Int?
    var fetchLatLng, isDefault: Bool?
    var createdAt, updatedAt: String?
    var user: Int?
    var list: MList?

    enum CodingKeys: String, CodingKey {
        case id, name
        case leadsCount
        case fetchLatLng
        case isDefault
        case createdAt
        case updatedAt
        case user
    }

    init(id: Int?, name: String?, leadsCount: Int?, fetchLatLng: Bool?, isDefault: Bool?, createdAt: String?, updatedAt: String?, user: Int?) {
        self.id = id
        self.name = name
        self.leadsCount = leadsCount
        self.fetchLatLng = fetchLatLng
        self.isDefault = isDefault
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.user = user
    }
}

typealias LoadListResponse = [LoadListResponseElement]
