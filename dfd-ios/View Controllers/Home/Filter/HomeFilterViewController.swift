//
//  HomeFilterViewController.swift
//  HouZes
//
//  Created by Mahadhi Hassan Chowdhury on 4/13/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//

import UIKit

let listDropDownBackgroundColor: UIColor = UIColor(red:0.97, green:0.98, blue:0.99, alpha:1.00)
let listDropDownBorderColor: UIColor = UIColor(red:0.97, green:0.98, blue:0.99, alpha:1.00)
let segmentedControlBackgroundColor: UIColor = UIColor(red:0.93, green:0.94, blue:0.95, alpha:1)

class HomeFilterViewController: UIViewController {

    @IBOutlet weak var btnDrag: UIButton!
    @IBOutlet weak var teamDropDown: TextDropDown!
    @IBOutlet weak var mySegmentedControl: UISegmentedControl!
    @IBOutlet weak var lblDurationOrList: UILabel!
    @IBOutlet weak var listDropDown: TextDropDown!
    @IBOutlet weak var btnApply: UIButton!
    @IBOutlet weak var upView: UIView!
    
    var viewTranslation = CGPoint(x: 0, y: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        FontAndColorConfigs.setDynamicLabelFontSize(labels: [lblDurationOrList], type: TextType.ListSubTitle.rawValue)
        FontAndColorConfigs.setDynamicButtonsFontSize(buttons: [btnApply], type: TextType.ListTitle.rawValue)
        
        setUpLeftViews()
        self.upView.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(handleDismiss)))
        btnDrag.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(handleDismiss)))
        self.upView.clipsToBounds = true
        self.view.clipsToBounds = true
        self.view.setNeedsLayout()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.setNeedsLayout()
    }
    
    func setUpLeftViews (){
        
        btnApply.backgroundColor = UIColor(red:0.47, green:0.67, blue:0.98, alpha:1)
        mySegmentedControl.selectedSegmentIndex = 0
        listDropDown.placeholder = "Choose List"
        lblDurationOrList.text = "Choose List"
       
        btnDrag.backgroundColor = UIColor(red:0.62, green:0.6, blue:0.58, alpha:1)
        
        teamDropDown.backgroundColor = listDropDownBackgroundColor
        teamDropDown.borderColor = listDropDownBorderColor
        
        listDropDown.backgroundColor =  listDropDownBackgroundColor
        listDropDown.borderColor = listDropDownBorderColor
        // Do any additional setup after loading the view.
        self.upView.roundCorners(corners: [.topRight, .topLeft], radius: 16.0)
        
        //segmented controls
        mySegmentedControl.backgroundColor = segmentedControlBackgroundColor
        mySegmentedControl.borderColor = UIColor(red:0.89, green:0.91, blue:0.95, alpha:1)
        
        mySegmentedControl.setTitleTextAttributes([.foregroundColor: UIColor(red:0.48, green:0.56, blue:0.72, alpha:1), .font: ProNovaR14], for: .selected)//UIColor(red:0.48, green:0.56, blue:0.72, alpha:1)
        mySegmentedControl.setTitleTextAttributes([.foregroundColor: UIColor(red:0.32, green:0.38, blue:0.52, alpha:1), .font: ProNovaR14], for: .normal)
        
    }
    
    @objc func handleDismiss(sender: UIPanGestureRecognizer) {
        // The rest of the code we will write here
        switch sender.state {
       
        case .changed:
            viewTranslation = sender.translation(in: view)
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.view.transform = CGAffineTransform(translationX: 0, y: self.viewTranslation.y)
            })
            
        case .ended:
            if viewTranslation.y < 200 {
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    self.view.transform = .identity
                })
            } else {
                dismiss(animated: true, completion: nil)
            }
        default:
            break
        }
    }
    
    @IBAction func goToPreviousVC(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnHideClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func segmentedValueChanged(_ sender: UISegmentedControl) {
        
        if sender.selectedSegmentIndex == 0{
            listDropDown.placeholder = "Choose List"
            lblDurationOrList.text = "Choose List"
        }else{
            lblDurationOrList.text = "Duration"
            listDropDown.placeholder = "Choose Duration"
        }
    }
    
}
