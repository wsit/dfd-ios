//
//  LoadListFilterResponse.swift
//  HouZes
//
//  Created by Mahadhi Hassan Chowdhury on 4/15/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//
//   let loadListFilterResponse = try? newJSONDecoder().decode(LoadListFilterResponse.self, from: jsonData)

import Foundation

// MARK: - LoadListFilterResponseElement
class LoadListFilterResponseElement: Codable {
    var id: Int?
    var latitude, longitude: Double?
    var propertyTags: [PropertyTag]?

    enum CodingKeys: String, CodingKey {
        case id, latitude, longitude
        case propertyTags = "property_tags"
    }

    init(id: Int?, latitude: Double?, longitude: Double?, propertyTags: [PropertyTag]?) {
        self.id = id
        self.latitude = latitude
        self.longitude = longitude
        self.propertyTags = propertyTags
    }
}
