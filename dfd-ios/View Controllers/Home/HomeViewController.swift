//
//  HomeViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 5/2/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit
import LNSideMenu
import GoogleMaps
import SnapKit
import TTGSnackbar
import Floaty
import JJFloatingActionButton
//import GooglePlaces
//import GooglePlacePicker
import Alamofire
import CoreFoundation
import DropDown
import RealmSwift
import Kingfisher


var isRegionExist: Bool = false
var allPropertyFromFilteredOldList: [LoadListFilterResponseElement] = []
var allOldDrivesFromFilteredPreDrives: [MyDrive] = []
var selectedSegmentIndexAfterFilter : Int = 0
//var isFilteredCleared : Bool = false
var previouslySelected: Bool = false

class HomeViewController: UIViewController, UIViewControllerTransitioningDelegate{
    
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        
        return HalfSizePresentationController(presentedViewController:presented, presenting: presenting)
        
    }
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var mapView: GMSMapView!
    
    // -----------new outlets & variables ---------------------------
    
    @IBOutlet weak var btnMinimize: UIButton!
    @IBOutlet weak var btnDriveNow: UIButton!
    @IBOutlet weak var btnPublic: UIButton!
    @IBOutlet weak var btnPrivate: UIButton!
    @IBOutlet weak var btnInfo: UIButton!
    @IBOutlet weak var informativeView: UIView!
    @IBOutlet weak var lblInformationHeader: UILabel!
    @IBOutlet weak var lblInformativeSubTitle: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var btnMapType: UIButton!
    @IBOutlet weak var centerUpView: UIView!
    @IBOutlet weak var drivesProgressView: CircularProgressView!
    @IBOutlet weak var taggedProgressView: CircularProgressView!
    @IBOutlet weak var houzesProgressView: CircularProgressView!
    @IBOutlet weak var centerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var mapTypeView: UIView!
    
    @IBOutlet weak var btnNormalView: UIButton!
    @IBOutlet weak var btnSatelliteView: UIButton!
    @IBOutlet weak var lblToast: UILabel!
    
    @IBOutlet weak var btnUserNameToastBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnUserNameToast: UIButton!
    @IBOutlet weak var btnMinimiseUserToast: UIButton!
    @IBOutlet weak var userToastView: UIView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var leftFilterButton: UIButton!
    
    @IBOutlet weak var leftFilterView: UIView!
    @IBOutlet weak var heightForLeftFilterView: NSLayoutConstraint!
    
    @IBOutlet weak var btnDrag: UIButton!
    @IBOutlet weak var teamDropDown: TextDropDown!
    @IBOutlet weak var mySegmentedControl: UISegmentedControl!
    @IBOutlet weak var lblDurationOrList: UILabel!
    @IBOutlet weak var listDropDown: TextDropDown!
    @IBOutlet weak var btnApply: UIButton!
    @IBOutlet weak var upView: UIView!
    @IBOutlet weak var btnClear: UIButton!
    
    
    var isDriving: Bool = false
    var isInformativeViewShown: Bool = true
    var mapTypeDropdown = DropDown()
    let mapTypeArray = ["NORMAL", "SATELLITE"]
    var duration: TimeInterval!
    var totalDistanceInKM: Double = 0.0
    var oldPolylineArr = [GMSPolyline]()
    var oldPolylineArr2 = [GMSPolyline]()
    
    // end-----------------------------------------------
    //todooffLine
    //------_REALM VARIABLES-------------
    //    var rPolylines: Results<RCoordinate>?
    //    var notificationToken: NotificationToken?
    //--------------------------
    
    
    let marker = GMSMarker()
    //var placesClient: GMSPlacesClient!
    let path = GMSMutablePath()
    var bounds = GMSCoordinateBounds()
    var locationManager = CLLocationManager()
    // let DriveNowButton = UIButton()
    let floatingButton = UIButton()
    var floaty = Floaty()
    var currentLocationMarker: GMSMarker?
    let actionButton = JJFloatingActionButton()
    let geocoder = GMSGeocoder()
    var infoWindow = UIView(frame: CGRect.init(x: 0, y: 0, width: 250, height: 80))
    var tappedMarker = GMSMarker()
    var currentLocation: CLLocation!
    var myLastLocation: CLLocation?
    var nearByUsers : [NearByUser] = []
    
    var userIDArray: [Int] = []
    var viewTranslation = CGPoint(x: 0, y: 0)
    
    //-------start filter variables on drive--------
    var teamMembers: [CurrentUserInfoResponse] = []
    var teamDropDownIDs: [Int] = []
    var listDropDownIDs: [Int] = []
    var allLists: [MList] = []    
    //-------end filter variables on drive--------
    
    //-------start geofencing variables on drive--------
    var geotifications: [Geotification] = []
    var circularOverlays: [GMSCircle] = []
    var allPropertyFromFilteredNewList: [LoadListFilterResponseElement] = []
    var allDrivesFromFilteredPreDrives: [MyDrive] = []
    var allPropertyMarkersFromFilteredList: [GMSMarker] = []
    
    //-------end geofencing variables on drive--------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //show marker //23.777176, 90.399452
        UIApplication.shared.isIdleTimerDisabled = true
        
        //        let grouped = assets.group(by: { $0.coin })
        //        print("grouped.count", grouped.count)
        //
        //        for (index,element) in grouped.enumerated(){
        //
        //            print("index--->",index)
        //            print("element--->",element)
        //            print(element.key)
        //            print(element.value)
        //
        //        }
        if globalAllTags.count > 0{
            print("globalAllTags.count--->",globalAllTags.count)
        }else{
            globalAllTags.removeAll()
            let url : String = UrlManager.baseURL() + UrlManager.apiString() + "/tag/"
            print(url)
            getAllTags(urlString: url)
            self.getAllTags(urlString: url)
        }
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [lblDurationOrList], type: TextType.ListSubTitle.rawValue)
        FontAndColorConfigs.setDynamicButtonsFontSize(buttons: [btnApply], type: TextType.ListTitle.rawValue)
        
        setUpLeftViews()
        configureDataForFilter()
        //self.upView.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(handleDismiss)))
        //btnDrag.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(handleDismiss)))
        //        self.leftFilterView.listDelegate = self
        //        self.leftFilterView.previousDrivesDelegate = self
        //
        SocketIOManager.shared.establishConnection()
        duration = 3    //Play with whatever value you want :]
        drivesProgressView.progressAnimation(duration: duration)
        drivesProgressView.createCircularPath(type: "drives")
        
        houzesProgressView.progressAnimation(duration: duration)
        houzesProgressView.createCircularPath(type: "houzes")
        
        taggedProgressView.progressAnimation(duration: duration)
        taggedProgressView.createCircularPath(type: "tagged")
        
        view.layoutIfNeeded()
        
        SocketIOManager.shared.locationReceiverDelegate =  self
        SocketIOManager.shared.userDisconnectedDelegate =  self
        
        
        locationManager.distanceFilter = 10
        
        //self.addFloatyButton()
        setUpView()
        setUpLowerViewDesign()
        setUpMapTypeDropDown()
        //setup Location Manager
        setUpLocationManager()
        configureViewForTutorialScreen()
        
        mapView.delegate = self
        
        if globalMapType == 0{
            mapView.mapType = .normal
            
        }else{
            mapView.mapType = .satellite
            
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodForDrivingFlag(notification:)), name: Notification.Name("NotificationIdentifier"), object: nil)
        //        NotificationCenter.default.addObserver(self, selector: #selector(self.notifyForPropertyAddedToList(notification:)), name: Notification.Name("AddedToList"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateUserLocationInSocket), name: Notification.Name("socketUserConnectedWithValidToken"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.UpgradeClickedAction(notification:)), name: Notification.Name("UpgradeClicked"), object: nil)
        
        loadAllGeotifications()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // mapView.clear()
        //mapView.animate(toZoom: 17)
        //setup Location Manager
        //setUpLocationManager()
        setUpLocationManager()
        //FontAndColorConfigs.setDynamicButtonsFontSize(buttons: [btnDropdown], type: TextType.ListTitle.rawValue)
        setupNavforDefaultMenu()
        //connect socket
        activeUserIDs.removeAll()
        markerArray.removeAll()
        if globalAllTags.count > 0{
            print("globalAllTags.count--->",globalAllTags.count)
        }else{
            globalAllTags.removeAll()
            let url : String = UrlManager.baseURL() + UrlManager.apiString() + "/tag/"
            print(url)
            getAllTags(urlString: url)
            self.getAllTags(urlString: url)
        }
        
        setUpDrivingDataForPreviousDriving()
        //list
        deleteAllPropertiesFromFilteredOldList()
        //drives
        deleteAllDrivesFromFilteredDrives()
        //controlUiAccordingToFilter
        if isGlobalDriveNowSelected == true{
            //if isFilteredCleared == true{
            if selectedSegmentIndexAfterFilter == 0{
                //list
                
                callAPIForApplyFilterOnList()
            }else{
                //drives
                callAPIForApplyFilterOnPreviuosDrives()
            }
            //}
        }
        else if isGlobalDriveNowSelected == false{
            //list
            deleteAllPropertiesFromFilteredOldList()
            //drives
            deleteAllDrivesFromFilteredDrives()
        }else{
            
        }
        
        //todooffLine
        //        if globalDrivingInfo?.id == nil{
        //
        //            getDataFromRealm()
        //            syncOfflineDataWithStaticVariables()
        //        }else{
        //            setUpDrivingDataForPreviousDriving()
        //        }
        UIApplication.shared.isIdleTimerDisabled = true
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        //mapView.clear()
        UIApplication.shared.isIdleTimerDisabled = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        UIApplication.shared.isIdleTimerDisabled = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        //todooffLine
        //notificationToken?.invalidate()
    }
    
    
    private func setupNavforDefaultMenu() {
        self.navigationBarTranslucentStyle()
        // Update side menu
        sideMenuManager?.instance()?.menu?.isNavbarHiddenOrTransparent = true
        // Re-enable sidemenu
        sideMenuManager?.instance()?.menu?.disabled = false
        sideMenuManager?.instance()?.menu?.allowLeftSwipe = false
        sideMenuManager?.instance()?.menu?.allowRightSwipe = false
        
    }
    
    
    //todooffLine
    //    func getDataFromRealm() {
    //
    //        let realm = RealmPolylineService.shared.realmPolyline
    //        rPolylines = realm.objects(RCoordinate.self)
    //        print(rPolylines?.count ?? 0)
    //
    //        if rPolylines != nil{
    //
    //            for cordinate in rPolylines! {
    //                print("Realm Latitude", cordinate.lat)
    //                print("Realm Longitude", cordinate.long)
    //            }
    //        }
    //
    //        notificationToken = realm.observe { [unowned self] note, realm in
    //            print("notificationToken",realm)
    //            //self.tableView.reloadData()
    //        }
    //
    //    }
    //todooffLine
    //    func syncOfflineDataWithStaticVariables() {
    //        if rPolylines != nil{
    //
    //            let offlinePath = GMSMutablePath()
    //
    //            for polyline in rPolylines!{
    //
    //            }
    //        }
    //    }
    
    func setUpDrivingDataForPreviousDriving() {
        
        if isGlobalDriveNowSelected == false{
            
            mapView.clear()
            if oldPolylineArr.count > 0{
                for polyline in oldPolylineArr{
                    polyline.map = nil
                }
            }
            
            if oldPolylineArr2.count > 0{
                for polyline in oldPolylineArr2{
                    polyline.map = nil
                }
            }
            
            if routeLocationPaths.count > 0{
                
                for path in routeLocationPaths{
                    path.removeAllCoordinates()
                }
            }
            
            if  isGlobalPauseSelected == false{
                
                //isGlobalPauseSelected is false
                self.btnDriveNow.isHidden = false
                self.btnPrivate.isHidden = true
                self.btnPublic.isHidden = true
                setUpLowerViewDesign()
                
            }else{
                //isGlobalPauseSelected is true
                
                //------------------start handle pause/resume polyline draw while paused
                let grouped = routeLocationsWithIndex.group(by: { $0.position })
                print("grouped.count", grouped.count)
                
                for (index,element) in grouped.enumerated(){
                    
                    print("index--->",index)
                    print("element--->",element)
                    print(element.key)
                    print(element.value)
                    let locations = element.value
                    
                    let lPath = GMSMutablePath()
                    
                    for location in locations{
                        
                        let lat = location.latitude
                        let long = location.longitude
                        //let position = location.position
                        lPath.add(CLLocationCoordinate2D(latitude: lat, longitude: long))
                        routeLocationPaths.append(lPath) //add all paths in one collection. delete all when needed
                    }
                    
                    let lPolyline = GMSPolyline(path: lPath) // GMSPolyline(path: path)
                    lPolyline.strokeWidth = 3
                    lPolyline.strokeColor = UIColor.red
                    lPolyline.map = mapView
                    oldPolylineArr2.append(lPolyline)
                    
                }
                //------------------end handle pause/resume polyline draw
                
                
                self.btnPrivate.isHidden = false
                self.btnPublic.isHidden = false
                self.btnDriveNow.isHidden = true
                self.showResumeButton(button: btnPublic)
                self.showFinishButton(button: btnPrivate)
                self.showMapUsingLocation()
                
                self.taggedProgressView.isHidden = true
                self.drivesProgressView.isHidden = true
                self.houzesProgressView.isHidden = true
                
                self.showResumeButton(button: btnPublic)
                self.showFinishButton(button: btnPrivate)
                self.showInfoButton(button: btnInfo)
                //leftFilterButton.isHidden = true
                showLeftFilterButton(button:leftFilterButton)
                
                self.centerViewHeightConstraint.constant = 0
                self.centerUpView.layoutIfNeeded()
                
                if globalListedPropertiesInCurrentDrive.count > 0{
                    noOfTaggedProperty = globalListedPropertiesInCurrentDrive.count
                    putListedPropertyMarkers()
                }else{
                    noOfTaggedProperty = globalListedPropertiesInCurrentDrive.count
                }
            }
        }else{
            //isGlobalDriveNowSelected is true
            
            mapView.clear()
            if oldPolylineArr2.count > 0{
                for polyline in oldPolylineArr2{
                    polyline.map = nil
                }
            }
            
            if routeLocationPaths.count > 0{
                for path in routeLocationPaths{
                    path.removeAllCoordinates()
                }
            }
            //isGlobalDriveNowSelected == true
            //            let path2 = GMSMutablePath()
            //            for location in routeLocations{
            //
            //                let lat = location.coordinate.latitude
            //                let long = location.coordinate.longitude
            //                path2.add(CLLocationCoordinate2D(latitude: lat, longitude: long))
            //            }
            //
            //            let polyline2 = GMSPolyline(path: path2) // GMSPolyline(path: path)
            //            polyline2.strokeWidth = 3
            //            polyline2.strokeColor = UIColor.red
            //            polyline2.map = mapView
            //            oldPolylineArr2.append(polyline2)
            
            //handle pause/resume polyline draw
            //var routeLocationsWithIndex: [LocationWithIndexPosition] = []
            
            //------------------start handle pause/resume polyline draw while driving
            let grouped = routeLocationsWithIndex.group(by: { $0.position })
            print("grouped.count", grouped.count)
            
            for (index,element) in grouped.enumerated(){
                
                print("index--->",index)
                print("element--->",element)
                print(element.key)
                print(element.value)
                let locations = element.value
                
                let lPath = GMSMutablePath()
                
                for location in locations{
                    
                    let lat = location.latitude
                    let long = location.longitude
                    //let position = location.position
                    lPath.add(CLLocationCoordinate2D(latitude: lat, longitude: long))
                    routeLocationPaths.append(lPath) //add all paths in one collection. delete all when needed
                }
                
                let lPolyline = GMSPolyline(path: lPath) // GMSPolyline(path: path)
                lPolyline.strokeWidth = 3
                lPolyline.strokeColor = UIColor.red
                lPolyline.map = mapView
                oldPolylineArr2.append(lPolyline)
                
            }
            //------------------end handle pause/resume polyline draw
            
            if  isGlobalPauseSelected == false{
                
                self.btnDriveNow.isHidden = false
                self.btnPrivate.isHidden = true
                self.btnPublic.isHidden = true
                
            }else{
                
                self.btnPrivate.isHidden = false
                self.btnPublic.isHidden = false
                self.btnDriveNow.isHidden = true
                self.showResumeButton(button: btnPublic)
                self.showFinishButton(button: btnPrivate)
                
                self.taggedProgressView.isHidden = true
                self.drivesProgressView.isHidden = true
                self.houzesProgressView.isHidden = true
                
                self.showResumeButton(button: btnPublic)
                self.showFinishButton(button: btnPrivate)
                self.showInfoButton(button: btnInfo)
                //leftFilterButton.isHidden = true
                showLeftFilterButton(button:leftFilterButton )
                self.centerViewHeightConstraint.constant = 0
                self.centerUpView.layoutIfNeeded()
                
                self.showMapUsingLocation()
                
            }
            
            if globalListedPropertiesInCurrentDrive.count > 0{
                
                noOfTaggedProperty = globalListedPropertiesInCurrentDrive.count
                putListedPropertyMarkers()
            }
        }
    }
    
    //Start: ------------New Design -----------------------------
    func putListedPropertyMarkers() {
        
        infoWindow.removeFromSuperview()
        mapView.selectedMarker = nil
        self.marker.map = nil
        
        for property in globalListedPropertiesInCurrentDrive{
            let marker = GMSMarker()
            let lat = property.latitude
            let long = property.longitude
            marker.position = CLLocationCoordinate2D(latitude: lat!, longitude: long!)
            marker.title = "\((property.id)!)"
            
            let markerImage = UIImage(named: "new_ic_tapped_property")!.withRenderingMode(.alwaysTemplate)
            //creating a marker view
            let markerView = UIImageView(image: markerImage)
            let tags = property.propertyTags
            
            
            if tags != nil{
                if tags?.count == 0{
                    
                    markerView.tintColor = UIColor.black //.systemBlue
                    marker.iconView = markerView
                    marker.icon = markerImage
                    
                }else if tags!.count > 1{
                    
                    markerView.tintColor = .systemGray
                    marker.iconView = markerView
                    marker.icon = markerImage
                    
                }else if tags?.count == 1{
                    let tag = tags?[0]
                    let color = tag?.color?.colorCode
                    print(color ?? "")
                    if color != nil && color != ""{
                        let uiColor =  UIColor(hexString: color!)
                        markerView.tintColor = uiColor
                        marker.iconView = markerView
                        marker.icon = markerImage
                        
                    }else{
                        markerView.tintColor = UIColor.black //.systemBlue
                        marker.iconView = markerView
                        marker.icon = markerImage
                    }
                }else{
                    print("unknown case")
                    markerView.tintColor = UIColor.black //.systemBlue
                    marker.iconView = markerView
                    marker.icon = markerImage
                }
            }else{
                markerView.tintColor = UIColor.black //.systemBlue
                marker.iconView = markerView
                marker.icon = markerImage
            }
            
            marker.map = self.mapView
            
        }
    }
    
    func setUpMapTypeDropDown( ) {
        
        mapTypeDropdown.anchorView = btnMapType
        
        mapTypeDropdown.direction = .bottom
        mapTypeDropdown.bottomOffset = CGPoint(x: 0, y: 44)
        mapTypeDropdown.dataSource = self.mapTypeArray
        mapTypeDropdown.width = self.view.frame.width/2.5
        mapTypeDropdown.textFont = ProNovaR16
        
        mapTypeDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            if index == 0{
                self.mapView.mapType = .normal
                globalMapType = 0
            }else{
                self.mapView.mapType = .satellite
                globalMapType = 1
            }
        }
    }
    
    func updateUserNameToastPosition() {
        if self.informativeView.isHidden == false{
            self.btnUserNameToastBottomConstraint.constant = 100
            self.btnUserNameToast.layoutIfNeeded()
            
        }else{
            self.btnUserNameToastBottomConstraint.constant = 0
            self.btnUserNameToast.layoutIfNeeded()
            
        }
    }
    
    func setUpLowerViewDesign() {
        
        animateTiles()
        
        self.lblInformationHeader.textColor = listTitleColor
        self.lblInformativeSubTitle.textColor = self.listSubTitleColor
        
        pageControl.currentPage = 0
        //        if pageControl.currentPage == 0{
        //            lblInformativeSubTitle.text = "Properties Added"
        //            lblInformationHeader.text = "4391"
        //        }
        
        if isGlobalDriveNowSelected == false{
            self.informativeView.isHidden = true
            self.updateUserNameToastPosition()
            
            //hide circleview
            //            self.taggedProgressView.isHidden = false
            //            self.drivesProgressView.isHidden = false
            //            self.houzesProgressView.isHidden = false
            //
            //            centerViewHeightConstraint.isActive = false
            //            centerViewHeightConstraint = NSLayoutConstraint(item: self.mapView!, attribute: .height, relatedBy: .equal, toItem: self.mapView, attribute: .height, multiplier: CGFloat(1), constant: 0)
            //            centerViewHeightConstraint!.isActive = true
            self.taggedProgressView.isHidden = true
            self.drivesProgressView.isHidden = true
            self.houzesProgressView.isHidden = true
            self.centerViewHeightConstraint.constant = 0
            self.centerUpView.layoutIfNeeded()
            
            btnDriveNow.isHidden = false
            btnPublic.isHidden = true
            btnPrivate.isHidden = true
            btnInfo.isHidden = true
            leftFilterButton.isHidden = true
            
            self.showDriveNowButton(button: btnDriveNow)
            self.showInfoButton(button: btnInfo)
            showLeftFilterButton(button:leftFilterButton )
        }else{
            self.informativeView.isHidden = false
            self.updateUserNameToastPosition()
            
            self.taggedProgressView.isHidden = true
            self.drivesProgressView.isHidden = true
            self.houzesProgressView.isHidden = true
            
            self.centerViewHeightConstraint.constant = 0
            self.centerUpView.layoutIfNeeded()
            
            self.showStopButton(button: btnDriveNow)
            self.showInfoButton(button: btnInfo)
            btnInfo.isHidden = true
            leftFilterButton.isHidden = false
            showLeftFilterButton(button:leftFilterButton )
        }
        
        let left = UISwipeGestureRecognizer(target : self, action : #selector(HomeViewController.leftSwipe))
        left.direction = .left
        self.informativeView.addGestureRecognizer(left)
        
        let right = UISwipeGestureRecognizer(target : self, action : #selector(HomeViewController.rightSwipe))
        right.direction = .right
        self.informativeView.addGestureRecognizer(right)
        
    }
    //End: ------------New Design -----------------------------
    
    
    func setUpUiForPrivateDrive() {
        self.btnPrivate.isHidden = true
        self.btnPublic.isHidden = true
        self.btnDriveNow.isHidden = false
        self.showStopButton(button: btnDriveNow)
        self.informativeView.isHidden = true
        
        self.updateUserNameToastPosition()
        
        self.btnInfo.isHidden = false
        
        leftFilterButton.isHidden = false
        //showLeftFilterButton(button:leftFilterButton )
        
        self.isInformativeViewShown = true
        isGlobalDriveNowSelected = true
        isGlobalPauseSelected = false
        //todooffLine
        //        AppConfigs.saveDrivingPausedStatus(status: isGlobalPauseSelected)
        //        print("isGlobalPauseSelected", isGlobalPauseSelected)
        //        print("AppConfigs.getSavedDrivingPausedStatus()", AppConfigs.getSavedDrivingPausedStatus())
        //
        self.taggedProgressView.isHidden = true
        self.drivesProgressView.isHidden = true
        self.houzesProgressView.isHidden = true
        self.centerViewHeightConstraint.constant = 0
        self.centerUpView.layoutIfNeeded()
        
        self.startButtonTapped()
        SocketIOManager.shared.updateCurrentLocation(myLocation: self.locationManager.location!, driveType: globalDriveType)
        
    }
    
    
    func setUpUiForPublicDrive() {
        self.btnPrivate.isHidden = true
        self.btnPublic.isHidden = true
        
        self.btnDriveNow.isHidden = false
        self.showStopButton(button: btnDriveNow)
        self.showInfoButton(button: btnInfo)
        self.informativeView.isHidden = true
        self.updateUserNameToastPosition()
        
        self.btnInfo.isHidden = false
        self.isInformativeViewShown = true
        
        leftFilterButton.isHidden = false
        showLeftFilterButton(button:leftFilterButton )
        //do actions for public
        
        isGlobalDriveNowSelected = true
        isGlobalPauseSelected = false
        //todooffLine
        //        AppConfigs.saveDrivingPausedStatus(status: isGlobalPauseSelected)
        //        print("isGlobalPauseSelected", isGlobalPauseSelected)
        //        print("AppConfigs.getSavedDrivingPausedStatus()", AppConfigs.getSavedDrivingPausedStatus())
        
        routeLocations.removeAll()
        routeLocationsWithIndex.removeAll()
        print(self.locationManager.location?.coordinate.latitude ?? 0.0)
        print(self.locationManager.location?.coordinate.longitude ?? 0.0)
        
        self.taggedProgressView.isHidden = true
        self.drivesProgressView.isHidden = true
        self.houzesProgressView.isHidden = true
        
        self.centerViewHeightConstraint.constant = 0
        self.centerUpView.layoutIfNeeded()
        
        
        self.startButtonTapped()
        SocketIOManager.shared.updateCurrentLocation(myLocation: self.locationManager.location!, driveType: globalDriveType)///myLastLocationGlobal
    }
    
    func sendStartDrivingLocation(location:CLLocation) {
        
        totalCoveredDistance = 0
        routeLocations.removeAll()
        routeLocationsWithIndex.removeAll()
        //delete polylines
        
        if self.oldPolylineArr.count > 0{
            print( self.oldPolylineArr.count)
            for  polyline in self.oldPolylineArr {
                polyline.map = nil
            }
            self.oldPolylineArr.removeAll()
            print( self.oldPolylineArr.count)
        }
        
        //current date time
        let now = Date()
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSX"
        let dateString = formatter.string(from: now)
        print(dateString)
        
        let params = [
            "start_point_latitude" : location.coordinate.latitude,
            "start_point_longitude" : location.coordinate.longitude,
            "start_time" : dateString] as [String : Any]
        print(params)
        
        let url = UrlManager.baseURL() + UrlManager.apiString() + "/history/start-driving/"
        print(url)
        
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        
        Alamofire.request(url, method: .post, parameters: params, encoding:  JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                //print("success",data)
                let statusCode = response.response?.statusCode
                
                if statusCode == 200{
                    let responseOfStartDriving = try? JSONDecoder().decode(DrivingResponse.self, from: response.data!)
                    drivingDetails = responseOfStartDriving?.data
                    globalDrivingInfo = responseOfStartDriving?.data
                    //print(responseOfStartDriving?.status!)
                    
                    if responseOfStartDriving?.status == true{
                        
                        if responseOfStartDriving?.data == nil{
                            print((responseOfStartDriving?.message)!)
                        }else{
                            
                            self.showToastLabel(label: self.lblToast, message:(responseOfStartDriving?.message ?? "Start Driving. Click & hold any property to add to list") )
                            
                            //save data for storage usage checking
                            //todooffLine
                            //                            if globalDrivingInfo != nil{
                            //
                            //                                AppConfigs.saveHistoryID(id: (globalDrivingInfo?.id)!)
                            //                                print("(AppConfigs.getSavedHistoryID",AppConfigs.getSavedHistoryID())
                            //                            }
                            //
                            //                            AppConfigs.saveDrivingType(token: globalDriveType)
                            //                            print("AppConfigs.getSavedDrivingType",AppConfigs.getSavedDrivingType())
                            //--------------------
                            currentPauseIndex =  1 // to maintain pause/resume status of the drive, set to 1
                            print(currentPauseIndex)
                            
                            
                            if globalDriveType == "PUBLIC"{
                                
                                self.setUpUiForPublicDrive()
                            }
                            else if globalDriveType == "PRIVATE"{
                                self.setUpUiForPrivateDrive()
                            }else{
                                print("globalDriveType === nil")
                            }
                        }
                    }else{
                        AppConfigs.showSnacbar(message: (responseOfStartDriving?.message)!, textColor: .red)
                    }
                    
                    DispatchQueue.main.async {
                        
                    }
                }
                    
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
                
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                //AppConfigs.showSnacbar(message: error.localizedDescription, textColor: .red)
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    //
    func handleFinishDriving() {
        
        self.mapView.mapType = .normal
        
        
        var previousLocation: CLLocation? = nil
        var currentLocation: CLLocation? = nil
        
        path.removeAllCoordinates() //delete all path coordinate
        globalListedPropertiesInCurrentDrive.removeAll()
        
        
        //------------------start handle pause/resume polyline draw while driving
        //        for location in routeLocations{
        //            path.add(CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude))
        //            bounds = bounds.includingCoordinate(CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude))
        //        }
        //
        //        let polyline = GMSPolyline(path: path)
        //        polyline.strokeWidth = 3
        //        polyline.strokeColor = UIColor.red
        //        polyline.map = self.mapView
        
        for location in routeLocations{
            path.add(CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude))
        }
        
        for location in routeLocationsWithIndex{
            bounds = bounds.includingCoordinate(CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude))
        }
        //------------------end handle pause/resume polyline draw while driving
        
        //let bounds = GMSCoordinateBounds(path: path)
        
        self.mapView.animate(toZoom: 10)
        self.mapView!.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 50.0))
        
        //------------------start handle pause/resume polyline draw while driving
        //        for (index, element) in routeLocations.enumerated() {
        //            print("Item \(index): \(element)")
        //
        //            currentLocation = routeLocations[index]
        //
        //            if index == 0 {
        //                //previousLocation = routeLocations[index]
        //            }else{
        //                previousLocation = routeLocations[index - 1]
        //                let distanceInMeters = previousLocation!.distance(from: currentLocation!)
        //                totalCoveredDistance = totalCoveredDistance + distanceInMeters
        //            }
        //        }
        
        let grouped = routeLocationsWithIndex.group(by: { $0.position })
        print("grouped.count", grouped.count)
        
        for (index,element) in grouped.enumerated(){
            
            print("index--->",index)
            print("element--->",element)
            print(element.key)
            print(element.value)
            let locations = element.value
            
            for (lindex, lelement) in locations.enumerated(){
                
                let lat = lelement.latitude
                let long = lelement.longitude
                
                currentLocation = CLLocation(latitude: lat, longitude: long)
                if lindex == 0 {
                    //previousLocation = routeLocations[index]
                }else{
                    let loc = locations[lindex - 1]
                    previousLocation = CLLocation(latitude: loc.latitude, longitude: loc.longitude)
                    let distanceInMeters = previousLocation!.distance(from: currentLocation!)
                    totalCoveredDistance = totalCoveredDistance + distanceInMeters
                }
            }
        }
        //------------------start handle pause/resume polyline draw while driving
        print("totalCoveredDistance-->", totalCoveredDistance)
        totalDistanceInKM = totalCoveredDistance/1000
        print("totalDistanceInKM----",totalDistanceInKM)
        totalCoveredDistance = totalDistanceInKM
        self.showToastLabel(label: self.lblToast, message: "Driving Finished")
        
    }
    
    func sendDrivngDataToServer() {
        
        let url = UrlManager.baseURL() + UrlManager.apiString() + "/history/\(String(describing: (drivingDetails?.id)!))/end-driving/"
        print(url)
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        
        //current date time
        let now = Date()
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSX"
        let dateString = formatter.string(from: now)
        print(dateString)
        
        
        var polyLines: [Polyline] = []
        //var data: [Data] = []
        //var results: [String] = []
        //var jsonArray : [MyPolyline] = []
        var decodedPolylines: Array<Polyline> = []
        
        polyLines.removeAll()
        decodedPolylines.removeAll()
        
        //        for location in routeLocations{
        //
        //            let polyline : Polyline = Polyline(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        //            polyLines.append(polyline)
        
        //            let jsonserializer = JSONSerializer.toJson(polyline)
        //            print(jsonserializer)
        //            results.append(jsonserializer)
        
        //        }
        
        //        let jsonserializerString = JSONSerializer.toJson(results)
        //        print("jsonserializerString", jsonserializerString)
        
        //        let jsonserializer = JSONSerializer.toJson(polyLines)
        //        print("jsonserializer", jsonserializer)
        //
        //        print(polyLines.count);
        //        print(polyLines)
        //
        let jsonSerializerWithPosition = JSONSerializer.toJson(routeLocationsWithIndex)
        print("jsonSerializerWithPosition", jsonSerializerWithPosition)
        print(jsonSerializerWithPosition)
        
        let params = [
            "end_point_latitude" : routeLocations.last?.coordinate.latitude ?? 0.00,
            "end_point_longitude" : routeLocations.last?.coordinate.longitude ?? 0.00,
            "end_time" : dateString,
            "polylines" : jsonSerializerWithPosition,//polyLines,//data,//jsonArray, //data, //polyLines,
            "length" : totalCoveredDistance/1000
            ] as Parameters
        print(params)
        print("totalDistanceInKM In Params----",totalCoveredDistance/1000)

        let renderer = UIGraphicsImageRenderer(size: self.mapView.frame.size)
        let image = renderer.image(actions: { context in
            self.mapView.layer.render(in: context.cgContext)
        })
        
        //Save it to the camera roll
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        
        let imgData = image.jpegData(compressionQuality: 0.7)!
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
            
            self.requestWith(endUrl: url, imageData: imgData, parameters: params, onCompletion: { (response) in
                
                DispatchQueue.main.async {
                    
                    print("response?.data---->", response?.data)
                    if response != nil{
                        globalDrivingInfo = response?.data
                        drivingDetails = globalDrivingInfo
                    }
                    self.removeSpinner()
                    
                    if isGlobalDriveNowSelected == false{
                        self.path.removeAllCoordinates()//delete all path coordinate
                        
                        if routeLocationPaths.count > 0{
                            
                            for path in routeLocationPaths{
                                path.removeAllCoordinates()
                            }
                        }
                    }
                    
                    if isGlobalDriveNowSelected == false{
                        if self.oldPolylineArr.count > 0{
                            for polyline in self.oldPolylineArr{
                                polyline.map = nil
                            }
                        }
                    }
                    
                    currentPauseIndex = 0
                    
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                    let nextViewController = storyBoard.instantiateViewController(withIdentifier: "RouteViewController") as! RouteViewController
                    nextViewController.modalPresentationStyle = .fullScreen
                    self.navigationController?.pushViewController(nextViewController, animated: true)
                }
                
            }) { (error) in
                print(error?.localizedDescription)
                self.removeSpinner()
                AppConfigs.showSnacbar(message: error!.localizedDescription, textColor: .red)
            }
        })
    }
    
    func requestWith(endUrl: String, imageData: Data?, parameters: [String : Any], onCompletion: ((DrivingResponse?) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
        
        self.showSpinner(onView: self.view)
        let url = endUrl /* your API url */
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken(),
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            if let data = imageData{
                multipartFormData.append(data, withName: "image", fileName: "file.png", mimeType: "image/png")
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print("Succesfully uploaded")
                    print(response)
                    globalSelectedList = nil
                    
                    let data = response.data
                    print(data)
                    
                    let responseOfEndDriving = try? JSONDecoder().decode(DrivingResponse.self, from: response.data!)
                    if responseOfEndDriving != nil{
                        print(responseOfEndDriving!)
                        onCompletion?(responseOfEndDriving)
                    }else{
                        onCompletion?(nil)
                    }
                    
                    if let err = response.error{
                        onError?(err)
                        return
                    }
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                onError?(error)
            }
        }
    }
    
    func showMarker(position: CLLocationCoordinate2D){
        marker.isDraggable = false
        marker.position = position
        marker.title = "Palo Alto"
        marker.snippet = "San Francisco"
        //marker.icon = UIImage(named: "broadcast")
        marker.map = mapView
    }
    
    func showInfoWindowPopup(title: String, snippet:String, position: CLLocationCoordinate2D) {
        
    }
    
    func showMarkerWithTitleSnippet(title: String, snippet:String, position: CLLocationCoordinate2D){
        
        infoWindow.removeFromSuperview()
        print("You tapped at \(position.latitude), \(position.longitude)")
        infoWindow.removeFromSuperview()
        infoWindow = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width - 100, height: 160))
        infoWindow.center = CGPoint(x: self.mapView.center.x + 5, y: self.mapView.center.y - 125)
        
        //showTriangularTipAtCenterBottom
        self.showTriangularTipAtCenterBottom(view: infoWindow, parentView: self.view)
        
        infoWindow.layer.cornerRadius = 10
        let font = ProNovaSB11// UIFont(name: "ProximaNova-SemiBold", size: 1.0)!
        // let font2 = UIFont(name: "Helvetica", size: 20.0)!
        
        let imgView =  UIImageView(frame: CGRect.init(x: 16, y: 20, width: 25, height: 25))
        imgView.image = UIImage(named: "Shape")
        imgView.backgroundColor = .white
        imgView.tintColor = .red
        imgView.contentMode = .center
        infoWindow.addSubview(imgView)
        
        
        let lbl1 = UILabel(frame: CGRect.init(x: 48, y: 16, width: infoWindow.frame.size.width - 120, height: AppConfigs.heightForView(text: marker.title ?? "", font: font, width: infoWindow.frame.size.width)))
        lbl1.text = marker.title
        lbl1.textColor = UIColor(red:0.2, green:0.3, blue:0.41, alpha:1)//.red// UIColor(red:0.2, green:0.3, blue:0.41, alpha:1)
        lbl1.textAlignment = .left
        lbl1.numberOfLines = 0
        lbl1.lineBreakMode = .byWordWrapping
        lbl1.sizeToFit()
        infoWindow.addSubview(lbl1)
        
        let buttonAddToList = UIButton(frame: CGRect.init(x: infoWindow.frame.size.width/2 - 30, y: lbl1.frame.origin.y + lbl1.frame.size.height + 16, width: 40, height: 40))
        buttonAddToList.backgroundColor = UIColor(red:0.07, green:0.82, blue:0.41, alpha:1)
        //buttonAddToList.setTitle("ADD", for: .normal)
        buttonAddToList.setImage(UIImage(named: "ic_like"), for: .normal)
        //buttonAddToList.titleLabel?.textColor = .white
        buttonAddToList.layer.cornerRadius = 20
        buttonAddToList.tintColor = .white
        //buttonAddToList.centerAndImage(withSpacing: 10.0)
        buttonAddToList.addTarget(self, action: #selector(viewDetailsButtonAction), for: .touchUpInside)
        //buttonAddToList.addTarget(self, action: #selector(addToListButtonAction), for: .touchUpInside)
        infoWindow.addSubview(buttonAddToList)
        
        self.view.addSubview(infoWindow)
        
    }
    
    @IBAction func NotificationClicked(_ sender: Any) {
        
        let storyBoard = UIStoryboard(name: "MyTasks", bundle: Bundle.main)
        guard let MyTasksViewController = storyBoard.instantiateViewController(withIdentifier: "MyTasksViewController") as? MyTasksViewController  else
        { return  }
        present(MyTasksViewController, animated: true, completion: nil)
    }
    
    @IBAction func toggleSideMenu(_ sender: Any) {
        
        sideMenuManager?.toggleSideMenuView()
        
    }
    
    @IBAction func btnUserNameToastClicked(_ sender: Any) {
        
        self.userToastView.pushNameTransition(2.0)
        
        self.btnMinimiseUserToast.isHidden = true
        
        self.btnUserNameToast.isHidden = true
        
        self.btnMinimiseUserToast.isHidden = true
        
        self.userToastView.isHidden = true
        
        
        self.userImageView.isHidden = true
    }
    
    func startMonitoringLocation() {
        if CLLocationManager.locationServicesEnabled() {
            locationManager.requestWhenInUseAuthorization()
            locationManager.requestAlwaysAuthorization()
            locationManager.startMonitoringSignificantLocationChanges()
            locationManager.startUpdatingLocation()
        }
    }
    
    func startReceivingSignificantLocationChanges() {
        _ = CLLocationManager.authorizationStatus()
        
        if !CLLocationManager.significantLocationChangeMonitoringAvailable() {
            // The service is not available.
            return
        }
        locationManager.delegate = self
        locationManager.startMonitoringSignificantLocationChanges()
    }
    
    func stopMonitoringLocation() {
        locationManager.stopMonitoringSignificantLocationChanges()
        locationManager.stopUpdatingLocation()
    }
    
    func addCurrentLocationMarker() {
        currentLocationMarker?.map = nil
        currentLocationMarker = nil
        var marker = GMSMarker()
        if let location = locationManager.location {
            
            marker = GMSMarker(position: location.coordinate)
            marker.icon = UIImage(named: "ic_car")
            marker.map = mapView
            marker.rotation = locationManager.location?.course ?? 0
            self.locationManager.startUpdatingLocation()
        }
    }
    
    func zoomToCoordinates(_ coordinates: CLLocationCoordinate2D) {
        let camera = GMSCameraPosition.camera(withTarget: CLLocationCoordinate2D(latitude: coordinates.latitude, longitude: coordinates.longitude), zoom: 17, bearing: 0, viewingAngle: 0)
        mapView.camera = camera
        mapView.animate(to: camera)
    }
    
    
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) -> String {
        
        var addressString : String = ""
        
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(center) { (response, error) in
            
            if error != nil{
                print("reverse geodcode fail: \(error!.localizedDescription)")
            }else{
                if let places = response?.results(){
                    if let place = places.first{
                        print("Place-->", place)
                        if let lines = place.lines{
                            print("GEOCODE: Formatted Address: \(lines)")
                        }
                    }else{
                        print("GEOCODE: nil first in places")
                    }
                }else{
                    print("GEOCODE: nil in places")
                }
            }
        }
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                
                let pm = placemarks! as [CLPlacemark]
                if pm.count > 0 {
                    
                    let pm = placemarks![0]
                    print("name---",pm.name ?? "")
                    print("administrativeArea---",pm.administrativeArea ?? "")
                    print("subAdministrativeArea---",pm.subAdministrativeArea ?? "")
                    print("country---", pm.country ?? "")
                    print("locality---",pm.locality ?? "")
                    print("subLocality---",pm.subLocality ?? "")
                    print("thoroughfare---",pm.thoroughfare ?? "")
                    print("postalCode---",pm.postalCode ?? "")
                    print("subThoroughfare---",pm.subThoroughfare ?? "")
                    print("areasOfInterest---",pm.areasOfInterest ?? "")
                    if pm.name != nil {
                        addressString = addressString + pm.name! + ", "
                    }
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    print(addressString)
                    
                    globalSelectedProperty.propertyAddress = addressString
                    globalSelectedProperty.latitude = lat
                    globalSelectedProperty.longitude = lon
                }
        })
        
        return addressString
    }
    
    
    @IBAction func hideLeftView(_ sender: Any) {
        self.leftFilterView.isHidden = true
    }
    
    @IBAction func applyButtonClicked(_ sender: Any) {
        //self.removeSpinner()
        //isFilteredCleared = false
        previouslySelected = true
        if selectedSegmentIndexAfterFilter == 0 {
            
            //list selected from segment
            if selectedListID != 0 && selectedTeamMemberID != 0 {
                print("applyButtonClicked", selectedListID)
                print("applyButtonClicked", selectedTeamMemberID)
                self.leftFilterView.isHidden = true
                createNewRegion()
                callAPIForApplyFilterOnList()
                
            }else{
                AppConfigs.showSnacbar(message: "Please Select all required filter options", textColor: .red)
            }
        }else{
            //previos drive selected from segment
            if selectedListID != 0 && selectedTeamMemberID != 0 {
                print("applyButtonClicked", selectedListID)
                print("applyButtonClicked", selectedTeamMemberID)
                self.leftFilterView.isHidden = true
                createNewRegion()
                callAPIForApplyFilterOnPreviuosDrives()
                
            }else{
                AppConfigs.showSnacbar(message: "Please Select all required filter options", textColor: .red)
            }
        }
        
    }
    
    @IBAction func clearButtonClicked(_ sender: Any) {
        //isFilteredCleared = true
        handleClearFilter()
        //        self.teamDropDown.text = "Choose Team"
        //        self.mySegmentedControl.selectedSegmentIndex = 0
        //        selectedSegmentIndexAfterFilter = 0
        //        self.lblDurationOrList.text = "Choose List"
        //        self.listDropDown.text = "Choose List"
        //
        //        deleteAllPropertiesFromFilteredOldList()
        //        deleteAllDrivesFromFilteredDrives()
        //        selectedListID = 0
        //        selectedTeamMemberID = 0
    }
    
    @IBAction func leftFilterButtonClicked(_ sender: Any) {
        
        if isGlobalDriveNowSelected == true{
            if self.leftFilterView.isHidden == true{
                
                self.informativeView.isHidden = true
                self.updateUserNameToastPosition()
                self.isInformativeViewShown = false
                self.btnInfo.isHidden = false
                
                CATransaction.begin()
                
                let transition = CATransition()
                transition.timingFunction = CAMediaTimingFunction.init(name: .easeInEaseOut)
                transition.type = .push
                transition.subtype = .fromTop
                
                self.leftFilterView.layer.add(transition, forKey: kCATransition)
                CATransaction.commit()
                
                self.leftFilterView.isHidden = false
                self.leftFilterView.roundCorners(corners: [.topLeft, .topRight], radius: 16.0)
                
                if AppConfigs.getCurrentUserInfo().isAdmin == false{
                    self.teamDropDown.optionArray.removeAll()
                    self.teamDropDownIDs.removeAll()
                    let id = AppConfigs.getCurrentUserInfo().id
                    self.teamDropDown.optionArray = ["Myself"]
                    self.teamDropDown.text = "Myself"
                    self.teamDropDownIDs = [id!]
                    selectedTeamMemberID = id!
                    
                    let url: String = UrlManager.baseURL() + UrlManager.apiString() + "/list/load-list/user/\(selectedTeamMemberID)/"
                    self.getLoadListData(urlString: url)
                    
                }else{
                    
//                    if Network.reachability.status == .unreachable{
//                        AppConfigs.updateUserInterface(view: view)
//                    }else{
                        //allTeamMembers.removeAll()
                        users.removeAll()
                        // unregisteredUsers.removeAll()
                        
                        let url : String = UrlManager.getTeamListURL()
                        
                        print(url)
                        self.showSpinner(onView: self.teamDropDown)
                        getAllTeamMembers(urlString: url)
//                    }
                }
            }else{
                self.leftFilterView.isHidden = true
                self.leftFilterView.roundCorners(corners: [.topLeft, .topRight], radius: 16.0)
            }
        }else{
            
        }
        //        if isGlobalDriveNowSelected == true{
        //
        //            if #available(iOS 13.0, *) {
        //                let vc = self.storyboard?.instantiateViewController(identifier: "HomeFilterViewController") as! HomeFilterViewController
        //                vc.modalPresentationStyle = .overCurrentContext
        ////                vc.transitioningDelegate = self
        ////                vc.view.backgroundColor = .opaqueSeparator
        //                self.present(vc, animated: true, completion: nil)
        //
        //            } else {
        //                // Fallback on earlier versions
        //                let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeFilterViewController") as! HomeFilterViewController
        //                vc.modalPresentationStyle = .overCurrentContext
        ////                vc.modalPresentationStyle = .custom
        ////                vc.transitioningDelegate = self
        //                self.present(vc, animated: true, completion: nil)
        //
        //            }
        //
        //           // self.leftFilterView.isHidden  = !self.leftFilterView.isHidden
        ////            if self.leftFilterView.isHidden  == true{
        ////                self.leftFilterView.isHidden  = false
        ////                self.heightForLeftFilterView.constant = self.view.frame.height * 0.6
        ////                self.leftFilterView.layoutIfNeeded()
        ////                self.leftFilterView.configureView()
        ////
        ////            }else{
        ////                self.leftFilterView.isHidden  = true
        ////                self.heightForLeftFilterView.constant = 0
        ////                self.leftFilterView.layoutIfNeeded()
        ////            }
        //        }else{
        ////           if #available(iOS 13.0, *) {
        ////                let vc = self.storyboard?.instantiateViewController(identifier: "HomeFilterViewController") as! HomeFilterViewController
        ////                self.present(vc, animated: true, completion: nil)
        ////
        ////            } else {
        ////                // Fallback on earlier versions
        ////                let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeFilterViewController") as! HomeFilterViewController
        ////                self.present(vc, animated: true, completion: nil)
        ////
        ////            }
        //        }
        //
    }
    
}

extension HomeViewController: LoadListButtonDelegate, PreviousDriveButtonDelegate{
    func handleLoadListAction(_ bottomView: BottomAudioPopUpView) {
        
        print("Load List Clicked")
    }
    
    func handlepreviousDrivesAction(_ bottomView: BottomAudioPopUpView) {
        print("Previous drive Clicked")
    }
    
    
    
}

extension HomeViewController: CLLocationManagerDelegate{
    //Location Manager delegates
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("location manager error -> \(error.localizedDescription)")
        AppConfigs.showSnacbar(message: "Please enable location permission", textColor: .red)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            locationManager.requestAlwaysAuthorization()
        case .restricted:
            break
        case .denied:
            stopMonitoringLocation()
            break
        default:
            //addCurrentLocationMarker()
            startMonitoringLocation()
            break
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        //Finally stop updating location otherwise it will come again and again in this delegate
        guard let lastLocation = locations.last  else { return }
        //self.zoomToCoordinates(lastLocation.coordinate)
        self.myLastLocation = lastLocation
        myLastLocationGlobal = lastLocation
        
        currentLocationMarker?.position = lastLocation.coordinate
        currentLocationMarker?.rotation = lastLocation.course
        
        if isGlobalDriveNowSelected == true{
            
            self.cameraMoveToLocation(toLocation: CLLocationCoordinate2D(latitude: lastLocation.coordinate.latitude, longitude: lastLocation.coordinate.longitude))
            UIApplication.shared.isIdleTimerDisabled = true
            
            var oldLocation: CLLocation? = nil
            //globalDriveType = "PUBLIC"
            print(globalDriveType)
            
            if self.myLastLocation != nil{
                SocketIOManager.shared.updateCurrentLocation(myLocation: self.myLastLocation!, driveType:globalDriveType )
            }
            
            //create realm obj and save data
            //todooffLine
            //            let newRCordinate = RCoordinate(lat: lastLocation.coordinate.latitude, long: lastLocation.coordinate.longitude)
            //            RealmPolylineService.shared.create(newRCordinate)
            
            
            if let firstLocation = routeLocations.last{
                print(firstLocation.coordinate.latitude, firstLocation.coordinate.longitude)
                oldLocation = firstLocation
                //previousLocation = oldLocation
            }
            
            if routeLocations.count < 2 {
                ///previousLocation = routeLocations.last
                routeLocations.append(lastLocation)
                
                let routeData = LocationWithIndexPosition()
                routeData.latitude = lastLocation.coordinate.latitude
                routeData.longitude = lastLocation.coordinate.longitude
                routeData.position =  currentPauseIndex
                
                routeLocationsWithIndex.append(routeData)
                
            }else{
                path.add(CLLocationCoordinate2D(latitude: (oldLocation?.coordinate.latitude)!, longitude: (oldLocation?.coordinate.longitude)!))
                path.add(CLLocationCoordinate2D(latitude: (lastLocation.coordinate.latitude), longitude: (lastLocation.coordinate.longitude)))
                
                let distanceInMeters = oldLocation!.distance(from: lastLocation)
                totalCoveredDistance = totalCoveredDistance + distanceInMeters
                
                routeLocations.append(lastLocation)
                
                let routeData = LocationWithIndexPosition()
                routeData.latitude = lastLocation.coordinate.latitude
                routeData.longitude = lastLocation.coordinate.longitude
                routeData.position =  currentPauseIndex
                
                routeLocationsWithIndex.append(routeData)
                
            }
            print("totalCoveredDistance-->", totalCoveredDistance)
            totalMiles = totalCoveredDistance
            
            let polyline = GMSPolyline(path: path) // GMSPolyline(path: path)
            polyline.strokeWidth = 3
            polyline.strokeColor = UIColor.red
            bounds = bounds.includingPath(path)
            polyline.map = mapView
            oldPolylineArr.append(polyline)
            
        }else{
            UIApplication.shared.isIdleTimerDisabled = false
            
        }
        
        if isGlobalDriveNowSelected == true {
            
            counterForProjection = counterForProjection + 1
            if counterForProjection > 15{
                
                let point:CGPoint = mapView.projection.point(for: lastLocation.coordinate)
                let camera:GMSCameraUpdate = GMSCameraUpdate.setTarget(mapView.projection.coordinate(for: point))
                mapView.animate(with: camera)
            }else{
                let camera = GMSCameraPosition.camera(withTarget: CLLocationCoordinate2D(latitude: myLastLocationGlobal!.coordinate.latitude, longitude: myLastLocationGlobal!.coordinate.longitude), zoom: 17, bearing: 0, viewingAngle: 0)
                
                mapView.animate(to: camera)
                CATransaction.begin()
                CATransaction.setAnimationDuration(3.0)
                mapView.animate(toLocation: CLLocationCoordinate2D(latitude: myLastLocationGlobal!.coordinate.latitude, longitude: myLastLocationGlobal!.coordinate.longitude))
                mapView.animate(toZoom: 17)
                CATransaction.commit()
            }
        } else {
            //when not driving stay to current location
            counterForProjection = 0
            let camera = GMSCameraPosition.camera(withTarget: CLLocationCoordinate2D(latitude: lastLocation.coordinate.latitude, longitude: lastLocation.coordinate.longitude), zoom: 17, bearing: 0, viewingAngle: 0)
            mapView.animate(to: camera)
            CATransaction.begin()
            CATransaction.setAnimationDuration(3.0)
            mapView.animate(toLocation: CLLocationCoordinate2D(latitude: lastLocation.coordinate.latitude, longitude: lastLocation.coordinate.longitude))
            mapView.animate(toZoom: 17)
            CATransaction.commit()
        }
    }
    
    func cameraMoveToLocation(toLocation: CLLocationCoordinate2D?) {
        
        if toLocation != nil {
            let camera = GMSCameraPosition.camera(withTarget: toLocation!, zoom: 17, bearing: 0, viewingAngle: 0)
            mapView.animate(to: camera)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        print( "isRegionExist:---->", isRegionExist)
        
        if region is CLCircularRegion {
            //handleEvent(for: region)
        }
        print("didEnterRegion: ", region.identifier)
        
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        print( "isRegionExist:---->", isRegionExist)
        if region is CLCircularRegion {
            //handleEvent(for: region)
            //stopMonitoring(geotification: <#T##Geotification#>)
        }
        print("didExitRegion: ", region.identifier)
        
    }
    
    func locationManager(_ manager: CLLocationManager, didDetermineState state: CLRegionState, for region: CLRegion) {
        
        let defaults: UserDefaults = UserDefaults.standard
        switch state {
        case .inside:
            defaults.set(true, forKey: "atHome")
            print("INSIDE REGION")
        case .outside:
            defaults.set(false, forKey: "atHome")
            print("OUTSIDE REGION")
            
            //list
            deleteAllPropertiesFromFilteredOldList()
            //drives
            deleteAllDrivesFromFilteredDrives()
            if isGlobalDriveNowSelected == true{
                if selectedListID != 0 && selectedTeamMemberID != 0 {
                    
                    print("applyButtonClicked", selectedListID)
                    print("applyButtonClicked",selectedTeamMemberID)
                    self.leftFilterView.isHidden = true
                    createNewRegion()
                    
                    if selectedSegmentIndexAfterFilter == 0{
                        callAPIForApplyFilterOnList()
                    }else{
                        callAPIForApplyFilterOnPreviuosDrives()
                    }
                }else{
                    
                }
            }else if isGlobalDriveNowSelected == false{
                
                if selectedSegmentIndexAfterFilter == 0{
                    deleteAllPropertiesFromFilteredOldList()
                }else{
                    deleteAllDrivesFromFilteredDrives()
                }
            }else{
                
            }
        default:
            defaults.set(true, forKey: "atHome")
            print("INSIDE DEFAULT")  //  Assume user is at home unless/until determined otherwise
        }
    }
    
}


extension HomeViewController: GMSMapViewDelegate{
    
    /* handles Info Window tap */
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        print("didTapInfoWindowOf")
    }
    /* handles Info Window long press */
    func mapView(_ mapView: GMSMapView, didLongPressInfoWindowOf marker: GMSMarker) {
        print("didLongPressInfoWindowOf")
    }
    
    func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D) {
        infoWindow.removeFromSuperview()
        
        if isGlobalDriveNowSelected == true{
            
//            if Network.reachability.status == .unreachable{
//                AppConfigs.updateUserInterface(view: view)
//            }else{
                mapView.selectedMarker = nil
                print("You tapped at \(coordinate.latitude), \(coordinate.longitude)")
                marker.isDraggable = true
                //29.709151, -95.456857
                marker.position.latitude = coordinate.latitude// 29.709151//coordinate.latitude
                marker.position.longitude = coordinate.longitude//-95.456857 // coordinate.longitude
                print(coordinate.latitude, coordinate.longitude)
                marker.snippet = "tapped"
                self.marker.icon = UIImage(named: "Shape")
                self.marker.map = mapView
                self.marker.appearAnimation = .pop
                mapView.selectedMarker = self.marker
                
                let point:CGPoint = mapView.projection.point(for: self.marker.position)
                let camera:GMSCameraUpdate = GMSCameraUpdate.setTarget(mapView.projection.coordinate(for: point))
                mapView.animate(with: camera)
                
                var addressString : String = ""
                let ceo: CLGeocoder = CLGeocoder()
                let loc: CLLocation = CLLocation(latitude:coordinate.latitude, longitude: coordinate.longitude)
                let tappedPlace = TappedPlace()
                ceo.reverseGeocodeLocation(loc, completionHandler:
                    {(placemarks, error) in
                        if (error != nil)
                        {
                            print("reverse geodcode fail: \(error!.localizedDescription)")
                        }
                        let pm = placemarks! as [CLPlacemark]
                        
                        if pm.count > 0 {
                            let pm = placemarks![0]
                            
                            print("pm.country-->",pm.country ?? "")
                            print("locality-->", pm.locality ?? "")
                            print("subLocality-->", pm.subLocality ?? "")
                            print("thoroughfare-->", pm.thoroughfare ?? "")
                            print("postalCode-->", pm.postalCode ?? "")
                            print("subThoroughfare-->", pm.subThoroughfare ?? "")
                            print("administrativeArea", pm.administrativeArea ?? "")
                            print(pm.inlandWater ?? "")
                            print(pm.isoCountryCode ?? "")
                            print("name-->",pm.name ?? "")
                            print("region-->", pm.region ?? "" )
                            //4613 Locust Street, Bellaire, TX, USA
                            
                            if pm.name != nil {
                                addressString = addressString + pm.name! + ", "
                                tappedPlace.street = pm.name!
                            }
                            
                            if pm.subLocality != nil {
                                addressString = addressString + pm.subLocality! + ", "
                                //tappedPlace.street = addressString
                            }
                            
                            if pm.locality != nil {
                                addressString = addressString + pm.locality! + ", "
                                tappedPlace.city = pm.locality!
                            }
                            
                            if pm.administrativeArea != nil {
                                addressString = addressString + pm.administrativeArea! + " "
                                tappedPlace.state = pm.administrativeArea!
                            }
                            
                            if pm.postalCode != nil {
                                addressString = addressString + pm.postalCode! + ",  "
                                tappedPlace.zip = pm.postalCode!
                            }
                            
                            if pm.country != nil {
                                addressString = addressString + pm.country!
                                tappedPlace.country = pm.country!
                            }
                            
                            print(addressString)
                            
                            self.marker.title = addressString
                            globalSelectedProperty.propertyAddress = addressString
                            globalSelectedProperty.latitude = Double(coordinate.latitude)
                            globalSelectedProperty.longitude = Double(coordinate.longitude)
                            
                            tappedPlace.latitude = Double(coordinate.latitude)
                            tappedPlace.longitude = Double(coordinate.longitude)
                            
                            globalTappedPlace = tappedPlace
                            self.showMarkerWithTitleSnippet(title: addressString, snippet: "tapped", position: self.marker.position)
                        }
                })
//            }
        }else{
            AppConfigs.showSnacbar(message: "Option is available when driving only", textColor: .red)
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        infoWindow.removeFromSuperview()
        marker.map = nil
        
    }
    
    /* set a custom Info Window */
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        
        return UIView()
    }
    
    //MARK - GMSMarker Dragging
    func mapView(_ mapView: GMSMapView, didBeginDragging marker: GMSMarker) {
        print("didBeginDragging")
    }
    func mapView(_ mapView: GMSMapView, didDrag marker: GMSMarker) {
        print("didDrag")
    }
    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
        print("didEndDragging")
        mapView.selectedMarker = marker
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        print("did tap a marker")
        
        print(marker.title ?? "")
        if marker.snippet == "Australia" {
            
            let url : String = UrlManager.baseURL() + UrlManager.apiString() + "/user/\(marker.title ?? "")/"
            print(url)
            
            self.btnUserNameToast.isHidden = false
            self.btnMinimiseUserToast.isHidden = false
            self.userToastView.isHidden = false
            self.userImageView.isHidden = false
            
            self.getCurrentUserInfo(urlString: url)
            
        }
        else if  marker.snippet == "tapped"{
            print(marker.snippet)
        }
        else{
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "PropertyDetailsViewController") as! PropertyDetailsViewController
            nextViewController.modalPresentationStyle = .fullScreen
            nextViewController.parentVC = "DrivedPropertyFromHome"
            nextViewController.propertyIDFromList = marker.title ?? ""
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        return false
        
    }
    
    // let the custom infowindow follows the camera
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        if (tappedMarker.userData != nil){
            let location = CLLocationCoordinate2D(latitude: marker.position.latitude, longitude: marker.position.longitude)
            
            infoWindow.center = mapView.projection.point(for: location)
            
        }
    }
    
    func getCurrentUserInfo(urlString:String) {
        print(urlString)
        self.showSpinner(onView: self.userImageView)
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success",data)
                let statusCode = response.response?.statusCode
                print(statusCode!)
                if statusCode == 200{
                    
                    DispatchQueue.main.async {
                        
                        
                        let currentUser = try? JSONDecoder().decode(CurrentUserInfoResponse.self, from: response.data!)
                        let fn = currentUser?.firstName
                        let ln = currentUser?.lastName
                        let thumbPhoto = currentUser?.photoThumb
                        self.btnUserNameToast.isHidden = false
                        self.btnMinimiseUserToast.isHidden = false
                        self.userToastView.isHidden = false
                        self.userImageView.isHidden = false
                        
                        
                        self.btnUserNameToast.alpha = 1.0
                        self.updateUserNameToastPosition()
                        self.btnUserNameToast.setTitle((fn ?? "") + " " + (ln ?? ""), for: .normal)
                        if thumbPhoto != nil {
                            let pictureURL = URL(string: thumbPhoto!)!
                            if pictureURL != nil{
                                self.userImageView.kf.setImage(with: pictureURL)
                            }else{
                                self.userImageView.image = UIImage(named: "new_ic_names")
                            }
                        }else{
                            self.userImageView.image = UIImage(named: "new_ic_names")
                        }
                        //                            UIView.animate(withDuration: 10.0, delay: 0.1, options: .curveEaseOut, animations: {
                        //                                self.btnUserNameToast.alpha = 0.0
                        //                            }, completion: {(isCompleted) in
                        //                                //label.removeFromSuperview()
                        //                                self.btnUserNameToast.isHidden = true
                        //                                self.btnMinimiseUserToast.isHidden = true
                        //
                        //                            })
                    }
                }
                else if statusCode == 403{
                    self.removeSpinner()
                    
                    self.btnUserNameToast.isHidden = true
                    self.btnMinimiseUserToast.isHidden = true
                    self.userToastView.isHidden = true
                    self.userImageView.isHidden = true
                    
                }
                else if statusCode == 401{
                    self.removeSpinner()
                    self.btnUserNameToast.isHidden = true
                    self.btnMinimiseUserToast.isHidden = true
                    self.userToastView.isHidden = true
                    self.userImageView.isHidden = true
                    
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                    
                    self.btnUserNameToast.isHidden = true
                    self.btnMinimiseUserToast.isHidden = true
                    self.userToastView.isHidden = true
                    
                    self.userImageView.isHidden = true
                    
                    
                }
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                
                self.btnUserNameToast.isHidden = true
                self.btnMinimiseUserToast.isHidden = true
                self.userToastView.isHidden = true
                
                self.userImageView.isHidden = true
                
                
                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
                //                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                //                let vc = mainStoryboard.instantiateViewController(withIdentifier: "WelcomeScreenViewController") as! WelcomeScreenViewController
                //                UIApplication.shared.keyWindow?.rootViewController = vc
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
    
    
}
