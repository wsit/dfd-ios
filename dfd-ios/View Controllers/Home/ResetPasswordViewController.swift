//
//  ResetPasswordViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 12/10/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit
import SwiftValidator
import Alamofire

class ResetPasswordViewController: UIViewController, ValidationDelegate {
    
    func validationSuccessful() {
        if  txtEmail.isEmpty == true  {
            
        }else{
            handleResetPassword()
        }
    }
    
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        for (field, error) in errors {
            if let field = field as? UITextField {
                field.layer.borderColor = UIColor.red.cgColor
                field.layer.borderWidth = 1.0
            }
            
        }
        
        
    }
    
    
    @IBOutlet weak var myNavBar: UINavigationBar!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var lblEmailTitle: UILabel!
    @IBOutlet weak var btnSentLink: UIButton!
    
    let validator = Validator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        myNavBar.prefersLargeTitles = false
        myNavBar.backgroundColor = .white
        
        myNavBar.layer.shadowColor = UIColor.black.cgColor
        myNavBar.layer.shadowOffset = CGSize(width: 0.0, height: 6.0)
        myNavBar.layer.shadowRadius = 4.5
        myNavBar.layer.shadowOpacity = 0.10
        myNavBar.layer.masksToBounds = false
        txtEmail.underlined()
        
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [lblEmailTitle], type: TextType.ListSubTitle.rawValue)
        txtEmail.font = ProNovaR16
        btnSentLink.backgroundColor = self.buttonBackgroundColor
        FontAndColorConfigs.setDynamicButtonsFontSize(buttons: [btnSentLink], type: TextType.ListTitle.rawValue)
        
        validator.registerField(txtEmail, errorLabel: nil ,rules: [RequiredRule(), EmailRule()])
        
        validator.styleTransformers(success:{ (validationRule) -> Void in
            print("here")
            // clear error label
            validationRule.errorLabel?.isHidden = true
            validationRule.errorLabel?.text = ""
            
            if let textField = validationRule.field as? UITextField {
                textField.layer.borderColor = UIColor.white.cgColor
                textField.underlined()
                //                textField.layer.borderWidth = 0.5
            }
        }, error:{ (validationError) -> Void in
            print("error")
            validationError.errorLabel?.isHidden = false
            validationError.errorLabel?.text = validationError.errorMessage
            if let textField = validationError.field as? UITextField {
                textField.layer.borderColor = UIColor.red.cgColor
                textField.layer.borderWidth = 1.0
            } else if let textField = validationError.field as? UITextView {
                textField.layer.borderColor = UIColor.red.cgColor
                textField.layer.borderWidth = 1.0
            }
        })
        
        
    }
    
    @IBAction func backClicked(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func sendLinkClicked(_ sender: Any) {
        
        validator.validate(self)
        
    }
    
    
    func handleResetPassword()  {
        
        let parameters = [ "email": txtEmail.text!] as [String : Any]
        print(parameters)
        
        let url = UrlManager.baseURL() + "/api/forget-password/"
        print(url)
        
        Alamofire.request(url, method: .post, parameters: parameters as Parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
            case.success(let data):
                print("success",data)
                self.removeSpinner()
                let statusCode = response.response?.statusCode
                print(statusCode!)
                
                if statusCode == 200{
                    
                    let resetPasswordResponse = try? JSONDecoder().decode(ResetPasswordResponse.self, from: response.data!)
                    
                    if resetPasswordResponse?.status == true{
                        
                        DispatchQueue.main.async {
                            self.removeSpinner()
                            AppConfigs.showSnacbar(message: (resetPasswordResponse?.message!)!, textColor: .green)
                            self.dismiss(animated: true, completion: nil)
                        }
                    }else{
                        AppConfigs.showSnacbar(message: (resetPasswordResponse?.message!)!, textColor: .green)
                    }                 }
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                   // AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)

                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                 self.removeSpinner()
            }
            self.removeSpinner()
        }
    }
}
