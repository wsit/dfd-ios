//
//  RouteViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 10/14/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON

class RouteViewController: UIViewController {
    
    @IBOutlet weak var myMap: GMSMapView!
    //   @IBOutlet weak var lblTitle: UILabel!
    
    var path = GMSMutablePath()
    var allPaths: [GMSMutablePath] = []
    var bounds = GMSCoordinateBounds()
    var locationManager = CLLocationManager()
    var currentLocation: CLLocation!
    var totalDistanceInKM: Double = 0.0
    var allDrivedProperties: [ListedProperty] = []
    var historyDetailsResponse: HistoryDetailsResponse?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.title = "Driving Details"
        
        let camera = GMSCameraPosition.camera(withTarget: CLLocationCoordinate2D(latitude: USA_LAT, longitude: USA_LONG), zoom: 17, bearing: 0, viewingAngle: 0)
        myMap.mapType = .satellite
        
        myMap.animate(to: camera)
        CATransaction.begin()
        CATransaction.setAnimationDuration(3.0)
        myMap.animate(toLocation: CLLocationCoordinate2D(latitude: USA_LAT, longitude: USA_LONG))
        myMap.animate(toZoom: 12)
        CATransaction.commit()
        
        if CLLocationManager.locationServicesEnabled() {
            self.myMap?.isMyLocationEnabled = true
            locationManager.desiredAccuracy = kCLLocationAccuracyBest // kCLLocationAccuracyBest
            locationManager.activityType = CLActivityType.automotiveNavigation
            //locationManager.distanceFilter = 1
            locationManager.headingFilter = 1
            self.locationManager.delegate = self
            self.locationManager.requestWhenInUseAuthorization()
            self.locationManager.startUpdatingLocation()
            self.locationManager.allowsBackgroundLocationUpdates = true
            //self.locationManager.startUpdatingHeading()
            
        }else{
            //location is not enabled
        }
        
        //        var previousLocation: CLLocation? = nil
        //        var currentLocation: CLLocation? = nil
        //
        //        for (index, element) in routeLocations.enumerated() {
        //            print("Item \(index): \(element)")
        //
        //            currentLocation = routeLocations[index]
        //
        //            if index == 0 {
        //                //previousLocation = routeLocations[index]
        //            }else{
        //                previousLocation = routeLocations[index - 1]
        //                let distanceInMeters = previousLocation!.distance(from: currentLocation!)
        //                totalCoveredDistance = totalCoveredDistance + distanceInMeters
        //            }
        //        }
        //
        //        print("totalCoveredDistance-->", totalCoveredDistance)
        //        totalDistanceInKM = totalCoveredDistance/1000
        //        print(totalDistanceInKM)
        //        print("totalCoveredDistance", totalCoveredDistance)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        //        allDrivedProperties.removeAll()
        //        let url : String = UrlManager.baseURL() + UrlManager.apiString() +  "/history/\((drivingDetails?.id)!)/properties/"
        //        print(url)
        //self.showSpinner(onView: view)
        // getAllDrivedProperties(urlString: url)
        
//        if Network.reachability.status == .unreachable{
//            AppConfigs.updateUserInterface(view: view)
//        }else{
//
            allDrivedProperties.removeAll()
            historyDetailsResponse = nil
            path.removeAllCoordinates()
            let url2 : String = UrlManager.baseURL() + UrlManager.apiString() +  "/history/\( String(describing: (drivingDetails?.id)!))/"
            print(url2)
            //self.showSpinner(onView: view)
            myMap.clear()
            getLatestDrivingDetails(urlString: url2)
            
//        }
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        //        }
    }
    
    @IBAction func backClicked(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func putMarkersOnMap() {
        
        for property in self.allDrivedProperties{
            
            let marker = GMSMarker()
            let lat = property.latitude
            let long = property.longitude
            marker.position = CLLocationCoordinate2D(latitude: lat!, longitude: long!)
            
            var address : String = ""
            let street = property.street
            if street != nil  &&  street != ""{
                address = address + street!
            }
            
            let city = property.city
            if city != nil &&  city != ""{
                address = address + ", " + city!
            }
            
            let state = property.state
            if state != nil &&  state != ""{
                address = address + ", " + state!
            }
            
            let zip = property.zip
            if zip != nil &&  zip != ""{
                address = address + ", " + zip!
            }
            
            marker.title = address //"\(property.street ?? ""), \(property.city ?? ""), \(property.state ?? ""), \(property.zip ?? "") "//\((property.id)!)"
            let markerImage = UIImage(named: "new_ic_tapped_property")!.withRenderingMode(.alwaysTemplate)
            //creating a marker view
            let markerView = UIImageView(image: markerImage)
            let tags = property.propertyTags
            if tags != nil{
                if tags?.count == 0{
                    
                    markerView.tintColor = UIColor.black // UIColor.blue
                    marker.iconView = markerView
                    marker.icon = markerImage
                    
                }else if tags!.count > 1{
                    
                    markerView.tintColor = .systemGray
                    marker.iconView = markerView
                    marker.icon = markerImage
                    
                }else if tags?.count == 1{
                    let tag = tags?[0]
                    let color = tag?.color?.colorCode
                    print(color ?? "")
                    if color != nil && color != ""{
                        let uiColor =  UIColor(hexString: color!)
                        markerView.tintColor = uiColor
                        marker.iconView = markerView
                        marker.icon = markerImage
                        
                    }else{
                        markerView.tintColor = UIColor.black // UIColor.blue
                        marker.iconView = markerView
                        marker.icon = markerImage
                    }
                }else{
                    print("unknown case")
                    markerView.tintColor = UIColor.black // UIColor.blue
                    marker.iconView = markerView
                    marker.icon = markerImage
                }
            }else{
                markerView.tintColor = UIColor.black // UIColor.blue
                marker.iconView = markerView
                marker.icon = markerImage
                
            }
            
            marker.map = self.myMap
            
        }
        
        if self.allDrivedProperties.count > 1{
            
            let cp = self.allDrivedProperties[0]
            let camera = GMSCameraPosition.camera(withTarget: CLLocationCoordinate2D(latitude: cp.latitude!, longitude: cp.longitude!), zoom: 15, bearing: 0, viewingAngle: 0)
            myMap.animate(to: camera)
        }else{
            
            if self.allDrivedProperties.count == 0{
                
            }else{
                
                let index = self.allDrivedProperties.count/2
                let cp = self.allDrivedProperties[index]
                let camera = GMSCameraPosition.camera(withTarget: CLLocationCoordinate2D(latitude: cp.latitude!, longitude: cp.longitude!), zoom: 15, bearing: 0, viewingAngle: 0)
                myMap.animate(to: camera)
            }
        }
    }
    
    
    var polyline = GMSPolyline()
    var animationPolyline = GMSPolyline()
    var animationPath = GMSMutablePath()
    var i: UInt = 0
    var timer: Timer!
    
    func drawPolylinesWithPause(){
        
        var polylinesWithPosition: [LocationWithIndexPosition] = []
        
        //        let string = (historyDetailsResponse?.polylines) ?? ""
        //        print(string)
        //        let data = string.data(using: .utf8)!
        let string = (historyDetailsResponse?.polylines) ?? ""
        let data = string.data(using: .utf8)!
        
        do {
            if let polylines = try? JSONDecoder().decode([Polyline].self, from: data)
            {
                print("polylines.count", polylines.count)
                
                for (index,polyline) in polylines.enumerated(){
                    
                    if index == 0{
                        
                        let startMarkerPosition = polyline
                        
                        let startMarker = GMSMarker()
                        startMarker.position = CLLocationCoordinate2D(latitude: startMarkerPosition.latitude!, longitude: startMarkerPosition.longitude!)
                        startMarker.icon = UIImage(named: "marker_drive_start_point")
                        startMarker.map = myMap
                    }
                    
                    let pwp = LocationWithIndexPosition()
                    pwp.latitude = polyline.latitude!
                    pwp.longitude = polyline.longitude!
                    
                    if polyline.position != nil{
                        pwp.position = polyline.position!
                    }else{
                        pwp.position = 0
                    }
                    polylinesWithPosition.append(pwp)
                    
                }
                
                let endMarkerPosition = polylines[polylines.count - 1]
                let endMarker = GMSMarker()
                endMarker.position = CLLocationCoordinate2D(latitude: endMarkerPosition.latitude ?? 0.0, longitude: endMarkerPosition.longitude ?? 0.0)
                endMarker.icon = UIImage(named: "marker_drive_end_point")
                endMarker.map = myMap
                
                
                //------------------start handle pause/resume polyline draw while driving
                
                let grouped = polylinesWithPosition.group(by: { $0.position })
                print("grouped.count", grouped.count)
                
                for (index,element) in grouped.enumerated(){
                    
                    print("index--->",index)
                    print("element--->",element)
                    print(element.key)
                    print(element.value)
                    let locations = element.value
                    
                    let lPath = GMSMutablePath()
                    
                    for (lindex, lelement) in locations.enumerated(){
                        
                        let lat = lelement.latitude
                        let long = lelement.longitude
                        
                        lPath.add(CLLocationCoordinate2D(latitude: lat, longitude: long))
                        path.add(CLLocationCoordinate2D(latitude: lat, longitude: long))
                        self.allPaths.append(lPath)
                        //self.timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(animatePolylinePath), userInfo: nil, repeats: true)
                        
                    }
                    
                    
                    let lPolyline = GMSPolyline(path: lPath) // GMSPolyline(path: path)
                    lPolyline.strokeWidth = 3
                    lPolyline.strokeColor = UIColor.red
                    lPolyline.map = myMap
                    //oldPolylineArr2.append(lPolyline)
                }
                //------------------start handle pause/resume polyline draw while driving
                
                
                let bounds = GMSCoordinateBounds(path: path)
                myMap.animate(toZoom: 10)
                self.myMap!.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 50.0))
                
            }else{
                
                print("very bad json")
            }
        }catch let error  {
            print(error.localizedDescription)
        }
    }
    
    func drawPolylines() {
        
        let string = (historyDetailsResponse?.polylines) ?? ""
        print(string)
        let data = string.data(using: .utf8)!
        
        do {
            if let polylines = try? JSONDecoder().decode([Polyline].self, from: data)
            {
                print("polylines.count", polylines.count)
                
                for polyline in polylines{
                    
                    let p = polylines.last
                    if p?.latitude == polyline.latitude{
                        // path.removeLastCoordinate()
                    }else{
                        path.add(CLLocationCoordinate2D(latitude: polyline.latitude!, longitude: polyline.longitude!))
                        bounds.includingCoordinate(CLLocationCoordinate2D(latitude: polyline.latitude!, longitude: polyline.longitude!))
                    }
                    
                }
                
                let startMarkerPosition = polylines.first
                let endMarkerPosition = polylines.last
                
                let startMarker = GMSMarker()
                startMarker.position = CLLocationCoordinate2D(latitude: startMarkerPosition?.latitude ?? 0.0, longitude: startMarkerPosition?.longitude ?? 0.0)
                startMarker.icon = UIImage(named: "marker_drive_start_point")
                startMarker.map = myMap
                
                let endMarker = GMSMarker()
                endMarker.position = CLLocationCoordinate2D(latitude: endMarkerPosition?.latitude ?? 0.0, longitude: endMarkerPosition?.longitude ?? 0.0)
                endMarker.icon = UIImage(named: "marker_drive_end_point")
                endMarker.map = myMap
                
                
                let polyline = GMSPolyline(path: path)
                //polyline.strokeWidth = 3
                //polyline.map = myMap
                let bounds = GMSCoordinateBounds(path: path)
                myMap.animate(toZoom: 10)
                self.myMap!.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 50.0))
                self.timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(animatePolylinePath), userInfo: nil, repeats: true)
                
                
            }else{
                
                print("very bad json")
            }
        }catch let error  {
            print(error.localizedDescription)
        }
    }
    
    
    @objc func animatePolylinePath(path: GMSMutablePath) {
        
        let counter = Double( self.path.count()/3000)
        print(counter)
        
        if (self.i < self.path.count()) {
            
            DispatchQueue.main.asyncAfter(deadline: .now() + (counter)) { // Change `2.0` to the desired number of seconds.
                // Code you want to be delayed
                self.animationPath.add(self.path.coordinate(at: self.i))
                self.animationPolyline.path = self.animationPath
                self.animationPolyline.strokeColor = UIColor.red
                self.animationPolyline.strokeWidth = 3
                self.animationPolyline.map = self.myMap
                self.i += 1
            }
            
            
        }
        else {
            
            self.timer.invalidate()
        }
    }
    
    
    
    func getAllDrivedProperties(urlString: String) {
        
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success",data)
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    
                    let allDrivedPropertyResponse = try? JSONDecoder().decode(GetAllPropertyFromHistoryResponse.self, from: response.data!)
                    
                    var items: [ListedProperty] = self.allDrivedProperties
                    items.append(contentsOf: (allDrivedPropertyResponse?.results ?? []))
                    self.allDrivedProperties.removeAll()
                    self.allDrivedProperties = items
                    
                    DispatchQueue.main.async {
                        print("allDrivedProperties------------\n",self.allDrivedProperties)
                        self.removeSpinner()
                        //self.putMarkersOnMap()
                    }
                }
                    
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                
                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
    func  getLatestDrivingDetails(urlString: String) {
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success",data)
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    
                    let allDrivedPropertyResponse = try? JSONDecoder().decode(HistoryDetailsResponse.self, from: response.data!)
                    self.historyDetailsResponse = allDrivedPropertyResponse
                    
                    let drivingDet = DrivingInfo(id: allDrivedPropertyResponse?.id, startPointLatitude: allDrivedPropertyResponse?.startPointLatitude, startPointLongitude: allDrivedPropertyResponse?.startPointLongitude, endPointLatitude: allDrivedPropertyResponse?.endPointLatitude, endPointLongitude: allDrivedPropertyResponse?.endPointLongitude, image: allDrivedPropertyResponse?.image, startTime: allDrivedPropertyResponse?.startTime, endTime: allDrivedPropertyResponse?.endTime, polylines: allDrivedPropertyResponse?.polylines, length: allDrivedPropertyResponse?.length, user: allDrivedPropertyResponse?.user, propertyCount: allDrivedPropertyResponse?.propertyCount, createdAt: allDrivedPropertyResponse?.createdAt, updatedAt: allDrivedPropertyResponse?.updatedAt)
                    drivingDetails = drivingDet
                    
                    self.allDrivedProperties.removeAll()
                    self.allDrivedProperties = []
                    
                    DispatchQueue.main.async {
                        self.allDrivedProperties = (allDrivedPropertyResponse?.property ?? [])
                        print("allDrivedProperties------------\n",self.allDrivedProperties)
                        self.removeSpinner()//self.view.activityStopAnimating()
                        
                        self.putMarkersOnMap()
                        
                        //handle pause/resume polyline draw
                        //                        self.drawPolylines()
                        self.drawPolylinesWithPause()//
                    }
                }
                    
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                
                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
    @IBAction func doneButtonClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        //sendDrivingData()
    }
    
    @IBAction func showListView(_ sender: Any) {
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "MyStory", bundle: nil)
        
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "HistoryDetailsViewController") as! HistoryDetailsViewController
        vc.historyID = (drivingDetails?.id)!
        print((drivingDetails?.id)!)
        print((drivingDetails?.length)!)
        
        vc.parentString = "route"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func requestWith(endUrl: String, imageData: Data?, parameters: [String : Any], onCompletion: ((DrivingResponse?) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
        
        self.showSpinner(onView: self.view)
        let url = endUrl /* your API url */
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken(),
            "Content-type": "multipart/form-data"
        ]
        
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value) in parameters {
                
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            if let data = imageData{
                multipartFormData.append(data, withName: "image", fileName: "file.png", mimeType: "image/png")
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print("Succesfully uploaded")
                    print(response)
                    if let err = response.error{
                        onError?(err)
                        return
                    }
                    onCompletion?(nil)
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                onError?(error)
            }
        }
    }
    
    func startMonitoringLocation() {
        if CLLocationManager.locationServicesEnabled() {
            locationManager.requestWhenInUseAuthorization()
            locationManager.startMonitoringSignificantLocationChanges()
            locationManager.startUpdatingLocation()
        }
    }
    
    
    func stopMonitoringLocation() {
        locationManager.stopMonitoringSignificantLocationChanges()
        locationManager.stopUpdatingLocation()
    }
    
}

extension RouteViewController: CLLocationManagerDelegate{
    //Location Manager delegates
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("location manager error -> \(error.localizedDescription)")
        AppConfigs.showSnacbar(message: "Please enable location permission", textColor: .red)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        case .restricted:
            break
        case .denied:
            stopMonitoringLocation()
            break
        default:
            //addCurrentLocationMarker()
            startMonitoringLocation()
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let location = locations.first else {
            return
        }
        
        locationManager.stopUpdatingLocation()
        //        currentLocation = locationManager.location
        //        print("latitude----", currentLocation.coordinate.latitude)
        //        print("longitude----", currentLocation.coordinate.longitude)
    }
    
    
    func cameraMoveToLocation(toLocation: CLLocationCoordinate2D?) {
        
    }
}
//
//    func sendDrivingData() {
//
//        let renderer = UIGraphicsImageRenderer(size: self.myMap.frame.size)
//        let image = renderer.image(actions: { context in
//            self.myMap.layer.render(in: context.cgContext)
//        })
//
//        //Save it to the camera roll
//        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
//
//        let url = UrlManager.baseURL() + UrlManager.apiString() + "/history/\(String(describing: (drivingDetails?.id)!))/end-driving/"
//        print(url)
//
//        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
//        print(headers)
//
//        //current date time
//        let now = Date()
//        let formatter = DateFormatter()
//        formatter.timeZone = TimeZone.current
//        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSX"
//        let dateString = formatter.string(from: now)
//        print(dateString)
//
//
//        var polyLines: [Polyline] = []
//        var data: [Data] = []
//        var results: [String] = []
//        var jsonArray : [MyPolyline] = []
//        var decodedPolylines: Array<Polyline> = []
//        polyLines.removeAll()
//        decodedPolylines.removeAll()
//
//        for location in routeLocations{
//
//            let polyline : Polyline = Polyline(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
//            polyLines.append(polyline)
//
//            let jsonserializer = JSONSerializer.toJson(polyline)
//            print(jsonserializer)
//            results.append(jsonserializer)
//
//        }
//
//        let jsonserializerString = JSONSerializer.toJson(results)
//        print("jsonserializerString", jsonserializerString)
//
//        let jsonserializer = JSONSerializer.toJson(polyLines)
//        print("jsonserializer", jsonserializer)
//
//        print(polyLines.count);print(results);print(polyLines)
//
//        let params = [
//            "end_point_latitude" : routeLocations.last?.coordinate.latitude ?? 0.00,
//            "end_point_longitude" : routeLocations.last?.coordinate.longitude ?? 0.00,
//            "end_time" : dateString,
//            "polylines" : jsonserializer,//polyLines,//data,//jsonArray, //data, //polyLines,
//            "length" : totalDistanceInKM
//            ] as Parameters
//        print(params)
//
//        let imgData = image.jpegData(compressionQuality: 0.7)!
//
//        self.requestWith(endUrl: url, imageData: imgData, parameters: params, onCompletion: { (response) in
//
//            DispatchQueue.main.async {
//                routeLocations.removeAll()
////                let polylines = response?.data?.polylines
////                print(polylines?.first?.latitude)
//                globalDrivingInfo = nil
//                self.removeSpinner()
//                self.dismiss(animated: true, completion: nil)
//            }
//
//        }) { (error) in
//            print(error?.localizedDescription)
//            self.removeSpinner()
//            AppConfigs.showSnacbar(message: error!.localizedDescription, textColor: .red)
//        }
//    }
