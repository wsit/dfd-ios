//
//  CircularProgressView.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 12/9/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import Foundation
import UIKit
class CircularProgressView: UIView {// First create two layer propertiesprivate var circleLayer = CAShapeLayer()
    
    // First create two layer properties
    private var circleLayer = CAShapeLayer()
    private var progressLayer = CAShapeLayer()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        // createCircularPath()
        
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        // createCircularPath()
        
    }
    
    func createCircularPath(type: String) {
        let circularPath = UIBezierPath(arcCenter: CGPoint(x: frame.size.width / 2.0, y: frame.size.height / 2.0), radius: 50, startAngle: -.pi / 2, endAngle: 3 * .pi / 2, clockwise: true)
        circleLayer.path = circularPath.cgPath
        circleLayer.fillColor = UIColor.clear.cgColor
        circleLayer.lineCap = .round
        circleLayer.lineWidth = 10.0
        circleLayer.strokeColor = UIColor.white.cgColor
        progressLayer.path = circularPath.cgPath
        progressLayer.fillColor = UIColor.clear.cgColor
        progressLayer.lineCap = .round
        progressLayer.lineWidth = 10.0
        progressLayer.strokeEnd = 0
        if type == "tagged"{
            circleLayer.strokeColor = UIColor(red:0.39, green:0.84, blue:0.93, alpha:1).cgColor
            progressLayer.strokeColor = UIColor(red:0.49, green:0.49, blue:0.94, alpha:1).cgColor
            
        }else if type == "drives"{
            circleLayer.strokeColor = UIColor(red:0.86, green:0.27, blue:0.58, alpha:1).cgColor
            progressLayer.strokeColor = UIColor(red:0.55, green:0.17, blue:0.94, alpha:1).cgColor
        }else if type == "houzes"{
            circleLayer.strokeColor = UIColor(red:1, green:0.8, blue:0.14, alpha:1).cgColor
            progressLayer.strokeColor = UIColor(red:1, green:0.56, blue:0.13, alpha:1).cgColor
        }else{
            
        }
        
        layer.addSublayer(circleLayer)
        layer.addSublayer(progressLayer)
        
    }
    
    func progressAnimation(duration: TimeInterval) {
        let circularProgressAnimation = CABasicAnimation(keyPath: "strokeEnd")
        circularProgressAnimation.duration = duration
        circularProgressAnimation.toValue = 1.0
        circularProgressAnimation.fillMode = .forwards
        circularProgressAnimation.isRemovedOnCompletion = false
        progressLayer.add(circularProgressAnimation, forKey: "progressAnim")}
    
}
