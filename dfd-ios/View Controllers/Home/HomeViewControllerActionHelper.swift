//
//  HomeViewControllerActionHelper.swift
//  HouZes
//
//  Created by Mahadhi Hassan Chowdhury on 3/10/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//

import Foundation

import UIKit
import LNSideMenu
import GoogleMaps
import SnapKit
import TTGSnackbar
import Floaty
import JJFloatingActionButton
//import GooglePlaces
//import GooglePlacePicker
import Alamofire
import CoreFoundation
import DropDown
import RealmSwift
import Kingfisher


extension HomeViewController{

    // Start: --------new design actions for Version 3
    
    
    // Start: --------end design actions for Version 3
    
    // Start: --------new design actions
    
    @IBAction func btnSatelliteClicked(_ sender: Any) {
        self.mapView.mapType = .satellite
        self.mapTypeView.isHidden = true
        globalMapType = 1
    }
    
    @IBAction func btnNormalClicked(_ sender: Any) {
        self.mapView.mapType = .normal
        self.mapTypeView.isHidden = true
        globalMapType = 0
    }
    
    @IBAction func minimiseButtonClicked(_ sender: Any) {
        self.informativeView.isHidden = true
        self.updateUserNameToastPosition()
        
        self.isInformativeViewShown = false
        self.btnInfo.isHidden = false
        leftFilterButton.isHidden = false
        //showLeftFilterButton(button:leftFilterButton )
    }
    
    @IBAction func switchMapType(_ sender: Any) {
        //mapTypeDropdown.show()
        
        if self.mapTypeView.isHidden == true{
            CATransaction.begin()
            
            let transition = CATransition()
            transition.timingFunction = CAMediaTimingFunction.init(name: .easeInEaseOut)
            transition.type = .push
            transition.subtype = .fromRight
            
            self.mapTypeView.layer.add(transition, forKey: kCATransition)
            CATransaction.commit()
            
            self.mapTypeView.isHidden = false
        }else{
            self.mapTypeView.isHidden = true
        }
    }
    
    @IBAction func changePageControllerI(_ sender: UIPageControl) {
        //var noOfTaggedProperty: Int = 0 var totalCoveredDistance: Double = 0.0 var duration: Double = 0.0
        if sender.currentPage == 0{
            lblInformativeSubTitle.text = "Properties Added"
            lblInformationHeader.text = "\(noOfTaggedProperty)"
        }else if sender.currentPage == 1{
            lblInformativeSubTitle.text = "Miles Driven"
            
            let distanceInMile = totalCoveredDistance/1609
            print(distanceInMile)
            
            lblInformationHeader.text = String(format: "%.2f", distanceInMile) //"\(totalCoveredDistance)" //"90631"
        }
        else if sender.currentPage == 2{
            lblInformativeSubTitle.text = "Duration"
            lblInformationHeader.text = timeString(time: TimeInterval(seconds))
            //lblInformationHeader.text = "\(duration)"
        }else{
            print("unknown")
        }
    }
    
    @IBAction func infoButtonClicked(_ sender: Any) {
        
        if self.isInformativeViewShown == true{
            self.isInformativeViewShown = false
            self.informativeView.isHidden = true
        }else{
            self.informativeView.isHidden = false
            self.isInformativeViewShown = true
            self.leftFilterView.isHidden = true
        }
        
        self.updateUserNameToastPosition()
        
    }
    
    
    @IBAction func driveNowButtonClicked(_ sender: Any) {
        
        
        if CLLocationManager.locationServicesEnabled() {
            
            switch CLLocationManager.authorizationStatus() {
           
            case .notDetermined, .restricted, .denied:
                print("No access")
                self.showAlert(message: "Location services are not enabled. Please enable your location from Device Setting")
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                
                
//                if Network.reachability.status == .unreachable{
//                    AppConfigs.updateUserInterface(view: view)
//                }else{
//
                    if self.btnDriveNow.currentTitle == "Drive Now"{
                        //buttons
                        self.btnDriveNow.isHidden = true
                        self.btnInfo.isHidden = true
                        self.btnPublic.isHidden = false
                        self.btnPrivate.isHidden = false
                        self.showPublicButton(button: btnPublic)
                        self.showPrivateButton(button: btnPrivate)
                        AppConfigs.showTopSnacbar(message: "Please choose your driving visibility", textColor: .red)
                        //leftFilterButton.isHidden = true
                        //showLeftFilterButton(button:leftFilterButton )
                        
                    }
                    else if self.btnDriveNow.currentTitle == "Stop"{
                        
                        SocketIOManager.shared.stoppedDriving()
                        
                        //buttons
                        self.btnDriveNow.isHidden = true
                        self.btnPrivate.isHidden = false
                        self.btnPublic.isHidden = false
                        self.btnInfo.isHidden = false
                        
                        leftFilterButton.isHidden = false
                        showLeftFilterButton(button:leftFilterButton )
                        
                        self.showResumeButton(button: btnPublic)
                        self.showFinishButton(button: btnPrivate)
                        self.showInfoButton(button: btnInfo)
                        
                        //views
                        self.informativeView.isHidden = true
                        self.updateUserNameToastPosition()
                        
                        isGlobalDriveNowSelected = false
                        isGlobalPauseSelected = true
                        
                        //handle pause/resume polyline draw
                        currentPauseIndex =  currentPauseIndex + 1
                        routeLocations.removeAll()
                        path.removeAllCoordinates()
                        //todooffLine
                        //                        AppConfigs.saveDrivingPausedStatus(status: isGlobalPauseSelected)
                        //                        print("isGlobalPauseSelected", isGlobalPauseSelected)
                        //                        print("AppConfigs.getSavedDrivingPausedStatus()", AppConfigs.getSavedDrivingPausedStatus())
                        
                        resumeTapped = false
                        self.pauseButtonTapped()
                        //do actions for Stop
                    }else{
                        print("unknown case")
                    }
//                }
            }
        } else {
            
            print("Location services are not enabled")
            self.showAlert(message: "Location services are not enabled. Please enable your location from Device Setting")
        }
        
    }
    
    @IBAction func publicButtonClicked(_ sender: Any) {
        
        if btnPublic.currentTitle == "Public"{
            globalDriveType = "PUBLIC"
            searchForLocation()
            if self.locationManager.location != nil{
                self.sendStartDrivingLocation(location: self.locationManager.location!)
            }else{
                AppConfigs.showSnacbar(message: "Please check your GPS service", textColor: .red)
            }
        }
        else if  btnPublic.currentTitle == "Resume"{
            self.btnPrivate.isHidden = true
            self.btnPublic.isHidden = true
            self.btnDriveNow.isHidden = false
            self.btnInfo.isHidden = false

            leftFilterButton.isHidden = false
            showLeftFilterButton(button:leftFilterButton )
            
            self.showStopButton(button: btnDriveNow)
            self.showInfoButton(button: btnInfo)
            self.informativeView.isHidden = false
            
            self.updateUserNameToastPosition()
            
           
            self.isInformativeViewShown = true
            
            //do actions for Resume
            isGlobalDriveNowSelected = true
            isGlobalPauseSelected = false
            //todooffLine
            //            AppConfigs.saveDrivingPausedStatus(status: isGlobalPauseSelected)
            //            print("isGlobalPauseSelected", isGlobalPauseSelected)
            //            print("AppConfigs.getSavedDrivingPausedStatus()", AppConfigs.getSavedDrivingPausedStatus())
            
            self.taggedProgressView.isHidden = true
            self.drivesProgressView.isHidden = true
            self.houzesProgressView.isHidden = true
            self.centerViewHeightConstraint.constant = 0
            self.centerUpView.layoutIfNeeded()
            
            isResumeGlobal = true
            runTimer()
            
            
        }else{
            print("unknown case")
        }
    }
    
    @IBAction func privateButtonClicked(_ sender: Any) {
        
        if btnPrivate.currentTitle == "Private"{
            
            globalDriveType = "PRIVATE"
            searchForLocation()
            if self.locationManager.location != nil{
                self.sendStartDrivingLocation(location: self.locationManager.location!)
            }else{
                
                AppConfigs.showSnacbar(message: "Please check your GPS service", textColor: .red)
            }
            
        }
        else if  btnPrivate.currentTitle == "Finish"{
            self.btnPrivate.isHidden = true
            self.btnPublic.isHidden = true
            self.btnInfo.isHidden = true
            self.btnDriveNow.isHidden = false
            leftFilterButton.isHidden = true
            //showLeftFilterButton(button:leftFilterButton )
            
            self.showDriveNowButton(button: btnDriveNow)
            activeUserIDs.removeAll()
            //views
            self.informativeView.isHidden = true
            self.updateUserNameToastPosition()
            self.isInformativeViewShown = false
            isGlobalDriveNowSelected = false
            isGlobalPauseSelected = false
            //todooffLine
            //            AppConfigs.saveDrivingPausedStatus(status: isGlobalPauseSelected)
            //            print("isGlobalPauseSelected", isGlobalPauseSelected)
            //            print("AppConfigs.getSavedDrivingPausedStatus()", AppConfigs.getSavedDrivingPausedStatus())
            handleClearFilter()
            sendDrivngDataToServer()
            self.handleFinishDriving()
            
            if  isGlobalPauseSelected == false{
                self.resetButtonTapped()
            }
            
            
        }else{
            print("unknown case")
        }
    }
    
    @objc func addToListButtonAction(sender: UIButton!) {
        print("addToListButtonAction")
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ListOfListViewController") as! ListOfListViewController
        nextViewController.modalPresentationStyle = .fullScreen
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    @objc func viewDetailsButtonAction(sender: UIButton!) {
        
        print("viewDetailsButtonAction")
        
        globalIsPropertyDetailsAvailable = false
        globalSelectedPropertyFromList = nil
        globalIsListedProperty = false
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "PropertyDetailsViewController") as! PropertyDetailsViewController
        nextViewController.modalPresentationStyle = .fullScreen
        nextViewController.parentVC = "Home"
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    
    @objc func openTrackerInBrowser(){
        
        //lat: String, long: String, dlat: String, dlong: String
        let lat = locationManager.location?.coordinate.latitude
        let long = locationManager.location?.coordinate.longitude
        let dlat = globalSelectedProperty.latitude
        let dlong = globalSelectedProperty.longitude
        
        if let url1 = URL(string: "comgooglemaps://") {
            if UIApplication.shared.canOpenURL(url1) {
                
                let url = "comgooglemaps-x-callback://?saddr=\(String(describing: lat!)),\(String(describing: long!))&daddr=\(dlat),\(dlong)&directionsmode=driving&x-success=myapp://?resume=true&x-source=MyAppName"
                if let url1 = URL(string: url) {
                    // UIApplication.shared.openURL(url1)
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(url1, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(url1)
                    }
                }
            } else {
                print("Can't use comgooglemaps://")
            }
        }
    }

}
