//
//  ResetPasswordResponse.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 12/10/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

//   let resetPasswordResponse = try? newJSONDecoder().decode(ResetPasswordResponse.self, from: jsonData)

import Foundation

// MARK: - ResetPasswordResponse
class ResetPasswordResponse: Codable {
    let status: Bool?
    let data: ResetData?
    let message: String?
    
    init(status: Bool?, data: ResetData?, message: String?) {
        self.status = status
        self.data = data
        self.message = message
    }
}

// MARK: - DataClass
class ResetData: Codable {
    let id: Int?
    let email, createdAt, updatedAt: String?
    
    enum CodingKeys: String, CodingKey {
        case id, email
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
    
    init(id: Int?, email: String?, createdAt: String?, updatedAt: String?) {
        self.id = id
        self.email = email
        self.createdAt = createdAt
        self.updatedAt = updatedAt
    }
}

