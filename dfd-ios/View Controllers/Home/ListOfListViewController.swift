//
//  ListOfListViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 5/27/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit
import Alamofire
import TTGSnackbar

class ListOfListViewController: UIViewController {
    
    @IBOutlet weak var myNavigationBar: UINavigationBar!
    @IBOutlet weak var listTableView: UITableView!
    @IBOutlet weak var doneButton: UIBarButtonItem!
    @IBOutlet weak var btnAddList: UIButton!
    
    let kCellIdentifier = "menuCell"
    //var items: Array = ["Divorced", "Engineers", "Doctors", "Actors", "Players", "Govt Job Holders"]
    var allList: [MList] = []
    var url : String = ""
    var nextUrl: String = ""
    var previousUrl: String = ""
    var jsonResponse: GetAllListsResponse?
    var parentString : String = ""
    var selectedList : MList?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Choose a List"
        
        myNavigationBar.barTintColor  = UIColor.white
        myNavigationBar.prefersLargeTitles = false
        myNavigationBar.backgroundColor = .white
        self.setUpNavigationBar()
        myNavigationBar.layer.shadowColor = UIColor.black.cgColor
        myNavigationBar.layer.shadowOffset = CGSize(width: 0.0, height: 6.0)
        myNavigationBar.layer.shadowRadius = 4.5
        myNavigationBar.layer.shadowOpacity = 0.10
        myNavigationBar.layer.masksToBounds = false
        
        btnAddList.cornerRadius = btnAddList.frame.width/2
        let nib = UINib(nibName: "MenuTableViewCell", bundle: nil)
        listTableView.register(nib, forCellReuseIdentifier: kCellIdentifier)
        self.listTableView.tableFooterView = UIView()
        self.doneButton.isEnabled = false
        self.doneButton.tintColor = UIColor.clear
        
        listTableView.separatorStyle = .singleLine
        listTableView.separatorColor = FontAndColorConfigs.getSeperatorColor()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        if Network.reachability.status == .unreachable{
            AppConfigs.updateUserInterface(view: view)
        }else{
            allList.removeAll()
            let url : String = UrlManager.getAllMyListsURL()
            
            print(url)
            self.showSpinner(onView: view)
            getAllLists(urlString: url)
        }
    }
    
    
    
    @IBAction func backAction(_ sender: Any) {
        //_ = self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneButtonAction(_ sender: Any) {
        
        if self.selectedList != nil{
            self.showSpinner(onView: view)
            self.handleAddToList()
            
        }else{
            
            AppConfigs.showSnacbar(message: "Select A List First", textColor: .green)
        }
    }
    
    @IBAction func btnAddNewListClicked(_ sender: Any) {
        
        showInputDialog(type: "list", title: "Create New List", subtitle: "Please provide a Name below.", actionTitle: "Create", cancelTitle: "Cancel", inputPlaceholder: "New List Name", inputKeyboardType: .default) { (input:String?) in
            print("The new name is \(input ?? "")")
            
            self.showSpinner(onView: self.view)
            self.handleAddNewList(listName: input!)
        }
    }
    
    func handleAddNewList(listName:String)  {
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        
        let parameters = [ K.APIParameterKey.name: listName] as [String : Any]
        print(parameters)
        
        let url = UrlManager.getAddNewListURL()
        print(url)
        
        Alamofire.request(url, method: .post, parameters: parameters as Parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
            case.success(let data):
                print("success",data)
                self.removeSpinner()
                let statusCode = response.response?.statusCode
                print(statusCode!)
                
                if statusCode == 200{
                    
                    let createListResponse = try? JSONDecoder().decode(CreateListResponse.self, from: response.data!)
                    
                    if createListResponse?.status == true{
                        
                        DispatchQueue.main.async {
                            self.removeSpinner()
                            AppConfigs.showSnacbar(message: (createListResponse?.message!)!, textColor: .green)
                            self.allList.removeAll()
                            //self.showSpinner(onView: self.view)
                            self.getAllLists(urlString: UrlManager.getAllMyListsURL())
                        }
                    }else{
                        AppConfigs.showSnacbar(message: (createListResponse?.message!)!, textColor: .green)
                    }                 }
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
            case.failure(let error):
                print("Not Success",error)
                
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                
                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
            self.removeSpinner()
        }
    }
    
    func handleAddToList() {
        
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        
        var parameters =  [
            "street": globalTappedPlace.street,
            "city": globalTappedPlace.city,
            "state": globalTappedPlace.state,
            "zip": globalTappedPlace.zip,
            "cad_acct": "12345",
            "gma_tag": 12346,
            "latitude": globalTappedPlace.latitude,
            "longitude": globalTappedPlace.longitude,
            "user_list" : (selectedList?.id)!
            ]  as [String : Any]
        
        if globalDrivingInfo != nil{
            parameters =  [
                "street": globalTappedPlace.street,
                "city": globalTappedPlace.city,
                "state": globalTappedPlace.state,
                "zip": globalTappedPlace.zip,
                "cad_acct": "12345",
                "gma_tag": 12346,
                "latitude": globalTappedPlace.latitude,
                "longitude": globalTappedPlace.longitude,
                "user_list" : (selectedList?.id)!,
                "history" : (globalDrivingInfo?.id)!
                ]  as [String : Any]
        }
        print(parameters)
        
        let url = UrlManager.baseURL() + UrlManager.apiString() + "/property/" //UrlManager.AddPropertyToListURL()
        print(url)
        Alamofire.request(url, method: .post, parameters: parameters as Parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                print("success",data)
                
                self.removeSpinner()
                let statusCode = response.response?.statusCode
                print(statusCode!)
                
                if statusCode == 200{
                    
                    let addedToListResponse = try? JSONDecoder().decode(AddedToListResponse.self, from: response.data!)
                    
                    if addedToListResponse?.status == true{
                        
                        if addedToListResponse?.data != nil {
                            globalSelectedPropertyFromList = addedToListResponse?.data!
                            
                            if globalSelectedPropertyFromList != nil{
                                globalListedPropertiesInCurrentDrive.append(globalSelectedPropertyFromList!)
                            }
                            print(globalSelectedPropertyFromList?.id ?? 0)
                            globalSelectedList = self.selectedList
                            globalIsListedProperty = true
                            
                            DispatchQueue.main.async {
                                self.removeSpinner()
                                if self.parentString == "PropertyDetails"{
                                    self.dismiss(animated: true, completion: nil)
                                }else{
                                    self.dismiss(animated: true) {
                                        NotificationCenter.default.post(name:NSNotification.Name("AddedToList"),object: nil)
                                        NotificationCenter.default.post(name:NSNotification.Name("notifyForPropertyAddedToListFromAddProperty"),object: nil)
                                    }
                                }
                            }
                        }else{
                            self.removeSpinner()
                            AppConfigs.showSnacbar(message: (addedToListResponse?.message)!, textColor: .green)
                        }
                        
                    }else{
                        
                    }
                    
                }
                    
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                    
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
            case.failure(let error):
                print("Not Success",error)
                
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                
                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
            self.removeSpinner()
        }
    }
    
    func getAllLists(urlString: String) {
        
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success",data)
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    
                    let allLists = try? JSONDecoder().decode(GetAllListsResponse.self, from: response.data!)
                    self.jsonResponse = allLists
                    
                    var items: [MList] = self.allList
                    items.append(contentsOf: (allLists?.results)!)
                    
                    self.allList.removeAll()
                    self.allList = items
                    
                    DispatchQueue.main.async {
                        print("allProjects------------\n",self.allList)
                        self.removeSpinner()//self.view.activityStopAnimating()
                        
                        self.perform(#selector(self.loadTable), with: nil, afterDelay: 1.0)
                    }
                }
                    
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
                
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                
                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
                
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
}

extension ListOfListViewController: UITableViewDataSource {
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if allList.count == 0 {
            tableView.setEmptyMessage("Nothing yet to show")
        } else {
            tableView.restore()
        }
        return allList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier) as? MenuTableViewCell
        if cell == nil {
            cell = (UITableViewCell(style: .default, reuseIdentifier: kCellIdentifier) as! MenuTableViewCell)
        }
        if allList.count > indexPath.row{
            
            let list = allList[indexPath.row]
            cell?.lblTitle.text =  (list.name)!
            cell?.textLabel?.textColor = UIColor(red:0.2, green:0.3, blue:0.41, alpha:1)
            
            FontAndColorConfigs.setDynamicLabelFontSize(labels: [cell!.lblTitle], type: TextType.ListTitle.rawValue)
            cell?.titleImageView?.image = UIImage(named: "new_ic_list")
            cell?.titleImageView?.tintColor = UIColor(red:0.2, green:0.28, blue:0.36, alpha:1)
            cell?.lblSeperator.isHidden = false
            cell?.lblSeperator.backgroundColor = FontAndColorConfigs.getSeperatorColor()
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row == allList.count - 1 {
            // we are at last cell load more content
            if allList.count < (jsonResponse?.count)! {
                self.getAllLists(urlString: (jsonResponse?.next)!)
            }
        }
    }
    
    
    @objc func loadTable() {
        self.listTableView.reloadData()
    }
}

extension ListOfListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let list = allList[indexPath.row]
        self.selectedList = list
        
        let cell = tableView.cellForRow(at: indexPath) as! MenuTableViewCell
        cell.backgroundColor = UIColor.white
        cell.contentView.backgroundColor = UIColor.white
        
        cell.textLabel?.textColor = UIColor(red:0.07, green:0.82, blue:0.41, alpha:1)
        
        self.doneButton.isEnabled = true
        self.doneButton.tintColor = UIColor.black
        
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [cell.lblTitle], type: TextType.ListTitle.rawValue)
        
        cell.titleImageView?.image = UIImage(named: "selected")
        cell.titleImageView?.tintColor = UIColor(red:0.07, green:0.82, blue:0.41, alpha:1)
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! MenuTableViewCell
        cell.backgroundColor = UIColor.white
        cell.contentView.backgroundColor = UIColor.white
        tableView.separatorStyle = .none
        cell.titleImageView?.image = UIImage(named: "new_ic_list")
        cell.titleImageView?.tintColor = UIColor(red:0.2, green:0.28, blue:0.36, alpha:1)
    }
}
