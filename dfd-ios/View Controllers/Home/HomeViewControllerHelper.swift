//
//  HomeViewControllerHelper.swift
//  HouZes
//
//  Created by Mahadhi Hassan Chowdhury on 3/10/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//


import UIKit
import LNSideMenu
import GoogleMaps
import SnapKit
import TTGSnackbar
import Floaty
import JJFloatingActionButton
//import GooglePlaces
//import GooglePlacePicker
import Alamofire
import CoreFoundation
import DropDown
import RealmSwift
import Kingfisher
import AuthenticationServices

extension HomeViewController: ASWebAuthenticationPresentationContextProviding{
    
    @available(iOS 12.0, *)
    func presentationAnchor(for session: ASWebAuthenticationSession) -> ASPresentationAnchor {
        return view.window!
    }
    
    
    // Mark: --------New Design functions ---------------
    
    func animateTiles() {
        CATransaction.begin()
        
        CATransaction.setAnimationDuration(15.0)
        CATransaction.setCompletionBlock {
            let time = DispatchTime.now() + 5.0
            DispatchQueue.main.asyncAfter(deadline: time) {
                self.animateTiles()
            }
        }
        
        let transition = CATransition()
        transition.timingFunction = CAMediaTimingFunction.init(name: .easeInEaseOut)
        transition.type = .push
        transition.subtype = .fromRight
        
        lblInformationHeader.layer.add(transition, forKey: kCATransition)
        lblInformativeSubTitle.layer.add(transition, forKey: kCATransition)
        
        if  pageControl.currentPage == 0{
            lblInformationHeader.pushTransition(3.0)
            lblInformativeSubTitle.pushTransition(3.0)
            lblInformativeSubTitle.text = "Properties Added"
            lblInformationHeader.text = "\(noOfTaggedProperty)"
            pageControl.currentPage = pageControl.currentPage + 1
        }
            
        else if  pageControl.currentPage == 1 {
            lblInformationHeader.pushTransition(3.0)
            lblInformativeSubTitle.pushTransition(3.0)
            lblInformativeSubTitle.text = "Miles Driven"
            
            let distanceInMile = totalCoveredDistance/1609
            print(distanceInMile)
            
            lblInformationHeader.text = String(format: "%.2f", distanceInMile) //"\(totalCoveredDistance)" //"90631"
            pageControl.currentPage = pageControl.currentPage + 1
        }
        else if  pageControl.currentPage == 2 {
            lblInformationHeader.pushTransition(3.0)
            lblInformativeSubTitle.pushTransition(3.0)
            lblInformativeSubTitle.text = "Duration"
            lblInformationHeader.text = timeString(time: TimeInterval(seconds))
            //lblInformationHeader.text =  "\(duration ?? 0.0)"
            pageControl.currentPage = pageControl.currentPage - 2
        }
        else{
            print("page controller out of bound")
        }
        CATransaction.commit()
        
    }
    
    
    @objc func leftSwipe(){
        //self.informativeView.backgroundColor = UIColor.blue
        print("left swipe")
        
        if pageControl.currentPage > 0 || pageControl.currentPage == 0{
            
            if pageControl.currentPage == 0{
                lblInformationHeader.pushTransition(3.0)
                lblInformativeSubTitle.pushTransition(3.0)
                lblInformativeSubTitle.text = "Properties Added"
                lblInformationHeader.text =  "\(noOfTaggedProperty)"
            }else if pageControl.currentPage == 1{
                lblInformationHeader.pushTransition(3.0)
                lblInformativeSubTitle.pushTransition(3.0)
                lblInformativeSubTitle.text = "Miles Driven"
                let distanceInMile = totalCoveredDistance/1609
                print(distanceInMile)
                
                lblInformationHeader.text = String(format: "%.2f", distanceInMile) //lblInformationHeader.text = "\(totalCoveredDistance)" //"90631"
            }
            else if pageControl.currentPage == 2{
                
                lblInformativeSubTitle.text = "Duration"
                lblInformationHeader.text = timeString(time: TimeInterval(seconds))
                //lblInformationHeader.text = "\(duration ?? 0.0)"
            }else{
                print("unknown")
            }
        }
        
        if pageControl.currentPage < 3{
            pageControl.currentPage = pageControl.currentPage + 1
        }
        
    }
    
    @objc func rightSwipe(){
        
        print("right swipe")
        if pageControl.currentPage > 0{
            pageControl.currentPage = pageControl.currentPage - 1
        }
        
        if pageControl.currentPage > 0 || pageControl.currentPage == 0{
            
            if pageControl.currentPage == 0{
                
                lblInformativeSubTitle.text = "Properties Added"
                lblInformationHeader.text = "\(noOfTaggedProperty)"
            }else if pageControl.currentPage == 1{
                lblInformationHeader.pushTransitionForRightSwipe(3.0)
                lblInformativeSubTitle.pushTransitionForRightSwipe(3.0)
                lblInformativeSubTitle.text = "Miles Driven"
                let distanceInMile = totalCoveredDistance/1609
                print(distanceInMile)
                
                lblInformationHeader.text = String(format: "%.2f", distanceInMile) //lblInformationHeader.text = "\(totalCoveredDistance)" //"90631"
            }
            else if pageControl.currentPage == 2{
                lblInformationHeader.pushTransitionForRightSwipe(3.0)
                lblInformativeSubTitle.pushTransitionForRightSwipe(3.0)
                lblInformativeSubTitle.text = "Duration"
                lblInformationHeader.text = timeString(time: TimeInterval(seconds))
                //lblInformationHeader.text = "\(duration ?? 0.0)" //"5378"
            }else{
                print("unknown")
            }
        }
    }
    
    func showDriveNowButton(button: UIButton) {
        button.setTitle("Drive Now", for: .normal)
        button.cornerRadius = button.layer.frame.width/2
        button.titleLabel?.textColor = .white
        button.titleLabel?.font = ProNovaR16
        button.backgroundColor = greenButtonBackground
        
    }
    
    func showStopButton(button: UIButton) {
        button.setTitle("Stop", for: .normal)
        button.cornerRadius = button.layer.frame.width/2
        button.titleLabel?.textColor = .white
        button.titleLabel?.font = ProNovaR16
        button.backgroundColor = greenButtonBackground
    }
    
    
    func showPublicButton(button: UIButton) {
        button.setTitle("Public", for: .normal)
        button.cornerRadius = button.layer.frame.width/2
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = ProNovaR16
        button.backgroundColor = greenButtonBackground
        
    }
    
    
    func showPrivateButton(button: UIButton) {
        button.setTitle("Private", for: .normal)
        button.cornerRadius = button.layer.frame.width/2
        button.titleLabel?.textColor = .white
        button.titleLabel?.font = ProNovaR16
        button.backgroundColor = blakishButtonBackground
    }
    
    
    func showResumeButton(button: UIButton) {
        button.setTitle("Resume", for: .normal)
        button.cornerRadius = button.layer.frame.width/2
        button.borderColor = greenButtonBackground
        button.borderWidth = 1
        button.setTitleColor(greenButtonBackground, for: .normal)
        button.titleLabel?.font = ProNovaR16
        button.backgroundColor = .white
    }
    
    
    func showFinishButton(button: UIButton) {
        button.setTitle("Finish", for: .normal)
        button.cornerRadius = button.layer.frame.width/2
        button.titleLabel?.textColor = .white
        button.titleLabel?.font = ProNovaR16
    }
    
    func showInfoButton(button: UIButton) {
        
        button.backgroundColor = UIColor.white
        button.layer.borderWidth = 0.8
        button.layer.borderColor = UIColor(red:0.93, green:0.93, blue:0.93, alpha:1).cgColor
        button.layer.shadowOffset = CGSize.zero
        button.layer.shadowColor = UIColor.lightGray.cgColor
        button.layer.shadowOpacity = 1
        button.layer.shadowRadius = 4
        
    }
    func showLeftFilterButton(button: UIButton) {
        
        button.backgroundColor = UIColor.white
//        button.layer.borderWidth = 0.8
//        button.layer.borderColor = UIColor(red:0.93, green:0.93, blue:0.93, alpha:1).cgColor
//        button.layer.shadowOffset = CGSize.zero
//        button.layer.shadowColor = UIColor.lightGray.cgColor
//        button.layer.shadowOpacity = 1
//        button.layer.shadowRadius = 4
        
    }
    
    // -----------End: New Design functions -----------------------------
    
    
    func configureViewForTutorialScreen() {
        
        if self.isKeyPresentInUserDefaults(key: AppConfigs.isFirstLogin) == true{
            
            if AppConfigs.getFirstLoginFlag() == true{
                
                print(AppConfigs.getFirstLoginFlag())
                AppConfigs.saveFirstLoginFlag(flag:false)
                let storyBoard = UIStoryboard(name: "Tuts", bundle: nil)
                let vc = storyBoard.instantiateViewController(withIdentifier: "TutsViewController") as! TutsViewController
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
                
            }else{
                print("Not First Login")
                print(AppConfigs.getFirstLoginFlag())
                
                if AppConfigs.getCurrentUserInfo().isAdmin == true && AppConfigs.getCurrentUserInfo().upgradeInfo?.plan?.id == 3 && isUpgradePopUpShowed == false{
                    
                    isUpgradePopUpShowed = true
                    let storyBoard = UIStoryboard(name: "Settings", bundle: nil)
                    let vc = storyBoard.instantiateViewController(withIdentifier: "UpgradePopUpTableViewController") as! UpgradePopUpTableViewController
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true, completion: nil)
                }
            }
            
        }else{
            //key doesn't exist
            AppConfigs.saveFirstLoginFlag(flag:true)
            let storyBoard = UIStoryboard(name: "Tuts", bundle: Bundle.main)
            guard let vc = storyBoard.instantiateViewController(withIdentifier: "TutsViewController") as? TutsViewController  else
            { return  }
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
            AppConfigs.saveFirstLoginFlag(flag:false)
        }
    }
    
    
    func setUpLocationManager()  {
        
        searchForLocation()
        
        
        if( CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() ==  .authorizedAlways){
            
            if currentLocation != nil{
                currentLocation = locationManager.location
                print("latitude----", currentLocation.coordinate.latitude)
                print("longitude----", currentLocation.coordinate.longitude)
                //to do
                //29.709151, -95.456857
                
                let camera = GMSCameraPosition.camera(withTarget: CLLocationCoordinate2D(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude), zoom: 17, bearing: 0, viewingAngle: 0)
                
                mapView.animate(to: camera)
//                CATransaction.begin()
//                CATransaction.setAnimationDuration(3.0)
//                mapView.animate(toLocation: CLLocationCoordinate2D(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude))
//                mapView.animate(toZoom: 17)
//                CATransaction.commit()
            }else{
                
                if myLastLocationGlobal != nil{

                    let camera = GMSCameraPosition.camera(withTarget: CLLocationCoordinate2D(latitude: (myLastLocationGlobal?.coordinate.latitude)!, longitude: (myLastLocationGlobal?.coordinate.latitude)!), zoom: 17, bearing: 0, viewingAngle: 0)
                    
                    mapView.animate(to: camera)
                }else{

                    let camera = GMSCameraPosition.camera(withTarget: CLLocationCoordinate2D(latitude: myLastLocationGlobal?.coordinate.latitude ?? USA_LAT, longitude: myLastLocationGlobal?.coordinate.latitude ?? USA_LONG), zoom: 17, bearing: 0, viewingAngle: 0)
                    
                    mapView.animate(to: camera)
                }
//
//                let camera = GMSCameraPosition.camera(withTarget: CLLocationCoordinate2D(latitude: myLastLocationGlobal?.coordinate.latitude ?? USA_LAT, longitude: myLastLocationGlobal?.coordinate.latitude ?? USA_LONG), zoom: 17, bearing: 0, viewingAngle: 0)
//
//                mapView.animate(to: camera)
//                CATransaction.begin()
//                CATransaction.setAnimationDuration(3.0)
//                mapView.animate(toLocation: CLLocationCoordinate2D(latitude: myLastLocationGlobal?.coordinate.latitude ?? USA_LAT, longitude: myLastLocationGlobal?.coordinate.latitude ?? USA_LONG))
//                mapView.animate(toZoom: 17)
//                CATransaction.commit()
            }
        }else{
            let camera = GMSCameraPosition.camera(withTarget: CLLocationCoordinate2D(latitude: USA_LAT, longitude: USA_LONG), zoom: 17, bearing: 0, viewingAngle: 0)
            
            mapView.animate(to: camera)
//            CATransaction.begin()
//            CATransaction.setAnimationDuration(3.0)
//            mapView.animate(toLocation: CLLocationCoordinate2D(latitude: USA_LAT, longitude: USA_LONG))
//            mapView.animate(toZoom: 17)
//            CATransaction.commit()
        }
        
        if self.locationManager.location != nil{
            self.zoomToCoordinates(CLLocationCoordinate2D(latitude: (self.locationManager.location?.coordinate.latitude)!, longitude: (self.locationManager.location?.coordinate.longitude)!))
        }else{
            
            AppConfigs.showSnacbar(message: "Please check your GPS service", textColor: .red)
        }
    }
    
    func searchForLocation() {
        if CLLocationManager.locationServicesEnabled() {
            self.mapView?.isMyLocationEnabled = true
            
            locationManager.desiredAccuracy = kCLLocationAccuracyBest // kCLLocationAccuracyBest
            locationManager.activityType = CLActivityType.automotiveNavigation
            //locationManager.distanceFilter = 1
            locationManager.headingFilter = 1
            self.locationManager.delegate = self
            self.locationManager.requestWhenInUseAuthorization()
            locationManager.requestAlwaysAuthorization()
            self.locationManager.startUpdatingLocation()
            self.locationManager.allowsBackgroundLocationUpdates = true
            //self.locationManager.startUpdatingHeading()
            
        }else{
            //location is not enabled
            AppConfigs.showSnacbar(message: "Location is not enabled. Please enable your location service", textColor: .red)
            
        }
    }
    
    func runTimer() {
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(HomeViewController.updateTimer)), userInfo: nil, repeats: true)
        //timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(ViewController.updateTimer)), userInfo: nil, repeats: true)
        isTimerRunning = true
    }
    
    @objc func updateTimer() {
        
        if isResumeGlobal  == true{
            seconds += 1
        }else{
            if seconds < 0 {
                timer.invalidate()
                seconds = 0
                //Send alert to indicate "time's up!"
            } else {
                seconds += 1     //This will decrement(count down)the seconds.
                //lblInformationHeader.text = timeString(time: TimeInterval(seconds)) //"\(seconds)" //This will update the label.
            }
        }
        
        print("seconds", seconds)
        //todooffLine
        //        let distanceInMile = totalCoveredDistance/1609
        //        print(distanceInMile)
        //
        //        let tilesData = TilesData()
        //        tilesData.seconds = seconds
        //        tilesData.noOfPropertiesAddred = globalListedPropertiesInCurrentDrive.count
        //        tilesData.distanceInMile = distanceInMile
        //
        //        if seconds % 3 == 0{
        //
        //            AppConfigs.saveTilesInfo(user: tilesData)
        //            print("AppConfigs.getSavedTilesInfo().seconds", AppConfigs.getSavedTilesInfo().seconds)
        //            print("AppConfigs.getSavedTilesInfo().noOfPropertiesAddred", AppConfigs.getSavedTilesInfo().noOfPropertiesAddred)
        //            print("AppConfigs.getSavedTilesInfo().distanceInMile", AppConfigs.getSavedTilesInfo().distanceInMile)
        //
        //
        //        }
        
    }
    
    func showMapUsingLocation(){
        if( CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() ==  .authorizedAlways){
            
            currentLocation = locationManager.location
            print("latitude----", currentLocation.coordinate.latitude)
            print("longitude----", currentLocation.coordinate.longitude)
            //to do
            //29.709151, -95.456857
            
            let camera = GMSCameraPosition.camera(withTarget: CLLocationCoordinate2D(latitude: USA_LAT, longitude: USA_LONG), zoom: 17, bearing: 0, viewingAngle: 0)
            
            mapView.animate(to: camera)
            CATransaction.begin()
            CATransaction.setAnimationDuration(3.0)
            mapView.animate(toLocation: CLLocationCoordinate2D(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude))
            mapView.animate(toZoom: 17)
            CATransaction.commit()
            
            
        }else{
            let camera = GMSCameraPosition.camera(withTarget: CLLocationCoordinate2D(latitude: USA_LAT, longitude: USA_LONG), zoom: 17, bearing: 0, viewingAngle: 0)
            
            mapView.animate(to: camera)
            CATransaction.begin()
            CATransaction.setAnimationDuration(3.0)
            mapView.animate(toLocation: CLLocationCoordinate2D(latitude: USA_LAT, longitude: USA_LONG))
            mapView.animate(toZoom: 17)
            CATransaction.commit()
        }
    }
    
    func timeString(time:TimeInterval) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
    }
    
    func pauseButtonTapped() {
        if resumeTapped == false {
            timer.invalidate()
            resumeTapped = true
            isTimerRunning = false
        } else {
            runTimer()
            resumeTapped = false
        }
    }
    
    func resetButtonTapped() {
        timer.invalidate()
        seconds = 0
        //lblInformationHeader.text = timeString(time: TimeInterval(seconds))
        isTimerRunning = false
    }
    
    func startButtonTapped() {
        if isTimerRunning == false {
            runTimer()
        }
    }
    
    func finishButtonTapped() {
        timer.invalidate()
        seconds = 0
        //lblInformationHeader.text = timeString(time: TimeInterval(seconds))
        isTimerRunning = false
    }
    
    func setUpView() {
        
        self.mapTypeView.isHidden = true
        //current location button code
        let CenterCurrenetLocationButton = UIButton(frame: CGRect(x: mapView.frame.width - 70, y: 10, width: 70, height: 70))
        CenterCurrenetLocationButton.setImage(UIImage(named: "ic_get_current_location"), for: .normal)
        CenterCurrenetLocationButton.adjustsImageSizeForAccessibilityContentSizeCategory = true
        CenterCurrenetLocationButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        self.mapView.addSubview(CenterCurrenetLocationButton)
        self.actionButton.isHidden = true
        
    }
    
    
    //Image function
    func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    
    @objc func UpgradeClickedAction(notification: Notification){
        
        if AppConfigs.getCurrentUserInfo().isAdmin == false{
            AppConfigs.showSnacbar(message: "You don't have permission to do this action", textColor: .blue)
        }else{

            let settingsSB: UIStoryboard = UIStoryboard(name: "Settings", bundle: nil)
            let vc = settingsSB.instantiateViewController(withIdentifier: "PackageViewController") as! PackageViewController
            //vc.modalPresentationStyle = .fullScreen
            self.navigationController?.pushViewController(vc, animated: true)
            //self.present(vc, animated: true, completion: nil)

//            let alertController = UIAlertController(title: "Please go to web to manage your account.", message: "", preferredStyle: .alert)
//            let OKAction = UIAlertAction(title: "Ok", style: .default) { (alert) in
//
//
//            }
//            alertController.addAction(OKAction)
//            self.present(alertController, animated: true, completion: nil)
        }
    
//            let settingsSB: UIStoryboard = UIStoryboard(name: "Settings", bundle: nil)
//            let vc = settingsSB.instantiateViewController(withIdentifier: "PackageViewController") as! PackageViewController
//            //vc.modalPresentationStyle = .fullScreen
//            self.navigationController?.pushViewController(vc, animated: true)
//    self.present(vc, animated: true, completion: nil)
}

@objc func updateUserLocationInSocket(notification: Notification){
    if self.locationManager.location != nil {
        //SocketIOManager.shared.updateCurrentLocation(myLocation: self.locationManager.location!, driveType: "currentLocation")
    }
    
}
//MARK: - - - - - Method for receiving Data through   Notificaiton - - - - -
@objc func methodForDrivingFlag(notification: Notification) {
    print("Value of notification : ", notification.object ?? "")
    
    //NotificationCenter.default.post(name:NSNotification.Name("socketConnectionStatus"),object: nil)
    if isGlobalDriveNowSelected == false{
        locationManager.pausesLocationUpdatesAutomatically = true
        locationManager.allowsBackgroundLocationUpdates = false
        
    }else{
        locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.allowsBackgroundLocationUpdates = true
    }
}

@objc func buttonAction(sender: UIButton!) {
    print("Button tapped")
    searchForLocation()
    if myLastLocation != nil{
        
        self.zoomToCoordinates(myLastLocation!.coordinate)
        self.locationManager.startUpdatingLocation()
    }else{
        AppConfigs.showSnacbar(message: "Location is not available", textColor: .red)
    }
}

}
