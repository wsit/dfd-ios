//
//  HomeVC + GeoFencing.swift
//  HouZes
//
//  Created by Mahadhi Hassan Chowdhury on 4/15/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

import Alamofire
import SwiftyJSON

var globalAllTagNames : [String] = []
var globalAllColorCodes : [String] = []
//var globalAllTags: [PropertyTag] = []
var jsonTagResponse: ListOfTagsResponse?

extension HomeViewController{
    
    func createNewRegion(){
        if isGlobalDriveNowSelected == true{
            for geotification in geotifications{
                remove(geotification)
                removeRadiusOverlay(forGeotification: geotification)
            }
            
            isRegionExist = false
            if isRegionExist == false{
                self.addGeotificationOnMapView(didAddCoordinate: CLLocationCoordinate2D(latitude: (myLastLocationGlobal?.coordinate.latitude)!, longitude: (myLastLocationGlobal?.coordinate.longitude)!), radius: 3200.0, identifier: "Sunny", note: "Sunny", eventType: .onEntry)
                isRegionExist = true
            }else{
                
            }
        }
    }
    
    // MARK: Loading and saving functions
    func loadAllGeotifications() {
        geotifications.removeAll()
        let allGeotifications = Geotification.allGeotifications()
        print(allGeotifications.count)
        allGeotifications.forEach { add($0) }
    }
    
    func add(_ geotification: Geotification) {
        geotifications.append(geotification)
        //mapView.addAnnotation(geotification)
        //addRadiusOverlay(forGeotification: geotification)
        updateGeotificationsCount()
    }
    
    // MARK: Map overlay functions
    func addRadiusOverlay(forGeotification geotification: Geotification) {
        //mapView?.add(MKCircle(center: geotification.coordinate, radius: geotification.radius))
        let cirlce: GMSCircle = GMSCircle(position: geotification.coordinate, radius: geotification.radius)
        cirlce.fillColor = UIColor(red: 0, green: 0, blue: 0.3, alpha: 0.2)
        cirlce.strokeColor = .blue
        cirlce.strokeWidth = 2
        cirlce.map = self.mapView
    }
    
    func updateGeotificationsCount() {
        print( "Geotifications: \(geotifications.count)")
    }
    
    func remove(_ geotification: Geotification) {
        
        guard let index = geotifications.index(of: geotification) else { return }
        geotifications.remove(at: index)
        //mapView.removeAnnotation(geotification)
        removeRadiusOverlay(forGeotification: geotification)
        updateGeotificationsCount()
        
    }
    
    
    func removeRadiusOverlay(forGeotification geotification: Geotification) {
        // Find exactly one overlay which has the same coordinates & radius to remove
        for overlay in circularOverlays{
            
            let coord = overlay.position
            if coord.latitude == geotification.coordinate.latitude
                && coord.longitude == geotification.coordinate.longitude
                && overlay.radius == geotification.radius
            {
                overlay.map = nil
                break
            }
        }
    }
    
    
    func region(with geotification: Geotification) -> CLCircularRegion {
        
        let region = CLCircularRegion(center: geotification.coordinate, radius: geotification.radius, identifier: geotification.identifier)
        region.notifyOnEntry = true// (geotification.eventType == .onEntry)
        region.notifyOnExit = true //!region.notifyOnEntry
        
        return region
    }
    
    func startMonitoring(geotification: Geotification) {
        if !CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
            showAlert(withTitle:"Error", message: "Geofencing is not supported on this device!")
            return
        }
        
//        if CLLocationManager.authorizationStatus() != .authorizedAlways {
//            let message = "Please turn on location service to 'Always' to show nearby HouZes"
//            //showAlert(withTitle:"Warning", message: message)
////            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
////            let action = UIAlertAction(title: "OK", style: .cancel,comple
////            alert.addAction(action)
////            present(alert, animated: true, completion: nil)
//
//            let alertController = UIAlertController(title: message, message: "", preferredStyle: .alert)
//            let OKAction = UIAlertAction(title: "Ok", style: .default) { (alert) in
//                if let appSettings = URL(string: UIApplication.openSettingsURLString) {
//                    UIApplication.shared.open(appSettings, options: [:], completionHandler: nil)
//                }
//            }
//            alertController.addAction(OKAction)
//            self.present(alertController, animated: true, completion: nil)
//        }
//
        let fenceRegion = region(with: geotification)
        locationManager.startMonitoring(for: fenceRegion)
    }
    
    func stopMonitoring(geotification: Geotification) {
        for region in locationManager.monitoredRegions {
            guard let circularRegion = region as? CLCircularRegion, circularRegion.identifier == geotification.identifier else { continue }
            locationManager.stopMonitoring(for: circularRegion)
        }
    }
    
    func addGeotificationOnMapView(didAddCoordinate coordinate: CLLocationCoordinate2D, radius: Double, identifier: String, note: String, eventType: Geotification.EventType) {
        
        let clampedRadius = min(radius, locationManager.maximumRegionMonitoringDistance)
        let geotification = Geotification(coordinate: coordinate, radius: clampedRadius, identifier: identifier, note: note, eventType: eventType)
        add(geotification)
        startMonitoring(geotification: geotification)
        saveAllGeotifications()
    }
    
    
    func saveAllGeotifications() {
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(geotifications)
            UserDefaults.standard.set(data, forKey: PreferencesKeys.savedItems)
        } catch {
            print("error encoding geotifications")
        }
    }
    
//    func handleEvent(for region: CLRegion!) {
//        // Show an alert if application is active
//        if UIApplication.shared.applicationState == .active {
//            guard let message = note(from: region.identifier) else { return }
//            self.showAlert(withTitle: nil, message: message)
//        } else {
//            // Otherwise present a local notification
//            guard let body = note(from: region.identifier) else { return }
//            let notificationContent = UNMutableNotificationContent()
//            notificationContent.body = body
//            notificationContent.sound = UNNotificationSound.default
//            notificationContent.badge = UIApplication.shared.applicationIconBadgeNumber + 1 as NSNumber
//            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
//            let request = UNNotificationRequest(identifier: "location_change",
//                                                content: notificationContent,
//                                                trigger: trigger)
//            UNUserNotificationCenter.current().add(request) { error in
//                if let error = error {
//                    print("Error: \(error)")
//                }
//            }
//        }
//    }
    
    func note(from identifier: String) -> String? {
        let geotifications = Geotification.allGeotifications()
        guard let matched = geotifications.filter({
            $0.identifier == identifier
        }).first else { return nil }
        return matched.note
    }
    
    
    func getAllTags(urlString: String) {
        
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                // print("success",data)
                let statusCode = response.response?.statusCode
                
                if statusCode == 200{
                    
                    let tagsResponse = try? JSONDecoder().decode(ListOfTagsResponse.self, from: response.data!)
                    print(tagsResponse)
                    globalAllTags = tagsResponse!
                    DispatchQueue.main.async {
                       // print("globalAllTags------------\n", globalAllTags)
                        self.removeSpinner()//self.view.activityStopAnimating()
                        //  self.tagTV.reloadData()
                        for tag in globalAllTags{
                            let name = tag.name
                            let colorCode = tag.color?.colorCode
                            globalAllTagNames.append(name!)
                            globalAllColorCodes.append(colorCode!)
                        }
                        
                    }
                }
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
//                AppConfigs.showSnacbar(message: error.localizedDescription, textColor: .red)
//                let statusCode = response.response?.statusCode
//                self.showMessageForServerError(code: statusCode ?? 500)
//
                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
}

extension UIViewController {
    func showAlert(withTitle title: String?, message: String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
        //if let url = NSURL(string: UIApplicationOpenSettingsURLString) as URL? {
//            UIApplication.shared.openURL(url)
//        }
    }
}
