//
//  LoadPropertyViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 8/7/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit
import DropDown
import Alamofire


var isFilteredList: Bool = false
var isFilteredTag: Bool = false
//
var isListSelected: Bool = false
var isMapSelected: Bool = false


class LoadHouzesContainerViewController: UIViewController, ListSelectionDelegate, TagSelectionDelegate {
    
    func getSelectedTag(tagID: Int) {
        print("tagID--->",tagID)
        if globalSelectedTag != nil{
            self.btnDropdown.setTitle(globalSelectedTag?.name, for: .normal)
        }
    }
    
    func getSelectedList(listID: Int) {
        print("listID--->",listID)
        if globalSelectedList != nil{
            self.btnDropdown.setTitle(globalSelectedList?.name, for: .normal)
        }
        
        if AppConfigs.getCurrentUserInfo().isAdmin == true{

            self.btnFilter.isEnabled = true
            self.btnFilter.image =  UIImage(named: "new_ic_team_filter")
            
            self.btnAssignList.isEnabled = true
            self.btnAssignList.image = UIImage(named: "ic_add_list_to_member")
            
        }else{
            //self.btnFilter.isHidden = false
            self.btnFilter.isEnabled = false
            self.btnFilter.image = nil
            
            self.btnAssignList.isEnabled = false
            self.btnAssignList.image = nil
            
        }
        
    }
    
    
    @IBOutlet weak var mySegmentedControl: UISegmentedControl!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var btnSwitchView: UIButton!
    @IBOutlet weak var btnAssignList: UIBarButtonItem!
    @IBOutlet weak var btnDropdown: UIButton!
    @IBOutlet weak var btnFilter: UIBarButtonItem!
    
    var listTitleArray : [String] = []
    var listIDArray : [Int] = []
    
    var tagNameArray : [String] = []
    var tagIDArray : [Int] = []
    
    var selectedProperty : [String] = []
    var couter: Int = 0
    var indexArray : [IndexPath] = []
    
    var dropDown = DropDown()
    
    let vcArray = ["PropertiesViewController","PropertiesMapViewController"]
    var dropDownFlag: String = ""
    var selectedlistID: Int = 0
    var allList: [MList] = []
    var url : String = ""
    var nextUrl: String = ""
    var previousUrl: String = ""
    var jsonResponse: AllListsResponseForDropdownElement?
    //var allListArray : [AllListsResponseForDropdown]
    var selectedList : MList?
    
    var allTags: [PropertyTag] = []
    var jsonTagResponse: ListOfTagsResponse?
    //var allListArray : [AllListsResponseForDropdown]
    var selectedTag : PropertyTag?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnSwitchView.setImage(UIImage(named: "world")!.withRenderingMode(.alwaysTemplate), for: .normal)
        //btnSwitchView.backgroundColor = UIColor(red:0, green:0.84, blue:0.37, alpha:1)
        btnSwitchView.isHidden = true
        
        self.view.backgroundColor = .white//UIColor(red:0.09, green:0.15, blue:0.22, alpha:1)
        self.btnAssignList.isEnabled = false
        self.btnAssignList.image = nil
        
        self.setUpNavigationBar()
        FontAndColorConfigs.setDynamicButtonsFontSize(buttons: [btnDropdown], type: TextType.ListTitle.rawValue)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if dropDownFlag == "Properties by Tag"{
            
            self.btnFilter.isEnabled = false
            self.btnFilter.image = nil
            
            btnDropdown.setTitle("Choose a Tag", for: .normal)
            btnDropdown.setImage(UIImage(named: "ic_dropdown"), for: .normal)
            self.title = "Property by Tag"
            
            if allTags.count > 0 {
                //btnAssignMember.isHidden = false
            }else{
                
                if Network.reachability.status == .unreachable{
                    AppConfigs.updateUserInterface(view: view)
                }else{

                    allTags.removeAll()
                    let url : String = UrlManager.baseURL() + UrlManager.apiString() + "/tag/"
                    
                    print(url)
                    //self.showSpinner(onView: view)
                    getAllTags(urlString: url)
                    
                }//                }
            }
            
            if AppConfigs.getCurrentUserInfo().isAdmin == true{

                self.btnFilter.isEnabled = true
                self.btnFilter.image =  UIImage(named: "new_ic_team_filter")
            }else{
                //self.btnFilter.isHidden = false
                self.btnFilter.isEnabled = false
                self.btnFilter.image = nil
            }
            
        }else{
            //properties by list clicked
            btnDropdown.setTitle("Choose a List", for: .normal)
            btnDropdown.setImage(UIImage(named: "ic_dropdown"), for: .normal)
            self.title = "Property by List"
            
            if isFilteredList == false{
                if allList.count > 0 {
                    print("list exists")
                }else{

                    if Network.reachability.status == .unreachable{
                        AppConfigs.updateUserInterface(view: view)
                    }else{

                        if isFilteredList == false{
                            
                            allList.removeAll()
                            let url : String = UrlManager.baseURL() + UrlManager.apiString() + "/list/"
                            
                            print(url)
                            //self.showSpinner(onView: view)
                            getAllLists(urlString: url)
                        }else{
                            loadListFromFilters()
                        }
                    }
                    //                }
                }
            }else{
                
                loadListFromFilters()
                
            }
            //if AppConfigs.getCurrentUserInfo().upgradeInfo?.isTeamInvitable == true{
            if AppConfigs.getCurrentUserInfo().isAdmin == true{

                self.btnFilter.isEnabled = true
                self.btnFilter.image =  UIImage(named: "new_ic_team_filter")
                
                self.btnAssignList.isEnabled = true
                self.btnAssignList.image = UIImage(named: "ic_add_list_to_member")
                
            }else{
                //self.btnFilter.isHidden = false
                self.btnFilter.isEnabled = false
                self.btnFilter.image = nil
                
                self.btnAssignList.isEnabled = false
                self.btnAssignList.image = nil
            }
        }
        
        self.loadViewController(id: vcArray[0])
        
        btnSwitchView.setImage(UIImage(named: "world")!.withRenderingMode(.alwaysTemplate), for: .normal)
        isListSelected = true
        isMapSelected = false
    }
    
    @IBAction func dropdownButtonClicked(_ sender: Any) {
        
        dropDown.show()
    }
    
    
    @IBAction func changeViewAction(_ sender: Any) {
        
        if btnSwitchView.currentImage == UIImage(named: "world"){
            btnSwitchView.setImage(UIImage(named: "list")!.withRenderingMode(.alwaysTemplate), for: .normal)
           
            isListSelected = false
            isMapSelected = true
            
            //btnSwitchView.backgroundColor = UIColor(red:0, green:0.84, blue:0.37, alpha:1)
            self.loadViewController(id: self.vcArray[1])
            btnSwitchView.tintColor = UIColor.white

            

        }else{
            
            btnSwitchView.setImage(UIImage(named: "world")!.withRenderingMode(.alwaysTemplate), for: .normal)
            
            isListSelected = true
            isMapSelected = false
            
            self.loadViewController(id: self.vcArray[0])
        }
        //btnSwitchView.backgroundColor = UIColor(red:0, green:0.84, blue:0.37, alpha:1)
        btnSwitchView.tintColor = UIColor.white
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func filterButtonClicked(_ sender: Any) {
        
        
        if AppConfigs.getCurrentUserInfo().upgradeInfo?.isTeamInvitable == true{
            
            isFilteredList = true
            let storyBoard : UIStoryboard = UIStoryboard(name: "MyStory", bundle:nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "SelecMemberViewController") as! SelecMemberViewController
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }else{
            self.showAlert(message: "You don't have permission to add Team member")
        }
        
        
        
    }
    
    @IBAction func addToMemberButtonClicked(_ sender: Any) {
        print("selectedlistID-->", selectedlistID)
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "LoadHouzes", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "AssignListToTeamMemberViewController") as! AssignListToTeamMemberViewController
        self.present(vc, animated: true, completion: nil)
    }
     
    
    func loadListFromFilters() {
        

        if Network.reachability.status == .unreachable{
            AppConfigs.updateUserInterface(view: view)
        }else{


            isFilteredList = false
            
            print(globalSelectedMembersID)
            let joined = globalSelectedMembersID.map { String($0) }.joined(separator: ",")
            print(joined)
            
            allList.removeAll()
            let url : String = UrlManager.baseURL() + UrlManager.apiString() +  "/list/filter/?members=\(joined)"
            //http://192.168.1.22:8000/api/property/filter/?members=5,3&tags=8&list=14&page=1&limit=10
            
            print(url)
            //self.showSpinner(onView: view)
            getAllLists(urlString: url)
        }
    }
    
    func loadViewController(id: String) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "LoadHouzes", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: id)
        self.addChild(vc)
        
        if id == "PropertiesViewController"{
            let storyBoard : UIStoryboard = UIStoryboard(name: "LoadHouzes", bundle:nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: id) as! PropertiesViewController
            vc.dropDownFlag = self.dropDownFlag
            vc.listDelegate = self
            vc.tagDelegae = self
            self.addChild(vc)
            vc.view.frame = CGRect(x: 0, y: 0, width: self.containerView.frame.size.width, height: self.containerView.frame.size.height)
            self.containerView.addSubview(vc.view)
        }
        else{
            let storyBoard : UIStoryboard = UIStoryboard(name: "LoadHouzes", bundle:nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: id) as! PropertiesMapViewController
            vc.dropDownFlag = self.dropDownFlag
            vc.allPropertyList = globalPropertyList
            
            self.addChild(vc)
            vc.view.frame = CGRect(x: 0, y: 0, width: self.containerView.frame.size.width, height: self.containerView.frame.size.height)
            self.containerView.addSubview(vc.view)
        }
        
    }
    
    func setUpDropDownOfTag(tag: Array<String>) {
        
        dropDown.anchorView = btnDropdown
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y: btnDropdown.bounds.height)
        dropDown.dataSource = tag
        dropDown.width = self.btnDropdown.frame.width
        dropDown.textFont = FontAndColorConfigs.getProximaRegular16()
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.btnDropdown.setTitle(item, for: .normal)
            self.selectedTag = self.allTags[index]
            globalSelectedTag = self.selectedTag
            print("self.selectedList?.name---->", (self.selectedTag?.name)!)
            
            self.btnSwitchView.isHidden = false
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "tagSelected"), object: nil)
            
        }
        
    }
    
    func setUpDropDownList(list: Array<String>) {
        
        dropDown.anchorView = btnDropdown
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y: btnDropdown.bounds.height)
        dropDown.dataSource = list
        dropDown.width = self.btnDropdown.frame.width
        dropDown.textFont = FontAndColorConfigs.getProximaRegular16()
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.btnDropdown.setTitle(item, for: .normal)
            self.selectedList = self.allList[index]
            globalSelectedList = self.selectedList
            print("self.selectedList?.name---->", (self.selectedList?.name)!)
            
//            self.btnAssignList.isEnabled = true
//            self.btnAssignList.image = UIImage(named: "ic_add_list_to_member")
            if AppConfigs.getCurrentUserInfo().isAdmin == true{

                self.btnFilter.isEnabled = true
                self.btnFilter.image =  UIImage(named: "new_ic_team_filter")
                
                self.btnAssignList.isEnabled = true
                self.btnAssignList.image = UIImage(named: "ic_add_list_to_member")
                
            }else{
                //self.btnFilter.isHidden = false
                self.btnFilter.isEnabled = false
                self.btnFilter.image = nil
                
                self.btnAssignList.isEnabled = false
                self.btnAssignList.image = nil
                
            }
            
            self.btnSwitchView.isHidden = false
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "listSelected"), object: nil)
            
        }
        
    }
    
    
    func getAllLists(urlString: String) {
        
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success response",data)
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    
                    let allListsResponseForDropdown = try? JSONDecoder().decode(AllListsResponseForDropdown.self, from: response.data!)
                    self.allList = allListsResponseForDropdown!
                    
                    DispatchQueue.main.async {
                        print("allProjects------------\n",self.allList)
                        self.removeSpinner()
                        
                        self.listTitleArray.removeAll()
                        self.listIDArray.removeAll()
                        
                        for list in self.allList{
                            let name = list.name
                            let id = list.id
                            print(name!, id!)
                            self.listTitleArray.append(name!)
                            self.listIDArray.append(id!)
                        }
                        print(self.listTitleArray)
                        self.setUpDropDownList(list: self.listTitleArray)
                    }
                }
                    
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                   // AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)

                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
    func getAllTags(urlString: String) {
        
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                self.removeSpinner()
                print("success",data)
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    let tagsResponse = try? JSONDecoder().decode(ListOfTagsResponse.self, from: response.data!)
                    print(tagsResponse)
                    self.allTags = tagsResponse!
                    
                    if tagsResponse!.count > 0{
                        self.tagNameArray.removeAll()
                        self.tagIDArray.removeAll()
                        for tag in tagsResponse!{
                            let name = tag.name
                            let id = tag.id
                            print(name!, id!)
                            self.tagNameArray.append(name!)
                            self.tagIDArray.append(id!)
                        }
                        print(self.tagNameArray)
                    }
                    DispatchQueue.main.async {
                        //let data = [allTagsResponse]
                        self.setUpDropDownOfTag(tag: self.tagNameArray)
                    }
                }
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                   // AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                self.removeSpinner()
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)

                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
}
