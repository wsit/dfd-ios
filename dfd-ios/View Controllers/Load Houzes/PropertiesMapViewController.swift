//
//  PropertiesMapViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 5/28/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit
import GoogleMaps
import SnapKit
import TTGSnackbar
import Floaty
import JJFloatingActionButton
import DropDown
import Alamofire
import SwiftyJSON


/// Point of Interest Item which implements the GMUClusterItem protocol.
class POIItem: NSObject, GMUClusterItem {
    var position: CLLocationCoordinate2D
    var name: String!
    var   icon: UIImage?
    var propertyTags: [PropertyTag]?
    
    init(position: CLLocationCoordinate2D, name: String, icon: UIImage?, propertyTags: [PropertyTag]?) {
        self.position = position
        self.name = name
        self.icon = icon
        self.propertyTags = propertyTags
    }
}


class PropertiesMapViewController: UIViewController {
    
    //@IBOutlet weak var dropDownButtonForMap: UIButton!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var mapTypeView: UIView!
    @IBOutlet weak var btnNormalView: UIButton!
    @IBOutlet weak var btnSatelliteView: UIButton!
    
    var dropDownFlag: String = ""
    var currentLocation: CLLocation!
    let marker = GMSMarker()
    var locationManager = CLLocationManager()
    var DriveNowButton = UIButton()
    let floatingButton = UIButton()
    var floaty = Floaty()
    var currentLocationMarker: GMSMarker?
    let actionButton = JJFloatingActionButton()
    let geocoder = GMSGeocoder()
    var infoWindow = UIView(frame: CGRect.init(x: 0, y: 0, width: 250, height: 80))
    var tappedMarker = GMSMarker()
    
    
    var tagItems = ["Tag 1", "Tag 2", "Tag 3", "Tag 4", "Tag 5", "Tag 6"]
    var listItems = ["List 1", "List 2", "List 3", "List 4", "List 5", "List 6"]
    
    let dropDown = DropDown()
    
    var allList: [MList] = []
    var url : String = ""
    var nextUrl: String = ""
    var previousUrl: String = ""
    var jsonResponse: AllListsResponseForDropdownElement?
    //var allListArray : [AllListsResponseForDropdown]
    var selectedList : MList?
    
    var allPropertyList: [ListedProperty] = []
    var urlForProperty : String = ""
    var nextUrlForProperty : String = ""
    var previousUrlForProperty : String = ""
    var jsonResponseForProperty : ListOfPropertyResponse?
    var jsonResponseForPropertyCluster : ListClusterResponse?
    var jsonResponseForTagCluster : TagClusterResponse?
    
    var allPropertyListForCluster: [ListClusterResponseElement] = []
    var allPropertyListForTagCluster: [TagClusterResponseElement] = []
    
    
    var allTags: [PropertyTag] = []
    var jsonTagResponse: ListOfTagsResponse?
    //var allListArray : [AllListsResponseForDropdown]
    var selectedTag : PropertyTag?
    var selectedTagID: [Int] = []
    var allTagNames : [String] = []
    var allColorCodes : [String] = []
    
    var bounds = GMSCoordinateBounds()
    
    var clusterManager: GMUClusterManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.loadPropertyList), name: NSNotification.Name(rawValue: "listSelected"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.loadPropertyTag), name: NSNotification.Name(rawValue: "tagSelected"), object: nil)
        
        self.title = dropDownFlag
        print(dropDownFlag)
        if dropDownFlag == "Properties by Tag"{
            
        }else{
            
        }
        
        self.addFloatyButton()
        setUpView()
        mapView.delegate = self
        mapView.mapType = .satellite
        //initialize location manager
        //self.mapView?.isMyLocationEnabled = true
        //Location Manager code to fetch current location
        //        self.locationManager.delegate = self
        //        self.locationManager.requestWhenInUseAuthorization()
        //        self.locationManager.startUpdatingLocation()
        self.navigationController?.navigationBar.prefersLargeTitles = false
    }
    
    @objc func loadPropertyList(notif: NSNotification) {
        
        if isMapSelected == true{
            self.allPropertyList.removeAll()
            self.getAllPropertyByList(id: (globalSelectedList?.id)!)
            
        }else{
            print("List view on")
        }
        
        
    }
    
    @objc func loadPropertyTag(notif: NSNotification) {
        
        
        if isMapSelected == true{
            
            self.allPropertyList.removeAll()
            self.getAllPropertyByTag(id: (globalSelectedTag?.id)!)
            
        }else{
            print("List view on")
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //setupNavforDefaultMenu()
        setUpConstraint()
        self.setUpNavigationBar()
        self.mapTypeView.isHidden = true
        
        allTags.removeAll()
        let url : String = UrlManager.baseURL() + UrlManager.apiString() + "/tag/"
        print(url)
        getAllTags(urlString: url)
        
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.mapView.clear()
    }
    
    /// Returns a random value between -1.0 and 1.0.
    private func randomScale() -> Double {
        return Double(arc4random()) / Double(UINT32_MAX) * 2.0 - 1.0
    }
    
    func setUpView() {
        
        //current location button code
        let CenterCurrenetLocationButton = UIButton(frame: CGRect(x: mapView.frame.width - 100, y: 10, width: 70, height: 70))
        CenterCurrenetLocationButton.setImage(UIImage(named: "ic_get_current_location"), for: .normal)
        
        CenterCurrenetLocationButton.adjustsImageSizeForAccessibilityContentSizeCategory = true
        CenterCurrenetLocationButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        // self.mapView.addSubview(CenterCurrenetLocationButton)
        
        //drive now button code
        DriveNowButton.backgroundColor = .white
        DriveNowButton.setTitle("DRIVE NOW", for: .normal)
        DriveNowButton.cornerRadius = 5
        DriveNowButton.setImage(UIImage(named: "broadcast"), for: .normal)
        DriveNowButton.titleLabel?.font = UIFont(name: "ProximaNova-Regular", size: 14)
        DriveNowButton.setTitleColor(UIColor(red:0.44, green:0.5, blue:0.58, alpha:1), for: .normal)
        DriveNowButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10);
        DriveNowButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0);
        DriveNowButton.adjustsImageSizeForAccessibilityContentSizeCategory = true
        DriveNowButton.addTarget(self, action: #selector(driveNowButtonAction), for: .touchUpInside)
        
        self.actionButton.isHidden = true
        
    }
    
    func setUpConstraint(){
        
        
    }
    
    
    func getAllPropertyByList(id:Int) {
        //self.showSpinner(onView: view)
        let urlString = UrlManager.baseURL() + UrlManager.apiString() + "/list/\(id)/property-cluster/"//"/property/list/\(id)/" //?limit=10&offset=0"
        print(urlString)
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                //print("success",data)
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    
                    let listOfPropertyResponse =  try? JSONDecoder().decode([ListClusterResponseElement].self, from: response.data!)
                    self.jsonResponseForPropertyCluster = listOfPropertyResponse
                    
                    self.allPropertyListForCluster.removeAll()
                    if listOfPropertyResponse != nil{
                        
                        for property in listOfPropertyResponse!{
                            self.allPropertyListForCluster.append(property)
                        }
                        print(self.allPropertyListForCluster.count)
                        
                        DispatchQueue.main.async {
                            //print("All Properties------------\n",self.allPropertyList)
                            //globalPropertyList =  self.allPropertyList
                            self.removeSpinner()//self.view.activityStopAnimating()
                            self.mapView.clear()
                            if self.allTags.count > 0{
                                
                                self.putMarkersOnMap()
                            }
                            
                        }
                    }else{
                        
                        print("listOfPropertyResponse === nil")
                        do {
                            var tagArray : [PropertyTag] = []
                            
                            let json = try JSON(data: response.data!)
                            let result = json.arrayValue
                            print(result.count)
                            for item in result{
                                
                                let id = item["id"].intValue
                                let lat = item["latitude"].doubleValue
                                let long = item["longitude"].doubleValue
                                let property_tags = item["property_tags"].arrayValue
                                print(property_tags.count)
                                for tag in property_tags{
                                    
                                    let id = tag["id"].intValue
                                    let itag = PropertyTag(id: id, name: "", color: nil, user: 0, createdAt: "", updatedAt: "")
                                    if tagArray.count > 0{
                                        if tagArray.contains(where: { tag in tag.id == id }) {
                                            print(" exists in the array")
                                        } else {
                                            print(" does not exists in the array")
                                            tagArray.append(itag)
                                        }
                                    }
                                }
                                print(tagArray.count)
                                print(tagArray)
                                let listElement = ListClusterResponseElement(id: id, latitude: lat, longitude: long, propertyTags: tagArray)
                                
                                self.allPropertyListForCluster.append(listElement)
                            }
                            
                            print(self.allPropertyListForCluster.count)
                            DispatchQueue.main.async {
                                //print("All Properties------------\n",self.allPropertyList)
                                //globalPropertyList =  self.allPropertyList
                                self.removeSpinner()//self.view.activityStopAnimating()
                                self.mapView.clear()
                                if self.allTags.count > 0{
                                    self.putMarkersOnMap()
                                }
                            }
                        }catch let error{
                            print(error.localizedDescription)
                        }
                    }
                }
                    
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                
                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
    func getAllPropertyByTag(id:Int) {
        // self.showSpinner(onView: view)
        
        var urlString : String = ""
        self.allPropertyList.removeAll()
        
        if isFilteredList == false{
            urlString = UrlManager.baseURL() + UrlManager.apiString() + "/tag/\(String(describing: (globalSelectedTag?.id)!))/property-cluster/"
        }else{
            
            print(globalSelectedMembersID)
            let joined = globalSelectedMembersID.map { String($0) }.joined(separator: ",")
            print(joined)
            
            print("Filter tag called")
            urlString = UrlManager.baseURL() + UrlManager.apiString() + "/tag/\((globalSelectedTag?.id)!)/property-cluster/?members=\(joined)"
        }
        print(urlString)
        
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                //print("success",data)
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    
                    let listOfPropertyResponse =  try? JSONDecoder().decode(TagClusterResponse.self, from: response.data!)
                    self.jsonResponseForTagCluster = listOfPropertyResponse
                    
                    self.allPropertyListForTagCluster.removeAll()
                    if listOfPropertyResponse != nil{
                        
                        self.allPropertyListForTagCluster = self.jsonResponseForTagCluster?.results ?? []
                        
                        print(self.allPropertyListForTagCluster.count)
                        
                        DispatchQueue.main.async {
                            //print("All Properties------------\n",self.allPropertyList)
                            //globalPropertyList =  self.allPropertyList
                            self.removeSpinner()//self.view.activityStopAnimating()
                            self.mapView.clear()
                            if self.allTags.count > 0{
                                self.putMarkersOnMap()
                            }
                        }
                    }else{
                        print("self.allPropertyListForTagCluster.nil")
                        
                        do {
                            var tagArray : [PropertyTag] = []
                            
                            let json = try JSON(data: response.data!)
                            let result = json["results"].arrayValue
                            print(result.count)
                            for item in result{
                                
                                let id = item["id"].intValue
                                let lat = item["latitude"].doubleValue
                                let long = item["longitude"].doubleValue
                                let property_tags = item["property_tags"].arrayValue
                                
                                for tag in property_tags{
                                    
                                    let id = tag["id"].intValue
                                    let itag = PropertyTag(id: id, name: "", color: nil, user: 0, createdAt: "", updatedAt: "")
                                    if tagArray.count > 0{
                                        if tagArray.contains(where: { tag in tag.id == id }) {
                                            print(" exists in the array")
                                        } else {
                                            print(" does not exists in the array")
                                            tagArray.append(itag)
                                        }
                                    }
                                }
                                print(tagArray.count)
                                
                                let tagElement = TagClusterResponseElement(id: id, latitude: lat, longitude: long, propertyTags: tagArray)
                                
                                self.allPropertyListForTagCluster.append(tagElement)
                            }
                            
                            print(self.allPropertyListForTagCluster.count)
                            DispatchQueue.main.async {
                                //print("All Properties------------\n",self.allPropertyList)
                                //globalPropertyList =  self.allPropertyList
                                self.removeSpinner()//self.view.activityStopAnimating()
                                self.mapView.clear()
                                if self.allTags.count > 0{
                                    self.putMarkersOnMap()
                                }
                            }
                        }catch let error{
                            print(error.localizedDescription)
                        }
                    }
                    
                }
                    
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                self.removeSpinner()
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                
                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
    
    
    @IBAction func backAction(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func dropDownButtonClicked(_ sender: Any) {
        dropDown.show()
    }
    
    @objc func buttonAction(sender: UIButton!) {
        print("Button tapped")
        self.locationManager.startUpdatingLocation()
    }
    
    
    @IBAction func btnSatelliteClicked(_ sender: Any) {
        self.mapView.mapType = .satellite
        self.mapTypeView.isHidden = true
    }
    
    @IBAction func btnNormalClicked(_ sender: Any) {
        self.mapView.mapType = .normal
        self.mapTypeView.isHidden = true
    }
    
    @IBAction func switchMapType(_ sender: Any) {
        //mapTypeDropdown.show()
        
        if self.mapTypeView.isHidden == true{
            CATransaction.begin()
            
            let transition = CATransition()
            
            transition.timingFunction = CAMediaTimingFunction.init(name: .easeInEaseOut)
            transition.type = .push
            transition.subtype = .fromRight
            
            self.mapTypeView.layer.add(transition, forKey: kCATransition)
            CATransaction.commit()
            
            self.mapTypeView.isHidden = false
        }else{
            self.mapTypeView.isHidden = true
        }
        
        
    }
    
    
    @objc func driveNowButtonAction(sender: UIButton!) {
        
        if DriveNowButton.currentTitle == "STOP DRIVING"{
            DriveNowButton.setTitle("DRIVE NOW", for: .normal)
            self.DriveNowButton.setImage(UIImage(named: "broadcast"), for: .normal)
            self.actionButton.isHidden = true
        }
        else if DriveNowButton.currentTitle == "DRIVE NOW" {
            
            let title = "Choose your visibility"
            let myMutableString = NSMutableAttributedString(string: title, attributes: [NSAttributedString.Key.font:UIFont(name: ".SFUIText-Light", size: 20.0)!])
            let customActionSheet = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
            customActionSheet.setValue(myMutableString, forKey: "attributedTitle")
            
            let firstButton = UIAlertAction(title: "Public", style: .default, handler: { action in
                //do action
                self.DriveNowButton.setTitle("STOP DRIVING", for: .normal)
                self.DriveNowButton.setImage(UIImage(named: "active_broadcast"), for: .normal)
                self.actionButton.isHidden = false
            })
            
            let secondButton = UIAlertAction(title: "Private", style: .default, handler: { action in
                //do action
                self.DriveNowButton.setTitle("STOP DRIVING", for: .normal)
                self.DriveNowButton.setImage(UIImage(named: "active_broadcast"), for: .normal)
                self.actionButton.isHidden = false
                
            })
            
            let cancelButton = UIAlertAction(title: "Cancel", style: .destructive, handler: { action in
                self.actionButton.isHidden = true
            })
            customActionSheet.addAction(firstButton)
            customActionSheet.addAction(secondButton)
            customActionSheet.addAction(cancelButton)
            present(customActionSheet, animated: true)
        }
        else{
            
        }
    }
    
    
    func getAllTags(urlString: String) {
        
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                // print("success",data)
                let statusCode = response.response?.statusCode
                
                if statusCode == 200{
                    
                    let tagsResponse = try? JSONDecoder().decode(ListOfTagsResponse.self, from: response.data!)
                    print(tagsResponse)
                    self.allTags = tagsResponse!
                    DispatchQueue.main.async {
                        print("allTags------------\n",self.allTags)
                        self.removeSpinner()//self.view.activityStopAnimating()
                        //  self.tagTV.reloadData()
                        for tag in self.allTags{
                            let name = tag.name
                            let colorCode = tag.color?.colorCode
                            self.allTagNames.append(name!)
                            self.allColorCodes.append(colorCode!)
                        }
                        print(self.dropDownFlag)
                        
                        if self.dropDownFlag == "Properties by Tag"{
                            //self.putMarkersOnMap()
                            if isMapSelected == true{
                                self.getAllPropertyByTag(id: (globalSelectedTag?.id)!)
                                
                            }else{
                                print("list view on")
                            }
                        }else{
                            if isMapSelected == true{
                                self.getAllPropertyByList(id: (globalSelectedList?.id)!)
                                
                            }else{
                                print("list view on")
                            }
                        }
                        
                    }
                }
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
                AppConfigs.showSnacbar(message: error.localizedDescription, textColor: .red)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                
                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
    func putMarkersOnMap() {
        
        print(self.allPropertyList.count )
        
        mapView.delegate = self
        
        let iconGenerator = GMUDefaultClusterIconGenerator()
        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
        let renderer = GMUDefaultClusterRenderer(mapView: mapView, clusterIconGenerator: iconGenerator)
        clusterManager = GMUClusterManager(map: mapView, algorithm: algorithm, renderer: renderer)
        
        bounds = GMSCoordinateBounds()
        
        if self.dropDownFlag == "Properties by Tag"{
            for property in self.allPropertyListForTagCluster{
                
                let address = property.id
                let lat = property.latitude
                let lon = property.longitude
                
                let tags = property.propertyTags
                
                if lat != nil || lon != nil{
                    bounds = bounds.includingCoordinate(CLLocationCoordinate2D(latitude: lat ?? USA_LAT, longitude: lon ?? USA_LONG))
                    print(address?.description ?? "0")
                    let item = POIItem(position: CLLocationCoordinate2DMake(lat ?? USA_LAT, lon ?? USA_LONG), name: address?.description ?? "0", icon: marker.icon, propertyTags: tags )
                    clusterManager.add(item)
                }
            }
        }else{
            for property in self.allPropertyListForCluster{
                
                let address = property.id
                let lat = property.latitude
                let lon = property.longitude
                
                let tags = property.propertyTags
                
                if lat != nil || lon != nil{
                    bounds = bounds.includingCoordinate(CLLocationCoordinate2D(latitude: lat ?? USA_LAT, longitude: lon ?? USA_LONG))
                    
                    print(address?.description ?? "0")
                    let item = POIItem(position: CLLocationCoordinate2DMake(lat ?? USA_LAT, lon ?? USA_LONG), name: address?.description ?? "0", icon: marker.icon, propertyTags: tags )
                    
                    clusterManager.add(item)
                    
                    //showMarkerWithTitleSnippet(property: property , title: address!, snippet: " ", position: CLLocationCoordinate2DMake (CLLocationDegrees(Double(lat ?? 0.0)), CLLocationDegrees(Double(lon ?? 0.0))))
                }
            }
        }
        
        
        
        let update = GMSCameraUpdate.fit(bounds, withPadding: 100)
        mapView.animate(with: update)
        //setUpClusterView()
        renderer.delegate = self
        clusterManager.cluster()
        // Register self to listen to both GMUClusterManagerDelegate and
        // GMSMapViewDelegate events.
        clusterManager.setDelegate(self, mapDelegate: self)
        // mapView.setMinZoom(8, maxZoom: 20)
        //clusterManager.GMUClusterRendererDelegate
    }
    
    // MARK: ADD MARKER TO CLUSTER
    //    func generatePOIItems(_ accessibilityLabel: String, position: CLLocationCoordinate2D, icon: UIImage?) {
    //        guard let item = POIItem(position: position, name: accessibilityLabel, icon: icon) else {
    //            return
    //        }
    //        self.clusterManager.add(item)
    //    }
    
    func addFloatyButton() {
        actionButton.buttonImage = UIImage(named: "visibility")
        actionButton.buttonAnimationConfiguration = .transition(toImage: UIImage(named: "close")!)
        actionButton.itemAnimationConfiguration = .slideIn(withInterItemSpacing: 14)
        actionButton.contentVerticalAlignment = .fill
        actionButton.contentHorizontalAlignment = .fill
        actionButton.buttonColor = UIColor(red:0.07, green:0.82, blue:0.41, alpha:1)
        
        let itemPrivate = actionButton.addItem()
        itemPrivate.titleLabel.text = "    Private    "
        itemPrivate.titleLabel.textColor = UIColor.blue
        itemPrivate.titleLabel.backgroundColor = .white
        itemPrivate.imageView.image = UIImage(named: "locked-padlock")
        itemPrivate.buttonColor = .white
        itemPrivate.action = { item in
            // Do something
        }
        
        let itemPublic = actionButton.addItem()
        itemPublic.titleLabel.text = "    Public    "
        itemPublic.titleLabel.textColor =  UIColor.blue
        itemPublic.titleLabel.backgroundColor = .white
        itemPublic.imageView.image = UIImage(named: "world")
        itemPublic.buttonColor = .white
        itemPublic.action = { item in
            // Do something
        }
        
        mapView.addSubview(actionButton)
        actionButton.translatesAutoresizingMaskIntoConstraints = false
        actionButton.trailingAnchor.constraint(equalTo: mapView.safeAreaLayoutGuide.trailingAnchor, constant: -20).isActive = true
        actionButton.bottomAnchor.constraint(equalTo: mapView.safeAreaLayoutGuide.bottomAnchor, constant: -30).isActive = true
        
    }
    
    
    func showMarkerWithTitleSnippet(property:ListedProperty, title: Int, snippet:String, position: CLLocationCoordinate2D){
        
        let marker = GMSMarker()
        marker.isDraggable = false
        marker.position = position
        marker.title = String(title)
        marker.icon = UIImage(named: "new_ic_tapped_property")
        marker.snippet = snippet
        marker.map = mapView
        let markerImage = UIImage(named: "new_ic_tapped_property")!.withRenderingMode(.alwaysTemplate)//marker_user_selected_tags_multi
        let multiTagMarkerImage = UIImage(named: "marker_user_selected_tags_multi")//!.withRenderingMode(.alwaysTemplate)
        //creating a marker view
        let markerView = UIImageView(image: markerImage)
        let tags = property.propertyTags
        
        if tags != nil{
            
            if tags?.count == 0{
                markerView.tintColor = UIColor.black // UIColor.blue
                marker.iconView = markerView
                marker.icon = markerImage
            }else if tags!.count > 1{
                markerView.tintColor =  UIColor.gray
                marker.iconView = markerView
                marker.icon = markerImage
            }else if tags?.count == 1{
                let tag = tags?[0]
                let color = tag?.color?.colorCode
                print(color ?? "")
                let propertyTagID = tag?.id ?? 0
                print(propertyTagID)
                for tag in self.allTags{
                    let id = tag.id
                    if id == propertyTagID{
                        let color = tag.color?.colorCode
                        print(color ?? "")
                        let uiColor =  UIColor(hexString: color!)
                        markerView.tintColor = uiColor
                        marker.iconView = markerView
                        marker.icon = markerImage
                    }
                }
            }else{
                print("unknown case")
                markerView.tintColor = UIColor.black // UIColor.blue
                marker.iconView = markerView
                marker.icon = markerImage
            }
        }else{
            markerView.tintColor = UIColor.black // UIColor.blue
            marker.iconView = markerView
            marker.icon = markerImage
        }
    }
    
    func startMonitoringLocation() {
        if CLLocationManager.locationServicesEnabled() {
            locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
            locationManager.activityType = CLActivityType.automotiveNavigation
            locationManager.distanceFilter = 1
            locationManager.headingFilter = 1
            locationManager.requestWhenInUseAuthorization()
            locationManager.startMonitoringSignificantLocationChanges()
            locationManager.startUpdatingLocation()
        }
        
        //todo
        //locationManager.stopMonitoringSignificantLocationChanges()
        //locationManager.stopUpdatingLocation()
    }
    
    func stopMonitoringLocation() {
        locationManager.stopMonitoringSignificantLocationChanges()
        locationManager.stopUpdatingLocation()
    }
    
    func addCurrentLocationMarker() {
        currentLocationMarker?.map = nil
        currentLocationMarker = nil
        if let location = locationManager.location {
            currentLocationMarker = GMSMarker(position: location.coordinate)
            currentLocationMarker?.icon = UIImage(named: "broadcast")
            currentLocationMarker?.map = mapView
            currentLocationMarker?.rotation = locationManager.location?.course ?? 0
        }
    }
    
    func zoomToCoordinates(_ coordinates: CLLocationCoordinate2D) {
        let camera = GMSCameraPosition.camera(withLatitude: coordinates.latitude, longitude: coordinates.longitude, zoom: 17)
        mapView.camera = camera
        //addCurrentLocationMarker()
    }
}

extension PropertiesMapViewController: CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("location manager error -> \(error.localizedDescription)")
        AppConfigs.showSnacbar(message: "Please enable location permission", textColor: .red)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            break
        case .restricted:
            break
        case .denied:
            stopMonitoringLocation()
            break
        default:
            //addCurrentLocationMarker()
            startMonitoringLocation()
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        //Finally stop updating location otherwise it will come again and again in this delegate
        if let lastLocation = locations.last {
            currentLocationMarker?.position = lastLocation.coordinate
            currentLocationMarker?.rotation = lastLocation.course
            self.zoomToCoordinates(lastLocation.coordinate)
        }
        self.locationManager.stopUpdatingLocation()
    }
    
    func cameraMoveToLocation(toLocation: CLLocationCoordinate2D?) {
        if toLocation != nil {
            mapView.camera = GMSCameraPosition.camera(withTarget: toLocation!, zoom: 17)
        }
    }
}

extension PropertiesMapViewController: GMSMapViewDelegate{
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        print("didTapInfoWindowOf")
    }
    
    /* handles Info Window long press */
    func mapView(_ mapView: GMSMapView, didLongPressInfoWindowOf marker: GMSMarker) {
        print("didLongPressInfoWindowOf")
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        
    }
    
    /* set a custom Info Window */
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        
        return UIView()
    }
    
    //MARK - GMSMarker Dragging
    func mapView(_ mapView: GMSMapView, didBeginDragging marker: GMSMarker) {
        print("didBeginDragging")
    }
    func mapView(_ mapView: GMSMapView, didDrag marker: GMSMarker) {
        print("didDrag")
    }
    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
        print("didEndDragging")
        // mapView.selectedMarker = marker
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        print("did tap a marker")
        
        // Remember to return false
        // so marker event is still handled by delegate
        print(marker.title)
        
        if let poiItem = marker.userData as? POIItem
        {
            //NSLog(“Did tap marker for cluster item \(poiItem.name)”)
            print("Did tap marker for cluster item \(poiItem.name)")
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "PropertyDetailsViewController") as! PropertyDetailsViewController
            nextViewController.parentVC = "LoadHouzesFromMap"
            nextViewController.propertyIDFromList = poiItem.name!
            
            self.navigationController?.pushViewController(nextViewController, animated: true)
        } else {
            
            print("viewDetailsButtonAction")
            //            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            //            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "PropertyDetailsViewController") as! PropertyDetailsViewController
            //            nextViewController.parentVC = "LoadHouzesFromMap"
            //            nextViewController.propertyIDFromList = marker.title!
            //            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        
        return false
        
    }
    
    
    // let the custom infowindow follows the camera
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        if (tappedMarker.userData != nil){
            let location = CLLocationCoordinate2D(latitude: marker.position.latitude, longitude: marker.position.longitude)
            infoWindow.center = mapView.projection.point(for: location)
            
        }
    }
}


extension PropertiesMapViewController: GMUClusterManagerDelegate, GMUClusterRendererDelegate{
    
    
    func renderer(_ renderer: GMUClusterRenderer, willRenderMarker marker: GMSMarker) {
        // if your marker is pointy you can change groundAnchor
        marker.groundAnchor = CGPoint(x: 0.5, y: 1)
        if  let markerData = (marker.userData as? POIItem) {
            
            let markerImage = UIImage(named: "new_ic_tapped_property")!.withRenderingMode(.alwaysTemplate)
            let multiTagMarkerImage = UIImage(named: "marker_user_selected_tags_multi")
            //creating a marker view
            let markerView = UIImageView(image: markerImage)
            let tags = markerData.propertyTags
            
            if tags != nil{
                
                if tags?.count == 0{
                    
                    markerView.tintColor = UIColor.black // UIColor.blue
                    marker.iconView = markerView
                    marker.icon = markerImage
                    
                }else if tags!.count > 1{
                    
                    markerView.tintColor =  UIColor.gray
                    marker.iconView = markerView
                    marker.icon = markerImage
                    
                }else if tags?.count == 1{
                    
                    let tag = tags?[0]
                    let color = tag?.color?.colorCode
                    print(color ?? "")
                    
                    let propertyTagID = tag?.id ?? 0
                    print(propertyTagID)
                    
                    
                    for tag in self.allTags{
                        
                        let id = tag.id
                        if id == propertyTagID{
                            let color = tag.color?.colorCode
                            print(color ?? "")
                            let uiColor =  UIColor(hexString: color!)
                            markerView.tintColor = uiColor
                            marker.iconView = markerView
                            marker.icon = markerImage
                        }
                    }
                }else{
                    print("unknown case")
                    markerView.tintColor = UIColor.black // UIColor.blue
                    marker.iconView = markerView
                    marker.icon = markerImage
                }
            }else{
                markerView.tintColor =  UIColor.black // UIColor.blue
                marker.iconView = markerView
                marker.icon = markerImage
            }
        }
    }
    
}
