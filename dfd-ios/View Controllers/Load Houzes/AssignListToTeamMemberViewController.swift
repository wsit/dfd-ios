//
//  AssignTeamMemberViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 9/23/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit
import Alamofire


class AssignListToTeamMemberViewController: UIViewController {
    
    @IBOutlet weak var memberTableView: UITableView!
    @IBOutlet weak var btnDone: UIBarButtonItem!
    @IBOutlet weak var myNavBar: UINavigationBar!
    
    var users: [UnregisteredInvitation] = []
    var unregisteredUsers: [UnregisteredInvitation] = []
    var allTeamMembers: [UnregisteredInvitation] = []
    var jsonResponse: MyTeamResponse?
    
    var selectedMember: UnregisteredInvitation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: "NewMemberTableViewCell", bundle: nil)
        memberTableView.register(nib, forCellReuseIdentifier: "NewMemberTableViewCell")
        self.memberTableView.tableFooterView = UIView()
        self.memberTableView.backgroundColor = .white
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.btnDone.isEnabled = false
        self.btnDone.title = " "

        if Network.reachability.status == .unreachable{
            AppConfigs.updateUserInterface(view: view)
        }else{
         

            allTeamMembers.removeAll()
            users.removeAll()
            unregisteredUsers.removeAll()
            
            let url : String = UrlManager.getTeamListURL()
            
            print(url)
            self.showSpinner(onView: view)
            getTeamMembers(urlString: url)
            
        }//        }
    }
    
    
    @IBAction func backButtonClicked(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneButtonClicked(_ sender: Any) {
        
        //self.dismiss(animated: true, completion: nil)
        if globalSelectedList?.id != nil{
            self.handleAssignListToMember()
        }else{
            AppConfigs.showSnacbar(message: "Please select a list first", textColor: .red)
        }
        //        self.navigationController?.popViewController(animated: true)
    }
    
    func handleAssignListToMember(){
        
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        let urlString = UrlManager.baseURL() + UrlManager.apiString() + "/list/\((globalSelectedList?.id)!)/push/" ///assign-member-to-list/"
        print(urlString)
        let parameter:Parameters = [
            //"list" : (globalSelectedList?.id)!,
            "member" : (selectedMember?.id)!
        ]
        print(parameter)
        
        Alamofire.request(urlString, method: .post, parameters: parameter as Parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
                
            case.success(let data):
                
                self.removeSpinner()
                print("success",data)
                let statusCode = response.response?.statusCode
                print(statusCode!)
                
                let responseData = try? JSONDecoder().decode(AssignListToMemberResponse.self, from: response.data!)
                
                if responseData?.status == true{
                    let alertController = UIAlertController(title: (responseData?.message)!, message: " ", preferredStyle: .alert)
                    let OKAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                        self.dismiss(animated: true, completion: nil)
                    })
                    alertController.addAction(OKAction)
                    self.present(alertController, animated: true, completion: nil)
                    
                }else{
                    self.showAlert(message: (responseData?.message)!, title: "Failed")
                }
                
                
                self.removeSpinner()
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)

                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
        }
        self.view.activityStopAnimating()
    }
    
    func getTeamMembers(urlString:String)  {
        
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success",data)
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    let teamMembers = try? JSONDecoder().decode(MyTeamResponse.self, from: response.data!)
                    self.jsonResponse = teamMembers
                    
                    let userList = teamMembers?.users
                    print("userList------\n",userList!)
                    
                    self.allTeamMembers = userList! //+ pendingUserList!
                    print("allTeamMembers-----\n", self.allTeamMembers)
                    
                    DispatchQueue.main.async {
                        print("allTeamMembers------------\n",self.allTeamMembers)
                        self.removeSpinner()//self.view.activityStopAnimating()
                        self.memberTableView.reloadData()
                    }
                }
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)

                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
}

extension AssignListToTeamMemberViewController: UITableViewDataSource {
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if allTeamMembers.count == 0 {
            tableView.setEmptyMessage("Nothing yet to show")
        } else {
            tableView.restore()
        }
        return allTeamMembers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewMemberTableViewCell", for: indexPath) as! NewMemberTableViewCell
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [cell.memberNameLabel], type: TextType.ListTitle.rawValue)
        cell.memberNameLabel.textColor = self.listTitleColor
        cell.pendingImageView.isHidden = true
        
        cell.separatorLine.backgroundColor = self.listSeperatorColor
        
        if allTeamMembers.count > indexPath.row{
            let member = allTeamMembers[indexPath.row]
            
            if member.firstName != nil || member.lastName != nil{
                let fullName: String = member.firstName! + " " + member.lastName!
                cell.memberNameLabel.text = fullName
                if member.photo != nil{
                    if member.photo != ""{
                        self.showPicture(url: (member.photo!), imageView: cell.memberImageView)
                    }else{
                        print("photo is empty")
                    }
                }else
                {
                    print("photo is nil")
                }
            }
        }
        
        return cell
    }
    
}

extension AssignListToTeamMemberViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if allTeamMembers.count > indexPath.row{
            
            let cell = tableView.cellForRow(at: indexPath) as! NewMemberTableViewCell
            //cell.memberImageView.image = UIImage(named: "selected")
            let member = allTeamMembers[indexPath.row]
            self.selectedMember = member
            cell.backgroundColor = self.selectedCellBackgroundColor
            cell.upView.backgroundColor = self.selectedCellBackgroundColor // UIColor(red:0.97, green:0.71, blue:0, alpha:1)
            cell.pendingImageView.isHidden = true
        }
        self.btnDone.isEnabled = true
        self.btnDone.title = "Done"
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! NewMemberTableViewCell
        let member = allTeamMembers[indexPath.row]
        let fullName = (member.firstName ?? "") + (member.lastName ?? "")
        cell.backgroundColor = UIColor.white
        cell.upView.backgroundColor = UIColor.white  
        
        //cell.memberImageView.setImage(string: fullName, color: UIColor.colorHash(name: fullName), circular: true, stroke: false)
        
    }
    
}
