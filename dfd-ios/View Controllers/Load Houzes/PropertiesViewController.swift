//
//  PropertiesViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 8/7/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit
import Alamofire
import DropDown
import SwiftyJSON
import Kingfisher

protocol ListSelectionDelegate {
    func getSelectedList(listID: Int)
}

protocol TagSelectionDelegate {
    func getSelectedTag(tagID: Int)
}

protocol MemberSelectionDelegate {
    func getSelectedMembersID(memberIDs: [Int])
}


class PropertiesViewController: UIViewController {
    
    //  @IBOutlet weak var dropDownButtonForMap: UIButton!
    @IBOutlet weak var propertyTV: UITableView!
    @IBOutlet weak var btnAssignMember: UIButton!
    @IBOutlet weak var lblMessageTitle: UILabel!
    
    var listDelegate: ListSelectionDelegate?
    var tagDelegae: TagSelectionDelegate?
    var memberDelegate: MemberSelectionDelegate?
    
    var dropDownFlag: String = ""
    var tagItems = ["Tag 1", "Tag 2", "Tag 3", "Tag 4", "Tag 5", "Tag 6"]
    var listItems = ["List 1", "List 2", "List 3", "List 4", "List 5", "List 6"]
    
    var listTitleArray : [String] = []
    var listIDArray : [Int] = []
    
    var selectedProperty : [String] = []
    var couter: Int = 0
    var indexArray : [IndexPath] = []
    
    let dropDown = DropDown()
    
    var allList: [MList] = []
    var url : String = ""
    var nextUrl: String = ""
    var previousUrl: String = ""
    var counter: Int = 0
    var jsonResponse: AllListsResponseForDropdownElement?
    
    var selectedList : MList?
    
    var allPropertyList: [ListedProperty] = []
    var urlForProperty : String = ""
    var nextUrlForProperty : String = ""
    var previousUrlForProperty : String = ""
    var jsonResponseForProperty : ListOfPropertyResponse?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpNavigationBar()
        NotificationCenter.default.addObserver(self, selector: #selector(self.loadPropertyList), name: NSNotification.Name(rawValue: "listSelected"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.loadPropertyTag), name: NSNotification.Name(rawValue: "tagSelected"), object: nil)
        
        
        let nib = UINib(nibName: "PropertyListTableViewCell", bundle: nil)
        propertyTV.register(nib, forCellReuseIdentifier: "PropertyListTableViewCell")
        self.propertyTV.tableFooterView = UIView()
        self.propertyTV.backgroundColor = .white
        //self.propertyTV.separatorStyle = .singleLine
        self.propertyTV.separatorColor = self.profileSeperatorColor
        self.propertyTV.isHidden = true
        self.title = dropDownFlag
        if dropDownFlag == "Properties by Tag"{
            self.lblMessageTitle.text = "Select a Tag to discover number of properties"
            //setUpDropDownList(list: tagItems)
        }else{
            self.lblMessageTitle.text = "Select a List to discover number of properties"
            // setUpDropDownList(list: listItems)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if dropDownFlag == "Properties by Tag"{
            
            if isFilteredList == false{
                if globalSelectedTag != nil {
                    
                    self.propertyTV.isHidden = false
                    self.allPropertyList.removeAll()
                    let urlString = UrlManager.baseURL() + UrlManager.apiString() + "/property/tag/\(String(describing: (globalSelectedTag?.id)!))/" //"/?limit=10&offset=0"
                    print(urlString)
                    self.getAllPropertyByTag(urlString: urlString)
                    self.tagDelegae?.getSelectedTag(tagID: (globalSelectedTag?.id)!)
                    //                }
                }else{
                    // btnAssignMember.isHidden = true
                    print("globalSelectedList == nil")
                }
            }else{
                if globalSelectedTag != nil {
                    loadPropertyFromTagFilters()
                }else{
                    print("globalSelectedList == nil")
                }
            }
        }else {
            
            if globalSelectedList != nil {
                if Network.reachability.status == .unreachable{
                    AppConfigs.updateUserInterface(view: view)
                }else{
                 

                    self.propertyTV.isHidden = false
                    self.allPropertyList.removeAll()
                    let urlString = UrlManager.baseURL() + UrlManager.apiString() + "/property/list/\((globalSelectedList?.id)!)/"
                    print(urlString)
                    self.getAllPropertyByList(urlString:urlString)
                    
                    //self.getAllPropertyByList(id: (globalSelectedList?.id)!)
                    self.listDelegate?.getSelectedList(listID: (globalSelectedList?.id)!)
                    
                    
                }
                //               }
            }else{
                // btnAssignMember.isHidden = true
                print("globalSelectedList == nil")
                
            }
        }
    }
    
    func loadPropertyFromTagFilters(){
        print(globalSelectedMembersID)
        let joined = globalSelectedMembersID.map { String($0) }.joined(separator: ",")
        print(joined)
        
        self.propertyTV.isHidden = false
        self.allPropertyList.removeAll()
        let urlString = UrlManager.baseURL() + UrlManager.apiString() + "/property/filter/?members=\(joined)&tags=\((globalSelectedTag?.id)!)"
        // /api/property/filter/?members=1,2,3&tags=2&page=1&limit=10
        
        print(urlString)
        self.getAllPropertyByTag(urlString: urlString)
        self.tagDelegae?.getSelectedTag(tagID: (globalSelectedTag?.id)!)
        
        //        print("Filter tag called")
        //        self.propertyTV.isHidden = false
        //        self.allPropertyList.removeAll()
        //        let urlString = UrlManager.baseURL() + UrlManager.apiString() + "/property/filter/?members=\(joined)&tags=\((globalSelectedTag?.id)!)/"
        //        print(urlString)
        //        self.getAllPropertyByTag(urlString:urlString)
        //        self.tagDelegae?.getSelectedTag(tagID: (globalSelectedTag?.id)!)
    }
    
    @objc func loadPropertyList(notif: NSNotification) {
        
        if isListSelected == true{
            
            self.propertyTV.isHidden = false
            self.allPropertyList.removeAll()
            let urlString = UrlManager.baseURL() + UrlManager.apiString() + "/property/list/\((globalSelectedList?.id)!)/"
            print(urlString)
            self.getAllPropertyByList(urlString:urlString)
        }else{
            print("Mapview is on")
        }
    }
    
    @objc func loadPropertyTag(notif: NSNotification) {
        
        if isListSelected == true{
            
            if isFilteredList == false{
                
                self.propertyTV.isHidden = false
                self.allPropertyList.removeAll()
                let urlString = UrlManager.baseURL() + UrlManager.apiString() + "/property/tag/\(String(describing: (globalSelectedTag?.id)!))/" //"/?limit=10&offset=0"
                print(urlString)
                self.getAllPropertyByTag(urlString: urlString)
                
            }else{
                loadPropertyFromTagFilters()
                
            }
            
        }else{
            print("Mapview is on")
        }
        
    }
    
    @IBAction func dropDownButtonClicked(_ sender: Any) {
        dropDown.show()
    }
    
    @IBAction func assignToMemberClicked(_ sender: Any) {
        
        if self.indexArray.count == 0 {
            AppConfigs.showSnacbar(message: "Please Select Properties First", textColor: .red)
        }else{
            let storyBoard : UIStoryboard = UIStoryboard(name: "List", bundle:nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "AssignTeamMemberViewController") as! AssignTeamMemberViewController
            vc.parentVC = "Property"
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func getAllPropertyByList(urlString:String) {
        //self.showSpinner(onView: view)
        //        let urlString = UrlManager.baseURL() + UrlManager.apiString() + "/property/list/\(id)/"
        //
        //        print(urlString)
        //
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                //  print("success",data)
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    
                    let listOfPropertyResponse: ListOfPropertyResponse? =  try? JSONDecoder().decode(ListOfPropertyResponse.self, from: response.data!)
                    self.jsonResponseForProperty = listOfPropertyResponse
                    self.counter = listOfPropertyResponse?.count ?? 0
                    self.nextUrl = listOfPropertyResponse?.next ?? ""
                    
                    var items: [ListedProperty] = self.allPropertyList
                    if listOfPropertyResponse != nil{
                        
                        items.append(contentsOf: (listOfPropertyResponse?.results)!)
                    }else{
                        
                        do {
                            let json = try JSON(data: response.data!)
                            let next = json["next"].stringValue
                            print(next)
                            self.nextUrl = next
                            
                            let previous = json["previous"].stringValue
                            print(previous)
                            
                            let count = json["count"].intValue
                            print(count)
                            self.counter = count
                            
                            let results = json["results"].array
                            print(results)
                            
                            for result in results!{
                                
                                let id = result["id"].intValue//                            "id":157,
                                print(id )
                                let user_list = result["user_list"].intValue// "user_list":10,
                                print(user_list)
                                
                                let street = result["street"].stringValue//       "street":"Murre Ln",
                                print(street)
                                let city = result["city"].stringValue//"city":"Sunnyvale",
                                print(city)
                                
                                let state = result["state"].stringValue//"city":"Sunnyvale",
                                print(state)
                                
                                let zip = result["zip"].stringValue//"zip":"94087",
                                print(zip)
                                let latitude = result["latitude"].doubleValue//"latitude":37.34308970000001,
                                print(latitude)
                                
                                let longitude = result["longitude"].doubleValue//"longitude":-122.0123984,
                                print(longitude)
                                let photo_count = result["photo_count"].intValue//photo_count":0,
                                print(photo_count)
                                
                                let power_trace_request_id = result["power_trace_request_id"].intValue//"power_trace_request_id":null,
                                print(power_trace_request_id)
                                let note_count = result["note_count"].intValue//
                                print(note_count)
                                let history = result["history"].intValue
                                print(history)
                                
                                let listedProperty : ListedProperty = ListedProperty(id: id, userList: user_list, street: street, city: city, state: state, zip: zip, cadAcct: "", gmaTag: 0, latitude: latitude, longitude: longitude, propertyTags: [], ownerInfo: [], photoCount: photo_count, noteCount: note_count, createdAt: "", updatedAt: "", powerTraceRequestID: power_trace_request_id, history: history)
                                items.append(listedProperty)
                            }
                            print("items.count",items.count)
                            
                        } catch let error as NSError {
                            // error
                        }
                    }
                    
                    self.allPropertyList.removeAll()
                    self.allPropertyList = items
                    
                    DispatchQueue.main.async {
                        print("All Properties------------\n",self.allPropertyList)
                        
                        globalPropertyList =  self.allPropertyList
                        self.removeSpinner()//self.view.activityStopAnimating()
                        self.propertyTV.reloadData()
                    }
                }
                    
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)

                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
            //self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
    
    func getAllPropertyByTag(urlString:String) {
        // self.showSpinner(onView: view)
        
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                //print("success",data)
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    
                    let listOfPropertyResponse: ListOfPropertyResponse? =  try? JSONDecoder().decode(ListOfPropertyResponse.self, from: response.data!)
                    self.jsonResponseForProperty = listOfPropertyResponse
                    self.counter = listOfPropertyResponse?.count ?? 0
                    self.nextUrl = listOfPropertyResponse?.next ?? ""
                    
                    var items: [ListedProperty] = self.allPropertyList
                    if listOfPropertyResponse != nil{
                        
                        items.append(contentsOf: (listOfPropertyResponse?.results)!)
                        print("items.count",items.count)
                    }else{
                        
                        do {
                            let json = try JSON(data: response.data!)
                            let next = json["next"].stringValue
                            print(next)
                            self.nextUrl = next
                            
                            let previous = json["previous"].stringValue
                            print(previous)
                            
                            let count = json["count"].intValue
                            print(count)
                            self.counter = count
                            
                            let results = json["results"].array
                            print(results)
                            
                            for result in results!{
                                
                                let id = result["id"].intValue//                            "id":157,
                                print(id )
                                let user_list = result["user_list"].intValue// "user_list":10,
                                print(user_list)
                                
                                let street = result["street"].stringValue//       "street":"Murre Ln",
                                print(street)
                                let city = result["city"].stringValue//"city":"Sunnyvale",
                                print(city)
                                
                                let state = result["state"].stringValue//"city":"Sunnyvale",
                                print(state)
                                
                                let zip = result["zip"].stringValue//"zip":"94087",
                                print(zip)
                                let latitude = result["latitude"].doubleValue//"latitude":37.34308970000001,
                                print(latitude)
                                
                                let longitude = result["longitude"].doubleValue//"longitude":-122.0123984,
                                print(longitude)
                                let photo_count = result["photo_count"].intValue//photo_count":0,
                                print(photo_count)
                                
                                let power_trace_request_id = result["power_trace_request_id"].intValue//"power_trace_request_id":null,
                                print(power_trace_request_id)
                                let note_count = result["note_count"].intValue//
                                print(note_count)
                                let history = result["history"].intValue
                                print(history)
                                
                                let listedProperty : ListedProperty = ListedProperty(id: id, userList: user_list, street: street, city: city, state: state, zip: zip, cadAcct: "", gmaTag: 0, latitude: latitude, longitude: longitude, propertyTags: [], ownerInfo: [], photoCount: photo_count, noteCount: note_count, createdAt: "", updatedAt: "", powerTraceRequestID: power_trace_request_id, history: history)
                                items.append(listedProperty)
                            }
                            print("items.count",items.count)
                        } catch let error as NSError {
                            print(error.localizedDescription)
                        }
                    }
                    
                    self.allPropertyList.removeAll()
                    self.allPropertyList = items
                    globalPropertyList =  self.allPropertyList
                    
                    DispatchQueue.main.async {
                        print("All Properties------------\n",self.allPropertyList.count)
                        self.removeSpinner()//self.view.activityStopAnimating()
                        self.propertyTV.reloadData()
                    }
                    
                }
                    
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                   // AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)

                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
    func handleDeleteProperty(id:Int)  {
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        
        let url = UrlManager.baseURL() + UrlManager.apiString() + "/property/\(id)/"
        
        Alamofire.request(url, method: .delete, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
            case.success(let data):
                print("success",data)
                //self.removeSpinner()
                let statusCode = response.response?.statusCode
                print(statusCode!)
                
                //let deleteScoutResponse = try? JSONDecoder().decode(DeleteTeamMemberResponse.self, from: response.data!)
                do {
                    let json = try JSON(data: response.data!)
                    let status = json["status"].boolValue
                    print(status)
                    let message = json["message"].stringValue
                    print(message)
                    
                    if statusCode == 200{
                        DispatchQueue.main.async {
                            self.removeSpinner()
                            AppConfigs.showSnacbar(message: message, textColor: .green)
                            
                            for (index,property) in self.allPropertyList.enumerated(){
                                let pid = property.id
                                if pid == id{
                                    self.allPropertyList.remove(at: index)
                                }
                            }
                            self.propertyTV.reloadData()
                        }
                    }
                    else if statusCode == 401{
                        self.removeSpinner()
                        //let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                        print("error with response status: \(String(describing: statusCode))")
                      //  AppConfigs.showSnacbar(message: message, textColor: .red)
                    }
                    else{
                        self.removeSpinner()
                        print("error with response status: \(String(describing: statusCode))")
                        AppConfigs.showSnacbar(message: message, textColor: .green)
                    }
                    
                }catch let error{
                    print(error.localizedDescription)
                }
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)

                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
            self.removeSpinner()
        }
    }
    
}

extension PropertiesViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if allPropertyList.count == 0 {
            tableView.setEmptyMessage("Nothing yet to show")
            //btnAssignMember.isHidden = true
        } else {
            tableView.restore()
            //btnAssignMember.isHidden = false
        }
        return allPropertyList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PropertyListTableViewCell", for: indexPath) as! PropertyListTableViewCell
        
        cell.backgroundColor = .white
        cell.upView.backgroundColor = .white
        cell.upView.cornerRadius = 8
        cell.lblProperttAddress.textColor = self.listTitleColor
        cell.lblNoOfNotes.textColor = UIColor.white
        cell.lblNoOfPhotosOfProperty.textColor = UIColor.white
        cell.lblDateAdded.textColor = self.listSubTitleColor
        cell.lblSeparator.backgroundColor = self.listSeperatorColor
        cell.lblNoOfNotes.backgroundColor = self.greenCapsuleColor
        cell.lblNoOfPhotosOfProperty.backgroundColor = self.blueCapsuleColor
        
        cell.lblNoOfNotes.layer.masksToBounds = true
        cell.lblNoOfPhotosOfProperty.layer.masksToBounds = true
        cell.lblPowerTraced.layer.masksToBounds = true
        
        cell.lblNoOfPhotosOfProperty.cornerRadius = 8
        cell.lblNoOfNotes.cornerRadius = 8
        cell.lblPowerTraced.cornerRadius = 8
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.accessoryType = .none
        cell.lblPowerTraced.backgroundColor = self.powerTraceFlagColor
        
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [cell.lblProperttAddress], type: TextType.ListTitle.rawValue)
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [cell.lblNoOfPhotosOfProperty, cell.lblNoOfNotes, cell.lblPowerTraced], type: TextType.Capsule.rawValue)
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [cell.lblDateAdded], type: TextType.ListSubTitle.rawValue)
        
        if allPropertyList.count > indexPath.row{
            
            let property = allPropertyList[indexPath.row]
            let street = property.street
            let city = property.city
            let state = property.state
            let zip = property.zip
            
            let tmp3 = (street ?? "") + " " + (city ?? "")
            let tmp2 = tmp3 + " " + (state ?? "") + " "
            
            cell.lblProperttAddress.text = tmp2 + (zip ?? "") //property.street ?? "N/A"
            cell.lblNoOfPhotosOfProperty.text =  "\(property.photoCount ?? 0) Photos"
            cell.lblNoOfNotes.text = "\(property.noteCount ?? 0) Notes"
            
            if property.powerTraceRequestID != nil{
                print(property.powerTraceRequestID)
                if property.powerTraceRequestID! == 0{
                    cell.lblPowerTraced.isHidden = true
                }else{
                    cell.lblPowerTraced.isHidden = false
                }
            }else{
                cell.lblPowerTraced.isHidden = true
            }
            
            let urlString = "https://api.houzes.com/api/photos/property/\((property.id)!)/thumb/"
            print(urlString)
            
            let url = URL(string: urlString)
            
            if url != nil {
                //  print(url!)
                //let url = URL(string: urlString)

                DispatchQueue.global().async {
                    let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                    DispatchQueue.main.async {
                        if data != nil{
                            //cell.propertyImage.kf.setImage(with: url)
                            cell.propertyImage.image = UIImage(data: data!)
                        }
                    }
                }
            }
            
            let dateString = property.createdAt ?? ""
            let tmp = String(dateString.prefix(10))
            //  print(tmp)
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS" //"yyyy-MM-dd'T'HH:mm:ss.ssssss'Z'"
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "MMM dd,yyyy"//"MMM dd yyyy h:mm a"  //"MMM d, h:mm a" for  Sep 12, 2:11 PM
            let datee = dateFormatterGet.date(from: property.createdAt ?? "")
            let dateStringInNewFormat =  dateFormatterPrint.string(from: datee ?? Date())
            // print(dateStringInNewFormat)
            
            cell.lblDateAdded.text = dateStringInNewFormat // String(dateString.prefix(10)) //dateStringInNewFormat
            
            if datee == nil{
                cell.lblDateAdded.text = tmp
                let strDate: String = tmp + " 12:24:26"
                //print(strDate)
                
                let dateFormatterGet = DateFormatter()
                dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
                
                let dateFormatterPrint = DateFormatter()
                dateFormatterPrint.dateFormat = "MMM dd,yyyy"
                
                if let date = dateFormatterGet.date(from: strDate) {
                    // print(dateFormatterPrint.string(from: date))
                    cell.lblDateAdded.text = dateFormatterPrint.string(from: date)
                } else {
                    print("There was an error decoding the string")
                }
            }else{
                print("datee is not nil")
                // print(datee)
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row ==  allPropertyList.count - 1{
            if self.counter > (allPropertyList.count) {
                //                self.getAllPropertyByListWithPagination(urlString: (jsonResponseForProperty?.next)!)
                let urlString = self.nextUrl
                print(urlString)
                
                if dropDownFlag == "Properties by Tag"{
                    self.getAllPropertyByTag(urlString: urlString)
                } else{
                    self.getAllPropertyByList(urlString:urlString)
                }
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if allPropertyList.count > indexPath.row{
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "PropertyDetailsViewController") as! PropertyDetailsViewController 
            
            let property = allPropertyList[indexPath.row]
            let sp = SelectedProperty()
            sp.propertyID = property.id!
            sp.propertyAddress = property.street ?? " "
            sp.latitude = (Double( property.latitude ?? 0.0)) //as! Double
            sp.longitude = (Double( property.longitude ?? 0.0)) //as! Double
            
            globalSelectedProperty = sp
            globalSelectedPropertyFromList = property
            //nextViewController.selectedPropertyFromList = property
            nextViewController.parentVC = "LoadHouzes"
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
    }
    
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        //tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        var config = UISwipeActionsConfiguration(actions: [])
        
        if allPropertyList.count > indexPath.row{
            
            let property = allPropertyList[indexPath.row]
            let sp = SelectedProperty()
            sp.propertyID = property.id!
            sp.propertyAddress = property.street ?? " "
            sp.latitude = (Double( property.latitude ?? 0.0)) //as! Double
            sp.longitude = (Double( property.longitude ?? 0.0)) //as! Double
            let id = property.id
            
            let delete = UIContextualAction(style: .destructive, title: "Delete") { (action, view, nil) in
                print(id!)
                
                let alert = UIAlertController(title: "Are you sure you want to remove ", message: " ", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
                alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action) in
                    self.handleDeleteProperty(id: id!)
                    print("delete")
                }))
                self.present(alert, animated: true, completion: nil)
            }
            
            delete.backgroundColor = UIColor.white // .red
            delete.image =  UIImage(named: "new_ic_delete")
            
            
            config = UISwipeActionsConfiguration(actions: [delete])
            config.performsFirstActionWithFullSwipe = false
        }else{
            //config = UISwipeActionsConfiguration(actions: [delete,copy])
            
        }
        return  config
        
    }
    
}
