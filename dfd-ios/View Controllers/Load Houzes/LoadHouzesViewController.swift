//
//  LoadHouzesViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 5/18/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit

import Sentry

class LoadHouzesViewController: UIViewController {
    
    @IBOutlet weak var propertiesByTagButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // SocketIOManager.shared.disconnectSocket()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // FIXME: Remove code below if u're using your own menu
        setupNavForDefaultMenu()
        self.setUpNavigationBar()
        
        // Add left bar button item
        let leftBarItem = UIBarButtonItem(image: UIImage(named: "burger"), style: .plain, target: self, action: #selector(toggleSideMenu))
        navigationItem.leftBarButtonItem = leftBarItem
        leftBarItem.tintColor = .black
        
        self.title = "Load Houzes"
        propertiesByTagButton.tintColor = UIColor(red:0.13, green:0.67, blue:0.37, alpha:1)
        globalSelectedList = nil
        globalSelectedTag = nil
        globalSelectedMembersID.removeAll()
        isFilteredList = false

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func setupNavForDefaultMenu() {
        // Revert navigation bar translucent style to default
        //navigationBarNonTranslecentStyle()
        // Update side menu after reverted navigation bar style
        sideMenuManager?.instance()?.menu?.isNavbarHiddenOrTransparent = true
        navigationItem.hidesBackButton = true
    }
    
    @objc func toggleSideMenu() {
        sideMenuManager?.toggleSideMenuView()
    }
    
    @IBAction func propertiesByTagButtonAction(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "LoadHouzes", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "LoadHouzesContainerViewController") as! LoadHouzesContainerViewController
        vc.dropDownFlag = "Properties by Tag"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func propertiesByListButtonAction(_ sender: Any) {
        //Client.shared?.crash()

        let storyBoard : UIStoryboard = UIStoryboard(name: "LoadHouzes", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "LoadHouzesContainerViewController") as! LoadHouzesContainerViewController
        vc.dropDownFlag = "Properties by List"
        self.navigationController?.pushViewController(vc, animated: true)
        
        //        let storyBoard : UIStoryboard = UIStoryboard(name: "LoadHouzes", bundle:nil)
        //        let vc = storyBoard.instantiateViewController(withIdentifier: "PropertiesMapViewController") as! PropertiesMapViewController
        //        vc.dropDownFlag = "Properties by List"
        //        self.navigationController?.pushViewController(vc, animated: true)
    }
}
