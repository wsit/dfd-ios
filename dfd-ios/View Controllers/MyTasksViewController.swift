//
//  MyTasksViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 5/8/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit

class MyTasksViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // FIXME: Remove code below if u're using your own menu
        //setupNavForDefaultMenu()
        
        // Add left bar button item
//        let leftBarItem = UIBarButtonItem(image: UIImage(named: "burger"), style: .plain, target: self, action: #selector(toggleSideMenu))
//        navigationItem.leftBarButtonItem = leftBarItem
//        
//        self.title = "My Tasks"
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func setupNavForDefaultMenu() {
        // Revert navigation bar translucent style to default
        //navigationBarNonTranslecentStyle()
        // Update side menu after reverted navigation bar style
        sideMenuManager?.instance()?.menu?.isNavbarHiddenOrTransparent = true
        navigationItem.hidesBackButton = true
    }
    
    @objc func toggleSideMenu() {
        //sideMenuManager?.toggleSideMenuView()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
