//
//  CardPaymentViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 1/11/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//

import UIKit
import Caishen
//import Stripe
import Alamofire
import SwiftyJSON
import SwiftLuhn


class CardPaymentViewController: UIViewController, UITextFieldDelegate {
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textFieldText = textField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
        }
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        return count <= 4
    }
    
    
    @IBOutlet weak var cardNumberTextField: NumberInputTextField!
    @IBOutlet weak var cvcInputTextField: UITextField!//CVCInputTextField!
    @IBOutlet weak var txtCardHolderName: UITextField!
    @IBOutlet weak var txtMonthYear: UITextField!
    
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblPlanName: UILabel!
    @IBOutlet weak var lblCardNameTitle: UILabel!
    @IBOutlet weak var lblCardNumber: UILabel!
    @IBOutlet weak var lblCVV: UILabel!
    @IBOutlet weak var lblExDate: UILabel!
    
    @IBOutlet weak var btnFlag: UIButton!
    @IBOutlet weak var btnConfirm: UIButton!
    
    var planID : Int = 0
    var amount: Double = 0.0
    var isSaved: Bool = true
    var isSaveSelected: Bool = true
    var selectedPlan : AllPlansResponseElement?
    
    var sendExpDateString: String = ""
    let expiryDatePicker = MonthYearPickerView()
    var finalCardNumber: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Add Wallet"
        btnConfirm.backgroundColor = self.buttonBackgroundColor
        
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [lblAmount], type: TextType.MainTitle.rawValue)
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [lblPlanName], type: TextType.ListSubTitle.rawValue)
        
        lblAmount.text = "$\(self.selectedPlan?.planCost ?? 0) paid monthly for \(self.selectedPlan?.planName ?? "") plan"
        let str = self.selectedPlan?.rechargeCardResponseDescription ?? ""
        let data = str.data(using: .utf8)!
        do{
            let arr = try JSON(data: data).arrayValue
            let desc = arr[0].stringValue
            lblPlanName.text = desc
            
        }catch let error{
            print(error.localizedDescription)
        }
        
        txtMonthYear.inputView = expiryDatePicker
        expiryDatePicker.onDateSelected = { (month: Int, year: Int) in
            let string = String(format: "%02d-%02d",month,year)
            let sendString = String(format: "%02d-%02d",year,month)
            print("sendString", sendString)
            NSLog("string", string) // should show something like 05/2015
            self.sendExpDateString = sendString
            self.txtMonthYear.text = string
        }
        
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [lblCardNameTitle, lblCVV, lblExDate, lblCardNumber], type: TextType.Capsule.rawValue)
        FontAndColorConfigs.setDynamicButtonsFontSize(buttons: [btnConfirm], type: TextType.ListTitle.rawValue)
        
        txtMonthYear.underlined()
        txtCardHolderName.underlined()
        cardNumberTextField.underlined()
        cvcInputTextField.underlined()
        cardNumberTextField.numberInputTextFieldDelegate = self
        cvcInputTextField.delegate = self
        
        self.view.backgroundColor = UIColor(red:0.13, green:0.10, blue:0.22, alpha:1.0)
        btnFlag.setImage(UIImage(named: "ios7-checkmark-outline"), for: .normal)
        
    }
    
    @IBAction func backFromNav(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func backClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func changeFlag(_ sender: Any) {
        
        if btnFlag.currentImage == UIImage(named: "ios7-checkmark-outline"){
            btnFlag.setImage(UIImage(named: "ios7-circle-outline"), for: .normal)
            self.isSaved = false
        }else{
            btnFlag.setImage(UIImage(named: "ios7-checkmark-outline"), for: .normal)
            self.isSaved = true
        }
    }
    
    
    @IBAction func confirmButtonClicked(_ sender: Any) {
        
        cardNumberTextField.numberInputTextFieldDelegate = self
        
        if txtCardHolderName.isEmpty == true || cardNumberTextField.isEmpty == true || cvcInputTextField.isEmpty == true || txtMonthYear.isEmpty == true
            //|| yearInputTextField.isEmpty == true || monthInputTextField.isEmpty == true
        {
            self.showAlert(message: "Please fill up all input fields")
        }else{
            
            let subString = cardNumberTextField.text?.replacingOccurrences(of: "-", with: "")//replacingOccurrences(of: "-", with: "")
            print(subString)
            let isValid = subString?.isValidCardNumber()
            if isValid == true{
                cardNumberTextField.textColor = .black
                self.finalCardNumber = subString!
                let url = UrlManager.baseURL() + UrlManager.apiString() + "/upgrade-profile/"
                self.handleConfirmAction(urlString: url )
                
            }else{
                cardNumberTextField.textColor = UIColor.red
                self.showAlert(message: "Invalid card number given")
            }
        }
    }
    
    func handleConfirmAction(urlString: String ){
        
        
        self.view.activityStartAnimating(activityColor: .black, backgroundColor: .white)
        
        let parameters = [
            "card_number" : self.finalCardNumber,
            "card_name" : self.txtCardHolderName.text ?? "",
            "card_code" : cvcInputTextField.text ?? "",
            "expiration_date" : self.sendExpDateString,
            "is_save" : self.isSaved,
            "plan" : self.planID
            ] as [String : Any]
        
        print(parameters)
        
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        print(urlString)
        
        Alamofire.request(urlString, method: .post, parameters: parameters as Parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
            case.success(let data):
                print("success",data)
                let statusCode = response.response?.statusCode
                print(statusCode!)
                let rechargeCardResponse = try? JSONDecoder().decode(RechargeCardResponse.self, from: response.data!)
                if statusCode == 200{
                    DispatchQueue.main.async {
                        self.view.activityStopAnimating()
                        self.removeSpinner()
                        
                        let alertController = UIAlertController(title: (rechargeCardResponse?.message)!, message:"", preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                                
                                let url : String = UrlManager.getCurrentUserInfoURL()
                                self.getCurrentUserInfo(urlString: url)
                                
                            }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion: nil)
                        
                    }
                }
                else if statusCode == 401{
                    self.view.activityStopAnimating()
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: (rechargeCardResponse?.message)!, textColor: .green)
                }
                else{
                    self.view.activityStopAnimating()
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: (rechargeCardResponse?.message)!, textColor: .green)
                }
                
            case.failure(let error):
                self.view.activityStopAnimating()
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)

                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
                self.removeSpinner()
            }
            self.removeSpinner()
        }
        
    }
    
    
    func getCurrentUserInfo(urlString:String) {
        print(urlString)
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success",data)
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    
                    DispatchQueue.main.async {
                        let currentUser = try? JSONDecoder().decode(CurrentUserInfoResponse.self, from: response.data!)
                        AppConfigs.saveCurrentUserInfo(user: currentUser!)
                        print(AppConfigs.getCurrentUserInfo().email!)
                        
                        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
                        guard let vc = storyBoard.instantiateViewController(withIdentifier: "SMNavigationController") as? SMNavigationController  else
                        { return  }
                        vc.modalPresentationStyle = .fullScreen
                        self.present(vc, animated: true, completion: nil)
                        
                    }
                }
                    
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)

                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
    
    
}

extension CardPaymentViewController: NumberInputTextFieldDelegate, CardInfoTextFieldDelegate {
    
    // A card that is not nil when valid information has been entered in the text fields:
    var card: Card? {
        let number = cardNumberTextField.cardNumber
        let cvc = CVC(rawValue: cvcInputTextField.text ?? "")
        let expiry = Expiry(month:  "02", year:   "2020")
            ?? Expiry.invalid
        
        let cardType = cardNumberTextField.cardTypeRegister.cardType(for: cardNumberTextField.cardNumber)
        if cardType.validate(cvc: cvc).union(cardType.validate(expiry: expiry)).union(cardType.validate(number: number)) == .Valid {
            return Card(number: number, cvc: cvc, expiry: expiry)
        } else {
            return nil
        }
    }
    
    func numberInputTextFieldDidComplete(_ numberInputTextField: NumberInputTextField) {
        //cvcInputTextField.cardType = numberInputTextField.cardTypeRegister.cardType(for: numberInputTextField.cardNumber)
        
        print("Card number: \(numberInputTextField.cardNumber)")
        print(card)
        cvcInputTextField.becomeFirstResponder()
    }
    
    func numberInputTextFieldDidChangeText(_ numberInputTextField: NumberInputTextField) {
        
        
    }
    
    func textField(_ textField: UITextField, didEnterValidInfo: String) {
        switch textField {
        case is MonthInputTextField:
            print("Month: \(didEnterValidInfo)")
            
        case is YearInputTextField:
            print("Year: \(didEnterValidInfo)")
        // monthInputTextField.becomeFirstResponder()
        case is CVCInputTextField:
            //yearInputTextField.becomeFirstResponder()
            print("CVC: \(didEnterValidInfo)")
        default:
            break
        }
        print(card)
    }
    
    func textField(_ textField: UITextField, didEnterPartiallyValidInfo: String) {
        // The user entered information that is not valid but might become valid on further input.
        // Example: Entering "1" for the CVC is partially valid, while entering "a" is not.
    }
    
    func textField(_ textField: UITextField, didEnterOverflowInfo overFlowDigits: String) {
        // This function is used in a CardTextField to carry digits to the next text field.
        // Example: A user entered "02/20" as expiry and now tries to append "5" to the month.
        //          On a card text field, the year will be replaced with "5" - the overflow digit.
    }
    
    // ...
    
    func cardTextFieldShouldShowAccessoryImage(_ cardTextField: CardTextField) -> UIImage? {
        return UIImage(named: "bt_ic_visa")
    }
    
    func cardTextFieldShouldProvideAccessoryAction(_ cardTextField: CardTextField) -> (() -> ())? {
        return nil
    }
    
}
