//
//  UpgradePopUpTableViewController.swift
//  HouZes
//
//  Created by Mahadhi Hassan Chowdhury on 2/13/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//

import UIKit
import YoutubePlayer_in_WKWebView

class UpgradePopUpTableViewController: UITableViewController {
    
    @IBOutlet weak var lblHeaderTxt: UILabel!
    @IBOutlet weak var btnFirstUpgrade: UIButton!
    @IBOutlet weak var btnSecondUpgrade: UIButton!
    @IBOutlet weak var btnSkip: UIButton!
    
    @IBOutlet weak var youtubeVideoPlayer: WKYTPlayerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblHeaderTxt.text = "Upgrade now to access all the top features"
        btnFirstUpgrade.backgroundColor = self.buttonBackgroundColor
       
        btnSecondUpgrade.backgroundColor = self.buttonBackgroundColor
       // youtubeVideoPlayer.load(withVideoId: "Mc0TMWYTU_k")
    }
    
    @IBAction func upgradeOneClicked(_ sender: Any) {
        goToPackageView()
    }
    
    @IBAction func upgradeTwoClicked(_ sender: Any) {
        goToPackageView()
        
    }
    
    @IBAction func skipButtonClicked(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func goToPackageView() {
//        let settingsSB: UIStoryboard = UIStoryboard(name: "Settings", bundle: nil)
//        let vc = settingsSB.instantiateViewController(withIdentifier: "PackageViewController") as! PackageViewController
//        vc.modalPresentationStyle = .fullScreen
        //self.navigationController?.pushViewController(vc, animated: true)
        //self.present(vc, animated: true, completion: nil)
         self.dismiss(animated: true, completion: nil)
        NotificationCenter.default.post(name:NSNotification.Name("UpgradeClicked"),object: nil)

        
        //nViewController =  settingsSB.instantiateViewController(withIdentifier: "PackageViewController")
    }
     
    
    /*
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
     
     // Configure the cell...
     
     return cell
     }
     */
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
