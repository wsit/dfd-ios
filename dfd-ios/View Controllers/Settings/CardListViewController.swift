//
//  CardListViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 1/14/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//

import UIKit
import Alamofire

class CardListViewController: UIViewController {
    
    @IBOutlet weak var cardTypeTV: UITableView!
    
    var allCards: [AllCardsResponseElement] = []
    var jsonResponse : AllCardsResponse?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: "CardsTableViewCell", bundle: nil)
        cardTypeTV.register(nib, forCellReuseIdentifier: "CardsTableViewCell")
        
        self.cardTypeTV.tableFooterView = UIView()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        if Network.reachability.status == .unreachable{
            AppConfigs.updateUserInterface(view: view)
        }else{
            allCards.removeAll()
            let url : String = UrlManager.baseURL() + UrlManager.apiString() + "/billing-card/"
            print(url)
            //self.showSpinner(onView: self.view)
            getAllCards(urlString: url)
        }
        
        //       }
        
    }
    
    @IBAction func backToPreviousVC(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func getAllCards(urlString: String) {
        
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success",data)
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    
                    let allCardsResonse = try? JSONDecoder().decode([AllCardsResponseElement].self, from: response.data!)//AllPlansResponse
                    //self.jsonResponse = allPlans
                    self.allCards = allCardsResonse ?? []
                    DispatchQueue.main.async {
                        print("allPlans------------\n",self.allCards)
                        self.removeSpinner()
                        self.cardTypeTV.reloadData()
                    }
                }
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                
                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
    
}

extension CardListViewController: UITableViewDataSource {
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if allCards.count == 0 {
            tableView.setEmptyMessage("Nothing yet to show")
        } else {
            tableView.restore()
        }
        return allCards.count
        // return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CardsTableViewCell", for: indexPath) as! CardsTableViewCell
        
        cell.upView.cornerRadius = 8
        cell.lblCardNumber.textColor = UIColor(red:0, green:0, blue:0, alpha:0.8)
        //cell.lblExpDate.textColor = UIColor(red:0.64, green:0.64, blue:0.64, alpha:1)
        cell.upView.backgroundColor = UIColor(red:0.95, green:0.96, blue:1.00, alpha:1.0)
        cell.cardImageView.image = UIImage(named: "new_ic_names")
        
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [cell.lblCardNumber], type: TextType.ListTitle.rawValue)
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [cell.lblExpDate, cell.lblName], type: TextType.ListSubTitle.rawValue)
        
        
        if allCards.count > indexPath.row{
            let card = allCards[indexPath.row]

            var resultString = String()
            card.cardNumber?.enumerated().forEach { (index, character) in

                // Add space every 4 characters
                if index % 4 == 0 && index > 0 {
                    resultString += "-"
                }

                if index < 12 {
                    // Replace the first 12 characters by *
                    resultString += "X"
                } else {
                    // Add the last 4 characters to your final string
                    resultString.append(character)
                }

            }
            
            cell.lblCardNumber.text = resultString
            
            
            cell.lblExpDate.text = card.expDate
            cell.lblName.text = card.cardName ?? ""
            
            let (type, formatted, valid) = checkCardNumber(input: card.cardNumber ?? "")
            
            print(type.rawValue)
            cell.lblCardType.text = type.rawValue
            //Unknown, Amex, Visa, MasterCard, Diners, Discover, JCB, Elo, Hipercard, UnionPay
            if type.rawValue == CardType.Amex.rawValue{
                print("You are Amex")
                cell.cardImageView.image = UIImage(named: "bt_ic_amex")
            }else if type.rawValue == CardType.Visa.rawValue{
                print("You are Visa")
                cell.cardImageView.image = UIImage(named: "bt_ic_visa")
            }else if type.rawValue == CardType.MasterCard.rawValue{
                cell.cardImageView.image = UIImage(named: "bt_ic_mastercard")
                print("You are master")
            }else if type.rawValue == CardType.Diners.rawValue{
                print("You are Diners")
                cell.cardImageView.image = UIImage(named: "bt_ic_diners_club")
            }else if type.rawValue == CardType.Discover.rawValue{
                cell.cardImageView.image = UIImage(named: "bt_ic_discover")
                print("You are Discover")
            } else if type.rawValue == CardType.JCB.rawValue{
                cell.cardImageView.image = UIImage(named: "bt_ic_jcb")
                print("You are JCB")
            }else if type.rawValue == CardType.Elo.rawValue{
                print("You are Elo")
                cell.cardImageView.image = UIImage(named: "bt_ic_visa")
                
            }else if type.rawValue == CardType.Hipercard.rawValue{
                print("You are Hipercard")
                cell.cardImageView.image = UIImage(named: "bt_ic_hipercard")
                
            }else if type.rawValue == CardType.UnionPay.rawValue{
                print("You are Unionpay")
                cell.cardImageView.image = UIImage(named: "bt_ic_unionpay")
                
            }else{
                print("Unknown")
                cell.cardImageView.image = UIImage(named: "bt_ic_visa")
            }
            
        }
        //cell.lblScoutName.text = items[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row ==  allCards.count - 1{
            
            
        }
    }
    
}

extension CardListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if allCards.count > indexPath.row{
            let card = allCards[indexPath.row]
            let dataDict : [String: AllCardsResponseElement] = ["card": card]
            self.dismiss(animated: true, completion: nil)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "cardSelected"), object: nil, userInfo: dataDict)
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        
    }
    
    
    
    
}
