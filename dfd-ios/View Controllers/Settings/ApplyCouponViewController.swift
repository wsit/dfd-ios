//
//  ApplyCouponViewController.swift
//  HouZes
//
//  Created by Mahadhi Hassan Chowdhury on 7/13/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ApplyCouponViewController: UIViewController {
    
    @IBOutlet weak var txtCoupon: TextField!
    @IBOutlet weak var btnApply: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtCoupon.underlined()
        btnApply.backgroundColor = self.buttonBackgroundColor
        // Do any additional setup after loading the view.
        self.title = "Apply Coupon"
        print("discount",AppConfigs.getCurrentUserInfo().upgradeInfo?.discount ?? 0)
        
    }
    
    @IBAction func backCLicked(_ sender: Any) {
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func applyClicked(_ sender: Any) {
        if txtCoupon.text == ""{
            AppConfigs.showSnacbar(message: "Please insert your coupon code!", textColor: .red)
        }else{
            handleApplyCoupon()
        }
    }
    
    func handleApplyCoupon() {
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()
        ]
        
        let parameters = [
            
            K.APIParameterKey.coupon: txtCoupon.text!,
        ]
        
        print(parameters)
        //save this email to shared pref
        //AppConfigs.saveEmailAddress(email: txtUserName.text!)
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        
        self.view.activityStartAnimating(activityColor: .black, backgroundColor: .white)
        print(UrlManager.applyCouponURL())
        Alamofire.request(UrlManager.applyCouponURL(), method: .post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                print("success",data)
                self.view.activityStopAnimating()
                
                let statusCode = response.response?.statusCode
                
                let applyCouponResponse = try? JSONDecoder().decode(ApplyCouponResponse.self, from: response.data!)
//NetworkResponseModel
                
                if statusCode == 200 {
                    //save access token and refresh token
                    
                    if applyCouponResponse?.status == false{
                        self.showAlert(message:(applyCouponResponse?.message ?? ""), title: "Error!")

                    }else{
                        DispatchQueue.main.async {
                            
                            let currentUser = try? JSONDecoder().decode(CurrentUserInfoResponse.self, from: response.data!)
                            
                            if currentUser == nil{
                                print("calling current user api")
                                let url : String = UrlManager.getCurrentUserInfoURL()
                                self.getCurrentUserInfo(urlString: url)
                            }else{
                                
                                AppConfigs.saveCurrentUserInfo(user: currentUser!)
                                let url : String = UrlManager.getCurrentUserInfoURL()
                                self.getCurrentUserInfo(urlString: url)
                                
                                print(AppConfigs.getCurrentUserInfo().email)
                                print("discount",AppConfigs.getCurrentUserInfo().upgradeInfo?.discount ?? 0)
                                
                                let alertController = UIAlertController(title: (applyCouponResponse?.message), message:"", preferredStyle: .alert)
                                
                                let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                                    self.navigationController?.popViewController(animated: true)
                                     
                                }
                                alertController.addAction(OKAction)
                                self.present(alertController, animated: true, completion: nil)
                                
                            }
                        }
                    }
                    
                }
                else if statusCode == 400
                {
                                    
                    let applyCouponResponse = try? JSONDecoder().decode(NetworkResponseModel.self, from: response.data!)
                    //NetworkResponseModel
                    self.showAlert(message:(applyCouponResponse?.message ?? ""), title: "Error!")
                    
                }else if statusCode == 404
                {
                    let applyCouponResponse = try? JSONDecoder().decode(NetworkResponseModel.self, from: response.data!)
                    self.showAlert(message:(applyCouponResponse?.message ?? "Incorrect coupon code"), title: "Error!")
                }else{
                    self.showAlert(message: "Oopps! Something went wrong!", title: "Error!")
                }
                
            case.failure(let error):
                self.view.activityStopAnimating()
                print("Not Success",error)
                self.removeSpinner()
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                //AppConfigs.showSnacbar(message: error.localizedDescription, textColor: .red)
                
            }
            
            self.view.activityStopAnimating()
        }
        
    }
    
    func getCurrentUserInfo(urlString:String) {
        print(urlString)
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success",data)
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    
                    DispatchQueue.main.async {
                        
                        let currentUser = try? JSONDecoder().decode(CurrentUserInfoResponse.self, from: response.data!)
                        AppConfigs.saveCurrentUserInfo(user: currentUser!)
                        print(AppConfigs.getCurrentUserInfo().email!)
                        print("discount",AppConfigs.getCurrentUserInfo().upgradeInfo?.discount ?? 0)

                         
                    }
                }
                    
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                
                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
}
