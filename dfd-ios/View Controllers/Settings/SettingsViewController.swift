//
//  SettingsViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 1/11/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//

import UIKit

import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn

class SettingsViewController: UIViewController {
    
    @IBOutlet weak var settingTableView: UITableView!
    @IBOutlet weak var lblVersionNo: UILabel!
    
    var items = [ "Welcome Tutorial", "Contact Support", "Terms and Services", "Privacy Policy", "Add $", "Apply Coupon", "Help", "Logout"]
    //["Open Houzes Web", "Welcome Tutorial", "Contact Support", "Terms and Services", "Privacy Policy", "Help", "Upgrade", "Add $", "Logout"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let nib = UINib(nibName: "PropertyDetailsTableViewCell", bundle: nil)
        settingTableView.register(nib, forCellReuseIdentifier: "PropertyDetailsTableViewCell")
        
        self.settingTableView.tableFooterView = UIView()
        
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        let build = dictionary["CFBundleVersion"] as! String
        
        lblVersionNo.text = "Version \(version).0.\(build)"
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // FIXME: Remove code below if u're using your own menu
        setupNavForDefaultMenu()
        self.setUpNavigationBar()
        // Add left bar button item
        let leftBarItem = UIBarButtonItem(image: UIImage(named: "burger"), style: .plain, target: self, action: #selector(toggleSideMenu))
        navigationItem.leftBarButtonItem = leftBarItem
        leftBarItem.tintColor = .black
        self.title = "Settings"
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func setupNavForDefaultMenu() {
        // Revert navigation bar translucent style to default
        //navigationBarNonTranslecentStyle()
        // Update side menu after reverted navigation bar style
        sideMenuManager?.instance()?.menu?.isNavbarHiddenOrTransparent = true
        navigationItem.hidesBackButton = true
    }
    
    @objc func toggleSideMenu() {
        sideMenuManager?.toggleSideMenuView()
    }
    
}

extension SettingsViewController: UITableViewDataSource {
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PropertyDetailsTableViewCell", for: indexPath) as! PropertyDetailsTableViewCell
        
        
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [cell.lblItemTitle], type: TextType.ListTitle.rawValue)
        
        cell.lblItemCount.layer.masksToBounds = true
        cell.lblItemTitle?.textColor = UIColor(red:0, green:0, blue:0, alpha:0.8)
        cell.lblItemCount.isHidden = true
        cell.isUserInteractionEnabled = true
        
        cell.itemImageView.tintColor = UIColor(red:0.2, green:0.3, blue:0.41, alpha:1)
        
        
        if items.count > indexPath.row{
            cell.lblItemTitle.text = items[indexPath.row]
            if items[indexPath.row] == "Upgrade"{
                cell.lblItemCount.isHidden = false
                cell.lblItemCount.text = AppConfigs.getCurrentUserInfo().upgradeInfo?.plan?.planName
                
                if AppConfigs.getCurrentUserInfo().upgradeInfo?.plan?.id == 2{
                    cell.isUserInteractionEnabled = false
                    cell.lblItemTitle.textColor = .lightGray
                }else{
                    cell.lblItemTitle.textColor = .black
                    cell.isUserInteractionEnabled = true
                }
            }
        }
        
        switch items[indexPath.row] {
            //["Open Houzes Web", "Welcome Tutorial", "Contact Support", "Terms and Services", "Privacy Policy", "Help", "Upgrade", "Add $", "Logout"]
            
        case "Open Houzes Web":
            if let myImage = UIImage(named: "wios") {
                let tintableImage = myImage.withRenderingMode(.alwaysOriginal)
                cell.itemImageView.image = tintableImage
            }
            
            break
            
        case "Welcome Tutorial":
            if let myImage = UIImage(named: "wtuts") {
                let tintableImage = myImage.withRenderingMode(.alwaysOriginal)
                cell.itemImageView.image = tintableImage
            }
            
            break
            
            
        case "Contact Support":
            
            if let myImage = UIImage(named: "wsupport") {
                let tintableImage = myImage.withRenderingMode(.alwaysOriginal)
                cell.itemImageView.image = tintableImage
            }
            
            break
            
            
        case "Terms and Services":
            if let myImage = UIImage(named: "wterms") {
                let tintableImage = myImage.withRenderingMode(.alwaysOriginal)
                cell.itemImageView.image = tintableImage
            }
            break
            
            
        case "Privacy Policy":
            if let myImage = UIImage(named: "wprivacy") {
                let tintableImage = myImage.withRenderingMode(.alwaysOriginal)
                cell.itemImageView.image = tintableImage
            }
            break
            
            
        case "License":
            
            if let myImage = UIImage(named: "wlicence") {
                let tintableImage = myImage.withRenderingMode(.alwaysOriginal)
                cell.itemImageView.image = tintableImage
            }
            break
            
        case "Help":
            if let myImage = UIImage(named: "whelp") {
                let tintableImage = myImage.withRenderingMode(.alwaysOriginal)
                cell.itemImageView.image = tintableImage
            }
            
            break
            
        case "Upgrade":
            if let myImage = UIImage(named: "wupgrade") {
                let tintableImage = myImage.withRenderingMode(.alwaysOriginal)
                cell.itemImageView.image = tintableImage
            }
            break
            
            
        case "Add $":
            if let myImage = UIImage(named: "wwallet") {
                let tintableImage = myImage.withRenderingMode(.alwaysOriginal)
                cell.itemImageView.image = tintableImage
            }
            
            break
            
            
        case "Apply Coupon":
            if let myImage = UIImage(named: "v3_ic_coupon") {
                let tintableImage = myImage.withRenderingMode(.alwaysOriginal)
                cell.itemImageView.image = tintableImage
            }
            
            break
            
        case "Logout":
            if let myImage = UIImage(named: "wlogout") {
                let tintableImage = myImage.withRenderingMode(.alwaysOriginal)
                cell.itemImageView.image = tintableImage
            }
            break
            
            
        default: break
        }
        
        return cell
        
    }
}

extension SettingsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //let cell = tableView.cellForRow(at: indexPath) as! PropertyDetailsTableViewCell
        let cell = tableView.cellForRow(at: indexPath) as! PropertyDetailsTableViewCell
        cell.lblItemCount.textColor =  UIColor(red:0.52, green:0.55, blue:0.57, alpha:1)
        cell.backgroundColor = UIColor.white
        cell.contentView.backgroundColor = UIColor.white
        let settingsSB: UIStoryboard = UIStoryboard(name: "Settings", bundle: nil)
        
        switch items[indexPath.row] {
        //["Open With Web Browser", "iOS Guide", "Contact Support", "Terms and Services", "Privacy Policy", "License", "Upgrade", "Logout"]
        case "Open Houzes Web":
            
            guard let url = URL(string: "https://houzes.com/") else {
                return //be safe
            }
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
            
            break
            
        case "Welcome Tutorial":
            //            guard let url = URL(string: "https://houzes.com/") else {
            //                return //be safe
            //            }
            //
            //            if #available(iOS 10.0, *) {
            //                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            //            } else {
            //                UIApplication.shared.openURL(url)
            //            }
            let storyBoard = UIStoryboard(name: "Tuts", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "TutsViewController") as! TutsViewController
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
            
            break
            
            
        case "Contact Support":
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ContactSupportViewController") as! ContactSupportViewController
            vc.modalPresentationStyle = .overCurrentContext
            self.present(vc, animated: true, completion:nil)
            
            break
            
            
        case "Terms and Services":
            guard let url = URL(string: UrlManager.getTermsandServicesURL()) else {
                return //be safe
            }
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
            
            break
            
            
        case "Privacy Policy":
            guard let url = URL(string: UrlManager.getTermsandServicesURL()) else {
                return //be safe
            }
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
            
            
            break
            
            
        case "License":
            guard let url = URL(string: UrlManager.getTermsandServicesURL()) else {
                return //be safe
            }
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
            
            break
            
            
        case "Help":
            
            guard let url = URL(string: "https://help.houzes.com") else {
                return //be safe
            }
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
            
            break
            
        case "Add $":
            if AppConfigs.getCurrentUserInfo().isAdmin == true{
                if AppConfigs.getCurrentUserInfo().thinker == true{
                    
                    let main = UIStoryboard(name: "Settings", bundle: nil)
                    let vc = main.instantiateViewController(withIdentifier: "RechargeForIAPViewController") as! RechargeForIAPViewController
                    //self.navigationController?.pushViewController(vc, animated: true)
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true, completion: nil)
                }else{
                    
                    let main = UIStoryboard(name: "Main", bundle: nil)
                    let vc = main.instantiateViewController(withIdentifier: "RechargeViewController") as! RechargeViewController
                    //self.navigationController?.pushViewController(vc, animated: true)
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true, completion: nil)
                }
                
            }else{
                AppConfigs.showSnacbar(message: ErrorMessage.InsufficientBalance.rawValue, textColor: .red)
            }
            break
            
        case "Apply Coupon":
            
            if AppConfigs.getCurrentUserInfo().isAdmin == true{

                if AppConfigs.getCurrentUserInfo().upgradeInfo?.discount != nil{
                    AppConfigs.showSnacbar(message: "You have already applied your coupon!", textColor: .red)

                }else{

                    let vc = settingsSB.instantiateViewController(withIdentifier: "ApplyCouponViewController") as! ApplyCouponViewController
                    //self.navigationController?.pushViewController(vc, animated: true)
                    //vc.modalPresentationStyle = .fullScreen
                    self.navigationController?.pushViewController(vc, animated: true)
                    //self.present(vc, animated: true, completion: nil)
                }
            }else{
                AppConfigs.showSnacbar(message: "You don't have permission for this! Contact with your Admin.", textColor: .red)
            }
            
            break
            
            
        case "Upgrade":
            
            let vc = settingsSB.instantiateViewController(withIdentifier: "PackageViewController") as! PackageViewController
            //self.navigationController?.pushViewController(vc, animated: true)
            //vc.modalPresentationStyle = .fullScreen
            self.navigationController?.pushViewController(vc, animated: true)
            //self.present(vc, animated: true, completion: nil)
            
            break
            
        case "Logout":
            
            if isGlobalDriveNowSelected == true{
                self.showAlert(message: "Please stop your drive before logout.")
            }else{
                
                if let bundleID = Bundle.main.bundleIdentifier {
                    UserDefaults.standard.removePersistentDomain(forName: bundleID)
                }
                print("AppConfigs.getSavedAccessToken()---> ", AppConfigs.getSavedAccessToken())
                
                if AccessToken.current != nil{
                    LoginManager().logOut()
                }
                
                if GIDSignIn.sharedInstance()?.currentUser != nil{
                    GIDSignIn.sharedInstance().signOut()
                }
                
                
                if let bundleID = Bundle.main.bundleIdentifier {
                    UserDefaults.standard.removePersistentDomain(forName: bundleID)
                }
                print("AppConfigs.getSavedAccessToken()---> ", AppConfigs.getSavedAccessToken())
                
                
                AppConfigs.setRootViewController()
            }
            
            break
            
            
        case "Billing":
            
            break
            
        default: break
        }
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if self.items.count > indexPath.row{
            
            
            
        }
        
    }
}
