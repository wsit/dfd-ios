//
//  RechargeViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 1/13/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//

import UIKit
import Caishen
import Alamofire
import SwiftyJSON

import SwiftLuhn

var selectedCard:AllCardsResponseElement? = nil

class RechargeViewController: UIViewController , UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textFieldText = textField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
        }
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        return count <= 4
    }
    
    
    @IBOutlet weak var cardNumberTextField: NumberInputTextField!
    @IBOutlet weak var cvcInputTextField: UITextField!//CVCInputTextField!
    @IBOutlet weak var txtCardHolderName: UITextField!
    @IBOutlet weak var txtMonthYear: UITextField!
    @IBOutlet weak var lblCardNameTitle: UILabel!
    @IBOutlet weak var lblCardNumberTitle: UILabel!
    @IBOutlet weak var lblCVV: UILabel!
    @IBOutlet weak var lblExDate: UILabel!
    @IBOutlet weak var lblSaveCardTitle: UILabel!
    @IBOutlet weak var lblCardholderName: UILabel!
    
    @IBOutlet weak var btnFlag: UIButton!
    
    var planID : Int = 0
    var amount: Double = 0.0
    var isSaved: Bool = true
    var isSaveSelected: Bool = true
    var selectedPlan : AllPlansResponseElement?
    var selectedPlanPrice : Double? = 0.0

    var sendExpDateString: String = ""
    let expiryDatePicker = MonthYearPickerView()
    var finalCardNumber: String = ""
    var defCardHolName : String = ""
    
    var parentString : String = ""
    
    @IBOutlet weak var lblAmountTitle: UILabel!
    @IBOutlet weak var txtAmount: UITextField!
    @IBOutlet weak var btnDropdownIcon: UIButton!
    @IBOutlet weak var btnDropDownText: UIButton!
    
    @IBOutlet weak var inputInfoView: UIView!
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var heightForCardView: NSLayoutConstraint!
    @IBOutlet weak var lblCardNumber: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var cardTypeImageView: UIImageView!
    @IBOutlet weak var lblExpDate: UILabel!
    @IBOutlet weak var lblSecuirityCode: UILabel!
    @IBOutlet weak var txtCvv: UITextField!
    @IBOutlet weak var cardInputView: UIView!
    @IBOutlet weak var heightForCardInputView: NSLayoutConstraint!
    @IBOutlet weak var btnPlus: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var containerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollViewHeight: UIScrollView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var amountTitleHeight: NSLayoutConstraint!
    @IBOutlet weak var amountTextHeight: NSLayoutConstraint!
    @IBOutlet weak var lblSubText: UILabel!
    @IBOutlet weak var subTextHeight: NSLayoutConstraint!
    @IBOutlet weak var subTextTopHeight: NSLayoutConstraint!
    
    let mask = "##/##"
    var isCardAvailable: Bool = true
    
    var allCards: [AllCardsResponseElement] = []
    var jsonResponse : AllCardsResponse?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [lblCardNameTitle,lblCardNumberTitle, lblCVV,lblExDate, lblSaveCardTitle, lblSecuirityCode,lblCardholderName ], type: TextType.ListSubTitle.rawValue)
        
        FontAndColorConfigs.setDynamicButtonsFontSize(buttons: [btnDropDownText, btnConfirm], type: TextType.ListTitle.rawValue)
        
        btnConfirm.backgroundColor = self.buttonBackgroundColor
        
        self.hideInputFields()
        
        
        btnConfirm.backgroundColor = self.buttonBackgroundColor
        
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [lblAmount], type: TextType.MainTitle.rawValue)
        
        //lblAmountTitle.text = "$\(self.selectedPlan?.planCost ?? 0).0 paid monthly for \(self.selectedPlan?.planName ?? "") plan"
        let str = self.selectedPlan?.rechargeCardResponseDescription ?? ""
        let data = str.data(using: .utf8)!
        do{
            let arr = try JSON(data: data).arrayValue
            let desc = arr[0].stringValue
            //lblPlanName.text = desc
        }catch let error{
            print(error.localizedDescription)
        }
        
        txtMonthYear.inputView = expiryDatePicker
        expiryDatePicker.onDateSelected = { (month: Int, year: Int) in
            let string = String(format: "%02d-%02d",month,year)
            let sendString = String(format: "%02d-%02d",month,year)
            print("sendString", sendString)
            NSLog("string", string) // should show something like 05/2015
            self.sendExpDateString = sendString
            self.txtMonthYear.text = string
        }
        
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [lblCardNameTitle, lblCVV, lblExDate, lblCardNumber, lblCardholderName], type: TextType.Capsule.rawValue)
        FontAndColorConfigs.setDynamicButtonsFontSize(buttons: [btnConfirm], type: TextType.ListTitle.rawValue)
        
        txtMonthYear.underlined()
        txtCardHolderName.underlined()
        cardNumberTextField.underlined()
        cvcInputTextField.underlined()
        cardNumberTextField.numberInputTextFieldDelegate = self
        cvcInputTextField.delegate = self
        txtCvv.delegate = self
        
        self.view.backgroundColor = UIColor(red:0.13, green:0.10, blue:0.22, alpha:1.0)
        btnFlag.setImage(UIImage(named: "ios7-checkmark-outline"), for: .normal)
        self.isSaved = true
        
        self.containerView.backgroundColor = UIColor(red:0.13, green:0.10, blue:0.22, alpha:1.0)
        self.view.backgroundColor = UIColor(red:0.13, green:0.10, blue:0.22, alpha:1.0)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleCardSelectAction(_:)), name: NSNotification.Name(rawValue: "cardSelected"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        if Network.reachability.status == .unreachable{
            AppConfigs.updateUserInterface(view: view)
        }else{
            allCards.removeAll()
            let url : String = UrlManager.baseURL() + UrlManager.apiString() + "/billing-card/"
            print(url)
            //self.showSpinner(onView: self.containerView)
            getAllCards(urlString: url)
        }

        self.containerViewHeight.constant = 700
        self.containerView.layoutIfNeeded()
        
        if self.parentString == "Plan"{
           
            self.lblTitle.text = "Subscription"
            self.lblAmountTitle.text = String(format: "$%.2f", selectedPlanPrice!)  + " paid monthly for \(self.selectedPlan?.planName ?? "") plan"
            
            //"$\(self.selectedPlan?.planCost ?? 0) paid monthly for \(self.selectedPlan?.planName ?? "") plan" //self.selectedPlan?.planName
            
            self.amountTitleHeight.constant = 50
            
            //lblAmount.text = "$\(self.selectedPlan?.planCost ?? 0).0 paid monthly for \(self.selectedPlan?.planName ?? "") plan"
            let str = self.selectedPlan?.rechargeCardResponseDescription ?? ""
            let data = str.data(using: .utf8)!
            do{
                let arr = try JSON(data: data).arrayValue
                let desc = arr[0].stringValue
                //lblPlanName.text = desc
                self.lblSubText.text = desc
                self.subTextHeight.constant = 40
            }catch let error{
                print(error.localizedDescription)
            }
            
            FontAndColorConfigs.setDynamicLabelFontSize(labels: [lblAmountTitle], type: TextType.ListTitle.rawValue)
            FontAndColorConfigs.setDynamicLabelFontSize(labels: [lblSubText], type: TextType.ListSubTitle.rawValue)
            
            self.txtAmount.isHidden = true
            self.amountTextHeight.constant = 0
            self.subTextTopHeight.constant = -25
            
//            self.lblSubText.text = ""
//            self.subTextHeight.constant = 0
            
        }else{
             self.lblTitle.text = "Add Wallet"
        }
                
    }
    
    func hideInputFields()  {
        lblCardNameTitle.isHidden = true
        lblCardNumberTitle.isHidden = true
        lblCardholderName.isHidden = true
        lblCVV.isHidden = true
        lblExDate.isHidden = true
        
        txtMonthYear.isHidden = true
        cvcInputTextField.isHidden = true
        cardNumberTextField.isHidden = true
        txtCardHolderName.isHidden = true
        btnFlag.isHidden = true
        lblSaveCardTitle.isHidden = true
        
    }
    
    func showInputFields()  {
        lblCardNameTitle.isHidden = false
        lblCardNumberTitle.isHidden = false
        lblCardholderName.isHidden = false
        lblCVV.isHidden = false
        lblExDate.isHidden = false
        
        txtMonthYear.isHidden = false
        cvcInputTextField.isHidden = false
        cardNumberTextField.isHidden = false
        txtCardHolderName.isHidden = false
        btnFlag.isHidden = false
        lblSaveCardTitle.isHidden = false
        
    }
    
    
    @objc func handleCardSelectAction(_ notification: NSNotification) {
        
        print(notification.userInfo ?? "")
        var resultString = String()
        
        if let dict = notification.userInfo as NSDictionary? {
            if let card = dict["card"] as? AllCardsResponseElement{
                self.isCardAvailable = true
                self.setUpConstraint()
                self.lblAmount.text = "$" + (self.txtAmount.text ?? "0.0")

                if parentString == "Plan"{
                    if self.selectedPlan?.planCost != nil{
                        self.lblAmount.text = "$" + "\(self.selectedPlan?.planCost ?? 0)"

                    }else{
                        self.lblAmount.text = ""

                    }
                }
                
                if card.cardNumber != nil{

                    self.finalCardNumber = card.cardNumber!
                    self.finalCardNumber.enumerated().forEach { (index, character) in
                        // Add space every 4 characters
                        if index % 4 == 0 && index > 0 {
                            resultString += "-"
                        }

                        if index < 12 {
                            // Replace the first 12 characters by *
                            resultString += "X"
                        } else {
                            // Add the last 4 characters to your final string
                            resultString.append(character)
                        }
                    }
                    let name = card.cardName
                    self.lblCardNumber.text = resultString + "\n\(name ?? "")"
                    self.lblCardholderName.text = card.cardName
                    self.defCardHolName = card.cardName!
                    print(" self.defCardHolName",  self.defCardHolName)
                }
                
                self.lblExpDate.text = card.expDate
                let (type, formatted, valid) = checkCardNumber(input: card.cardNumber ?? "")
                
                print(type.rawValue)
                if type.rawValue == CardType.Amex.rawValue{
                    print("You are Amex")
                    self.cardTypeImageView.image = UIImage(named: "bt_ic_amex")
                }else if type.rawValue == CardType.Visa.rawValue{
                    print("You are Visa")
                    self.cardTypeImageView.image = UIImage(named: "bt_ic_visa")
                }else if type.rawValue == CardType.MasterCard.rawValue{
                    self.cardTypeImageView.image = UIImage(named: "bt_ic_mastercard")
                    print("You are master")
                }else if type.rawValue == CardType.Diners.rawValue{
                    print("You are Diners")
                    self.cardTypeImageView.image = UIImage(named: "bt_ic_diners_club")
                }else if type.rawValue == CardType.Discover.rawValue{
                    self.cardTypeImageView.image = UIImage(named: "bt_ic_discover")
                    print("You are Discover")
                } else if type.rawValue == CardType.JCB.rawValue{
                    self.cardTypeImageView.image = UIImage(named: "bt_ic_jcb")
                    print("You are JCB")
                }else if type.rawValue == CardType.Elo.rawValue{
                    print("You are Elo")
                    self.cardTypeImageView.image = UIImage(named: "bt_ic_visa")
                    
                }else if type.rawValue == CardType.Hipercard.rawValue{
                    print("You are Hipercard")
                    self.cardTypeImageView.image = UIImage(named: "bt_ic_hipercard")
                    
                }else if type.rawValue == CardType.UnionPay.rawValue{
                    print("You are Unionpay")
                    self.cardTypeImageView.image = UIImage(named: "bt_ic_unionpay")
                    
                }else{
                    print("Unknown")
                    self.cardTypeImageView.image = UIImage(named: "bt_ic_visa")
                    
                }
                
            }
        }
    }
    
    
    @IBAction func changeFlag(_ sender: Any) {
        
        if btnFlag.currentImage == UIImage(named: "ios7-checkmark-outline"){
            btnFlag.setImage(UIImage(named: "ios7-circle-outline"), for: .normal)
            self.isSaved = false
        }else{
            btnFlag.setImage(UIImage(named: "ios7-checkmark-outline"), for: .normal)
            self.isSaved = true
        }
    }
    
    
    
    @IBAction func btnPlusClicked(_ sender: Any) {
        
        // self.setUpConstraint()
        self.heightForCardInputView.constant = 300
        self.cardInputView.layoutIfNeeded()
        self.heightForCardView.constant = 0
        self.cardInputView.layoutIfNeeded()
        self.showInputFields()
        
        self.containerViewHeight.constant = 700
        self.containerView.layoutIfNeeded()
        

        self.scrollView.contentSize = CGSize(width: UIScreen.main.bounds.width, height: 700)
        self.scrollView.layoutIfNeeded()
        
        isCardAvailable = false
    }
    
    
    @IBAction func dropDownListClicked(_ sender: Any) {
        
        if self.allCards.count > 0{
            self.isCardAvailable = true
        }
        else{
            self.isCardAvailable = false
        }
        
        self.setUpConstraint()
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CardListViewController") as! CardListViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func dropDownImageClicked(_ sender: Any) {
        
        if self.allCards.count > 0{
            self.isCardAvailable = true
        }
        else{
            self.isCardAvailable = false
        }
        
        self.setUpConstraint()
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CardListViewController") as! CardListViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func backClicked(_ sender: Any) {
        
        //self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func confirmButtonClicked(_ sender: Any) {
        
        let amount =  Double(txtAmount.text!)
        
        if isCardAvailable == true{
            
            if self.allCards.count > 0 || txtCardHolderName.text?.isEmpty == true || lblExpDate.text?.count == 0{
                
                if txtCvv.text?.isEmpty == true{
                    AppConfigs.showSnacbar(message: "Please fill up all input fields", textColor: .red)
                }
                else if Int(amount ?? 0) < 10 {
                    AppConfigs.showSnacbar(message: "Minimum amount $10", textColor: .red)
                }
                else{
                    
                    var params : Parameters = [
                        "card_number" :  self.finalCardNumber,
                        "card_name" : self.defCardHolName,
                        "card_code" : self.txtCvv.text!,
                        "expiration_date" : lblExpDate.text!,
                        "is_save" : false,
                        "amount" : amount ?? 0 ] as [String : Any]
//                        "amount" : 1 ] as [String : Any]

                    print(params)
                    
                    if parentString == "Plan"{
//                        params = [
//                           "card_number" : self.finalCardNumber,
//                           "card_name" : self.txtCardHolderName.text ?? "",
//                           "card_code" : cvcInputTextField.text ?? "",
//                           "expiration_date" : self.sendExpDateString,
//                           "is_save" : self.isSaved,
//                           "plan" : self.planID
//                           ] as [String : Any]
                        
                        params = [
                        "card_number" : self.finalCardNumber,
                        "card_name" : self.defCardHolName,
                        "card_code" : self.txtCvv.text!,
                        "expiration_date" : lblExpDate.text!,
                        "is_save" : self.isSaved,
                        "plan" : self.planID
                        ] as [String : Any]
                   }
                   
                   print(params)

                   var url = UrlManager.baseURL() + UrlManager.apiString() + "/payment-gateway/charge-card/"
                  
                    if parentString == "Plan"{
                       url = UrlManager.baseURL() + UrlManager.apiString() + "/upgrade-profile/"
                   }
                   
                   print("url-->", url)
                   
                    self.handleConfirmAction(urlString: url, parameters: params )
                }
                
            }else{
                AppConfigs.showSnacbar(message: "Select a card first!", textColor: .red)
            }
            
        }else{
            cardNumberTextField.numberInputTextFieldDelegate = self
            
            if txtCardHolderName.isEmpty == true || cardNumberTextField.isEmpty == true || cvcInputTextField.isEmpty == true || txtMonthYear.isEmpty == true
                //|| yearInputTextField.isEmpty == true || monthInputTextField.isEmpty == true
            {
                self.showAlert(message: "Please fill up all input fields")
            }

            else if Int(amount ?? 0) < 10 {
                AppConfigs.showSnacbar(message: "Minimum amount $10", textColor: .red)
            }
            else{
                
                let subString = cardNumberTextField.text?.replacingOccurrences(of: "-", with: "")//replacingOccurrences(of: "-", with: "")
                print(subString)
                let isValid = subString?.isValidCardNumber()
                if isValid == true{
                    cardNumberTextField.textColor = .black
                    self.finalCardNumber = subString!
                    
                    var params : Parameters = [
                        "card_number" : self.finalCardNumber,
                        "card_name" : self.defCardHolName,
                        "card_code" : cvcInputTextField.text!,
                        "expiration_date" : self.sendExpDateString,
                        "is_save" : self.isSaved,
                        "amount" : amount ?? 0
//                        "amount" : 1

                        ] as [String : Any]
                    
                    print("parentString-->", parentString)
                    
                    if parentString == "Plan"{
                         params = [
                            "card_number" : self.finalCardNumber,
                            "card_name" : self.txtCardHolderName.text ?? "",
                            "card_code" : cvcInputTextField.text ?? "",
                            "expiration_date" : self.sendExpDateString,
                            "is_save" : self.isSaved,
                            "plan" : self.planID
                            ] as [String : Any]
                    }
                    print(params)
                    
                    var url = UrlManager.baseURL() + UrlManager.apiString() + "/payment-gateway/charge-card/"
                    if parentString == "Plan"{
                        url = UrlManager.baseURL() + UrlManager.apiString() + "/upgrade-profile/"
                    }
                    print("url-->", url)
                    
                    self.handleConfirmAction(urlString: url, parameters: params )
                                        
                }else{
                    cardNumberTextField.textColor = UIColor.red
                    self.showAlert(message: "Invalid card number given")
                }
            }
        }
    }
    
    func setUpConstraint()  {
        
        if self.isCardAvailable == false{
            
            self.heightForCardInputView.constant = 300
            self.cardInputView.layoutIfNeeded()
            self.heightForCardView.constant = 0
            self.cardInputView.layoutIfNeeded()

            self.containerViewHeight.constant = 700
            self.containerView.layoutIfNeeded()

            self.scrollView.contentSize = CGSize(width: UIScreen.main.bounds.width, height: 700)
            self.scrollView.layoutIfNeeded()
            
            self.showInputFields()
            //            isCardAvailable = true
            
        }else{
            self.heightForCardInputView.constant = 0
            self.cardInputView.layoutIfNeeded()
            self.heightForCardView.constant = 300
            self.cardInputView.layoutIfNeeded()
            
            self.containerViewHeight.constant = 700
            self.containerView.layoutIfNeeded()
            
            self.scrollView.contentSize = CGSize(width: UIScreen.main.bounds.width, height: 700)
            self.scrollView.layoutIfNeeded()

            self.hideInputFields()
            
            self.lblAmount.text = "$" + (self.txtAmount.text ?? "0.0")
            
            if parentString == "Plan"{
                if self.selectedPlan?.planCost != nil{
                    self.lblAmount.text = "$" + "\(self.selectedPlan?.planCost ?? 0)"

                }else{
                    self.lblAmount.text = ""

                }
            }
            var resultString = String()
            self.allCards[0].cardNumber?.enumerated().forEach { (index, character) in

                // Add space every 4 characters
                if index % 4 == 0 && index > 0 {
                    resultString += "-"
                }

                if index < 12 {
                    // Replace the first 12 characters by *
                    resultString += "X"
                } else {
                    // Add the last 4 characters to your final string
                    resultString.append(character)
                }

            }
            let name = self.allCards[0].cardName
            self.finalCardNumber = self.allCards[0].cardNumber!
            
            self.lblCardNumber.text = resultString  + "\n\(name ?? "")"

            self.lblCardholderName.text = self.allCards[0].cardName

            self.defCardHolName = self.allCards[0].cardName!
            print(" self.defCardHolName",  self.defCardHolName)
            
            //self.lblCardNumber.text = self.allCards[0].cardNumber
            self.lblExpDate.text = self.allCards[0].expDate

            //self.lblCardholderName.text = self.allCards[0].cardName
            //            isCardAvailable = false
            let (type, formatted, valid) = checkCardNumber(input: self.allCards[0].cardNumber ?? "")
            
            print(type.rawValue)
            //Unknown, Amex, Visa, MasterCard, Diners, Discover, JCB, Elo, Hipercard, UnionPay
            if type.rawValue == CardType.Amex.rawValue{
                print("You are Amex")
                self.cardTypeImageView.image = UIImage(named: "bt_ic_amex")
            }else if type.rawValue == CardType.Visa.rawValue{
                print("You are Visa")
                self.cardTypeImageView.image = UIImage(named: "bt_ic_visa")
            }else if type.rawValue == CardType.MasterCard.rawValue{
                self.cardTypeImageView.image = UIImage(named: "bt_ic_mastercard")
                print("You are master")
            }else if type.rawValue == CardType.Diners.rawValue{
                print("You are Diners")
                self.cardTypeImageView.image = UIImage(named: "bt_ic_diners_club")
            }else if type.rawValue == CardType.Discover.rawValue{
                self.cardTypeImageView.image = UIImage(named: "bt_ic_discover")
                print("You are Discover")
            } else if type.rawValue == CardType.JCB.rawValue{
                self.cardTypeImageView.image = UIImage(named: "bt_ic_jcb")
                print("You are JCB")
            }else if type.rawValue == CardType.Elo.rawValue{
                print("You are Elo")
                self.cardTypeImageView.image = UIImage(named: "bt_ic_visa")
                
            }else if type.rawValue == CardType.Hipercard.rawValue{
                print("You are Hipercard")
                self.cardTypeImageView.image = UIImage(named: "bt_ic_hipercard")
                
            }else if type.rawValue == CardType.UnionPay.rawValue{
                print("You are Unionpay")
                self.cardTypeImageView.image = UIImage(named: "bt_ic_unionpay")
                
            }else{
                print("Unknown")
                self.cardTypeImageView.image = UIImage(named: "bt_ic_visa")
            }
        }
        
    }
    
    
    func getAllCards(urlString: String) {
        
        self.activityIndicator.startAnimating()
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success",data)
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    
                    let allCardsResonse = try? JSONDecoder().decode([AllCardsResponseElement].self, from: response.data!)//AllPlansResponse
                    //self.jsonResponse = allPlans
                    self.allCards = allCardsResonse ?? []
                    DispatchQueue.main.async {
                        print("allPlans------------\n",self.allCards)
                        self.activityIndicator.stopAnimating()
                        self.removeSpinner()
                        
                        if self.allCards.count > 0{
                            self.isCardAvailable = true
                        }
                        else{
                            self.isCardAvailable = false
                        }
                        
                        self.setUpConstraint()
                    }
                }
                else if statusCode == 401{
                    self.activityIndicator.stopAnimating()
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                   // AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    self.activityIndicator.stopAnimating()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
                self.activityIndicator.stopAnimating()
                
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
            self.removeSpinner()//self.view.activityStopAnimating()
            
        }
    }
    
    
    func handleConfirmAction(urlString: String, parameters: Parameters ){
        
        self.showSpinner(onView: btnConfirm)
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print("headers", headers)
        print("urlString", urlString)
        print("parameters", parameters)
        
        
        Alamofire.request(urlString, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
            case.success(let data):
                print("success",data)
                self.removeSpinner()
                //self.activityStopAnimating()
                let statusCode = response.response?.statusCode
                print(statusCode!)
                let rechargeCardResponse = try? JSONDecoder().decode(RechargeCardResponse.self, from: response.data!)
                if statusCode == 200{
                    if rechargeCardResponse?.status == true{
                        
                        DispatchQueue.main.async {
                            self.removeSpinner()
                            //AppConfigs.showSnacbar(message: (rechargeCardResponse?.message)!, textColor: .green)
                            
                            let alertController = UIAlertController(title: (rechargeCardResponse?.message)!, message:"", preferredStyle: .alert)
                                let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                                    
//                                    let url : String = UrlManager.getCurrentUserInfoURL()
//                                    self.getCurrentUserInfo(urlString: url)
 
                                    if self.parentString == "Plan"{
                                        let url : String = UrlManager.getCurrentUserInfoURL()
                                        self.getCurrentUserInfo(urlString: url)
                                    }else{

                                        self.dismiss(animated: true, completion: nil)
                                        
                                        let url : String = UrlManager.getCurrentUserInfoURL()
                                        self.getCurrentUserInfo(urlString: url)
                                        
                                    }
                                }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion: nil)
                            
                            
                        }
                    }else{
                        AppConfigs.showSnacbar(message: (rechargeCardResponse?.message)!, textColor: .green)
                    }
                }
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: (rechargeCardResponse?.message)!, textColor: .green)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: (rechargeCardResponse?.message)!, textColor: .green)
                }
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                
                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
                self.removeSpinner()
            }
            self.removeSpinner()
        }
    }
    
    
    func getCurrentUserInfo(urlString:String) {
        print(urlString)
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success",data)
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    
                    DispatchQueue.main.async {
                        
                        let currentUser = try? JSONDecoder().decode(CurrentUserInfoResponse.self, from: response.data!)
                        AppConfigs.saveCurrentUserInfo(user: currentUser!)
                        print(AppConfigs.getCurrentUserInfo().email!)
                        
                        if self.parentString == "Plan"{

                            let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
                            guard let vc = storyBoard.instantiateViewController(withIdentifier: "SMNavigationController") as? SMNavigationController  else
                            { return  }
                            vc.modalPresentationStyle = .fullScreen
                            self.present(vc, animated: true, completion: nil)
                            
                        }else{
                            print(self.parentString)
                        }
                    }
                }
                    
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                
                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
    
}

extension RechargeViewController: NumberInputTextFieldDelegate, CardInfoTextFieldDelegate {
    
    // A card that is not nil when valid information has been entered in the text fields:
    var card: Card? {
        let number = cardNumberTextField.cardNumber
        let cvc = CVC(rawValue: cvcInputTextField.text ?? "")
        let expiry = Expiry(month:  "02", year:   "2020")
            ?? Expiry.invalid
        
        let cardType = cardNumberTextField.cardTypeRegister.cardType(for: cardNumberTextField.cardNumber)
        if cardType.validate(cvc: cvc).union(cardType.validate(expiry: expiry)).union(cardType.validate(number: number)) == .Valid {
            return Card(number: number, cvc: cvc, expiry: expiry)
        } else {
            return nil
        }
    }
    
    func numberInputTextFieldDidComplete(_ numberInputTextField: NumberInputTextField) {
        //cvcInputTextField.cardType = numberInputTextField.cardTypeRegister.cardType(for: numberInputTextField.cardNumber)
        
        print("Card number: \(numberInputTextField.cardNumber)")
        print(card)
        cvcInputTextField.becomeFirstResponder()
    }
    
    func numberInputTextFieldDidChangeText(_ numberInputTextField: NumberInputTextField) {
        
        
    }
    
    func textField(_ textField: UITextField, didEnterValidInfo: String) {
        switch textField {
        case is MonthInputTextField:
            print("Month: \(didEnterValidInfo)")
            
        case is YearInputTextField:
            print("Year: \(didEnterValidInfo)")
        // monthInputTextField.becomeFirstResponder()
        case is CVCInputTextField:
            //yearInputTextField.becomeFirstResponder()
            print("CVC: \(didEnterValidInfo)")
        default:
            break
        }
        print(card)
    }
    
    func textField(_ textField: UITextField, didEnterPartiallyValidInfo: String) {
        // The user entered information that is not valid but might become valid on further input.
        // Example: Entering "1" for the CVC is partially valid, while entering "a" is not.
    }
    
    func textField(_ textField: UITextField, didEnterOverflowInfo overFlowDigits: String) {
        // This function is used in a CardTextField to carry digits to the next text field.
        // Example: A user entered "02/20" as expiry and now tries to append "5" to the month.
        //          On a card text field, the year will be replaced with "5" - the overflow digit.
    }
    
    // ...
    
    func cardTextFieldShouldShowAccessoryImage(_ cardTextField: CardTextField) -> UIImage? {
        return UIImage(named: "bt_ic_visa")
    }
    
    func cardTextFieldShouldProvideAccessoryAction(_ cardTextField: CardTextField) -> (() -> ())? {
        return nil
    }
    
}

//extension RechargeViewController: UITextFieldDelegate {
//    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        guard let normalText = textField.text else { return false }
//        
//        let beginning = textField.beginningOfDocument
//        // save cursor location
//        let cursorLocation = textField.position(from: beginning, offset: range.location + string.count)
//        
//        let newString = (normalText as NSString).replacingCharacters(in: range, with: string)
//        let newStringClean = newString.stringWithOnlyNumbers().withMask(mask: mask)
//        
//        guard newString != newStringClean else { return true }
//        
//        textField.text = newStringClean
//        guard string != "" else { return false }
//        
//        // fix cursor location after changing textfield.text
//        if let cL = cursorLocation {
//            let textRange = textField.textRange(from: cL, to: cL)
//            textField.selectedTextRange = textRange
//        }
//        
//        return false
//    }
//}
extension String {
    func stringWithOnlyNumbers() -> String {
        return self.reduce("") { (acc, c) -> String in
            guard c.isDigit() else { return acc }
            return "\(acc)\(c)"
        }
    }
    
    func withMask(mask: String) -> String {
        var resultString = String()
        
        let chars = self
        let maskChars = mask
        
        var stringIndex = chars.startIndex
        var maskIndex = mask.startIndex
        
        while stringIndex < chars.endIndex && maskIndex < maskChars.endIndex {
            if (maskChars[maskIndex] == "#") {
                resultString.append(chars[stringIndex])
                stringIndex = chars.index(after: stringIndex)
            } else {
                resultString.append(maskChars[maskIndex])
            }
            maskIndex = chars.index(after: maskIndex)
        }
        
        return resultString
    }
    
}

extension Character {
    func isDigit() -> Bool {
        let s = String(self).unicodeScalars
        let uni = s[s.startIndex]
        
        let digits = NSCharacterSet.decimalDigits
        let isADigit = digits.hasMember(inPlane: UInt8(uni.value))
        
        return isADigit
    }
    
}
