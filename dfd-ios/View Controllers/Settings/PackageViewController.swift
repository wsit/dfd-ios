//
//  PackageViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 1/11/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//

import UIKit
import FSPagerView
import Alamofire
import SwiftyJSON
import StoreKit


public typealias SuccessBlock = () -> Void
public typealias FailureBlock = (Error?) -> Void


let sandboxServer = "https://sandbox.itunes.apple.com/verifyReceipt"
let liveServer = "https://buy.itunes.apple.com/verifyReceipt"
let storeKitShareSecret = "982a19ca6bae44a98acf22cf67ae7b8f"

class PackageViewController: UIViewController, FSPagerViewDataSource,FSPagerViewDelegate {
    
    var imageNames: [String] = []
    var selectedPlan : AllPlansResponseElement?
    var selectedPlanPrice : Double? = 0.0
    
    fileprivate var typeIndex = 0 {
        didSet {
            
            let transform = CGAffineTransform(scaleX: 0.85, y: 0.9)
            self.pagerView.itemSize = self.pagerView.frame.size.applying(transform)
            self.pagerView.decelerationDistance = FSPagerView.automaticDistance
            
        }
    }
    
    @IBOutlet weak var myCustomView: CurvedView!
    @IBOutlet weak var lblPlanName: UILabel!
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var pagerView: FSPagerView!
    //        {
    //        didSet {
    //            self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
    //            pagerView.transformer = FSPagerViewTransformer(type: .linear)
    //            self.typeIndex = 0
    //
    //        }
    //    }
    var selectedProductIdentifier : String = ""
    var isSandboxUser: Bool!
    var refreshSubscriptionSuccessBlock : SuccessBlock?
    var refreshSubscriptionFailureBlock : FailureBlock?
    var sharedSecret = "7bc6807f34954f78a7b61a47df7df50a"
    var successBlock : SuccessBlock?
    var failureBlock : FailureBlock?
    var appStoreReceiptURL: URL!
    
    
    var planID: Int = 0
    var allPlans: [AllPlansResponseElement] = []
    var jsonResponse : AllPlansResponse?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().restoreCompletedTransactions()
        
        FontAndColorConfigs.setDynamicButtonsFontSize(buttons: [btnConfirm], type: TextType.ListTitle.rawValue)
        //textView.text = freeText
        self.textView.textAlignment = .center
        self.lblAmount.text = "$0.0/month"
        self.selectedPlanPrice = 0.0
        self.btnConfirm.backgroundColor = UIColor(red:0.48, green:0.87, blue:0.74, alpha:1.0)
        self.lblPlanName.text = "Free Plan"
        self.view.backgroundColor = UIColor(red:0.13, green:0.10, blue:0.22, alpha:1.0)
        self.planID = 0
        self.btnConfirm.setTitle("Start Now", for: .normal)
        self.textView.font = ProNovaR16
        
        print("discount", AppConfigs.getCurrentUserInfo().upgradeInfo?.discount )
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        if Network.reachability.status == .unreachable{
            AppConfigs.updateUserInterface(view: view)
        }else{
            allPlans.removeAll()
            let url : String = UrlManager.baseURL() + UrlManager.apiString() + "/plan/"
            
            print(url)
            self.showSpinner(onView: view)
            getAllPlans(urlString: url)
        }
        // FIXME: Remove code below if u're using your own menu
        setupNavForDefaultMenu()
        self.setUpNavigationBar()
        // Add left bar button item
        let leftBarItem = UIBarButtonItem(image: UIImage(named: "burger"), style: .plain, target: self, action: #selector(toggleSideMenu))
        navigationItem.leftBarButtonItem = leftBarItem
        leftBarItem.tintColor = .black
        self.title = "Plan"
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func setupNavForDefaultMenu() {
        // Revert navigation bar translucent style to default
        //navigationBarNonTranslecentStyle()
        // Update side menu after reverted navigation bar style
        sideMenuManager?.instance()?.menu?.isNavbarHiddenOrTransparent = true
        navigationItem.hidesBackButton = true
    }
    
    @objc func toggleSideMenu() {
        sideMenuManager?.toggleSideMenuView()
    }
    
    //
    //    @IBAction func backClicked(_ sender: Any) {
    //        self.navigationController?.popViewController(animated: true)
    //    }
    
    func fetchProduct(productID: NSSet){
        
        if (SKPaymentQueue.canMakePayments())
        {
            
            let productsRequest:SKProductsRequest = SKProductsRequest(productIdentifiers: productID as! Set<String>);
            productsRequest.delegate = self;
            productsRequest.start();
            print("Fething Products");
        }else{
            print("can't make purchases");
        }
    }
    
    @IBAction func subscribeButtonClicked(_ sender: Any) {
        
        let soloUserDef = UserDefaults.standard.string(forKey: RageProducts.subscriptionSolo)
        let teamUserDef = UserDefaults.standard.string(forKey: RageProducts.subscriptionTeam)
        
        print(soloUserDef)
        print(teamUserDef)
        
        var productID:NSSet?
        
        let id = AppConfigs.getCurrentUserInfo().upgradeInfo?.plan?.id
        print(id ?? 0)
        print(self.planID)
        if self.planID == 1{
            productID = NSSet(object: RageProducts.subscriptionSolo)
            self.selectedProductIdentifier = RageProducts.subscriptionSolo
            
        }else if self.planID == 2{
            productID = NSSet(object: RageProducts.subscriptionTeam)
            self.selectedProductIdentifier = RageProducts.subscriptionTeam
        }else{
            
        }
        
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        if id == self.planID{
            self.showAlert(message: "You are currently using this package")
            guard let vc = storyBoard.instantiateViewController(withIdentifier: "SMNavigationController") as? SMNavigationController  else
            { return  }
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
            //sideMenuManager?.toggleSideMenuView()
            //self.dismiss(animated: true, completion: nil)
        }
        else if id! > self.planID{
            //3 > 1
            if AppConfigs.getCurrentUserInfo().thinker == true{
                
                print("About to fetch the products");
                
                if  UserDefaults.standard.string(forKey: selectedProductIdentifier) != nil{
                    //not nil, data exists
                    let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss VV"
                    
                    let  date = formatter.date(from:UserDefaults.standard.string(forKey: selectedProductIdentifier)!)
                    print(date)
                    
                    if  date! > Date(){
                        self.showAlert(message: "You are already using this package")
                    }else{
                        print(selectedProductIdentifier)
                        self.fetchProduct(productID: productID!)
                    }
                }else{
                    self.fetchProduct(productID: productID!)
                }
                //
            }else{
                
                let vc = storyBoard.instantiateViewController(withIdentifier: "RechargeViewController") as! RechargeViewController
                vc.planID = self.planID
                vc.selectedPlan = self.selectedPlan
                vc.selectedPlanPrice = self.selectedPlanPrice
                vc.parentString = "Plan"
                vc.modalPresentationStyle = .fullScreen
                //self.navigationController?.pushViewController(vc, animated: true)
                self.present(vc, animated: true, completion: nil)
            }
            
        }
        else{
            
            if AppConfigs.getCurrentUserInfo().thinker == true{
                print("About to fetch the products");
                if  UserDefaults.standard.string(forKey: selectedProductIdentifier) != nil{
                    //not nil, data exists
                    let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss VV"
                    let  date = formatter.date(from:UserDefaults.standard.string(forKey: selectedProductIdentifier)!)
                    print(date)
                    if  date! > Date(){
                        self.showAlert(message: "You are already using this package")
                    }else{
                        print(selectedProductIdentifier)
                        self.fetchProduct(productID: productID!)
                    }
                }else{
                    
                    self.fetchProduct(productID: productID!)
                }
                
            }else{
                if self.planID == 2{
                    
                    let vc = storyBoard.instantiateViewController(withIdentifier: "RechargeViewController") as! RechargeViewController
                    vc.planID = self.planID
                    vc.selectedPlan = self.selectedPlan
                    vc.selectedPlanPrice = self.selectedPlanPrice

                    vc.parentString = "Plan"
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true, completion: nil)
                }else{
                    self.showAlert(message: " Please contact with admin")
                }
            }
        }
    }
    
    
    @IBAction func backClicked(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        //self.dismiss(animated: true, completion: nil)
    }
    
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return allPlans.count
    }
    
    func getAllPlans(urlString: String) {
        
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success",data)
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    
                    let allPlans = try? JSONDecoder().decode([AllPlansResponseElement].self, from: response.data!)//AllPlansResponse
                    //self.jsonResponse = allPlans
                    self.allPlans = allPlans ?? []
                    DispatchQueue.main.async {
                        print("allPlans------------\n",self.allPlans)
                        self.removeSpinner()
                        for plan in self.allPlans{
                            //["ic_solo","ic_team","ic_free"]
                            if plan.id == 1 {
                                self.imageNames.append("ic_solo")
                            }
                            else if plan.id == 2{
                                self.imageNames.append("ic_team")
                            }else if plan.id == 3{
                                self.imageNames.append("ic_free")
                            }else{
                                
                            }
                        }
                        self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
                        self.pagerView.transformer = FSPagerViewTransformer(type: .linear)
                        self.typeIndex = 0
                        self.pagerView.reloadData()
                        
                    }
                }
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    // AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        cell.imageView?.image = UIImage(named: self.imageNames[index])
        cell.imageView?.contentMode = .scaleAspectFill
        cell.imageView?.clipsToBounds = true
        
        if index == 0{
            self.selectedPlan = allPlans[index]
            let str = allPlans[index].rechargeCardResponseDescription ?? ""
            let data = str.data(using: .utf8)!
            var desc : String = ""
            
            do{
                let arr = try JSON(data: data).arrayValue
                
                //                for element in arr{
                //                    let str = element.stringValue
                //                    print(str)
                //                    desc = desc + str
                //                }
                desc = arr[1].stringValue
                print(desc)
                
            }catch let error{
                print(error.localizedDescription)
            }
            
            textView.text = desc //soloText
            self.btnConfirm.backgroundColor = UIColor(red:0.37, green:0.67, blue:0.90, alpha:1.0)
            self.btnConfirm.setTitle("Start Now", for: .normal)
            
            if AppConfigs.getCurrentUserInfo().upgradeInfo?.discount != nil   {
                
                let oldPrice = allPlans[index].planCost ?? 0.0
                let newPrice = self.calculateNewPrice(oldPrice: oldPrice)
                print(newPrice)
                
                
                self.lblAmount.text = String(format: "$%.2f", newPrice) + "/month"//"\(newPrice)/month"// "$0.0/month"
                self.selectedPlanPrice = newPrice
                 
            }else{
                self.lblAmount.text = "$" + "\(allPlans[index].planCost ?? 0)/month"// "$0.0/month"
                self.selectedPlanPrice = allPlans[index].planCost ?? 0
                
            }
            self.lblPlanName.text = "\(allPlans[index].planName ?? "Solo Plan")" //"Free Plan"
            self.planID = self.allPlans[index].id ?? 0
        }
        
        
        return cell
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
    }
    
    func pagerViewDidScroll(_ pagerView: FSPagerView) {
        print(pagerView.currentIndex )
        self.setUpUI(index: pagerView.currentIndex)
        
    }
    
    func calculateNewPrice ( oldPrice: Double) -> Double{
        let discount : Double = (AppConfigs.getCurrentUserInfo().upgradeInfo?.discount)!
        print(discount)
        //let oldPrice = allPlans[index].planCost
        print(oldPrice)
        
        let discountPrice = (oldPrice * discount)/100
        print(discountPrice)
        
        let newPrice = oldPrice - discountPrice
        print(newPrice)
        
        return newPrice
    }
    
    func setUpUI(index: Int) {
        
        let str = allPlans[index].rechargeCardResponseDescription ?? ""
        let data = str.data(using: .utf8)!
        var desc : String = ""
        do{
            let arr = try JSON(data: data).arrayValue
            //            for element in arr{
            //                let str = element.stringValue
            //                print(str)
            //                desc = desc + str
            //            }
            desc = arr[1].stringValue
            print(desc)
            
        }catch let error{
            print(error.localizedDescription)
        }
        
        if index == 0{
            self.btnConfirm.backgroundColor = self.soloTitleColor
            textView.text = desc  //textView.text = soloText
            
            if AppConfigs.getCurrentUserInfo().upgradeInfo?.discount != nil   {
                
                let oldPrice = allPlans[index].planCost ?? 0.0
                let newPrice = self.calculateNewPrice(oldPrice: oldPrice)
                print(newPrice)
                
                self.lblAmount.text = String(format: "$%.2f", newPrice) + "/month"//"\(newPrice)/month"// "$0.0/month"
                self.selectedPlanPrice = newPrice

                //self.lblAmount.text = "$" + "\(newPrice)/month"// "$0.0/month"
                
            }else{
                self.lblAmount.text = "$" + "\(allPlans[index].planCost ?? 0)/month"// "$0.0/month"
                self.selectedPlanPrice = allPlans[index].planCost
                
            }
            //self.lblAmount.text = "$" + "\(allPlans[index].planCost ?? 0)"// "$0.0/month"
            self.lblPlanName.text = "\(allPlans[index].planName ?? "Solo Plan")" //"Free Plan"
            self.planID = self.allPlans[index].id ?? 0
            self.btnConfirm.setTitle("Start Now", for: .normal)
            
        }
        else if index == 1{
            self.btnConfirm.backgroundColor = self.teamTitleColor
            textView.text = desc//teamText
            
            if AppConfigs.getCurrentUserInfo().upgradeInfo?.discount != nil   {
                
                let oldPrice = allPlans[index].planCost ?? 0.0
                let newPrice = self.calculateNewPrice(oldPrice: oldPrice)
                print(newPrice)
                
                self.lblAmount.text = String(format: "$%.2f", newPrice) + "/month"//"\(newPrice)/month"// "$0.0/month"
                self.selectedPlanPrice = newPrice

                //self.lblAmount.text = "$" + "\(newPrice)/month"// "$0.0/month"
                
            }else{
                self.lblAmount.text = "$" + "\(allPlans[index].planCost ?? 0)/month"// "$0.0/month"
                self.selectedPlanPrice = allPlans[index].planCost
                
            }
            //self.lblAmount.text = "$" + "\(allPlans[index].planCost ?? 0)/month"// "$0.0/month"
            self.lblPlanName.text = "\(allPlans[index].planName ?? "Team Plan")" //"Free Plan"
            self.planID = self.allPlans[index].id ?? 0
            self.btnConfirm.setTitle("Start Now", for: .normal)
        }
        else if index == 2{
            self.btnConfirm.backgroundColor = self.freeTitleColor
            textView.text = desc //freeText
            if AppConfigs.getCurrentUserInfo().upgradeInfo?.discount != nil   {
                
                let oldPrice = allPlans[index].planCost ?? 0.0
                let newPrice = self.calculateNewPrice(oldPrice: oldPrice)
                print(newPrice)
                
                self.lblAmount.text = String(format: "$%.2f", newPrice) + "/month"//"\(newPrice)/month"// "$0.0/month"
                self.selectedPlanPrice = newPrice

                //self.lblAmount.text = "$" + "\(newPrice)/month"// "$0.0/month"
                
            }else{
                self.lblAmount.text = "$" + "\(allPlans[index].planCost ?? 0)/month"// "$0.0/month"
                self.selectedPlanPrice = allPlans[index].planCost
                
            }
            //self.lblAmount.text = "$" + "\(allPlans[index].planCost ?? 0)"// "$0.0/month"
            
            self.lblPlanName.text = "\(allPlans[index].planName ?? "Free Plan")" //"Free Plan"
            self.planID = self.allPlans[index].id ?? 0
            self.btnConfirm.setTitle("Start Now", for: .normal)
        }
        else{
            
        }
        self.selectedPlan = allPlans[index]
        
        self.lblAmount.text = "$" + "\(allPlans[index].planCost ?? 0)/month"// "$0.0/month"
        if AppConfigs.getCurrentUserInfo().upgradeInfo?.discount != nil   {
            
            let oldPrice = allPlans[index].planCost ?? 0.0
            let newPrice = self.calculateNewPrice(oldPrice: oldPrice)
            print(newPrice)
            
            self.lblAmount.text = String(format: "$%.2f", newPrice) + "/month"//"\(newPrice)/month"// "$0.0/month"
            self.selectedPlanPrice = newPrice

            //self.lblAmount.text = "$" + "\(newPrice)/month"// "$0.0/month"
            
        }else{
            self.lblAmount.text = "$" + "\(allPlans[index].planCost ?? 0)/month"// "$0.0/month"
            self.selectedPlanPrice = allPlans[index].planCost
            
        }
        //
        
        self.lblPlanName.text = "\(allPlans[index].planName ?? "Team Plan")" //"Free Plan"
        self.planID = self.allPlans[index].id ?? 0
        self.btnConfirm.setTitle("Start Now", for: .normal)
        
    }
}


extension PackageViewController: SKProductsRequestDelegate{
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        print("got the request from Apple")
        let count : Int = response.products.count
        if (count>0) {
            let validProducts = response.products
            
            //let validProduct: SKProduct = response.products[0] as SKProduct
            for validProduct in validProducts{
                if (validProduct.productIdentifier == self.selectedProductIdentifier) {
                    print(validProduct.localizedTitle)
                    print(validProduct.localizedDescription)
                    print(validProduct.price)
                    buyProduct(product: validProduct);
                } else {
                    print(validProduct.productIdentifier)
                }
            }
            
        } else {
            print("nothing")
        }
        
    }
    
    func request(_ request: SKRequest, didFailWithError error: Error) {
        
        print("Error Fetching product information");
    }
    
    // Helper Methods
    
    func buyProduct(product: SKProduct){
        print("Sending the Payment Request to Apple");
        let payment = SKPayment(product: product)
        SKPaymentQueue.default().add(payment);
        
    }
    
    
}

extension PackageViewController: SKPaymentTransactionObserver{
    
    //If an error occurs, the code will go to this function
    func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error) {
        
        print(error.localizedDescription)
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        
        print("Received Payment Transaction Response from Apple");
        
        for transaction:AnyObject in transactions {
            if let trans:SKPaymentTransaction = transaction as? SKPaymentTransaction{
                switch trans.transactionState {
                    
                case .purchasing:
                    print("Purchasing state");
                case .purchased:
                    print("Product Purchased");
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    
                    notifyIsPurchased(transaction: transaction as! SKPaymentTransaction)
                    self.sendSubscriptionDataToServer(transaction:transaction as! SKPaymentTransaction )
                    break;
                case .failed:
                    print("Purchased Failed");
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    self.failureBlock?(transaction.error)
                    print(trans.error?.localizedDescription)
                    self.showAlert(message: trans.error?.localizedDescription ??  "Something went wrong")
                    cleanUp()
                    break;
                case .restored:
                    print("Product Restored");
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    notifyIsPurchased(transaction: transaction as! SKPaymentTransaction)
                    // SKPaymentQueue.default().restoreCompletedTransactions()
                    break
                    //[self restoreTransaction:transaction];
                    
                case .deferred:
                    print("Defered state");
                    break
                    
                default:
                    break;
                }
            }
        }
        
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, removedTransactions transactions: [SKPaymentTransaction]) {
        
        print("The Payment was removedTransactions!")
        //SKPaymentQueue.default().remove(self)
    }
    
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        
        print("The Payment was successfull!")
        
    }
    
    
}

extension PackageViewController{
    
    func handleConfirmAction(urlString: String, parameters: Parameters ){
        self.showSpinner(onView:view )
        
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print("headers", headers)
        print("urlString", urlString)
        print("parameters", parameters)
        
        Alamofire.request(urlString, method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
            case.success(let data):
                print("success",data)
                self.removeSpinner()
                //self.activityStopAnimating()
                let statusCode = response.response?.statusCode
                print(statusCode!)
                let rechargeCardResponse = try? JSON(data: response.data!)
                let status = rechargeCardResponse?["status"].boolValue
                let message = rechargeCardResponse?["message"].stringValue
                
                if statusCode == 200{
                    if status == true{
                        let url : String = UrlManager.getCurrentUserInfoURL()
                        self.getCurrentUserInfo(urlString: url)
                    }else{
                        AppConfigs.showSnacbar(message: message ?? "", textColor: .green)
                    }
                }
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: (rechargeCardResponse?.message)!, textColor: .green)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: (rechargeCardResponse?.message)!, textColor: .green)
                }
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                
                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
                self.removeSpinner()
            }
            self.removeSpinner()
        }
    }
    
    func sendSubscriptionDataToServer(transaction: SKPaymentTransaction) {
        
        let url: String = UrlManager.baseURL() + UrlManager.apiString() + "/apple-upgrade-profile/"
        print(url)
        if transaction.payment.productIdentifier == RageProducts.subscriptionSolo{
            let params: Parameters = ["plan": 1]
            self.handleConfirmAction(urlString: url, parameters: params )
            
        }else if transaction.payment.productIdentifier == RageProducts.subscriptionTeam{
            let params: Parameters = ["plan": 2]
            self.handleConfirmAction(urlString: url, parameters: params )
            
        }else{
            
        }
        
    }
    
    func getCurrentUserInfo(urlString:String) {
        print(urlString)
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success",data)
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    
                    DispatchQueue.main.async {
                        
                        let currentUser = try? JSONDecoder().decode(CurrentUserInfoResponse.self, from: response.data!)
                        AppConfigs.saveCurrentUserInfo(user: currentUser!)
                        print(AppConfigs.getCurrentUserInfo().email!)
                        
                        
                        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
                        guard let vc = storyBoard.instantiateViewController(withIdentifier: "SMNavigationController") as? SMNavigationController  else
                        { return  }
                        vc.modalPresentationStyle = .fullScreen
                        self.present(vc, animated: true, completion: nil)
                        
                    }
                }
                    
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                
                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
    func expirationDateFor(_ identifier : String) -> Date?{
        return UserDefaults.standard.object(forKey: identifier) as? Date
    }
    
    
    func notifyIsPurchased(transaction: SKPaymentTransaction) {
        refreshSubscriptionsStatus(callback: {
            self.successBlock?()
            self.cleanUp()
        }) { (error) in
            // couldn't verify receipt
            print("\(error) -----> \(error?.localizedDescription)")
            self.failureBlock?(error)
            self.cleanUp()
        }
    }
    
    /*
     Private method. Should not be called directly. Call refreshSubscriptionsStatus instead.
     */
    func refreshReceipt(){
        let request = SKReceiptRefreshRequest(receiptProperties: nil)
        request.delegate = self
        request.start()
    }
    
    func requestDidFinish(_ request: SKRequest) {
        // call refresh subscriptions method again with same blocks
        if request is SKReceiptRefreshRequest {
            refreshSubscriptionsStatus(callback: self.successBlock ?? {}, failure: self.failureBlock ?? {_ in})
        }
    }
    
    /* It's the most simple way to send verify receipt request. Consider this code as for learning purposes. You shouldn't use current code in production apps.
     This code doesn't handle errors.
     */
    func refreshSubscriptionsStatus(callback : @escaping SuccessBlock, failure : @escaping FailureBlock){
        
        self.refreshSubscriptionSuccessBlock = callback
        self.refreshSubscriptionFailureBlock = failure
        
        guard let receiptUrl = Bundle.main.appStoreReceiptURL else {
            refreshReceipt()
            // do not call block in this case. It will be called inside after receipt refreshing finishes.
            return
        }
        
        #if DEBUG
        let urlString = "https://sandbox.itunes.apple.com/verifyReceipt"
        #else
        let urlString = "https://buy.itunes.apple.com/verifyReceipt"
        #endif
        
        print("urlString", urlString)
        
        let receiptData = try? Data(contentsOf: receiptUrl).base64EncodedString()
        print("Receipt Data -----> \(String(describing: receiptData))")
        
        let requestData = ["receipt-data" : receiptData ?? "", "password" : self.sharedSecret, "exclude-old-transactions" : true] as [String : Any]
        
        var request = URLRequest(url: URL(string: urlString)!)
        request.httpMethod = "POST"
        request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
        
        let httpBody = try? JSONSerialization.data(withJSONObject: requestData, options: [])
        request.httpBody = httpBody
        
        URLSession.shared.dataTask(with: request)  { (data, response, error) in
            DispatchQueue.main.async {
                if data != nil {
                    if let json = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments){
                        self.parseReceipt(json as! Dictionary<String, Any>)
                        return
                    }
                } else {
                    print("error validating receipt: \(error?.localizedDescription ?? "")")
                }
                self.refreshSubscriptionFailureBlock?(error)
                self.cleanUpRefeshReceiptBlocks()
            }
        }.resume()
    }
    
    /* It's the most simple way to get latest expiration date. Consider this code as for learning purposes. You shouldn't use current code in production apps.
     This code doesn't handle errors or some situations like cancellation date.
     */
    func parseReceipt(_ json : Dictionary<String, Any>) {
        guard let receipts_array = json["latest_receipt_info"] as? [Dictionary<String, Any>] else {
            self.refreshSubscriptionFailureBlock?(nil)
            self.cleanUpRefeshReceiptBlocks()
            return
        }
        
        var isInvalidUser: Bool!
        for receipt in receipts_array {
            let productID = receipt["product_id"] as! String
            print("productID--->", productID)
            print(receipt)
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss VV"
            let receiptDictionary:NSDictionary = receipt as! NSDictionary
            print(receiptDictionary)
            
            if receipt["expires_date"] != nil{
                
                if let date = formatter.date(from: receipt["expires_date"] as! String) {
                    if date > Date() {
                        // do not save expired date to user defaults to avoid overwriting with expired date
                        if productID == RageProducts.subscriptionSolo{
                            UserDefaults.standard.set(date, forKey: productID)
                        }else if productID == RageProducts.subscriptionTeam {
                            UserDefaults.standard.set(date, forKey: productID)
                        }
                        print("\(productID) -----> \(date)")
                    }else{
                        print(" MY_BLOCK: \(productID) -----> expires_date \(date)")
                        UserDefaults.standard.set(nil, forKey: productID)
                        print("UserDefaults.standard---->", UserDefaults.standard.string(forKey: productID))
                        if productID == RageProducts.subscriptionSolo{
                            
                        }else if productID == RageProducts.subscriptionTeam {
                            
                        }
                    }
                }
            }
            
            //            let receiptDictionary:NSDictionary = receipt as! NSDictionary
            //            print(receiptDictionary)
            //            let expiresDateMs = receiptDictionary.value(forKey: "expires_date_ms")
            //            if expiresDateMs != nil && !(expiresDateMs is NSNull) {
            //
            //                let currentDate =  Date().timeIntervalSince1970
            //                let expiredDate = (expiresDateMs as AnyObject).doubleValue / 1000.0
            //
            //                if currentDate > expiredDate {
            //                    isInvalidUser = true
            //                    // [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isPremiumUser"];
            //                } else {
            //
            //                    isInvalidUser = false
            //                    //UserDefaults.standard.set(true, forKey: "isPremiumUser")
            //                    let exdate = Date(timeIntervalSince1970: TimeInterval((expiresDateMs as AnyObject).doubleValue / 1000.0))
            //                    print("EXPIRED DATE ==== \(exdate)")
            //                    break
            //                }
            //            }
        }
        self.refreshSubscriptionSuccessBlock?()
        self.cleanUpRefeshReceiptBlocks()
    }
    
    
    func cleanUpRefeshReceiptBlocks(){
        self.refreshSubscriptionSuccessBlock = nil
        self.refreshSubscriptionFailureBlock = nil
    }
    
    
    func cleanUp(){
        self.successBlock = nil
        self.failureBlock = nil
    }
    
    
    func startValidatingReceiptsAndUpdateLocalStore() {
        
        getAppStoreReceipt(withCompletionHandler: { receipts, error in
            if error != nil {
                if let error = error {
                    print("Receipt validation failed with error: \(error)")
                }
                if (error?.localizedDescription == "This receipt is from the test environment.") {
                    self.startValidatingReceiptsAndUpdateLocalStore()
                } else {
                    if let error = error {
                        print("Receipt validation failed with error: \(error)")
                    }
                }
            }else{
                
                var receiptsArray:NSArray!
                receiptsArray = NSArray.init()
                if receipts != nil {
                    receiptsArray = receipts as NSArray?
                    print("Recipt ==>>> \(String(describing: receipts))")
                }
                
                var isInvalidUser: Bool!
                for i in (0..<receiptsArray.count)
                {
                    let receiptDictionary:NSDictionary = receiptsArray.object(at: i) as! NSDictionary
                    print(receiptDictionary)
                    
                    let expiresDateMs = receiptDictionary.value(forKey: "expires_date_ms")
                    if expiresDateMs != nil && !(expiresDateMs is NSNull) {
                        
                        let currentDate =  Date().timeIntervalSince1970
                        let expiredDate = (expiresDateMs as AnyObject).doubleValue / 1000.0
                        
                        if currentDate > expiredDate {
                            isInvalidUser = true
                            // [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isPremiumUser"];
                        } else {
                            
                            isInvalidUser = false
                            UserDefaults.standard.set(true, forKey: "isPremiumUser")
                            let exdate = Date(timeIntervalSince1970: TimeInterval((expiresDateMs as AnyObject).doubleValue / 1000.0))
                            print("EXPIRED DATE ==== \(exdate)")
                            break
                        }
                    }
                    
                    if i == receiptsArray.count - 1 {
                        if isInvalidUser == true {
                            UserDefaults.standard.set(false, forKey: "isPremiumUser")
                        }
                        
                        //                        if (self.purchaseCompleteBlock == nil) {
                        //                            self.restoreChecking()
                        //                        }
                    }
                }
            }
        })
        
    }
    
    
    
    func getAppStoreReceipt(withCompletionHandler completionHandler: @escaping (_ receipts: [AnyHashable]?, _ error: Error?) -> Void) {
        
        if let appStoreReceiptURL = Bundle.main.appStoreReceiptURL {
            self.appStoreReceiptURL = appStoreReceiptURL
        }
        
        var receiptData: Data? {
            guard let receiptDataURL = appStoreReceiptURL,
                let data = try? Data(contentsOf: receiptDataURL) else {
                    completionHandler(nil, nil)
                    return nil
            }
            return data
        }
        
        // var error: Error?
        var requestContents = ["receipt-data" : receiptData?.base64EncodedString(options: []) ?? ""]
        let sharedSecret = storeKitShareSecret
        if sharedSecret != "" {
            requestContents["password"] = sharedSecret
        }
        
        var requestData: Data? = nil
        do {
            requestData = try JSONSerialization.data(withJSONObject: requestContents, options: [])
        } catch {
        }
        
        
        var storeRequest: URLRequest! = nil
        if !isSandboxUser {
            if let url = URL(string: liveServer) {
                print("url-->",url)
                storeRequest = URLRequest(url: url)
            }
        } else {
            if let url = URL(string: sandboxServer) {
                print("url-->",url)
                storeRequest = URLRequest(url: url)
                
            }
        }
        
        storeRequest!.httpMethod = "POST"
        storeRequest!.httpBody = requestData
        
        let session = URLSession.shared
        let task = session.dataTask(with: storeRequest) { data, response, error in
            
            if error != nil{
                completionHandler(nil, error)
            }else{
                
                if data != nil {
                    do {
                        if let jsonResponse = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: Any] {
                            
                            let status = (jsonResponse["status"] as! NSNumber).intValue
                            if status == 21007 {
                                self.isSandboxUser = true
                            }
                            //  if let jsonResponse = jsonResponse {
                            print("appstore json recipt =  \(jsonResponse)")
                            // }
                            
                            if (jsonResponse["receipt"] != nil) {
                                
                                let dic = jsonResponse ["receipt"] as! NSDictionary
                                let originalAppVersion = dic["original_application_version"] as? String
                                if originalAppVersion == nil {
                                    completionHandler(nil, nil)
                                }
                            }else{
                                completionHandler(nil, nil)
                            }
                            
                            if status != 0 {
                                
                                let errorDictionary = [
                                    NSNumber(value: 21000): "The App Store could not read the JSON object you provided.",
                                    NSNumber(value: 21002): "The data in the receipt-data property was malformed or missing.",
                                    NSNumber(value: 21003): "The receipt could not be authenticated.",
                                    NSNumber(value: 21004): "The shared secret you provided does not match the shared secret on file for your accunt.",
                                    NSNumber(value: 21005): "The receipt server is not currently available.",
                                    NSNumber(value: 21006): "This receipt is valid but the subscription has expired.",
                                    NSNumber(value: 21007): "This receipt is from the test environment.",
                                    NSNumber(value: 21008): "This receipt is from the production environment."]
                                
                                let error = NSError(domain: "net.twinbit.storekit", code: status, userInfo:
                                    [
                                        NSLocalizedDescriptionKey: errorDictionary[NSNumber(value: status)] as Any
                                ])
                                completionHandler(nil, error)
                                
                            }else{
                                
                                let array: NSMutableArray =  jsonResponse["latest_receipt_info"] as! NSMutableArray
                                let receipts:NSMutableArray = array.mutableCopy() as! NSMutableArray
                                
                                
                                
                                if (jsonResponse["receipt"] as? NSNull) != NSNull() {
                                    
                                    let dic = jsonResponse["receipt"] as! NSDictionary
                                    let inAppReceipts:NSArray = dic["in_app"] as! NSArray
                                    receipts.addObjects(from: inAppReceipts as! [Any])
                                    // self.isSandboxUser = NO;
                                    completionHandler(receipts as? [AnyHashable] , nil)
                                } else {
                                    completionHandler(nil, nil)
                                }
                            }
                            
                            
                        }
                    } catch let error {
                        print(error.localizedDescription)
                        completionHandler(nil, error)
                    }
                }
            }
            
        }
        task.resume()
        
    }
    
    
    
}
