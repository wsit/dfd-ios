//
//  ListViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 5/7/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit

class ListViewController: UIViewController {
    
    @IBOutlet weak var myListTableView: UITableView!
    
    let kCellIdentifier = "menuCell"
    var items: Array = ["Area 1", "Area 2 ", "Area 3", "Area 4", "Area 5", "Area 6"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: "MenuTableViewCell", bundle: nil)
        myListTableView.register(nib, forCellReuseIdentifier: kCellIdentifier)
        self.myListTableView.tableFooterView = UIView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // FIXME: Remove code below if u're using your own menu
        setupNavForDefaultMenu()
        self.setUpNavigationBar()
        
        // Add left bar button item
        let leftBarItem = UIBarButtonItem(image: UIImage(named: "burger"), style: .plain, target: self, action: #selector(toggleSideMenu))
        navigationItem.leftBarButtonItem = leftBarItem
        leftBarItem.tintColor = .black
        self.title = "My List"
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func setupNavForDefaultMenu() {
        // Revert navigation bar translucent style to default
        //navigationBarNonTranslecentStyle()
        // Update side menu after reverted navigation bar style
        sideMenuManager?.instance()?.menu?.isNavbarHiddenOrTransparent = true
        navigationItem.hidesBackButton = true
    }
    
    @objc func toggleSideMenu() {
        sideMenuManager?.toggleSideMenuView()
    }
    
    
}

extension ListViewController: UITableViewDataSource {
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier, for: indexPath) as! MenuTableViewCell
        cell.lblTitle.text = items[indexPath.row]
        cell.textLabel?.textColor = UIColor(red:0.2, green:0.3, blue:0.41, alpha:1)
        if let myImage = UIImage(named: "active_status") {
            let tintableImage = myImage.withRenderingMode(.alwaysTemplate)
            cell.titleImageView.image = tintableImage
        }
        
        cell.titleImageView.tintColor = UIColor(red:0.2, green:0.3, blue:0.41, alpha:1)
        return cell
    }
    
}

extension ListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! MenuTableViewCell
        cell.backgroundColor = UIColor.white
        cell.contentView.backgroundColor = UIColor.white
        //tableView.separatorStyle = .none
        cell.textLabel?.textColor = UIColor(red:0.07, green:0.82, blue:0.41, alpha:1)
        cell.titleImageView.tintColor = UIColor(red:0.07, green:0.82, blue:0.41, alpha:1)
        
        let data = items[indexPath.row]
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "List", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "PropertyListViewController") as! PropertyListViewController
        vc.listTitle = data
        
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! MenuTableViewCell
        cell.backgroundColor = UIColor.white
        cell.contentView.backgroundColor = UIColor.white
        //tableView.separatorStyle = .none
        cell.titleImageView.tintColor = UIColor(red:0.2, green:0.3, blue:0.41, alpha:1)
    }
}

