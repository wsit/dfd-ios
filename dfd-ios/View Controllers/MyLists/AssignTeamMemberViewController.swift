//
//  AssignTeamMemberViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 7/19/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit
import Alamofire

class AssignTeamMemberViewController: UIViewController {
    
    @IBOutlet weak var assignTeamMemberTV: UITableView!
    @IBOutlet weak var btnDone: UIBarButtonItem!
    
    var parentVC: String = ""
    var items = ["Bran Stack", "Jon Snow", "Daynaris Targerian", "Tyrion Lanaster", "Arya Stark", "Khal Drogo", "The Knight King"]
    
    
    var users: [UnregisteredInvitation] = []
    var unregisteredUsers: [UnregisteredInvitation] = []
    var allTeamMembers: [UnregisteredInvitation] = []
    
    var jsonResponse: MyTeamResponse?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(parentVC)
        let nib = UINib(nibName: "NewMemberTableViewCell", bundle: nil)
        assignTeamMemberTV.register(nib, forCellReuseIdentifier: "NewMemberTableViewCell")
        self.assignTeamMemberTV.tableFooterView = UIView()
        self.btnDone.title = ""
        self.btnDone.isEnabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
                if Network.reachability.status == .unreachable{
                    AppConfigs.updateUserInterface(view: view)
                }else{
                        allTeamMembers.removeAll()
                        users.removeAll()
                        unregisteredUsers.removeAll()
                        
                        let url : String = UrlManager.getTeamListURL()
                        
                        print(url)
                        self.showSpinner(onView: view)
                        getAllTeamMembers(urlString: url)
                }
    }
    
    @IBAction func backClicked(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
        //self.dismiss(animated: true, completion: nil)
    }
    @IBAction func doneClicked(_ sender: Any) {
        
        AppConfigs.showSnacbar(message: "Member Assigned", textColor: .green)
        _ = self.navigationController?.popViewController(animated: true)
        
    }
    
    
    func getAllTeamMembers(urlString:String)  {
        
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success",data)
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    
                    let teamMembers = try? JSONDecoder().decode(MyTeamResponse.self, from: response.data!)
                    self.jsonResponse = teamMembers
                    
                    let userList = teamMembers?.users
                    //let pendingUserList = teamMembers?.unregisteredInvitations
                    self.allTeamMembers = userList! //+ pendingUserList!
                    
                    var items: [UnregisteredInvitation] = self.allTeamMembers
                    items.append(contentsOf: (teamMembers?.users)!)
                    
                    DispatchQueue.main.async {
                        print("allTeamMembers------------\n",self.allTeamMembers)
                        self.removeSpinner()//self.view.activityStopAnimating()
                        self.assignTeamMemberTV.reloadData()
                        
                        //  self.perform(#selector(self.loadTable), with: nil, afterDelay: 1.0)
                    }
                }
                    
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)

                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
    
}

extension AssignTeamMemberViewController: UITableViewDataSource {
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if allTeamMembers.count == 0 {
            tableView.setEmptyMessage("Nothing yet to show")
        } else {
            tableView.restore()
        }
        return allTeamMembers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewMemberTableViewCell", for: indexPath) as! NewMemberTableViewCell
        FontAndColorConfigs.setTextColor(labels: [cell.memberNameLabel], color: .black)
        FontAndColorConfigs.setLabelFontSize(labels: [cell.memberNameLabel], type: FontAndColorConfigs.getTextTypeHeader())//FontAndColorConfigs.setTitletLabelFontSize(labels: [cell.memberNameLabel]) //
        cell.separatorLine.backgroundColor = FontAndColorConfigs.getSeperatorColor()
        //        cell.memberNameLabel.text = items[indexPath.row]
        //        //cell.lblUrl.text = "www.facebook.com"
        //        cell.memberImageView.setImage(string: items[indexPath.row], color: UIColor.colorHash(name: items[indexPath.row]), circular: true, stroke: false)
        
        if allTeamMembers.count > indexPath.row{
            let member = allTeamMembers[indexPath.row]
            
            if member.firstName != nil || member.lastName != nil{
                var fullName: String = member.firstName! + " " + member.lastName!
                if member.status == 0{
                    //fullName = fullName + " (pending)"
                    let string_to_color = "pending"
                    let range = (fullName as NSString).range(of: string_to_color)
                    let attribute = NSMutableAttributedString.init(string: fullName)
                    attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red , range: range)
                    //cell.memberNameLabel.attributedText = attribute
                    cell.memberNameLabel.text = fullName
                    cell.pendingImageView.isHidden = false
                }else{
                    cell.memberNameLabel.text = fullName
                }
            }else{
                if member.status == 0{
                    let email = member.email ?? ""
                    cell.pendingImageView.isHidden = false
                    let emailString = email + " (pending)"
                    let string_to_color = "pending"
                    
                    let range = (emailString as NSString).range(of: string_to_color)
                    let attribute = NSMutableAttributedString.init(string: emailString)
                    attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red , range: range)
                    //cell.memberNameLabel.attributedText = attribute
                    cell.memberNameLabel.text = email
                }else{
                    cell.memberNameLabel.text = member.email
                }
            }
        }
        return cell
    }
    
}

extension AssignTeamMemberViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.parentVC == "Property" {
            let cell = tableView.cellForRow(at: indexPath) as! NewMemberTableViewCell
            cell.memberNameLabel.text = allTeamMembers[indexPath.row].email
            cell.memberNameLabel?.textColor = UIColor(red:0.07, green:0.82, blue:0.41, alpha:1)
            cell.backgroundColor = UIColor.white
            cell.contentView.backgroundColor = UIColor.white
            tableView.separatorStyle = .none
        }else{
            
        }
        self.btnDone.title = "DONE"
        self.btnDone.isEnabled = true
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! NewMemberTableViewCell
        cell.memberNameLabel?.textColor = UIColor(red:0.2, green:0.3, blue:0.41, alpha:1)
        cell.backgroundColor = UIColor.white
        cell.contentView.backgroundColor = UIColor.white
        tableView.separatorStyle = .none
        
        //tableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.none)
    }
    
}

