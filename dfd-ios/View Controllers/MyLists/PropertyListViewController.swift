//
//  PropertyListViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 7/19/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit

class PropertyListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnAssignMember: UIButton!
    
    var listTitle: String = ""
    
    var selectedProperty : [String] = []
    var couter: Int = 0
    var indexArray : [IndexPath] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = listTitle
        self.btnAssignMember.backgroundColor = UIColor(red:0.07, green:0.82, blue:0.41, alpha:1)
        
        let nib = UINib(nibName: "PropertyListTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "PropertyListTableViewCell")
        self.tableView.tableFooterView = UIView()
        self.tableView.backgroundColor = .groupTableViewBackground
        self.tableView.separatorStyle = .none
    }
    
    @IBAction func backAction(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func assignToMemberClicked(_ sender: Any) {
        
        if self.indexArray.count == 0 {
            AppConfigs.showSnacbar(message: "Please Select Properties First", textColor: .red)
        }else{
            let storyBoard : UIStoryboard = UIStoryboard(name: "List", bundle:nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "AssignTeamMemberViewController") as! AssignTeamMemberViewController
            vc.parentVC = "Property"
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    
}

extension PropertyListViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        //        switch (section){
        //        case 0:
        //            return 16;
        //            break;
        //
        //        case 1:
        //            return 44; //Required height value here
        //            break;
        //
        //        default:
        //            return 16; //Any default value
        //        }
        return 16;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PropertyListTableViewCell", for: indexPath) as! PropertyListTableViewCell
        cell.backgroundColor = .groupTableViewBackground
        cell.upView.backgroundColor = .white
        cell.upView.cornerRadius = 8
        cell.lblProperttAddress.textColor = UIColor(red:0.2, green:0.3, blue:0.41, alpha:1)
        cell.lblNoOfNotes.textColor = UIColor(red:0.2, green:0.3, blue:0.41, alpha:1)
        cell.lblNoOfPhotosOfProperty.textColor = UIColor(red:0.2, green:0.3, blue:0.41, alpha:1)
        //cell.photosImageView.tintColor = UIColor(red:0.35, green:0.78, blue:0.98, alpha:1)
        
        cell.lblDateAdded.textColor = UIColor(red:0.2, green:0.3, blue:0.41, alpha:1)
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.accessoryType = .none
        //        if indexPath.row % 2 == 0{
        //            cell.visitedImageView.tintColor = UIColor(red:0, green:0.84, blue:0.37, alpha:1)
        //        }else{
        //            cell.visitedImageView.tintColor = UIColor(red:1, green:0.29, blue:0.3, alpha:1)
        //            cell.lblVisitedFlag.text = "Not Visited"
        //        }
        
        if (self.indexArray.contains(indexPath))
        {
            cell.accessoryType = .checkmark
        }
        else
        {
            cell.accessoryType  = .none
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath)
        //tableView.dequeueReusableCell(withIdentifier: "PropertyListTableViewCell", for: indexPath) as! PropertyListTableViewCell
        //cell?.selectionStyle = UITableViewCell.SelectionStyle.none
        let row = indexPath.row
        print(row)
        if cell?.accessoryType == .checkmark {
            cell?.accessoryType =  .none
            //selectedArr.remove(where:{ $0 == item })
            //            if selectedProperty.count == 1{
            //                self.selectedProperty.remove(at: row)
            //            }else{
            //                self.selectedProperty.remove(at: row - 1)
            //            }
            //            if let index = selectedProperty.firstIndex(of: selectedProperty[indexPath.row]) {
            //                selectedProperty.remove(at: index)
            //            }
            //
            if let pos = self.indexArray.firstIndex(of: indexPath)
            {
                self.indexArray.remove(at: pos)
            }
        } else {
            cell?.accessoryType = .checkmark
            //selectedArr.append(item)
            //self.selectedProperty.append("\(indexPath.row)")
            self.couter = self.couter + 1
            self.indexArray.append(indexPath)
            
        }
        print(selectedProperty.count)
        
    }
    
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        //tableView.deselectRow(at: indexPath, animated: true)
        
    }
}



