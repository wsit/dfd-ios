//
//  TutsViewController.swift
//  HouZes
//
//  Created by Mahadhi Hassan Chowdhury on 2/6/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//

import UIKit
import FSPagerView

class TutsViewController: UIViewController,FSPagerViewDataSource,FSPagerViewDelegate  {
    
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
    }
    
    func pagerViewDidScroll(_ pagerView: FSPagerView) {
        print(pagerView.currentIndex )
        
        self.pageControl.currentPage = pagerView.currentIndex
        
        if pagerView.currentIndex == 6{
            self.btnConfirm.setTitle("Continue", for: .normal)
        }else{
            self.btnConfirm.setTitle("Skip", for: .normal)
        }
    }
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return self.imageNames.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        cell.imageView?.image = UIImage(named: self.imageNames[index])
        cell.imageView?.contentMode = .scaleAspectFit
        cell.imageView?.clipsToBounds = true
        
        return cell
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        
        
    }
    
    
    @IBOutlet weak var btnConfirm: UIButton! 
    @IBOutlet weak var pagerView: FSPagerView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    
    
    var imageNames: [String] = ["1","2","3","4","5","6","7"]
    
    fileprivate var typeIndex = 0 {
        didSet {
            //
            //            let transform = CGAffineTransform(scaleX: 0.85, y: 0.9)
            //            self.pagerView.itemSize = self.pagerView.frame.size.applying(transform)
            //            self.pagerView.decelerationDistance = FSPagerView.automaticDistance
            
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.btnConfirm.setTitle("Skip", for: .normal)
        //self.btnConfirm.backgroundColor = self.buttonBackgroundColor
        
        self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        self.pagerView.transformer = FSPagerViewTransformer(type: .linear)
        self.typeIndex = 0
        self.pagerView.reloadData()
        
    }
    
    @IBAction func skipButtonClicked(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
