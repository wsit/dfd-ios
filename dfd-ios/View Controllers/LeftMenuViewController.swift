//
//  LeftMenuViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 5/2/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit
import Alamofire
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn
import Foundation

protocol LeftMenuDelegate: class {
    func didSelectItemAtIndex(index idx: Int)
}

class LeftMenuViewController: UIViewController {
    
    // MARK: IBOutlets
    @IBOutlet weak var userAvatarImg: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var menuTableView: UITableView!
    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var btnUpgrade: UIButton!
    @IBOutlet weak var userEmailLabel: UILabel!
    @IBOutlet weak var lblPlanName: UILabel!
    
    // MARK: Properties
    let kCellIdentifier = "menuCell"
    let items = ["Home", "Saved HouZes", "Add HouZes", "Previous Drives", "My Team", /*"Visited Places",*/ "Profile", "Settings" /*"My Lists", "Logout"*/]
    weak var delegate: LeftMenuDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnUpgrade.backgroundColor = UIColor(red:0.14, green:0.75, blue:0.45, alpha:1)
        btnUpgrade.setTitleColor(UIColor.white, for: .normal)
        lblPlanName.layer.masksToBounds = true
        //lblPlanName.backgroundColor = self.blueCapsuleColor
        FontAndColorConfigs.setLabelFontSize(labels: [lblPlanName], type: TextType.ListSubTitle.rawValue)
        lblPlanName.text = "\(String(describing: (AppConfigs.getCurrentUserInfo().upgradeInfo?.plan?.planName) ?? ""))"
        
        let nib = UINib(nibName: "MenuTableViewCell", bundle: nil)
        menuTableView.register(nib, forCellReuseIdentifier: kCellIdentifier)
        self.menuTableView.tableFooterView = UIView()
        self.menuTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        self.setUpView()
    }
    
    func setUpView() {
        
        userNameLabel.textColor = UIColor(red:0.14, green:0.78, blue:0.47, alpha:1)
        userEmailLabel.textColor = UIColor(red:0.39, green:0.54, blue:0.55, alpha:1)
        headerView.backgroundColor = .white // UIColor(red:0.04, green:0.09, blue:0.14, alpha:1)
        
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [userNameLabel], type: TextType.MainTitle.rawValue)
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [userEmailLabel], type: TextType.ListTitle.rawValue)
        FontAndColorConfigs.setDynamicButtonsFontSize(buttons: [btnUpgrade], type: TextType.ListTitle.rawValue)
        btnUpgrade.backgroundColor = self.buttonBackgroundColor
        if AppConfigs.getCurrentUserInfo().upgradeInfo?.plan?.id == 1{
            lblPlanName.backgroundColor = self.soloTitleColor
        }else if  AppConfigs.getCurrentUserInfo().upgradeInfo?.plan?.id == 2{
            lblPlanName.backgroundColor = self.teamTitleColor
            btnUpgrade.isUserInteractionEnabled = false
            btnUpgrade.backgroundColor = self.disableButtonBackgroundColor
        }else{
            lblPlanName.backgroundColor = self.freeTitleColor
        }
        //self.btnAmount.setTitle("$\(String(describing: (amount)!))", for: .normal)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        //    menuTableView.reloadSections(IndexSet(integer: 0), with: .none)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // Circle avatar imageview
        userAvatarImg.layer.cornerRadius = userAvatarImg.frame.size.width/2
        userAvatarImg.layer.masksToBounds = true
        userAvatarImg.clipsToBounds = true
        
        // Border
        userAvatarImg.layer.borderWidth = 1
        userAvatarImg.layer.borderColor = UIColor.black.cgColor
        
        // Shadow img
        userAvatarImg.layer.shadowColor = UIColor.white.cgColor
        userAvatarImg.layer.shadowOpacity = 1
        userAvatarImg.layer.shadowOffset = .zero
        userAvatarImg.layer.shadowRadius = 10
        userAvatarImg.layer.shadowPath = UIBezierPath(rect: userAvatarImg.bounds).cgPath
        userAvatarImg.layer.shouldRasterize = true
        
        let user = AppConfigs.getCurrentUserInfo()
        userNameLabel.text = (user.firstName ?? "") + " " +  (user.lastName ?? "")
        userEmailLabel.text = user.email
        if user.photo != nil{
            if user.photo != ""{
                self.showPicture(url: user.photo!, imageView: self.userAvatarImg)
            }else{
                print("photo is empty")
            }
        }else
        {
            print("photo is nil")
        }
    }
    
    
    @IBAction func upgradeButtonClicked(_ sender: Any) {
        
        if  AppConfigs.getCurrentUserInfo().upgradeInfo?.plan?.id == 2{
            print("team")
            
        }else{
            
            
            if let delegate = delegate {
                delegate.didSelectItemAtIndex(index: 1001)
            }
        }
    }
    
    func handleLogout() {
        let parameters = [
            K.APIParameterKey.clientID: "hasan@workspaceit.com",
            K.APIParameterKey.clientSecret: "hasan@workspaceit.com",
            K.APIParameterKey.grantType: "refresh_token",
            K.APIParameterKey.refreshToken: AppConfigs.getSavedRefreshToken()
        ]
        print(parameters)
        
        //        let manager = Alamofire.SessionManager.default
        //        manager.session.configuration.timeoutIntervalForRequest = 120
        //
        self.view.activityStartAnimating(activityColor: .black, backgroundColor: .white)
        
        Alamofire.request(UrlManager.logoutURL(), method: .post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                print("success",data)
                self.view.activityStopAnimating()
                
                let statusCode = response.response?.statusCode
                let loginResponse = try? JSONDecoder().decode(LoginResponse.self, from: response.data!)
                
                if statusCode == 200{
                    
                }
                
            case.failure(let error):
                print("Not Success",error)
                    self.removeSpinner()
                    let statusCode = response.response?.statusCode
                    self.showMessageForServerError(code: statusCode ?? 500)
                    //AppConfigs.showSnacbar(message: error.localizedDescription, textColor: .red)
                
            }
            self.view.activityStopAnimating()
        }
    }
}

extension LeftMenuViewController: UITableViewDataSource {
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier, for: indexPath) as! MenuTableViewCell
        cell.lblTitle.text = items[indexPath.row]
        cell.textLabel?.textColor = UIColor(red:0.52, green:0.55, blue:0.57, alpha:1)
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [ cell.lblTitle], type: TextType.ListTitle.rawValue)
        
        switch items[indexPath.row] {
        case "Home":
            if let myImage = UIImage(named: "home") {
                let tintableImage = myImage.withRenderingMode(.alwaysOriginal)
                cell.titleImageView.image = tintableImage
            }
            
        case "Saved HouZes":
            if let myImage = UIImage(named: "ic_load_houzes") {
                let tintableImage = myImage.withRenderingMode(.alwaysOriginal)
                cell.titleImageView.image = tintableImage
            }
            
        case "Add HouZes":
            if let myImage = UIImage(named: "ic_add_properties") {
                let tintableImage = myImage.withRenderingMode(.alwaysOriginal)
                cell.titleImageView.image = tintableImage
            }
            
        case "Previous Drives":
            if let myImage = UIImage(named: "ic_history") {
                let tintableImage = myImage.withRenderingMode(.alwaysOriginal)
                cell.titleImageView.image = tintableImage
            }
            
        case "My Team":
            if let myImage = UIImage(named: "team") {
                let tintableImage = myImage.withRenderingMode(.alwaysOriginal)
                cell.titleImageView.image = tintableImage
            }
            
        case "Visited Places":
            if let myImage = UIImage(named: "location") {
                let tintableImage = myImage.withRenderingMode(.alwaysOriginal)
                cell.titleImageView.image = tintableImage
            }
            
        case "Profile":
            if let myImage = UIImage(named: "profile") {
                let tintableImage = myImage.withRenderingMode(.alwaysOriginal)
                cell.titleImageView.image = tintableImage
            }
            
        case "Lists":
            if let myImage = UIImage(named: "list") {
                let tintableImage = myImage.withRenderingMode(.alwaysOriginal)
                cell.titleImageView.image = tintableImage
            }
            
        case "Settings":
            if let myImage = UIImage(named: "new_ic_settings") {
                let tintableImage = myImage.withRenderingMode(.alwaysOriginal)
                cell.titleImageView.image = tintableImage
            }
            
        case "Logout":
            if let myImage = UIImage(named: "logout") {
                let tintableImage = myImage.withRenderingMode(.alwaysOriginal)
                cell.titleImageView.image = tintableImage
            }
            
            
        default: break
            // cell.titleImageView.image = UIImage(imageLiteralResourceName: "home.png")
        } 
        return cell
    }
    
}

extension LeftMenuViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! MenuTableViewCell
        cell.lblTitle.textColor = UIColor(red:0.07, green:0.82, blue:0.41, alpha:1)
        cell.backgroundColor = UIColor.white
        cell.contentView.backgroundColor = .white
        //cell.upView.roundCorners(corners: [.topRight, .bottomRight], radius: 40.0)
        //cell.upView.backgroundColor = UIColor(red:0.95, green:0.99, blue:0.97, alpha:1)//UIColor.white
        //tableView.separatorStyle = .none
        // cell.titleImageView.tintColor = UIColor(red:0.13, green:0.84, blue:0.44, alpha:1) //UIColor(red:0.07, green:0.82, blue:0.41, alpha:1) //UIColor(red:0.52, green:0.55, blue:0.57, alpha:1)
        
        
        
        if items[indexPath.row] == "Logout" {
            //self.handleLogout()
            //to do
            if let bundleID = Bundle.main.bundleIdentifier {
                UserDefaults.standard.removePersistentDomain(forName: bundleID)
            }
            print("AppConfigs.getSavedAccessToken()---> ", AppConfigs.getSavedAccessToken())
            
            if AccessToken.current != nil{
                
                LoginManager().logOut()
            }
            
            if GIDSignIn.sharedInstance()?.currentUser != nil{
                
                GIDSignIn.sharedInstance().signOut()
            }
            
            AppConfigs.setRootViewController()
        }
            //        else if items[indexPath.row] == "My Profile"{
            
            //            let mainStoryboard: UIStoryboard = UIStoryboard(name: "MyProfile", bundle: nil)
            //            let vc = mainStoryboard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            //            self.present(vc, animated: true, completion: nil)
            
            
            //           let showItemStoryboard = UIStoryboard(name: "MyProfile", bundle: nil)
            //            let showItemNavController = showItemStoryboard.instantiateViewController(withIdentifier: "ProfileNavigationViewController") as! UINavigationController
            //            let showItemVC = showItemNavController.topViewController as! ProfileViewController
            // Set the properties in your showItemVC
            //            present(showItemNavController, animated: true, completion: nil)
            
            //        }
            //        else if items[indexPath.row] == "My Team"{
            //            
            //        }
        else{
            
            if let delegate = delegate {
                delegate.didSelectItemAtIndex(index: indexPath.row)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! MenuTableViewCell
        cell.lblTitle.textColor =  UIColor.black//UIColor(red:0.52, green:0.55, blue:0.57, alpha:1)// UIColor(red:0.40, green:0.42, blue:0.48, alpha:1.0)//UIColor(red:0.2, green:0.3, blue:0.41, alpha:1)
        cell.backgroundColor = UIColor.white
        cell.contentView.backgroundColor = UIColor.white
        cell.upView.backgroundColor = .white //UIColor(red:0.95, green:0.99, blue:0.97, alpha:1)
        //tableView.separatorStyle = .none
        // cell.titleImageView.tintColor = UIColor(red:0.40, green:0.42, blue:0.48, alpha:1.0)// UIColor(red:0.52, green:0.55, blue:0.57, alpha:1) //UIColor(red:0.13, green:0.84, blue:0.44, alpha:1)//UIColor(red:0.2, green:0.3, blue:0.41, alpha:1)
        
        //tableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.none)
    }
}
