//
//  NoteCreateViewUpdateViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 9/3/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class NoteViewUpdateViewController: UIViewController, UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "enter description here"
            textView.textColor = UIColor.lightGray
        }
    }
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtNoteTitle: UITextField!
    @IBOutlet weak var lblNoteDescriptionTitle: UILabel!
    @IBOutlet weak var txtNoteDescription: UITextView!
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var upView: UIView!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var lblPageTitle: UILabel!
    
    var note : Note?
    var parentString : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtNoteDescription.delegate = self
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [lblTitle, lblNoteDescriptionTitle], type: TextType.ListTitle.rawValue)
        
        txtNoteTitle.font = ProNovaR16
        txtNoteDescription.font = ProNovaR16
        
        FontAndColorConfigs.setDynamicButtonsFontSize(buttons: [btnConfirm], type: TextType.ListTitle.rawValue)
        btnConfirm.backgroundColor = self.buttonBackgroundColor
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        if parentString == "addNote"{
            txtNoteDescription.isUserInteractionEnabled = true
            txtNoteDescription.text = "enter description here"
            txtNoteDescription.textColor = UIColor.lightGray
            lblPageTitle.text = "Add Note"
            txtNoteTitle.isUserInteractionEnabled = true
            btnConfirm.isHidden = false
            btnConfirm.setTitle("ADD NOTE", for: .normal)
            btnConfirm.isHidden = false
            //btnConfirm.backgroundColor = FontAndColorConfigs.getGreenishThemeColor()
            FontAndColorConfigs.setButtonsTextColorAndFont(buttons: [btnConfirm], color: .white, font: FontAndColorConfigs.getProximaSemiBold20())
            
            btnEdit.isHidden = true
            
        }else {
            self.btnConfirm.isHidden = true
            if note != nil{
                
                txtNoteTitle.text = note?.title
                txtNoteDescription.text = note?.notes
            }
            txtNoteDescription.isEditable = false
            txtNoteDescription.isScrollEnabled = true
            txtNoteTitle.isUserInteractionEnabled = false
        }
        
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func editButtonClicked(_ sender: Any) {
        
        txtNoteDescription.isUserInteractionEnabled = true
        txtNoteTitle.isUserInteractionEnabled = true
        txtNoteDescription.isEditable = true
        btnConfirm.isHidden = false
        btnConfirm.setTitle("CONFIRM", for: .normal)
        // btnConfirm.backgroundColor = FontAndColorConfigs.getGreenishThemeColor()
        FontAndColorConfigs.setButtonsTextColorAndFont(buttons: [btnConfirm], color: .white, font: FontAndColorConfigs.getProximaSemiBold20())
        txtNoteTitle.becomeFirstResponder()
        lblPageTitle.text = "Edit Note"
    }
    
    @IBAction func confirmButtonClicked(_ sender: Any) {
        
        if txtNoteTitle.isEmpty == true{
            showAlert(message: "Title Can't be Empty")
        }
        else if txtNoteDescription.text.count == 0 || txtNoteDescription.text == "enter description here"{
            showAlert(message: "Description Can't be Empty")
        }
        else{
            if self.parentString == "addNote"{
                
                let url = UrlManager.baseURL() + UrlManager.apiString() + "/note/"
                handleAddNote(urlString: url)
            }else{
                
                let url = UrlManager.baseURL() + UrlManager.apiString() + "/note/\(String(describing: (note?.id)!))/"
                print(url)
                handleEditNote(urlString: url)
            }
        }
    }
    
    func handleAddNote(urlString: String) {
        print(urlString)
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        
        let parameters = [
            "title": txtNoteTitle.text!,
            "notes": txtNoteDescription.text!,
            "property": globalSelectedPropertyFromList?.id!
            ] as [String : Any]
        print(parameters)
        
        Alamofire.request(urlString, method: .post, parameters: parameters as Parameters, encoding: URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                print("success",data)
                
                self.removeSpinner()
                let statusCode = response.response?.statusCode
                print(statusCode!)
                
                if statusCode == 200{
                    
                    DispatchQueue.main.async {
                        self.removeSpinner()

                        do{
                            let json = try JSON(data: response.data!)
                            
                            let message = json["message"].stringValue
                            let status = json["status"].boolValue
                            
                            if status == true{

                                let alertController = UIAlertController(title: message, message:" ", preferredStyle: .alert)
                                //let cancelAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                                let okAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                                    self.dismiss(animated: true, completion: nil)
                                    //                            let url = UrlManager.baseURL() + UrlManager.apiString() + "/note/"
                                    //                            self.handleAddNote(urlString: url)
                                    //  NotificationCenter.default.post(name:NSNotification.Name("ReloadNoteList"),object: nil)
                                })
                                alertController.addAction(okAction)
                                self.present(alertController, animated: true, completion: nil)
                            }else{
                                AppConfigs.showSnacbar(message: message, textColor: .red)
                            }
                        }catch let error{
                            print(error.localizedDescription)
                        }
                        
                    }
                }
                    
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                   // AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                    
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)

                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
                self.removeSpinner()
            }
            self.removeSpinner()
        }
    }
    
    
    func handleEditNote(urlString: String) {
        
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        
        let parameters  = [
            "title": txtNoteTitle.text!
            , "notes": txtNoteDescription.text!
            ] as [String : Any]
        
        print(urlString)
        
        Alamofire.request(urlString, method: .patch, parameters: parameters, encoding: URLEncoding.httpBody, headers: headers)
            .responseJSON { response in
                
                switch(response.result) {
                case.success(let data):
                    print("success",data)
                    self.view.activityStopAnimating()
                    let statusCode = response.response?.statusCode
                    
                    if statusCode == 200{
                        
                        let alertController = UIAlertController(title: "Note Updated ", message:" ", preferredStyle: .alert)
                        let okAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                            self.dismiss(animated: true, completion: nil)
                            //NotificationCenter.default.post(name:NSNotification.Name("ReloadNoteList"),object: nil)
                        })
                        alertController.addAction(okAction)
                        self.present(alertController, animated: true, completion: nil)
                    }
                    else if statusCode == 400{
                        
                        self.showAlert(message: "Some Error Occured!", title: "Sorry!")
                    }
                    else{
                        self.showAlert(message: "Some Error Occured!", title: "Sorry!")
                    }
                    
                case.failure(let error):
                    self.view.activityStopAnimating()
                    print("Not Success",error)
                    let statusCode = response.response?.statusCode
                    self.showMessageForServerError(code: statusCode ?? 500)

                    //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                    self.removeSpinner()
                }
        }
    }
    
}
