//
//  NeighbourDetailsViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 1/7/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class NeighbourDetailsViewController: UIViewController {
    
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var btnPowerTrace: UIButton!
    let tableViewCellIdentifier = "CurrentNeighboursTableViewCell"
    
    // var mobileNumbers : [String] = []
    var height : Int = 180
    var propertyID: Int = 0
    var jsonResponse: [GetNeighbourResponseElement]?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: "CurrentNeighboursTableViewCell", bundle: nil)
        myTableView.register(nib, forCellReuseIdentifier: "CurrentNeighboursTableViewCell")
        
        self.myTableView.tableFooterView = UIView()
        // myTableView.rowHeight = UITableView.automaticDimension
        //myTableView.estimatedRowHeight = 180
        let insets = UIEdgeInsets(top: 0, left: 0, bottom: 60, right: 0)
        myTableView.contentInset = insets
        
        btnPowerTrace.backgroundColor = self.buttonBackgroundColor
        
        self.setUpNavigationBar()
        myTableView.reloadData()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        if Network.reachability.status == .unreachable{
            AppConfigs.updateUserInterface(view: view)
        }else{

            let url : String = UrlManager.baseURL() + UrlManager.apiString() + "/get-neighborhood/property/\(String(describing: (propertyID)))/"
            
            print(url)
            // self.showSpinner(onView: view)
            getNeighbourInfoByPropertyID(urlString: url)
        }
        //            }
        
    }
    
    @IBAction func backToPreviousVC(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func powerTraceClicked(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "GetNeighbourPopUpViewController") as! GetNeighbourPopUpViewController
        
        vc.modalPresentationStyle = .overCurrentContext
        vc.parentVC = "cn" 
        vc.propertyID = self.propertyID
        vc.currentNeighbours = globalNeighboursResponse
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @objc func labelTapped(_ sender: MyTapGesture ) {
        print("labelTapped")
        print(sender.title)
        let nmbr = sender.title.components(separatedBy: CharacterSet.decimalDigits.inverted)
            .joined()
        print(nmbr)
        if let url = URL(string: "tel://\(nmbr)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        
    }
    
    
    @objc func addToWishListBtnTarget() {
        print("add  to wish btn target")
    }
    
    @objc func emailToFriendBtnTarget() {
        print(" email to friend target")
    }
    
    @objc func shareBtnTarget() {
        print("share btn target")
    }
    
    func getNeighbourInfoByPropertyID(urlString: String) {
        
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                //print("success",data)
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    
                    let allDriveResponse = try? JSONDecoder().decode([GetNeighbourResponseElement].self, from: response.data!)
                    self.jsonResponse = allDriveResponse
                    globalNeighboursResponse.removeAll()
                    globalNeighboursResponse = self.jsonResponse ?? []
                    print(self.jsonResponse ?? [])
                    print("self.jsonResponse",self.jsonResponse?.count ?? 0)
                    DispatchQueue.main.async {
                        // print("drivingList------------\n",self.drivingList)
                        self.myTableView.reloadData()
                        self.removeSpinner()//self.view.activityStopAnimating()
                    }
                }
                    
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                   // AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)

                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
}

extension NeighbourDetailsViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(globalNeighboursResponse.count)
        return globalNeighboursResponse.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //        let cell = tableView.dequeueReusableCell(withIdentifier: "CurrentNeighboursTableViewCell", for: indexPath) as! CurrentNeighboursTableViewCell
        //
        var cell = tableView.dequeueReusableCell(withIdentifier: "CurrentNeighboursTableViewCell", for: indexPath) as! CurrentNeighboursTableViewCell
        //tableView.dequeueReusableCell(withIdentifier: "CurrentNeighboursTableViewCell") as! CurrentNeighboursTableViewCell
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: "CurrentNeighboursTableViewCell") as! CurrentNeighboursTableViewCell
        }
        cell.backgroundColor = .white
        cell.contentView.backgroundColor = .white
        
        tableView.reloadInputViews()
        tableView.reloadRows(at: [indexPath], with: .none)
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [cell.lblName, cell.lblLocation], type: TextType.ListTitle.rawValue)
        
        if globalNeighboursResponse.count > indexPath.row{
            
            cell.mobileNumbers.removeAll()
            
            
            let neighbour = globalNeighboursResponse[indexPath.row]
            
            print(indexPath.row)
            
            cell.lblName.text = neighbour.ownershipInfo?.fullName
            
            cell.lblLocation.text = neighbour.neighborAddress
        }
        
        cell.layoutSubviews()
        cell.layoutIfNeeded()
        
        cell.reloadInputViews()
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if globalNeighboursResponse.count > indexPath.row{
            
            
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if globalNeighboursResponse.count > indexPath.row{
            
            let neighbour = globalNeighboursResponse[indexPath.row]
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OwnerDetailsViewController") as! OwnerDetailsViewController
            vc.parentSring = "neighbour"
            vc.neighbourID = neighbour.id!.description
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
}

//
//            let email = neighbour.powerTrace?.email
//            let replaced = email?.replacingOccurrences(of: ",", with: "\n")
//           // print(replaced)
//            let emailArray = email?.components(separatedBy: ",")
//            //print(emailArray)
//
//            cell.lblEmail.text = replaced
//            var yPos = CGFloat(0)
//            let social = neighbour.powerTrace?.social//jsondata["social"].stringValue
//
//            if social != nil && social != ""{
//                let socialArray: [String] = social?.components(separatedBy: ",") ?? []
//                //print(socialArray)
//
//                for i in 0..<socialArray.count {
//                    let element = socialArray[i]
//                    let labelNum = UILabel()
//                    let imgView = UIImageView()
//
//                    labelNum.text = element
//                    labelNum.textColor = .black
//                    labelNum.textAlignment = .left
//                    labelNum.numberOfLines = 0
//                    labelNum.lineBreakMode = NSLineBreakMode.byWordWrapping
//                    labelNum.sizeToFit()
//
//                    FontAndColorConfigs.setDynamicLabelFontSize(labels: [labelNum], type: TextType.ListTitle.rawValue)
//                    imgView.frame = CGRect(x: 0, y: yPos, width: 30, height: 30)
//                    labelNum.frame = CGRect( x:40, y:yPos, width:cell.socialView.frame.width - imgView.frame.width - 8 , height: 30)
//
//                    if element.contains("www.facebook.com") {
//                        //print("facebook exists")
//                        imgView.image = UIImage(named: "new_ic_fb")
//                    }
//                    else if element.contains("www.twitter.com") {
//                        //print(" twitter exists")
//                        imgView.image = UIImage(named: "new_ic_twitter")
//                    }
//                    else if element.contains("google") {
//                       // print("google exists")
//                        imgView.image = UIImage(named: "new_ic_google")
//                    }
//                    else if element.contains("www.linkedin.com") {
//                        //print("linkedin exists")
//                        imgView.image = UIImage(named: "new_ic_ln")
//                    }else{
//                        imgView.image = UIImage(named: "new_ic_links")
//                    }
//                    cell.socialView.addSubview(imgView)
//                    cell.socialView.addSubview(labelNum)
//
//                    yPos += 30
//                    cell.heightForSocialView.constant = yPos
//                    cell.socialView.layoutIfNeeded()
//                }
//            }else{
//                cell.heightForSocialView.constant = 0
//                cell.socialView.layoutIfNeeded()
//            }
//
//            let verifiedCellphones = neighbour.powerTrace?.verifiedCellphones//jsondata["verified_cellphones"].stringValue
//            let landline = neighbour.powerTrace?.landline//jsondata["landline"].stringValue
//            let verified_landline = neighbour.powerTrace?.verifiedLandline//jsondata["verified_landline"].stringValue
//            let cellphones = neighbour.powerTrace?.cellphones//sondata["cellphones"].stringValue
//
//            if verifiedCellphones != ""{
//
//                let arr1: [String] = verifiedCellphones?.components(separatedBy: ",") ?? []
//                ///print(arr1)
//                for item in arr1{
//                    cell.mobileNumbers.append(item)
//                }
//            }
//            if landline != ""{
//
//                let arr1: [String] = landline?.components(separatedBy: ",") ?? []
//                //print(arr1)
//                for item in arr1{
//                    cell.mobileNumbers.append(item)
//                }
//            }
//            if verified_landline != ""{
//
//                let arr1: [String] = verified_landline?.components(separatedBy: ",") ?? []
//                //print(arr1)
//                for item in arr1{
//                    cell.mobileNumbers.append(item)
//                }
//            }
//
//            if cellphones != ""{
//
//                let arr1 : [String] = cellphones?.components(separatedBy: ",") ?? []
//                //print(arr1)
//                for item in arr1{
//                    cell.mobileNumbers.append(item)
//                }
//            }
//           // print("Total Mobile No Count", cell.mobileNumbers.count)
//
//            var yPos2 = CGFloat(0)
//            if cell.mobileNumbers.count > 0{
//
//                for i in 0..<cell.mobileNumbers.count {
//                    let element = cell.mobileNumbers[i]
//
//                    let labelNum = UILabel()
//                    labelNum.text = element
//                    labelNum.textColor = .black
//                    labelNum.textAlignment = .left
//                    labelNum.numberOfLines = 0
//                    labelNum.lineBreakMode = NSLineBreakMode.byWordWrapping
//                    labelNum.sizeToFit()
//                    FontAndColorConfigs.setDynamicLabelFontSize(labels: [labelNum], type: TextType.ListTitle.rawValue)
//                    labelNum.frame = CGRect( x:0, y:yPos2, width:cell.mobileNumberView.frame.width - 16 , height: 30)
//                    cell.mobileNumberView.addSubview(labelNum)
//
//                    yPos2 += 30
//                    cell.heightForMobileNumberView.constant = yPos2
//                    cell.mobileNumberView.layoutIfNeeded()
//
//
//                    let tappy = MyTapGesture(target: self, action: #selector(self.labelTapped(_:)))
//                    labelNum.isUserInteractionEnabled = true
//                    labelNum.addGestureRecognizer(tappy)
//                    tappy.title = labelNum.text!
//
//                }
//            }else{
//                cell.heightForMobileNumberView.constant = 30
//                cell.mobileNumberView.layoutIfNeeded()
//            }
//
//            cell.heightForArea.constant = 180 + yPos + yPos2
//            cell.layoutIfNeeded()
//
//            self.height = Int(180 + yPos + yPos2)

