//
//  AssignTagsViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 7/19/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit
import Alamofire
import SelectionList

class AssignTagsViewController: UIViewController {
    
    //@IBOutlet weak var tagTV: UITableView!
    @IBOutlet weak var btnDone: UIBarButtonItem!
    @IBOutlet weak var selectionList: SelectionList!
    
    
    let kCellIdentifier = "menuCell"
    //let items = ["Confirmed vacant", "Visibly distressed", "Overgrown yard", "Referred by area expert", "Spoke to neighbor", "Govt Job Holders"]
    
    var allTags: [PropertyTag] = []
    var jsonTagResponse: ListOfTagsResponse?
    //var allListArray : [AllListsResponseForDropdown]
    var selectedTag : PropertyTag?
    var selectedTagID: [Int] = []
    var allTagNames : [String] = []
    var allColorCodes : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Assign Tag"
        
        selectionList.items = []
        //selectionList.selectedIndexes = [0, 1, 4]
        selectionList.tableView.tableFooterView = UIView()
        
        selectionList.addTarget(self, action: #selector(selectionChanged), for: .valueChanged)
        selectionList.setupCell = { (cell: UITableViewCell, index: Int) in
            
            
            
            let color = self.allColorCodes[index]
            cell.textLabel?.textColor = UIColor(hexString: color)
            
            FontAndColorConfigs.setDynamicLabelFontSize(labels: [cell.textLabel!], type: TextType.ListTitle.rawValue)
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.setUpNavigationBar()
        
        if Network.reachability.status == .unreachable{
            AppConfigs.updateUserInterface(view: view)
        }else{

            allTags.removeAll()
            let url : String = UrlManager.baseURL() + UrlManager.apiString() + "/tag/"
            
            print(url)
            //self.showSpinner(onView: view)
            getAllTags(urlString: url)
            
        }//        }
    }
    
    @objc func selectionChanged() {
        print(selectionList.selectedIndexes)
    }
    
    @IBAction func backClicked(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func doneClicked(_ sender: Any) {
        
        
        if selectionList.selectedIndexes.count > 0{
            for index in selectionList.selectedIndexes{
                let tagID = self.allTags[index].id!
                self.selectedTagID.append(tagID)
            }
            
            handleAssignTagToProperty()
            
        }else{
            //AppConfigs.showSnacbar(message: "Select a Tag ", textColor: .red)
            handleAssignTagToProperty()
        }
        
        
        //        AppConfigs.showSnacbar(message: "Tag Assigned", textColor: .green)
        //        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func handleAssignTagToProperty() {
        
        print( self.selectedTagID)
        let joined =  self.selectedTagID.map { String($0) }.joined(separator: ",")
        print(joined)
        
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        let urlString = UrlManager.baseURL() + UrlManager.apiString() + "/property/\((globalSelectedPropertyFromList?.id)!)/assign-multiple-tags/" // "/list-properties/\(globalSelectedProperty.propertyID)/"
        print(urlString)
        let parameter:Parameters = [
            "tags" : joined
        ]
        print(parameter)
        
        Alamofire.request(urlString, method: .post, parameters: parameter as Parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
                
            case.success(let data):
                
                self.removeSpinner()
                print("success",data)
                let statusCode = response.response?.statusCode
                print(statusCode!)
                
                if statusCode == 200{
                    
                    let assignTagResponse = try? JSONDecoder().decode(AssignTagResponse.self, from: response.data!)
                    
                    if assignTagResponse?.status == false{
                        AppConfigs.showSnacbar(message: assignTagResponse?.message ?? "Tag Already Assigned", textColor: .green)
                        
                    }else{
                        AppConfigs.showSnacbar(message: (assignTagResponse?.message)!, textColor: .green)
                        _ = self.navigationController?.popViewController(animated: true)
                    }
                    
                }else{
                    self.showAlert(message: " ", title: "Error Occured")
                }
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)

                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
        }
        //self.view.activityStopAnimating()
        
    }
    
    
    func getAllTags(urlString: String) {
        
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success",data)
                let statusCode = response.response?.statusCode
                
                if statusCode == 200{
                    
                    let tagsResponse = try? JSONDecoder().decode(ListOfTagsResponse.self, from: response.data!)
                    print(tagsResponse)
                    self.allTags = tagsResponse!
                    DispatchQueue.main.async {
                        print("allTags------------\n",self.allTags)
                        self.removeSpinner()//self.view.activityStopAnimating()
                        //  self.tagTV.reloadData()
                        
                        for tag in self.allTags{
                            let name = tag.name
                            let colorCode = tag.color?.colorCode
                            self.allTagNames.append(name!)
                            self.allColorCodes.append(colorCode!)
                        }
                        
                        self.selectionList.items = self.allTagNames
                        
                        let existingTags = globalSelectedPropertyFromList?.propertyTags
                        if existingTags != nil  {
                            if existingTags!.count > 0 {
                                for etag in existingTags!{
                                    let ename = etag.name

                                     for (index, element) in self.allTagNames.enumerated() {
                                         print(index, ":", element)
                                        if element == ename{
                                            self.selectionList.selectedIndexes.append(index)
                                        }
                                     }
                                        
                                }
                            }
                        }
                    }
                }
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)

                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
}
//
extension AssignTagsViewController:  UITableViewDataSource {
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allTags.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AssignTagTableViewCell", for: indexPath) as! AssignTagTableViewCell
        
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [cell.lblTagTitle], type: TextType.ListTitle.rawValue)
        if allTags.count > indexPath.row{
            
            cell.lblTagTitle.text = (allTags[indexPath.row].name)!
            let tagColor = allTags[indexPath.row].color?.colorCode
            print(tagColor!)
            
            let color = UIColor(hexString: tagColor!)
            cell.btnTagColor.backgroundColor = color//UIColor(red:0.40, green:0.70, blue:1.00, alpha:1.0) // UIColor(hex: tagColor!)  //UIColor.init(hex: tagColor!)
        }
        
        cell.lblTagTitle?.textColor = self.listTitleColor
        cell.leftImageView.tintColor = UIColor(red:0.2, green:0.3, blue:0.41, alpha:1)
        
        if let myImage = UIImage(named: "active_status") {
            let tintableImage = myImage.withRenderingMode(.alwaysTemplate)
            cell.leftImageView.image = tintableImage
        }
        
        //cell.btnTagColor.backgroundColor = UIColor.red
        
        return cell
    }
    
}

extension AssignTagsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if allTags.count > indexPath.row{
            
            let cell = tableView.cellForRow(at: indexPath) as! AssignTagTableViewCell
            cell.lblTagTitle.textColor = UIColor(red:0.07, green:0.82, blue:0.41, alpha:1)
            cell.backgroundColor = UIColor.white
            cell.contentView.backgroundColor = UIColor.white
            tableView.separatorStyle = .none
            
            let tagColor = allTags[indexPath.row].color?.colorCode
            let color = UIColor(hexString: tagColor!)
            cell.btnTagColor.backgroundColor = color
            
            self.selectedTag = allTags[indexPath.row]
            if let myImage = UIImage(named: "selected") {
                let tintableImage = myImage.withRenderingMode(.alwaysOriginal)
                cell.leftImageView.image = tintableImage
            }
            btnDone.title = "DONE"
            btnDone.isEnabled = true
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! AssignTagTableViewCell
        cell.lblTagTitle.textColor = UIColor(red:0.2, green:0.3, blue:0.41, alpha:1)
        cell.backgroundColor = UIColor.white
        cell.contentView.backgroundColor = UIColor.white
        tableView.separatorStyle = .none
        cell.leftImageView.tintColor = UIColor(red:0.2, green:0.3, blue:0.41, alpha:1)
        
        if let myImage = UIImage(named: "active_status") {
            let tintableImage = myImage.withRenderingMode(.alwaysTemplate)
            cell.leftImageView.image = tintableImage
        }
        
        let tagColor = allTags[indexPath.row].color?.colorCode
        let color = UIColor(hexString: tagColor!)
        cell.btnTagColor.backgroundColor = color
        //cell.btnTagColor.backgroundColor = UIColor.red
        //tableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.none)
    }
}

