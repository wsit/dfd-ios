//
//  ChooseMailerWizardViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 7/24/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit
import Alamofire

var globalPropertyID :Int = 0
var globalSelectedTemplate: MailTemplate?
class ChooseMailerWizardViewController: UIViewController {
    
    
    @IBOutlet weak var templateCollectionView: UICollectionView!
    
    var allTemplates:[MailTemplate] = []
    var jsonResponse: GetAllTemplateResponse?
    var propertyID :Int = 0
    var neighbourID : Int = 0
    
    var parentString :String = ""// neighbour"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        templateCollectionView.register(UINib.init(nibName: "MailCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "MailCollectionViewCell")
        templateCollectionView.backgroundColor = .groupTableViewBackground
        
        
        
        if Network.reachability.status == .unreachable{
            AppConfigs.updateUserInterface(view: view)
        }else{

            allTemplates.removeAll()
            let url : String = UrlManager.baseURL() + UrlManager.apiString() + "/mail-wizard/all/"
            print(url)
            self.showSpinner(onView: view)
            getAllTemplates(urlString: url)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.mailWizardSelected(notification:)), name: Notification.Name("MailWizardSelected"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.neighbourMailWizardSelectionAction(notification:)), name: Notification.Name("NeighbourMailWizardSelected"), object: nil)
        
        
    }
    
    
    @objc func mailWizardSelected (notification: Notification) {
        print("Value of notification : ", notification.object ?? "")
        
        print("globalPropertyID", globalPropertyID)
        print("propertyID", self.propertyID)
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "EditMailTemplateViewController") as! EditMailTemplateViewController
        nextViewController.modalPresentationStyle = .fullScreen
        nextViewController.propertyID = globalPropertyID
        nextViewController.parentString = "nmw"
        
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @objc func neighbourMailWizardSelectionAction (notification: Notification) {
        print("Value of notification : ", notification.object ?? "")
        
        print("neighbourID", self.neighbourID)
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "EditMailTemplateViewController") as! EditMailTemplateViewController
        nextViewController.modalPresentationStyle = .fullScreen
        nextViewController.neighbourID = self.neighbourID
        
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        //        }
    }
    
    @IBAction func backClicked(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    
    func getAllTemplates(urlString: String) {
        
        
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success",data)
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    
                    let allLists = try? JSONDecoder().decode(GetAllTemplateResponse.self, from: response.data!)
                    self.jsonResponse = allLists
                    
                    self.removeSpinner()
                    if allLists?.status == true{
                        
                        var items: [MailTemplate] = self.allTemplates
                        items.append(contentsOf: (allLists?.data)!)
                        
                        self.allTemplates.removeAll()
                        self.allTemplates = items
                        
                        DispatchQueue.main.async {
                            print("allTemplates------------\n",self.allTemplates)
                            self.removeSpinner()//self.view.activityStopAnimating()
                            
                            self.templateCollectionView.reloadData()
                        }
                    }else{
                        self.removeSpinner()
                        AppConfigs.showSnacbar(message: allLists?.message ?? "", textColor: .red)
                    }
                }
                    
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)

                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
}

extension ChooseMailerWizardViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allTemplates.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MailCollectionViewCell", for: indexPath) as! MailCollectionViewCell
        
        cell.backgroundColor = .white
        cell.cornerRadius = 8
        
        if allTemplates.count > indexPath.row{
            
            let data = allTemplates[indexPath.row]
            let url = data.thumbURL ?? ""
            cell.mailImageView.pin_updateWithProgress = true
            cell.mailImageView.pin_setImage(from: URL(string: url))
            cell.lblName.text = data.code
            
            if  indexPath.row == 0{
                cell.isPremiumContent.isHidden = true
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width/2.0 - 16
        let height = width + 40
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if allTemplates.count > indexPath.row{
            
            let data = allTemplates[indexPath.row]
            
            if  indexPath.row != 0 && AppConfigs.getCurrentUserInfo().upgradeInfo?.plan?.id != 2
            {
                self.showAlert(message: "You don't have permission to do this", title: "Please upgrade your package")
            }else{
                
                if self.parentString == "neighbour"{
                    
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                    let vc = storyBoard.instantiateViewController(withIdentifier: "MailWizardPreviewViewController") as! MailWizardPreviewViewController
                    //vc.templateData = data
                    globalSelectedTemplate = data
                    vc.neighbourID = self.neighbourID
                    vc.parentString = "neighbour"
                    //self.present(vc, animated: true, completion: nil)
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    //MailWizardPreviewViewController
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                    let vc = storyBoard.instantiateViewController(withIdentifier: "MailWizardPreviewViewController") as! MailWizardPreviewViewController
                    //vc.templateData = data
                    globalSelectedTemplate = data
                    vc.propertyID = self.propertyID
                    globalPropertyID = self.propertyID
                    vc.parentString = "mw"
                    //vc.modalPresentationStyle = .fullScreen
                    //self.present(vc, animated: true, completion: nil)
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            
        }
    }
    
}
