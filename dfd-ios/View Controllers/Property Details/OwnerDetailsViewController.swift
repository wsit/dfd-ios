//
//  OwnerDetailsViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 7/19/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MessageUI

var globalCurrentPropertyDetails: PropertyDetailsResponse?

class OwnerDetailsViewController: UIViewController ,MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate{
     
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        dismiss(animated: true, completion: nil)
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        
        var message:String
        switch (result) {
            
        case .cancelled:
            print("Message was cancelled")
            message = "Message was cancelled"
        case .failed:
            print("Message failed")
            message = "Message was failed"
        case .sent:
            print("Message was sent")
            message = "Message was sent"
            
        default:
            return
        }
        dismiss(animated: true, completion: nil)
        showAlertForMessage(message: message)
    }
    
    
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var socialView: UIView!
    @IBOutlet weak var heightForSocialView: NSLayoutConstraint!
    @IBOutlet weak var mobileNumberView: UIView!
    @IBOutlet weak var heightForMobileNumberView: NSLayoutConstraint!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var heightForContentView: NSLayoutConstraint!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var bottomViewTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var lblPT: UILabel!
    @IBOutlet weak var imgPT: UIImageView!
    @IBOutlet weak var ptView: UIView!
    
    @IBOutlet weak var oiView: UIView!
    @IBOutlet weak var imgOI: UIImageView!
    @IBOutlet weak var lblOI: UILabel!
    
    @IBOutlet weak var miView: UIView!
    @IBOutlet weak var imgMW: UIImageView!
    @IBOutlet weak var lblMW: UILabel!
    
    var contentRect = CGRect.zero
    var propertyID : Int = 0
    var powerTraceData: PowerTraceData?
    var mobileNumbers : [String] = []
    var titleForNav : String = ""
    var currentPropertyDetails: PropertyDetailsResponse?
    
    var parentSring: String = ""
    var neighbourID: String = ""
    var selectedNeighbour: GetNeighbourResponseElement?
    var emailHeight : CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.ptView.backgroundColor = UIColor(red:1.00, green:0.85, blue:0.85, alpha:1.0)
        self.oiView.backgroundColor = UIColor(red:0.80, green:0.90, blue:1.00, alpha:1.0)
        self.miView.backgroundColor = UIColor(red:0.80, green:0.90, blue:1.00, alpha:1.0)
        
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [lblAddress, lblEmail, lblName], type: TextType.ListTitle.rawValue)
        
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [lblMW, lblOI, lblPT], type: TextType.Capsule.rawValue)
        
        if self.currentPropertyDetails != nil{
            self.lblAddress.text = self.currentPropertyDetails?.ownerInfo![0].fullAddress
            self.lblName.text = self.currentPropertyDetails?.ownerInfo![0].fullName
        }else{
            // self.imgOI.image = UIImage(named: "ic_owner_details")
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadOwnerInfoData(_:)), name: NSNotification.Name(rawValue: "SingleOwnerInfoNotif"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadPowerTraceData(_:)), name: NSNotification.Name(rawValue: "SinglePowerTracedNotif"), object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = titleForNav
        self.mobileNumbers.removeAll()
        
        if parentSring == "neighbour"{
            
            self.title = "Neighbour Info"
            for data in globalNeighboursResponse{
                let id = data.id!
                let nid = Int(neighbourID)
                
                if nid == id{
                    self.selectedNeighbour = data
                    print(self.propertyID)
                    self.propertyID = (self.selectedNeighbour?.property!)!
                    print(self.propertyID)
                    if data.powerTrace == nil{
                        self.imgPT.image = UIImage(named: "ic_powertrace")
                    }
                    if data.ownershipInfo == nil{
                        self.imgOI.image = UIImage(named: "ic_owner_details")
                    }
                    
                    self.getNeighbourDetails(neighbour: data)
                }
            }
        }else{
            
            if currentPropertyDetails?.powerTraceRequestID != nil{
                if Network.reachability.status == .unreachable{
                    AppConfigs.updateUserInterface(view: view)
                }else{
                    
                    let url : String = UrlManager.baseURL() + UrlManager.apiString() + "/property/\(propertyID)/get-existing-power-trace/"
                    
                    print(url)
                    self.showSpinner(onView: view)
                    getPowerTraceInfo(urlString: url)
                    
                }//       }
            }else{
                self.imgPT.image = UIImage(named: "ic_powertrace")
            }
            
            if currentPropertyDetails?.ownerInfo?[0].fullAddress == nil{
                self.imgOI.image = UIImage(named: "ic_owner_details")
            }
        }
        
    }
    
    
    @objc func reloadOwnerInfoData(_ notification: NSNotification) {
        
        let url : String = UrlManager.baseURL() + UrlManager.apiString() + "/property/\(propertyID)/"
        print(url)
        // self.showSpinner(onView: view)
        self.getlistedPropertyDetails(urlString: url)
        
        
        
    }
    
    func showAlertForMessage(message:String){
        print("IN SHOW ALERT")
        self.showAlert(message: message)
        
    }
    
    func sendEmail(recipientArray: [String]) {
        
        //var recipientArray = ["mhcsunny@gmail.com", "hasan@workspaceit.com"]
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            
            mail.setToRecipients(recipientArray)
            mail.setMessageBody("Write here", isHTML: false)

            present(mail, animated: true)
        } else {
            // show failure alert
            
            self.showAlertForMessage(message: "Set Up your email please")
         }
    }
 
    func getlistedPropertyDetails(urlString: String)  {
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        
        Alamofire.request(urlString, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
                
            case.success(let data):
                
                self.removeSpinner()
                //print("success",data)
                let statusCode = response.response?.statusCode
                print(statusCode!)
                
                if statusCode == 200{
                    
                    let singlePropertyDetailsResponse = try? JSONDecoder().decode(PropertyDetailsResponse.self, from: response.data!)
                    
                    if singlePropertyDetailsResponse == nil{
                        print("singlePropertyDetailsResponse == nil")
                    }else{
                        
                        //globalCurrentPropertyDetails = singlePropertyDetailsResponse
                        
                        self.currentPropertyDetails = singlePropertyDetailsResponse
                         
                        //replace current drive property info with latest info by matching id
                        
                        DispatchQueue.main.async {
                            
                            if self.currentPropertyDetails != nil{
                                self.lblAddress.text = self.currentPropertyDetails?.ownerInfo![0].fullAddress
                                self.lblName.text = self.currentPropertyDetails?.ownerInfo![0].fullName
                            }else{
                                print("currentPropertyDetails == nil")
                            }
                        }
                    }
                    
                }else{
                    self.showAlert(message: " ", title: "Some Error Occured")
                }
                
            case.failure(let error):
                print("Not Success",error)
                
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                
                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
        }
    }
    
    
    @objc func reloadPowerTraceData(_ notification: NSNotification) {
        
        self.mobileNumbers.removeAll()
        let url : String = UrlManager.baseURL() + UrlManager.apiString() + "/property/\(propertyID)/get-existing-power-trace/"
        
        print(url)
        //self.showSpinner(onView: view)
        getPowerTraceInfo(urlString: url)
    }
    
    
    @IBAction func backClicked(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func ownerInfoClicked(_ sender: Any) {
        
        print(self.propertyID)
        print(self.selectedNeighbour?.id)
        if parentSring == ""{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PopUpForOwnerAndPowerTraceViewController") as! PopUpForOwnerAndPowerTraceViewController
            vc.modalPresentationStyle = .overCurrentContext
            vc.parentString = "od"
            if self.currentPropertyDetails?.ownerInfo![0].fullName != nil{
                vc.isReOwnerInfoClicked = true
            }
            self.present(vc, animated: true, completion: nil)
        }else{
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "GetNeighbourPopUpViewController") as! GetNeighbourPopUpViewController
            vc.modalPresentationStyle = .overCurrentContext
            vc.parentVC = "nod"
            vc.selectedNeighbour = self.selectedNeighbour
            vc.propertyID = self.propertyID
            if selectedNeighbour?.ownershipInfo != nil{
                vc.isReOwnerInfoClicked = true
            }
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func powerTraceClicked(_ sender: Any) {
        if parentSring == ""{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PopUpForOwnerAndPowerTraceViewController") as! PopUpForOwnerAndPowerTraceViewController
            vc.modalPresentationStyle = .overCurrentContext
            vc.parentString = "pt"
            if self.currentPropertyDetails?.powerTraceRequestID != nil{
                vc.isRePowerTracedClicked = true
            }
            self.present(vc, animated: true, completion: nil)
        }else{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "GetNeighbourPopUpViewController") as! GetNeighbourPopUpViewController
            
            vc.modalPresentationStyle = .overCurrentContext
            vc.parentVC = "npt"
            vc.propertyID = self.propertyID
            vc.selectedNeighbour = self.selectedNeighbour
            if self.selectedNeighbour?.powerTrace != nil{
                vc.isRePowerTracedClicked = true
            }
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func mailerWizardClicked(_ sender: Any) {
        
        if self.parentSring == ""{
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChooseMailerWizardViewController") as! ChooseMailerWizardViewController
            vc.modalPresentationStyle = .overCurrentContext
            vc.propertyID = self.propertyID
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChooseMailerWizardViewController") as! ChooseMailerWizardViewController
            vc.neighbourID =  self.selectedNeighbour?.id ?? 0
            vc.parentString = "neighbour"
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    @objc func socialLabelTapped(_ sender: MyTapGesture ) {
        print("labelTapped")
        print(sender.title)
        guard let url = URL(string: sender.title) else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @objc func emailLabelTapped(_ sender: MyTapGesture ) {
        print("emaillabelTapped")
        print(sender.title)
        
        let emailArray = sender.title.components(separatedBy: ",")
        print("emailArray--", emailArray.count)
        
        var array: [String] = []
//        for data in emailArray{
//
//            let mail = data.trimmingCharacters(in: .whitespacesAndNewlines)
//            array.append(mail)
//            print("email---\(mail)." )
//        }
//        print("mail count--", array.count)
        let endIndex = sender.title.range(of: " ")!.lowerBound
        //let str:String = sender.title.substring(to: endIndex).trimmingCharacters(in: .whitespacesAndNewlines)
        //print("str---",str)
        
        let tmp = extractEmailAddrIn(text: sender.title)
        print("tmp------",tmp)
        
//        UIPasteboard.general.string = sender.title
//        if let myString = UIPasteboard.general.string {
//            AppConfigs.showSnacbar(message: "Copied to Clipboard", textColor: .green)
//            print("myString-->", myString)
//        }
//        
        sendEmail(recipientArray: tmp)
        
    }
    
    func extractEmailAddrIn(text: String) -> [String] {
        var results = [String]()

        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        let nsText = text as NSString
        do {
            let regExp = try NSRegularExpression(pattern: emailRegex, options: NSRegularExpression.Options.caseInsensitive)
            let range = NSMakeRange(0, text.count)
            let matches = regExp.matches(in: text, options: .reportProgress, range: range)

            for match in matches {
                let matchRange = match.range
                results.append(nsText.substring(with: matchRange))
            }

        } catch _ {
        }

        return results
    }
    
    @objc func labelTapped(_ sender: MyTapGesture ) {
       
        print("labelTapped")
        print(sender.title)
        
        let nmbr = sender.title.components(separatedBy: CharacterSet.decimalDigits.inverted)
            .joined()
        print("nmbr---",nmbr)
        
        let alert = UIAlertController(title: nmbr, message: "select an option", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Call", style: .default , handler:{ (UIAlertAction)in
            print("User click Call button")
            
            if let url = URL(string: "tel://\(nmbr)"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }))

        alert.addAction(UIAlertAction(title: "Send SMS", style: .default , handler:{ (UIAlertAction)in
            print("User click SMS button")
            let messageVC = MFMessageComposeViewController()
            messageVC.body = "Enter a message details here";
            messageVC.recipients = [nmbr]
            messageVC.messageComposeDelegate = self
            self.present(messageVC, animated: true, completion: nil)
            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel , handler:{ (UIAlertAction)in
            print("User click Cancel button")
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.alertClose(_:)))
            //UITapGestureRecognizer(target: self, action: #selector(alertClose(_:)))
            alert.view.superview?.addGestureRecognizer(tapGestureRecognizer)
        })
        
    }
    
    @objc func alertClose(_ gesture: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func getPowerTraceInfo(urlString: String) {
        
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success",data)
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    
                    do {
                        let json = try JSON(data: response.data!)
                        let status = json["status"]
                        print(status)
                        
                        DispatchQueue.main.async {
                            
                            if status == true{
                                
                                let jsondata = json["data"]
                                let address = jsondata["address"].stringValue
                                
                                let email = jsondata["email"].stringValue
                                let lowerCased = email.lowercased()
                                let replaced = lowerCased.replacingOccurrences(of: ",", with: "\n")
                                print(replaced)
                                
                                let height = AppConfigs.heightForView(text: lowerCased, font: ProNovaR16, width: self.lblEmail.frame.width)
                                print("height", height)
                                self.emailHeight = height
                                
                                let emailArray = lowerCased.components(separatedBy: ",")
                                print(emailArray)
                                
                                let first_name = jsondata["first_name"].stringValue
                                let last_name = jsondata["last_name"].stringValue
                                
                                self.lblName.text = "\(String(describing: first_name)) \(String(describing: last_name))"
                                self.lblEmail.text = replaced
                                self.lblAddress.text = address
                                
                                let tappyEmail = MyTapGesture(target: self, action: #selector(self.emailLabelTapped(_:)))
                                self.lblEmail.addGestureRecognizer(tappyEmail)
                                self.lblEmail.isUserInteractionEnabled = true
                                tappyEmail.title = replaced
                                
                                
                                var yPos = CGFloat(0)
                                let social = jsondata["social"].stringValue
                                
                                if social != nil && social != ""{
                                    let socialArray = social.components(separatedBy: ", ")
                                    print(socialArray)
                                    
                                    //reset height for social view
                                    self.heightForSocialView.constant = 0
                                    self.socialView.layoutIfNeeded()
                                    
                                    for i in 0..<socialArray.count {
                                        let element = socialArray[i]
                                        let labelNum = UILabel()
                                        let imgView = UIImageView()
                                        
                                        labelNum.text = element
                                        labelNum.textColor = .black
                                        labelNum.textAlignment = .left
                                        labelNum.numberOfLines = 2
                                        labelNum.font = ProNovaR16
                                        labelNum.lineBreakMode = NSLineBreakMode.byWordWrapping
                                        labelNum.sizeToFit()
                                        
                                        imgView.frame = CGRect(x: 0, y: yPos, width:40, height: 40)
                                        labelNum.frame = CGRect( x:40, y:yPos, width:self.socialView.frame.width - imgView.frame.width - 8 , height: 50)
                                        let tappy = MyTapGesture(target: self, action: #selector(self.socialLabelTapped(_:)))
                                        labelNum.addGestureRecognizer(tappy)
                                        labelNum.isUserInteractionEnabled = true
                                        tappy.title = labelNum.text!
                                        
                                        if element.contains("www.facebook.com") {
                                            print("facebook exists")
                                            imgView.image = UIImage(named: "new_ic_fb")
                                        }
                                        else if element.contains("www.twitter.com") {
                                            print(" twitter exists")
                                            imgView.image = UIImage(named: "new_ic_twitter")
                                        }
                                        else if element.contains("google") {
                                            print("google exists")
                                            imgView.image = UIImage(named: "new_ic_google")
                                        }
                                        else if element.contains("www.linkedin.com") {
                                            print("linkedin exists")
                                            imgView.image = UIImage(named: "new_ic_ln")
                                        }else{
                                            imgView.image = UIImage(named: "new_ic_links")
                                        }
                                        self.socialView.addSubview(imgView)
                                        self.socialView.addSubview(labelNum)
                                        
                                        yPos += 50
                                        //add height to social view
                                        self.heightForSocialView.constant = yPos
                                        self.socialView.layoutIfNeeded()
                                        
                                    }
                                }else{
                                    self.heightForSocialView.constant = 0
                                    self.socialView.layoutIfNeeded()
                                }
                                
                                let verifiedCellphones = jsondata["verified_cellphones"].stringValue
                                print(verifiedCellphones)
                                let landline = jsondata["landline"].stringValue
                                print(landline)
                                let verified_landline = jsondata["verified_landline"].stringValue
                                print(verified_landline)
                                let cellphones = jsondata["cellphones"].stringValue
                                print(cellphones)
                                
                                if verifiedCellphones != ""{
                                    
                                    let arr1 = verifiedCellphones.components(separatedBy: ",")
                                    print(arr1)
                                    for item in arr1{self.mobileNumbers.append(item)
                                    }
                                }
                                if landline != ""{
                                    
                                    let arr1 = landline.components(separatedBy: ",")
                                    print(arr1)
                                    for item in arr1{self.mobileNumbers.append(item)
                                    }
                                }
                                if verified_landline != ""{
                                    
                                    let arr1 = verified_landline.components(separatedBy: ",")
                                    print(arr1)
                                    for item in arr1{self.mobileNumbers.append(item)
                                    }
                                }
                                
                                if cellphones != ""{
                                    
                                    let arr1 = cellphones.components(separatedBy: ",")
                                    print(arr1)
                                    for item in arr1{self.mobileNumbers.append(item)
                                    }
                                }
                                print("Total Mobile No Count", self.mobileNumbers.count)
                                
                                var yPos2 = CGFloat(0)
                                if self.mobileNumbers.count > 0{
                                    
                                    self.heightForMobileNumberView.constant = 0
                                    self.mobileNumberView.layoutIfNeeded()
                                    
                                    for i in 0..<self.mobileNumbers.count {
                                        let element = self.mobileNumbers[i]
                                        
                                        let labelNum = UILabel()
                                        labelNum.text = element
                                        labelNum.textColor = .black
                                        labelNum.textAlignment = .left
                                        labelNum.numberOfLines = 0
                                        labelNum.lineBreakMode = NSLineBreakMode.byWordWrapping
                                        labelNum.sizeToFit()
                                        
                                        FontAndColorConfigs.setDynamicLabelFontSize(labels: [labelNum], type: TextType.ListTitle.rawValue)
                                        labelNum.frame = CGRect( x:0, y:yPos2, width:self.mobileNumberView.frame.width - 16 , height: 30)
                                        
                                        
                                        self.mobileNumberView.addSubview(labelNum)

                                        yPos2 += 30
                                        self.heightForMobileNumberView.constant = yPos2
                                        self.mobileNumberView.layoutIfNeeded()
                                        
                                        
                                        let tappy = MyTapGesture(target: self, action: #selector(self.labelTapped(_:)))
                                        labelNum.isUserInteractionEnabled = true
                                        labelNum.addGestureRecognizer(tappy)
                                        tappy.title = labelNum.text!
                                        
                                    }
                                }else{
                                    self.heightForMobileNumberView.constant = 0
                                    self.mobileNumberView.layoutIfNeeded()
                                }
                                
                                for view in self.contentView.subviews {
                                    self.contentRect = self.contentRect.union(view.frame)
                                }
                                print("self.heightForMobileNumberView.constant ", self.heightForMobileNumberView.constant )
                                print("self.emailHeight ", self.emailHeight )
                                print("self.heightForMobileNumberView.constant",self.heightForMobileNumberView.constant)
                                
                                self.heightForContentView.constant = self.heightForMobileNumberView.constant//self.heightForSocialView.constant + self.heightForMobileNumberView.constant// + self.emailHeight - 150// + 80 //self.contentRect.height
                                self.bottomView.layoutIfNeeded()
                                self.contentView.needsUpdateConstraints()
                                self.contentView.layoutIfNeeded()
                                
                                print( "self.heightForContentView.constant", self.heightForContentView.constant)
                                //self.socialView
                            }else{
                                let message = json["message"].string
                                AppConfigs.showSnacbar(message: message!, textColor: .red)
                            }
                        }
                        
                    } catch let error as NSError {
                        // error
                    }
                    
                    
                }
                    
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                
                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
    
    func getNeighbourDetails(neighbour: GetNeighbourResponseElement) {
        self.lblName.text = neighbour.ownershipInfo?.fullName
        let email = neighbour.powerTrace?.email
        let lowerCased = email?.lowercased()
        let replaced = lowerCased?.replacingOccurrences(of: ",", with: "\n")
        print(replaced)
        
        let height = AppConfigs.heightForView(text: lowerCased ?? "", font: ProNovaR16, width: self.lblEmail.frame.width)
        print("height", height)
        self.emailHeight = height
        
        let emailArray = lowerCased?.components(separatedBy: ", ")
        //print(emailArray)
        
        self.lblEmail.text = replaced
        self.lblAddress.text = neighbour.neighborAddress
        
        let tappyEmail = MyTapGesture(target: self, action: #selector(self.emailLabelTapped(_:)))
        self.lblEmail.addGestureRecognizer(tappyEmail)
        self.lblEmail.isUserInteractionEnabled = true
        tappyEmail.title = replaced ?? ""
        
        var yPos = CGFloat(0)//frm.origin.y + 16 //self.socialView.frame.origin.y +  self.socialView.frame.size.height + 48 //self.lblName.frame.size.height + self.lblEmail.frame.size.height + self.lblAddress.frame.size.height +  48//self.socialView.frame.origin.y +  self.socialView.frame.size.height + 48
        let social = neighbour.powerTrace?.social//jsondata["social"].stringValue
        
        if social != nil && social != ""{
            let socialArray: [String] = social?.components(separatedBy: ", ") ?? []
            print(socialArray)
            
            for i in 0..<socialArray.count {
                let element = socialArray[i]
                let labelNum = UILabel()
                let imgView = UIImageView()
                
                labelNum.text = element
                labelNum.textColor = .black
                labelNum.textAlignment = .left
                labelNum.numberOfLines = 2
                labelNum.font = ProNovaR16
                labelNum.adjustsFontSizeToFitWidth = true
                labelNum.lineBreakMode = .byWordWrapping//NSLineBreakMode.byWordWrapping
                labelNum.sizeToFit()
                
                
                FontAndColorConfigs.setDynamicLabelFontSize(labels: [labelNum], type: TextType.ListTitle.rawValue)
                
                imgView.frame = CGRect(x: 0, y: yPos, width: 40, height: 40)
                labelNum.frame = CGRect( x:40, y:yPos, width:self.socialView.frame.width - imgView.frame.width - 8 , height: 50)
                
                let tappy = MyTapGesture(target: self, action: #selector(self.socialLabelTapped(_:)))
                labelNum.isUserInteractionEnabled = true
                labelNum.addGestureRecognizer(tappy)
                tappy.title = labelNum.text!
                
                if element.contains("www.facebook.com") {
                    print("facebook exists")
                    imgView.image = UIImage(named: "new_ic_fb")
                }
                else if element.contains("www.twitter.com") {
                    print(" twitter exists")
                    imgView.image = UIImage(named: "new_ic_twitter")
                }
                else if element.contains("google") {
                    print("google exists")
                    imgView.image = UIImage(named: "new_ic_google")
                }
                else if element.contains("www.linkedin.com") {
                    print("linkedin exists")
                    imgView.image = UIImage(named: "new_ic_ln")
                }else{
                    imgView.image = UIImage(named: "new_ic_links")
                }
                self.socialView.addSubview(imgView)
                self.socialView.addSubview(labelNum)
                
                yPos += 50
                self.heightForSocialView.constant = yPos
                self.socialView.layoutIfNeeded()
            }
        }else{
            self.heightForSocialView.constant = 0
            self.socialView.layoutIfNeeded()
        }
        
        let verifiedCellphones = neighbour.powerTrace?.verifiedCellphones//jsondata["verified_cellphones"].stringValue
        let landline = neighbour.powerTrace?.landline//jsondata["landline"].stringValue
        let verified_landline = neighbour.powerTrace?.verifiedLandline//jsondata["verified_landline"].stringValue
        let cellphones = neighbour.powerTrace?.cellphones//sondata["cellphones"].stringValue
        
        if verifiedCellphones != ""{
            
            let arr1: [String] = verifiedCellphones?.components(separatedBy: ",") ?? []
            print(arr1)
            for item in arr1{self.mobileNumbers.append(item)
            }
        }
        if landline != ""{
            
            let arr1: [String] = landline?.components(separatedBy: ",") ?? []
            print(arr1)
            for item in arr1{
                self.mobileNumbers.append(item)
            }
        }
        if verified_landline != ""{
            
            let arr1: [String] = verified_landline?.components(separatedBy: ",") ?? []
            print(arr1)
            for item in arr1{
                self.mobileNumbers.append(item)
            }
        }
        
        if cellphones != ""{
            
            let arr1 : [String] = cellphones?.components(separatedBy: ",") ?? []
            print(arr1)
            for item in arr1{
                self.mobileNumbers.append(item)
            }
        }
        print("Total Mobile No Count", self.mobileNumbers.count)
        
        var yPos2 = CGFloat(0)
        if self.mobileNumbers.count > 0{
            
            for i in 0..<self.mobileNumbers.count {
                let element = self.mobileNumbers[i]
                
                let labelNum = UILabel()
                labelNum.text = element
                labelNum.textColor = .black
                labelNum.textAlignment = .left
                labelNum.numberOfLines = 0
                labelNum.lineBreakMode = NSLineBreakMode.byWordWrapping
                labelNum.sizeToFit()
                FontAndColorConfigs.setDynamicLabelFontSize(labels: [labelNum], type: TextType.ListTitle.rawValue)
                labelNum.frame = CGRect( x:0, y:yPos2, width:self.mobileNumberView.frame.width - 16 , height: 30)
                self.mobileNumberView.addSubview(labelNum)
                
                yPos2 += 30
                self.heightForMobileNumberView.constant = yPos2
                self.mobileNumberView.layoutIfNeeded()
                
                let tappy = MyTapGesture(target: self, action: #selector(self.labelTapped(_:)))
                labelNum.isUserInteractionEnabled = true
                labelNum.addGestureRecognizer(tappy)
                tappy.title = labelNum.text!
            }
        }else{
            self.heightForMobileNumberView.constant = 0
            self.mobileNumberView.layoutIfNeeded()
        }
        
        if neighbour.status == "requested"{
            AppConfigs.showSnacbar(message: ErrorMessage.Requested.rawValue, textColor: .red)
        }else{
            
        }
        
        print("self.heightForMobileNumberView.constant ", self.heightForMobileNumberView.constant )
        print("self.emailHeight ", self.emailHeight )
        print("self.heightForMobileNumberView.constant",self.heightForMobileNumberView.constant)
        
        self.heightForContentView.constant =  self.heightForMobileNumberView.constant//self.heightForSocialView.constant + self.heightForMobileNumberView.constant //+ self.emailHeight - 150 //+ 80 //self.contentRect.height
        self.contentView.needsUpdateConstraints()
        self.contentView.layoutIfNeeded()
        print( "self.heightForContentView.constant", self.heightForContentView.constant)
        
    }
}


class MyTapGesture: UITapGestureRecognizer {
    var title = String()
}
