//
//  EditMailTemplateViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 7/24/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class EditMailTemplateViewController: UIViewController, UITextViewDelegate {
    
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var txtView: UITextView!
    
    var templateData: MailTemplate?
    var propertyID : Int = 0
    var neighbourID : Int = 0
    var parentString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        FontAndColorConfigs.setDynamicButtonsFontSize(buttons: [btnNext], type: TextType.ListTitle.rawValue)
        self.btnNext.backgroundColor = self.buttonBackgroundColor
        txtView.delegate = self
        txtView.text = "type response content here"
        txtView.textColor = UIColor.lightGray
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if txtView.text.isEmpty {
            txtView.text = "type response content here"
            txtView.textColor = UIColor.lightGray
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnNextClicked(_ sender: Any) {
        
        if txtView.text == "type response content here"{
            AppConfigs.showSnacbar(message: "Please insert response content", textColor: .red)
            
        }else{
            
            if parentString == "nmw"{
                
                let url = UrlManager.baseURL() + UrlManager.apiString() + "/mail-wizard/neighbor/\(self.neighbourID)/"
                print(url)
                self.handleSendMail(urlString: url)
            }else{
                
                let url = UrlManager.baseURL() + UrlManager.apiString() + "/mail-wizard/property/\(self.propertyID)/"
                print(url)
                self.handleSendMail(urlString: url)
            }
        }
        
    }
    
    func handleSendMail(urlString: String) {
        print(urlString)
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        
        print(txtView.text ?? "")
        let parameters = [
            "mail_text" : txtView.text!,
            "tem_item_id" : Int((globalSelectedTemplate?.itemID)!)!,
            "subs_id" : 0
            ] as [String : Any]
        print(parameters)
        
        Alamofire.request(urlString, method: .post, parameters: parameters as Parameters, encoding: URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                print("success",data)
                
                self.removeSpinner()
                let statusCode = response.response?.statusCode
                print(statusCode!)
                
                
                do{
                    
                    let data = try JSON(data: response.data!)
                    let status = data["status"].boolValue
                    let message = data["message"].stringValue
                    
                    if statusCode == 200{
                        
                        
                        DispatchQueue.main.async {
                            self.removeSpinner()
                            
                            if status == true{
                                AppConfigs.showSnacbar(message: message, textColor: .green)
                                self.navigationController?.popViewController(animated: true)
                            }else{
                                
                                AppConfigs.showSnacbar(message: message, textColor: .green)
                                
                            }
                        }
                        
                    }else if statusCode == 401{
                        self.removeSpinner()
                        let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                        print("error with response status: \(String(describing: statusCode))")
                        //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                       // AppConfigs.showSnacbar(message: message, textColor: .green)
                    }
                        
                    else{
                        self.removeSpinner()
                        print("error with response status: \(String(describing: statusCode))")
                        AppConfigs.showSnacbar(message: message, textColor: .green)
                        //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                    }
                }catch let error{
                    
                    AppConfigs.showSnacbar(message: error.localizedDescription, textColor: .green)
                    
                }
                
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)

                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
                self.removeSpinner()
            }
            self.removeSpinner()
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

