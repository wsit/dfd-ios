//
//  MailerWizardPopUpViewController.swift
//  HouZes
//
//  Created by Mahadhi Hassan Chowdhury on 1/20/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//

import UIKit
import DropDown
import Alamofire
import SwiftyJSON
//MailWizardInputTableViewController
class MailerWizardPopUpViewController: UIViewController, UITextFieldDelegate {
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("While entering the characters this method gets called")
        
        let nmbr = Double(string) ?? 1
        let amount = nmbr * mailWizardAmount
        self.lblCost.text = String(format: "Total Cost: $%.2f", self.totalAmount)//"Total Cost: $\(amount)"
        self.totalAmount = amount
        print(totalAmount)
        return true;
    }
    
    let dropDown = DropDown()
    
    @IBOutlet weak var btnDropDown: UIButton!
    @IBOutlet weak var btnImageDropdown: UIButton!
    @IBOutlet weak var txtNoOfMailOccurances: UITextField!
    @IBOutlet weak var lblCost: UILabel!
    @IBOutlet weak var btnConfirm: UIButton!
    
    var allList : [MailWizardSubcriptionTypeResponseElement] = []
    var listTitleArray : [String] = []
    var listIDArray : [Int] = []
    
    var neighbourID : Int = 0
    var parentString : String = ""
    var mailWizardAmount : Double = 0.0
    var totalAmount : Double = 0.0
    
    var selectedTypeID :Int  = 0
    var propertyID: Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtNoOfMailOccurances.delegate = self
        txtNoOfMailOccurances.addTarget(self, action: #selector(MailerWizardPopUpViewController.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        self.mailWizardAmount = AppConfigs.getCurrentUserInfo().upgradeInfo?.mailWizard ?? 0.0
        self.lblCost.text = String(format: "Total Cost: $%.2f", self.mailWizardAmount)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if Network.reachability.status == .unreachable{
            AppConfigs.updateUserInterface(view: view)
        }else{

            allList.removeAll()
            let url : String = UrlManager.baseURL() + UrlManager.apiString() + "/mail-wizard-subscriptions/"
            
            print(url)
            self.showSpinner(onView: btnDropDown)
            getAllLists(urlString: url)
        }
        
        
        //                }
        
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        let string = textField.text ?? "1"
        let nmbr = Double(string) ?? 1
        let amount = nmbr * mailWizardAmount
        
        self.totalAmount = amount
        print(totalAmount)
        self.lblCost.text = String(format: "Total Cost: $%.2f", self.totalAmount)//"Total Cost: $\(amount)"
    }
    
    @IBAction func blurSectionClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func dropdownButtonClicked(_ sender: Any) {
        
        dropDown.show()
    }
    
    @IBAction func minimiseButtonClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func btnConfirmClicked(_ sender: Any) {
        
        if txtNoOfMailOccurances.text?.isEmpty == true{
            AppConfigs.showSnacbar(message: "Please enter number of occurance", textColor: .red)
            
        }
        else{
            
            let count = Int(txtNoOfMailOccurances.text!) ?? 0
            if count > 0{
                
                if parentString == "nmw"{
                    
                    let url = UrlManager.baseURL() + UrlManager.apiString() + "/mail-wizard/neighbor/\(self.neighbourID)/"
                    print(url)
                    self.handleSendMail(urlString: url)
                }else{
                    
                    let url = UrlManager.baseURL() + UrlManager.apiString() + "/mail-wizard/property/\(self.propertyID)/"
                    print(url)
                    self.handleSendMail(urlString: url)
                }
            }else{
                AppConfigs.showSnacbar(message: "Number of occurance value must greater than 0", textColor: .red)
            }
            
        }
        
    }
    
    
    func handleSendMail(urlString: String) {
        print(urlString)
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        
        let count = Int(txtNoOfMailOccurances.text!) ?? 1
        
        let parameters = [
            //               "mail_text" : txtView.text!,
            "tem_item_id" : Int((globalSelectedTemplate?.itemID)!)!,
            "subs_id" :  self.selectedTypeID,
            "mail_count" : count
            ] as [String : Any]
        
        /*
         {
         "tem_item_id" : 32,
         "subs_id" : 1,
         "mail_count" : 2
         }
         */
        print(parameters)
        
        Alamofire.request(urlString, method: .post, parameters: parameters as Parameters, encoding: URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                print("success",data)
                
                self.removeSpinner()
                let statusCode = response.response?.statusCode
                print(statusCode!)
                
                do{
                    
                    let data = try JSON(data: response.data!)
                    let status = data["status"].boolValue
                    let message = data["message"].stringValue
                    
                    if statusCode == 200{
                        
                        DispatchQueue.main.async {
                            self.removeSpinner()
                            
                            if status == true{
                                AppConfigs.showSnacbar(message: message, textColor: .green)
                                self.dismiss(animated: true, completion: nil)//self.navigationController?.popViewController(animated: true)
                            }else{
                                
                                let jsonData = data["data"]
                                let payment = jsonData["payment"].boolValue
                               
                                if payment == false{
                                    if AppConfigs.getCurrentUserInfo().isAdmin == false{
                                        AppConfigs.showSnacbar(message: message, textColor: .green)
                                    }else{

//                                       let alertController = UIAlertController(title: message, message: "Please go to web to manage your account.", preferredStyle: .alert)
//                                       let OKAction = UIAlertAction(title: "Ok", style: .default) { (alert) in
//
//                                       }
//                                       alertController.addAction(OKAction)
//                                       self.present(alertController, animated: true, completion: nil)
                                        
                                        let alertController = UIAlertController(title: message, message: "", preferredStyle: .alert)
                                        let OKAction = UIAlertAction(title: "Ok", style: .default) { (alert) in

                                           if AppConfigs.getCurrentUserInfo().thinker == true{
                                                
                                                let main = UIStoryboard(name: "Settings", bundle: nil)
                                                let vc = main.instantiateViewController(withIdentifier: "RechargeForIAPViewController") as! RechargeForIAPViewController
                                                //self.navigationController?.pushViewController(vc, animated: true)
                                                vc.modalPresentationStyle = .fullScreen
                                                self.present(vc, animated: true, completion: nil)
                                            }else{
                                                
                                                let main = UIStoryboard(name: "Main", bundle: nil)
                                                let vc = main.instantiateViewController(withIdentifier: "RechargeViewController") as! RechargeViewController
                                                //self.navigationController?.pushViewController(vc, animated: true)
                                                vc.modalPresentationStyle = .fullScreen
                                                self.present(vc, animated: true, completion: nil)
                                            }
                                        }
                                        alertController.addAction(OKAction)
                                        self.present(alertController, animated: true, completion: nil)
                                    }
                                }else{
                                    AppConfigs.showSnacbar(message: message, textColor: .green)
                                }
                            }
                        }
                        
                    }else if statusCode == 401{
                        self.removeSpinner()
                        let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                        print("error with response status: \(String(describing: statusCode))")
                        //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                        AppConfigs.showSnacbar(message: message, textColor: .green)
                    }
                        
                    else{
                        self.removeSpinner()
                        print("error with response status: \(String(describing: statusCode))")
                        AppConfigs.showSnacbar(message: message, textColor: .green)
                        //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                    }
                }catch let error{
                    
                    AppConfigs.showSnacbar(message: error.localizedDescription, textColor: .green)
                    
                }
                
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)

                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
             }
            self.removeSpinner()
        }
    }
    
    
    func setUpDropDownList(list: Array<String>) {
        self.btnDropDown.setTitle(self.allList[0].typeName, for: .normal)
        self.selectedTypeID = self.allList[0].id ?? 0
        
        dropDown.anchorView = btnDropDown
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y: btnDropDown.bounds.height)
        dropDown.dataSource = list
        dropDown.width = self.btnDropDown.frame.width
        dropDown.textFont = FontAndColorConfigs.getProximaRegular16()
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.btnDropDown.setTitle(item, for: .normal)
            self.selectedTypeID = self.allList[index].id ?? 0
            
            print(self.selectedTypeID)
            
        }
        
    }
    
    func getAllLists(urlString: String) {
        
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success response",data)
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    
                    let allListsResponseForDropdown = try? JSONDecoder().decode(MailWizardSubcriptionTypeResponse.self, from: response.data!)
                    self.allList = allListsResponseForDropdown ?? []
                    
                    DispatchQueue.main.async {
                        print("allProjects------------\n",self.allList)
                        self.removeSpinner()
                        
                        self.listTitleArray.removeAll()
                        self.listIDArray.removeAll()
                        if self.allList.count > 0{

                            for list in self.allList{
                                let name = list.typeName
                                let id = list.id
                                print(name!, id!)
                                self.listTitleArray.append(name!)
                                self.listIDArray.append(id!)
                            }
                            print(self.listTitleArray)
                            self.setUpDropDownList(list: self.listTitleArray)
                        }
                    }
                }
                    
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                   // AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)

                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
}
