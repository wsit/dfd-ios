//
//  EditTemplateViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 7/24/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit

class EditTemplateViewController: UIViewController {

    @IBOutlet weak var myNavBar: UINavigationBar!
    @IBOutlet weak var lblNameTitle: UILabel!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var lblContentTitle: UILabel!
    @IBOutlet weak var lblSignatureTitle: UILabel!
    @IBOutlet weak var txtSignature: UITextField!
    @IBOutlet weak var txtContent: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        lblNameTitle.textColor = UIColor(red:0.09, green:0.15, blue:0.22, alpha:1)
        txtName.borderColor = UIColor(red:0.87, green:0.93, blue:0.99, alpha:1)
        lblContentTitle.textColor = UIColor(red:0.09, green:0.15, blue:0.22, alpha:1)
        
        lblSignatureTitle.textColor = UIColor(red:0.09, green:0.15, blue:0.22, alpha:1)

        
    }
    
    @IBAction func backClicked(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnDoneClicked(_ sender: Any) {
         self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
