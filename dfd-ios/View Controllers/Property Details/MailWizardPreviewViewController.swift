//
//  MailWizardPreviewViewController.swift
//  HouZes
//
//  Created by Mahadhi Hassan Chowdhury on 1/28/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//

import UIKit

class MailWizardPreviewViewController: UIViewController {

    
    var neighbourID : Int = 0
    var parentString : String = ""
    var mailWizardAmount : Double = 0.0
    var totalAmount : Double = 0.0
    
    var selectedTypeID :Int  = 0
    var propertyID: Int = 0
    
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var previewImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        FontAndColorConfigs.setDynamicButtonsFontSize(buttons: [btnNext], type: TextType.ListTitle.rawValue)
        btnNext.backgroundColor = self.buttonBackgroundColor
        
        let url = globalSelectedTemplate?.url ?? ""
        previewImageView.pin_updateWithProgress = true
        //previewImageView.contentMode = .scaleToFill
        previewImageView.pin_setImage(from: URL(string: url))
        
    }
    
    @IBAction func backClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextButtonClicked(_ sender: Any) {
        
        if self.parentString == "neighbour"{
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            //MailWizardInputTableViewController
            let vc = storyBoard.instantiateViewController(withIdentifier: "MailWizardInputTableViewController") as! MailWizardInputTableViewController
//            let vc = storyBoard.instantiateViewController(withIdentifier: "MailerWizardPopUpViewController") as! MailerWizardPopUpViewController
            //vc.templateData = data
             vc.neighbourID = self.neighbourID
            vc.parentString = "nmw"
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }else{
            //MailWizardPreviewViewController
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//            let vc = storyBoard.instantiateViewController(withIdentifier: "MailerWizardPopUpViewController") as! MailerWizardPopUpViewController
            let vc = storyBoard.instantiateViewController(withIdentifier: "MailWizardInputTableViewController") as! MailWizardInputTableViewController

            //vc.templateData = data
             vc.propertyID = self.propertyID
            globalPropertyID = self.propertyID
            vc.parentString = "mw"
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }
    }
    

}
