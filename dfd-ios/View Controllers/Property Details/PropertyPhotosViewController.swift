//
//  PropertyPhotosViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 6/27/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit
import Pickle
import Photos
import Alamofire
import PINRemoteImage
import Lightbox

class PropertyPhotosViewController: UIViewController, ImagePickerControllerDelegate {
    
    @IBOutlet weak var photoesCollectionView: UICollectionView!
    
    var allPhotos: [PropertyPhoto] = []
    var url : String = ""
    var nextUrl: String = ""
    var previousUrl: String = ""
    var jsonResponse: GetPropertyPhotosResponse?
    
    var SelectedAssets = [PHAsset]()
    var PhotoArray = [UIImage]()
    var DataPhotoArray : [Data] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpNavigationBar()
        photoesCollectionView.register(UINib.init(nibName: "PhotosCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PhotosCollectionViewCell")
        
        photoesCollectionView.backgroundColor = .white
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        if Network.reachability.status == .unreachable{
            AppConfigs.updateUserInterface(view: view)
        }else{
            allPhotos.removeAll()
            let url : String = UrlManager.baseURL() + UrlManager.apiString() + "/photo/property/\((globalSelectedPropertyFromList?.id)!)/?limit=10&page=1"
            
            print(url)
            //self.showSpinner(onView: view)
            getAllPhotos(urlString: url)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.removeSpinner()
    }
    
    func getAllPhotos(urlString: String) {
        //ViewControllerUtils().showActivityIndicator(uiView: self.view)
        //self.showSpinner(onView: self.view)
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success",data)
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    
                    let allPhotosResponse = try? JSONDecoder().decode(GetPropertyPhotosResponse.self, from: response.data!)
                    self.jsonResponse = allPhotosResponse
                    
                    var items: [PropertyPhoto] = self.allPhotos
                    items.append(contentsOf: (allPhotosResponse?.results)!)
                    
                    self.allPhotos.removeAll()
                    self.allPhotos = items
                    ViewControllerUtils().hideActivityIndicator(uiView: self.view)
                    DispatchQueue.main.async {
                        print("allProjects------------\n",self.allPhotos)
                        self.removeSpinner()//self.view.activityStopAnimating()
                        ViewControllerUtils().hideActivityIndicator(uiView: self.view);
                        self.photoesCollectionView.reloadData()
                    }
                }
                    
                else if statusCode == 401{
                    ViewControllerUtils().hideActivityIndicator(uiView: self.view)
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    ViewControllerUtils().hideActivityIndicator(uiView: self.view)
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                self.removeSpinner()
                ViewControllerUtils().hideActivityIndicator(uiView: self.view)
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                
                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addPhotoClicked(_ sender: Any) {
        
        let picker = ImagePickerController()
        picker.delegate = self as? ImagePickerControllerDelegate
        picker.modalPresentationStyle = .fullScreen
        present(picker, animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: ImagePickerController, shouldLaunchCameraWithAuthorization status: AVAuthorizationStatus) -> Bool {
        
        print(status)
        
        return true
    }
    
    func imagePickerController(_ picker: ImagePickerController, didFinishPickingImageAssets assets: [PHAsset]) {
        
        print(assets.count)
        ViewControllerUtils().showActivityIndicator(uiView: picker.view)
        self.SelectedAssets.removeAll()
        self.DataPhotoArray.removeAll()
        self.PhotoArray.removeAll()
        
        for i in 0..<assets.count
        {
            self.SelectedAssets.append(assets[i])
        }
        
        self.convertAssetToImages(pickerView: picker.view)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: ImagePickerController) {
        
        print("cancel")
        self.dismiss(animated: true, completion: nil)
    }
    
    func convertAssetToImages(pickerView: UIView) -> Void {
        
        if SelectedAssets.count != 0{
            
            for i in 0..<SelectedAssets.count{
                
                let manager = PHImageManager.default()
                let option = PHImageRequestOptions()
                var thumbnail = UIImage()
                option.isSynchronous = true
                
                manager.requestImage(for: SelectedAssets[i], targetSize: CGSize(width: 800, height: 600), contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
                    thumbnail = result!
                })
                
                let data = thumbnail.jpegData(compressionQuality: 0.75)
                let newImage = UIImage(data: data!)
                self.DataPhotoArray.append(data!)
                self.PhotoArray.append(newImage! as UIImage)
            }
            
        }
        
        print("complete photo array \(self.PhotoArray)")
        print("complete data photo array \(self.DataPhotoArray.count)")
        
        let urlString = UrlManager.baseURL() + UrlManager.apiString() + "/photo/property/\(String(describing: (globalSelectedPropertyFromList?.id)!))/multiple-upload/"
        print(urlString)
        self.handlePhotoUpload(url: urlString, pickerView: pickerView)
        
    }
    
    
    func handlePhotoUpload(url: String,pickerView: UIView) {
        //self.showSpinner(onView: self.view)
        AppConfigs.getTopMostViewController()?.showSpinner(onView: self.view)
        let headers: HTTPHeaders = [
            "Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken(),
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            //            for (key, value) in parameter {
            //                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            //            }
            print("self.DataPhotoArray.count--->",self.DataPhotoArray.count)
            var count = 1
            for imageDic in self.DataPhotoArray
            {
                
                multipartFormData.append(imageDic, withName: "photo\(count)", fileName: "image\(count).jpg", mimeType: "image/jpg")
                print("count--> ", count)
                count = count + 1
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print("Succesfully uploaded")
                    if let err = response.error{
                        //onError?(err)
                        AppConfigs.showSnacbar(message: err.localizedDescription, textColor: .red)
                        print(response)
                        ViewControllerUtils().hideActivityIndicator(uiView: pickerView)
                        AppConfigs.getTopMostViewController()?.removeSpinner()
                        self.dismiss(animated: true, completion: nil)
                        return
                    }
                    
                    DispatchQueue.main.async {
                        print(response)
                        //self.activityIndicator.stopAnimating()
                        ViewControllerUtils().hideActivityIndicator(uiView: pickerView)
                        AppConfigs.getTopMostViewController()?.removeSpinner()
                        self.allPhotos.removeAll()
                        //let url : String = UrlManager.baseURL() + UrlManager.apiString() + "/photo/property/\(globalSelectedProperty.propertyID)/?limit=10&page=1"
                        //print(url)
                        //self.showSpinner(onView: self.view)
                        //self.getAllPhotos(urlString: url)
                        self.dismiss(animated: true, completion: nil)
                    }
                    
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                //onError?(error)
                // self.activityIndicator.stopAnimating()
                ViewControllerUtils().hideActivityIndicator(uiView: pickerView)
                AppConfigs.getTopMostViewController()?.removeSpinner()
                AppConfigs.showSnacbar(message: error.localizedDescription, textColor: .red)
                self.dismiss(animated: true, completion: nil)
            }
            AppConfigs.getTopMostViewController()?.removeSpinner()
        }
    }
    
}


extension PropertyPhotosViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if allPhotos.count == 0 {
            collectionView.setEmptyMessage("Nothing yet to show")
        } else {
            collectionView.restore()
        }
        return allPhotos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotosCollectionViewCell", for: indexPath) as! PhotosCollectionViewCell
        
        cell.backgroundColor = .white
        cell.cornerRadius = 8
        
        if allPhotos.count > indexPath.row{
            
            let data = allPhotos[indexPath.row]
            let url = data.thumbPhotoUrl ?? ""
            cell.photoImageView.pin_updateWithProgress = true
            cell.photoImageView.pin_setImage(from: URL(string: url)!)
            
            if indexPath.row ==  self.allPhotos.count - 1{
                if self.jsonResponse?.count != self.allPhotos.count{
                    
                    if let nextValue = self.jsonResponse?.next {
                        print(nextValue)
                        self.getAllPhotos(urlString: nextValue)
                    }
                }
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if allPhotos.count > indexPath.row{
            
            let data = allPhotos[indexPath.row]
            let url = data.photoURL ?? ""
            
            let images = [
                LightboxImage(imageURL: URL(string: url)!) 
            ]
            // Create an instance of LightboxController.
            let controller = LightboxController(images: images, startIndex:indexPath.row)
            //LightboxController(images: imagess, startIndex:indexPath.row)
            // Set delegates.
            controller.pageDelegate = self
            controller.dismissalDelegate = self
            
            // Use dynamic background.
            controller.dynamicBackground = true
            controller.modalPresentationStyle = .fullScreen
            // Present your controller.
            present(controller, animated: true, completion: nil)
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width/2.0 - 16
        let height = width
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    
}

extension PropertyPhotosViewController: LightboxControllerPageDelegate, LightboxControllerDismissalDelegate{
    
    func lightboxControllerWillDismiss(_ controller: LightboxController) {
        
    }
    
    
    func lightboxController(_ controller: LightboxController, didMoveToPage page: Int) {
        
        print(page)
    }
    
    
}
