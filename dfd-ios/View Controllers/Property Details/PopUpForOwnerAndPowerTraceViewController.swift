//
//  PopUpForOwnerAndPowerTraceViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 12/20/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
var testing : Bool = false


class PopUpForOwnerAndPowerTraceViewController: UIViewController {
    
    @IBOutlet weak var lblCostTitle: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var ownerInfoImageView: UIImageView!
    @IBOutlet weak var lblOwnerDetailsTitle: UILabel!
    @IBOutlet weak var lblOwnerDetailsAmount: UILabel!
    @IBOutlet weak var lblFirstSeperator: UILabel!
    @IBOutlet weak var btnPowerTraceSelectionImage: UIButton!
    @IBOutlet weak var lblSecondSeperator: UILabel!
    @IBOutlet weak var lblPowerTraceAmount: UILabel!
    @IBOutlet weak var heightForOwnerDetailsView: NSLayoutConstraint!
    @IBOutlet weak var btnOwnerInfo: UIButton!
    @IBOutlet weak var ownerDetailsView: UIView!
    @IBOutlet weak var lblPowerTraceTitle: UILabel!
    @IBOutlet weak var imgPowerTrace: UIImageView!
    @IBOutlet weak var powerTraceView: UIView!
    @IBOutlet weak var lblPTMessage: UILabel!
    
    var isPowerTraceSelected: Int = 1
    var totalAmount : Double = 0.0
    var powerTraceAmount : Double = 0.0
    var ownerDetailsAmount : Double = 0.0
    var mailWizardAmount : Double = 0.0
    
    var parentString :String =  ""
    var propertyID: Int = 0
    var neighbourID: Int = 0
    
    var isReOwnerInfoClicked: Bool = false
    var isRePowerTracedClicked: Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblPTMessage.isHidden = true
        
        lblCostTitle.textColor = self.popUpBluishTitleColor
        lblAmount.textColor = self.popUpBluishTitleColor
        lblOwnerDetailsAmount.textColor = self.popUpBluishTitleColor
        lblPowerTraceAmount.textColor = self.popUpBluishTitleColor
        
        self.totalAmount = AppConfigs.getCurrentUserInfo().upgradeInfo?.powerTrace ?? 0.0
        self.powerTraceAmount = AppConfigs.getCurrentUserInfo().upgradeInfo?.powerTrace ?? 0.0
        self.ownerDetailsAmount = AppConfigs.getCurrentUserInfo().upgradeInfo?.ownerInfo ?? 0.0
        
        self.lblAmount.text =  String(format: "$%.2f", self.totalAmount)
        self.lblPowerTraceAmount.text =  String(format: "$%.2f", self.powerTraceAmount) //"$\(self.powerTraceAmount)"
        self.lblOwnerDetailsAmount.text = String(format: "$%.2f", self.ownerDetailsAmount) //"$\(self.ownerDetailsAmount)"
        
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [lblCostTitle, lblPTMessage], type: TextType.ListSubTitle.rawValue)
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [lblOwnerDetailsTitle, lblOwnerDetailsAmount, lblPowerTraceAmount], type: TextType.ListTitle.rawValue)
        
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [lblAmount], type: TextType.MainTitle.rawValue)
        FontAndColorConfigs.setDynamicButtonsFontSize(buttons: [btnConfirm], type: TextType.ListTitle.rawValue)
        
        lblFirstSeperator.backgroundColor = self.listSeperatorColor
        lblSecondSeperator.backgroundColor = self.listSeperatorColor
        
        btnPowerTraceSelectionImage.setImage(UIImage(named: "ios7-circle-outline"), for: .normal)
        
        if self.parentString == "pt"{
            btnPowerTraceSelectionImage.setImage(UIImage(named: "ios7-checkmark-outline"), for: .normal)
            
            self.totalAmount = powerTraceAmount
            self.lblAmount.text = String(format: "$%.2f", self.totalAmount) //"$\(self.totalAmount)"
            
            btnPowerTraceSelectionImage.isUserInteractionEnabled = false
            self.lblOwnerDetailsTitle.isHidden = true
            self.ownerInfoImageView.isHidden = true
            self.lblOwnerDetailsAmount.isHidden = true
            self.btnOwnerInfo.isHidden = true
            self.heightForOwnerDetailsView.constant = 0
            
            if isRePowerTracedClicked == true{
                self.lblPowerTraceTitle.text = "Re-run Power trace"
            }
            
            lblPTMessage.text = "Lookup the latest mobile phones, landlines, emails, social media accounts"
            lblPTMessage.isHidden = false
            
            self.ownerDetailsView.layoutIfNeeded()
        }
        else if self.parentString == "OIandPT"{
            btnPowerTraceSelectionImage.setImage(UIImage(named: "ios7-checkmark-outline"), for: .normal)
            self.totalAmount = powerTraceAmount + ownerDetailsAmount
            self.lblAmount.text = String(format: "$%.2f", self.totalAmount)
            lblPTMessage.text = "Lookup the latest mobile phones, landlines, emails, social media accounts"
            lblPTMessage.isHidden = false
            
        }
        else if self.parentString == "od"{
            
            self.totalAmount =  ownerDetailsAmount
            self.lblAmount.text = String(format: "$%.2f", self.totalAmount) //String(format: "$%.2f", self.totalAmount) //"$\(self.totalAmount)"
            
            self.lblPowerTraceTitle.isHidden = true
            self.lblPowerTraceAmount.isHidden = true
            self.btnPowerTraceSelectionImage.isHidden = true
            self.imgPowerTrace.isHidden = true
            self.lblSecondSeperator.isHidden = true
            
            if isReOwnerInfoClicked == true{
                self.lblOwnerDetailsTitle.text = "Re-fetch Owner details"
            }
            
            
            self.ownerDetailsView.layoutIfNeeded()
        }
            
        else if self.parentString == "mw" || self.parentString == "nmw"{
            
            print("self.parentString----> ", self.parentString )
            
            
            self.mailWizardAmount = AppConfigs.getCurrentUserInfo().upgradeInfo?.mailWizard ?? 0.0
            
            self.totalAmount =  mailWizardAmount
            self.lblAmount.text = String(format: "$%.2f", self.totalAmount) //"$\(self.totalAmount)"
            self.lblOwnerDetailsAmount.text = String(format: "$%.2f", self.totalAmount) //"$\(self.totalAmount)"
            self.lblOwnerDetailsTitle.text = "Mailer Wizard"
            self.ownerInfoImageView.image = UIImage(named: "new_ic_mailer_wizard")
            
            self.lblPowerTraceTitle.isHidden = true
            self.lblPowerTraceAmount.isHidden = true
            self.btnPowerTraceSelectionImage.isHidden = true
            self.imgPowerTrace.isHidden = true
            self.lblSecondSeperator.isHidden = true
            self.ownerDetailsView.layoutIfNeeded()
        }
        else if self.parentString == "npt"{
            self.lblOwnerDetailsTitle.isHidden = true
            self.ownerInfoImageView.isHidden = true
            self.lblOwnerDetailsAmount.isHidden = true
            self.btnOwnerInfo.isHidden = true
            self.heightForOwnerDetailsView.constant = 0
            self.ownerDetailsView.layoutIfNeeded()
            lblPTMessage.text = "Lookup the latest mobile phones, landlines, emails, social media accounts"
            lblPTMessage.isHidden = false
        }else{
            
        }
    }
    
    
    @IBAction func hideVC(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        //        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func powerTraceSelectionButtonClicked(_ sender: Any) {
        
        if self.btnPowerTraceSelectionImage.currentImage == UIImage(named: "ios7-circle-outline"){
            print("power trace selected")
            btnPowerTraceSelectionImage.setImage(UIImage(named: "ios7-checkmark-outline"), for: .normal)
            
            self.isPowerTraceSelected = 1
            
            self.totalAmount = powerTraceAmount + ownerDetailsAmount
            self.lblAmount.text = String(format: "$%.2f", self.totalAmount) //"$\(self.totalAmount)"
        }
        else if self.btnPowerTraceSelectionImage.currentImage == UIImage(named: "ios7-checkmark-outline"){
            print("power trace not selected")
            btnPowerTraceSelectionImage.setImage(UIImage(named: "ios7-circle-outline"), for: .normal)
            self.isPowerTraceSelected = 0
            
            self.totalAmount =  ownerDetailsAmount
            self.lblAmount.text = String(format: "$%.2f", self.totalAmount)//"$\(self.totalAmount)"
        }
        else{
            print("unknown case")
        }
    }
    
    
    @IBAction func confirmButtonClicked(_ sender: Any) {
        testing = false
        if testing == true{
            //let storyboard = UIStoryboard(name: "Settings", bundle: nil)
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "RechargeViewController") as! RechargeViewController
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
            //            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            
            let url = UrlManager.baseURL() + UrlManager.apiString() + "/property/\(( globalSelectedPropertyFromList?.id)!)/payment/"
            print(url)
            
            var parameters : Parameters = [:]
            
            if self.parentString == "npt"{
                parameters  = [
                    "fetch_owner_info" : 1,
                    "power_trace" : 1,
                    "neighbor_id" : [286]
                ]
                
                handleOwnerInfoPowerTraceOperation(urlString: url, parameter: parameters)
            }
            else if self.parentString == "pt"{
                
                parameters = [
                    "fetch_owner_info" :0,
                    "power_trace" : 1
                ]
                print(parameters)
                
                handleOwnerInfoPowerTraceOperation(urlString: url, parameter: parameters)
            }
            else if self.parentString == "od"{
                
                parameters = [
                    "fetch_owner_info" : 1,
                    "power_trace" : 0
                ]
                print(parameters)
                
                handleOwnerInfoPowerTraceOperation(urlString: url, parameter: parameters)
            }
            else if self.parentString == "mw"{
                self.dismiss(animated: true, completion: nil)
                NotificationCenter.default.post(name:NSNotification.Name("MailWizardSelected"),object: nil)
                
            }
            else if self.parentString == "nmw" {
                self.dismiss(animated: true, completion: nil)
                NotificationCenter.default.post(name:NSNotification.Name("NeighbourMailWizardSelected"),object: nil)
            }
            else{
                
                if globalSelectedPropertyFromList?.id != nil{
                    
                    parameters = [
                        
                        "fetch_owner_info" : 1,
                        "power_trace" : isPowerTraceSelected
                    ]
                    print(parameters)
                    
                    handleOwnerInfoPowerTraceOperation(urlString: url, parameter: parameters)
                }else{
                    print("No Property Selected")
                }
            }
            //            }
            
        }
    }
    
    
    func handleOwnerInfoPowerTraceOperation(urlString: String,parameter:Parameters ) {
        self.showSpinner(onView: self.btnConfirm)
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        
        Alamofire.request(urlString, method: .post, parameters: parameter as Parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
                
            case.success(let data):
                
                self.removeSpinner()
                print("success",data)
                let statusCode = response.response?.statusCode
                print(statusCode!)
                
                if statusCode == 200{
                    self.removeSpinner()
                    let ownerInfoAndPowerTraceResponse = try? JSONDecoder().decode(OwnerInfoAndPowerTraceResponse.self, from: response.data!)
                    
                    if ownerInfoAndPowerTraceResponse != nil{
                        
                        let messageArray = ownerInfoAndPowerTraceResponse?.message ?? ""
                        let json = JSON(parseJSON: messageArray).arrayValue
 
                        var firstIndex : String = ""
                        var secIndex : String = ""
                        
                        if json.count == 1{
                            firstIndex = json[0].stringValue
                            print(firstIndex)
                        }else if json.count == 2{
                            firstIndex = json[0].stringValue
                            secIndex = json[1].stringValue
                        }else{
                            
                        }
                        
                        let totalMessage = firstIndex + "\n" + secIndex
                        print(totalMessage)
                        
                        if ownerInfoAndPowerTraceResponse?.status == false{
                            
                            AppConfigs.showSnacbar(message: totalMessage, textColor: .red)
                            
                            let data = ownerInfoAndPowerTraceResponse?.data
                            if data != nil{
                                let payment = data?.payment
                                if payment == false{
//                                    if AppConfigs.getCurrentUserInfo().isAdmin == false{
//                                        AppConfigs.showSnacbar(message: totalMessage, textColor: .red)
//                                        self.showAlert(message: totalMessage)
//                                    }else{
//                                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RechargeViewController") as! RechargeViewController
//                                        vc.modalPresentationStyle = .fullScreen
//                                        self.present(vc, animated: true, completion: nil)
//                                    }
                                    if AppConfigs.getCurrentUserInfo().isAdmin == false{
                                       AppConfigs.showSnacbar(message: totalMessage, textColor: .blue)
                                   }else{
                                        
                                        let alertController = UIAlertController(title: totalMessage, message: "", preferredStyle: .alert)
                                        let OKAction = UIAlertAction(title: "Ok", style: .default) { (alert) in
                                            
                                            if AppConfigs.getCurrentUserInfo().thinker == true{
                                                
                                                let main = UIStoryboard(name: "Settings", bundle: nil)
                                                let vc = main.instantiateViewController(withIdentifier: "RechargeForIAPViewController") as! RechargeForIAPViewController
                                                //self.navigationController?.pushViewController(vc, animated: true)
                                                vc.modalPresentationStyle = .fullScreen
                                                self.present(vc, animated: true, completion: nil)
                                            }else{
                                                
                                                let main = UIStoryboard(name: "Main", bundle: nil)
                                                let vc = main.instantiateViewController(withIdentifier: "RechargeViewController") as! RechargeViewController
                                                vc.modalPresentationStyle = .fullScreen
                                                self.present(vc, animated: true, completion: nil)
                                            }

//                                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "RechargeViewController") as! RechargeViewController
//                                            vc.modalPresentationStyle = .fullScreen
//                                            self.present(vc, animated: true, completion: nil)
                                        }
                                        alertController.addAction(OKAction)
                                        self.present(alertController, animated: true, completion: nil)
                                        
//                                        let alertController = UIAlertController(title: totalMessage, message: "Please go to web to manage your account.", preferredStyle: .alert)
//                                        let OKAction = UIAlertAction(title: "Ok", style: .default) { (alert) in
//
//
//                                        }
//                                        alertController.addAction(OKAction)
//                                        self.present(alertController, animated: true, completion: nil)
                                        
                                   }

                                }
                            }
                        }else{
                            self.removeSpinner()
                            AppConfigs.showSnacbar(message: totalMessage, textColor: .green)
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PowerTracedNotif"), object: nil, userInfo: nil)
                            
                            self.dismiss(animated: true, completion: nil)
                            _ = self.navigationController?.popViewController(animated: true)
                           
                            if self.parentString == "npt"{
                                
                            }
                            else if self.parentString == "pt"{
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SinglePowerTracedNotif"), object: nil, userInfo: nil)
                            }
                            else if self.parentString == "od"{
                                 NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SingleOwnerInfoNotif"), object: nil, userInfo: nil)
                            }
                            else if self.parentString == "mw"{
                                 
                                
                            }
                            else if self.parentString == "nmw" {
                                 
                            }
                            else{
                                
                            }
                        }
                    }else{
                        do{
                            let json1 = try JSON(data: response.data!)
                            //                                let status = json["status"].stringValue
                            let message = json1["message"].stringValue
                            let status = json1["status"].boolValue
                            
                            let json = JSON(parseJSON: message).arrayValue
                            var firstIndex : String = ""
                            var secIndex : String = ""
                            
                            if json.count == 1{
                                firstIndex = json[0].stringValue
                                print(firstIndex)
                            }else if json.count == 2{
                                firstIndex = json[0].stringValue
                                secIndex = json[1].stringValue
                                print(firstIndex)

                            }else{
                                
                            }
                            
                            let totalMessage = firstIndex + "\n" + secIndex
                            print(totalMessage)
                            
                            AppConfigs.showSnacbar(message: totalMessage, textColor: .green)
                            
                            if status == true{
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PowerTracedNotif"), object: nil, userInfo: nil)
                                self.dismiss(animated: true, completion: nil)
                                
                                if self.parentString == "npt"{
                                    
                                }
                                else if self.parentString == "pt"{
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SinglePowerTracedNotif"), object: nil, userInfo: nil)
                                }
                                else if self.parentString == "od"{
                                     NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SingleOwnerInfoNotif"), object: nil, userInfo: nil)
                                }
                                else if self.parentString == "mw"{
                                     
                                    
                                }
                                else if self.parentString == "nmw" {
                                     
                                }
                                else{
                                    
                                }
                                
                            }else{
                                
                                let jsonData = json1["data"]
                                let payment = jsonData["payment"].boolValue
                                if payment == false{
                                    
                                    if AppConfigs.getCurrentUserInfo().isAdmin == false{
                                           AppConfigs.showSnacbar(message: totalMessage, textColor: .blue)
                                       }else{
                                          
//                                        let alertController = UIAlertController(title: totalMessage, message: "Please go to web to manage your account.", preferredStyle: .alert)
//                                        let OKAction = UIAlertAction(title: "Ok", style: .default) { (alert) in
//
//
//                                        }
//                                        alertController.addAction(OKAction)
//                                        self.present(alertController, animated: true, completion: nil)
                                        
                                           let alertController = UIAlertController(title: totalMessage, message: "", preferredStyle: .alert)
                                           let OKAction = UIAlertAction(title: "Ok", style: .default) { (alert) in
                                           
                                            if AppConfigs.getCurrentUserInfo().thinker == true{
                                                
                                                let main = UIStoryboard(name: "Settings", bundle: nil)
                                                let vc = main.instantiateViewController(withIdentifier: "RechargeForIAPViewController") as! RechargeForIAPViewController
                                                //self.navigationController?.pushViewController(vc, animated: true)
                                                vc.modalPresentationStyle = .fullScreen
                                                self.present(vc, animated: true, completion: nil)
                                            }else{
                                                
                                                let main = UIStoryboard(name: "Main", bundle: nil)
                                                let vc = main.instantiateViewController(withIdentifier: "RechargeViewController") as! RechargeViewController
                                                //self.navigationController?.pushViewController(vc, animated: true)
                                                vc.modalPresentationStyle = .fullScreen
                                                self.present(vc, animated: true, completion: nil)
                                            }

//                                               let vc = self.storyboard?.instantiateViewController(withIdentifier: "RechargeViewController") as! RechargeViewController
//                                               vc.modalPresentationStyle = .fullScreen
//                                               self.present(vc, animated: true, completion: nil)
                                           }
                                           alertController.addAction(OKAction)
                                           self.present(alertController, animated: true, completion: nil)

                                           
                                       }
                                }else{
                                    AppConfigs.showSnacbar(message: totalMessage, textColor: .green)
                                }
                            }
                            
                        }catch let error{
                            AppConfigs.showSnacbar(message: error.localizedDescription, textColor: .red)
                        }
                        
                    }
                    self.removeSpinner()
                    
                }else{
                    self.showAlert(message: " ", title: "Error Occured")
                    self.removeSpinner()
                }
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)

                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                 self.removeSpinner()
            }
        }
    }
    
}
