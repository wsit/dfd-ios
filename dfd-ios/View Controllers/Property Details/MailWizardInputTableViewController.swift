//
//  MailWizardInputTableViewController.swift
//  HouZes
//
//  Created by Mahadhi Hassan Chowdhury on 2/2/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//

import UIKit
import DropDown
import Alamofire
import SwiftyJSON
import SwiftValidator



var global_first_name =   ""
var global_last_name =  ""
var global_email =  ""
var global_phoneNo = ""
var global_address_street =  ""
var global_address_city = ""
var global_address_state = ""
var global_address_zip = 0
var global_agent_license = ""
var global_website = ""
var global_companyName = ""


class MailWizardInputTableViewController: UITableViewController , UITextFieldDelegate, ValidationDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        
        if picker == logoImagePicker{
            let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
            self.logoImageView.image = image
        }
        
        if picker == coverImagePicker{
            let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
            self.coverImageView.image = image
        }
        
        
    }
    
    
    func validationSuccessful() {
        
        
        
        if txtFirstName.isEmpty == true || txtLastName.isEmpty == true || txtEmail.isEmpty == true || txtMobileNumber.isEmpty == true || txtStreet.isEmpty == true || txtCity.isEmpty || txtZip.isEmpty || txtNoOfMailOccurances.isEmpty
        {
            AppConfigs.showSnacbar(message: "Fill All required fields", textColor: .red)
        }else{
            
            let count = Int(txtNoOfMailOccurances.text!) ?? 0
            if count > 0{
                
                if Network.reachability.status == .unreachable{
                    AppConfigs.updateUserInterface(view: view)
                }else{
                    if parentString == "nmw"{
                        
                        let url = UrlManager.baseURL() + UrlManager.apiString() + "/mail-wizard/neighbor/\(self.neighbourID)/"
                        print(url)
                        self.handleSendMail(urlString: url)
                    }else{
                        
                        let url = UrlManager.baseURL() + UrlManager.apiString() + "/mail-wizard/property/\(self.propertyID)/"
                        print(url)
                        self.handleSendMail(urlString: url)
                    }
                }
            }else{
                AppConfigs.showSnacbar(message: "Number of occurance value must greater than 0", textColor: .red)
            }
            
        }
    }
    
    func validationFailed(_ errors:[(Validatable ,ValidationError)]) {
        // turn the fields to red
        for (field, error) in errors {
            if let field = field as? UITextField {
                field.layer.borderColor = UIColor.red.cgColor
                field.layer.borderWidth = 1.0
            }
            error.errorLabel?.text = error.errorMessage // works if you added labels
            error.errorLabel?.isHidden = false
        }
        
    }
    
    func validationFailed(errors: [UITextField : ValidationError]) {
        print("Validation FAILED!")
    }
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("While entering the characters this method gets called")
        
        let nmbr = Double(string) ?? 1
        let amount = nmbr * mailWizardAmount
        self.lblCost.text = String(format: " Total Cost: $%.2f", self.totalAmount)
        self.totalAmount = amount
        print(totalAmount)
        
        return true;
    }
    
    let dropDown = DropDown()
    let stateDropdown = DropDown()
    let validator = Validator()
    
    @IBOutlet weak var btnDropDown: UIButton!
    @IBOutlet weak var btnImageDropdown: UIButton!
    @IBOutlet weak var txtNoOfMailOccurances: UITextField!
    @IBOutlet weak var lblCost: UILabel!
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var lblStateValue: UILabel!
    
    @IBOutlet weak var lblFrequency: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmailTitle: UILabel!
    @IBOutlet weak var lblStreetTitle: UILabel!
    @IBOutlet weak var lblStateTitle: UILabel!
    @IBOutlet weak var lblCityTitle: UILabel!
    @IBOutlet weak var lblZipTitle: UILabel!
    @IBOutlet weak var lblAgentLicense: UILabel!
    @IBOutlet weak var lblLogoTitle: UILabel!
    @IBOutlet weak var lblCoverTitle: UILabel!
    @IBOutlet weak var lblWebsite: UILabel!
    @IBOutlet weak var lblCompanyName: UILabel!
    
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtMobileNumber: UITextField!
    @IBOutlet weak var txtStreet: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtZip: UITextField!
    @IBOutlet weak var txtAgentLicense: UITextField!
    @IBOutlet weak var txtWebsite: UITextField!
    @IBOutlet weak var txtCompanyName: UITextField!
    
    
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var btnLogoImage: UIButton!
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var btnCoverImage: UIButton!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    

    
    var inputResponse: InputValuesResponse?
    var inputData : InputData?
    var allList : [MailWizardSubcriptionTypeResponseElement] = []
    var listTitleArray : [String] = []
    var listIDArray : [Int] = []
    var states : [String] = ["AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "DC", "FL", "GA", "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"]
    var neighbourID : Int = 0
    var parentString : String = ""
    var mailWizardAmount : Double = 0.0
    var totalAmount : Double = 0.0
    
    var selectedTypeID :Int  = 0
    var propertyID: Int = 0
    
    let logoImagePicker = UIImagePickerController()
    let coverImagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUIValidation()
        logoImagePicker.delegate = self
        coverImagePicker.delegate = self
        
        let url : String = UrlManager.baseURL() + UrlManager.apiString() + "/mail-wizard-user-info/get/"
        
        print(url)
        self.getAllInputData(urlString: url)
        
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [lblName, lblEmailTitle, lblStreetTitle, lblStateTitle, lblCityTitle, lblZipTitle, lblAgentLicense, lblLogoTitle, lblCoverTitle, lblWebsite, lblCompanyName], type: TextType.ListSubTitle.rawValue)
        
        txtNoOfMailOccurances.delegate = self
        txtNoOfMailOccurances.addTarget(self, action: #selector(MailWizardInputTableViewController.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        self.mailWizardAmount = AppConfigs.getCurrentUserInfo().upgradeInfo?.mailWizard ?? 0.0
        self.lblCost.text = String(format: " Total Cost: $%.2f", self.mailWizardAmount * 8)
        self.lblCost.layer.masksToBounds = true
        self.lblStateValue.layer.masksToBounds = true
        
        let tappy = MyTapGesture(target: self, action: #selector(self.labelTapped(_:)))
        lblStateValue.isUserInteractionEnabled = true
        lblStateValue.addGestureRecognizer(tappy)
        tappy.title = lblStateValue.text!
        
        setUpStateDropdown()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if Network.reachability.status == .unreachable{
            AppConfigs.updateUserInterface(view: view)
        }else{
            
            allList.removeAll()
            let url : String = UrlManager.baseURL() + UrlManager.apiString() + "/mail-wizard-subscriptions/"
            
            print(url)
            //self.showSpinner(onView: btnDropDown)
            getAllLists(urlString: url)
            
        }
        
    }
    
    func configureUIValidation() {
        
        validator.registerField(txtNoOfMailOccurances,errorLabel: nil , rules: [RequiredRule()])
        validator.registerField(txtFirstName,errorLabel: nil , rules: [RequiredRule()])
        validator.registerField(txtLastName, errorLabel: nil ,rules: [RequiredRule()])
        validator.registerField(txtEmail, errorLabel: nil ,rules: [RequiredRule(), EmailRule()])
        validator.registerField(txtStreet,errorLabel: nil , rules: [RequiredRule()])
        validator.registerField(txtCity, errorLabel: nil ,rules: [RequiredRule()])
        validator.registerField(txtZip, errorLabel: nil ,rules: [RequiredRule()])
        validator.registerField(txtMobileNumber, errorLabel: nil ,rules: [RequiredRule(), MaxLengthRule(length: 16)])
        
        validator.styleTransformers(success:{ (validationRule) -> Void in
            print("here")
            // clear error label
            validationRule.errorLabel?.isHidden = true
            validationRule.errorLabel?.text = ""
            
            if let textField = validationRule.field as? UITextField {
                // textField.layer.borderColor = UIColor.gray.cgColor
                // textField.layer.borderWidth = 1
                textField.cornerRadius = 4
                //textField.layer.borderWidth = 0.5
            } else if let textField = validationRule.field as? UITextView {
                //                textField.layer.borderColor = UIColor.green.cgColor
                //                textField.layer.borderWidth = 0.5
            }
        }, error:{ (validationError) -> Void in
            print("error")
            validationError.errorLabel?.isHidden = false
            validationError.errorLabel?.text = validationError.errorMessage
            if let textField = validationError.field as? UITextField {
                textField.layer.borderColor = UIColor.red.cgColor
                textField.layer.borderWidth = 1.0
            } else if let textField = validationError.field as? UITextView {
                textField.layer.borderColor = UIColor.red.cgColor
                textField.layer.borderWidth = 1.0
            }
        })
    }
    
    
    @objc func labelTapped(_ sender: MyTapGesture ) {
        print("labelTapped")
        print(sender.title)
        stateDropdown.show()
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        let string = textField.text ?? "1"
        let nmbr = Double(string) ?? 1
        let amount = nmbr * mailWizardAmount
        
        self.totalAmount = amount
        print(totalAmount)
        self.lblCost.text = String(format: " Total Cost: $%.2f", self.totalAmount)
    }
    
    @IBAction func dropDownImageClicked(_ sender: Any) {
        dropDown.show()
    }
    
    @IBAction func dropdownButtonClicked(_ sender: Any) {
        
        dropDown.show()
    }
    
    @IBAction func stateDropdownClicked(_ sender: Any) {
        stateDropdown.show()
    }
    
    @IBAction func minimiseButtonClicked(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func coverButtonClicked(_ sender: Any) {
        
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            btnCoverImage.setImage(nil, for: .normal)
            coverImagePicker.sourceType = .photoLibrary
            coverImagePicker.allowsEditing = false
            coverImagePicker.modalPresentationStyle = .fullScreen
            present(coverImagePicker, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func logoButtonClicked(_ sender: Any) {
        
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            btnLogoImage.setImage(nil, for: .normal)
            logoImagePicker.sourceType = .photoLibrary
            logoImagePicker.allowsEditing = false
            logoImagePicker.modalPresentationStyle = .fullScreen
            present(logoImagePicker, animated: true, completion: nil)
        }
        
    }
    
    func setUpStateDropdown(){
        self.lblStateValue.text = self.states[0]
        stateDropdown.anchorView = self.lblStateValue
        stateDropdown.direction = .any
        stateDropdown.bottomOffset = CGPoint(x: 0, y: self.lblStateValue.bounds.height)
        stateDropdown.dataSource = states
        stateDropdown.width = self.lblStateValue.frame.width
        stateDropdown.textFont = FontAndColorConfigs.getProximaRegular16()
        stateDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.lblStateValue.text = item
            print(self.lblStateValue.text)
            
        }
        
    }
    
    func setUpDropDownList(list: Array<String>) {
        self.btnDropDown.setTitle(self.allList[0].typeName, for: .normal)
        self.selectedTypeID = self.allList[0].id ?? 0
        
        dropDown.anchorView = btnDropDown
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y: btnDropDown.bounds.height)
        dropDown.dataSource = list
        dropDown.width = self.btnDropDown.frame.width
        dropDown.textFont = FontAndColorConfigs.getProximaRegular16()
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.btnDropDown.setTitle(item, for: .normal)
            self.selectedTypeID = self.allList[index].id ?? 0
            
            print(self.selectedTypeID)
            
        }
        
    }
    
    
    @IBAction func btnConfirmClicked(_ sender: Any) {
        
        validator.validate(self)
        
    }
    
    func clearFields() {
        global_first_name =   ""
        global_last_name =  ""
        global_email =  ""
        global_phoneNo = ""
        global_address_street =  ""
        global_address_city = ""
        global_address_state = ""
        global_address_zip = 0
        global_agent_license = ""
        global_website = ""
        global_companyName = ""
        
    }
    
    func handleSendMail(urlString: String) {
        //self.showSpinner(onView: btnConfirm)
        print(urlString)
        //let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
//        self.displayActivityIndicator(shouldDisplay: true)
//        self.showSpinner(onView: self.contentView)
        //let parameters: [String:Any] = [:]
        let headers: HTTPHeaders = [
            "Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken(),
            "Content-type": "multipart/form-data"
        ]
        
        let count = Int(txtNoOfMailOccurances.text!) ?? 1
        
        let fN = txtFirstName.text
        let lN = txtLastName.text
        let email = txtEmail.text
        let phoneNo = txtMobileNumber.text
        let street = txtStreet.text
        let state = lblStateValue.text
        let city = txtCity.text
        var zip = 0
        if  let t = Int(txtZip.text!){
            zip =  t
        }
        
        print(zip )
        let agentLicenseNo = txtAgentLicense.text ?? ""
        print(agentLicenseNo)
        let website = txtWebsite.text ?? ""
        print(website)
        let companyName = txtCompanyName.text ?? ""
        print(companyName)
        
        let userInfo: MailWizardInputField = MailWizardInputField() //(first_name: fN ?? "", last_name: lN ?? "", email: email ?? "", address_street: street ?? "", address_city: city ?? "", address_state: state ?? "", address_zip: zip )
        
        userInfo.first_name = fN ?? ""
        userInfo.last_name = lN ?? ""
        userInfo.email = email ?? ""
        userInfo.phone_no = phoneNo ?? ""
        userInfo.address_street = street ?? ""
        userInfo.address_city = city ?? ""
        userInfo.address_state = state ?? ""
        userInfo.address_zip = zip
        userInfo.agent_license_number = agentLicenseNo
        userInfo.website = website
        userInfo.company_name = companyName
        print(userInfo)
        
        //        let jsonserializer = JSONSerializer.toJson(userInfo)// JSONSerializer.toJson(userInfo)
        //        let json =  JSON(parseJSON: jsonserializer)
        //        print(json)
        // print("jsonserializer", jsonserializer)
        
        let parameters = [
            //               "mail_text" : txtView.text!,
            "tem_item_id" : Int((globalSelectedTemplate?.itemID)!)!,
            "subs_id" :  self.selectedTypeID,
            "mail_count" : count,
            //            "user_info" : [
            //                "first_name" : fN ?? "",
            //                "last_name" : lN ?? "",
            //                "email" : email ?? "",
            //                "phone_no": phoneNo ?? "",
            //                "address_street" : street ?? "",
            //                "address_city" : city ?? "",
            //                "address_state" : state ?? "",
            //                "address_zip" : zip
            //            ]
            "first_name" : fN ?? "",
            "last_name" : lN ?? "",
            "email" : email ?? "",
            "phone_no": phoneNo ?? "",
            "address_street" : street ?? "",
            "address_city" : city ?? "",
            "address_state" : state ?? "",
            "address_zip" : zip,
            "offer_code" : agentLicenseNo,
            "website": website,
            "company_name" : companyName
            ] as [String : Any]
        print(parameters)
        
        
        global_first_name =  fN ?? ""
        global_last_name =  lN ?? ""
        global_email =  email ?? ""
        global_phoneNo = phoneNo ?? ""
        global_address_street =  street ?? ""
        global_address_city = city ?? ""
        global_address_state = state ?? ""
        global_address_zip = zip
        global_agent_license = agentLicenseNo
        global_website = website
        global_companyName = companyName
        
        let logoImageData = logoImageView.image?.jpegData(compressionQuality: 0.70)
        let coverImageData = coverImageView.image?.jpegData(compressionQuality: 0.70)
        
        self.activityIndicator.startAnimating()
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            if (logoImageData != nil){
                multipartFormData.append(logoImageData!, withName: "logo", fileName: "image.jpg", mimeType: "image/jpg")
            }
            
            if (coverImageData != nil){
                multipartFormData.append(coverImageData!, withName: "property_image", fileName: "image.jpg", mimeType: "image/jpg")
            }
            
        }, usingThreshold: UInt64.init(), to:urlString, method: .post, headers: headers) { (result) in
            switch result{
                
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    self.removeSpinner()
                    self.displayActivityIndicator(shouldDisplay: false)
                    self.activityIndicator.stopAnimating()
                    let statusCode = response.response?.statusCode
                    print(statusCode!)
                    if let err = response.error{
                        //onError?(err)
                        self.removeSpinner()
                         AppConfigs.showSnacbar(message: err.localizedDescription, textColor: .red)
                        return
                    }
                    DispatchQueue.main.async {
                        //self.activityIndicator.stopAnimating()
                        let statusCode = response.response?.statusCode
                        print(statusCode!)
                        do{
                            let data = try JSON(data: response.data!)
                            let status = data["status"].boolValue
                            let message = data["message"].stringValue
                            print(message)
                            if statusCode == 200{
                                
                                DispatchQueue.main.async {
                                    self.removeSpinner()
                                    
                                    if status == true{
                                        
                                        self.clearFields()
                                        AppConfigs.showSnacbar(message: message, textColor: .green)
                                        self.dismiss(animated: true, completion: nil)//self.navigationController?.popViewController(animated: true)
                                        
                                    }else{
                                        
                                        //AppConfigs.showSnacbar(message: message, textColor: .green)
                                        let jsonData = data["data"]
                                        let payment = jsonData["payment"].boolValue
                                        
                                        if payment == false{
                                            if AppConfigs.getCurrentUserInfo().isAdmin == false{
                                                AppConfigs.showSnacbar(message: message, textColor: .green)
                                            }else{
                                                
                                                let alertController = UIAlertController(title: message, message: "", preferredStyle: .alert)
                                                let OKAction = UIAlertAction(title: "Ok", style: .default) { (alert) in
                                                    
                                                    if AppConfigs.getCurrentUserInfo().thinker == true{
                                                        
                                                        let main = UIStoryboard(name: "Settings", bundle: nil)
                                                        let vc = main.instantiateViewController(withIdentifier: "RechargeForIAPViewController") as! RechargeForIAPViewController
                                                        //self.navigationController?.pushViewController(vc, animated: true)
                                                        vc.modalPresentationStyle = .fullScreen
                                                        self.present(vc, animated: true, completion: nil)
                                                    }else{
                                                        
                                                        let main = UIStoryboard(name: "Main", bundle: nil)
                                                        let vc = main.instantiateViewController(withIdentifier: "RechargeViewController") as! RechargeViewController
                                                        //self.navigationController?.pushViewController(vc, animated: true)
                                                        vc.modalPresentationStyle = .fullScreen
                                                        self.present(vc, animated: true, completion: nil)
                                                    }
                                                }
                                                alertController.addAction(OKAction)
                                                self.present(alertController, animated: true, completion: nil)
                                                
                                            }
                                        }else{
                                            AppConfigs.showSnacbar(message: message, textColor: .green)
                                        }
                                    }
                                }
                                
                            }else if statusCode == 401{
                                self.removeSpinner()
                                let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                                print("error with response status: \(String(describing: statusCode))")
                                //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                                AppConfigs.showSnacbar(message: message, textColor: .green)
                            }
                                
                            else{
                                self.removeSpinner()
                                print("error with response status: \(String(describing: statusCode))")
                                AppConfigs.showSnacbar(message: message, textColor: .green)
                                //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                            }
                        }catch let error{
                            AppConfigs.showSnacbar(message: error.localizedDescription, textColor: .green)
                        }
                    }
                }
            case .failure(let error):
                self.activityIndicator.stopAnimating()
                
                print("Error in upload: \(error.localizedDescription)")
                //onError?(error)
                //self.activityIndicator.stopAnimating()
                self.displayActivityIndicator(shouldDisplay: false)

                self.removeSpinner()
                AppConfigs.showSnacbar(message: error.localizedDescription, textColor: .red)
            }
            self.removeSpinner()
        }
        
        //        Alamofire.request(urlString, method: .post, parameters: parameters as Parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
        //
        //            switch(response.result) {
        //            case.success(let data):
        //                print("success",data)
        //
        //                self.removeSpinner()
        //                let statusCode = response.response?.statusCode
        //                print(statusCode!)
        //                do{
        //                    let data = try JSON(data: response.data!)
        //                    let status = data["status"].boolValue
        //                    let message = data["message"].stringValue
        //
        //                    if statusCode == 200{
        //
        //                        DispatchQueue.main.async {
        //                            self.removeSpinner()
        //
        //                            if status == true{
        //
        //                                self.clearFields()
        //                                AppConfigs.showSnacbar(message: message, textColor: .green)
        //                                self.dismiss(animated: true, completion: nil)//self.navigationController?.popViewController(animated: true)
        //
        //                            }else{
        //
        //                                //AppConfigs.showSnacbar(message: message, textColor: .green)
        //                                let jsonData = data["data"]
        //                                let payment = jsonData["payment"].boolValue
        //
        //                                if payment == false{
        //                                    if AppConfigs.getCurrentUserInfo().isAdmin == false{
        //                                        AppConfigs.showSnacbar(message: message, textColor: .green)
        //                                    }else{
        //
        //                                        let alertController = UIAlertController(title: message, message: "", preferredStyle: .alert)
        //                                        let OKAction = UIAlertAction(title: "Ok", style: .default) { (alert) in
        //
        //                                            if AppConfigs.getCurrentUserInfo().thinker == true{
        //
        //                                                let main = UIStoryboard(name: "Settings", bundle: nil)
        //                                                let vc = main.instantiateViewController(withIdentifier: "RechargeForIAPViewController") as! RechargeForIAPViewController
        //                                                //self.navigationController?.pushViewController(vc, animated: true)
        //                                                vc.modalPresentationStyle = .fullScreen
        //                                                self.present(vc, animated: true, completion: nil)
        //                                            }else{
        //
        //                                                let main = UIStoryboard(name: "Main", bundle: nil)
        //                                                let vc = main.instantiateViewController(withIdentifier: "RechargeViewController") as! RechargeViewController
        //                                                //self.navigationController?.pushViewController(vc, animated: true)
        //                                                vc.modalPresentationStyle = .fullScreen
        //                                                self.present(vc, animated: true, completion: nil)
        //                                            }
        //                                        }
        //                                        alertController.addAction(OKAction)
        //                                        self.present(alertController, animated: true, completion: nil)
        //
        //                                    }
        //                                }else{
        //                                    AppConfigs.showSnacbar(message: message, textColor: .green)
        //                                }
        //                            }
        //                        }
        //
        //                    }else if statusCode == 401{
        //                        self.removeSpinner()
        //                        let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
        //                        print("error with response status: \(String(describing: statusCode))")
        //                        //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
        //                        AppConfigs.showSnacbar(message: message, textColor: .green)
        //                    }
        //
        //                    else{
        //                        self.removeSpinner()
        //                        print("error with response status: \(String(describing: statusCode))")
        //                        AppConfigs.showSnacbar(message: message, textColor: .green)
        //                        //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
        //                    }
        //                }catch let error{
        //                    AppConfigs.showSnacbar(message: error.localizedDescription, textColor: .green)
        //                }
        //
        //            case.failure(let error):
        //                print("Not Success",error)
        //                let statusCode = response.response?.statusCode
        //                self.showMessageForServerError(code: statusCode ?? 500)
        //
        //                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
        //                self.removeSpinner()
        //            }
        //            self.removeSpinner()
        //        }
    }
     
    
    func getAllInputData(urlString: String) {
        
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success response",data)
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    
                    let inputValuesResponse = try? JSONDecoder().decode(InputValuesResponse.self, from: response.data!)
                    print(inputValuesResponse?.data)
                    self.inputResponse = inputValuesResponse
                    self.inputData = inputValuesResponse?.data
                    
                    DispatchQueue.main.async {
                        print("inputValuesResponse------------\n", self.inputData)
                        self.removeSpinner()
                        
                        self.txtFirstName.text = self.inputData?.firstName
                        self.txtLastName.text = self.inputData?.lastName
                        self.txtEmail.text = self.inputData?.email
                        self.txtMobileNumber.text = self.inputData?.phoneNo
                        self.txtWebsite.text = self.inputData?.website
                        self.txtStreet.text = self.inputData?.addressStreet
                        self.lblStateValue.text = self.inputData?.addressState
                        self.txtCity.text = self.inputData?.addressCity
                        self.txtZip.text = self.inputData?.addressZip
                        self.txtAgentLicense.text = self.inputData?.agentLicense
                        self.txtCompanyName.text = self.inputData?.companyName
                        
                        
                        if self.inputData?.logo != nil{
                            if self.inputData?.logo != ""{
                                self.showPicture(url: (self.inputData?.logo!)!, imageView: self.logoImageView)

                                self.btnLogoImage.setImage(nil, for: .normal)
                            }else{
                                print("photo is empty")
                            }
                        }else
                        {
                            print("photo is nil")
                        }
                        
                        if self.inputData?.coverPhoto != nil{
                            if self.inputData?.coverPhoto != ""{
                                self.showPicture(url: (self.inputData?.coverPhoto!)!, imageView: self.coverImageView)

                                self.btnCoverImage.setImage(nil, for: .normal)

                            }else{
                                print("photo is empty")
                            }
                        }else
                        {
                            print("photo is nil")
                        }
                        
                        if inputValuesResponse == nil{
                            if global_address_zip > 0{
                                self.txtFirstName.text = global_first_name
                                self.txtLastName.text = global_last_name
                                self.txtEmail.text = global_email
                                self.txtMobileNumber.text = global_phoneNo
                                self.txtStreet.text = global_address_street
                                self.lblStateValue.text = global_address_state
                                self.txtCity.text = global_address_city
                                self.txtZip.text = "\(global_address_zip)"
                                self.txtAgentLicense.text = global_agent_license
                                self.txtWebsite.text = global_website
                                self.txtCompanyName.text = global_companyName
                                
                            }
                        }
                    }
                }
                    
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                
                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
    func getAllLists(urlString: String) {
        
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success response",data)
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    
                    let allListsResponseForDropdown = try? JSONDecoder().decode(MailWizardSubcriptionTypeResponse.self, from: response.data!)
                    self.allList = allListsResponseForDropdown ?? []
                    
                    DispatchQueue.main.async {
                        print("allProjects------------\n",self.allList)
                        self.removeSpinner()
                        
                        self.listTitleArray.removeAll()
                        self.listIDArray.removeAll()
                        
                        for list in self.allList{
                            let name = list.typeName
                            let id = list.id
                            print(name!, id!)
                            self.listTitleArray.append(name!)
                            self.listIDArray.append(id!)
                        }
                        print(self.listTitleArray)
                        self.setUpDropDownList(list: self.listTitleArray)
                        
                        
                    }
                }
                    
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                
                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
    
    // MARK: - Table view data source
    
    //    override func numberOfSections(in tableView: UITableView) -> Int {
    //        // #warning Incomplete implementation, return the number of sections
    //        return 0
    //    }
    //
    //    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    //        // #warning Incomplete implementation, return the number of rows
    //        return 0
    //    }
    
    /*
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
     
     // Configure the cell...
     
     return cell
     }
     */
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
