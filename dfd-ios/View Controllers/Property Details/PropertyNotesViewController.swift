//
//  PropertyNotesViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 6/27/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit
import JJFloatingActionButton
import Alamofire


class PropertyNotesViewController: UIViewController {
    
    @IBOutlet weak var notesTV: UITableView!
    
    let actionButton = JJFloatingActionButton()
    
    var allNotes: [Note] = []
    var url : String = ""
    var nextUrl: String = ""
    var previousUrl: String = ""
    var jsonResponse: GetAllNotesResponse?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
        let nib = UINib(nibName: "NotesTableViewCell", bundle: nil)
        notesTV.register(nib, forCellReuseIdentifier: "NotesTableViewCell")
        self.notesTV.tableFooterView = UIView()
        self.setUpNavigationBar()
        self.notesTV.rowHeight = UITableView.automaticDimension
        self.notesTV.estimatedRowHeight = 70.0;
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodForReloadNoteList(notification:)), name: Notification.Name("ReloadNoteList"), object: nil)
        
    }
    
    @objc func methodForReloadNoteList(notification: Notification) {
        if Network.reachability.status == .unreachable{
            AppConfigs.updateUserInterface(view: view)
        }else{
            allNotes.removeAll()
            let url : String = UrlManager.baseURL() + UrlManager.apiString() +  "/note/property/\((globalSelectedPropertyFromList?.id)!)/?limit=10&offset=0"
            print(url)
            //self.showSpinner(onView: view)
            getAllNotes(urlString: url)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if Network.reachability.status == .unreachable{
            AppConfigs.updateUserInterface(view: view)
        }else{

            allNotes.removeAll()
            let url : String = UrlManager.baseURL() + UrlManager.apiString() +  "/note/property/\(String(describing: (globalSelectedPropertyFromList?.id)!))/?limit=10&offset=0"
            print(url)
            //self.showSpinner(onView: view)
            getAllNotes(urlString: url)
        }
        //        }
    }
    
    
    @IBAction func addNoteClicked(_ sender: Any) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "NoteViewUpdateViewController") as! NoteViewUpdateViewController
        vc.parentString = "addNote"
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @objc func loadNoteTable() {
        self.notesTV.reloadData()
    }
    
    func getAllNotes(urlString: String) {
        
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success",data)
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    self.removeSpinner()//self.view.activityStopAnimating()
                    let alllNotes = try? JSONDecoder().decode(GetAllNotesResponse.self, from: response.data!)
                    self.jsonResponse = alllNotes
                    
                    var items: [Note] = self.allNotes
                    items.append(contentsOf: (alllNotes?.results)!)
                    
                    self.allNotes.removeAll()
                    self.allNotes = items
                    
                    DispatchQueue.main.async {
                        print("allNotes------------\n",self.allNotes)
                        self.removeSpinner()//self.view.activityStopAnimating()
                        self.notesTV.reloadData()
                        //self.perform(#selector(self.loadTable), with: nil, afterDelay: 1.0)
                    }
                }
                    
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                   // AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    self.removeSpinner()
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                
                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
    func handleDeleteNote(id:Int) {
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        
        let url = UrlManager.baseURL() + UrlManager.apiString() + "/note/" + "\(id)/"
        Alamofire.request(url, method: .delete, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
            case.success(let data):
                print("success",data)
                //self.removeSpinner()
                let statusCode = response.response?.statusCode
                print(statusCode!)
                
                if statusCode == 200{
                    
                    let deleteNoteResponse = try? JSONDecoder().decode(DeleteTeamMemberResponse.self, from: response.data!)
                    if deleteNoteResponse?.status == true{
                        
                        DispatchQueue.main.async {
                            self.removeSpinner()
                            AppConfigs.showSnacbar(message: (deleteNoteResponse?.message)!, textColor: .green)
                            self.allNotes.removeAll()
                            //self.showSpinner(onView: self.view)
                            let urlString = UrlManager.baseURL() + UrlManager.apiString() +  "/note/property/\((globalSelectedPropertyFromList?.id)!)/?limit=10&offset=0"
                            self.getAllNotes(urlString: urlString )
                        }
                    }else{
                        AppConfigs.showSnacbar(message: (deleteNoteResponse?.message)!, textColor: .green)
                    }
                }
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                
                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
                self.removeSpinner()
            }
            self.removeSpinner()
        }
    }
    
    
}


extension PropertyNotesViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if allNotes.count == 0 {
            tableView.setEmptyMessage("Nothing yet to show")
        } else {
            tableView.restore()
        }
        return allNotes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotesTableViewCell", for: indexPath) as! NotesTableViewCell
        cell.backgroundColor = .white //.groupTableViewBackground
        cell.upView.backgroundColor = .white
        cell.upView.cornerRadius = 8
        cell.lblNoteTitle.textColor = self.listTitleColor//UIColor(red:0, green:0, blue:0, alpha:0.8)
        cell.lblDate.textColor = UIColor(red:0.44, green:0.5, blue:0.58, alpha:1)
        cell.lblDescription.textColor = self.listSubTitleColor
        cell.lblNoteSeprator.backgroundColor = FontAndColorConfigs.getSeperatorColor()
        cell.lblDescription.adjustsFontSizeToFitWidth = false;
        
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [cell.lblNoteTitle], type: TextType.ListTitle.rawValue)
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [cell.lblDescription], type: TextType.ListSubTitle.rawValue)
        
        
        FontAndColorConfigs.setTextColor(labels: [cell.lblDate], color: FontAndColorConfigs.getGreenishThemeColor())
        
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [cell.lblDate], type: TextType.ListSubTitle.rawValue)
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        if allNotes.count > indexPath.row{
            
            let note = allNotes[indexPath.row]
            
            cell.lblNoteTitle.text = note.title
            cell.lblDescription.text = note.notes
            
            let dateString = note.updatedAt
            cell.lblDate.text = String( (dateString?.prefix(10))!)
            
            let tmp = String(dateString?.prefix(10) ?? "")
            
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS" //"yyyy-MM-dd'T'HH:mm:ss.ssssss'Z'"
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "MMM dd,yyyy"//"MMM dd yyyy h:mm a"  //"MMM d, h:mm a" for  Sep 12, 2:11 PM
            let datee = dateFormatterGet.date(from: note.updatedAt ?? "")
            let dateStringInNewFormat =  dateFormatterPrint.string(from: datee ?? Date())
            // print(dateStringInNewFormat)
            
            cell.lblDate.text = dateStringInNewFormat // String(dateString.prefix(10)) //dateStringInNewFormat
            
            if datee == nil{
                cell.lblDate.text = tmp
                let strDate: String = tmp + " 12:24:26"
                //print(strDate)
                
                let dateFormatterGet = DateFormatter()
                dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
                
                let dateFormatterPrint = DateFormatter()
                dateFormatterPrint.dateFormat = "MMM dd,yyyy"
                
                if let date = dateFormatterGet.date(from: strDate) {
                    // print(dateFormatterPrint.string(from: date))
                    cell.lblDate.text = dateFormatterPrint.string(from: date)
                } else {
                    print("There was an error decoding the string")
                }
            }else{
                print("datee is not nil")
                // print(datee)
            }
            
            if indexPath.row ==  self.allNotes.count - 1{
                if self.jsonResponse?.count != self.allNotes.count{
                    
                    if let nextValue = self.jsonResponse?.next {
                        print(nextValue)
                        self.getAllNotes(urlString: nextValue)
                    }
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if allNotes.count > indexPath.row{
            
            let note = allNotes[indexPath.row]
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "NoteViewUpdateViewController") as! NoteViewUpdateViewController
            vc.note = note
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }
        
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let delete = UIContextualAction(style: .destructive, title: "Delete") { (action, view, nil) in
            let alert = UIAlertController(title: "Are you sure you want to remove ", message: "  ", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action) in
                self.handleDeleteNote(id: self.allNotes[indexPath.row].id!)
                print("delete")
            }))
            self.present(alert, animated: true, completion: nil)
        }
        delete.backgroundColor = .white
        delete.image =  UIImage(named: "new_ic_delete")
        
        let config = UISwipeActionsConfiguration(actions: [delete])
        config.performsFirstActionWithFullSwipe = false
        
        
        return  config
        
    }
    
    
}
