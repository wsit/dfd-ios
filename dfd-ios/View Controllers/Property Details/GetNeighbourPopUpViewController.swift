//
//  GetNeighbourPopUpViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 12/23/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON

class GetNeighbourPopUpViewController: UIViewController {
    
    @IBOutlet weak var lblCostTitle: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var ownerInfoImageView: UIImageView!
    @IBOutlet weak var lblOwnerDetailsTitle: UILabel!
    @IBOutlet weak var lblOwnerDetailsAmount: UILabel!
    @IBOutlet weak var lblFirstSeperator: UILabel!
    @IBOutlet weak var btnPowerTraceSelectionImage: UIButton!
    @IBOutlet weak var lblSecondSeperator: UILabel!
    @IBOutlet weak var lblPowerTraceAmount: UILabel!
    @IBOutlet weak var lblPowerTraceTitle: UILabel!
    @IBOutlet weak var btnOwnerInfo: UIButton!
    @IBOutlet weak var powerTraceImage: UIImageView!
    @IBOutlet weak var lblPTMessage: UILabel!

    var selectedNeighboursArray: [GMSMarker] = []
    var tappedNeighboursArray: [TappedPlace] = []
    
    var currentNeighbours: GetNeighbourResponse = []
    
    var isPowerTraceSelected = 0
    var isOwnerInfoSelected = 0
    
    let basePrice : Int = 20
    var selectedNeighbour: GetNeighbourResponseElement?
    var propertyID : Int = 0
    
    
    var totalAmount : Double = 0.0
    var powerTraceAmount : Double = 0.0
    var ownerDetailsAmount : Double = 0.0
    var parentVC: String = ""
    
    var isReOwnerInfoClicked: Bool = false
    var isRePowerTracedClicked: Bool = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblPTMessage.isHidden = true

        lblCostTitle.textColor = self.popUpBluishTitleColor
        lblAmount.textColor = self.popUpBluishTitleColor
        lblOwnerDetailsAmount.textColor = self.popUpBluishTitleColor
        lblPowerTraceAmount.textColor = self.popUpBluishTitleColor
        
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [lblCostTitle], type: TextType.ListSubTitle.rawValue)
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [lblOwnerDetailsTitle, lblOwnerDetailsAmount, lblPowerTraceAmount], type: TextType.ListTitle.rawValue)
        
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [lblAmount], type: TextType.MainTitle.rawValue)
        FontAndColorConfigs.setDynamicButtonsFontSize(buttons: [btnConfirm], type: TextType.ListTitle.rawValue)
        
        lblFirstSeperator.backgroundColor = self.listSeperatorColor
        lblSecondSeperator.backgroundColor = self.listSeperatorColor
        
        btnPowerTraceSelectionImage.setImage(UIImage(named: "ios7-circle-outline"), for: .normal)
        
        self.powerTraceAmount = Double(self.selectedNeighboursArray.count) *  (AppConfigs.getCurrentUserInfo().upgradeInfo?.powerTrace ?? 0.0)
        self.ownerDetailsAmount = Double(self.selectedNeighboursArray.count) * (AppConfigs.getCurrentUserInfo().upgradeInfo?.ownerInfo ?? 0.0)
        self.totalAmount = Double(self.selectedNeighboursArray.count) *  (AppConfigs.getCurrentUserInfo().upgradeInfo?.ownerInfo ?? 0.0)
        
        self.lblAmount.text = String(format: "$%.2f", self.totalAmount) //"$\(self.totalAmount)"
        self.lblPowerTraceAmount.text =  String(format: "$%.2f", self.powerTraceAmount) //"$\(self.powerTraceAmount)"
        self.lblOwnerDetailsAmount.text =  String(format: "$%.2f", self.ownerDetailsAmount) //"$\(self.ownerDetailsAmount)"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //single neighbour owner details fetch
        if self.parentVC == "nod"{
            self.lblPowerTraceTitle.isHidden = true
            self.powerTraceImage.isHidden = true
            self.lblPowerTraceAmount.isHidden = true
            self.btnPowerTraceSelectionImage.isHidden = true
            self.powerTraceImage.isHidden = true
            self.lblSecondSeperator.isHidden = true
            
            self.totalAmount =  ownerDetailsAmount
            self.lblAmount.text = String(format: "$%.2f", self.totalAmount)
            
            self.isPowerTraceSelected = 0
            self.isOwnerInfoSelected = 1
            
            
            if isReOwnerInfoClicked == true{
                self.lblOwnerDetailsTitle.text = "Re-fetch Owner details"
            }
            
        }
            //single neighbour power trace fetch
        else if self.parentVC == "npt"{
            self.lblOwnerDetailsTitle.isHidden = true
            self.ownerInfoImageView.isHidden = true
            self.lblOwnerDetailsAmount.isHidden = true
            self.btnOwnerInfo.isHidden = true
            self.lblFirstSeperator.isHidden = true
            
            btnPowerTraceSelectionImage.setImage(UIImage(named: "ios7-checkmark-outline"), for: .normal)
            self.totalAmount = (AppConfigs.getCurrentUserInfo().upgradeInfo?.powerTrace)!
            self.lblAmount.text = String(format: "$%.2f", self.totalAmount)
            self.lblPowerTraceAmount.text = String(format: "$%.2f", self.totalAmount)
            self.btnPowerTraceSelectionImage.isUserInteractionEnabled = false
            
            self.isPowerTraceSelected = 1
            self.isOwnerInfoSelected = 0

            if isRePowerTracedClicked == true{
                self.lblPowerTraceTitle.text = "Re-run Power trace"
            }

            lblPTMessage.text = "Lookup the latest mobile phones, landlines, emails, social media accounts"
            lblPTMessage.isHidden = false
            
            
        }
            //all current neighbours
        else if parentVC == "cn"{
            btnPowerTraceSelectionImage.setImage(UIImage(named: "ios7-checkmark-outline"), for: .normal)
            self.isPowerTraceSelected = 1
            self.isOwnerInfoSelected = 1
            self.powerTraceAmount = Double(self.currentNeighbours.count) *  (AppConfigs.getCurrentUserInfo().upgradeInfo?.powerTrace ?? 0.0)
            self.ownerDetailsAmount = Double(self.currentNeighbours.count) * (AppConfigs.getCurrentUserInfo().upgradeInfo?.ownerInfo ?? 0.0)
            
            self.totalAmount = powerTraceAmount + ownerDetailsAmount
            self.totalAmount = powerTraceAmount + ownerDetailsAmount
            
            self.lblPowerTraceAmount.text = String(format: "$%.2f", self.powerTraceAmount)
            self.lblAmount.text = String(format: "$%.2f", self.totalAmount)
            self.lblOwnerDetailsAmount.text = String(format: "$%.2f", self.ownerDetailsAmount)
            lblPTMessage.text = "Lookup the latest mobile phones, landlines, emails, social media accounts"
            lblPTMessage.isHidden = false
        }
        else{
            //all neighbour fetch
            lblPTMessage.text = "Lookup the latest mobile phones, landlines, emails, social media accounts"
            lblPTMessage.isHidden = false
        }
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func powerTraceSelectionButtonClicked(_ sender: Any) {
        
        if self.btnPowerTraceSelectionImage.currentImage == UIImage(named: "ios7-circle-outline"){
            print("power trace selected")
            btnPowerTraceSelectionImage.setImage(UIImage(named: "ios7-checkmark-outline"), for: .normal)
            
            self.isPowerTraceSelected = 1
            
            self.totalAmount = powerTraceAmount + ownerDetailsAmount
            self.lblAmount.text = String(format: "$%.2f", self.totalAmount)
            
        }
        else if self.btnPowerTraceSelectionImage.currentImage == UIImage(named: "ios7-checkmark-outline"){
            print("power trace not selected")
            btnPowerTraceSelectionImage.setImage(UIImage(named: "ios7-circle-outline"), for: .normal)
            
            self.isPowerTraceSelected = 0
            self.totalAmount =  ownerDetailsAmount
            self.lblAmount.text = String(format: "$%.2f", self.totalAmount)
            
            
        }
        else{
            print("unknown case")
        }
    }
    
    @IBAction func confirmButtonClicked(_ sender: Any) {
        
        testing = false
        if testing == true{
            //            let storyboard = UIStoryboard(name: "Settings", bundle: nil)
            //            let vc = storyboard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
            //            self.present(vc, animated: true, completion: nil)
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "RechargeViewController") as! RechargeViewController
            self.present(vc, animated: true, completion: nil)
        }else{
            
            //            if AppConfigs.getCurrentUserInfo().upgradeInfo?.amount ?? 0.0 < self.totalAmount {
            //                //let storyboard = UIStoryboard(name: "Settings", bundle: nil)
            //                let vc = self.storyboard?.instantiateViewController(withIdentifier: "RechargeViewController") as! RechargeViewController
            //                vc.modalPresentationStyle = .fullScreen
            //                self.present(vc, animated: true, completion: nil)
            //            }else{
            
            let url = UrlManager.baseURL() + UrlManager.apiString() + "/property/\(self.propertyID)/get-neighborhood/"
            print(url)
            
            var parameters: Parameters = [:]
            
            if self.parentVC == "nod"{
                parameters = [
                    "fetch_owner_info" : 1,
                    "power_trace" :  0,
                    "neighbor_id" : [(self.selectedNeighbour?.id)!]
                ]
            }
            else if self.parentVC == "npt"{
                parameters = [
                    "fetch_owner_info" : 0,
                    "power_trace" :  1,
                    "neighbor_id" : [(self.selectedNeighbour?.id)!]
                ]
            }
            else if parentVC == "cn"{
                var neighboursID: [Int] = []
                for neighbour in self.currentNeighbours{
                    
                    let id = neighbour.id
                    print(id!)
                    neighboursID.append(id!)
                }
                let jsonserializer = JSONSerializer.toJson(neighboursID)
                print("jsonserializer", jsonserializer)
                parameters = [
                    "fetch_owner_info" : 1,
                    "power_trace" :  self.isPowerTraceSelected,
                    "neighbor_id" : neighboursID
                ]
            }
            else{
                //new neighbour
                var neighbours: [SelectedNeighbour] = []
                for i in 0..<tappedNeighboursArray.count{
                    let data = tappedNeighboursArray[i]
                    let neighbour : SelectedNeighbour = SelectedNeighbour(latitude: data.latitude, longitude: data.longitude, address: data.fullAddress, street: data.street, city: data.city, state: data.state, zip: data.zip)
                    neighbours.append(neighbour)
                     
                }
                let jsonserializer = JSONSerializer.toJson(neighbours)
                print("jsonserializer", jsonserializer)
                do{
                    let jsonNeighbourArray = try JSONSerializer.toArray(jsonserializer)
                    print("jsonNeighbourArray", jsonNeighbourArray)

                    parameters = [
                        "fetch_owner_info" : 1,
                        "power_trace" :  self.isPowerTraceSelected,
                        "address" : jsonNeighbourArray
                    ]
                }catch let error{
                    print(error.localizedDescription)
                }
            }
            
            print(parameters)
            handlePowerTraceOwnerInfoForNeighbours(urlString: url, params: parameters)
            //            }
        }
        
    }
    
    func handlePowerTraceOwnerInfoForNeighbours(urlString: String , params: Parameters) {
        self.showSpinner(onView: self.btnConfirm)
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        
        Alamofire.request(urlString, method: .post, parameters: params as Parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
                
            case.success(let data):
                
                self.removeSpinner()
                print("success",data)
                let statusCode = response.response?.statusCode
                print(statusCode!)
                
                if statusCode == 200{
                    self.removeSpinner()
                    // let ownerInfoAndPowerTraceResponse = try? JSONDecoder().decode(OwnerInfoAndPowerTraceResponse.self, from: response.data!)
                    
                    do {
                        
                        let data = try JSON(data: response.data!)
                        let status = data["status"].boolValue
                        let message = data["message"].stringValue
                        AppConfigs.showSnacbar(message: message, textColor: .blue)
                        
                        if status == true{
                            self.dismiss(animated: true, completion: nil)
                            NotificationCenter.default.post(name:NSNotification.Name("getNeighbourDone"),object: nil)
                            
                        }else{
                            
                            let jsonData = data["data"]
                            let payment = jsonData["payment"].boolValue
                            if payment == false{
                                if AppConfigs.getCurrentUserInfo().isAdmin == false{
                                    AppConfigs.showSnacbar(message: message, textColor: .blue)
                                }else{
                                    //AppConfigs.showSnacbar(message: message, textColor: .blue)
                                    let alertController = UIAlertController(title: message, message: "", preferredStyle: .alert)
                                    let OKAction = UIAlertAction(title: "Ok", style: .default) { (alert) in
                                        
                                        if AppConfigs.getCurrentUserInfo().thinker == true{
                                            
                                            let main = UIStoryboard(name: "Settings", bundle: nil)
                                            let vc = main.instantiateViewController(withIdentifier: "RechargeForIAPViewController") as! RechargeForIAPViewController
                                            //self.navigationController?.pushViewController(vc, animated: true)
                                            vc.modalPresentationStyle = .fullScreen
                                            self.present(vc, animated: true, completion: nil)
                                        }else{
                                            
                                            let main = UIStoryboard(name: "Main", bundle: nil)
                                            let vc = main.instantiateViewController(withIdentifier: "RechargeViewController") as! RechargeViewController
                                            //self.navigationController?.pushViewController(vc, animated: true)
                                            vc.modalPresentationStyle = .fullScreen
                                            self.present(vc, animated: true, completion: nil)
                                        }

//                                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RechargeViewController") as! RechargeViewController
//                                        vc.modalPresentationStyle = .fullScreen
//                                        self.present(vc, animated: true, completion: nil)
                                    }
                                    alertController.addAction(OKAction)
                                    self.present(alertController, animated: true, completion: nil)
//
//                                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "RechargeViewController") as! RechargeViewController
//                                    vc.modalPresentationStyle = .fullScreen
//                                    self.present(vc, animated: true, completion: nil)
                                    
//                                    let alertController = UIAlertController(title: message, message: "Please go to web to manage your account.", preferredStyle: .alert)
//                                    let OKAction = UIAlertAction(title: "Ok", style: .default) { (alert) in
//
//
//                                    }
//                                    alertController.addAction(OKAction)
//                                    self.present(alertController, animated: true, completion: nil)
                                }
                            }else{
                                AppConfigs.showSnacbar(message: message, textColor: .green)
                            }
                        }
                    }catch let error{
                        self.showAlert(message: " ", title: error.localizedDescription)
                        self.removeSpinner()
                    }
                    
                    self.removeSpinner()
                    //
                }else{
                    self.showAlert(message: " ", title: "Error Occured")
                    self.removeSpinner()
                }
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)

                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                 self.removeSpinner()
            }
            
        }
        
    }
    
    
}
