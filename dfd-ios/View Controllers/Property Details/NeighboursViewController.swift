//
//  NeighboursViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 7/16/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit
import GoogleMaps
import JJFloatingActionButton
import SnapKit
import TTGSnackbar
import Alamofire

var globalNeighboursResponse:[GetNeighbourResponseElement] = []
class NeighboursViewController: UIViewController {
    
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var btnMapType: UIButton! 
    @IBOutlet weak var mapTypeView: UIView!
    
    @IBOutlet weak var btnNormalView: UIButton!
    @IBOutlet weak var btnSatelliteView: UIButton!
    
    let marker = GMSMarker()
    var locationManager = CLLocationManager()
    let getInfoButton = UIButton()
    let actionButton = JJFloatingActionButton()
    let geocoder = GMSGeocoder()
    var infoWindow = UIView(frame: CGRect.init(x: 0, y: 0, width: 250, height: 80))
    var tappedMarker = GMSMarker()
    var currentLocation: CLLocation!
    var myLastLocation: CLLocation?
    var currentLocationMarker: GMSMarker?
    var selectedMarkerFromNeighbour: GMSMarker?
    
    var neighboursMarkerArray: [GMSMarker] = []
    var neighboursTappedMarkerArray: [TappedPlace] = []
    
    
    var infoViewHeightConstraint: NSLayoutConstraint?
    var currentPropertyDetails: PropertyDetailsResponse?
    var jsonResponse: [GetNeighbourResponseElement]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addFloatyButton()
        setUpView()
        mapView.mapType = .satellite
        mapView.delegate = self
        //setup Location Manager
        if CLLocationManager.locationServicesEnabled() {
            self.mapView?.isMyLocationEnabled = true
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.activityType = CLActivityType.automotiveNavigation
            //locationManager.distanceFilter = 1
            locationManager.headingFilter = 1
            self.locationManager.delegate = self
            self.locationManager.requestWhenInUseAuthorization()
            self.locationManager.startUpdatingLocation()
            //self.locationManager.startUpdatingHeading()
            
        }else{
            //location is not enabled
        }
        
        self.navigationController?.navigationBar.prefersLargeTitles = false
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.mapTypeView.isHidden = true
        //setupNavforDefaultMenu()
        setUpConstraint()
        mapView.clear()
        neighboursMarkerArray.removeAll()
        neighboursTappedMarkerArray.removeAll()
        setCurrentPropertyMarker()
        
        if Network.reachability.status == .unreachable{
            AppConfigs.updateUserInterface(view: view)
        }else{

            let url : String = UrlManager.baseURL() + UrlManager.apiString() + "/get-neighborhood/property/\(String(describing: (currentPropertyDetails?.id)!))/"
            
            print(url)
            // self.showSpinner(onView: view)
            getNeighbourInfoByPropertyID(urlString: url)
            
        }//            }
        
        
    }
    
    func setCurrentPropertyMarker() {
        
        let lat = currentPropertyDetails?.latitude
        let long = currentPropertyDetails?.longitude
        let camera = GMSCameraPosition.camera(withLatitude:lat ?? 0.0, longitude:long ?? 0.0, zoom: 17.0)
        mapView.camera = camera
        
        let marker = GMSMarker()
        marker.isDraggable = false
        marker.position = CLLocationCoordinate2D(latitude: lat ?? 0.0, longitude: long ?? 0.0)
        marker.title = title
        marker.icon = GMSMarker.markerImage(with: .red) //UIImage(named: "new_ic_tapped_property")
        marker.snippet = "currentProperty"
        marker.appearAnimation = .pop
        marker.map = mapView
        
    }
    
    @objc func buttonAction(sender: UIButton!) {
        print("Button tapped")
        self.zoomToCoordinates(myLastLocation!.coordinate)
        self.locationManager.startUpdatingLocation()
    }
    
    func setUpConstraint(){
        
        //drive now button constraint
        getInfoButton.snp.makeConstraints { (make) -> Void in
            make.height.equalTo(48)
            make.bottom.equalTo(mapView).offset(-20)
            make.leading.lessThanOrEqualTo(mapView).offset(16)
            make.trailing.lessThanOrEqualTo(mapView).offset(16)
            make.centerX.equalTo(mapView)
        }
    }
    
    @IBAction func backClicked(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSatelliteClicked(_ sender: Any) {
        self.mapView.mapType = .satellite
        self.mapTypeView.isHidden = true
    }
    
    @IBAction func btnNormalClicked(_ sender: Any) {
        self.mapView.mapType = .normal
        self.mapTypeView.isHidden = true
    }
    
    @IBAction func showNeighbourList(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "NeighbourDetailsViewController") as! NeighbourDetailsViewController
        vc.propertyID = (currentPropertyDetails?.id)!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func switchMapType(_ sender: Any) {
        //mapTypeDropdown.show()
        
        if self.mapTypeView.isHidden == true{
            CATransaction.begin()
            
            let transition = CATransition()
            
            transition.timingFunction = CAMediaTimingFunction.init(name: .easeInEaseOut)
            transition.type = .push
            transition.subtype = .fromRight
            
            
            self.mapTypeView.layer.add(transition, forKey: kCATransition)
            CATransaction.commit()
            
            self.mapTypeView.isHidden = false
        }else{
            self.mapTypeView.isHidden = true
        }
        
        
    }
    
    func setUpView() {
        
        //current location button code
        let CenterCurrenetLocationButton = UIButton(frame: CGRect(x: mapView.frame.width - 30, y: 10, width: 70, height: 70))
        CenterCurrenetLocationButton.setImage(UIImage(named: "ic_get_current_location"), for: .normal)
        CenterCurrenetLocationButton.adjustsImageSizeForAccessibilityContentSizeCategory = true
        CenterCurrenetLocationButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        //self.mapView.addSubview(CenterCurrenetLocationButton)
        self.mapView.addSubview(getInfoButton)
        //self.mapView.addSubview(floatingButton)
        
        //drive now button code
        getInfoButton.backgroundColor = self.buttonBackgroundColor
        
        FontAndColorConfigs.setDynamicButtonsFontSize(buttons: [getInfoButton], type: TextType.ListTitle.rawValue)
        
        getInfoButton.setTitle("Next", for: .normal)
        getInfoButton.cornerRadius = 8
        getInfoButton.setTitleColor(UIColor.white, for: .normal)
        getInfoButton.addTarget(self, action: #selector(getInfoButtonAction), for: .touchUpInside)
        
        self.getInfoButton.isHidden = true
        self.actionButton.isHidden = true
        
    }
    
    @objc func getInfoButtonAction() {
        print("getInfoButtonAction clicked")
        if neighboursTappedMarkerArray.count > 0 {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "NeighboursListViewController") as! NeighboursListViewController
            vc.propertyID = (currentPropertyDetails?.id!)!
            vc.selectedNeighboursArray = neighboursMarkerArray
            vc.tappedNeighboursArray = neighboursTappedMarkerArray
            
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            AppConfigs.showSnacbar(message: "Please Select Neighbours First", textColor: .red)
        }
    }
    
    
    func getNeighbourInfoByPropertyID(urlString: String) {
        
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                //print("success",data)
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    
                    let allDriveResponse = try? JSONDecoder().decode([GetNeighbourResponseElement].self, from: response.data!)
                    self.jsonResponse = allDriveResponse
                    globalNeighboursResponse.removeAll()
                    globalNeighboursResponse = self.jsonResponse ?? []
                    print(self.jsonResponse ?? [])
                    print("self.jsonResponse",self.jsonResponse?.count ?? 0)
                    DispatchQueue.main.async {
                        // print("drivingList------------\n",self.drivingList)
                        self.putCurrentNeighbourMarkersInMap()
                        self.removeSpinner()//self.view.activityStopAnimating()
                    }
                }
                    
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                   // AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)

                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
    
    func putCurrentNeighbourMarkersInMap(){
        if self.jsonResponse != nil{
            if self.jsonResponse!.count > 0{
                for neighbour in self.jsonResponse!{
                    let marker = GMSMarker()
                    marker.isDraggable = false
                    print("lneighbour.latitude",neighbour.latitude!)
                    print("neighbour.longitude!", neighbour.longitude!)
                    
                    marker.position = CLLocationCoordinate2D(latitude: neighbour.latitude!, longitude: neighbour.longitude!)
                    marker.title = neighbour.neighborAddress ?? ""
                    marker.icon = GMSMarker.markerImage(with: .orange)
                    //UIImage(named: "Shape")
                    marker.snippet = "\(neighbour.id!)"
                    marker.appearAnimation = .pop
                    marker.map = mapView
                }
            }
        }
    }
    
    func addFloatyButton() {
        actionButton.buttonImage = UIImage(named: "visibility")
        actionButton.buttonAnimationConfiguration = .transition(toImage: UIImage(named: "close")!)
        actionButton.itemAnimationConfiguration = .slideIn(withInterItemSpacing: 14)
        actionButton.contentVerticalAlignment = .fill
        actionButton.contentHorizontalAlignment = .fill
        actionButton.buttonColor = UIColor(red:0.07, green:0.82, blue:0.41, alpha:1)
        
        let itemPrivate = actionButton.addItem()
        itemPrivate.titleLabel.text = "    Private    "
        itemPrivate.titleLabel.textColor = .black
        itemPrivate.titleLabel.backgroundColor = .white
        itemPrivate.imageView.image = UIImage(named: "locked-padlock")
        itemPrivate.buttonColor = .white
        itemPrivate.action = { item in
            // Do something
        }
        
        let itemPublic = actionButton.addItem()
        itemPublic.titleLabel.text = "    Public    "
        itemPublic.titleLabel.textColor = .black
        itemPublic.titleLabel.backgroundColor = .white
        itemPublic.imageView.image = UIImage(named: "world")
        itemPublic.buttonColor = .white
        itemPublic.action = { item in
            // Do something
        }
        
        mapView.addSubview(actionButton)
        actionButton.translatesAutoresizingMaskIntoConstraints = false
        actionButton.trailingAnchor.constraint(equalTo: mapView.safeAreaLayoutGuide.trailingAnchor, constant: -20).isActive = true
        actionButton.bottomAnchor.constraint(equalTo: mapView.safeAreaLayoutGuide.bottomAnchor, constant: -30).isActive = true
        
    }
    
    func showMarkerWithTitleSnippet(title: String, snippet:String, position: CLLocationCoordinate2D, tappedPlace: TappedPlace){
        
        infoWindow.removeFromSuperview()
        print("You tapped at \(position.latitude), \(position.longitude)")
        
        let marker = GMSMarker()
        marker.isDraggable = false
        marker.position = position
        marker.title = title
        marker.icon = GMSMarker.markerImage(with: .cyan)
        marker.snippet = snippet
        marker.appearAnimation = .pop
        marker.map = mapView
        
        let point:CGPoint = mapView.projection.point(for: marker.position)
        let camera:GMSCameraUpdate = GMSCameraUpdate.setTarget(mapView.projection.coordinate(for: point))
        mapView.animate(with: camera)
        neighboursMarkerArray.append(marker)
        neighboursTappedMarkerArray.append(tappedPlace)
        
        if neighboursTappedMarkerArray.count > 0{
            self.getInfoButton.isHidden = false
            
        }else{
            self.getInfoButton.isHidden = true
            
        }
        
    }
    
    func startMonitoringLocation() {
        if CLLocationManager.locationServicesEnabled() {
            
            locationManager.requestWhenInUseAuthorization()
            locationManager.startMonitoringSignificantLocationChanges()
            locationManager.startUpdatingLocation()
        }
    }
    
    func startReceivingSignificantLocationChanges() {
        let authorizationStatus = CLLocationManager.authorizationStatus()
        //        if authorizationStatus != .authorizedAlways {
        //            // User has not authorized access to location information.
        //            return
        //        }
        
        if !CLLocationManager.significantLocationChangeMonitoringAvailable() {
            // The service is not available.
            return
        }
        locationManager.delegate = self
        locationManager.startMonitoringSignificantLocationChanges()
    }
    
    func stopMonitoringLocation() {
        locationManager.stopMonitoringSignificantLocationChanges()
        locationManager.stopUpdatingLocation()
    }
    
    func addCurrentLocationMarker() {
        currentLocationMarker?.map = nil
        currentLocationMarker = nil
        var marker = GMSMarker()
        if let location = locationManager.location {
            
            marker = GMSMarker(position: location.coordinate)
            marker.icon = UIImage(named: "ic_car")
            marker.map = mapView
            marker.rotation = locationManager.location?.course ?? 0
            self.locationManager.startUpdatingLocation()
            
        }
    }
    
    func zoomToCoordinates(_ coordinates: CLLocationCoordinate2D) {
        let camera = GMSCameraPosition.camera(withLatitude: coordinates.latitude, longitude: coordinates.longitude, zoom: 17)
        mapView.camera = camera
        //addCurrentLocationMarker()
    }
    
    @objc func cancelBtnAction()  {
        infoWindow.removeFromSuperview()
        
        print(self.neighboursMarkerArray.count)
        print(self.neighboursTappedMarkerArray.count)
        
    }
    
    @objc func deleteBtnAction()  {
        
        print(self.neighboursMarkerArray.count)
        
        var position : Int = 0
        var isFound = false
        var deletedMarker = GMSMarker()
        
        if self.neighboursMarkerArray.count > 0{
            
            for i in 0..<self.neighboursMarkerArray.count {
                
                let marker = self.neighboursMarkerArray[i]
                let title = marker.title
                let selectedMarkerFromNeighbourTitle = self.selectedMarkerFromNeighbour?.title
                
                if selectedMarkerFromNeighbourTitle == title{
                    
                    position = i
                    isFound = true
                    deletedMarker = marker
                    break
                }else{
                    print(title!)
                    isFound = false
                }
            }
            
            if isFound == true{
                deletedMarker.map = nil
                self.selectedMarkerFromNeighbour?.title = ""
                self.neighboursMarkerArray.remove(at: position)
                self.neighboursTappedMarkerArray.remove(at: position)
            }
        }
        print(self.neighboursMarkerArray.count)
        print(self.neighboursTappedMarkerArray.count)
        
        if neighboursTappedMarkerArray.count > 0{
            self.getInfoButton.isHidden = false
            
        }else{
            self.getInfoButton.isHidden = true
            
        }
        
        infoWindow.removeFromSuperview()
        
    }
    
    
}

extension NeighboursViewController: CLLocationManagerDelegate{
    //Location Manager delegates
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("location manager error -> \(error.localizedDescription)")
        AppConfigs.showSnacbar(message: "Please enable location permission", textColor: .red)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        case .restricted:
            break
        case .denied:
            stopMonitoringLocation()
            break
        default:
            //addCurrentLocationMarker()
            startMonitoringLocation()
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //Finally stop updating location otherwise it will come again and again in this delegate
        guard let lastLocation = locations.last  else { return }
        //self.zoomToCoordinates(lastLocation.coordinate)
        self.myLastLocation = lastLocation
        currentLocationMarker?.position = lastLocation.coordinate
        currentLocationMarker?.rotation = lastLocation.course
        
    }
    
    
    func cameraMoveToLocation(toLocation: CLLocationCoordinate2D?) {
        if toLocation != nil {
            mapView.camera = GMSCameraPosition.camera(withTarget: toLocation!, zoom: 17)
        }
    }
}

extension NeighboursViewController: GMSMapViewDelegate{
    
    /* handles Info Window tap */
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        print("didTapInfoWindowOf")
    }
    
    /* handles Info Window long press */
    func mapView(_ mapView: GMSMapView, didLongPressInfoWindowOf marker: GMSMarker) {
        print("didLongPressInfoWindowOf")
    }
    
    
    func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D) {
        infoWindow.removeFromSuperview()
        mapView.selectedMarker = nil
        
        
        if neighboursTappedMarkerArray.count == 10{
            self.showAlert(message: "You can only add 10 Neighbors at a time", title: "Warning!")
            
        }else{
            
            
           
            marker.isDraggable = true
            //29.709151, -95.456857
            marker.position.latitude = coordinate.latitude
            marker.position.longitude = coordinate.longitude
            print(coordinate.latitude, coordinate.longitude)
            
            //marker.snippet = "San Francisco"
            self.marker.icon = GMSMarker.markerImage(with: UIColor.cyan)
            //UIImage(named: "Shape")
            //self.marker.map = mapView
            self.marker.appearAnimation = .pop
            mapView.selectedMarker = self.marker
            
            let point:CGPoint = mapView.projection.point(for: marker.position)
            let camera:GMSCameraUpdate = GMSCameraUpdate.setTarget(mapView.projection.coordinate(for: point))
            mapView.animate(with: camera)
            
            var addressString : String = ""
            let ceo: CLGeocoder = CLGeocoder()
            let loc: CLLocation = CLLocation(latitude:coordinate.latitude, longitude: coordinate.longitude)
            
            let tappedPlace = TappedPlace()
            ceo.reverseGeocodeLocation(loc, completionHandler:
                {(placemarks, error) in
                    if (error != nil)
                    {
                        print("reverse geodcode fail: \(error!.localizedDescription)")
                    }
                    let pm = placemarks! as [CLPlacemark]
                    if pm.count > 0 {
                        let pm = placemarks![0]
                        
                        print("pm.country-->",pm.country ?? "")
                        print("locality-->", pm.locality ?? "")
                        print("subLocality-->", pm.subLocality ?? "")
                        print("thoroughfare-->", pm.thoroughfare ?? "")
                        print("postalCode-->", pm.postalCode ?? "")
                        print("subThoroughfare-->", pm.subThoroughfare ?? "")
                        print("administrativeArea", pm.administrativeArea ?? "")
                        print(pm.inlandWater ?? "")
                        print(pm.isoCountryCode ?? "")
                        print("name-->",pm.name ?? "")
                        print("region-->", pm.region ?? "" )
                        //4613 Locust Street, Bellaire, TX, USA
                        
                        if pm.name != nil {
                            addressString = addressString + pm.name! + ", "
                            tappedPlace.street = pm.name!
                        }
//                        
//                        if pm.subLocality != nil {
//                            addressString = addressString + pm.subLocality! + ", "
//                            //tappedPlace.street = addressString
//                        }
                        if pm.locality != nil {
                            addressString = addressString + pm.locality! + ", "
                            tappedPlace.city = pm.locality!
                        }
                        
                        if pm.administrativeArea != nil {
                            addressString = addressString + pm.administrativeArea! + " "
                            tappedPlace.state = pm.administrativeArea!
                        }
                        if pm.postalCode != nil {
                            addressString = addressString + pm.postalCode! //+ ",  "
                            tappedPlace.zip = pm.postalCode!
                        }
//                        if pm.country != nil {
//                            addressString = addressString + pm.country!
//                            tappedPlace.country = pm.country!
//                        }
                        
                        print(addressString)
                        
                        self.marker.title = addressString
                        globalSelectedProperty.propertyAddress = addressString
                        globalSelectedProperty.latitude = Double(coordinate.latitude)
                        globalSelectedProperty.longitude = Double(coordinate.longitude)
                         print("You tapped at \(coordinate.latitude), \(coordinate.longitude)")
                        tappedPlace.fullAddress = addressString
                        tappedPlace.latitude = Double(coordinate.latitude)
                        tappedPlace.longitude = Double(coordinate.longitude)
                        
                        globalTappedPlace = tappedPlace
                        self.showMarkerWithTitleSnippet(title: addressString, snippet: "tappedMarker", position: self.marker.position, tappedPlace: tappedPlace)
                    }
            })
            
            
        }
        
        
        
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        
        infoWindow.removeFromSuperview()
        
    }
    
    /* set a custom Info Window */
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        
        return UIView()
    }
    
    //MARK - GMSMarker Dragging
    func mapView(_ mapView: GMSMapView, didBeginDragging marker: GMSMarker) {
        print("didBeginDragging")
    }
    func mapView(_ mapView: GMSMapView, didDrag marker: GMSMarker) {
        print("didDrag")
    }
    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
        print("didEndDragging")
        mapView.selectedMarker = marker
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        print("did tap a marker")
        
        
        self.selectedMarkerFromNeighbour = marker
        
        if marker.snippet == "tappedMarker"{
            
            infoWindow.removeFromSuperview()
            //let location = CLLocationCoordinate2D(latitude: marker.position.latitude, longitude: marker.position.longitude)
            //        tappedMarker = marker
            infoWindow.removeFromSuperview()
            infoWindow = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width - 32, height: 140))
            infoViewHeightConstraint = infoWindow.heightAnchor.constraint(equalToConstant: 140)
            //infoWindow.center = mapView.projection.point(for: location)
            infoWindow.center = CGPoint(x: self.mapView.center.x + 5, y: self.mapView.center.y - 120)
            infoWindow.backgroundColor = UIColor.white
            infoWindow.layer.cornerRadius = 6
            
            let titleLbl = UILabel(frame: CGRect.init(x: 16, y: 16, width: infoWindow.frame.size.width - 8, height: AppConfigs.heightForView(text: marker.title ?? "", font: ProNovaSB18, width: infoWindow.frame.size.width)))
            titleLbl.text = "Neighbor Address"
            titleLbl.font = ProNovaSB20
            titleLbl.textColor = UIColor(red:0.2, green:0.3, blue:0.41, alpha:1)//.red// UIColor(red:0.2, green:0.3, blue:0.41, alpha:1)
            titleLbl.textAlignment = .left
            titleLbl.numberOfLines = 0
            titleLbl.lineBreakMode = .byWordWrapping
            titleLbl.sizeToFit()
            infoWindow.addSubview(titleLbl)
            
            let lbl1 = UILabel(frame: CGRect.init(x: 16, y: 32 + titleLbl.frame.size.height, width: infoWindow.frame.size.width - 16, height: AppConfigs.heightForView(text: marker.title ?? "", font: ProNovaR14, width: infoWindow.frame.size.width)))
            lbl1.text = marker.title
            lbl1.textColor = UIColor(red:0.2, green:0.3, blue:0.41, alpha:1)//.red// UIColor(red:0.2, green:0.3, blue:0.41, alpha:1)
            lbl1.textAlignment = .left
            lbl1.numberOfLines = 0
            lbl1.lineBreakMode = .byWordWrapping
            lbl1.sizeToFit()
            infoWindow.addSubview(lbl1)
            //
            let cancelBtn = UIButton(frame: CGRect.init(x: 16, y: lbl1.frame.origin.y + lbl1.frame.size.height + 3, width: infoWindow.frame.size.width/2 - 32, height: 30))
            cancelBtn.backgroundColor = self.greenCapsuleColor
            cancelBtn.setTitle("Cancel", for: .normal)
            //        cancelBtn.setImage(UIImage(named: "ic_like"), for: .normal)
            cancelBtn.titleLabel?.textColor = .white
            cancelBtn.layer.cornerRadius = 5
            cancelBtn.tintColor = .white
            cancelBtn.centerAndImage(withSpacing: 10.0)
            cancelBtn.addTarget(self, action: #selector(cancelBtnAction), for: .touchUpInside)
            //infoWindow.addSubview(cancelBtn)
            
            let deleteBtn = UIButton(frame: CGRect.init(x: infoWindow.frame.size.width/2 - 70 , y: lbl1.frame.origin.y + lbl1.frame.size.height + 8, width: 200, height: 30))
            //let deleteBtn = UIButton(frame: CGRect.init(x: cancelBtn.frame.size.width + 32 , y: lbl1.frame.origin.y + lbl1.frame.size.height + 3, width: infoWindow.frame.size.width/2 - 32, height: 30))
            deleteBtn.backgroundColor = self.blueCapsuleColor
            deleteBtn.setTitle("Remove", for: .normal)
            // deleteBtn.setImage(UIImage(named: "ic_show_property_details"), for: .normal)
            deleteBtn.titleLabel?.textColor = .white
            deleteBtn.tintColor = .white
            deleteBtn.layer.cornerRadius = 5
            deleteBtn.centerAndImage(withSpacing: 10.0)
            deleteBtn.addTarget(self, action: #selector(deleteBtnAction), for: .touchUpInside)
            infoWindow.addSubview(deleteBtn)
            self.view.addSubview(infoWindow)
            
            let height = titleLbl.frame.size.height + cancelBtn.frame.size.height + lbl1.frame.size.height + 40
            infoViewHeightConstraint = infoWindow.heightAnchor.constraint(equalToConstant: height)
            
        }
        else if marker.snippet == "currentProperty"{
            
        }else{
            print(marker.snippet)
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OwnerDetailsViewController") as! OwnerDetailsViewController
            vc.parentSring = "neighbour"
            vc.neighbourID = marker.snippet ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        return false
        
    }
    
    
    // let the custom infowindow follows the camera
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        if (tappedMarker.userData != nil){
            let location = CLLocationCoordinate2D(latitude: marker.position.latitude, longitude: marker.position.longitude)
            infoWindow.center = mapView.projection.point(for: location)
            
        }
    }
    
    // take care of the close event
    
    
}
