//
//  NeighboursListViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 7/19/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit
import GoogleMaps

class NeighboursListViewController: UIViewController {
    
    @IBOutlet weak var neighboursTV: UITableView!
    @IBOutlet weak var btnPowerTrace: UIButton!
    
    var selectedNeighboursArray: [GMSMarker] = []
    var tappedNeighboursArray: [TappedPlace] = []
    
    var propertyID : Int = 0
    
    var indexArray : [IndexPath] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnPowerTrace.backgroundColor = self.buttonBackgroundColor
        FontAndColorConfigs.setDynamicButtonsFontSize(buttons: [self.btnPowerTrace], type: TextType.ListTitle.rawValue)
        
        let nib = UINib(nibName: "NeighboursTableViewCell", bundle: nil)
        neighboursTV.register(nib, forCellReuseIdentifier: "NeighboursTableViewCell")
        self.neighboursTV.tableFooterView = UIView()
        self.neighboursTV.backgroundColor = .white
        self.neighboursTV.separatorStyle = .none
        
        neighboursTV.rowHeight = UITableView.automaticDimension
        neighboursTV.estimatedRowHeight = UITableView.automaticDimension
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.goToParentVC), name: Notification.Name("getNeighbourDone"), object: nil)
        
    }
    
    
    @objc func goToParentVC(notification: Notification){
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonNextClicked(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "GetNeighbourPopUpViewController") as! GetNeighbourPopUpViewController
        vc.propertyID = self.propertyID
        vc.modalPresentationStyle = .overCurrentContext
        vc.selectedNeighboursArray = self.selectedNeighboursArray
        vc.tappedNeighboursArray = self.tappedNeighboursArray
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func powerTraceClicked(_ sender: Any) {
        if indexArray.count == 0{
            //AppConfigs.showSnacbar(message: "Select Neighbours First", textColor: .red)
            self.showAlert(message: "Select A Neighbours First", title: "Error")
        }else{
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "PowerTraceViewController") as! PowerTraceViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func mailerWizardClicked(_ sender: Any) {
        
        if indexArray.count == 0{
            self.showAlert(message: "Select Neighbours First", title: "Error")
            //AppConfigs.showSnacbar(message: "Select Neighbours First", textColor: .red)
        }else{
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "MailerWizardViewController") as! MailerWizardViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension NeighboursListViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tappedNeighboursArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NeighboursTableViewCell", for: indexPath) as! NeighboursTableViewCell
        cell.backgroundColor = .white
        cell.upView.backgroundColor = .white
        cell.upView.cornerRadius = 8
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.lblNeighboursAddress.textColor = UIColor(red:0.2, green:0.28, blue:0.36, alpha:1)
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [cell.lblNeighboursAddress], type: TextType.ListTitle.rawValue)
        cell.lblSeperator.backgroundColor = self.listSeperatorColor
        
        if tappedNeighboursArray.count > 0{
            let marker = tappedNeighboursArray[indexPath.row]
            cell.lblNeighboursAddress.text = marker.fullAddress
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath)
        let row = indexPath.row
        print(row)
        
        print(indexArray.count)
    }
}
