//
//  MailerWizardViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 7/16/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit

class MailerWizardViewController: UIViewController {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblCityStateZip: UILabel!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var lblMailID: UILabel!
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var btnNext: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lblName.borderColor = UIColor(red:0.8, green:0.84, blue:0.89, alpha:1)
        lblName.textColor = UIColor(red:0.77, green:0.79, blue:0.82, alpha:1)
        lblAddress.borderColor = UIColor(red:0.8, green:0.84, blue:0.89, alpha:1)
        lblAddress.textColor = UIColor(red:0.77, green:0.79, blue:0.82, alpha:1)
        lblCityStateZip.borderColor = UIColor(red:0.8, green:0.84, blue:0.89, alpha:1)
        lblCityStateZip.textColor = UIColor(red:0.77, green:0.79, blue:0.82, alpha:1)
        lblPhoneNumber.borderColor = UIColor(red:0.8, green:0.84, blue:0.89, alpha:1)
        lblPhoneNumber.textColor = UIColor(red:0.77, green:0.79, blue:0.82, alpha:1)
        lblMailID.borderColor = UIColor(red:0.8, green:0.84, blue:0.89, alpha:1)
        lblMailID.textColor = UIColor(red:0.77, green:0.79, blue:0.82, alpha:1)
        lblCompanyName.borderColor = UIColor(red:0.8, green:0.84, blue:0.89, alpha:1)
        lblCompanyName.textColor = UIColor(red:0.77, green:0.79, blue:0.82, alpha:1)
        btnNext.backgroundColor = UIColor(red:0.07, green:0.82, blue:0.41, alpha:1)
        
        view.backgroundColor = UIColor(red:0.92, green:0.94, blue:0.98, alpha:1)
    }
    
    @IBAction func backCllicked(_ sender: Any) {
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnNextClicked(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "ChooseMailerWizardViewController") as! ChooseMailerWizardViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
