//
//  UpdateMobileNumberViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 9/10/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit
import Alamofire
import SwiftValidator
import TTGSnackbar

import ContactsUI
import PhoneNumberKit


class UpdateMobileNumberViewController: UIViewController, ValidationDelegate{
    
    func validationSuccessful() {
        if isNumberValid{

            print(phoneNoTextField.nationalNumber)
            print(phoneNoTextField.isValidNumber)
            if phoneNoTextField.isValidNumber == true{
                lblError1.isHidden = true
                phoneNoTextField.borderColor = .white
                if Network.reachability.status == .unreachable{
                    AppConfigs.updateUserInterface(view: view)
                }else{

                    self.handleProfileUpdate()
                }
            }else{
                lblError1.isHidden = false
                phoneNoTextField.borderColor = .red
                phoneNoTextField.layer.borderWidth = 1.0
                lblError1.text = "Invalid Mobile Number"
            }
            //            }
        }else{
            AppConfigs.showSnacbar(message: "Wrong Phone Number", textColor: .red)
        }
    }
    
    func validationFailed(_ errors:[(Validatable ,ValidationError)]) {
        // turn the fields to red
        for (field, error) in errors {
            if let field = field as? UITextField {
                field.layer.borderColor = UIColor.red.cgColor
                field.layer.borderWidth = 1.0
            }
            error.errorLabel?.text = error.errorMessage // works if you added labels
            error.errorLabel?.isHidden = false
        }
    }
    
    func validationFailed(errors: [UITextField : ValidationError]) {
        print("Validation FAILED!")
    }
    
    //@IBOutlet weak var txtMobileNo: FPNTextField!
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var lblFirst: UILabel!
    @IBOutlet weak var lblError1: UILabel!
    @IBOutlet weak var phoneNoTextField: PhoneNumberTextField!
    
    var code:String = ""
    var rawNumber: String = ""
    var isNumberValid: Bool = true
    let phoneNumberKit = PhoneNumberKit()
    let validator = Validator()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        validator.registerField(phoneNoTextField, errorLabel: lblError1, rules: [RequiredRule()])
        
        validator.styleTransformers(success:{ (validationRule) -> Void in
            print("here")
            // clear error label
            validationRule.errorLabel?.isHidden = true
            validationRule.errorLabel?.text = ""
            
            if let textField = validationRule.field as? UITextField {
                textField.layer.borderColor = UIColor.white.cgColor
                textField.layer.borderWidth = 0.5
            } else if let textField = validationRule.field as? UITextView {
                textField.layer.borderColor = UIColor.green.cgColor
                textField.layer.borderWidth = 0.5
            }
        }, error:{ (validationError) -> Void in
            print("error")
            validationError.errorLabel?.isHidden = false
            validationError.errorLabel?.text = validationError.errorMessage
            if let textField = validationError.field as? UITextField {
                textField.layer.borderColor = UIColor.red.cgColor
                textField.layer.borderWidth = 1.0
                //textField.textColor = .red
            } else if let textField = validationError.field as? UITextView {
                textField.layer.borderColor = UIColor.red.cgColor
                textField.layer.borderWidth = 1.0
                //textField.textColor = .red
            }
        })
        
        
        
        phoneNoTextField.delegate = self
        
        phoneNoTextField.placeholder = "Mobile No (ex: +1-541-754-3010)"
        phoneNoTextField.isPartialFormatterEnabled = true
        phoneNoTextField.withPrefix = false
        //phoneNoTextField.currentRegion = "US"
        NotificationCenter.default
            .addObserver(self,
                         selector: #selector(statusManager),
                         name: .flagsChanged,
                         object: nil)
        AppConfigs.updateUserInterface(view: view)
        
        // btnUpdate.backgroundColor = UIColor(red:0.07, green:0.82, blue:0.41, alpha:1)
        
        FontAndColorConfigs.setLabelFontSize(labels: [lblFirst], type: FontAndColorConfigs.getTextTypeHeader())
        FontAndColorConfigs.setButtonsTextColorAndFont(buttons: [btnUpdate], color: .white, font: FontAndColorConfigs.proximaSemiBold20!)
        
        lblFirst.text = "Phone Number"
        phoneNoTextField.keyboardType = .phonePad
       
        if AppConfigs.getCurrentUserInfo().phoneNumber != nil{
            
            phoneNoTextField.text = AppConfigs.getCurrentUserInfo().phoneNumber!
        }
        
        setUpMobileUpdateUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        DispatchQueue.main.async {
            self.phoneNoTextField.becomeFirstResponder()
        }
    }
    
    @objc func statusManager(_ notification: Notification) {
        //updateUserInterface()
        AppConfigs.updateUserInterface(view: view)
    }
    
    func setUpMobileUpdateUI() {
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [lblFirst], type: TextType.ListTitle.rawValue)
        
        FontAndColorConfigs.setDynamicButtonsFontSize(buttons: [btnUpdate], type: TextType.ListTitle.rawValue)
        
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [lblFirst], type: TextType.ListSubTitle.rawValue)
        
        phoneNoTextField.underlined()
        phoneNoTextField.font = ProNovaR16
        lblFirst.textColor = self.listSubTitleColor
    }
    
    @IBAction func updateButtonClicked(_ sender: Any) {
        print("Validating...")

        
        validator.validate(self)
        
    }
    
    @IBAction func closeButtonClicked(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    func handleProfileUpdate() {
        print(phoneNoTextField.nationalNumber)
        var myString: String = phoneNoTextField.nationalNumber
        print(myString.prefix(1))
        
        if myString.prefix(1) == "1"{

            let removeCountryCode = myString.remove(at: myString.startIndex)
            print(removeCountryCode)
        }
        
        print(myString)
        let parameters : [String : Any] =  [
            K.APIParameterKey.phone: myString
            ] as [String : Any]
        
        print(parameters)
        let uid = AppConfigs.getCurrentUserInfo().id
        print("uid", uid)
        let urlString = UrlManager.baseURL() + UrlManager.apiString() + "/user/\(String(describing: uid!))/"
        print(urlString)
        
        Alamofire.request(urlString, method: .patch, parameters: parameters, encoding: URLEncoding.httpBody, headers: nil)
            .responseJSON { response in
                switch(response.result) {
                case.success(let data):
                    print("success",data)
                    self.view.activityStopAnimating()
                    let statusCode = response.response?.statusCode
                    
                    if statusCode == 200{
                        let userResponse = try? JSONDecoder().decode(UpdateUserInfoResponse.self, from: response.data!)
                        
                        if userResponse == nil{
                            print("nil")
                            do {
                                
                            }catch let error as NSError{
                                print(error.localizedDescription)
                            }
                            
                        }else{
                            
                            if userResponse?.status == true{
                                AppConfigs.saveCurrentUserInfo(user: (userResponse?.data!)!)
                                print(AppConfigs.getCurrentUserInfo().firstName!)
                                
                                let alertController = UIAlertController(title: userResponse?.message ?? "Information Updated", message:" ", preferredStyle: .alert)
                                //let cancelAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                                let okAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                                    self.dismiss(animated: true, completion: nil)
                                })
                                alertController.addAction(okAction)
                                self.present(alertController, animated: true, completion: nil)
                            }else{
                                self.showAlert(message: userResponse?.message ?? "Something bad happened")
                            }
                            
                        }
                    }
                    else if statusCode == 400{
                        self.showAlert(message: "Some Error Occured!", title: "Sorry!")
                    }
                    else{
                        self.showAlert(message: "Some Error Occured!", title: "Sorry!")
                    }
                    
                case.failure(let error):
                    self.view.activityStopAnimating()
                    print("Not Success",error)
                        self.removeSpinner()
                        let statusCode = response.response?.statusCode
                        self.showMessageForServerError(code: statusCode ?? 500)
                        //AppConfigs.showSnacbar(message: error.localizedDescription, textColor: .red)
                    
                }
        }
        
        
    }
    
}
extension UpdateMobileNumberViewController: UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textField.layer.borderWidth = 0
        textField.layer.borderColor = UIColor(red:0.07, green:0.82, blue:0.41, alpha:1).cgColor
        textField.layer.shadowOffset = CGSize.zero
        textField.layer.shadowColor = UIColor(red:0.87, green:0.99, blue:0.89, alpha:1).cgColor
        textField.layer.shadowOpacity = 1
        textField.layer.shadowRadius = 7
        lblError1.isHidden = true
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        textField.layer.borderWidth = 0
        textField.layer.borderColor = UIColor(red:0.77, green:0.99, blue:0.75, alpha:1).cgColor
        
        textField.layer.shadowColor = UIColor.white.cgColor //UIColor(red:0.87, green:0.99, blue:0.89, alpha:1).cgColor
    }
}
