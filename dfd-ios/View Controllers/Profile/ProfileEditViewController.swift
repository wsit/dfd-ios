//
//  ProfileEditViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 8/30/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit
import Alamofire
import SwiftValidator
import SwiftyJSON


class ProfileEditViewController: UIViewController, ValidationDelegate {
    
    var isNameEdit : Bool = false
    var isEmailEdit : Bool = false
    var isPhoneNoEdit : Bool = false
    var isPasswordEdit : Bool = false
    
    @IBOutlet weak var lblFirst: UILabel!
    @IBOutlet weak var txtFirst: UITextField!
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var lblSecond: UILabel!
    @IBOutlet weak var txtSecond: UITextField!
    @IBOutlet weak var lblError1: UILabel!
    @IBOutlet weak var lblError2: UILabel!
    
    let validator = Validator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default
            .addObserver(self,
                         selector: #selector(statusManager),
                         name: .flagsChanged,
                         object: nil)
        AppConfigs.updateUserInterface(view: view)
        setUpProfileEditUI()
        configureProfileEditView()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        DispatchQueue.main.async {
            self.txtFirst.becomeFirstResponder()
        }
        
    }
    
    @objc func statusManager(_ notification: Notification) {
        //updateUserInterface()
        AppConfigs.updateUserInterface(view: view)
    }
    
    func setUpProfileEditUI() {
        
        FontAndColorConfigs.setButtonsTextColorAndFont(buttons: [btnUpdate], color: .white, font: FontAndColorConfigs.proximaSemiBold20!)
        
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [lblFirst,lblSecond], type: TextType.ListTitle.rawValue)
        
        FontAndColorConfigs.setDynamicButtonsFontSize(buttons: [btnUpdate], type: TextType.ListTitle.rawValue)
        
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [lblFirst, lblSecond], type: TextType.ListSubTitle.rawValue)
        
        txtFirst.underlined()
        txtSecond.underlined()
        txtFirst.font = ProNovaR16
        txtSecond.font = ProNovaR16
        lblFirst.textColor = self.listSubTitleColor
        lblSecond.textColor = self.listSubTitleColor
    }
    
    @IBAction func updateButtonClicked(_ sender: Any) {
        print("Validating...")
        validator.validate(self)
    }
    
    @IBAction func closeButtonClicked(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func configureProfileEditView() {
        
        if isNameEdit == true{
            lblSecond.isHidden = false
            txtSecond.isHidden = false
            
        }else{
            lblSecond.isHidden = true
            txtSecond.isHidden = true
        }
        
        if isNameEdit == true
        {
            lblFirst.text = "First Name"
            txtFirst.placeholder = "enter first name here"
            lblSecond.text = "Last Name"
            txtSecond.placeholder = "enter last name here"
            txtFirst.keyboardType = .default
            txtFirst.text = AppConfigs.getCurrentUserInfo().firstName!
            txtSecond.text = AppConfigs.getCurrentUserInfo().lastName!
            // Validation Rules are evaluated from left to right.
            validator.registerField(txtFirst,errorLabel: lblError1 , rules: [RequiredRule()])
            validator.registerField(txtSecond, errorLabel: lblError2 ,rules: [RequiredRule()])
            
        }
        else if isEmailEdit == true{
            lblFirst.text = "Email"
            txtFirst.placeholder = "enter email here"
            txtFirst.keyboardType = .emailAddress
            txtFirst.text = AppConfigs.getCurrentUserInfo().email!
            validator.registerField(txtFirst, errorLabel: lblError1 ,rules: [RequiredRule(), EmailRule()])
        }
            
        else if isPhoneNoEdit == true{
            lblFirst.text = "Phone Number"
            txtFirst.placeholder = "enter phone number here"
            txtFirst.keyboardType = .phonePad
            txtFirst.text = AppConfigs.getCurrentUserInfo().phoneNumber!
            validator.registerField(txtFirst,errorLabel: lblError1, rules: [RequiredRule(), PhoneNumberRule()])
            
        }
            
        else if isPasswordEdit == true{
            lblFirst.text = "Old Password"
            txtFirst.placeholder = "enter old password here"
            txtFirst.keyboardType = .default
            txtFirst.isSecureTextEntry = true
            validator.registerField(txtFirst,errorLabel: lblError1 ,rules: [RequiredRule(), MinLengthRule(length: 6)])
            
            lblSecond.isHidden = false
            txtSecond.isHidden = false
            
            
            lblSecond.text = "New Password"
            txtSecond.placeholder = "enter new password here"
            txtSecond.keyboardType = .default
            txtFirst.isSecureTextEntry = true
            //txtSecond.text = AppConfigs.getCurrentUserInfo().lastName!
            // Validation Rules are evaluated from left to right.
            validator.registerField(txtFirst,errorLabel: lblError1 , rules: [RequiredRule()])
            validator.registerField(txtSecond, errorLabel: lblError2 ,rules: [RequiredRule()])
            
        }
        
    }
    
    func validationSuccessful() {
        print("Succefull")
        if Network.reachability.status == .unreachable{
            AppConfigs.updateUserInterface(view: view)
        }else{

            self.handleProfileUpdate()
            
        }//       }
    }
    
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        // turn the fields to red
        for (field, error) in errors {
            if let field = field as? UITextField {
                field.layer.borderColor = UIColor.red.cgColor
                field.layer.borderWidth = 1.0
            }
            error.errorLabel?.text = error.errorMessage // works if you added labels
            error.errorLabel?.isHidden = false
        }
    }
    
    
    
    func validationFailed(errors: [UITextField : ValidationError]) {
        print("Validation FAILED!")
    }
    
    
    func handleProfileUpdate() {
        var parameters : [String : Any] = [:]
        if isNameEdit == true
        {
            parameters = [
                K.APIParameterKey.firstName: txtFirst.text!
                , K.APIParameterKey.lastName: txtSecond.text!
                ] as [String : Any]
        }
        else if isEmailEdit == true{
            parameters = [
                K.APIParameterKey.email: txtFirst.text!
                ] as [String : Any]
        }
            
        else if isPhoneNoEdit == true{
            parameters = [
                K.APIParameterKey.phone: txtFirst.text!
                ] as [String : Any]
        }
            
        else if isPasswordEdit == true{
            //            parameters = [
            //                K.APIParameterKey.password: txtFirst.text!
            //                ] as [String : Any]
            parameters = [
                "old_password": txtFirst.text!
                , "password": txtSecond.text!
                ] as [String : Any]
        }
        
        print(parameters)
        let urlString = UrlManager.baseURL() + UrlManager.apiString() + "/user/\(String(describing: AppConfigs.getCurrentUserInfo().id!))/"
        print(urlString)
        
        Alamofire.request(urlString, method: .patch, parameters: parameters, encoding: URLEncoding.httpBody, headers: nil)
            .responseJSON { response in
                //Alamofire.request(urlString, method: .patch, parameters: parameters, encoding: JSONEncoding.default, headers: nil)
                //   .responseJSON { response in
                
                switch(response.result) {
                case.success(let data):
                    print("success",data)
                    self.view.activityStopAnimating()
                    let statusCode = response.response?.statusCode
                    
                    if statusCode == 200{
                        var userResponse = try? JSONDecoder().decode(UpdateUserInfoResponse.self, from: response.data!)
                        
                        if userResponse == nil{
                            print("nil")
                            do {
                                //                                let datas = try JSON(data: response.data!)
                                //                                let json = datas["data"].dictionaryValue
                                //                                let id = json["id"]?.intValue // as? Int
                                //                                let lastLogin = json["last_login"]?.stringValue //as? String
                                //                                let firstName = json["first_name"]?.stringValue //as? String
                                //                                let lastName = json["last_name"]?.stringValue //as? String
                                //                                let email = json["email"]?.stringValue //as? String
                                //                                let phoneNumber = json["phone_number"]?.stringValue //as? String
                                //                                let invitedBy = json["invited_by"]?.intValue
                                //                                let photo = json["photo"]?.stringValue //as? String
                                //                                let isActive = json["is_active"]?.boolValue //as? String
                                //                                let isAdmin = json["is_admin"]?.boolValue //as? String.stringValue //as? String
                                //                                let createdAt = json["created_at"]?.stringValue //as? String
                                //                                let updatedAt = json["updated_at"]?.stringValue //as? String
                                //                                let upgradeInfo = json["upgrade_info"]?.dictionaryValue //as? String
                                //                                
                                //                                let upgrade = upgradeInfo?["upgrade"]?.boolValue
                                //                                let amount = upgradeInfo?["amount"]?.intValue
                                //                                let powerTrace = upgradeInfo?["power_trace"]?.doubleValue
                                //                                let ownerInfo = upgradeInfo?["owner_info"]?.doubleValue
                                //                                 
                                //                                let currentUser : CurrentUserInfoResponse = CurrentUserInfoResponse(from: <#Decoder#>)
                                //                                //(id: id, lastLogin: lastLogin, firstName: firstName, lastName: lastName, email: email, phoneNumber: phoneNumber, invitedBy: invitedBy, photo: photo, isActive: isActive, isAdmin: isAdmin, upgradeInfo: UpgradeInfo(upgrade: upgrade, amount: amount, powerTrace: powerTrace, ownerInfo: ownerInfo), createdAt: createdAt, updatedAt: updatedAt)
                                //                                
                                //                                print(currentUser.email!)
                                //                                AppConfigs.saveCurrentUserInfo(user: currentUser)
                                //                                print(AppConfigs.getCurrentUserInfo().firstName!)
                                //
                                //                                let alertController = UIAlertController(title: "Information Updated", message:" ", preferredStyle: .alert)
                                //                                //let cancelAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                                //                                let okAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                                //                                    self.dismiss(animated: true, completion: nil)
                                //                                })
                                //                                alertController.addAction(okAction)
                                //                                self.present(alertController, animated: true, completion: nil)
                                
                                
                            }catch let error as NSError{
                                
                            }
                            
                        }else{
                            if userResponse?.status == true{
                                AppConfigs.saveCurrentUserInfo(user: (userResponse?.data!)!)
                                print(AppConfigs.getCurrentUserInfo().firstName!)
                                
                                let alertController = UIAlertController(title: userResponse?.message ?? "Information Updated", message:" ", preferredStyle: .alert)
                                //let cancelAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                                let okAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                                    self.dismiss(animated: true, completion: nil)
                                })
                                alertController.addAction(okAction)
                                self.present(alertController, animated: true, completion: nil)
                            }else{
                                self.showAlert(message: userResponse?.message ?? "Something bad happened")
                            }
                            
//                            print(userResponse?.status!)
//                            print(userResponse?.data?.email!)
//                            AppConfigs.saveCurrentUserInfo(user: (userResponse?.data)!)
//                            print(AppConfigs.getCurrentUserInfo().firstName!)
//
//                            let alertController = UIAlertController(title: "Information Updated", message:" ", preferredStyle: .alert)
//                            //let cancelAction = UIAlertAction(title: "OK", style: .default, handler: nil)
//                            let okAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
//                                self.dismiss(animated: true, completion: nil)
//                            })
//                            alertController.addAction(okAction)
//                            self.present(alertController, animated: true, completion: nil)
                        }
                        
                        
                        
                    }
                    else if statusCode == 400{
                        //let errorResponse = try? JSONDecoder().decode(SignUpErrorResponse.self, from: response.data!)
                        //self.showAlert(message: (errorResponse?.email?[0])!, title: "Sorry!")
                        self.showAlert(message: "Some Error Occured!", title: "Sorry!")
                    }
                    else{
                        self.showAlert(message: "Some Error Occured!", title: "Sorry!")
                    }
                    
                case.failure(let error):
                    self.view.activityStopAnimating()
                    print("Not Success",error)
                        self.removeSpinner()
                        let statusCode = response.response?.statusCode
                        self.showMessageForServerError(code: statusCode ?? 500)
                        //AppConfigs.showSnacbar(message: error.localizedDescription, textColor: .red)
                    
                }
        }
        
        
    }
}
