//
//  ProfileViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 5/3/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit
import Alamofire

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var btnName: UIButton!
    @IBOutlet weak var lblSeperator: UILabel!
    @IBOutlet weak var btnEmail: UIButton!
    @IBOutlet weak var lblSeperatorTwo: UILabel!
    @IBOutlet weak var lblSeperatorThree: UILabel!
    @IBOutlet weak var btnPassword: UIButton!
    @IBOutlet weak var btnMobile: UIButton!
    @IBOutlet weak var lblSeperatorFour: UILabel!
    @IBOutlet weak var btnWallet: UIButton!
    @IBOutlet weak var lblAmount: UILabel!
    
    let imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.delegate = self
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let leftBarItem = UIBarButtonItem(image: UIImage(named: "leftarrow"), style: .plain, target: self, action: #selector(toggleSideMenu))
        leftBarItem.tintColor = .black
        navigationItem.leftBarButtonItem = leftBarItem
        
        self.title = "My Profile"
        
        setUpProfileUI()
        if Network.reachability.status == .unreachable{
            //AppConfigs.updateUserInterface(view: view)
            AppConfigs.showSnacbar(message: "Option not available when offline", textColor: .red)
        }else{
            let url : String = UrlManager.getCurrentUserInfoURL()
            self.getCurrentUserInfo(urlString: url)
            
        }
        
        self.setUpNavigationBar() 
    }
    
    func setUpProfileUI() {
        
        profileImageView.borderColor = self.profileBlueRoundColor
        profileImageView.borderWidth = 5
        lblSeperator.backgroundColor = self.profileSeperatorColor
        lblSeperatorTwo.backgroundColor = self.profileSeperatorColor
        lblSeperatorThree.backgroundColor = self.profileSeperatorColor
        lblSeperatorFour.backgroundColor = self.profileSeperatorColor
        lblAmount.font = ProNovaSB20
        lblAmount.layer.masksToBounds = true
        
        FontAndColorConfigs.setDynamicButtonsFontSize(buttons: [btnName, btnEmail, btnMobile, btnPassword, btnWallet], type: TextType.ListTitle.rawValue)
        
        let user = AppConfigs.getCurrentUserInfo()
        let name = (user.firstName ?? "") + " " +  (user.lastName ?? "")
        let amount = user.upgradeInfo?.amount
        btnName.setTitle(name, for: .normal)
        btnEmail.setTitle(user.email, for: .normal)
        btnPassword.setTitle("**********", for: .normal)
        btnMobile.setTitle(user.phoneNumber ?? "", for: .normal)
        btnMobile.setTitle(user.phoneNumber ?? "", for: .normal)
        btnWallet.setTitle("$\(String(describing: (amount) ?? 0.0))", for: .normal)
        
        lblAmount.text = "$\(user.upgradeInfo?.amount ?? 0.0)"
        
        if user.photo != nil{
            if user.photo != ""{
                self.showPicture(url: user.photo!, imageView: self.profileImageView)
            }else{
                print("photo is empty")
            }
        }else
        {
            print("photo is nil")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func setupNavForDefaultMenu() {
        // Revert navigation bar translucent style to default
        //navigationBarNonTranslecentStyle()
        // Update side menu after reverted navigation bar style
        sideMenuManager?.instance()?.menu?.isNavbarHiddenOrTransparent = true
        navigationItem.hidesBackButton = true
    }
    
    @objc func toggleSideMenu() {
        sideMenuManager?.toggleSideMenuView()
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        guard let signInVC = storyBoard.instantiateViewController(withIdentifier: "SMNavigationController") as? SMNavigationController  else
        { return  }
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        signInVC.modalPresentationStyle = .fullScreen
        present(signInVC, animated: true, completion: nil)
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func amountClicked(_ sender: Any) {
        if AppConfigs.getCurrentUserInfo().isAdmin == true{
            if AppConfigs.getCurrentUserInfo().thinker == true{
                
                let main = UIStoryboard(name: "Settings", bundle: nil)
                let vc = main.instantiateViewController(withIdentifier: "RechargeForIAPViewController") as! RechargeForIAPViewController
                //self.navigationController?.pushViewController(vc, animated: true)
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            }else{
                
                let main = UIStoryboard(name: "Main", bundle: nil)
                let vc = main.instantiateViewController(withIdentifier: "RechargeViewController") as! RechargeViewController
                //self.navigationController?.pushViewController(vc, animated: true)
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            }
            //            let alertController = UIAlertController(title: "Please go to web to manage your account.", message: "", preferredStyle: .alert)
            //            let OKAction = UIAlertAction(title: "Ok", style: .default) { (alert) in
            //
            //            }
            //            alertController.addAction(OKAction)
            //            self.present(alertController, animated: true, completion: nil)
            
        }else{
            AppConfigs.showSnacbar(message: ErrorMessage.InsufficientBalance.rawValue, textColor: .red)
        }
        
        //        if AppConfigs.getCurrentUserInfo().isAdmin == true{
        //
        //            let alertController = UIAlertController(title: "Please go to web to manage your account.", message: "", preferredStyle: .alert)
        //            let OKAction = UIAlertAction(title: "Ok", style: .default) { (alert) in
        //
        //            }
        //            alertController.addAction(OKAction)
        //            self.present(alertController, animated: true, completion: nil)
        //
        //        }else{
        //            AppConfigs.showSnacbar(message: ErrorMessage.InsufficientBalance.rawValue, textColor: .red)
        //        }
        
    }
    
    @IBAction func editNameClicked(_ sender: Any) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "MyProfile", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "ProfileEditViewController") as! ProfileEditViewController
        vc.isNameEdit = true
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func editEmailClicked(_ sender: Any) {
        //        let mainStoryboard: UIStoryboard = UIStoryboard(name: "MyProfile", bundle: nil)
        //        let vc = mainStoryboard.instantiateViewController(withIdentifier: "ProfileEditViewController") as! ProfileEditViewController
        //        vc.isEmailEdit = true
        //        vc.modalPresentationStyle = .fullScreen
        //        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func editPhoneNumberClicked(_ sender: Any) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "MyProfile", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "UpdateMobileNumberViewController") as! UpdateMobileNumberViewController
        //vc.isPhoneNoEdit = true
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func walletButtonClicked(_ sender: Any) {
        
        if AppConfigs.getCurrentUserInfo().isAdmin == true{
            
            
            if AppConfigs.getCurrentUserInfo().thinker == true{
                
                let main = UIStoryboard(name: "Settings", bundle: nil)
                let vc = main.instantiateViewController(withIdentifier: "RechargeForIAPViewController") as! RechargeForIAPViewController
                //self.navigationController?.pushViewController(vc, animated: true)
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            }else{
                
                let main = UIStoryboard(name: "Main", bundle: nil)
                let vc = main.instantiateViewController(withIdentifier: "RechargeViewController") as! RechargeViewController
                //self.navigationController?.pushViewController(vc, animated: true)
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            }
            //            let alertController = UIAlertController(title: "Please go to web to manage your account.", message: "", preferredStyle: .alert)
            //            let OKAction = UIAlertAction(title: "Ok", style: .default) { (alert) in
            //
            //            }
            //            alertController.addAction(OKAction)
            //            self.present(alertController, animated: true, completion: nil)
            
        }else{
            AppConfigs.showSnacbar(message: ErrorMessage.InsufficientBalance.rawValue, textColor: .red)
        }
        
    }
    
    @IBAction func editPasswordChange(_ sender: Any) {
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "MyProfile", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "ProfileEditViewController") as! ProfileEditViewController
        vc.isPasswordEdit = true
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func profilePicChangeButtonClicked(_ sender: Any) {
        
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = false
            imagePicker.modalPresentationStyle = .fullScreen
            present(imagePicker, animated: true, completion: nil)
        }
        
    }
    
    
    func getCurrentUserInfo(urlString:String) {
        print(urlString)
        //self.showSpinner(onView: self.view)
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success",data)
                let statusCode = response.response?.statusCode
                print(statusCode!)
                if statusCode == 200{
                    
                    DispatchQueue.main.async {
                        
                        
                        let currentUser = try? JSONDecoder().decode(CurrentUserInfoResponse.self, from: response.data!)
                        AppConfigs.saveCurrentUserInfo(user: currentUser!)
                        print(AppConfigs.getCurrentUserInfo().email!)
                        
                        self.setUpProfileUI()
                        
                    }
                }
                else if statusCode == 403{
                }
                else if statusCode == 401{
                    self.removeSpinner()
                    
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                   // AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                
                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
                //                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                //                let vc = mainStoryboard.instantiateViewController(withIdentifier: "WelcomeScreenViewController") as! WelcomeScreenViewController
                //                UIApplication.shared.keyWindow?.rootViewController = vc
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
    func handleUploadImage(image: UIImage) {
        
        self.displayActivityIndicator(shouldDisplay: true)
        //self.showSpinner(onView: self.profileImageView)
        let parameters: [String:Any] = [:]
        let headers: HTTPHeaders = [
            "Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken(),
            "Content-type": "multipart/form-data"
        ]
        let imageData = image.jpegData(compressionQuality: 0.50)
        print(image, imageData!)
        let url =  UrlManager.baseURL() + UrlManager.apiString() + "/user/\(String(describing: AppConfigs.getCurrentUserInfo().id!))/"
        print(url)
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(imageData!, withName: "photo", fileName: "image.jpg", mimeType: "image/jpg")
        }, usingThreshold: UInt64.init(), to:url, method: .patch, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    self.removeSpinner()
                    self.displayActivityIndicator(shouldDisplay: false)
                    if let err = response.error{
                        //onError?(err)
                        self.removeSpinner()
                        AppConfigs.showSnacbar(message: err.localizedDescription, textColor: .red)
                        return
                    }
                    DispatchQueue.main.async {
                        //self.activityIndicator.stopAnimating()
                        self.removeSpinner()
                        self.displayActivityIndicator(shouldDisplay: false)
                        print(response)
                        print(response.data!)
                        self.profileImageView.image = image
                        //self.dismiss(animated: true, completion: nil)
                        let currentUser = try? JSONDecoder().decode(CurrentUserInfoResponse.self, from: response.data!)
                        
                        AppConfigs.saveCurrentUserInfo(user: currentUser!)
                        
                        let alert = UIAlertController(title: "Photo Updated", message: " ", preferredStyle: .alert)
                        
                        let okAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                            print("Ok Clicked")
                            self.removeSpinner()
                            self.displayActivityIndicator(shouldDisplay: false)
                            self.profileImageView.image = image
                        })
                        alert.addAction(okAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                //onError?(error)
                //self.activityIndicator.stopAnimating()
                
                self.removeSpinner()
                AppConfigs.showSnacbar(message: error.localizedDescription, textColor: .red)
            }
            self.removeSpinner()
        }
    }
    
}

extension ProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        
         
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        self.profileImageView.image = image
        self.handleUploadImage(image: image)
    }
    
}
