//
//  MyTeamViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 5/3/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit
import LNSideMenu
import Alamofire
class MyTeamViewController: UIViewController {
    
    @IBOutlet weak var membersTableView: UITableView!
    @IBOutlet weak var addMemberButton: UIButton!
    
    var items = ["Bran Stack", "Jon Snow", "Daynaris Targerian", "Tyrion Lanaster", "Arya Stark", "Khal Drogo", "The Knight King"]
    var parentVc : String = ""
    
    var users: [UnregisteredInvitation] = []
    var unregisteredUsers: [UnregisteredInvitation] = []
    var allTeamMembers: [UnregisteredInvitation] = []
    
    var jsonResponse: MyTeamResponse?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: "NewMemberTableViewCell", bundle: nil)
        membersTableView.register(nib, forCellReuseIdentifier: "NewMemberTableViewCell")
        self.membersTableView.tableFooterView = UIView()
        //self.membersTableView.estimatedRowHeight = 80.0
        //self.membersTableView.rowHeight = UITableView.automaticDimension
        
        print(parentVc)
        //addMemberButton.backgroundColor = UIColor(red:0.07, green:0.82, blue:0.41, alpha:1)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // FIXME: Remove code below if u're using your own menu
        setupNavForDefaultMenu()
        
        // Add left bar button item
        let leftBarItem = UIBarButtonItem(image: UIImage(named: "leftarrow"), style: .plain, target: self, action: #selector(toggleSideMenu))
        leftBarItem.tintColor = .white
        navigationItem.leftBarButtonItem = leftBarItem
        
        self.title = "My Team"
        ///api/teams/?limit=10&offset=0

        if Network.reachability.status == .unreachable{
            AppConfigs.updateUserInterface(view: view)
        }else{

            allTeamMembers.removeAll()
            users.removeAll()
            unregisteredUsers.removeAll()
            
            let url : String = UrlManager.getTeamListURL()
            
            print(url)
            self.showSpinner(onView: view)
            getAllTeamMembers(urlString: url)
            
        }//        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func setupNavForDefaultMenu() {
        // Revert navigation bar translucent style to default
        //navigationBarNonTranslecentStyle()
        // Update side menu after reverted navigation bar style
        sideMenuManager?.instance()?.menu?.isNavbarHiddenOrTransparent = true
        navigationItem.hidesBackButton = true
        
    }
    
    @objc func toggleSideMenu() {
        sideMenuManager?.toggleSideMenuView()
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        guard let signInVC = storyBoard.instantiateViewController(withIdentifier: "SMNavigationController") as? SMNavigationController  else
        { return  }
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        
        present(signInVC, animated: true, completion: nil)
        
    }
    
    func getAllTeamMembers(urlString:String)  {
        
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success",data)
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    
                    let teamMembers = try? JSONDecoder().decode(MyTeamResponse.self, from: response.data!)
                    self.jsonResponse = teamMembers
                    
                    let userList = teamMembers?.users
                    
                    let pendingUserList = teamMembers?.unregisteredInvitations
                    self.allTeamMembers = userList! + pendingUserList!
                    
                    var items: [UnregisteredInvitation] = self.allTeamMembers
                    items.append(contentsOf: (teamMembers?.users)!)
                    
                    DispatchQueue.main.async {
                        print("allTeamMembers------------\n",self.allTeamMembers)
                        self.removeSpinner()//self.view.activityStopAnimating()
                        self.membersTableView.reloadData()
                        
                        //  self.perform(#selector(self.loadTable), with: nil, afterDelay: 1.0)
                    }
                }
                    
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                   // AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
                self.removeSpinner()
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                //AppConfigs.showSnacbar(message: error.localizedDescription, textColor: .red)
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
    
    @objc func loadTable(tableview:UITableView) {
        self.membersTableView.reloadData()
    }
    
}

extension MyTeamViewController: UITableViewDataSource {
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if allTeamMembers.count == 0 {
            tableView.setEmptyMessage("Nothing yet to show")
        } else {
            tableView.restore()
        }
        return allTeamMembers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewMemberTableViewCell", for: indexPath) as! NewMemberTableViewCell
        
        cell.memberNameLabel?.textColor = UIColor(red:0.2, green:0.3, blue:0.41, alpha:1)
        
        FontAndColorConfigs.setLabelFontSize(labels: [cell.memberNameLabel], type: FontAndColorConfigs.getTextTypeHeader())
        
        if allTeamMembers.count > indexPath.row{
            let member = allTeamMembers[indexPath.row]
            
            if member.firstName != nil || member.lastName != nil{
                var fullName: String = member.firstName! + " " + member.lastName!
                
                //cell.memberImageView.setImage(string: fullName, color: UIColor.colorHash(name: fullName), circular: true, stroke: false)
                
                if member.status == 0{
                    cell.pendingImageView.isHidden = false
                    //fullName = fullName + " (pending)"
                    //                    let string_to_color = "pending"
                    //                    let range = (fullName as NSString).range(of: string_to_color)
                    //                    let attribute = NSMutableAttributedString.init(string: fullName)
                    //                    attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red , range: range)
                    //cell.memberNameLabel.attributedText = attribute
                    cell.memberNameLabel.text = fullName
                    
                }else{
                    cell.memberNameLabel.text = fullName
                }
            }else{
                if member.status == 0{
                    cell.pendingImageView.isHidden = false
                    let email = member.email ?? "" + " (pending)"
                    let string_to_color = "pending"
                    
                    let range = (email as NSString).range(of: string_to_color)
                    
                    let attribute = NSMutableAttributedString.init(string: email)
                    attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red , range: range)
                    // cell.memberNameLabel.attributedText = attribute
                    cell.memberNameLabel.text = member.email
                }else{
                    cell.memberNameLabel.text = member.email
                }
            }
        }
        
        return cell
    }
    
}

extension MyTeamViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        //        if (editingStyle == .delete) {
        //            items.remove(at: indexPath.row)
        //            tableView.deleteRows(at: [indexPath], with: .fade)
        //        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let backView = UIView(frame: CGRect(x: 0, y: 0, width: 90, height: 75))
        backView.backgroundColor = .white
        
        let myImage = UIImageView(frame: CGRect(x: 10, y: backView.frame.size.height/2 - 20, width:40 , height: 40))//backView.frame.size.width/2 - 20
        myImage.image = UIImage(named: "trash")
        backView.addSubview(myImage)
        
        let imgSize: CGSize = tableView.frame.size
        UIGraphicsBeginImageContextWithOptions(imgSize, false, UIScreen.main.scale)
        let context = UIGraphicsGetCurrentContext()
        backView.layer.render(in: context!)
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        let delete = UITableViewRowAction(style: .destructive, title: "           ") { (action, indexPath) in
            print("Delete")
        }
        
        delete.backgroundColor = UIColor(patternImage: newImage)
        
        return [delete]
    }
    
}
