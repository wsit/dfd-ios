//
//  MainNavigationController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 10/18/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import Firebase

class MainNavigationController: UIViewController {
    
    var count : Int = 0
    
    let manager = Alamofire.SessionManager.default
    
    override func viewDidLoad() {
        super.viewDidLoad()
        manager.session.configuration.timeoutIntervalForRequest = 30
        
        view.backgroundColor = .white
        
        print(AppConfigs.getSavedAccessToken())
        
        if AppConfigs.getSavedAccessToken() != "" {
            if Network.reachability.status == .unreachable{
                //AppConfigs.updateUserInterface(view: view)
                AppConfigs.showSnacbar(message: "Option not available when offline", textColor: .red)
                perform(#selector(showLoginController), with: nil, afterDelay: 0.01)
            }else{
                
                if Network.reachability.status == .unreachable{
                    //AppConfigs.updateUserInterface(view: view)
                    AppConfigs.showSnacbar(message: "Option not available when offline", textColor: .red)
                    perform(#selector(showLoginController), with: nil, afterDelay: 0.01)
                }else{
                    
                    let deviceID = UIDevice.current.identifierForVendor!.uuidString
                    print("deviceID----", deviceID)
                    var firebaseToken: String  = ""
                    
                    let version:String = Bundle.main.infoDictionary!["CFBundleShortVersionString"]! as! String
                    let build:String = Bundle.main.infoDictionary!["CFBundleVersion"]! as! String
                    print(version)
                    print(build)
                    print("---------ACCESS TOKEN AVAILABLE---------")
                    InstanceID.instanceID().instanceID { (result, error) in
                        if let error = error {
                            print("Error fetching remote instange ID: \(error)")
                        } else if let result = result {
                            print("Remote instance ID token: \(result.token)")
                            print("Remote instance ID: \(result.instanceID)")
                            firebaseToken = result.instanceID
                            //self.registerDevice(fid: result.token)
                        }
                    }
                    
                    let url : String = UrlManager.getCurrentUserInfoURL()
                    let pushUrl : String = url + "?device_type=ios&firebase_token=\(AppConfigs.getSavedFcmToken())&device_id=\(deviceID)&version=\(version).0.\(build)"
                    print("pushUrl---",pushUrl)
                    self.getCurrentUserInfo(urlString: pushUrl)
                    
                }
            }
        }else{
            //sleep(1)
            perform(#selector(showLoginController), with: nil, afterDelay: 0.01)
        }
    }
    
    @objc func showLoginController() {
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "WelcomeScreenViewController") as! WelcomeScreenViewController
        UIApplication.shared.keyWindow?.rootViewController = vc
        
    }
    
    func getCurrentUserInfo(urlString:String) {
        print(urlString)
        self.showSpinnerWithText(onView: self.view)
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        
        manager.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success",data)
                let statusCode = response.response?.statusCode
                print(statusCode!)
                if statusCode == 200{
                    
                    DispatchQueue.main.async {
                        
                        
                        let currentUser = try? JSONDecoder().decode(CurrentUserInfoResponse.self, from: response.data!)
                        if currentUser != nil{

                            AppConfigs.saveCurrentUserInfo(user: currentUser!)
                            print(AppConfigs.getCurrentUserInfo().email!)
                            print("---------ACCESS TOKEN AVAILABLE---------")

                            print(AppConfigs.getCurrentUserInfo().upgradeInfo?.plan?.planName)
                            
                            let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
                            guard let vc = storyBoard.instantiateViewController(withIdentifier: "SMNavigationController") as? SMNavigationController  else
                            { return  }
                            vc.modalPresentationStyle = .fullScreen
                            self.present(vc, animated: true, completion: nil)
                        }else{
                            
                            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = mainStoryboard.instantiateViewController(withIdentifier: "WelcomeScreenViewController") as! WelcomeScreenViewController
                            UIApplication.shared.keyWindow?.rootViewController = vc
                            
                        }
                        // UIApplication.shared.keyWindow?.rootViewController = vc
                    }
                }
                else if statusCode == 403{
                    self.refreshCurrentToken()
                }
                else if statusCode == 401{
                    self.removeSpinner()
                    self.refreshCurrentToken()
                    
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                    self.refreshCurrentToken()
                }
                
            case.failure(let error):
                print("Not Success",error)
                self.removeSpinner()
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                //AppConfigs.showSnacbar(message: error.localizedDescription, textColor: .red)
                
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "WelcomeScreenViewController") as! WelcomeScreenViewController
                UIApplication.shared.keyWindow?.rootViewController = vc
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
    func refreshCurrentToken(){
        self.showSpinnerWithText(onView: self.view)
        print("---------ACCESS TOKEN NOT AVAILABLE---------")

        //let refToken =  AppConfigs.getSavedRefreshToken()
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        let parameters = [
            K.APIParameterKey.clientID: UrlManager.getClientID(),
            K.APIParameterKey.clientSecret: UrlManager.getClientSecret(),
            K.APIParameterKey.grantType: "refresh_token",
            "refresh_token": AppConfigs.getSavedRefreshToken()
        ]
        print(parameters)
        
        let url = "http://3.83.112.37:8000/o/token/"
        print("expired...",url)
        manager.request(url, method: .post, parameters: parameters as Parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
            case.success(let data):
                print("success refresh",data)
                
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    
                    DispatchQueue.main.async {
                        let currentUser = try? JSONDecoder().decode(CurrentUserInfoResponse.self, from: response.data!)
                        AppConfigs.saveCurrentUserInfo(user: currentUser!)
                        print(AppConfigs.getCurrentUserInfo().email!)
                        print(AppConfigs.getCurrentUserInfo().upgradeInfo?.plan?.planName)
                        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
                        guard let vc = storyBoard.instantiateViewController(withIdentifier: "SMNavigationController") as? SMNavigationController  else
                        { return  }
                        vc.modalPresentationStyle = .fullScreen
                        self.present(vc, animated: true, completion: nil)
                        // UIApplication.shared.keyWindow?.rootViewController = vc
                    }
                }
                else if statusCode == 403{
                    
                    if self.count == 3 || self.count > 1 {
                        self.count = 0
                        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = mainStoryboard.instantiateViewController(withIdentifier: "WelcomeScreenViewController") as! WelcomeScreenViewController
                        UIApplication.shared.keyWindow?.rootViewController = vc
                    }else{
                        self.count = self.count + 1
                        self.refreshCurrentToken()
                    }
                    
                }
                else{
                    self.removeSpinner()
                    //AppConfigs.showSnacbar(message: "Status code not 200", textColor: .red)
                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "WelcomeScreenViewController") as! WelcomeScreenViewController
                    UIApplication.shared.keyWindow?.rootViewController = vc
                    
                }
                
            case.failure(let error):
                print("Not Success",error)
                self.removeSpinner()
                let statusCode = response.response?.statusCode
               // self.showMessageForServerError(code: statusCode ?? 500)
                //AppConfigs.showSnacbar(message: error.localizedDescription, textColor: .red)
                
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "WelcomeScreenViewController") as! WelcomeScreenViewController
                UIApplication.shared.keyWindow?.rootViewController = vc
                
            }
        }
        
    }
    
}

