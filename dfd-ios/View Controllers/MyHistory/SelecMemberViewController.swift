//
//  SelecMemberViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 12/12/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit
import SelectionList
import Alamofire


class SelecMemberViewController: UIViewController {
    
    @IBOutlet weak var selectionList: SelectionList!
    @IBOutlet weak var lblMyTitle: UILabel!
    @IBOutlet weak var lblSeperator: UILabel!
    @IBOutlet weak var lblTeamHouzesTItle: UILabel!
    @IBOutlet weak var MyList: SelectionList!
    @IBOutlet weak var btnDone: UIButton!
    
    var users: [UnregisteredInvitation] = []
    var unregisteredUsers: [UnregisteredInvitation] = []
    var allTeamMembers: [UnregisteredInvitation] = []
    var jsonResponse: MyTeamResponse?
    
    var selectedMember: UnregisteredInvitation?
    
    var membersIDArray : [Int] = []
    var selectedMembersID : [Int] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [lblTeamHouzesTItle, lblMyTitle], type: TextType.MainTitle.rawValue)
        
        //selectionList.items = []//"One", "Two", "Three", "Four", "Five"
        //selectionList.selectedIndexes = [0]
        
        if Network.reachability.status == .unreachable{
            AppConfigs.updateUserInterface(view: view)
        }else{

            allTeamMembers.removeAll()
            users.removeAll()
            unregisteredUsers.removeAll()
            
            let url : String = UrlManager.getTeamListURL()
            
            print(url)
            self.showSpinner(onView: view)
            getTeamMembers(urlString: url)
            
        }//        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
    }
    
    @objc func selectionChanged() {
        print(selectionList.selectedIndexes)
    }
    
    @objc func selectionChangedForMyList() {
        print(MyList.selectedIndexes)
    }
    
    @IBAction func crossButtonClicked(_ sender: Any) {
        globalIsFilterSelected = false
        isFilteredList = false
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneButtonClicked(_ sender: Any) {

        if MyList.selectedIndexes.count != 0{
            let user = AppConfigs.getCurrentUserInfo()
            print(user.id!)
            self.selectedMembersID.append(user.id!)
         }
        
        if selectionList.selectedIndexes.count > 0{
            
            for index in selectionList.selectedIndexes{
                let memberID = self.membersIDArray[index]
                print("membersID", memberID)
                self.selectedMembersID.append(memberID)
            }
            print(self.selectedMembersID)
        }
        
        if self.selectedMembersID.count == 0{
            //btnDone.isUserInteractionEnabled = false
            AppConfigs.showSnacbar(message: "Please select minimum one individual", textColor: .black)
         }else{
            //btnDone.isUserInteractionEnabled = true
            globalIsFilterSelected = true
            isFilteredList = true
            globalSelectedMembersID = self.selectedMembersID
            print(globalSelectedMembersID)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    func getTeamMembers(urlString:String)  {
        
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success",data)
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    let teamMembers = try? JSONDecoder().decode(MyTeamResponse.self, from: response.data!)
                    self.jsonResponse = teamMembers
                    
                    let userList = teamMembers?.users
                    print("userList------\n",userList!)
                    
                    self.allTeamMembers = userList! //+ pendingUserList!
                    print("allTeamMembers-----\n", self.allTeamMembers)
                    
                    DispatchQueue.main.async {
                        
                        print("allTeamMembers------------\n",self.allTeamMembers)
                        self.removeSpinner()
                        self.selectionList.items.removeAll()
                        
                        for member in self.allTeamMembers{
                            
                            let name = (member.firstName ?? "") + " " + (member.lastName ?? "")
                            let id = member.id
                            
                            if member.firstName ?? "" != AppConfigs.getCurrentUserInfo().firstName  && member.lastName ?? "" != AppConfigs.getCurrentUserInfo().lastName  {
                                
                                self.selectionList.items.append(name)
                            }
                            
                            if id != nil{
                                
                                if id != AppConfigs.getCurrentUserInfo().id{

                                    self.membersIDArray.append(id!)
                                }else{
                                    print("This is me")
                                }
                            }
                        }

                        var selectedIndexes : [Int] = []
                        if globalSelectedMembersID.count > 0{
                            
                            for (index,id) in self.membersIDArray.enumerated(){
                                
                                for gid in globalSelectedMembersID{
                                    
                                    if gid == id{
                                        selectedIndexes.append(index)
                                        //self.selectionList.selectedIndexes.append(index)
                                    }
                                    
                                    if gid == AppConfigs.getCurrentUserInfo().id {
                                        self.MyList.selectedIndexes = [0]
                                    }
                                }
                            }
                        }
                        print(selectedIndexes.count)
                        
                        self.selectionList.isUserInteractionEnabled = true
                        self.selectionList.selectedIndexes = selectedIndexes // [0,1]//selectedIndexes

                        self.selectionList.tableView.tableFooterView = UIView()
                        self.selectionList.addTarget(self, action: #selector(self.selectionChanged), for: .valueChanged)
                        self.selectionList.setupCell = { (cell: UITableViewCell, _: Int) in
                            cell.textLabel?.textColor = .gray
                            FontAndColorConfigs.setDynamicLabelFontSize(labels: [cell.textLabel!], type: TextType.ListTitle.rawValue)
                        }
                        
                        self.MyList.items = ["Myself" ]
                        self.MyList.isUserInteractionEnabled = true
                        self.MyList.tableView.tableFooterView = UIView()
                        //self.MyList.selectedIndexes = [0]
                        self.MyList.addTarget(self, action: #selector(self.selectionChangedForMyList), for: .valueChanged)
                        self.MyList.setupCell = { (cell: UITableViewCell, _: Int) in
                            cell.textLabel?.textColor = .gray
                            FontAndColorConfigs.setDynamicLabelFontSize(labels: [cell.textLabel!], type: TextType.ListTitle.rawValue)
                        }
                        //self.selectionList.tableView.reloadData()
                        
                    }
                }
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
                self.removeSpinner()
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)

                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
             }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
    
}
