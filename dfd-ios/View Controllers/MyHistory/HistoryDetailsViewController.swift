//
//  HistoryDetailsViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 6/21/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class HistoryDetailsViewController: UIViewController {
    
    //    @IBOutlet weak var lblVertialLine: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDateTitle: UILabel!
    @IBOutlet weak var lblTotalDistanceTitle: UILabel!
    @IBOutlet weak var lblTotalDistance: UILabel!
    @IBOutlet weak var distanceTV: UITableView!
    @IBOutlet weak var lblVisitedPlacesTitle: UILabel!
    
    let kCellIdentifier = "PropertyAddressTableViewCell"
    
    var allDrivedProperties: [ListedProperty] = []
    var historyID : Int = 0
    var parentString: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpNavigationBar()
        let nib = UINib(nibName: "PropertyAddressTableViewCell", bundle: nil)
        distanceTV.register(nib, forCellReuseIdentifier: kCellIdentifier)
        self.distanceTV.tableFooterView = UIView()
        lblDateTitle.textColor = UIColor(red:0.44, green:0.5, blue:0.58, alpha:1)
        lblTotalDistanceTitle.textColor = UIColor(red:0.44, green:0.5, blue:0.58, alpha:1)
        lblDate.textColor = UIColor(red:0.2, green:0.28, blue:0.36, alpha:1)
        lblTotalDistance.textColor = UIColor(red:0.2, green:0.28, blue:0.36, alpha:1)
        lblVisitedPlacesTitle.textColor = UIColor(red:0.44, green:0.5, blue:0.58, alpha:1)
        
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [lblDate, lblTotalDistance], type: TextType.MainTitle.rawValue)
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [lblDateTitle, lblTotalDistanceTitle, lblVisitedPlacesTitle], type: TextType.ListSubTitle.rawValue)
        lblVisitedPlacesTitle.textColor = self.listSubTitleColor
        lblTotalDistanceTitle.textColor = self.listSubTitleColor
        lblVisitedPlacesTitle.textColor = self.listSubTitleColor
        
        if globalSelectedHistory != nil{
            
            let dateString = (globalSelectedHistory?.startTime)!
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSS'Z'"//"yyyy-MM-dd'T'HH:mm:ss.ssssss'Z'"
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd MMM, yyyy"  //"MMM d, h:mm a" for  Sep 12, 2:11 PM
            let datee = dateFormatterGet.date(from: dateString)
            let dateStringInNewFormat =  dateFormatterPrint.string(from: datee ?? Date())
            print(dateStringInNewFormat)
            
            if globalSelectedHistory?.length != nil{
                
                let tmp2 = Double((globalSelectedHistory?.length)!)
                print("tmp2------",tmp2)

                let tmp = ((globalSelectedHistory?.length)! / 1.609)
                self.lblDate.text = dateStringInNewFormat
                print("miles------",tmp)
                self.lblTotalDistance.text = String(format: "%.2f", tmp) + " miles" //"\(String(describing: (globalSelectedHistory?.length)!))"
            }else{
                self.lblTotalDistance.text = "0.0"
            }
        }
        
        if parentString == "route"{
            
            self.title = "Drive Details"
            let dateString = (drivingDetails?.startTime)!
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSS'Z'"//"yyyy-MM-dd'T'HH:mm:ss.ssssss'Z'"
            
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd MMM, yyyy"  //"MMM d, h:mm a" for  Sep 12, 2:11 PM
            let datee = dateFormatterGet.date(from: dateString)
            let dateStringInNewFormat =  dateFormatterPrint.string(from: datee ?? Date())
            print(dateStringInNewFormat)
            
            if drivingDetails?.length != nil{
                print("drivingDetails?.length",(drivingDetails?.length)!)
                let tmp = ((drivingDetails?.length)! / 1.609)
                print("drivingDetails?.length------tmp----",tmp)
                
                self.lblDate.text = dateStringInNewFormat
                self.lblTotalDistance.text = String(format: "%.2f", tmp) + " miles" //"\(String(describing: (globalSelectedHistory?.length)!))"
            }else{
                self.lblTotalDistance.text = "0.0"
            }
            
            print("totalCoveredDistance-->", totalCoveredDistance)
            let totalDistanceInKM = totalCoveredDistance/1000
            print(totalDistanceInKM)
            
            self.lblDate.text = dateStringInNewFormat
            //let tmp = (totalDistanceInKM / 1.609)
            
            self.lblDate.text = dateStringInNewFormat
            
            
            //self.lblTotalDistance.text = String(format: "%.2f", tmp) + " miles"
            
        }else{
            let dateString = (globalSelectedHistory?.startTime) ?? ""
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSS'Z'"//"yyyy-MM-dd'T'HH:mm:ss.ssssss'Z'"
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd MMM, yyyy"
            let datee = dateFormatterGet.date(from: dateString)
            let dateStringInNewFormat =  dateFormatterPrint.string(from: datee ?? Date())
            print(dateStringInNewFormat)
            self.lblDate.text = dateStringInNewFormat
        }
        
        print(globalSelectedHistory?.polylines! ?? "")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
//        if Network.reachability.status == .unreachable{
//            AppConfigs.updateUserInterface(view: view)
//        }else{

            allDrivedProperties.removeAll()
            let url : String = UrlManager.baseURL() + UrlManager.apiString() +  "/history/\(historyID)/properties/"
            print(url)
            self.showSpinner(onView: view)
            getAllDrivedProperties(urlString: url)
//        }
        
        //        }
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        _ =  self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func closeButtonClicked(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func getAllDrivedProperties(urlString: String) {
        
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success",data)
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    
                    let allDrivedPropertyResponse = try? JSONDecoder().decode(GetAllPropertyFromHistoryResponse.self, from: response.data!)
                    
                    
                    if allDrivedPropertyResponse != nil{
                        
                        var items: [ListedProperty] = self.allDrivedProperties
                        items.append(contentsOf: (allDrivedPropertyResponse?.results ?? []))
                        self.allDrivedProperties.removeAll()
                        self.allDrivedProperties = items
                        
                        DispatchQueue.main.async {
                            print("allDrivedProperties------------\n",self.allDrivedProperties)
                            self.removeSpinner()//self.view.activityStopAnimating()
                            self.distanceTV.reloadData()
                            
                        }
                    }else{
                        do{
                            let json = try JSON(data: response.data!)
                            let properties = json["results"].arrayValue
                            print(properties.count)
                            
                            for property in properties{
                                
                                let city = property["city"].stringValue
                                let state = property["state"].stringValue
                                let street = property["street"].stringValue
                                let zip = property["zip"].stringValue
                                
                                let gmaTag = property["gma_tag"].intValue
                                let history = property["history"].intValue
                                let id = property["id"].intValue
                                
                                let photoCount = property["photo_count"].intValue
                                let noteCount = property["note_count"].intValue
                                
                                let latitude = property["latitude"].doubleValue
                                let longitude = property["longitude"].doubleValue
                                let powerTraceRequestID = property["power_trace_request_id"].intValue
                                let userList = property["user_list"].intValue
                                
                                
                                let listedProperty = ListedProperty(id: id, userList: userList, street: street, city: city, state: state, zip: zip, cadAcct: "", gmaTag: gmaTag, latitude: latitude, longitude: longitude, propertyTags: [], ownerInfo: [], photoCount: photoCount, noteCount: noteCount, createdAt: "", updatedAt: "", powerTraceRequestID: powerTraceRequestID, history: history)
                                self.allDrivedProperties.append(listedProperty)
                            }
                            
                            DispatchQueue.main.async {
                                print("allDrivedProperties------------\n",self.allDrivedProperties)
                                self.removeSpinner()//self.view.activityStopAnimating()
                                self.distanceTV.reloadData()
                            }
                        }catch let error {
                            AppConfigs.showSnacbar(message: error.localizedDescription, textColor: .red)
                        }
                    }
                }
                    
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                   // AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)

                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
    
}

extension HistoryDetailsViewController: UITableViewDataSource, UITableViewDelegate {
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if allDrivedProperties.count == 0 {
            tableView.setEmptyMessage("Nothing yet to show")
        } else {
            tableView.restore()
        }
        return allDrivedProperties.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier, for: indexPath) as! PropertyAddressTableViewCell
        cell.lblPropertyAddress?.textColor = self.listTitleColor
        cell.lblSeparatorLine.backgroundColor = self.listSeperatorColor
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [cell.lblPropertyAddress], type: TextType.ListTitle.rawValue)
        
        if allDrivedProperties.count > indexPath.row{
            
            let property = allDrivedProperties[indexPath.row]
            let street = property.street
            let city = property.city
            let state = property.state
            let zip = property.zip
            let tmp3 = (street ?? "") + " " + (city ?? "")
            let tmp2 = tmp3 + " " + (state ?? "") + " "
            
            cell.lblPropertyAddress?.text = tmp2 + (zip ?? "")
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if allDrivedProperties.count > indexPath.row{
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "PropertyDetailsViewController") as! PropertyDetailsViewController
            let property = allDrivedProperties[indexPath.row]
           
            let sp = SelectedProperty()
            sp.propertyID = property.id!
            sp.propertyAddress = property.street ?? " "
            sp.latitude = (Double( property.latitude ?? 0.0))
            sp.longitude = (Double( property.longitude ?? 0.0))
            
            globalSelectedProperty = sp
            globalSelectedPropertyFromList = property
            
            //nextViewController.selectedPropertyFromList = property
            nextViewController.parentVC = "History"
            self.navigationController?.pushViewController(nextViewController, animated: true)
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
        
    }
    
}

