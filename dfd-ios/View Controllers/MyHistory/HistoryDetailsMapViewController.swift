//
//  HistoryDetailsMapViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 11/1/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON

class HistoryDetailsMapViewController: UIViewController {
    
    @IBOutlet weak var myMap: GMSMapView!
    @IBOutlet weak var btnLoadListView: UIButton!
    
    var historyID : Int = 0
    var allDrivedProperties: [ListedProperty] = []
    var path = GMSMutablePath()
    var allPaths: [GMSMutablePath] = []
    var bounds = GMSCoordinateBounds()
    var finishDriveResponse : HistoryDetailsResponse?
    var parentString : String = ""
    var timerAnimation: Timer!
    
    
    var allTags: [PropertyTag] = []
    var jsonTagResponse: ListOfTagsResponse?
    //var allListArray : [AllListsResponseForDropdown]
    var selectedTag : PropertyTag?
    var selectedTagID: [Int] = []
    var allTagNames : [String] = []
    var allColorCodes : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        myMap.delegate = self
        btnLoadListView.cornerRadius = btnLoadListView.frame.size.width/2
        let configuration = URLSessionConfiguration.default
        configuration.httpMaximumConnectionsPerHost = 10
        
        
//        if Network.reachability.status == .unreachable{
//            AppConfigs.updateUserInterface(view: view)
//        }else{
//
            
            allTags.removeAll()
            let url : String = UrlManager.baseURL() + UrlManager.apiString() + "/tag/"
            print(url)
            getAllTags(urlString: url)
            
            if parentString == "home"{
                self.allDrivedProperties.removeAll()
                allDrivedProperties = []
                //self.allDrivedProperties = (finishDriveResponse?.property)!
                print(drivingDetails?.id)
                let url : String = UrlManager.baseURL() + UrlManager.apiString() +  "/history/\( String(describing: (drivingDetails?.id)!))/"
                print(url)
                //  self.showSpinner(onView: view)
                getAllDrivedProperties(urlString: url)
                
            }else{
                
                allDrivedProperties.removeAll()
                allDrivedProperties = []
                
                let url : String = UrlManager.baseURL() + UrlManager.apiString() +  "/history/\(historyID)/"
                print(url)
                //  self.showSpinner(onView: view)
                getAllDrivedProperties(urlString: url)
            }
            
//        }
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        stopAnimatePolylinePath()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        
        //        }
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func showListView(_ sender: Any) {
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "MyStory", bundle: nil)
        
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "HistoryDetailsViewController") as! HistoryDetailsViewController
        vc.historyID = self.historyID
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func putPropertyMarkersInMap() {
        
        if allDrivedProperties.count > 0{
            if allDrivedProperties.count % 2 == 0{
                let index = allDrivedProperties.count/2
                let mdata = allDrivedProperties[index]
                let camera = GMSCameraPosition.camera(withTarget: CLLocationCoordinate2D(latitude: mdata.latitude!, longitude: mdata.longitude!), zoom: 16)
                myMap.animate(to: camera)
            }else{
                let index = (allDrivedProperties.count - 1) / 2
                let mdata = allDrivedProperties[index]
                let camera = GMSCameraPosition.camera(withTarget: CLLocationCoordinate2D(latitude: mdata.latitude!, longitude: mdata.longitude!), zoom: 16)
                myMap.animate(to: camera)
            }
        }
        
        for property in allDrivedProperties{
            
            let lat = property.latitude
            let long = property.longitude
            
            let marker = GMSMarker()
            if lat != nil && long != nil{
                marker.position = CLLocationCoordinate2D(latitude: lat!, longitude: long!)
            }
            marker.title = String(property.id!)
            //marker.icon = GMSMarker.markerImage(with: .red)
            // marker.appearAnimation = .pop //kGMSMarkerAnimationPop
            marker.map = myMap
            
            let markerImage = UIImage(named: "new_ic_tapped_property")!.withRenderingMode(.alwaysTemplate)
            //creating a marker view
            let markerView = UIImageView(image: markerImage)
            let tags = property.propertyTags
            
            if tags != nil{
                
                if tags?.count == 0{
                    
                    markerView.tintColor = UIColor.black //.systemBlue
                    marker.iconView = markerView
                    marker.icon = markerImage
                    
                }else if tags!.count > 1{
                    
                    markerView.tintColor = .systemGray
                    marker.iconView = markerView
                    marker.icon = markerImage
                    
                }else if tags?.count == 1{
                    
                    let tag = tags?[0]
                    let color = tag?.color?.colorCode
                    print(color ?? "")
                    
                    let propertyTagID = tag?.id ?? 0
                    print(propertyTagID)
                    
                    
                    for tag in self.allTags{
                        
                        let id = tag.id
                        if id == propertyTagID{
                            let color = tag.color?.colorCode
                            print(color ?? "")
                            let uiColor =  UIColor(hexString: color!)
                            markerView.tintColor = uiColor
                            marker.iconView = markerView
                            marker.icon = markerImage
                        }
                    }
                }else{
                    print("unknown case")
                    markerView.tintColor = UIColor.black //.systemBlue
                    marker.iconView = markerView
                    marker.icon = markerImage
                }
            }else{
                markerView.tintColor = UIColor.black //.systemBlue
                marker.iconView = markerView
                marker.icon = markerImage
            }
        }
    }
    
    var polyline = GMSPolyline()
    var animationPolyline = GMSPolyline()
    var animationPath = GMSMutablePath()
    var i: UInt = 0
    var timer: Timer!
    
    
    func drawPolylinesWithPause(){
        
        var polylinesWithPosition: [LocationWithIndexPosition] = []
        
//        let string = (historyDetailsResponse?.polylines) ?? ""
//        print(string)
//        let data = string.data(using: .utf8)!
        let string = (globalSelectedHistory?.polylines) ?? ""
        let data = string.data(using: .utf8)!
        
        do {
            if let polylines = try? JSONDecoder().decode([Polyline].self, from: data)
            {
                print("polylines.count", polylines.count)
                
                for (index,polyline) in polylines.enumerated(){
                    
                    if index == 0{
                        
                        let startMarkerPosition = polyline
                        
                        let startMarker = GMSMarker()
                        startMarker.position = CLLocationCoordinate2D(latitude: startMarkerPosition.latitude!, longitude: startMarkerPosition.longitude!)
                        startMarker.icon = UIImage(named: "marker_drive_start_point")
                        startMarker.map = myMap
                    }
                    
                    let pwp = LocationWithIndexPosition()
                    pwp.latitude = polyline.latitude!
                    pwp.longitude = polyline.longitude!
                    
                    if polyline.position != nil{
                        pwp.position = polyline.position!
                    }else{
                        pwp.position = 0
                    }
                    polylinesWithPosition.append(pwp)
                    
                }
                
                let endMarkerPosition = polylines[polylines.count - 1]
                let endMarker = GMSMarker()
                endMarker.position = CLLocationCoordinate2D(latitude: endMarkerPosition.latitude ?? 0.0, longitude: endMarkerPosition.longitude ?? 0.0)
                endMarker.icon = UIImage(named: "marker_drive_end_point")
                endMarker.map = myMap
                
                
                //------------------start handle pause/resume polyline draw while driving
                
                let grouped = polylinesWithPosition.group(by: { $0.position })
                print("grouped.count", grouped.count)
                
                for (index,element) in grouped.enumerated(){
                    
                    print("index--->",index)
                    print("element--->",element)
                    print(element.key)
                    print(element.value)
                    let locations = element.value
                     
                    let lPath = GMSMutablePath()
                    
                    for (lindex, lelement) in locations.enumerated(){
                        
                        let lat = lelement.latitude
                        let long = lelement.longitude
                       
                        lPath.add(CLLocationCoordinate2D(latitude: lat, longitude: long))
                        path.add(CLLocationCoordinate2D(latitude: lat, longitude: long))
                        self.allPaths.append(lPath)
                        //self.timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(animatePolylinePath), userInfo: nil, repeats: true)
                        
                    }


                    let lPolyline = GMSPolyline(path: lPath) // GMSPolyline(path: path)
                    lPolyline.strokeWidth = 3
                    lPolyline.strokeColor = UIColor.red
                    lPolyline.map = myMap
                    //oldPolylineArr2.append(lPolyline)
                }
                //------------------start handle pause/resume polyline draw while driving
                
                
                let bounds = GMSCoordinateBounds(path: path)
                myMap.animate(toZoom: 10)
                self.myMap!.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 50.0))
                
            }else{
                
                print("very bad json")
            }
        }catch let error  {
            print(error.localizedDescription)
        }
    }
    
    @objc func animatePolylinePath(path: GMSMutablePath) {
        
        //let timer: Timer!
        //var polyline = GMSPolyline()
        let animationPolyline = GMSPolyline()
        let animationPath = GMSMutablePath()
        var i: UInt = 0
        
        let counter = Double( self.path.count()/3000)
        print(counter)
        
        for path in self.allPaths{
            
            if (self.i < self.path.count()) {
                
                DispatchQueue.main.asyncAfter(deadline: .now() + (counter)) { // Change `2.0` to the desired number of seconds.
                    // Code you want to be delayed
                    animationPath.add(path.coordinate(at: i))
                    animationPolyline.path = animationPath
                    animationPolyline.strokeColor = UIColor.red
                    animationPolyline.strokeWidth = 3
                    animationPolyline.map = self.myMap
                    i += 1
                }
            }
            else {
                timer.invalidate()
            }
        }
    }
    
    func drawPolylines() {
        
        path.removeAllCoordinates()
        let string = (globalSelectedHistory?.polylines) ?? ""
        let data = string.data(using: .utf8)!
        
        do {
            if let polylines = try? JSONDecoder().decode([Polyline].self, from: data)
            {
                print(polylines.count)
                
                for polyline in polylines{
                    path.add(CLLocationCoordinate2D(latitude: polyline.latitude!, longitude: polyline.longitude!))
                    bounds.includingCoordinate(CLLocationCoordinate2D(latitude: polyline.latitude!, longitude: polyline.longitude!))
                }
                
                let startMarkerPosition = polylines.first
                let endMarkerPosition = polylines.last
                
                let startMarker = GMSMarker()
                startMarker.position = CLLocationCoordinate2D(latitude: startMarkerPosition?.latitude ?? 0.0, longitude: startMarkerPosition?.longitude ?? 0.0)
                startMarker.icon = UIImage(named: "marker_drive_start_point")
                startMarker.map = myMap
                
                let endMarker = GMSMarker()
                endMarker.position = CLLocationCoordinate2D(latitude: endMarkerPosition?.latitude ?? 0.0, longitude: endMarkerPosition?.longitude ?? 0.0)
                endMarker.icon = UIImage(named: "marker_drive_end_point")
                endMarker.map = myMap
                
                let polyline = GMSPolyline(path: path)
                //polyline.map = myMap
                let bounds = GMSCoordinateBounds(path: path)
                myMap.animate(toZoom: 10)
                self.myMap!.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 50.0))
                self.timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(animatePolylinePath), userInfo: nil, repeats: true)
                
            }else{
                print("very bad json")
            }
        }catch let error  {
            print(error.localizedDescription)
        }
    }
    
//    @objc func animatePolylinePath(path: GMSMutablePath) {
//
//
//        let counter = Double( self.path.count()/3000)
//        print(counter)
//
//        if (self.i < self.path.count()) {
//
//            DispatchQueue.main.asyncAfter(deadline: .now() + (counter)) { // Change `2.0` to the desired number of seconds.
//                // Code you want to be delayed
//                self.animationPath.add(self.path.coordinate(at: self.i))
//                self.animationPolyline.path = self.animationPath
//                self.animationPolyline.strokeColor = UIColor.red
//                self.animationPolyline.strokeWidth = 3
//                self.animationPolyline.map = self.myMap
//                self.i += 1
//            }
//
//
//        }
//        else {
//
//            self.timer.invalidate()
//        }
//    }
    
    
    func stopAnimatePolylinePath() {
        
        /// self.timerAnimation.invalidate()
    }
    
    func getAllTags(urlString: String) {
        
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success",data)
                let statusCode = response.response?.statusCode
                
                if statusCode == 200{
                    
                    let tagsResponse = try? JSONDecoder().decode(ListOfTagsResponse.self, from: response.data!)
                    print(tagsResponse)
                    self.allTags = tagsResponse!
                    DispatchQueue.main.async {
                        print("allTags------------\n",self.allTags)
                        self.removeSpinner()//self.view.activityStopAnimating()
                        //  self.tagTV.reloadData()
                        
                        for tag in self.allTags{
                            let name = tag.name
                            let colorCode = tag.color?.colorCode
                            self.allTagNames.append(name!)
                            self.allColorCodes.append(colorCode!)
                        }
                    }
                }
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                
                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
    func getAllDrivedProperties(urlString: String) {
        
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success",data)
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    
                    let allDrivedPropertyResponse = try? JSONDecoder().decode(HistoryDetailsResponse.self, from: response.data!)
                    
                    if allDrivedPropertyResponse != nil{
                        self.allDrivedProperties.removeAll()
                        self.allDrivedProperties = []
                        self.myMap.clear()
                        
                        DispatchQueue.main.async {
                            self.allDrivedProperties = (allDrivedPropertyResponse?.property ?? [])
                            
                            print("allDrivedProperties------------\n",self.allDrivedProperties)
                            self.removeSpinner()//self.view.activityStopAnimating()
                            
                            self.putPropertyMarkersInMap()
                            //self.drawPolylines()
                            self.drawPolylinesWithPause()
                            
                        }
                    }else{
                        
                        do{
                            let json = try JSON(data: response.data!)
                            let properties = json["property"].arrayValue
                            print(properties.count)
                            
                            for property in properties{
                                
                                let city = property["city"].stringValue
                                let state = property["state"].stringValue
                                let street = property["street"].stringValue
                                let zip = property["zip"].stringValue
                                
                                let gmaTag = property["gma_tag"].intValue
                                let history = property["history"].intValue
                                let id = property["id"].intValue
                                
                                let photoCount = property["photo_count"].intValue
                                let noteCount = property["note_count"].intValue
                                
                                let latitude = property["latitude"].doubleValue
                                let longitude = property["longitude"].doubleValue
                                let powerTraceRequestID = property["power_trace_request_id"].intValue
                                let userList = property["user_list"].intValue
                                
                                
                                let listedProperty = ListedProperty(id: id, userList: userList, street: street, city: city, state: state, zip: zip, cadAcct: "", gmaTag: gmaTag, latitude: latitude, longitude: longitude, propertyTags: [], ownerInfo: [], photoCount: photoCount, noteCount: noteCount, createdAt: "", updatedAt: "", powerTraceRequestID: powerTraceRequestID, history: history)
                                self.allDrivedProperties.append(listedProperty)
                            }
                            
                            DispatchQueue.main.async {
                                print("allDrivedProperties------------\n",self.allDrivedProperties)
                                self.removeSpinner()//self.view.activityStopAnimating()
                                self.putPropertyMarkersInMap()
                                //self.drawPolylines()
                                self.drawPolylinesWithPause()

                            }
                            
                            
                        }catch let error {
                            AppConfigs.showSnacbar(message: error.localizedDescription, textColor: .red)
                        }
                    }
                    
                }
                    
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                   // AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                
                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
}

extension HistoryDetailsMapViewController: GMSMapViewDelegate{
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        print("did tap a marker")
        
        // Remember to return false
        // so marker event is still handled by delegate
        print(marker.title)
        
        if marker.title != nil{
            
            print("viewDetailsButtonAction")
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "PropertyDetailsViewController") as! PropertyDetailsViewController
            nextViewController.parentVC = "LoadHouzesFromMap" //because it is loading properties which is added to list
            //        globalSelectedPropertyFromList?.id = Int((marker.title)!)
            //        print((globalSelectedPropertyFromList?.id)!)
            nextViewController.propertyIDFromList = marker.title!
            self.navigationController?.pushViewController(nextViewController, animated: true)
            
        }
        return true
    }
}
