//
//  StoryViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 5/3/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher

//let manager = Alamofire.SessionManager.default

class StoryViewController: UIViewController {
    
    @IBOutlet weak var storyTV: UITableView!
    @IBOutlet weak var btnFilter: UIBarButtonItem!
    
    
    var drivingList: [MyDrive] = []
    var url : String = ""
    var nextUrl: String = ""
    var previousUrl: String = ""
    var jsonResponse: MyHistoryResponse?
    var parentVc: String = ""
    let manager = Alamofire.SessionManager.default
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        manager.session.configuration.timeoutIntervalForRequest = 30

        let nib = UINib(nibName: "HistoryTableViewCell", bundle: nil)
        storyTV.register(nib, forCellReuseIdentifier: "HistoryTableViewCell")
        self.storyTV.tableFooterView = UIView()
        self.storyTV.backgroundColor = .white
        self.storyTV.separatorStyle = .none
        
        self.setUpNavigationBar()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // FIXME: Remove code below if u're using your own menu
        setupNavForDefaultMenu()
        
        // Add left bar button item
        let leftBarItem = UIBarButtonItem(image: UIImage(named: "burger"), style: .plain, target: self, action: #selector(toggleSideMenu))
        leftBarItem.tintColor = .black
        navigationItem.leftBarButtonItem = leftBarItem
        
        if AppConfigs.getCurrentUserInfo().upgradeInfo?.isTeamInvitable == false{
            self.btnFilter.isEnabled = false
            self.btnFilter.image = nil
            
        }else{
            self.btnFilter.isEnabled = true
            self.btnFilter.image = UIImage(named: "new_ic_team_filter")
        }
        
        self.title = "My History"
        
//        if Network.reachability.status == .unreachable{
//            AppConfigs.updateUserInterface(view: view)
//        }else{

            if globalIsFilterSelected == true{
                
                print(globalSelectedMembersID)
                let joined = globalSelectedMembersID.map { String($0) }.joined(separator: ",")
                print(joined)
                
                drivingList.removeAll()
                let url : String = UrlManager.baseURL() + UrlManager.apiString() +  "/history/member/?members=\(joined)"
                //http://192.168.1.22:8000/api/history/member/?members=5,3
                print(url)
                self.showSpinner(onView: view)
                getAllDrives(urlString: url)
                
            }else{
                drivingList.removeAll()
                let url : String = UrlManager.baseURL() + UrlManager.apiString() +  "/history/user/"
                print(url)
                self.showSpinner(onView: view)
                getAllDrives(urlString: url)
                
            }
//        }

        
        //        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func setupNavForDefaultMenu() {
        // Revert navigation bar translucent style to default
        //navigationBarNonTranslecentStyle()
        // Update side menu after reverted navigation bar style
        sideMenuManager?.instance()?.menu?.isNavbarHiddenOrTransparent = true
        navigationItem.hidesBackButton = true
        
    }
    
    @objc func toggleSideMenu() {
        sideMenuManager?.toggleSideMenuView()
    }
    
    @objc func showTeamList() {
        print("show team list")
    }
    
    @IBAction func showMemberList(_ sender: Any) {
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "MyStory", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "SelecMemberViewController") as! SelecMemberViewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
        
    }
    
    func getAllDrives(urlString: String) {
        
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        manager.request(urlString, method: .get, parameters: nil, encoding:  JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success",data)
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    
                    let allDriveResponse = try? JSONDecoder().decode(MyHistoryResponse.self, from: response.data!)
                    self.jsonResponse = allDriveResponse
                    
                    var items: [MyDrive] = self.drivingList
                    items.append(contentsOf: (allDriveResponse?.results)!)
                    
                    self.drivingList.removeAll()
                    self.drivingList = items
                    
                    DispatchQueue.main.async {
                        print("drivingList------------\n",self.drivingList)
                        self.removeSpinner()//self.view.activityStopAnimating()
                        self.storyTV.reloadData()
                    }
                }
                    
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)

                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
    func calculateTimeDifference(startDateString:String, endDateString: String,completionClosure: (_ timeInHour: Double)-> ())  {
        let dateFormatter = DateFormatter()
        let timeZone = NSTimeZone(name: "UTC")
        dateFormatter.timeZone = timeZone as TimeZone?
        dateFormatter.dateFormat =  "yyyy-MM-dd'T'HH:mm:ss.SSS" //"yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        //let date = dateFormatter.date(from: stringToConvert)
        
        guard let startDate = dateFormatter.date(from: startDateString) else {
            return
        }
        
        guard let endDate = dateFormatter.date(from: endDateString) else {
            return
        }
        
        let secondsBetween: TimeInterval = endDate.timeIntervalSince(startDate)
        
        print("secondsBetween-->",secondsBetween)
        
        let minute = secondsBetween/3600
        print(minute)
        
        completionClosure(minute)
        
    }
}

extension StoryViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if drivingList.count == 0 {
            tableView.setEmptyMessage("Nothing yet to show")
        } else {
            tableView.restore()
        }
        return drivingList.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        
        return 0;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryTableViewCell", for: indexPath) as! HistoryTableViewCell
      
        cell.backgroundColor = .white
        cell.upView.backgroundColor = .white
        cell.upView.cornerRadius = 0
        
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [cell.lblDistanceTitle, cell.lblDurationTitle, cell.visitingDate], type: TextType.ListSubTitle.rawValue)
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [cell.lblTotalVisitDistance, cell.lblDrivingDuration], type: TextType.MainSubTitle.rawValue)
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [cell.lblNoOfProperties], type: TextType.ListTitle.rawValue)
        
        cell.visitingDate.textColor = self.listTitleColor
        cell.lblNoOfProperties.textColor = self.pinkListTitleColor
        cell.lblDurationTitle.textColor = self.listTitleColor
        cell.lblDistanceTitle.textColor = self.listTitleColor
        
        cell.lblDurationTitle.textColor = self.listSubTitleColor
        cell.lblDistanceTitle.textColor = self.listSubTitleColor
        cell.lblSeperator.backgroundColor = self.listSeperatorColor
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        
        if drivingList.count > indexPath.row{
            
            let data = drivingList[indexPath.row]
            
            if data.length != nil{
                let tmp = ((data.length)! / 1.609)
                cell.lblTotalVisitDistance.text =  String(format: "%.2f", tmp)  //data.length
            }else{
                cell.lblTotalVisitDistance.text = "0.0"
            }
            
            cell.lblNoOfProperties.text = "\(String(describing: (data.propertyCount)!)) Properties"
            
            let dateString = data.startTime
            let endTime = data.endTime
            if endTime != nil && dateString != nil {
                
                let startDate =  DateFormatter.getDateFromString(string: dateString!, fromFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSS'Z'")//dateFormatter.date(from: dateString)!
                print("startDate : \(String(describing: startDate))")
                
                let txt = DateFormatter.getStringFromDate(date: startDate ?? Date(), fromDateFormat: "dd MMM, yyyy hh:mm a")//(date: startDate ?? Date())
                cell.visitingDate.text = txt
                
                let endDate = DateFormatter.getDateFromString(string: endTime!, fromFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSS'Z'") //dateFormatter.date(from: endTime!)
                print("endDate : \(String(describing: endDate))")
                
                if startDate != nil  && endDate != nil{
                    
                    let secondsBetween: TimeInterval = endDate!.timeIntervalSince(startDate!)
                    let hours = secondsBetween/3600
                    print(hours)
                    cell.lblDrivingDuration.text = String(format: "%.2f", hours)
                }
            }
            
            let imageUrl = data.image ?? ""
            if imageUrl == ""{
                cell.routeImageView.image = UIImage(named: "route.png")
            }else{
                cell.routeImageView.image = UIImage(named: "route.png")
                let pictureURL = URL(string: imageUrl)!
                cell.routeImageView.kf.setImage(with: pictureURL)
                
            }
            
//
//            if  cell.routeImageView.image != nil{
//
//                DispatchQueue.main.async {
//                    
//                    let resizedImage = self.resizeImage(image: cell.routeImageView.image!, newWidth: cell.routeImageView.frame.size.width)
//                    cell.routeImageView.image = resizedImage
//                    
////                    let resizeImageWithScaling = self.resizeImageWithScaling(image: cell.routeImageView.image!, scaleToSize: cell.routeImageView.frame.size, isAspectRation: true)
////                    cell.routeImageView.image = resizeImageWithScaling
//                    
//                }
//            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row == drivingList.count - 1 {
            // we are at last cell load more content
            if drivingList.count < (jsonResponse?.count)! {
                self.getAllDrives(urlString: (jsonResponse?.next ?? ""))
            }
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 270
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if drivingList.count > indexPath.row{
            
            let data = drivingList[indexPath.row]
            print(data)
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "MyStory", bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "HistoryDetailsMapViewController") as! HistoryDetailsMapViewController
            vc.historyID = data.id!
            globalSelectedHistory = data
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
    }
}


extension DateFormatter {
    
    private static var dateFormatter = DateFormatter()
    
    class func initWithSafeLocale(withDateFormat dateFormat: String? = nil) -> DateFormatter {
        
        dateFormatter = DateFormatter()
        
        var en_US_POSIX: Locale? = nil;
        
        if (en_US_POSIX == nil) {
            en_US_POSIX = Locale.init(identifier: "en_US_POSIX")
        }
        dateFormatter.locale = en_US_POSIX
        
        if dateFormat != nil, let format = dateFormat {
            dateFormatter.dateFormat = format
        }else{
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        }
        return dateFormatter
    }
    
    // ------------------------------------------------------------------------------------------
    
    class func getDateFromString(string: String, fromFormat dateFormat: String? = nil) -> Date? {
        
        if dateFormat != nil, let format = dateFormat {
            dateFormatter = DateFormatter.initWithSafeLocale(withDateFormat: format)
        }else{
            dateFormatter = DateFormatter.initWithSafeLocale()
        }
        guard let date = dateFormatter.date(from: string) else {
            return nil
        }
        return date
    }
    
    // ------------------------------------------------------------------------------------------
    
    class func getStringFromDate(date: Date, fromDateFormat dateFormat: String? = nil)-> String {
        
        if dateFormat != nil, let format = dateFormat {
            dateFormatter = DateFormatter.initWithSafeLocale(withDateFormat: "dd MMM, yyyy hh:mm a")
        }else{
            dateFormatter = DateFormatter.initWithSafeLocale()
        }
        
        let string = dateFormatter.string(from: date)
        
        return string
    }
    
    
}

