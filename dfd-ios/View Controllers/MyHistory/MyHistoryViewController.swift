//
//  MyHistoryViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 7/16/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit
import XMSegmentedControl

class MyHistoryViewController: UIViewController  {
    
    
    @IBOutlet weak var containerView: UIView!
    
    
    let vcArray = ["StoryViewController", "VisitedPlacesViewController"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "History"
        self.view.backgroundColor = FontAndColorConfigs.getSegmentedControllerColor()//UIColor(red:0.09, green:0.15, blue:0.22, alpha:1)
        
        self.loadViewController(id: vcArray[0])
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavForDefaultMenu()
        
        self.setUpNavigationBar()
        // Add left bar button item
        let leftBarItem = UIBarButtonItem(image: UIImage(named: "burger"), style: .plain, target: self, action: #selector(toggleSideMenu))
        leftBarItem.tintColor = .black
        navigationItem.leftBarButtonItem = leftBarItem
    }
    
    @objc func toggleSideMenu() {
        sideMenuManager?.toggleSideMenuView()
    }
    
    private func setupNavForDefaultMenu() {
        // Revert navigation bar translucent style to default
        //navigationBarNonTranslecentStyle()
        // Update side menu after reverted navigation bar style
        sideMenuManager?.instance()?.menu?.isNavbarHiddenOrTransparent = true
        navigationItem.hidesBackButton = true
        
    }
    
    @IBAction func switchView(_ sender: UISegmentedControl) {
        
        if sender.selectedSegmentIndex == 0 {
            self.loadViewController(id: vcArray[0])
            
        }
        else {
            self.loadViewController(id: vcArray[1])
        }
    }
    
    
    func loadViewController(id: String) {
        
        if id == "StoryViewController" {
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "MyStory", bundle:nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: id)
            self.addChild(vc)
            vc.view.frame = CGRect(x: 0, y: 0, width: self.containerView.frame.size.width, height:
                
                self.containerView.frame.size.height)
            self.containerView.addSubview(vc.view)
        }else{
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "VisitedPlaces", bundle:nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: id)
            self.addChild(vc)
            vc.view.frame = CGRect(x: 0, y: 0, width: self.containerView.frame.size.width, height:
                
                self.containerView.frame.size.height)
            self.containerView.addSubview(vc.view)
        }
        
    }
    
    
}
