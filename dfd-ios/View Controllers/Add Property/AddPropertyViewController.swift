//
//  AddPropertyViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 5/18/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit
import SearchTextField
import GoogleMaps
import SnapKit
import TTGSnackbar
import Floaty
import JJFloatingActionButton
import DropDown
import GooglePlaces

class AddPropertyViewController: UIViewController {
    
    @IBOutlet weak var bannerView: UIView!
    //@IBOutlet weak var addressTextField: SearchTextField!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var btnAddress: UIButton!
    
    
    @IBOutlet weak var btnNormalView: UIButton!
    @IBOutlet weak var btnSatelliteView: UIButton!
    @IBOutlet weak var mapTypeView: UIView!
    
    let marker = GMSMarker()
    var locationManager = CLLocationManager()
    let geocoder = GMSGeocoder()
    var tappedMarker = GMSMarker()
    var currentLocationMarker: GMSMarker?
    var addLocationButton = UIButton()
    var selectedPlaceCoordinate: CLLocationCoordinate2D?
    var selectedPlaceAddress: String = ""
    var currentLocation: CLLocation!
    
    var infoWindow = UIView(frame: CGRect.init(x: 0, y: 0, width: 250, height: 80))
    
    var items = ["Holding No: 81, 3rd Floor, Bosila, 16 Rd No. 12, Dhaka", "114, Rupayan Trade Center, Kazi Nazrul Islam Avenue, Bangla Motor, Dhaka 1000",  "8th floor, Plot-02, Amtoli, Bir Uttam Ak Khandaker Rd, Dhaka 1212" ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if( CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() ==  .authorizedAlways){
            
            currentLocation = locationManager.location
            print("latitude----", currentLocation.coordinate.latitude)
            print("longitude----", currentLocation.coordinate.longitude)
            //to do
            //29.709151, -95.456857
            
            let camera = GMSCameraPosition.camera(withTarget: CLLocationCoordinate2D(latitude: USA_LAT, longitude: USA_LONG), zoom: 17, bearing: 0, viewingAngle: 0)
            
            mapView.animate(to: camera)
            CATransaction.begin()
            CATransaction.setAnimationDuration(3.0)
            mapView.animate(toLocation: CLLocationCoordinate2D(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude))
            mapView.animate(toZoom: 17)
            CATransaction.commit()
            
        }else{
            
            
        }
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.notifyForPropertyAddedToListFromAddProperty(notification:)), name: Notification.Name("notifyForPropertyAddedToListFromAddProperty"), object: nil)
        
        
        let spacing: CGFloat = 8.0 / 2
        
        //btnAddress.imageEdgeInsets = UIEdgeInsets(top: 0, left:  buttonWidth - titleWidth! - imageWidth - 16.0, bottom: 0, right: 0)
        btnAddress.titleEdgeInsets = UIEdgeInsets(top: 0, left: spacing, bottom: 0, right: 0)
        btnAddress.contentEdgeInsets = UIEdgeInsets(top: 0, left: spacing, bottom: 0, right: spacing)
        btnAddress.addTarget(self, action: #selector(showAutoCompleteView), for: .touchUpInside)
        
        //mapView.mapStyle(withFilename: "style", andType: "json")
        mapView.mapType = .satellite
        mapView.delegate = self
        //bannerView.backgroundColor = UIColor(red:0.07, green:0.82, blue:0.41, alpha:1)
        //setUpAddressTextField()
        
        setUpView()
        self.addLocationButton.isHidden = true
        locationManager.requestAlwaysAuthorization()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.placeFound(notification:)), name: Notification.Name("placeFound"), object: nil)
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // FIXME: Remove code below if u're using your own menu
        setupNavForDefaultMenu()
        setUpConstraint()
        self.setUpNavigationBar()
        mapTypeView.isHidden = true
        // Add left bar button item
        let leftBarItem = UIBarButtonItem(image: UIImage(named: "burger"), style: .plain, target: self, action: #selector(toggleSideMenu))
        navigationItem.leftBarButtonItem = leftBarItem
        leftBarItem.tintColor = .black
        self.title = "Add HouZes"
        mapView.isMyLocationEnabled = true
        
    }
    
    @objc func placeFound(notification: Notification) {
        
        self.addLocationButton.isHidden = false
        
        mapView.animate(toLocation: self.selectedPlaceCoordinate!)
        //
        CATransaction.begin()
        CATransaction.setAnimationDuration(3.0)
        mapView.animate(toZoom: 17)
        
        CATransaction.commit()
        //mapView.clear()
        
        //let marker = GMSMarker()
        marker.position = self.selectedPlaceCoordinate!
        marker.title = self.selectedPlaceAddress
        marker.snippet = ""
        
        marker.map = mapView
        
        
    }
    
    @objc func notifyForPropertyAddedToListFromAddProperty (notification: Notification) {
        print("Value of notification : ", notification.object ?? "")
        
        print("viewDetailsButtonAction")
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "PropertyDetailsViewController") as! PropertyDetailsViewController
        nextViewController.modalPresentationStyle = .fullScreen
        nextViewController.parentVC = "AddProperty"
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @IBAction func switchMapType(_ sender: Any) {
        //mapTypeDropdown.show()
        
        if self.mapTypeView.isHidden == true{
            CATransaction.begin()
            
            let transition = CATransition()
            
            transition.timingFunction = CAMediaTimingFunction.init(name: .easeInEaseOut)
            transition.type = .push
            transition.subtype = .fromRight
            
            self.mapTypeView.layer.add(transition, forKey: kCATransition)
            CATransaction.commit()
            
            self.mapTypeView.isHidden = false
        }else{
            self.mapTypeView.isHidden = true
        }
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func setupNavForDefaultMenu() {
        // Revert navigation bar translucent style to default
        //navigationBarNonTranslecentStyle()
        // Update side menu after reverted navigation bar style
        sideMenuManager?.instance()?.menu?.isNavbarHiddenOrTransparent = true
        navigationItem.hidesBackButton = true
    }
    
    @objc func showAutoCompleteView(sender: UIButton!) {
        let acController = GMSAutocompleteViewController()
        //acController.navigationController?.ser
        acController.delegate = self
        present(acController, animated: true, completion: nil)
        
    }
    
    @objc func toggleSideMenu() {
        sideMenuManager?.toggleSideMenuView()
    }
    
    func setUpConstraint(){
        
        //drive now button constraint
        addLocationButton.snp.makeConstraints { (make) -> Void in
            
            make.width.equalTo(180)
            make.height.equalTo(66)
            make.bottom.equalTo(mapView).offset(-20)
            make.centerX.equalTo(mapView)
        }
        
    }
    
    func setUpView() {
        
        let CenterCurrenetLocationButton = UIButton(frame: CGRect(x: mapView.frame.width - 70, y: 60, width: 70, height: 70))
        CenterCurrenetLocationButton.setImage(UIImage(named: "ic_get_current_location"), for: .normal)
        CenterCurrenetLocationButton.adjustsImageSizeForAccessibilityContentSizeCategory = true
        CenterCurrenetLocationButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        self.mapView.addSubview(CenterCurrenetLocationButton)
        
        
        addLocationButton.backgroundColor = self.buttonBackgroundColor
        addLocationButton.setTitle("Add To List", for: .normal)
        addLocationButton.cornerRadius = 5
        addLocationButton.setImage(UIImage(named: "ic_like"), for: .normal)
        addLocationButton.titleLabel?.font = UIFont(name: "ProximaNova-Regular", size: 15)
        addLocationButton.setTitleColor(UIColor.white, for: .normal)
        addLocationButton.tintColor = .white
        addLocationButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10);
        addLocationButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0);
        addLocationButton.adjustsImageSizeForAccessibilityContentSizeCategory = true
        addLocationButton.addTarget(self, action: #selector(addLocationButtonAction), for: .touchUpInside)
        self.mapView.addSubview(addLocationButton)
    }
    
    
    @objc func buttonAction(sender: UIButton!) {
        print("Button tapped")
        self.zoomToCoordinates(locationManager.location?.coordinate ?? CLLocationCoordinate2D(latitude: USA_LAT, longitude: USA_LONG))
        self.locationManager.delegate = self
        self.locationManager.startUpdatingLocation()
    }
    
    @objc func addLocationButtonAction(sender: UIButton!) {
        
        //        AppConfigs.showSnacbar(message: "Property Added", textColor: .green)
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ListOfListViewController") as! ListOfListViewController
        nextViewController.modalPresentationStyle = .fullScreen
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    
    @IBAction func btnSatelliteClicked(_ sender: Any) {
        self.mapView.mapType = .satellite
        self.mapTypeView.isHidden = true
    }
    
    @IBAction func btnNormalClicked(_ sender: Any) {
        self.mapView.mapType = .normal
        self.mapTypeView.isHidden = true
    }
    
    func showMarkerWithTitleSnippet(title: String, snippet:String, position: CLLocationCoordinate2D){
        
        print(position.longitude)
        print(title)
        print(snippet)
        infoWindow.removeFromSuperview()
        print("You tapped at \(position.latitude), \(position.longitude)")
        infoWindow.removeFromSuperview()
       
        let font = ProNovaSB11;// UIFont(name: "Helvetica", size: 12.0)!
               //let font2 = UIFont(name: "Helvetica", size: 20.0)!
        let h = AppConfigs.heightForView(text: marker.title ?? "", font: font, width: infoWindow.frame.size.width - 100)
        print("jh", h)
        
        infoWindow = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width - 75, height: h + 120))
        infoWindow.center = CGPoint(x: self.view.center.x, y: self.view.center.y - 100)
        
        //showTriangularTipAtCenterBottom
        self.showTriangularTipAtCenterBottom(view: infoWindow, parentView: self.view)
        
        infoWindow.layer.cornerRadius = 10
       
        
        let imgView =  UIImageView(frame: CGRect.init(x: 16, y: 20, width: 20, height: 20))
        imgView.image = UIImage(named: "Shape")
        imgView.backgroundColor = .white
        imgView.tintColor = .red
        imgView.contentMode = .center
        infoWindow.addSubview(imgView)
        
        
        let lbl1 = UILabel(frame: CGRect.init(x: 48, y: 16, width: infoWindow.frame.size.width - 100, height: AppConfigs.heightForView(text: marker.title ?? "", font: font, width: infoWindow.frame.size.width - 100)))
        lbl1.text = marker.title
        lbl1.textColor = UIColor(red:0.2, green:0.3, blue:0.41, alpha:1)//.red// UIColor(red:0.2, green:0.3, blue:0.41, alpha:1)
        lbl1.textAlignment = .left
        lbl1.numberOfLines = 0
        lbl1.lineBreakMode = .byWordWrapping
        lbl1.sizeToFit()
        infoWindow.addSubview(lbl1)
        
        let buttonAddToList = UIButton(frame: CGRect.init(x: 50, y: lbl1.frame.origin.y + lbl1.frame.size.height + 16, width: 40, height: 40))
        buttonAddToList.backgroundColor = UIColor(red:0.07, green:0.82, blue:0.41, alpha:1)
        //buttonAddToList.setTitle("ADD", for: .normal)
        buttonAddToList.setImage(UIImage(named: "ic_like"), for: .normal)
        //buttonAddToList.titleLabel?.textColor = .white
        buttonAddToList.layer.cornerRadius = 20
        buttonAddToList.tintColor = .white
        //buttonAddToList.centerAndImage(withSpacing: 10.0)
        buttonAddToList.addTarget(self, action: #selector(addLocationButtonAction), for: .touchUpInside)
        infoWindow.addSubview(buttonAddToList)
        
        let routeButton = UIButton(frame: CGRect.init(x: buttonAddToList.frame.size.width  + 60 , y: lbl1.frame.origin.y + lbl1.frame.size.height + 16, width: 40, height: 40))
        routeButton.backgroundColor = UIColor(red:0.05, green:0.51, blue:1, alpha:1) //UIColor.blue
        // routeButton.setTitle("ROUTE HERE", for: .normal)
        routeButton.setImage(UIImage(named: "routing"), for: .normal)
        //routeButton.titleLabel?.textColor = .white
        routeButton.layer.cornerRadius = 20
        routeButton.tintColor = .white
        //to do
        routeButton.addTarget(self, action: #selector(openTrackerInBrowser), for: .touchUpInside)
        
        infoWindow.addSubview(routeButton)
        
        self.view.addSubview(infoWindow)
        
    }
    
    @objc func openTrackerInBrowser(){
        
        //lat: String, long: String, dlat: String, dlong: String
        let lat = locationManager.location?.coordinate.latitude
        let long = locationManager.location?.coordinate.longitude
        let dlat = self.selectedPlaceCoordinate?.latitude
        let dlong = self.selectedPlaceCoordinate?.longitude
        
        if let url1 = URL(string: "comgooglemaps://") {
            if UIApplication.shared.canOpenURL(url1) {
                
                let url = "comgooglemaps-x-callback://?saddr=\(String(describing: lat!)),\(String(describing: long!))&daddr=\(dlat ?? USA_LAT),\(dlong ?? USA_LONG)&directionsmode=driving&x-success=myapp://?resume=true&x-source=MyAppName"
                if let url1 = URL(string: url) {
                    // UIApplication.shared.openURL(url1)
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(url1, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(url1)
                    }
                }
            } else {
                print("Can't use comgooglemaps://")
            }
        }
    }
    
    
    func startMonitoringLocation() {
        if CLLocationManager.locationServicesEnabled() {
            locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
            locationManager.activityType = CLActivityType.automotiveNavigation
            locationManager.distanceFilter = 1
            locationManager.headingFilter = 1
            locationManager.requestWhenInUseAuthorization()
            locationManager.startMonitoringSignificantLocationChanges()
            locationManager.startUpdatingLocation()
        }
        
    }
    
    func stopMonitoringLocation() {
        locationManager.stopMonitoringSignificantLocationChanges()
        locationManager.stopUpdatingLocation()
    }
    
    func addCurrentLocationMarker() {
        currentLocationMarker?.map = nil
        currentLocationMarker = nil
        if let location = locationManager.location {
            currentLocationMarker = GMSMarker(position: location.coordinate)
            currentLocationMarker?.icon = UIImage(named: "broadcast")
            currentLocationMarker?.map = mapView
            currentLocationMarker?.rotation = locationManager.location?.course ?? 0
        }
    }
    
    func zoomToCoordinates(_ coordinates: CLLocationCoordinate2D) {
        let camera = GMSCameraPosition.camera(withLatitude: coordinates.latitude, longitude: coordinates.longitude, zoom: 17)
        mapView.camera = camera
        //addCurrentLocationMarker()
    }
    
    
}

extension AddPropertyViewController: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        //mapView.clear()
        infoWindow.removeFromSuperview()
        
        let fields: GMSPlaceField = GMSPlaceField(rawValue:UInt(GMSPlaceField.name.rawValue) |
            UInt(GMSPlaceField.placeID.rawValue) |
            UInt(GMSPlaceField.coordinate.rawValue) |
            GMSPlaceField.addressComponents.rawValue |
            GMSPlaceField.formattedAddress.rawValue)!
        viewController.placeFields = fields
        
        self.btnAddress.setTitle( place.formattedAddress, for: .normal)
        self.selectedPlaceAddress = place.formattedAddress!
        print(place.formattedAddress!)
        
        let tappedPlace = TappedPlace()
        
        for (index,element) in (place.addressComponents?.enumerated())!{
            
            print(place.addressComponents![index].shortName ?? place.addressComponents![index].name )
            print(element)
            
            if place.addressComponents![index].types.contains("street_number")
            {
                tappedPlace.street = place.addressComponents![index].shortName ?? place.addressComponents![index].name
            }
            else if  place.addressComponents![index].types.contains("route")
            {
                tappedPlace.street = tappedPlace.street + " " +  (place.addressComponents![index].shortName ?? place.addressComponents![index].name)
            }
            else if  place.addressComponents![index].types.contains("locality")
            {
                tappedPlace.city = (place.addressComponents![index].shortName ?? place.addressComponents![index].name)
            }
            else if  place.addressComponents![index].types.contains("administrative_area_level_1")
            {
                tappedPlace.state = place.addressComponents![index].shortName ?? place.addressComponents![index].name
                
            }
            else if  place.addressComponents![index].types.contains("postal_code")
            {
                tappedPlace.zip = place.addressComponents![index].shortName ?? place.addressComponents![index].name
            }
            else{
                print("to do")
            }
        }
        
        print(tappedPlace.street + tappedPlace.state + tappedPlace.city + tappedPlace.zip)
        
        tappedPlace.latitude = place.coordinate.latitude
        tappedPlace.longitude = place.coordinate.longitude
        
        print(place.coordinate.latitude)
        print(place.coordinate.longitude)
        
        DispatchQueue.main.async {
            
            globalTappedPlace = tappedPlace
            self.selectedPlaceCoordinate = place.coordinate
            self.dismiss(animated: true) {
                NotificationCenter.default.post(name:NSNotification.Name("placeFound"),object: nil)
                //
            }
        }
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // Handle the error
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        // Dismiss when the user canceled the action
        dismiss(animated: true, completion: nil)
    }}

extension AddPropertyViewController: CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("location manager error -> \(error.localizedDescription)")
        AppConfigs.showSnacbar(message: "Please enable location permission", textColor: .red)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            break
        case .restricted:
            break
        case .denied:
            stopMonitoringLocation()
            break
        default:
            //addCurrentLocationMarker()
            startMonitoringLocation()
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        //Finally stop updating location otherwise it will come again and again in this delegate
        if let lastLocation = locations.last {
            currentLocationMarker?.position = lastLocation.coordinate
            currentLocationMarker?.rotation = lastLocation.course
            self.zoomToCoordinates(lastLocation.coordinate)
        }
        self.locationManager.stopUpdatingLocation()
        
        
        //        let location = locations.last
        //
        //        let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 17.0)
        //
        //        self.mapView?.animate(to: camera)
        //
        //        //Finally stop updating location otherwise it will come again and again in this delegate
        //        self.locationManager.stopUpdatingLocation()
        
    }
    
    func cameraMoveToLocation(toLocation: CLLocationCoordinate2D?) {
        if toLocation != nil {
            mapView.camera = GMSCameraPosition.camera(withTarget: toLocation!, zoom: 17)
        }
    }
}

extension AddPropertyViewController: GMSMapViewDelegate{
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        print("didTapInfoWindowOf")
    }
    
    /* handles Info Window long press */
    func mapView(_ mapView: GMSMapView, didLongPressInfoWindowOf marker: GMSMarker) {
        print("didLongPressInfoWindowOf")
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        
        infoWindow.removeFromSuperview()
    }
    
    /* set a custom Info Window */
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        
        
        return UIView()
    }
    
    //MARK - GMSMarker Dragging
    func mapView(_ mapView: GMSMapView, didBeginDragging marker: GMSMarker) {
        infoWindow.removeFromSuperview()
        print("didBeginDragging")
    }
    func mapView(_ mapView: GMSMapView, didDrag marker: GMSMarker) {
        infoWindow.removeFromSuperview()
        print("didDrag")
    }
    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
        print("didEndDragging")
        marker.tracksInfoWindowChanges = true
        mapView.selectedMarker = marker
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        print("did tap a marker")
        
        print(marker.title)
        print(marker.position.latitude)
        marker.tracksInfoWindowChanges = true
        showMarkerWithTitleSnippet(title: self.selectedPlaceAddress, snippet: marker.snippet!, position: marker.position)
        
        return false
        
    }
    
    // let the custom infowindow follows the camera
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        
    }
    
    
    func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D) {
        infoWindow.removeFromSuperview()
        
        if Network.reachability.status == .unreachable{
            AppConfigs.updateUserInterface(view: view)
        }else{
            
            mapView.selectedMarker = nil
            print("You tapped at \(coordinate.latitude), \(coordinate.longitude)")
            marker.isDraggable = true
            //29.709151, -95.456857
            marker.position.latitude = coordinate.latitude// 29.709151//coordinate.latitude
            marker.position.longitude = coordinate.longitude//-95.456857 // coordinate.longitude
            print(coordinate.latitude, coordinate.longitude)
            marker.snippet = "tapped"
            self.marker.icon = UIImage(named: "Shape")
            self.marker.map = mapView
            self.marker.appearAnimation = .pop
            mapView.selectedMarker = self.marker
            marker.tracksInfoWindowChanges = true
            let point:CGPoint = mapView.projection.point(for: self.marker.position)
            let camera:GMSCameraUpdate = GMSCameraUpdate.setTarget(mapView.projection.coordinate(for: point))
            mapView.animate(with: camera)
            
            var addressString : String = ""
            let ceo: CLGeocoder = CLGeocoder()
            let loc: CLLocation = CLLocation(latitude:coordinate.latitude, longitude: coordinate.longitude)
            let tappedPlace = TappedPlace()
            ceo.reverseGeocodeLocation(loc, completionHandler:
                {(placemarks, error) in
                    if (error != nil)
                    {
                        print("reverse geodcode fail: \(error!.localizedDescription)")
                    }
                    let pm = placemarks! as [CLPlacemark]
                    
                    if pm.count > 0 {
                        let pm = placemarks![0]
                        
                        print("pm.country-->",pm.country ?? "")
                        print("locality-->", pm.locality ?? "")
                        print("subLocality-->", pm.subLocality ?? "")
                        print("thoroughfare-->", pm.thoroughfare ?? "")
                        print("postalCode-->", pm.postalCode ?? "")
                        print("subThoroughfare-->", pm.subThoroughfare ?? "")
                        print("administrativeArea", pm.administrativeArea ?? "")
                        print(pm.inlandWater ?? "")
                        print(pm.isoCountryCode ?? "")
                        print("name-->",pm.name ?? "")
                        print("region-->", pm.region ?? "" )
                        //4613 Locust Street, Bellaire, TX, USA
                        
                        if pm.name != nil {
                            addressString = addressString + pm.name! + ", "
                            tappedPlace.street = pm.name!
                        }
                        
                        if pm.subLocality != nil {
                            addressString = addressString + pm.subLocality! + ", "
                            //tappedPlace.street = addressString
                        }
                        
                        if pm.locality != nil {
                            addressString = addressString + pm.locality! + ", "
                            tappedPlace.city = pm.locality!
                        }
                        
                        if pm.administrativeArea != nil {
                            addressString = addressString + pm.administrativeArea! + " "
                            tappedPlace.state = pm.administrativeArea!
                        }
                        
                        if pm.postalCode != nil {
                            addressString = addressString + pm.postalCode! + ",  "
                            tappedPlace.zip = pm.postalCode!
                        }
                        
                        if pm.country != nil {
                            addressString = addressString + pm.country!
                            tappedPlace.country = pm.country!
                        }
                        
                        print(addressString)
                        
                        self.marker.title = addressString
                        globalSelectedProperty.propertyAddress = addressString
                        globalSelectedProperty.latitude = Double(coordinate.latitude)
                        globalSelectedProperty.longitude = Double(coordinate.longitude)
                        
                        tappedPlace.latitude = Double(coordinate.latitude)
                        tappedPlace.longitude = Double(coordinate.longitude)
                        
                        globalTappedPlace = tappedPlace
                        self.showMarkerWithTitleSnippet(title: addressString, snippet: "tapped", position: self.marker.position)
                    }
            })
        }
        
    }
    
}
