//
//  TeamMemberDetailsViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 9/10/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit
class TeamMemberDetailsViewController: UIViewController {
    
    @IBOutlet weak var btnName: UIButton!
    @IBOutlet weak var btnEmail: UIButton!
    @IBOutlet weak var btnPassword: UIButton!
    @IBOutlet weak var btnMobile: UIButton!
    
    @IBOutlet weak var lblSecondSeperator: UILabel!
    @IBOutlet weak var lblFirstSeperator: UILabel!
    @IBOutlet weak var lblThirdSeperator: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    var user: UnregisteredInvitation?
    var scout: Scout?
    var parentString : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpProfileUI()
        self.setUpNavigationBar()
    }
    
    func setUpProfileUI() {
        
        imgView.borderColor = self.profileBlueRoundColor
        imgView.borderWidth = 5
        lblFirstSeperator.backgroundColor = self.profileSeperatorColor
        lblSecondSeperator.backgroundColor = self.profileSeperatorColor
        lblThirdSeperator.backgroundColor = self.profileSeperatorColor
        FontAndColorConfigs.setDynamicButtonsFontSize(buttons: [btnName, btnEmail, btnMobile, btnPassword], type: TextType.ListTitle.rawValue)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if self.parentString == ""{
            
        }else if self.parentString == "scout"{
            //let user = user
            //            lblName.text = (scout?.firstName ?? "") + " " +  (scout?.lastName ?? "")
            //            lblEmail.text = user?.email
            //            lblPhoneNumber.text =  "   " //user.phoneNumber
            btnName.setTitle((scout?.firstName ?? "") + " " +  (scout?.lastName ?? ""), for: .normal)
            btnEmail.setTitle(scout?.url, for: .normal)
            btnEmail.setImage(UIImage(named: "ulinksss"), for: .normal)
            self.lblSecondSeperator.isHidden = true
            btnPassword.isEnabled =  false
            self.btnPassword.isHidden = true
            btnMobile.isEnabled = false
            self.btnMobile.isHidden = true
            self.lblThirdSeperator.isHidden = true
            
        }else{
            let name = (user?.firstName ?? "") + " " +  (user?.lastName ?? "")
            btnName.setTitle(name, for: .normal)
            btnEmail.setTitle(user?.email, for: .normal)
            btnEmail.setImage(UIImage(named: "new_ic_email"), for: .normal)
            btnPassword.setTitle("**********", for: .normal)
            btnMobile.setTitle(user?.phoneNumber ?? "01882348313", for: .normal)
            
            if user?.photo != nil{
                if user?.photo != ""{
                    self.showPicture(url: (user?.photo)!, imageView: self.imgView)
                }else{
                    print("photo is empty")
                }
            }else
            {
                print("photo is nil")
            }
        }
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        //self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func linkButtonClicked(_ sender: Any) {
        
        if self.parentString == "scout"{
            
            let url = scout?.url ?? ""
            
            UIPasteboard.general.string = url
            if let myString = UIPasteboard.general.string {
                AppConfigs.showSnacbar(message: "Copied to Clipboard", textColor: .green)
                print(myString)
            }
            
            guard let url2 = URL(string: scout?.url ?? "") else {
                return
            }
            if UIApplication.shared.canOpenURL(url2) {
                UIApplication.shared.open(url2, options: [:], completionHandler: nil)
            }
        }
    }
    
}
