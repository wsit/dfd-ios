//
//  MyTeamsViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 8/27/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit
import Alamofire

class MyTeamsViewController: UIViewController {
    
    @IBOutlet weak var myTeamsTableView: UITableView!
    @IBOutlet weak var addTeamButton: UIButton!
    
    var users: [UnregisteredInvitation] = []
    var unregisteredUsers: [UnregisteredInvitation] = []
    var allTeamMembers: [UnregisteredInvitation] = []
    var jsonResponse: MyTeamResponse?
    
    var items = ["Kevin Wheeler", "Kevin Wheeler", "Kevin Wheeler", "Kevin Wheeler", "Kevin Wheeler", "Kevin Wheeler", "Kevin Wheeler"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: "NewMemberTableViewCell", bundle: nil)
        myTeamsTableView.register(nib, forCellReuseIdentifier: "NewMemberTableViewCell")
        self.myTeamsTableView.tableFooterView = UIView()
        self.setUpNavigationBar()
        
        if AppConfigs.getCurrentUserInfo().isAdmin == true{
            addTeamButton.isHidden = false
        }else{
            addTeamButton.isHidden = true

        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // FIXME: Remove code below if u're using your own menu
        setupNavForDefaultMenu()
        
        
        if AppConfigs.getCurrentUserInfo().isAdmin == true{
            addTeamButton.isHidden = false
            
            addTeamButton.setImage(UIImage(named: "gradient_add"), for: .normal)
            
        }else{
            addTeamButton.isHidden = true
            addTeamButton.setImage(nil, for: .normal)

        }
        
        // Add left bar button item
        let leftBarItem = UIBarButtonItem(image: UIImage(named: "leftarrow"), style: .plain, target: self, action: #selector(toggleSideMenu))
        leftBarItem.tintColor = .white
        navigationItem.leftBarButtonItem = leftBarItem
        
        self.title = "My Team"
        ///api/teams/?limit=10&offset=0
        
                if Network.reachability.status == .unreachable{
                    AppConfigs.updateUserInterface(view: view)
                }else{
                    allTeamMembers.removeAll()
                    users.removeAll()
                    unregisteredUsers.removeAll()
                    
                    let url : String = UrlManager.getTeamListURL()
                    
                    print(url)
                    self.showSpinner(onView: view)
                    getAllTeamMembers(urlString: url)
                }
        
    }
    
    
    private func setupNavForDefaultMenu() {
        // Revert navigation bar translucent style to default
        //navigationBarNonTranslecentStyle()
        // Update side menu after reverted navigation bar style
        sideMenuManager?.instance()?.menu?.isNavbarHiddenOrTransparent = true
        navigationItem.hidesBackButton = true
        
    }
    
    @objc func toggleSideMenu() {
        sideMenuManager?.toggleSideMenuView()
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        guard let signInVC = storyBoard.instantiateViewController(withIdentifier: "SMNavigationController") as? SMNavigationController  else
        { return  }
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        
        present(signInVC, animated: true, completion: nil)
        
    }
    
    @IBAction func addTeamMemberClicked(_ sender: Any) {
        
        
        if AppConfigs.getCurrentUserInfo().upgradeInfo?.isTeamInvitable == true{
            
            showInputDialog(type: "email", title: "Add Team Member", subtitle: "Please Provide Team Member Email", actionTitle: "Confirm", cancelTitle: "Cancel", inputPlaceholder: "email", inputKeyboardType: .default){
                (input:String?) in
                print("The new name is \(input ?? "")")
                self.showSpinner(onView: self.view)
                self.handleAddNewMember(email: input!)
            }
        }else{
            //self.showAlert(message: "Please choose team plan and enjoy this feature", title: "Unauthorized")

            let alertController = UIAlertController(title: "Unauthorized", message: "Only Team can enjoy this feature. Please upgrade your account.", preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
//                let settingsSB: UIStoryboard = UIStoryboard(name: "Settings", bundle: nil)
//                let vc = settingsSB.instantiateViewController(withIdentifier: "PackageViewController") as! PackageViewController
//                //vc.modalPresentationStyle = .fullScreen
//                self.navigationController?.pushViewController(vc, animated: true)
//                
                let settingsSB: UIStoryboard = UIStoryboard(name: "Settings", bundle: nil)
                let vc = settingsSB.instantiateViewController(withIdentifier: "PackageViewController") as! PackageViewController
                self.navigationController?.pushViewController(vc, animated: true)
//                vc.modalPresentationStyle = .fullScreen
//                self.present(vc, animated: true, completion: nil)
            }//UIAlertAction(title: "OK", style: .default, handler: nil)
            
            let notNow = UIAlertAction(title: "Not Now", style: .cancel, handler: nil)
            alertController.addAction(OKAction)
            alertController.addAction(notNow)
            self.present(alertController, animated: true, completion: nil)
        }
        
    }
    
    
    func handleAddNewMember(email:String )  {
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        
        let parameters = [ K.APIParameterKey.email: email,
                           K.APIParameterKey.user: AppConfigs.getCurrentUserInfo().id!
            ] as [String : Any]
        print(parameters)
        
        let url = UrlManager.getAddTeamURL()
        Alamofire.request(url, method: .post, parameters: parameters as Parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
            case.success(let data):
                print("success",data)
                //self.removeSpinner()
                let statusCode = response.response?.statusCode
                print(statusCode!)
                let addMemberResponse = try? JSONDecoder().decode(AddMemberResponse.self, from: response.data!)
                
                if statusCode == 200{
                    DispatchQueue.main.async {
                        self.removeSpinner()
                        AppConfigs.showSnacbar(message: (addMemberResponse?.message)!, textColor: .green)
                        self.allTeamMembers.removeAll()
                        //self.showSpinner(onView: self.view)
                        self.getAllTeamMembers(urlString: UrlManager.getTeamListURL())
                    }
                }
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: (addMemberResponse?.message)!, textColor: .green)
                }
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)

                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
                self.removeSpinner()
            }
            self.removeSpinner()
        }
    }
    
    func handleDeleteTeamMember(id:Int, urlString: String)  {
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        
        let parameters = [ K.APIParameterKey.memberID: id ]
        print(parameters)
        
        
        Alamofire.request(urlString, method: .delete, parameters: parameters as Parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
            case.success(let data):
                print("success",data)
                //self.removeSpinner()
                let statusCode = response.response?.statusCode
                print(statusCode!)
                
                
                if statusCode == 200{
                    
                    let deleteTeamMemberResponse = try? JSONDecoder().decode(DeleteTeamMemberResponse.self, from: response.data!)
                    
                    if deleteTeamMemberResponse?.status == true{
                        
                        DispatchQueue.main.async {
                            self.removeSpinner()
                            AppConfigs.showSnacbar(message: (deleteTeamMemberResponse?.message)!, textColor: .green)
                            self.allTeamMembers.removeAll()
                            //self.showSpinner(onView: self.view)
                            self.getAllTeamMembers(urlString: UrlManager.getTeamListURL() )
                        }
                    }else{
                        AppConfigs.showSnacbar(message: (deleteTeamMemberResponse?.message)!, textColor: .green)
                    }
                    
                }
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)

                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                 self.removeSpinner()
            }
            self.removeSpinner()
        }
    }
    
    func getAllTeamMembers(urlString:String)  {
        
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success",data)
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    
                    let teamMembers = try? JSONDecoder().decode(MyTeamResponse.self, from: response.data!)
                    self.jsonResponse = teamMembers
                    let userList = teamMembers?.users
                    print("userList------\n",userList)
                    let pendingUserList = teamMembers?.unregisteredInvitations
                    self.allTeamMembers = userList! + pendingUserList!
                    print("allTeamMembers-----\n", self.allTeamMembers)
                    
                    DispatchQueue.main.async {
                        print("allTeamMembers------------\n",self.allTeamMembers)
                        self.removeSpinner()//self.view.activityStopAnimating()
                        self.myTeamsTableView.reloadData()
                        
                    }
                }
                    
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)

                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
    
    @objc func loadTable(tableview:UITableView) {
        self.myTeamsTableView.reloadData()
    }
}

extension MyTeamsViewController: UITableViewDataSource {
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if allTeamMembers.count == 0 {
            tableView.setEmptyMessage("Nothing yet to show")
        } else {
            tableView.restore()
        }
        return allTeamMembers.count
        //return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewMemberTableViewCell", for: indexPath) as! NewMemberTableViewCell
        
        cell.separatorLine.backgroundColor = self.listSeperatorColor
        cell.memberNameLabel.textColor = UIColor(red:0, green:0, blue:0, alpha:0.8)
        cell.memberImageView.layer.cornerRadius = 19
        cell.memberImageView.layer.borderWidth = 0
        
        
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [cell.memberNameLabel], type: TextType.ListTitle.rawValue)
        
        if allTeamMembers.count > indexPath.row{
            let member = allTeamMembers[indexPath.row]
            
            if member.firstName != nil || member.lastName != nil{
                var fullName: String = member.firstName! + " " + member.lastName!
                
                if member.photo != nil{
                    if member.photo != ""{
                        self.showPicture(url: (member.photo!), imageView: cell.memberImageView)
                    }else{
                        print("photo is empty")
                    }
                }else
                {
                    print("photo is nil")
                }
                
                if member.status == 0{
                    cell.pendingImageView.isHidden = false
                    //fullName = fullName + " (pending)"
                    cell.pendingImageView.isHidden = false
                    let string_to_color = "pending"
                    let range = (fullName as NSString).range(of: string_to_color)
                    let attribute = NSMutableAttributedString.init(string: fullName)
                    attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red , range: range)
                    // cell.memberNameLabel.attributedText = attribute
                    cell.memberNameLabel.text = fullName
                }else{
                    cell.memberNameLabel.text = fullName
                }
            }else{
                if member.status == 0{
                    cell.pendingImageView.isHidden = false
                    cell.pendingImageView.isHidden = false
                    let email = member.email ?? ""
                    
                    let emailString = email + " (pending)"
                    let string_to_color = "pending"
                    
                    let range = (emailString as NSString).range(of: string_to_color)
                    let attribute = NSMutableAttributedString.init(string: emailString)
                    attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red , range: range)
                    //cell.memberNameLabel.attributedText = attribute
                    cell.memberNameLabel.text = email
                }else{
                    cell.memberNameLabel.text = member.email
                }
            }
            
            if member.firstName != nil || member.lastName != nil{
                if member.firstName == AppConfigs.getCurrentUserInfo().firstName &&  member.firstName == AppConfigs.getCurrentUserInfo().firstName{
                     
                    cell.memberNameLabel.text = "Me"
                    cell.memberNameLabel.textColor = self.greenButtonBackground
                }else{
                     cell.memberNameLabel.textColor = UIColor(red:0, green:0, blue:0, alpha:0.8)
                }
            }
            
            if member.isAdmin != nil{
                if member.isAdmin == true{
                    cell.memberNameLabel.textColor = UIColor.red
                }
            }
            
            
        }
        return cell
    }
    
}

extension MyTeamsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if allTeamMembers.count > indexPath.row{
            let user = allTeamMembers[indexPath.row]
            print(user.email!)
            if let status = user.status{
                print(status)
                if status == 0{
                    AppConfigs.showSnacbar(message: "Invitation Pending! Details not available", textColor: .red)
                }
            }else{
                

                if user.firstName != nil || user.lastName != nil{
                    if user.firstName == AppConfigs.getCurrentUserInfo().firstName &&  user.firstName == AppConfigs.getCurrentUserInfo().firstName{
                         //me
                        
                    }else{
                        
                        let mainStoryboard: UIStoryboard = UIStoryboard(name: "MyTeam", bundle: nil)
                        let vc = mainStoryboard.instantiateViewController(withIdentifier: "TeamMemberDetailsViewController") as! TeamMemberDetailsViewController
                        vc.user = user
                        vc.parentString = "team"
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let data = self.allTeamMembers[indexPath.row]
        let delete = UIContextualAction(style: .destructive, title: "Delete") { (action, view, nil) in
            print(data.id!)
            
            let alert = UIAlertController(title: "Are you sure you want to remove ", message: " ", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action) in
                
                let invitedBy = data.invitedBy
                if invitedBy != nil{
                    let url = UrlManager.baseURL() + UrlManager.apiString() + "/user/\(data.id!)/"
                    self.handleDeleteTeamMember(id: data.id!, urlString: url)
                }else{
                    let url = UrlManager.getTeamListURL() + "\(data.id!)/"
                    self.handleDeleteTeamMember(id: data.id!, urlString: url)
                }
                
                print("delete")
            }))
            self.present(alert, animated: true, completion: nil)
        }
        delete.backgroundColor = UIColor.white //.red
        delete.image =  UIImage(named: "new_ic_delete")//?.colored(in: .red)
        
        var config = UISwipeActionsConfiguration(actions: [])
       
        if data.id == AppConfigs.getCurrentUserInfo().id{
            config = UISwipeActionsConfiguration(actions: [])
        }else{
            config = UISwipeActionsConfiguration(actions: [delete])
        }
         config.performsFirstActionWithFullSwipe = false
        
        return  config
        
    }
    
    
}
