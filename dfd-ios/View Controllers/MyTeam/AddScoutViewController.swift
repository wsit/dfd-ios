//
//  AddScoutViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 1/2/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//

import UIKit
import Alamofire
import SwiftValidator
import PasswordTextField
import PhoneNumberKit

class AddScoutViewController: UIViewController, ValidationDelegate {
    
    @IBOutlet weak var lblFNTitle: UILabel!
    @IBOutlet weak var lblLNTitle: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblMobile: UILabel!
    
    @IBOutlet weak var firstNameErrorLabel: UILabel!
    @IBOutlet weak var lastNameErrorLabel: UILabel!
    @IBOutlet weak var emailErrorLabel: UILabel!
    @IBOutlet weak var mobileErrorLabel: UILabel!
    
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPhoneNumber: PhoneNumberTextField!
    @IBOutlet weak var btnCreateScout: UIButton!
    
    
    
    let validator = Validator()
    var code:String = ""
    var rawNumber: String = ""
    var isNumberValid: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpSignUpView()
        configureUIValidation()
        
        FontAndColorConfigs.setDynamicButtonsFontSize(buttons: [btnCreateScout], type: TextType.ListTitle.rawValue)
        
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [firstNameErrorLabel, lastNameErrorLabel, lastNameErrorLabel, emailErrorLabel, mobileErrorLabel], type: TextType.ListSubTitle.rawValue)
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [lblFNTitle,lblLNTitle, lblEmail, lblMobile ], type: TextType.ListSubTitle.rawValue)
        
    }
    
    
    @IBAction func backButtonClicked(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func createButtonClicked(_ sender: Any) {
        
        print("Validating...")
        validator.validate(self)
    }
    
    func setUpSignUpView()  {
        
        txtFirstName.delegate = self;  txtEmail.delegate = self; txtLastName.delegate = self; txtPhoneNumber.delegate = self;
        
        txtFirstName.underlined(); txtLastName.underlined(); txtEmail.underlined(); txtPhoneNumber.underlined();
        
        lblFNTitle.textColor = self.listSubTitleColor
        lblEmail.textColor = self.listSubTitleColor
        lblLNTitle.textColor = self.listSubTitleColor
        lblMobile.textColor = self.listSubTitleColor
        
        
        txtEmail.font = ProNovaR16; txtFirstName.font = ProNovaR16; txtLastName.font = ProNovaR16; txtPhoneNumber.font = ProNovaR16;
        txtPhoneNumber.placeholder = "    ex: +1-541-754-3010"
        
        btnCreateScout.backgroundColor = self.buttonBackgroundColor
    }
    
    func configureUIValidation() {
        
        // Validation Rules are evaluated from left to right.
        validator.registerField(txtFirstName,errorLabel: firstNameErrorLabel , rules: [RequiredRule()])
        validator.registerField(txtLastName, errorLabel: lastNameErrorLabel ,rules: [RequiredRule()])
        // You can pass in error labels with your rules
        // You can pass in custom error messages to regex rules (such as ZipCodeRule and EmailRule)
        validator.registerField(txtEmail, errorLabel: emailErrorLabel ,rules: [RequiredRule(), EmailRule()])
        
        // You can validate against other fields using ConfirmRule
        validator.registerField(txtPhoneNumber,errorLabel: mobileErrorLabel , rules: [RequiredRule()])
        
        validator.styleTransformers(success:{ (validationRule) -> Void in
            print("here")
            // clear error label
            validationRule.errorLabel?.isHidden = true
            validationRule.errorLabel?.text = ""
            
            if let textField = validationRule.field as? UITextField {
                //                textField.layer.borderColor = UIColor.green.cgColor
                //                textField.layer.borderWidth = 0.5
            } else if let textField = validationRule.field as? UITextView {
                //                textField.layer.borderColor = UIColor.green.cgColor
                //                textField.layer.borderWidth = 0.5
            }
        }, error:{ (validationError) -> Void in
            print("error")
            validationError.errorLabel?.isHidden = false
            validationError.errorLabel?.text = validationError.errorMessage
            if let textField = validationError.field as? UITextField {
                textField.layer.borderColor = UIColor.red.cgColor
                textField.layer.borderWidth = 1.0
            } else if let textField = validationError.field as? UITextView {
                textField.layer.borderColor = UIColor.red.cgColor
                textField.layer.borderWidth = 1.0
            }
        })
    }
    
    func clearTextfields() {
        txtFirstName.text = ""; txtFirstName.placeholder = "First Name"
        txtLastName.text = ""; txtLastName.placeholder = "Last Name"
        txtEmail.text = ""; txtEmail.placeholder = "Email"
        txtPhoneNumber.text = "" ; txtPhoneNumber.placeholder = "ex: +1-541-754-3010"
        
    }
    
    
    
    
    func validationSuccessful() {
        

        if txtFirstName.isEmpty == true || txtLastName.isEmpty == true || txtEmail.isEmpty == true || txtPhoneNumber.isEmpty {
            
        }else{
            print("txtMobileNo---->",txtPhoneNumber.nationalNumber)
            print(self.rawNumber)
            
            if txtPhoneNumber.isValidNumber == true{
                mobileErrorLabel.isHidden = true
        
                if Network.reachability.status == .unreachable{
                    AppConfigs.updateUserInterface(view: view)
                }else{
                    
                    handleAddScout(email: txtEmail.text!, phone: txtPhoneNumber.nationalNumber, firstName: txtFirstName.text!, lastName: txtLastName.text!)
                }
            }else{
                mobileErrorLabel.isHidden = false
                mobileErrorLabel.text = "Invalid Mobile Number"
                txtPhoneNumber.layer.borderWidth = 1.0
                txtPhoneNumber.borderColor = .red
            }
        }
        //        }
    }
    
    func handleAddScout(email:String,  phone: String, firstName: String, lastName :String){
        
        self.view.activityStartAnimating(activityColor: .black, backgroundColor: .white)
        
        let parameters = [
            "email": email
            , "phone_number": phone
            , "first_name": firstName
            , "last_name": lastName
            ] as [String : Any]
        print(parameters)
        
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        
        let url = UrlManager.getAddScoutURL()
        Alamofire.request(url, method: .post, parameters: parameters as Parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
            case.success(let data):
                print("success",data)
                //self.removeSpinner()
                let statusCode = response.response?.statusCode
                print(statusCode!)
                let addScoutResponse = try? JSONDecoder().decode(AddScoutResponse.self, from: response.data!)
                if statusCode == 200{
                    DispatchQueue.main.async {
                        self.removeSpinner()
                        AppConfigs.showSnacbar(message: (addScoutResponse?.message)!, textColor: .green)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ScoutAddedNotif"), object: nil, userInfo: nil)
                        self.dismiss(animated: true, completion: nil)
                    }
                }
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: (addScoutResponse?.message)!, textColor: .green)
                }
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)

                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
             }
            self.removeSpinner()
        }
    }
    
    func validationFailed(_ errors:[(Validatable ,ValidationError)]) {
        // turn the fields to red
        for (field, error) in errors {
            if let field = field as? UITextField {
                field.layer.borderColor = UIColor.red.cgColor
                field.layer.borderWidth = 1.0
            }
            error.errorLabel?.text = error.errorMessage // works if you added labels
            error.errorLabel?.isHidden = false
        }
        
        if txtPhoneNumber.isEmpty == false{
            if txtPhoneNumber.isValidNumber == true{
                mobileErrorLabel.isHidden = true
                
            }else{
                mobileErrorLabel.isHidden = false
                mobileErrorLabel.text = "Invalid Mobile Number"
                txtPhoneNumber.layer.borderWidth = 1.0
                txtPhoneNumber.borderColor = .red
            }
        }else{
            mobileErrorLabel.text = "This field is required"
        }
    }
    
    func validationFailed(errors: [UITextField : ValidationError]) {
        print("Validation FAILED!")
    }
    
    
}


extension AddScoutViewController: UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textField.underlined()
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor.white.cgColor //UIColor(red:0.07, green:0.82, blue:0.41, alpha:1).cgColor
        textField.layer.shadowOffset = CGSize.zero
        textField.layer.shadowColor = UIColor.white.cgColor// UIColor(red:0.87, green:0.99, blue:0.89, alpha:1).cgColor
        textField.layer.shadowOpacity = 1
        textField.layer.shadowRadius = 7
        mobileErrorLabel.isHidden = true
        textField.underlined()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor.white.cgColor//UIColor(red:0.77, green:0.99, blue:0.75, alpha:1).cgColor
        
        textField.layer.shadowColor = UIColor.white.cgColor
        textField.underlined()//UIColor(red:0.87, green:0.99, blue:0.89, alpha:1).cgColor
    }
    
}
