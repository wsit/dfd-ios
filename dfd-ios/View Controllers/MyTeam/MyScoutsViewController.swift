//
//  MyScoutsViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 8/26/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit
import Alamofire

class MyScoutsViewController: UIViewController {
    
    @IBOutlet weak var scoutsTableView: UITableView!
    @IBOutlet weak var addScoutButton: UIButton!
    
    var items = ["Bran Stack", "Jon Snow", "Daynaris Targerian", "Tyrion Lanaster", "Arya Stark", "Khal Drogo", "The Knight King"]
    
    var allList: [Scout] = []
    var url : String = ""
    var nextUrl: String = ""
    var previousUrl: String = ""
    var jsonResponse: MyScoutResponse?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: "ScoutsTableViewCell", bundle: nil)
        scoutsTableView.register(nib, forCellReuseIdentifier: "ScoutsTableViewCell")
        self.scoutsTableView.tableFooterView = UIView()
        
        self.setUpNavigationBar()
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadList(_:)), name: NSNotification.Name(rawValue: "ScoutAddedNotif"), object: nil)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if Network.reachability.status == .unreachable{
            AppConfigs.updateUserInterface(view: view)
        }else{
            allList.removeAll()
            let url : String = UrlManager.getScoutURL() //UrlManager.baseURL() + "/api/scout/?limit=10&offset=0"
            
            print(url)
            self.showSpinner(onView: view)
            getAllScouts(urlString: url)
        }
    }
    
    @objc func reloadList(_ notification: NSNotification) {
        allList.removeAll()
        let url : String = UrlManager.getScoutURL() //UrlManager.baseURL() + "/api/scout/?limit=10&offset=0"
        
        print(url)
        //self.showSpinner(onView: view)
        getAllScouts(urlString: url)
        
    }
    
    func getAllScouts(urlString: String) {
        
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success",data)
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    
                    let allLists = try? JSONDecoder().decode(MyScoutResponse.self, from: response.data!)
                    self.jsonResponse = allLists
                    
                    var items: [Scout] = self.allList
                    items.append(contentsOf: (allLists?.results)!)
                    
                    self.allList.removeAll()
                    self.allList = items
                    
                    DispatchQueue.main.async {
                        print("allProjects------------\n",self.allList)
                        self.removeSpinner()//self.view.activityStopAnimating()
                        self.perform(#selector(self.loadTable), with: nil, afterDelay: 0.0)
                    }
                }
                    
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                
                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
    @objc func loadTable() {
        self.scoutsTableView.reloadData()
    }
    
    @IBAction func addScoutClicked(_ sender: Any) {
        
        //        alertWithTF(title: "Add Scout", message: "Please give below information")
        //        {(fN:String?, ln: String?) in
        //            print(fN!)
        //            print(ln ?? "")
        //            self.showSpinner(onView: self.view)
        //            self.handleAddNewScout(fName: fN!, lName: ln ?? "")
        //        }
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddScoutViewController") as! AddScoutViewController
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
    
    func handleAddNewScout(fName:String, lName:String)  {
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        
        let parameters = [ K.APIParameterKey.firstName: fName,
                           K.APIParameterKey.lastName: lName,
                           K.APIParameterKey.managerID: AppConfigs.getCurrentUserInfo().id!
            ] as [String : Any]
        print(parameters)
        
        let url = UrlManager.getAddScoutURL()
        Alamofire.request(url, method: .post, parameters: parameters as Parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
            case.success(let data):
                print("success",data)
                //self.removeSpinner()
                let statusCode = response.response?.statusCode
                print(statusCode!)
                let addScoutResponse = try? JSONDecoder().decode(AddScoutResponse.self, from: response.data!)
                if statusCode == 200{
                    DispatchQueue.main.async {
                        self.removeSpinner()
                        AppConfigs.showSnacbar(message: (addScoutResponse?.message)!, textColor: .green)
                        self.allList.removeAll()
                        //self.showSpinner(onView: self.view)
                        self.getAllScouts(urlString: UrlManager.getScoutURL())
                    }
                }
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                   // AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: (addScoutResponse?.message)!, textColor: .green)
                }
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                
                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
                self.removeSpinner()
            }
            self.removeSpinner()
        }
    }
    
    func handleDeleteScout(id:Int)  {
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        
        let parameters = [ K.APIParameterKey.scoutID: id
        ]
        print(parameters)
        
        let url = UrlManager.getAddScoutURL() + "\(id)/"
        Alamofire.request(url, method: .delete, parameters: parameters as Parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
            case.success(let data):
                print("success",data)
                //self.removeSpinner()
                let statusCode = response.response?.statusCode
                print(statusCode!)
                
                let deleteScoutResponse = try? JSONDecoder().decode(DeleteTeamMemberResponse.self, from: response.data!)
                if statusCode == 200{
                    DispatchQueue.main.async {
                        self.removeSpinner()
                        AppConfigs.showSnacbar(message: (deleteScoutResponse?.message)!, textColor: .green)
                        self.allList.removeAll()
                        //self.showSpinner(onView: self.view)
                        self.getAllScouts(urlString: UrlManager.getScoutURL())
                    }
                }
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                   // AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                   // AppConfigs.showSnacbar(message: (deleteScoutResponse?.message)!, textColor: .green)
                }
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                
                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
                self.removeSpinner()
            }
            self.removeSpinner()
        }
    }
    
}

extension MyScoutsViewController: UITableViewDataSource {
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if allList.count == 0 {
            tableView.setEmptyMessage("Nothing yet to show")
        } else {
            tableView.restore()
        }
        return allList.count
        // return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ScoutsTableViewCell", for: indexPath) as! ScoutsTableViewCell
        
        
        cell.lblSeperator.backgroundColor = self.listSeperatorColor
        cell.lblScoutName.textColor = UIColor(red:0, green:0, blue:0, alpha:0.8)
        cell.lblUrl.textColor = UIColor(red:0.64, green:0.64, blue:0.64, alpha:1)
        cell.scoutImageView.layer.cornerRadius = 19
        cell.scoutImageView.layer.borderWidth = 0
        cell.scoutImageView.image = UIImage(named: "new_ic_names")
        
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [cell.lblScoutName], type: TextType.ListTitle.rawValue)
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [cell.lblUrl], type: TextType.ListSubTitle.rawValue)
        
        
        if allList.count > indexPath.row{
            let scout = allList[indexPath.row]
            cell.lblScoutName.text = (scout.firstName ?? "") + " " + (scout.lastName ?? "")
            cell.lblUrl.text = scout.url ?? ""
            
            
        }
        //cell.lblScoutName.text = items[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row ==  allList.count - 1{
            if jsonResponse?.count != allList.count{
                if let nextValue = jsonResponse?.next {
                    print(nextValue)
                    getAllScouts(urlString: nextValue)
                }
            }
        }
    }
    
}

extension MyScoutsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    //        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //            return UITableView.automaticDimension
    //        }
    //
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if allList.count > indexPath.row{
            let scout = allList[indexPath.row]
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "MyTeam", bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "TeamMemberDetailsViewController") as! TeamMemberDetailsViewController
            
            vc.scout = scout
            vc.parentString = "scout"
            //vc.isPhoneNoEdit = true
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        var config = UISwipeActionsConfiguration(actions: [])
        
        if allList.count > indexPath.row{
            
            let data = allList[indexPath.row]
            let url: String = data.url ?? ""
            let id = data.id
            UIPasteboard.general.string = url
            
            let delete = UIContextualAction(style: .destructive, title: "Delete") { (action, view, nil) in
                print(id!)
                
                let alert = UIAlertController(title: "Are you sure you want to remove ", message: "", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
                alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action) in
                    self.handleDeleteScout(id: id!)
                    print("delete")
                }))
                self.present(alert, animated: true, completion: nil)
            }
            
            delete.backgroundColor = UIColor.white // .red
            delete.image =  UIImage(named: "new_ic_delete")
            
            let copy = UIContextualAction(style: .destructive, title: "Copy") { (action, view, nil) in
                print("new_ic_copy")
                
                if let myString = UIPasteboard.general.string {
                    AppConfigs.showSnacbar(message: "Copied to Clipboard", textColor: .green)
                    print(myString)
                }
            }
            copy.backgroundColor = UIColor.white
            
            copy.image = UIImage(named: "new_ic_copy")//?.colored(in: UIColor(red:0, green:0.57, blue:1, alpha:1))
            
            config = UISwipeActionsConfiguration(actions: [delete,copy])
            config.performsFirstActionWithFullSwipe = false
        }else{
            //config = UISwipeActionsConfiguration(actions: [delete,copy])
            
        }
        return  config
        
    }
    
}
