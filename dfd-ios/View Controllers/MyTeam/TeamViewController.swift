//
//  TeamViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 8/26/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit
import XMSegmentedControl

class TeamViewController: UIViewController, XMSegmentedControlDelegate {
    
    func xmSegmentedControl(_ xmSegmentedControl: XMSegmentedControl, selectedSegment: Int) {
        
        if selectedSegment == 0 {
            self.loadViewController(id: vcArray[0])
            
        }
        else {
            self.loadViewController(id: vcArray[1])
        }
    }
    
    
    @IBOutlet weak var mySegmentedControl: XMSegmentedControl!
    @IBOutlet weak var containerView: UIView!
    
    let vcArray = ["MyTeamsViewController", "MyScoutsViewController"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mySegmentedControl.delegate = self
        let titles = ["My Team", "My Scout"]
        let icons = [UIImage(named: "new_ic_teams")!, UIImage(named: "new_ic_scouts")!]
        
        let backgroundColor = UIColor.white // UIColor(red: 205/255, green: 74/255, blue: 1/255, alpha: 1)
        let highlightColor = UIColor(red:0.14, green:0.78, blue:0.47, alpha:0.6)
        mySegmentedControl.segmentContent = (titles, icons)
        mySegmentedControl.selectedItemHighlightStyle = XMSelectedItemHighlightStyle.bottomEdge
        mySegmentedControl.backgroundColor = backgroundColor
        mySegmentedControl.highlightColor = highlightColor
        mySegmentedControl.tint = UIColor.gray // UIColor(red:0.09, green:0.08, blue:0.1, alpha:0.8)
        mySegmentedControl.highlightTint = highlightColor
        mySegmentedControl.edgeHighlightHeight = 2
        
        self.loadViewController(id: vcArray[0])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupNavForDefaultMenu()
        
        // Add left bar button item
        let leftBarItem = UIBarButtonItem(image: UIImage(named: "leftarrow"), style: .plain, target: self, action: #selector(toggleSideMenu))
        leftBarItem.tintColor = .black
        navigationItem.leftBarButtonItem = leftBarItem
        self.title = "TEAMS"
        self.setUpNavigationBar()
        
    }
    
    private func setupNavForDefaultMenu() {
        // Revert navigation bar translucent style to default
        //navigationBarNonTranslecentStyle()
        // Update side menu after reverted navigation bar style
        sideMenuManager?.instance()?.menu?.isNavbarHiddenOrTransparent = true
        navigationItem.hidesBackButton = true
        
    }
    
    @objc func toggleSideMenu() {
        sideMenuManager?.toggleSideMenuView()
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        guard let signInVC = storyBoard.instantiateViewController(withIdentifier: "SMNavigationController") as? SMNavigationController  else
        { return  }
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        signInVC.modalPresentationStyle = .fullScreen
        present(signInVC, animated: true, completion: nil)
        
    }
    
    
    @IBAction func switchView(_ sender: UISegmentedControl) {
        
        if sender.selectedSegmentIndex == 0 {
            self.loadViewController(id: vcArray[0])
            
        }
        else {
            self.loadViewController(id: vcArray[1])
        }
        
    }
    
    
    func loadViewController(id: String) {
        
        if id == "StoryViewController" {
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "MyTeam", bundle:nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: id)
            self.addChild(vc)
            vc.view.frame = CGRect(x: 0, y: 0, width: self.containerView.frame.size.width, height:
                
                self.containerView.frame.size.height)
            self.containerView.addSubview(vc.view)
        }else{
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "MyTeam", bundle:nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: id)
            self.addChild(vc)
            vc.view.frame = CGRect(x: 0, y: 0, width: self.containerView.frame.size.width, height:
                
                self.containerView.frame.size.height)
            self.containerView.addSubview(vc.view)
        }
        
    }
}

extension UIApplication {
    var statusBarUIView: UIView? {
        if #available(iOS 13.0, *) {
            let tag = 38482458385
            if let statusBar = keyWindow?.viewWithTag(tag) {
                return statusBar
            } else {
                let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
                statusBarView.tag = tag
                keyWindow?.addSubview(statusBarView)
                return statusBarView
            }
        } else if responds(to: Selector(("statusBar"))) {
            return value(forKey: "statusBar") as? UIView
        } else {
            return nil
        }
    }
}
