//
//  SignInViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 5/2/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit
import Alamofire
import PasswordTextField
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit
import SwiftyJSON
import AuthenticationServices

@available(iOS 13.0, *)
class SignInViewController: UIViewController , GIDSignInDelegate{
    
    // Stop the UIActivityIndicatorView animation that was started when the user
    // pressed the Sign In button
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error?) {
        //myActivityIndicator.stopAnimating()
    }
    
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
                print("The user has not signed in before or they have since signed out.")
            } else {
                print("\(error.localizedDescription)")
            }
            return
        }
        let email = user.profile.email
        print("\(String(describing: email))")
        
        AppConfigs.registerSocialUser(view: self.view, vc: self, type: "google") { (response,responseCode, status, error)  in
            
            if responseCode == 200{
                
                if status == true{
                    
                    AppConfigs.saveAccessToken(token: (response?.accessToken)!)
                    AppConfigs.saveRefreshToken(token: (response?.refreshToken)!)
                    
                    let url : String = UrlManager.getCurrentUserInfoURL()
                    self.getCurrentUserInfo(urlString: url)
                }else{
                    AppConfigs.showSnacbar(message: error ?? "", textColor: .red)
                }
                
            }else{
                if response != nil{
                    
                    AppConfigs.showSnacbar(message: (response?.errorDescription)!, textColor: .red)
                }else{
                    
                    AppConfigs.showSnacbar(message: error!, textColor: .red)
                }
            }
        }
    }
    
    
    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblPassword: UILabel!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var btnSignIn: GIDSignInButton!
    @IBOutlet weak var btnFbSignIn: UIButton!
    @IBOutlet weak var AppleSignInView: UIStackView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance()?.delegate = self
        GIDSignIn.sharedInstance().presentingViewController = self
        
        if GIDSignIn.sharedInstance()?.currentUser != nil{
            print("Google User Found", GIDSignIn.sharedInstance()?.currentUser! as Any)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(statusManager), name: .flagsChanged, object: nil)
        AppConfigs.updateUserInterface(view: view)
        
        //get saved email address and put it on text field
        if AppConfigs.getSavedEmailAddress() != ""{
            txtUserName.text = AppConfigs.getSavedEmailAddress()
        }
        //SocketIOManager.shared.disconnectSocket()
        
        FontAndColorConfigs.setDynamicButtonsFontSize(buttons: [btnLogin, btnFbSignIn], type: TextType.ListTitle.rawValue)
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [lblEmail,lblPassword], type: TextType.ListSubTitle.rawValue)
        
        self.setUpView()
        self.setUpNavigationBar()
        txtUserName.underlined()
        txtPassword.underlined()
        
        
    }
    
    @objc func statusManager(_ notification: Notification) {
        AppConfigs.updateUserInterface(view: view)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    func setUpView()  {
        //signInWithApple.cornerRadius = 8
        //        signInWithApple.backgroundColor = .black
        //        signInWithApple.tintColor = .white
        // signInWithApple = ASAuthorizationAppleIDButton(authorizationButtonType: .continue, authorizationButtonStyle: .black)
        let appleButton = ASAuthorizationAppleIDButton(type: .continue, style: .black)
        appleButton.translatesAutoresizingMaskIntoConstraints = true
        
        appleButton.addTarget(self, action: #selector(handleLogInWithAppleID), for: .touchUpInside)
        
        AppleSignInView.addArrangedSubview(appleButton)
        NSLayoutConstraint.activate([
            // appleButton.centerYAnchor.constraint(equalTo: AppleSignInView.centerYAnchor, constant: -70),
            appleButton.leadingAnchor.constraint(equalTo: AppleSignInView.leadingAnchor, constant: 0),
            appleButton.trailingAnchor.constraint(equalTo: AppleSignInView.trailingAnchor, constant: 0),
            appleButton.heightAnchor.constraint(equalToConstant: 48)
            ,appleButton.widthAnchor.constraint(equalTo: AppleSignInView.widthAnchor, multiplier: 1.0)
        ])
        
        txtUserName.delegate = self
        txtPassword.delegate = self
        
        lblPassword.textColor = self.listSubTitleColor
        lblEmail.textColor = self.listSubTitleColor
        
        
        txtUserName.font = ProNovaR16; txtPassword.font = ProNovaR16
        btnLogin.backgroundColor = self.buttonBackgroundColor
        btnFbSignIn.backgroundColor = self.fbButtonBackgroundColor
        
        
    }
    
    @objc func handleLogInWithAppleID() {
        let request = ASAuthorizationAppleIDProvider().createRequest()
        request.requestedScopes = [.fullName, .email]
        
        let controller = ASAuthorizationController(authorizationRequests: [request])
        
        controller.delegate = self
        controller.presentationContextProvider = self
        
        controller.performRequests()
    }
    @IBAction func googleSignInClicked(_ sender: Any) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func backAction(_ sender: Any) {
        AppConfigs.setRootViewController()
    }
    
    @IBAction func googleImageClicked(_ sender: Any) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func fbLoginClicked(_ sender: Any) {
        
        
        if Network.reachability.status == .unreachable{
            AppConfigs.updateUserInterface(view: view)
        }else{
            
            loginWithFb()
            
        }//        }
    }
    
    @IBAction func fbImageClicked(_ sender: Any) {
        
        if Network.reachability.status == .unreachable{
            AppConfigs.updateUserInterface(view: view)
        }else{
            
            loginWithFb()
            
        }
    }
    
    
    @IBAction func loginButtonClicked(_ sender: Any) {
        
        
        if txtUserName.isEmpty == true || txtPassword.isEmpty == true {
            
            self.showAlert(message: "Username/Email Can't be empty", title: "Error!")
        }else{
            
//            if Network.reachability.status == .unreachable{
//                AppConfigs.updateUserInterface(view: view)
//            }else{
//
//                self.handleLogin(email: txtUserName.text!, password: txtPassword.text!)
//
//            }
            self.handleLogin(email: txtUserName.text!, password: txtPassword.text!)
        }
        //        }
    }
    
    
    @IBAction func forgetPasswordClicked(_ sender: Any) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        guard let vc = storyBoard.instantiateViewController(withIdentifier: "ResetPasswordViewController") as? ResetPasswordViewController  else
        { return  }
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
    }
    
    
    func loginWithFb() {
        
        let loginManager = LoginManager()
        //UIApplication.shared.statusBarStyle = .default  // remove this line if not required
        loginManager.logIn(permissions: ["public_profile", "email"], from: self) { (loginResult, error) in
            
            if AccessToken.current != nil{
                
                print(loginResult!)
                print(AccessToken.current!)
                print(AccessToken.current?.tokenString as Any)
                AppConfigs.registerSocialUser(view: self.view, vc: self, type: "fb") { (response,responseCode,status, error)  in
                    
                    if responseCode == 200{
                        if status == true{
                            AppConfigs.saveAccessToken(token: (response?.accessToken)!)
                            AppConfigs.saveRefreshToken(token: (response?.refreshToken)!)
                            
                            let url : String = UrlManager.getCurrentUserInfoURL()
                            self.getCurrentUserInfo(urlString: url)
                        }else{
                            AppConfigs.showSnacbar(message: error ?? "" , textColor: .red)
                        }
                        
                        
                    }else{
                        if response != nil{
                            
                            AppConfigs.showSnacbar(message: (response?.errorDescription)!, textColor: .red)
                        }else{
                            
                            AppConfigs.showSnacbar(message: error!, textColor: .red)
                        }
                    }
                }
            }else{
                
            }
            
        }
    }
    
    func handleLogin(email: String, password: String) {
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        let parameters = [
            K.APIParameterKey.clientID: UrlManager.getClientID(),
            K.APIParameterKey.clientSecret: UrlManager.getClientSecret(),
            K.APIParameterKey.grantType: "password",
            K.APIParameterKey.username: txtUserName.text!,
            K.APIParameterKey.password: txtPassword.text!
        ]
        
        print(parameters)
        //save this email to shared pref
        AppConfigs.saveEmailAddress(email: txtUserName.text!)
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        
        self.view.activityStartAnimating(activityColor: .black, backgroundColor: .white)
        print(UrlManager.loginURL())
        Alamofire.request(UrlManager.loginURL(), method: .post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                print("success",data)
                self.view.activityStopAnimating()
                
                let statusCode = response.response?.statusCode
                
                let loginResponse = try? JSONDecoder().decode(LoginResponse.self, from: response.data!)
                
                if statusCode == 200 {
                    //save access token and refresh token
                    AppConfigs.saveAccessToken(token: (loginResponse?.accessToken)!)
                    AppConfigs.saveRefreshToken(token: (loginResponse?.refreshToken)!)
                    
                    let url : String = UrlManager.getCurrentUserInfoURL()
                    self.getCurrentUserInfo(urlString: url)
                }
                else if statusCode == 400
                {
                    self.showAlert(message:(loginResponse?.errorDescription ?? ""), title: "Error!")
                }else if statusCode == 401
                {
                   // self.showAlert(message: (loginResponse?.errorDescription ?? ""), title: "Error!")
                }else{
                    self.showAlert(message: "Undefined", title: "Error!")
                }
                
            case.failure(let error):
                self.view.activityStopAnimating()
                print("Not Success",error)
                self.removeSpinner()
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                //AppConfigs.showSnacbar(message: error.localizedDescription, textColor: .red)
                
            }
            
            self.view.activityStopAnimating()
        }
        
    }
    
    func getCurrentUserInfo(urlString:String) {
        
        print(urlString)
        
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        print("deviceID----", deviceID)
        var firebaseToken: String  = ""
        
        let version:String = Bundle.main.infoDictionary!["CFBundleShortVersionString"]! as! String
        let build:String = Bundle.main.infoDictionary!["CFBundleVersion"]! as! String
        print(version)
        print(build)
        
//        InstanceID.instanceID().instanceID { (result, error) in
//            if let error = error {
//                print("Error fetching remote instange ID: \(error)")
//            } else if let result = result {
//                print("Remote instance ID token: \(result.token)")
//                print("Remote instance ID: \(result.instanceID)")
//                firebaseToken = result.instanceID
//                //self.registerDevice(fid: result.token)
//            }
//        }
//
        let url : String = UrlManager.getCurrentUserInfoURL()
        let pushUrl : String = url + "?device_type=ios&firebase_token=\(AppConfigs.getSavedFcmToken())&device_id=\(deviceID)&version=\(version).0.\(build)"
        print("pushUrl---",pushUrl)
        //self.getCurrentUserInfo(urlString: pushUrl)
        
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(pushUrl, method: .get, parameters: nil, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success",data)
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    
                    
                    DispatchQueue.main.async {
                        let currentUser = try? JSONDecoder().decode(CurrentUserInfoResponse.self, from: response.data!)
                        
                        if currentUser == nil{
                            do {
                                let json = try JSON(data: response.data!)
                                let status = json["status"]
                                print(status)
                                
                                DispatchQueue.main.async {
                                    
                                    if self.isKeyPresentInUserDefaults(key: AppConfigs.isFirstLogin) == true{
                                        //key exist
                                        AppConfigs.saveFirstLoginFlag(flag:false)
                                    }else{
                                        //key doesn't exist
                                        AppConfigs.saveFirstLoginFlag(flag:true)
                                    }
                                    
                                    if status == true{
                                        let jsondata = json["data"]
                                        let address = jsondata["address"].stringValue
                                        let email = jsondata["email"].stringValue
                                        let replaced = email.replacingOccurrences(of: ",", with: "\n")
                                        print(replaced)
                                        let emailArray = email.components(separatedBy: ", ")
                                        print(emailArray)
                                        let first_name = jsondata["first_name"].stringValue
                                        let last_name = jsondata["last_name"].stringValue
                                        
                                    }
                                }
                            }catch{
                                
                            }
                        }else{
                            
                            AppConfigs.saveCurrentUserInfo(user: currentUser!)
                            print(AppConfigs.getCurrentUserInfo().email!)
                            
                            if self.isKeyPresentInUserDefaults(key: AppConfigs.isFirstLogin) == true{
                                //key exist
                                AppConfigs.saveFirstLoginFlag(flag:false)
                            }else{
                                //key doesn't exist
                                AppConfigs.saveFirstLoginFlag(flag:true)
                            }
                            //                        SocketIOManager.shared.establishConnection()
                            let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
                            guard let vc = storyBoard.instantiateViewController(withIdentifier: "SMNavigationController") as? SMNavigationController  else
                            { return  }
                            vc.modalPresentationStyle = .fullScreen
                            self.present(vc, animated: true, completion: nil)
                            // UIApplication.shared.keyWindow?.rootViewController = vc
                            
                        }
                    }
                }
                    
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                    
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
                self.removeSpinner()
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                //AppConfigs.showSnacbar(message: error.localizedDescription, textColor: .red)
                
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
    
    
}

@available(iOS 13.0, *)
extension SignInViewController: UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor.white.cgColor // UIColor(red:0.07, green:0.82, blue:0.41, alpha:1).cgColor
        textField.layer.shadowOffset = CGSize.zero
        textField.layer.shadowColor = UIColor(red:0.87, green:0.99, blue:0.89, alpha:1).cgColor
        textField.layer.shadowOpacity = 1
        textField.layer.shadowRadius = 7
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor.white.cgColor //UIColor(red:0.77, green:0.99, blue:0.75, alpha:1).cgColor
        
        textField.layer.shadowColor = UIColor.white.cgColor //UIColor(red:0.87, green:0.99, blue:0.89, alpha:1).cgColor
    }
    
}

@available(iOS 13.0, *)
extension SignInViewController: ASAuthorizationControllerDelegate {
    
    func callNetworkAPIForAppleLogin(_ email:String,_ code: String, _ userId:String, _ identityToken:String) {
        // Call your backend to exchange an API token with the code.
        let urlString : String = "https://api.houzes.com/auth/apple/jwt/?user_id=\(userId)"
        print(urlString)
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.httpBody, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success",data)
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    
                    
                    DispatchQueue.main.async {
                        do {
                            let json = try JSON(data: response.data!)
                            let status = json["status"].boolValue
                            let data = json["data"].stringValue
                            let message = json["message"].stringValue
                            
                            print(status, data, message)
                            var parameters: Parameters = [:]
                            
                            parameters = [
                                "code": code,
                                "user_id": userId,
                                "email": email,
                                "jwt_token" : data,
                                "type": "UP"
                            ]
                            print(parameters)
                            
                            ///auth/apple/jwt/?user_id=123
                            AppConfigs.registerAppleUser(view: self.view, vc: self, parameters: parameters) { (data, code, error) in
                                
                                if code == 200{
                                    do{
                                        let json = try JSON(data: data!)
                                        let status = json["status"].boolValue
                                        print(status)
                                        let message = json["message"].stringValue
                                        print(message)
                                        
                                        if status == true{
                                            let dataValue = json["data"]
                                            let access_token = dataValue["access_token"].stringValue
                                            let token_type = dataValue["token_type"].stringValue
                                            let refresh_token = dataValue["refresh_token"].stringValue
                                            print("access_token --> \(access_token) ")
                                            //self.showAlert(message: message)
                                            AppConfigs.saveAccessToken(token: access_token)
                                            AppConfigs.saveRefreshToken(token: refresh_token)
                                            
                                            let url : String = UrlManager.getCurrentUserInfoURL()
                                            self.getCurrentUserInfo(urlString: url)
                                            
                                        }else{
                                            self.showAlert(message: message)
                                        }
                                        
                                    }catch let error{
                                        self.showAlert(message: error.localizedDescription)
                                    }
                                }else if code == 9803{
                                    self.showAlert(message: "Undefined", title: "Error!")
                                }else if code == 500{
                                    //AppConfigs.showSnacbar(message: ErrorMessage.c500.rawValue, textColor: .red)
                                    self.showAlert(message: ErrorMessage.c500.rawValue)
                                }
                                else if code == 502{
                                    self.showAlert(message: ErrorMessage.c502.rawValue)
                                    
                                    //AppConfigs.showSnacbar(message: ErrorMessage.c502.rawValue, textColor: .red)
                                    
                                }else{
                                    self.showAlert(message: ErrorMessage.c502.rawValue)
                                    
                                    //AppConfigs.showSnacbar(message: ErrorMessage.c502.rawValue, textColor: .red)
                                }
                            }
                        
                        }catch let error{
                            print(error.localizedDescription)
                        }
                    }
                }
                    
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                    
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
                self.removeSpinner()
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                //AppConfigs.showSnacbar(message: error.localizedDescription, textColor: .red)
                
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
        
        
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        switch authorization.credential {
            
        case let appleIDCredential as ASAuthorizationAppleIDCredential:
            
            let userIdentifier = appleIDCredential.user
            //appleIDCredential.
            
            let defaults = UserDefaults.standard
            defaults.set(userIdentifier, forKey: "userIdentifier1")
            print("userIdentifier---.", userIdentifier)
            print("userIdentifier---.", appleIDCredential.email ?? "")
            print("userIdentifier---.", appleIDCredential.fullName ?? "")
            
            //self.showAlert(message: "userIdentifier---.\(userIdentifier), \(appleIDCredential.email), \(appleIDCredential.fullName) ")
            
            if appleIDCredential.authorizationCode != nil{
                
                let authCode = String(data: appleIDCredential.authorizationCode!, encoding: .utf8)
                print("authCode:",authCode!)
                print(appleIDCredential.identityToken!)
                
                let identityToken = String(data: appleIDCredential.identityToken!, encoding: .utf8)
                print("identityToken",identityToken!)
                
                self.callNetworkAPIForAppleLogin(appleIDCredential.email ?? "", authCode!, userIdentifier, identityToken!)
                
            }
            
            
            //Save the UserIdentifier somewhere in your server/database
            //            let vc = UserViewController()
            //            vc.userID = userIdentifier-++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-+
            //            self.present(UINavigationController(rootViewController: vc), animated: true)
            break
        default:
            break
        }
        
        
    }
}

@available(iOS 13.0, *)
extension SignInViewController: ASAuthorizationControllerPresentationContextProviding {
    
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
}

