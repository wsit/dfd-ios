//
//  SignUpViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 5/2/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit
import Alamofire
import SwiftValidator
import PasswordTextField
import PhoneNumberKit

class SignUpViewController: UIViewController, ValidationDelegate{
    
    
    @IBOutlet weak var createAccountButton: UIButton!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    //@IBOutlet weak var txtMobileNo: UITextField!
    @IBOutlet weak var txtMobile: UITextField!
    //@IBOutlet weak var txtMobileNo: FPNTextField!
    @IBOutlet weak var txtPhoneNumber: PhoneNumberTextField!
    @IBOutlet weak var txtCouponCode: PasswordTextField!
    
    // Error Labels
    @IBOutlet weak var firstNameErrorLabel: UILabel!
    @IBOutlet weak var lastNameErrorLabel: UILabel!
    @IBOutlet weak var emailErrorLabel: UILabel!
    @IBOutlet weak var mobileNoErrorLabel: UILabel!
    @IBOutlet weak var passwordErrorLabel: UILabel!
    @IBOutlet weak var passwordConfirmErrorLabel: UILabel!
    
    @IBOutlet weak var lblFirstNameTitle: UILabel!
    @IBOutlet weak var lblLastNameTitle: UILabel!
    @IBOutlet weak var lblEmailTitle: UILabel!
    @IBOutlet weak var lblMobileNo: UILabel!
    @IBOutlet weak var lblPassword: UILabel!
    @IBOutlet weak var lblCoupon: UILabel!
    
    
    let validator = Validator()
    var code:String = ""
    var rawNumber: String = ""
    var isNumberValid: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default
            .addObserver(self, selector: #selector(statusManager), name: .flagsChanged, object: nil)
        AppConfigs.updateUserInterface(view: view)
        
        self.setUpNavigationBar()
        self.setUpSignUpView()
        configureUIValidation()
        
        FontAndColorConfigs.setDynamicButtonsFontSize(buttons: [createAccountButton], type: TextType.ListTitle.rawValue)
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [firstNameErrorLabel, emailErrorLabel, lastNameErrorLabel, lblMobileNo, lblPassword, lblEmailTitle, lblLastNameTitle, lblFirstNameTitle, lblCoupon], type: TextType.ListSubTitle.rawValue)
        
    }
    
    
    @objc func statusManager(_ notification: Notification) {
        //updateUserInterface()
        AppConfigs.updateUserInterface(view: view)
    }
    
    func setUpSignUpView()  {
        
        txtFirstName.delegate = self;  txtEmail.delegate = self; txtLastName.delegate = self; txtPassword.delegate = self; txtPhoneNumber.delegate = self
        
        txtFirstName.underlined(); txtLastName.underlined(); txtEmail.underlined(); txtPassword.underlined(); txtPhoneNumber.underlined(); txtCouponCode.underlined()
        
        lblFirstNameTitle.textColor = self.listSubTitleColor
        lblLastNameTitle.textColor = self.listSubTitleColor
        lblEmailTitle.textColor = self.listSubTitleColor
        lblMobileNo.textColor = self.listSubTitleColor
        lblPassword.textColor = self.listSubTitleColor
        lblCoupon.textColor = self.listSubTitleColor

        
        txtEmail.font = ProNovaR16; txtFirstName.font = ProNovaR16; txtLastName.font = ProNovaR16; txtPassword.font = ProNovaR16; txtPhoneNumber.font = ProNovaR16;  txtCouponCode.font = ProNovaR16;
        txtPhoneNumber.placeholder = "ex: +1-541-754-3010"

        createAccountButton.backgroundColor = self.buttonBackgroundColor
    }
    
    func configureUIValidation() {
        
        // Validation Rules are evaluated from left to right.
        validator.registerField(txtFirstName,errorLabel: firstNameErrorLabel , rules: [RequiredRule()])
        validator.registerField(txtLastName, errorLabel: lastNameErrorLabel ,rules: [RequiredRule()])
        //validator.registerField(txtCouponCode, errorLabel: lblCouponErrorLabel ,rules: [RequiredRule()])
        // You can pass in error labels with your rules
        // You can pass in custom error messages to regex rules (such as ZipCodeRule and EmailRule)
        validator.registerField(txtEmail, errorLabel: emailErrorLabel ,rules: [RequiredRule(), EmailRule()])
        
        // You can validate against other fields using ConfirmRule
        //validator.registerField(txtPhoneNumber,errorLabel: mobileNoErrorLabel , rules: [RequiredRule()])
        
        // You can now pass in regex and length parameters through overloaded contructors
        validator.registerField(txtPassword, errorLabel: passwordErrorLabel ,rules: [RequiredRule(), MinLengthRule(length: 6)])
        
        validator.styleTransformers(success:{ (validationRule) -> Void in
            print("here")
            // clear error label
            validationRule.errorLabel?.isHidden = true
            validationRule.errorLabel?.text = ""
            
            if let textField = validationRule.field as? UITextField {
                //                textField.layer.borderColor = UIColor.green.cgColor
                //                textField.layer.borderWidth = 0.5
            } else if let textField = validationRule.field as? UITextView {
                //                textField.layer.borderColor = UIColor.green.cgColor
                //                textField.layer.borderWidth = 0.5
            }
        }, error:{ (validationError) -> Void in
            print("error")
            validationError.errorLabel?.isHidden = false
            validationError.errorLabel?.text = validationError.errorMessage
            if let textField = validationError.field as? UITextField {
                textField.layer.borderColor = UIColor.red.cgColor
                textField.layer.borderWidth = 1.0
            } else if let textField = validationError.field as? UITextView {
                textField.layer.borderColor = UIColor.red.cgColor
                textField.layer.borderWidth = 1.0
            }
        })
    }
    
    func clearTextfields() {
        txtFirstName.text = ""; txtFirstName.placeholder = "First Name"
        
        txtLastName.text = ""; txtLastName.placeholder = "Last Name"
        
        txtEmail.text = ""; txtEmail.placeholder = "Email"
        
        txtPassword.text = ""; txtPassword.placeholder = "Password"
        
        txtPhoneNumber.text = "" ;
        txtPhoneNumber.placeholder = "ex: +1-541-754-3010"
        
    }
    
    
    
    @IBAction func backAction(_ sender: Any) {
        
        //self.dismiss(animated: true, completion: nil)
        AppConfigs.setRootViewController()
    }
    
    @IBAction func signInClicked(_ sender: Any) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        guard let vc = storyBoard.instantiateViewController(withIdentifier: "SignInNavigationController") as? SignInNavigationController  else
        { return  }
        
        vc.modalPresentationStyle = .fullScreen 
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func createAccountButtonClicked(_ sender: Any) {
        
        print("Validating...")
        validator.validate(self)
        
    }
    
    
    func validationSuccessful() {
        
        if txtFirstName.isEmpty == true || txtLastName.isEmpty == true || txtEmail.isEmpty == true || txtPassword.isEmpty == true   {
            
        }else{
            print("txtMobileNo---->",txtPhoneNumber.nationalNumber)
            print(self.rawNumber)
            
            if txtPhoneNumber.isEmpty == true{
                mobileNoErrorLabel.isHidden = true
                
                if Network.reachability.status == .unreachable{
                    AppConfigs.updateUserInterface(view: view)
                }else{
                    
                    handleSignUp(email: txtEmail.text!, password: txtPassword.text!, phone: "", firstName: txtFirstName.text!, lastName: txtLastName.text!)
                }
            }else{
                
                if txtPhoneNumber.isValidNumber == true{
                    mobileNoErrorLabel.isHidden = true
                    
                    if Network.reachability.status == .unreachable{
                        AppConfigs.updateUserInterface(view: view)
                    }else{
                        
                        handleSignUp(email: txtEmail.text!, password: txtPassword.text!, phone: txtPhoneNumber.nationalNumber, firstName: txtFirstName.text!, lastName: txtLastName.text!)
                    }
                }else{
                    mobileNoErrorLabel.isHidden = false
                    
                    mobileNoErrorLabel.text = "Invalid Mobile Number"
                    
                    txtPhoneNumber.layer.borderWidth = 1.0
                    txtPhoneNumber.borderColor = .red
                }
            }
        }
        //        }
    }
    
    func validationFailed(_ errors:[(Validatable ,ValidationError)]) {
        // turn the fields to red
        for (field, error) in errors {
            if let field = field as? UITextField {
                field.layer.borderColor = UIColor.red.cgColor
                field.layer.borderWidth = 1.0
            }
            error.errorLabel?.text = error.errorMessage // works if you added labels
            error.errorLabel?.isHidden = false
        }
        
        if txtPhoneNumber.isEmpty == false{
            if txtPhoneNumber.isValidNumber == true{
                mobileNoErrorLabel.isHidden = true
                
            }else{
                mobileNoErrorLabel.isHidden = false
                mobileNoErrorLabel.text = "Invalid Mobile Number"
                txtPhoneNumber.layer.borderWidth = 1.0
                txtPhoneNumber.borderColor = .red
            }
        }else{
            mobileNoErrorLabel.text = "This field is required"
        }
    }
    
    func validationFailed(errors: [UITextField : ValidationError]) {
        print("Validation FAILED!")
    }
    
    func handleSignUp(email:String, password: String, phone: String, firstName: String, lastName :String){
        
        self.view.activityStartAnimating(activityColor: .black, backgroundColor: .white)
        
        var myString: String = phone
        //        let removeCountryCode = myString.remove(at: myString.startIndex)
        //        print(removeCountryCode)
        //        print(myString)
        
        print(myString.prefix(1))
        
        if myString.prefix(1) == "1"{
            
            let removeCountryCode = myString.remove(at: myString.startIndex)
            print(removeCountryCode)
        }
        
        var parameters : Parameters;
        
        print(myString)
        if txtCouponCode.text == ""{
            parameters = [
            K.APIParameterKey.email: email
            , K.APIParameterKey.password: password
            , K.APIParameterKey.phone: myString
            , K.APIParameterKey.firstName: firstName
            , K.APIParameterKey.lastName: lastName
            , "is_admin" : true
            ] as [String : Any]
        }else{
            parameters = [
            K.APIParameterKey.email: email
            , K.APIParameterKey.password: password
            , K.APIParameterKey.phone: myString
            , K.APIParameterKey.firstName: firstName
            , K.APIParameterKey.lastName: lastName
            , "is_admin" : true
                , "coupon": txtCouponCode.text!
            ] as [String : Any]
        }
        
//        parameters = [
//            K.APIParameterKey.email: email
//            , K.APIParameterKey.password: password
//            , K.APIParameterKey.phone: myString
//            , K.APIParameterKey.firstName: firstName
//            , K.APIParameterKey.lastName: lastName
//            , "is_admin" : true
//            ] as [String : Any]
        print(parameters)
        
        //save this email to shared pref
        
        // let url : String = "http://58.84.34.65:8585/api/user-register/"
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        //192.168.1.22:8000/api/user/
        //UrlManager.signUpURL()
        Alamofire.request(UrlManager.signUpURL(), method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)
            .responseJSON { response in
                
                switch(response.result) {
                case.success(let data):
                    print("success",data)
                    self.view.activityStopAnimating()
                    let statusCode = response.response?.statusCode
                    
                    if statusCode == 200{
                        let registeredUserResponse = try? JSONDecoder().decode(SignUpResponse.self, from: response.data!)
                        
                        if registeredUserResponse?.status == true{
                            if registeredUserResponse?.data != nil{
                                
                                let alertController = UIAlertController(title: registeredUserResponse?.message, message:" ", preferredStyle: .alert)
                                let OKAction = UIAlertAction(title: "Go To Login", style: .default, handler: {
                                    alertAction in
                                    
                                    AppConfigs.saveEmailAddress(email: email)
                                    
                                    let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
                                    guard let vc = storyBoard.instantiateViewController(withIdentifier: "SignInNavigationController") as? SignInNavigationController  else
                                    { return  }
                                    vc.modalPresentationStyle = .fullScreen
                                    self.present(vc, animated: true, completion: nil)
                                })
                                
                                let cancelAction = UIAlertAction(title: "Not Now", style: .default, handler: nil)
                                alertController.addAction(OKAction)
                                alertController.addAction(cancelAction)
                                self.present(alertController, animated: true, completion: nil)
                            }
                        }else{
                            AppConfigs.showSnacbar(message: (registeredUserResponse?.message)!, textColor: .red)
                        }
                        
                    }
                    else if statusCode == 400{
                        let errorResponse = try? JSONDecoder().decode(SignUpErrorResponse.self, from: response.data!)
                        self.showAlert(message: (errorResponse?.email?[0])!, title: "Sorry!")
                    }
                    else{
                        self.showAlert(message: "Some Error Occured!", title: "Sorry!")
                    }
                    
                case.failure(let error):
                    self.view.activityStopAnimating()
                    print("Not Success",error)
                    self.removeSpinner()
                    let statusCode = response.response?.statusCode
                    self.showMessageForServerError(code: statusCode ?? 500)
                    //AppConfigs.showSnacbar(message: error.localizedDescription, textColor: .red)
                    
                }
        }
    }
    
}
extension SignUpViewController: UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor.white.cgColor //UIColor(red:0.07, green:0.82, blue:0.41, alpha:1).cgColor
        textField.layer.shadowOffset = CGSize.zero
        textField.layer.shadowColor = UIColor.white.cgColor// UIColor(red:0.87, green:0.99, blue:0.89, alpha:1).cgColor
        textField.layer.shadowOpacity = 1
        textField.layer.shadowRadius = 7
        mobileNoErrorLabel.isHidden = true
        textField.underlined()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor.white.cgColor//UIColor(red:0.77, green:0.99, blue:0.75, alpha:1).cgColor
        
        textField.layer.shadowColor = UIColor.white.cgColor
        textField.underlined()//UIColor(red:0.87, green:0.99, blue:0.89, alpha:1).cgColor
    }
    
}
