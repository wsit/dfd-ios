//
//  UIImage.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 8/28/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import Foundation
import UIKit


extension UIImage {
    func colored(in color: UIColor) -> UIImage {
        let renderer = UIGraphicsImageRenderer(size: size)
        return renderer.image { context in
            color.set()
            self.withRenderingMode(.alwaysTemplate).draw(in: CGRect(origin: .zero, size: size))
        }
    }
}
