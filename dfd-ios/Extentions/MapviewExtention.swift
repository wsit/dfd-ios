//
//  MapviewExtention.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 5/13/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import Foundation
import GoogleMaps

extension GMSMapView {
    func mapStyle(withFilename name: String, andType type: String) {
        do {
            if let styleURL = Bundle.main.url(forResource: name, withExtension: type) {
                self.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
    }
}
/*
 // Set the map style by passing the URL of the local file.
 //        do {
 //            if let styleURL = Bundle.main.url(forResource: "style", withExtension: "json") {
 //                mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
 //
 //            } else {
 //                NSLog("Unable to find style.json")
 //            }
 //        } catch {
 //            NSLog("One or more of the map styles failed to load. \(error)")
 //        }
 */
