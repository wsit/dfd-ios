//
//  UITextField.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 5/22/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    var isEmpty: Bool {
        return text?.isEmpty ?? true
    }
     
    
    
    func underlined(){
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor(red:0.84, green:0.84, blue:0.84, alpha:1).cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}
class TextField: UITextField {

    let padding = UIEdgeInsets(top: 0, left: 4, bottom: 0, right: 4)

    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}

