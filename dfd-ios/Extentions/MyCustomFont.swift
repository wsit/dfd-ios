//
//  MyCustomFont.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 11/19/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//
 

import Foundation
import UIKit

// Usage Examples
let system12 = Font(.system, size: .standard(.h5)).instance
let robotoThin20 = Font(.installed(.RobotoThin), size: .standard(.h1)).instance
let robotoBlack17 = Font(.installed(.RobotoBlack), size:.custom(17.0))
let robotoBlack14       = Font(.installed(.RobotoBlack), size: .standard(.h4)).instance

let helveticaLight13 = Font(.custom("Helvetica-Light"), size: .custom(13.0)).instance

let ProNovaR16 = MyFont(.installed(.ProximaNovaRegular), size: .standard(.s16)).instance
let ProNovaR18 = MyFont(.installed(.ProximaNovaRegular), size: .standard(.s18)).instance
let ProNovaR20 = MyFont(.installed(.ProximaNovaRegular), size: .standard(.s20)).instance
let ProNovaR22 = MyFont(.installed(.ProximaNovaRegular), size: .standard(.s22)).instance
let ProNovaR11 = MyFont(.installed(.ProximaNovaRegular), size: .standard(.s11)).instance
let ProNovaR1225 = MyFont(.installed(.ProximaNovaRegular), size: .standard(.s1225)).instance
let ProNovaR1350 = MyFont(.installed(.ProximaNovaRegular), size: .standard(.s1350)).instance
let ProNovaR9 = MyFont(.installed(.ProximaNovaRegular), size: .standard(.s9)).instance
let ProNovaR14 = MyFont(.installed(.ProximaNovaRegular), size: .standard(.s14)).instance
let ProNovaSB11 = MyFont(.installed(.ProximaNovaSemibold), size: .standard(.s11)).instance
let ProNovaSB18 = MyFont(.installed(.ProximaNovaSemibold), size: .standard(.s18)).instance
let ProNovaSB20 = MyFont(.installed(.ProximaNovaSemibold), size: .standard(.s20)).instance
let ProNovaSB22 = MyFont(.installed(.ProximaNovaSemibold), size: .standard(.s22)).instance


 
struct MyFont {
    
    enum FontType {
        case installed(FontName)
        case custom(String)
        case system
        case systemBold
        case systemItatic
        case systemWeighted(weight: Double)
        case monoSpacedDigit(size: Double, weight: Double)
    }
    
    enum FontSize {
        case standard(StandardSize)
        case custom(Double)
        var value: Double {
            switch self {
            case .standard(let size):
                return size.rawValue
            case .custom(let customSize):
                return customSize
            }
        }
    }
    
    enum FontName: String {
        
        case ProximaNovaRegular = "ProximaNova-Regular"
        case ProximaNovaSemibold = "ProximaNova-Semibold"
        
    }
    
    enum StandardSize: Double {
        case s22 = 22.0
        case s20 = 20.0
        case s18 = 18.0
        case s16 = 16.0
        case s14 = 14.0
        case s1350 = 13.50
        case s1225 = 12.25
        case s11 = 11.0
        case s9 = 9.0
    }
    
    
    var type: FontType
    var size: FontSize
    init(_ type: FontType, size: FontSize) {
        self.type = type
        self.size = size
    }
}

extension MyFont {
    
    var instance: UIFont {
        
        var instanceFont: UIFont!
        switch type {
        case .custom(let fontName):
            guard let font =  UIFont(name: fontName, size: CGFloat(size.value)) else {
                fatalError("\(fontName) font is not installed, make sure it added in Info.plist and logged with Utility.logAllAvailableFonts()")
            }
            instanceFont = font
        case .installed(let fontName):
            guard let font =  UIFont(name: fontName.rawValue, size: CGFloat(size.value)) else {
                fatalError("\(fontName.rawValue) font is not installed, make sure it added in Info.plist and logged with Utility.logAllAvailableFonts()")
            }
            instanceFont = font
                
        case .system:
            instanceFont = UIFont.systemFont(ofSize: CGFloat(size.value))
        case .systemBold:
            instanceFont = UIFont.boldSystemFont(ofSize: CGFloat(size.value))
        case .systemItatic:
            instanceFont = UIFont.italicSystemFont(ofSize: CGFloat(size.value))
        case .systemWeighted(let weight):
            instanceFont = UIFont.systemFont(ofSize: CGFloat(size.value),
                                             weight: UIFont.Weight(rawValue: CGFloat(weight)))
        case .monoSpacedDigit(let size, let weight):
            instanceFont = UIFont.monospacedDigitSystemFont(ofSize: CGFloat(size),
                                                            weight: UIFont.Weight(rawValue: CGFloat(weight)))
        }
        return instanceFont
    }
}

//class Utility {
//    /// Logs all available fonts from iOS SDK and installed custom font
//    class func logAllAvailableFonts() {
//        for family in UIFont.familyNames {
//            print("\(family)")
//            for name in UIFont.fontNames(forFamilyName: family) {
//                print("   \(name)")
//            }
//        }
//    }
//}
