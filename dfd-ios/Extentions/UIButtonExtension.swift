//
//  UIButtonExtension.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 5/29/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    func centerAndImage(withSpacing spacing: CGFloat) {
        imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: spacing)
        titleEdgeInsets = UIEdgeInsets(top: 0, left: spacing, bottom: 0, right: 0)
    }
    func underlined(){
            let border = CALayer()
            let width = CGFloat(1.0)
        border.borderColor = UIColor(red:0.64, green:0.64, blue:0.64, alpha:1).cgColor
            border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
            border.borderWidth = width
            self.layer.addSublayer(border)
            self.layer.masksToBounds = true
    }
}
