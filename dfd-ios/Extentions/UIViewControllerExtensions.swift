//
//  UIViewControllerExtensions.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 5/3/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import Foundation
import UIKit

var vSpinner : UIView?


enum ErrorMessage: String {
    
    case InsufficientBalance = "Please contact admin to add a balance"
    case c500 =  "There was an error understanding the request."
    case c502 =  "The server is encountering problems."
    case Requested = "Data fetch in progress. Please check back in a few minutes"
}

extension UIViewController {
    
    func resizeImageWithScaling(image: UIImage, scaleToSize newSize: CGSize, isAspectRation aspect: Bool) -> UIImage{

        let originRatio = image.size.width / image.size.height;//CGFloat
        let newRatio = newSize.width / newSize.height;

        var sz: CGSize = CGSize.zero

        if (!aspect) {
            sz = newSize
        }else {
            if (originRatio < newRatio) {
                sz.height = newSize.height
                sz.width = newSize.height * originRatio
            }else {
                sz.width = newSize.width
                sz.height = newSize.width / originRatio
            }
        }
        let scale: CGFloat = 1.0

        sz.width /= scale
        sz.height /= scale
        UIGraphicsBeginImageContextWithOptions(sz, false, scale)
        image.draw(in: CGRect(x: 0, y: 0, width: sz.width, height: sz.height))
        
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        
        let newHeight = CGFloat(180.0)//image.size.height * scale
        
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return newImage!
        
    }
    
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    func showMessageForServerError(code: Int)  {
        
        if code == 500{
            AppConfigs.showSnacbar(message: ErrorMessage.c500.rawValue, textColor: .red)
        }
        else if code == 502{
            AppConfigs.showSnacbar(message: ErrorMessage.c502.rawValue, textColor: .red)
            
        }else{
            AppConfigs.showSnacbar(message: ErrorMessage.c502.rawValue, textColor: .red)
        }
    }
    
    func showToastLabel(label: UILabel, message: String){
        label.isHidden = false
        label.alpha = 1.0
        label.text = message
        label.backgroundColor = self.buttonBackgroundColor
        label.font = ProNovaR16
       // UIView.animate(withDuration: TimeInterval, animations: <#T##() -> Void#>)
        UIView.animate(withDuration: 15.0, delay: 0.6, options: .curveEaseOut, animations: {
            label.alpha = 0.0
        }, completion: {(isCompleted) in
            //label.removeFromSuperview()
            label.isHidden = true
        })
    }
    
    func showToast(message : String ) {
        let font = ProNovaR16
        let toastLabel = UILabel(frame: CGRect(x: 0 , y: 60, width: self.view.frame.size.width, height: 20))
        //let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 150, height: 35))
        
        toastLabel.backgroundColor = .green// UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.font = font
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    
    func matchesRegex(regex: String!, text: String!) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: regex, options: [.caseInsensitive])
            let nsString = text as NSString
            let match = regex.firstMatch(in: text, options: [], range: NSMakeRange(0, nsString.length))
            return (match != nil)
        } catch {
            return false
        }
    }
    
    
    func luhnCheck(number: String) -> Bool {
        var sum = 0
        let digitStrings = number.reversed().map { String($0) } //number.reversed().map { String($0) } //number.characters.reverse().map { String($0) }
        // number.reversed().map{ String($0) }
        //        for tuple in digitStrings.enumerate() {
        //            guard let digit = Int(tuple.element) else { return false }
        //            let odd = tuple.index % 2 == 1
        //
        //            switch (odd, digit) {
        //            case (true, 9):
        //                sum += 9
        //            case (true, 0...8):
        //                sum += (digit * 2) % 9
        //            default:
        //                sum += digit
        //            }
        //        }
        //
        //        return sum % 10 == 0
        for (idx, element) in digitStrings.enumerated() {
            guard let digit = Int(element) else { return false }
            switch ((idx % 2 == 1), digit) {
            case (true, 9): sum += 9
            case (true, 0...8): sum += (digit * 2) % 9
            default: sum += digit
            }
        }
        return sum % 10 == 0
    }
    
    func checkCardNumber(input: String) -> (type: CardType, formatted: String, valid: Bool) {
        // Get only numbers from the input string
        //let numberOnly = input.stringByReplacingOccurrencesOfString("[^0-9]", withString: "", options: .RegularExpressionSearch)
        let numberOnly = input.replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression)
        var type: CardType = .Unknown
        var formatted = ""
        var valid = false
        
        // detect card type
        for card in CardType.allCards {
            if (matchesRegex(regex: card.regex, text: numberOnly)) {
                type = card
                break
            }
        }
        
        // check validity
        valid = luhnCheck(number: numberOnly)
        
        // format
        var formatted4 = ""
        for character in numberOnly {
            if formatted4.count == 4 {
                formatted += formatted4 + " "
                formatted4 = ""
            }
            formatted4.append(character)
        }
        
        formatted += formatted4 // the rest
        
        // return the tuple
        return (type, formatted, valid)
    }
    
    
    
    func showPicture(url: String, imageView: UIImageView) {
        // self.showSpinner(onView: imageView)
        
        let pictureURL = URL(string: url)!
        let session = URLSession(configuration: .default)
        let downloadPicTask = session.dataTask(with: pictureURL) { (data, response, error) in
            // The download has finished.
            if let e = error {
                self.removeSpinner()
                print("Error downloading cat picture: \(e)")
            } else {
                if let res = response as? HTTPURLResponse {
                    print("Downloaded cat picture with response code \(res.statusCode)")
                    self.removeSpinner()
                    if let imageData = data {
                        DispatchQueue.main.async { // Correct
                            self.removeSpinner()
                            let image = UIImage(data: imageData)
                            // Do something with your image.
                            imageView.image = image
                        }
                    } else {
                        self.removeSpinner()
                        print("Couldn't get image: Image is nil")
                    }
                } else {
                    self.removeSpinner()
                    print("Couldn't get response code for some reason")
                }
                self.removeSpinner()
            }
        }
        
        downloadPicTask.resume()
    }
    
    func alertWithTF(title: String, message: String, cancelHandler: ((UIAlertAction) -> Swift.Void)? = nil, actionHandler: ((_ text: String?, _ text: String?) -> Void)? = nil) {
        
        //Step : 1
        let attributedTitleString = NSAttributedString(string: title, attributes: [
            NSAttributedString.Key.paragraphStyle: NSParagraphStyle.default,
            NSAttributedString.Key.font : UIFont(name: "ProximaNova-Semibold", size: 22)!,
            NSAttributedString.Key.foregroundColor : UIColor.black
        ])
        
        let attributedMessageString = NSAttributedString(string: message, attributes: [
            NSAttributedString.Key.paragraphStyle: NSParagraphStyle.default,
            NSAttributedString.Key.font : UIFont(name: "ProximaNova-Regular", size: 18)!,
            NSAttributedString.Key.foregroundColor : UIColor.red
        ])
        let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
        alert.setValue(attributedTitleString, forKey: "attributedTitle")
        alert.setValue(attributedMessageString, forKey: "attributedMessage")
        
        //Cancel action
        alert.addAction(UIAlertAction(title: "Cancel", style: .default) { (alertAction) in })
        
        //Step : 2
        let saveAction = UIAlertAction(title: "Confirm", style: .destructive) { (alertAction) in
            let textField = alert.textFields![0]
            let textField2 = alert.textFields![1]
            if textField.text != "" {
                //Read textfield data
                print(textField.text!)
                print("TF 1 : \(textField.text!)")
            } else {
                print("TF 1 is Empty...")
                actionHandler?(nil, nil)
            }
            if textField2.text != "" {
                //Read textfield data
                print(textField2.text!)
                print("TF 2 : \(textField2.text!)")
            } else {
                print("TF 2 is Empty...")
            }
            if textField.isEmpty == true  || textField2.isEmpty{
                //self.showAlert(message: "Enter both First Name and Last Name")
                self.alertWithTF(title: "Add Scout", message: "Enter both First Name and Last Name")
            }else{
                actionHandler?(textField.text,textField2.text)
            }
        }
        
        alert.addAction(saveAction)
        //Step : 3
        //For first TF
        alert.addTextField { (textField) in
            textField.placeholder = "first name"
            textField.textAlignment = .left
            textField.borderStyle = .none
            textField.addConstraint(textField.heightAnchor.constraint(equalToConstant: 40))
            FontAndColorConfigs.setTextFieldsTextColorAndFont(txtFields: [textField], color: .black, font: FontAndColorConfigs.getProximaRegular18())
            saveAction.isEnabled = false
            NotificationCenter.default.addObserver(forName: UITextField.textDidChangeNotification, object: textField, queue: OperationQueue.main) { (notification) in
                saveAction.isEnabled = textField.text!.count > 0
            }
        }
        //For second TF
        alert.addTextField { (textField) in
            textField.placeholder = "last name"
            textField.textAlignment = .left
            textField.borderStyle = .none
            
            textField.addConstraint(textField.heightAnchor.constraint(equalToConstant: 40))
            FontAndColorConfigs.setTextFieldsTextColorAndFont(txtFields: [textField], color: .black, font: FontAndColorConfigs.getProximaRegular18())
        }
        self.present(alert, animated:true, completion: nil)
        
    }
    
    func showTriangularTipAtCenterBottom(view: UIView, parentView: UIView)  {
        
        view.backgroundColor = UIColor.clear
        view.borderColor = .black
        
        // set the shadow properties
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        view.layer.shadowOpacity = 0.2
        view.layer.shadowRadius = 4.0
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: self.view.frame.size.width - 120, y: 0))//self.view.frame.size.width - 80
        path.addLine(to: CGPoint(x: self.view.frame.size.width - 120, y: 160))
        
        // Draw arrow
        path.addLine(to: CGPoint(x: (self.view.frame.size.width - 120)/2 + 20, y: 160))
        path.addLine(to: CGPoint(x: (self.view.frame.size.width - 120)/2 , y: 180))
        path.addLine(to: CGPoint(x: (self.view.frame.size.width - 120)/2 - 20, y: 160))
        
        path.addLine(to: CGPoint(x: 0, y: 160))
        path.close()
        
        let shape = CAShapeLayer()
        shape.fillColor = UIColor.white.cgColor
        shape.path = path.cgPath
        shape.cornerRadius = 10
        view.layer.addSublayer(shape)
    }
    
    func showInputDialog(type: String, title:String? = nil, subtitle:String? = nil, actionTitle:String? = "Create", cancelTitle:String? = "Cancel", inputPlaceholder:String? = nil, inputKeyboardType:UIKeyboardType = UIKeyboardType.default, cancelHandler: ((UIAlertAction) -> Swift.Void)? = nil, actionHandler: ((_ text: String?) -> Void)? = nil) {
        
        let alert = UIAlertController(title: title, message: subtitle, preferredStyle: .alert)
        let saveAction = UIAlertAction(title: actionTitle, style: .destructive, handler: { (action:UIAlertAction) in
            
            guard let textField =  alert.textFields?.first else {
                actionHandler?(nil)
                return
            }
            
            if textField.isEmpty == true{
                
            }else{
                actionHandler?(textField.text)
            }
        })
        
        alert.addTextField { (textField:UITextField) in
            textField.placeholder = inputPlaceholder
            textField.keyboardType = inputKeyboardType
            textField.addConstraint(textField.heightAnchor.constraint(equalToConstant: 40))
            FontAndColorConfigs.setTextFieldsTextColorAndFont(txtFields: [textField], color: .black, font: FontAndColorConfigs.getProximaRegular18())
            
            saveAction.isEnabled = false
            NotificationCenter.default.addObserver(forName: UITextField.textDidChangeNotification, object: textField, queue: OperationQueue.main) { (notification) in
                saveAction.isEnabled = false
                if type == "email"{
                    
                    let isEmailVaild = AppConfigs.validateEmail(candidate: textField.text!)
                    if isEmailVaild{
                        saveAction.isEnabled = textField.text!.count > 0
                    }else{
                        saveAction.isEnabled = false
                    }
                }else{
                    saveAction.isEnabled = textField.text!.count > 0
                }
            }
        }
        
        alert.addAction(saveAction)
        alert.addAction(UIAlertAction(title: cancelTitle, style: .cancel, handler: cancelHandler))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlert(message: String, title: String = "") {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func backToPreviusController() {
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func hideCurrentViewController() {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func showSpinner(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = .lightGray//UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        vSpinner = spinnerView
    }
    
    func showSpinnerWithText(onView : UIView) {
        
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = .white//UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        
        let ai = SpinnerView(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
//        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
//        ai.color = .green
//        ai.startAnimating()
        ai.center = spinnerView.center
        
        let label = UILabel(frame: CGRect(x: onView.center.x , y: 0, width: self.view.frame.size.width, height: 42))
    
        label.numberOfLines = 0
        label.center = CGPoint(x:onView.center.x + 40, y: spinnerView.center.y + 60)
        label.textAlignment = .center
        label.textColor = .black
        label.text = "Hang tight while we load your experience!"
        label.sizeToFit()
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            spinnerView.addSubview(label)
            onView.addSubview(spinnerView)
        }
        
        vSpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            vSpinner?.removeFromSuperview()
            vSpinner = nil
        }
    }
    
}
