//
//  UIApplication.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 8/27/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import Foundation
import UIKit
extension UIApplication {
    var statusBarView: UIView? {
        if responds(to: Selector("statusBar")) {
            return value(forKey: "statusBar") as? UIView
        }
        return nil
    }
}
