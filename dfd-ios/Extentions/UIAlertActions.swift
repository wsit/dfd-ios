//
//  UIAlertActions.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 5/13/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import Foundation
import UIKit
extension UIAlertController{
    
    func AlertWithTextField(_ view:UIViewController) -> UIAlertController{
        
        let actionSheetController: UIAlertController = UIAlertController(title: "Choose your visibility", message: message, preferredStyle: UIAlertController.Style.actionSheet)
        
        
        actionSheetController.addAction(UIAlertAction(title: "Public" , style: .default , handler:{ (UIAlertAction)in
            print("Public Clicket")
        }))
        actionSheetController.addAction(UIAlertAction(title: "Private" , style: .default , handler:{ (UIAlertAction)in
            
            print("Private Clicket")
        }))
        
        
        view.present(actionSheetController, animated: true, completion: {
            print("completion block")
        })
        return actionSheetController
    }
}
