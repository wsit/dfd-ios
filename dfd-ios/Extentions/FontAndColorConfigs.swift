//
//  FontAndColorConfigs.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 8/26/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import Foundation
import UIKit

enum TextType: String {
    
    case ListTitle = "ListTitle"
    case ListSubTitle = "ListSubTitle"
    case MainTitle = "MainTitle"
    case MainSubTitle = "MainSubTitle"
    case Capsule = "Capsule"
    
}

class FontAndColorConfigs: NSObject {
    
    //text type
    static private let header = "header"
    static private let paragraph = "paragraph"
    static private let subParagraph = "subParagraph"
    
    static func getTextTypeParagraph()-> String{
        return paragraph
    }
    static func getTextTypeHeader()-> String{
        return header
    }
    static func getTextTypeSubParagraph()-> String{
        return subParagraph
    }
    static private let greenishThemeColor: UIColor = UIColor(red:0.31, green:0.82, blue:0.54, alpha:1)
    static private let profileTextColor: UIColor = UIColor(red:0.16, green:0.78, blue:0.44, alpha:1)
    static private let paragraphTextColor: UIColor = UIColor(red:0.58, green:0.58, blue:0.58, alpha:1)
    //UIColor(red:0.26, green:0.36, blue:0.46, alpha:1)
    static private let segmentedControllerActiveColor : UIColor = UIColor(red:0.32, green:0.44, blue:0.56, alpha:1)
    static private let segmentedControllerColor: UIColor = UIColor(red:0.26, green:0.36, blue:0.46, alpha:1)
    static private let seperatorColor =  UIColor(red:0.93, green:0.93, blue:0.93, alpha:1)
    
    static func getGreenishThemeColor()-> UIColor{
        return greenishThemeColor
    }
    static func getSeperatorColor()-> UIColor{
        return seperatorColor
    }
    static func getProfileTextColor()-> UIColor{
        return profileTextColor
    }
    
    static func getParagraphTextColor()-> UIColor{
        return paragraphTextColor
    }
    static func getSegmentedControllerActiveColor()-> UIColor{
        return segmentedControllerActiveColor
    }
    static func getSegmentedControllerColor()-> UIColor{
        return segmentedControllerColor
    }
    
    static  let proximaRegular23 = UIFont(name: "ProximaNova-Regular", size: 23)
    static  let proximaRegular22 = UIFont(name: "ProximaNova-Regular", size: 22)
    static  let proximaRegular20 = UIFont(name: "ProximaNova-Regular", size: 20)
    static  let proximaRegular18 = UIFont(name: "ProximaNova-Regular", size: 18)
    static  let proximaRegular16 = UIFont(name: "ProximaNova-Regular", size: 16)
    static  let proximaRegular14 = UIFont(name: "ProximaNova-Regular", size: 14)
    
    static  let proximaSemiBold22 = UIFont(name: "ProximaNova-Semibold", size: 22)
    static  let proximaSemiBold20 = UIFont(name: "ProximaNova-Semibold", size: 20)
    static  let proximaSemiBold18 = UIFont(name: "ProximaNova-Semibold", size: 18)
    static  let proximaSemiBold16 = UIFont(name: "ProximaNova-Semibold", size: 16)
    
    static func getProximaSemiBold18 () -> UIFont{
        return proximaSemiBold18!
    }
    static func getProximaSemiBold20 () -> UIFont{
        return proximaSemiBold20!
    }
    static func getProximaSemiBold22 () -> UIFont{
        return proximaSemiBold22!
    }
    static func getProximaRegular23 () -> UIFont{
        return proximaRegular23!
    }
    static func getProximaRegular22 () -> UIFont{
        return proximaRegular22!
    }
    static func getProximaRegular18 () -> UIFont{
        return proximaRegular18!
    }
    
    static func getProximaRegular16 () -> UIFont{
        return proximaRegular16!
    }
    
    static func setTextColor(labels: [UILabel], color: UIColor){
        for label in labels{
            label.textColor = color
        }
    }
    
    static func setLabelsTextColorAndFont(labels: [UILabel], color: UIColor, font: UIFont){
        for label in labels{
            label.textColor = color
            label.font = font
        }
    }
    
    static func setTextFieldsTextColorAndFont(txtFields: [UITextField], color: UIColor, font: UIFont){
        for txtfield in txtFields{
            txtfield.textColor = color
            txtfield.font = font
        }
    }
    
    
    static func setButtonsTextColorAndFont(buttons: [UIButton], color: UIColor, font: UIFont){
        for button in buttons{
            button.setTitleColor(color, for: .normal)
            button.titleLabel?.font = font
        }
    }
    
    static func setLabelFontSize(labels: [UILabel], type: String)  {
       
        let deviceType = UIDevice.current.deviceType
        if type == self.header{
            for label in labels{
                
                switch deviceType {
                    
                case .iPhone4_4S:
                    label.font = UIFont(name: "ProximaNova-Regular", size: 14)
                    
                case .iPhones_5_5s_5c_SE:
                    label.font = UIFont(name: "ProximaNova-Regular", size: 18)
                    
                case .iPhones_6_6s_7_8:
                    label.font = proximaRegular20 //UIFont(name: "ProximaNova-Regular", size: 18)
                case .iPhones_6Plus_6sPlus_7Plus_8Plus:
                    label.font = proximaRegular23
                    
                case .iPhoneX_XS_11Pro:
                    label.font = proximaRegular23
                default:
                    print("iPad or Unkown device")
                }
            }
        }
        else if type == self.paragraph{
            for label in labels{
                
                switch deviceType {
                    
                case .iPhone4_4S:
                    label.font = UIFont(name: "ProximaNova-Regular", size: 14)
                    
                case .iPhones_5_5s_5c_SE:
                    label.font = UIFont(name: "ProximaNova-Regular", size: 16)
                    
                case .iPhones_6_6s_7_8:
                    label.font = proximaRegular18 //UIFont(name: "ProximaNova-Regular", size: 18)
                    
                case .iPhones_6Plus_6sPlus_7Plus_8Plus:
                    label.font = proximaRegular20
                    
                case .iPhoneX_XS_11Pro:
                    label.font = proximaRegular20
                default:
                    print("iPad or Unkown device")
                }
            }
        }
        else if type == self.subParagraph{
            for label in labels{
                
                switch deviceType {
                    
                case .iPhone4_4S:
                    label.font = UIFont(name: "ProximaNova-Regular", size: 14)
                    
                case .iPhones_5_5s_5c_SE:
                    label.font = UIFont(name: "ProximaNova-Regular", size: 16)
                    
                case .iPhones_6_6s_7_8:
                    label.font = proximaRegular18 //UIFont(name: "ProximaNova-Regular", size: 18)
                    
                case .iPhones_6Plus_6sPlus_7Plus_8Plus:
                    label.font = proximaRegular18
                    
                case .iPhoneX_XS_11Pro:
                    label.font = proximaRegular20
                default:
                    print("iPad or Unkown device")
                }
            }
        }

    }
    
    static func setTitletLabelFontSize(labels: [UILabel])  {
        
        for label in labels{
            
            let deviceType = UIDevice.current.deviceType
            
            switch deviceType {
                
            case .iPhone4_4S:
                label.font = proximaSemiBold16
                
            case .iPhones_5_5s_5c_SE:
                label.font = proximaSemiBold16
                
            case .iPhones_6_6s_7_8:
                label.font = proximaSemiBold18
                
            case .iPhones_6Plus_6sPlus_7Plus_8Plus:
                label.font = proximaSemiBold22
                
            case .iPhoneX_XS_11Pro:
                label.font = proximaSemiBold22
                
            default:
                print("iPad or Unkown device")
                label.font = proximaSemiBold22
            }
        }
    }
    
    
    static func setDynamicLabelFontSize(labels: [UILabel], type: String)  {
       
        let deviceType = UIDevice.current.deviceType
        
        if type == TextType.MainTitle.rawValue{
            
            for label in labels{
                
                switch deviceType {
                    
                case .iPhone4_4S:
                    label.font = UIFont(name: "ProximaNova-Regular", size: 14)
                    
                case .iPhones_5_5s_5c_SE:
                    label.font = UIFont(name: "ProximaNova-Regular", size: 16)
                    
                case .iPhones_6_6s_7_8:
                    label.font = ProNovaSB18 //UIFont(name: "ProximaNova-Regular", size: 18)
                case .iPhones_6Plus_6sPlus_7Plus_8Plus:
                    label.font = ProNovaSB20
                    
                case .iPhoneX_XS_11Pro:
                    label.font = ProNovaSB22
                default:
                    print("iPad or Unkown device")
                    label.font = ProNovaSB22
                }
            }
        }
        else if type == TextType.ListTitle.rawValue{
            for label in labels{
                
                switch deviceType {
                    
                case .iPhone4_4S:
                    label.font = UIFont(name: "ProximaNova-Regular", size: 14)
                    
                case .iPhones_5_5s_5c_SE:
                    label.font = UIFont(name: "ProximaNova-Regular", size: 14)
                    
                case .iPhones_6_6s_7_8:
                    label.font = ProNovaR16 //UIFont(name: "ProximaNova-Regular", size: 18)
                case .iPhones_6Plus_6sPlus_7Plus_8Plus:
                    label.font = ProNovaR18
                    
                case .iPhoneX_XS_11Pro:
                    label.font = ProNovaR20
                default:
                    print("iPad or Unkown device")
                    label.font = ProNovaR20
                }
            }
        }
        else if type == TextType.ListSubTitle.rawValue{
            for label in labels{
                
                switch deviceType {
                    
                case .iPhone4_4S:
                    label.font = UIFont(name: "ProximaNova-Regular", size: 11)
                    
                case .iPhones_5_5s_5c_SE:
                    label.font = UIFont(name: "ProximaNova-Regular", size: 11)
                    
                case .iPhones_6_6s_7_8:
                    label.font = ProNovaR11 //UIFont(name: "ProximaNova-Regular", size: 18)
                case .iPhones_6Plus_6sPlus_7Plus_8Plus:
                    label.font = ProNovaR1225
                    
                case .iPhoneX_XS_11Pro:
                    label.font = ProNovaR14
                default:
                    print("iPad or Unkown device")
                    label.font = ProNovaR16
                }
            }
        }
        else if type == TextType.MainSubTitle.rawValue{
            for label in labels{
                
                switch deviceType {
                    
                case .iPhone4_4S:
                    label.font = MyFont(.installed(.ProximaNovaSemibold), size: .standard(.s9)).instance
                    
                case .iPhones_5_5s_5c_SE:
                    label.font = MyFont(.installed(.ProximaNovaSemibold), size: .standard(.s9)).instance
                    
                case .iPhones_6_6s_7_8:
                    label.font = MyFont(.installed(.ProximaNovaSemibold), size: .standard(.s11)).instance
                case .iPhones_6Plus_6sPlus_7Plus_8Plus:
                    label.font = MyFont(.installed(.ProximaNovaSemibold), size: .standard(.s1225)).instance
                    
                case .iPhoneX_XS_11Pro:
                    label.font = MyFont(.installed(.ProximaNovaSemibold), size: .standard(.s1350)).instance
                default:
                    print("iPad or Unkown device")
                    label.font = MyFont(.installed(.ProximaNovaSemibold), size: .standard(.s1350)).instance
                }
            }
        }

    }
    
    static func setDynamicButtonsFontSize(buttons: [UIButton], type: String)  {
       
        let deviceType = UIDevice.current.deviceType
        
        if type == TextType.MainTitle.rawValue{
            
            for btn in buttons{
                
                switch deviceType {
                    
                case .iPhone4_4S:
                    btn.titleLabel?.font = UIFont(name: "ProximaNova-Regular", size: 14)
                    
                case .iPhones_5_5s_5c_SE:
                    btn.titleLabel?.font = UIFont(name: "ProximaNova-Regular", size: 16)
                    
                case .iPhones_6_6s_7_8:
                    btn.titleLabel?.font = ProNovaR18 //UIFont(name: "ProximaNova-Regular", size: 18)
                case .iPhones_6Plus_6sPlus_7Plus_8Plus:
                    btn.titleLabel?.font = ProNovaR20
                    
                case .iPhoneX_XS_11Pro:
                    btn.titleLabel?.font = ProNovaR22
                default:
                    print("iPad or Unkown device")
                    btn.titleLabel?.font = ProNovaR22
                }
            }
        }
        else if type == TextType.ListTitle.rawValue{
            for btn in buttons{
                
                switch deviceType {
                    
                case .iPhone4_4S:
                    btn.titleLabel?.font = UIFont(name: "ProximaNova-Regular", size: 14)
                    
                case .iPhones_5_5s_5c_SE:
                    btn.titleLabel?.font = UIFont(name: "ProximaNova-Regular", size: 14)
                    
                case .iPhones_6_6s_7_8:
                    btn.titleLabel?.font = ProNovaR16 //UIFont(name: "ProximaNova-Regular", size: 18)
                case .iPhones_6Plus_6sPlus_7Plus_8Plus:
                    btn.titleLabel?.font = ProNovaR18
                    
                case .iPhoneX_XS_11Pro:
                    btn.titleLabel?.font = ProNovaR20
                default:
                    print("iPad or Unkown device")
                    btn.titleLabel?.font = ProNovaR20
                }
            }
        }
        else if type == TextType.ListSubTitle.rawValue{
            for btn in buttons{
                
                switch deviceType {
                    
                case .iPhone4_4S:
                    btn.titleLabel?.font = UIFont(name: "ProximaNova-Regular", size: 11)
                    
                case .iPhones_5_5s_5c_SE:
                   btn.titleLabel?.font = UIFont(name: "ProximaNova-Regular", size: 11)
                    
                case .iPhones_6_6s_7_8:
                    btn.titleLabel?.font = ProNovaR11 //UIFont(name: "ProximaNova-Regular", size: 18)
                case .iPhones_6Plus_6sPlus_7Plus_8Plus:
                    btn.titleLabel?.font = ProNovaR1225
                    
                case .iPhoneX_XS_11Pro:
                    btn.titleLabel?.font = ProNovaR14
                default:
                    print("iPad or Unkown device")
                    btn.titleLabel?.font = ProNovaR16
                }
            }
        }
        else if type == TextType.MainSubTitle.rawValue{
            for btn in buttons{
                
                switch deviceType {
                    
                case .iPhone4_4S:
                    btn.titleLabel?.font = MyFont(.installed(.ProximaNovaSemibold), size: .standard(.s9)).instance
                    
                case .iPhones_5_5s_5c_SE:
                    btn.titleLabel?.font = MyFont(.installed(.ProximaNovaSemibold), size: .standard(.s9)).instance
                    
                case .iPhones_6_6s_7_8:
                   btn.titleLabel?.font = MyFont(.installed(.ProximaNovaSemibold), size: .standard(.s11)).instance
                case .iPhones_6Plus_6sPlus_7Plus_8Plus:
                    btn.titleLabel?.font = MyFont(.installed(.ProximaNovaSemibold), size: .standard(.s1225)).instance
                    
                case .iPhoneX_XS_11Pro:
                    btn.titleLabel?.font = MyFont(.installed(.ProximaNovaSemibold), size: .standard(.s1350)).instance
                default:
                    print("iPad or Unkown device")
                    btn.titleLabel?.font = MyFont(.installed(.ProximaNovaSemibold), size: .standard(.s1350)).instance
                }
            }
        }

    }
    
}
