//
//  UIView.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 11/27/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func pushTransition(_ duration:CFTimeInterval) {
        let animation:CATransition = CATransition()
//        animation.timingFunction = CAMediaTimingFunction(name:
//            CAMediaTimingFunctionName.easeInEaseOut)
//        animation.type = CATransitionType.push
//        animation.subtype = CATransitionSubtype.fromTop
        animation.timingFunction = CAMediaTimingFunction.init(name: .easeInEaseOut)
        animation.type = .push
        animation.subtype = .fromRight
        animation.duration = duration
        
        
        
        layer.add(animation, forKey: CATransitionType.push.rawValue)
    }
    
    func pushNameTransition(_ duration:CFTimeInterval) {
            let animation:CATransition = CATransition()
    //        animation.timingFunction = CAMediaTimingFunction(name:
    //            CAMediaTimingFunctionName.easeInEaseOut)
    //        animation.type = CATransitionType.push
    //        animation.subtype = CATransitionSubtype.fromTop
            animation.timingFunction = CAMediaTimingFunction.init(name: .easeInEaseOut)
            animation.type = .push
            animation.subtype = .fromLeft
            animation.duration = duration
            
            
            
            layer.add(animation, forKey: CATransitionType.push.rawValue)
        }
    
    func pushTransitionForRightSwipe(_ duration:CFTimeInterval) {
            let animation:CATransition = CATransition()
    //        animation.timingFunction = CAMediaTimingFunction(name:
    //            CAMediaTimingFunctionName.easeInEaseOut)
    //        animation.type = CATransitionType.push
    //        animation.subtype = CATransitionSubtype.fromTop
            animation.timingFunction = CAMediaTimingFunction.init(name: .easeInEaseOut)
            animation.type = .push
            animation.subtype = .fromLeft
            animation.duration = duration
            
            
            
            layer.add(animation, forKey: CATransitionType.push.rawValue)
        }
    
}

