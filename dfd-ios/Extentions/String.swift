//
//  String.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 7/11/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import Foundation
extension String {
    func toDouble() -> Double? {
        return NumberFormatter().number(from: self)?.doubleValue
    }
}
