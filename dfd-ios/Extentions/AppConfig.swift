//
//  AppConfig.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 5/3/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import Foundation
import UIKit
import TTGSnackbar
import Alamofire
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit
import SwiftyJSON

class AppConfigs: NSObject {
    
    static  let registeredEmail = "registeredEmail"
    
    static  let accessToken = "access_token"
    static  let fcmToken = "fcm_token"
    static  let refreshToken = "refresh_token"
    
    static  let tokenType = "token_type"
    static  let expiresIn = "expires_in"
    static  let scope = "scope"
    
    static  let uid = "uid"
    static  let orgID = "org_id"
    static  let orgSlug = "org_slug"
    static  let orgName = "org_name"
    static  let name = "name"
    
    static  let selectedOrgId = "id"
    static  let selectedOrgSlug = "slug"
    static  let userInfo = "userInfo"
    static  let historyID = "historyID"
    static  let isFirstLogin = "isFirstLogin"
    static let drivingType = "drivingType"
    static let tilesData = "tilesData"
    static let isPaused = "isPaused"
    
    static func saveCurrentUserInfo(user: CurrentUserInfoResponse){
        
        let preferences = UserDefaults.standard
        let encoder = JSONEncoder()
        
        if let encodedUserInfo = try? encoder.encode(user) {
            preferences.set(encodedUserInfo, forKey: userInfo)
        }
        
        let didSave = preferences.synchronize()
        if !didSave {
            print(" userInfo wasn't saved")
        }
    }
    
    static func getCurrentUserInfo() -> CurrentUserInfoResponse{
        
        var userInformation : CurrentUserInfoResponse  = CurrentUserInfoResponse(id: 0, lastLogin: "", firstName: "", lastName: "", email: "", phoneNumber: "", invitedBy: nil, photo: "", photoThumb: "", isActive: false, isAdmin: false, thinker: false, upgradeInfo: nil, createdAt: "", updatedAt: "", defaultList: nil)
        
        let preferences = UserDefaults.standard
        
        if let data = preferences.object(forKey: userInfo) as? Data {
            
            let decoder = JSONDecoder()
            if let currentUser = try? decoder.decode(CurrentUserInfoResponse.self, from: data) {
                print(currentUser.id ?? 0)
                userInformation = currentUser
            }
        }
        
        return userInformation
        
    }
    
    static func saveEmailAddress(email: String){
        
        let preferences = UserDefaults.standard
        preferences.set(email, forKey: registeredEmail)
        
        let didSave = preferences.synchronize()
        if !didSave {
            print("registeredEmail wasn't saved")
        }
    }
    
    static func getSavedEmailAddress() -> String{
        
        let preferences = UserDefaults.standard
        let email = preferences.string(forKey: registeredEmail) ?? ""
        
        return email
        
    }
    
    static func saveAccessToken(token: String){
        
        let preferences = UserDefaults.standard
        preferences.set(token, forKey: accessToken)
        
        let didSave = preferences.synchronize()
        if !didSave {
            print("accessToken wasn't saved")
        }
    }
    
    static func getSavedAccessToken() -> String{
        
        let preferences = UserDefaults.standard
        let email = preferences.string(forKey: accessToken) ?? ""
        print(email)
        return email
        
    }
    
    
    static func saveFcmToken(token: String){
        
        let preferences = UserDefaults.standard
        preferences.set(token, forKey: fcmToken)
        
        let didSave = preferences.synchronize()
        if !didSave {
            print("fcmToken wasn't saved")
        }
    }
    
    static func getSavedFcmToken() -> String{
        
        let preferences = UserDefaults.standard
        let fcm  = preferences.string(forKey: fcmToken) ?? ""
        print(fcm)
        return fcm
        
    }
    
    
    static func saveRefreshToken(token: String){
        
        let preferences = UserDefaults.standard
        preferences.set(token, forKey: refreshToken)
        
        let didSave = preferences.synchronize()
        if !didSave {
            print("refreshToken wasn't saved")
        }
    }
    
    
    static func saveFirstLoginFlag(flag: Bool){
        
        let preferences = UserDefaults.standard
        preferences.set(flag, forKey: isFirstLogin)
        
        let didSave = preferences.synchronize()
        if !didSave {
            print("isFirstLogin wasn't saved")
        }
    }
    
    
    static func getFirstLoginFlag() -> Bool{
        
        let preferences = UserDefaults.standard
        let hi = preferences.bool(forKey: isFirstLogin)
        print("isFirstLogin",hi)
        return hi
        
    }
    
    static func saveHistoryID(id: Int){
        
        let preferences = UserDefaults.standard
        preferences.set(id, forKey: historyID)
        
        let didSave = preferences.synchronize()
        if !didSave {
            print("refreshToken wasn't saved")
        }
    }
    
    
    static func getSavedHistoryID() -> Int{
        
        let preferences = UserDefaults.standard
        let hi = preferences.integer(forKey: historyID) 
        print("historyID",hi)
        return hi
        
    }
    
    static func deleteHistoryID() {
        
        let preferences = UserDefaults.standard
        let keyValue = preferences.string(forKey:historyID)
        print("historyID",keyValue)
        preferences.removeObject(forKey:historyID)
        
    }
    
    
    static func saveDrivingType(token: String){
           
           let preferences = UserDefaults.standard
           preferences.set(token, forKey: drivingType)
           
           let didSave = preferences.synchronize()
           if !didSave {
               print("drivingType wasn't saved")
           }
       }
    
    static func getSavedDrivingType() -> String{
        
        let preferences = UserDefaults.standard
        let hi = preferences.string(forKey: drivingType) ?? ""
        print("drivingType",hi)
        return hi
        
    }
    
    static func deleteDrivingType() {
        
        let preferences = UserDefaults.standard
        let keyValue = preferences.string(forKey:drivingType)
        print("drivingType",keyValue)
        preferences.removeObject(forKey:drivingType)
        
    }
    
    static func saveDrivingPausedStatus(status: Bool){
           
           let preferences = UserDefaults.standard
           preferences.set(status, forKey: isPaused)
           
           let didSave = preferences.synchronize()
           if !didSave {
               print("drivingType wasn't saved")
           }
       }
    
    static func getSavedDrivingPausedStatus() -> Bool{
        
        let preferences = UserDefaults.standard
        let hi = preferences.bool(forKey: isPaused)
        print("drivingType",hi)
        return hi
        
    }
    
    static func deletePauseStatus() {
        
        let preferences = UserDefaults.standard
        let keyValue = preferences.string(forKey:isPaused)
        print("drivingType",keyValue)
        preferences.removeObject(forKey:isPaused)
        
    }
    
    static func saveTilesInfo(user: TilesData){
        
        let preferences = UserDefaults.standard
        let encoder = JSONEncoder()
        
        if let encodedUserInfo = try? encoder.encode(user) {
            preferences.set(encodedUserInfo, forKey: tilesData)
        }
        
        let didSave = preferences.synchronize()
        if !didSave {
            print(" userInfo wasn't saved")
        }
    }
    
    static func getSavedTilesInfo() -> TilesData{
        
        var userInformation : TilesData  =  TilesData()
        let preferences = UserDefaults.standard
        if let data = preferences.object(forKey: tilesData) as? Data {
            
            let decoder = JSONDecoder()
            if let currentUser = try? decoder.decode(TilesData.self, from: data) {
                print(currentUser.seconds)
                userInformation = currentUser
            }
        }
        
        return userInformation
        
    }
    
    
    static func deleteTilesData() {
        
        let preferences = UserDefaults.standard
        let keyValue = preferences.string(forKey:tilesData)
        print("drivingType",keyValue)
        preferences.removeObject(forKey:tilesData)
        
    }
    
    
    
    
    
    static func getSavedRefreshToken() -> String{
        
        let preferences = UserDefaults.standard
        let email = preferences.string(forKey: refreshToken) ?? ""
        
        return email
        
    }
    
    static func radians<T: FloatingPoint>(degrees: T) -> T {
        return .pi * degrees / 180
    }
    
    static func degrees<T: FloatingPoint>(radians: T) -> T {
        return radians * 180 / .pi
    }
    
    static func setLabelFontSize(label: UILabel)  {
        
        let deviceType = UIDevice.current.deviceType
        
        switch deviceType {
            
        case .iPhone4_4S:
            label.font = UIFont(name: "ProximaNova-Regular", size: 12)  //UIFont.systemFont(ofSize: 10)
            
        case .iPhones_5_5s_5c_SE:
            label.font = UIFont(name: "ProximaNova-Regular", size: 13) //UIFont.systemFont(ofSize: 12)
            
        case .iPhones_6_6s_7_8:
            label.font = UIFont(name: "ProximaNova-Regular", size: 14) //UIFont.systemFont(ofSize: 14)
            
        case .iPhones_6Plus_6sPlus_7Plus_8Plus:
            label.font = UIFont(name: "ProximaNova-Regular", size: 16) //UIFont.systemFont(ofSize: 16)
            
        case .iPhoneX_XS_11Pro:
            label.font = UIFont(name: "ProximaNova-Regular", size: 18)//UIFont.systemFont(ofSize: 18)
            
        default:
            print("iPad or Unkown device")
            label.font = UIFont(name: "ProximaNova-Regular", size: 20) // UIFont.systemFont(ofSize: 20)
        }
    }
    
    static func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        
        label.sizeToFit()
        
        var height = label.frame.height
        if height < 30 {
            height = 30
        }
        return height
    }
    
    static func setRootViewController()  {
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "WelcomeScreenViewController") as! WelcomeScreenViewController
        UIApplication.shared.keyWindow?.rootViewController = vc
    }
    
    static func showSnacbar(message:String, textColor: UIColor){
        let snackbar = TTGSnackbar(message: message, duration: .long)
        snackbar.messageTextColor = .black // textColor
        //snackbar.animationType = .slideFromTopBackToTop
        snackbar.animationType = .slideFromLeftToRight
        snackbar.messageTextAlign = .center
        snackbar.backgroundColor = FontAndColorConfigs.getSeperatorColor()
        //snackbar.topMargin = 80.0
        snackbar.bottomMargin = 60.0
        snackbar.messageTextFont = ProNovaR16
        snackbar.show()
    }
    
    static func showTopSnacbar(message:String, textColor: UIColor){
        let snackbar = TTGSnackbar(message: message, duration: .middle)
        snackbar.messageTextColor = .black // textColor
        //snackbar.animationType = .slideFromTopBackToTop
        snackbar.animationType = .slideFromLeftToRight
        snackbar.messageTextAlign = .center
        snackbar.backgroundColor = FontAndColorConfigs.getSeperatorColor()
        //snackbar.topMargin = 80.0
        snackbar.bottomMargin = 120.0
        snackbar.messageTextFont = ProNovaR16
        snackbar.show()
    }
    
    
    static func validateEmail(candidate: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: candidate)
    }
    
    static func getTopMostViewController() -> UIViewController? {
        var topMostViewController = UIApplication.shared.keyWindow?.rootViewController
        
        while let presentedViewController = topMostViewController?.presentedViewController {
            topMostViewController = presentedViewController
        }
        
        return topMostViewController
    }
    
    
    static func updateUserInterface(view: UIView) {
        switch Network.reachability.status {
        case .unreachable:
            //view.backgroundColor = .red
            let snackbar = TTGSnackbar(message: "Option not available when offline", duration: .middle)
            snackbar.messageTextColor = .red
            snackbar.show()
        case .wwan: break
        //view.backgroundColor = .yellow
        case .wifi: break
            //view.backgroundColor = .green
        }
        print("Reachability Summary")
        print("Status:", Network.reachability.status)
        print("HostName:", Network.reachability.hostname ?? "nil")
        print("Reachable:", Network.reachability.isReachable)
        print("Wifi:", Network.reachability.isReachableViaWiFi)
    }
    
    
    static func refreshCurrentToken(view: UIView){
        
        let parameters = [
            K.APIParameterKey.clientID: "hasan@workspaceit.com",
            K.APIParameterKey.clientSecret: "hasan@workspaceit.com",
            K.APIParameterKey.grantType: "refresh_token",
            K.APIParameterKey.refreshToken: AppConfigs.getSavedRefreshToken()
        ]
        print(parameters)
        
        //        let manager = Alamofire.SessionManager.default
        //        manager.session.configuration.timeoutIntervalForRequest = 120
        //
        view.activityStartAnimating(activityColor: .black, backgroundColor: .white)
        
        Alamofire.request(UrlManager.loginURL(), method: .post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                print("success",data)
                view.activityStopAnimating()
                
                let statusCode = response.response?.statusCode
                let loginResponse = try? JSONDecoder().decode(LoginResponse.self, from: response.data!)
                
                if statusCode == 200 {
                    //save access token and refresh token
                    AppConfigs.saveAccessToken(token: (loginResponse?.accessToken)!)
                    AppConfigs.saveRefreshToken(token: (loginResponse?.refreshToken)!)
                }
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                if statusCode == 500{
                    AppConfigs.showSnacbar(message: ErrorMessage.c500.rawValue, textColor: .red)
                }
                else if statusCode == 502{
                    AppConfigs.showSnacbar(message: ErrorMessage.c502.rawValue, textColor: .red)
                    
                }else{
                    AppConfigs.showSnacbar(message: ErrorMessage.c502.rawValue, textColor: .red)
                }
                //AppConfigs.showSnacbar(message: error.localizedDescription, textColor: .red)
                
            }
            view.activityStopAnimating()
        }
        
    }
    static func resetDefaults() {
        
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
        print(Array(UserDefaults.standard.dictionaryRepresentation().keys).count)
    }
    
    static func registerSocialUser(view: UIView, vc: UIViewController, type: String, Compleationhandler:@escaping (LoginResponse?, _ responseStatus:Int, _ status: Bool, _ error:String?)->Void){
        
        var parameters: Parameters = [:]
        if type == "google"{
            print("Google Token-->",(GIDSignIn.sharedInstance()?.currentUser.authentication.idToken)!)
            parameters = [
                K.APIParameterKey.clientID: UrlManager.getClientID(),
                K.APIParameterKey.clientSecret: UrlManager.getClientSecret(),
                K.APIParameterKey.grantType: "convert_token",
                "backend" : "google-oauth2",
                "token" : (GIDSignIn.sharedInstance()?.currentUser.authentication.accessToken)!,
                "type": "sign-in"
            ]
        }else
        {
            print((AccessToken.current?.tokenString) ?? "")
            parameters = [
                K.APIParameterKey.clientID: UrlManager.getClientID(),
                K.APIParameterKey.clientSecret: UrlManager.getClientSecret(),
                K.APIParameterKey.grantType: "convert_token",
                "backend" : "facebook",
                "token" : (AccessToken.current?.tokenString) ?? "",
                //"type": "sign-in"
            ]
        }
        
        let urlString: String = UrlManager.baseURL() + "/auth/convert-token" // "/auth/convert-token-review/" // "/auth/convert-token"
        print(urlString)
        
        print(parameters)
        let headers: HTTPHeaders = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        view.activityStartAnimating(activityColor: .black, backgroundColor: .white)
        
        Alamofire.request(urlString, method: .post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                print("success",data)
                view.activityStopAnimating()
                
                let statusCode = response.response?.statusCode
                
                let loginResponse = try? JSONDecoder().decode(LoginResponse.self, from: response.data!)
                //let loginResponse = socialSignInResponse?.data
                if statusCode == 200 {
                    //save access token and refresh token
                    if loginResponse != nil{

//                        if loginResponse?.status == true{
//                            Compleationhandler(loginResponse!, 200, true,loginResponse?.errorDescription)
//
//                        }else{
//                            Compleationhandler(loginResponse!, 400, false,loginResponse?.errorDescription)
//                        }
                        Compleationhandler(loginResponse!, 200, true,loginResponse?.errorDescription)
                    }else{
//                        do{
//                            let json = try JSON(data: response.data!)
//                            let status = json["status"].boolValue
//                            let message = json["message"].stringValue
//                            let data = json["data"]
////                            let accessToken = data[].stringValue
////                            let expiresIn = data[].stringValue
////                            let accessToken = data[].stringValue
////                            let accessToken = data[].stringValue
////                            let accessToken = data[].stringValue
////                            let accessToken = data[].stringValue
////                            let accessToken = data[].stringValue
////
////                            let loginResponse : SocialSignInResponse = SocialSignInResponse(status: status, data: LoginResponse(accessToken: <#T##String?#>, expiresIn: <#T##Int?#>, tokenType: <#T##String?#>, scope: <#T##String?#>, refreshToken: <#T##String?#>, error: <#T##String?#>, errorDescription: <#T##String?#>), message: message)
//                            Compleationhandler(nil, 9803,false,message)
//
//                        }catch let error {
//                            print(error.localizedDescription)
//                        }
                    }
                    //self.getCurrentUserInfo(urlString: url)
                }
                else if statusCode == 400
                {
                    vc.showAlert(message:loginResponse?.errorDescription ?? "", title: loginResponse?.error ?? "")
                    Compleationhandler(loginResponse!, 400, false, loginResponse?.errorDescription)
                }else if statusCode == 401
                {
//                    vc.showAlert(message: (socialSignInResponse?.err ?? ""), title: "Error!")

                    vc.showAlert(message:loginResponse?.errorDescription ?? "", title: loginResponse?.error ?? "")
                    Compleationhandler(loginResponse!, 401, false, loginResponse?.errorDescription)
                }else{
                    vc.showAlert(message: "Undefined", title: "Error!")
                    Compleationhandler(loginResponse!, 9803, false, ErrorMessage.c500.rawValue) //custom code. user any for your own purpose
                }
                
            case.failure(let error):
                view.activityStopAnimating()
                print("Not Success",error)
                let code = response.response?.statusCode
                if code == 500{
                    AppConfigs.showSnacbar(message: ErrorMessage.c500.rawValue, textColor: .red)
                }
                else if code == 502{
                    AppConfigs.showSnacbar(message: ErrorMessage.c502.rawValue, textColor: .red)
                    
                }else{
                    AppConfigs.showSnacbar(message: ErrorMessage.c502.rawValue, textColor: .red)
                }
                //AppConfigs.showSnacbar(message: error.localizedDescription, textColor: .red)
                
                Compleationhandler(nil, 9803,false, ErrorMessage.c500.rawValue)
                
            }
            
            view.activityStopAnimating()
        }
    }
    
    static func registerAppleUser(view: UIView, vc: UIViewController, parameters: Parameters, Compleationhandler:@escaping (Data?, _ responseStatus:Int, _ error:String?)->Void){
        
        let urlString: String = "https://api.houzes.com/auth/apple-login/"
        print(urlString)
        
        print(parameters)
        let headers: HTTPHeaders = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        view.activityStartAnimating(activityColor: .black, backgroundColor: .white)
        
        Alamofire.request(urlString, method: .post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                print("success",data)
                view.activityStopAnimating()
                
                let statusCode = response.response?.statusCode
                                
                if statusCode == 200 {
                    //save access token and refresh token
                    
                    Compleationhandler(response.data, 200, nil)
                    //self.getCurrentUserInfo(urlString: url)
//                }
//                else if statusCode == 400
//                {
//                    vc.showAlert(message:(loginResponse?.errorDescription ?? ""), title: "Error!")
//                    Compleationhandler(loginResponse!, 400, nil)
//                }else if statusCode == 401
//                {
//                    vc.showAlert(message: (loginResponse?.errorDescription ?? ""), title: "Error!")
//                    Compleationhandler(loginResponse!, 401, nil)
                }else{
                    vc.showAlert(message: "Undefined", title: "Error!")
                    Compleationhandler(response.data, 9803, nil) //custom code. user any for your own purpose
                }
                
            case.failure(let error):
                view.activityStopAnimating()
                print("Not Success",error)
                let code = response.response?.statusCode
                if code == 500{
                    AppConfigs.showSnacbar(message: ErrorMessage.c500.rawValue, textColor: .red)
                }
                else if code == 502{
                    AppConfigs.showSnacbar(message: ErrorMessage.c502.rawValue, textColor: .red)
                    
                }else{
                    AppConfigs.showSnacbar(message: ErrorMessage.c502.rawValue, textColor: .red)
                }
                //AppConfigs.showSnacbar(message: error.localizedDescription, textColor: .red)
                
                Compleationhandler(nil, 9803, error.localizedDescription)
                
            }
            
            view.activityStopAnimating()
        }
    }
    
    
    
    
}


