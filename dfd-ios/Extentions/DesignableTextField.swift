//
//  DesignableTextField.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 5/30/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit

@IBDesignable
public class DesignableUITextField: UITextField {
    
    // Provides left padding for images
    override public func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        var textRect = super.leftViewRect(forBounds: bounds)
        textRect.origin.x += leftPadding
        return textRect
    }
    
    override public func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        var textRect = super.rightViewRect(forBounds: bounds)
        textRect.origin.x -= rightPadding
        return textRect
    }
    
    @IBInspectable var leftImage: UIImage? {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var rightImage: UIImage? {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var leftPadding: CGFloat = 0
    @IBInspectable var rightPadding: CGFloat = 0
    
    @IBInspectable var color: UIColor = UIColor.lightGray {
        didSet {
            updateView()
        }
    }
//    @IBInspectable var borderColor: UIColor = UIColor.lightGray {
//        didSet {
//            self.layer.borderWidth = 1
//            self.borderStyle = .none
//            self.layer.borderColor = borderColor.cgColor
//        }
//    }
    @IBInspectable var borderCornerRadius: CGFloat = 4 {
        didSet {
            self.layer.borderWidth = 1
            self.borderStyle = .none
            self.layer.cornerRadius = borderCornerRadius
        }
    }
    
    @IBInspectable var textPadding: CGFloat = 0 {
        didSet {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: textPadding, height: self.frame.height))
            self.leftView = paddingView
            self.leftViewMode = .always
        }
    }
    
    func updateView() {
        if let image = leftImage {
            leftViewMode = UITextField.ViewMode.always
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            imageView.image = image
            // Note: In order for your image to use the tint color, you have to select the image in the Assets.xcassets and change the "Render As" property to "Template Image".
            imageView.tintColor = color
            leftView = imageView
        } else {
            leftViewMode = UITextField.ViewMode.never
            leftView = nil
        }
        
        if let image = rightImage {
            rightViewMode = UITextField.ViewMode.always
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            imageView.image = image
            imageView.tintColor = color
            rightView = imageView
        } else {
            rightViewMode = UITextField.ViewMode.never
            rightView = nil
        }
        
        // Placeholder text color
        attributedPlaceholder = NSAttributedString(string: placeholder != nil ?  placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: color])
    }
}
//class DesignableTextField: UITextField {
    
//    @IBInspectable var cornerRadius : CGFloat = 0{
//        didSet{
//            layer.cornerRadius = cornerRadius
//            //updateView()
//        }
//    }
//    
//    @IBInspectable var leftImage: UIImage?{
//        didSet{
//            updateView()
//        }
//    }
//
//
//    @IBInspectable var rightImage: UIImage? {
//        didSet {
//            updateView()
//        }
//    }
//
//    @IBInspectable var leftPadding : CGFloat = 0 {
//        didSet{
//            updateView()
//        }
//    }
//
//    @IBInspectable var rightPadding: CGFloat = 0{
//        didSet{
//            updateView()
//        }
//    }
//
//    func updateView() {
//
//        if let image = leftImage{
//            leftViewMode = .always
//            let imageView =  UIImageView(frame:CGRect(x: leftPadding, y: 0, width: 20, height: 20))
//            imageView.image = image
//            imageView.tintColor = tintColor
//
//            var width = leftPadding + 20
//
//            if borderStyle == UITextField.BorderStyle.none || borderStyle == UITextField.BorderStyle.line {
//                width = width + 8
//            }
//
//            let view = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 20))
//            view.addSubview(imageView)
//            leftView = view
//
//        }else{
//            leftViewMode = .never
//        }
//
//        if let image = rightImage {
//            rightViewMode = UITextField.ViewMode.always
//            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
//            imageView.image = image
//            imageView.tintColor = tintColor
//            rightView = imageView
//        } else {
//            rightViewMode = UITextField.ViewMode.never
//            rightView = nil
//        }
//        attributedPlaceholder = NSAttributedString(string: placeholder != nil ? placeholder! : "search address", attributes: [NSAttributedString.Key.foregroundColor: tintColor])
//    }
//
//
//}
