//
//  Colors.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 11/20/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    var profileBlueRoundColor : UIColor{
        return UIColor(red:0.14, green:0.78, blue:0.47, alpha:1)
    }
    
    var profileSeperatorColor : UIColor{
        return UIColor(red:0.89, green:0.91, blue:0.95, alpha:1)
    }
    
    
    var listTitleColor : UIColor{
        return UIColor(red:0, green:0, blue:0, alpha:0.8)
    }
    
    var pinkListTitleColor : UIColor{
        return UIColor(red:1, green:0.26, blue:0.67, alpha:1)
    }
    
    var listSubTitleColor : UIColor{
        return UIColor(red:0, green:0, blue:0, alpha:0.6)
    }
    
    var buttonBackgroundColor : UIColor{
        return UIColor(red:0.14, green:0.75, blue:0.45, alpha:1)
    }
    
    
    var disableButtonBackgroundColor : UIColor{
        return   UIColor(red:0.65, green:0.88, blue:0.74, alpha:1.0)
    }
    
    var fbButtonBackgroundColor : UIColor{
        return UIColor(red:0.25, green:0.36, blue:0.69, alpha:1)
    }
    
    var segmentActiveColor: UIColor{
        return UIColor(red:0.14, green:0.78, blue:0.47, alpha:1)
    }
    
    var segmentInactiveColor: UIColor{
        return  UIColor(red:0.37, green:0.38, blue:0.45, alpha:1)
    }
    
    var listSeperatorColor: UIColor{
        return UIColor(red:0.82, green:0.82, blue:0.82, alpha:1)
    }
    
    var greenButtonBackground: UIColor{
        return UIColor(red:0.14, green:0.78, blue:0.47, alpha:1.0) //UIColor(red:0.14, green:0.78, blue:0.47, alpha:1)
    }
    
    var blakishButtonBackground: UIColor{
        return UIColor(red:0.37, green:0.38, blue:0.45, alpha:1)
    }
    
    var powerTraceFlagColor: UIColor{
        return UIColor(red:0.25, green:0.55, blue:1, alpha:1)
    }
    
    var blueCapsuleColor: UIColor{
        return UIColor(red:0.21, green:0.67, blue:0.91, alpha:1)
    }
    
    var greenCapsuleColor: UIColor{
        return UIColor(red:0.14, green:0.78, blue:0.47, alpha:1)
    }
    
    var selectedCellBackgroundColor: UIColor{
        return UIColor(red:0.92, green:0.92, blue:0.92, alpha:1) //UIColor(red:1, green:0.95, blue:0.84, alpha:1)
    }
    
    var popUpBluishTitleColor: UIColor{
        return UIColor(red:0.26, green:0.55, blue:1, alpha:1)
    }
    
    var soloTitleColor: UIColor{
        return UIColor(red:0.37, green:0.67, blue:0.90, alpha:1.0)
    }
    var freeTitleColor: UIColor{
        return UIColor(red:0.48, green:0.87, blue:0.74, alpha:1.0)
    }
    var teamTitleColor: UIColor{
        return UIColor(red:0.29, green:0.31, blue:0.31, alpha:1.0)
    }
    
    
}
