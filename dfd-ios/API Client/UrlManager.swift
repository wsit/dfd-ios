//
//  UrlManager.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 5/23/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import Foundation

class UrlManager: NSObject {
    
    private static let DEV_BASE_URL = "https://api.houzes.com"//https://devapi.houzes.com"//https://api.houzes.com" //http://54.84.137.18:8001"//https://api.houzes.com"//"http://192.168.1.22:8000"//"https://api.houzes.com"// "http://3.83.112.37:8001" //http://3.83.112.37:8000" //"http://3.83.112.37:8000" //"http://58.84.34.65:8191" //"http://58.84.34.65:3001" // "http://172.18.1.11:3001"
    private static let PRIVACY_POLICY_URL = "https://houzes.com/privacy-policy"
    private static let TERMS_SERVICES_URL = "https://houzes.com/privacy-policy"
    
    private static let CLIENT_ID_STRING = "b4eGPYQSTehw9G6sv36G8mXWXuW3qWd5z3EL4qpq"//enKpO5OOK8AFVY2U5fuR5l3uD9UH4mGKrJXpTbGJ"
    private static let CLIENT_SECRET_STRING = "NzrpAlEBLOE8RN7bbbFSVAF8Q8lm3DqFgUlwLZ5gS7ribxsiLfBO5F659KjDkfgjHNAvvplv8hSsTpjQP5YJTFeMHsTubkGq0Eero6jqWIJgVu8tx29pfvTczKatDKny" // "vQgrl1HEfiUktjgXFmC3wK5t7JkU6L6mDwDKkBfb4h4F5yAslvaKXs9kKcH5g0Gcmwi67CC8Lhi1p9Bd0Roo9kexnC9VRanLp1SMmzrskfjLTJwyUALFbckI0j58QL1Q"
    
    private static let LOGIN_STRING = "/o/token/"
    private static let SIGN_UP_STRING = "/api/user/"
    private static let SOCIAL_LOGIN_VERIFY_STRING = "/api/verify-email/"
    private static let API_STRING = "/api"
    private static let LOGOUT_STRING = "/oauth/token/revoke"
    
    
    private static let LIST_STRING = "/list/?limit=10&offset=0"
    private static let ADD_TO_LIST_STRING = "/api/list-properties/"
    private static let ADD_NEW_LIST_STRING = "/list/"
    private static let GET_CURRENT_USER_INFO = "/user/get-current-user-info/"
    private static let TEAM_STRING = "/team/"

    private static let SCOUT_STRING = "/scout/?limit=10&offset=0"
    private static let ADD_SCOUT =  "/scout/"
    private static let ADD_TEAM =  "/team/"
    
    private static let ADD_PROPERTY_STRING =  "/property/"
    private static let GET_PROPERTY_NOTES_STRING =  "/note/?limit=10&offset=0"
    private static let COUPON_STRING =  "/upgrade-profile/add-coupon/"

    
    
    
    class func getPrivacyPolicyURL() -> String{
        return PRIVACY_POLICY_URL
    }
    
    class func getTermsandServicesURL() -> String{
        return TERMS_SERVICES_URL
    }
    class func getAllNotesURL() -> String{
        return DEV_BASE_URL + API_STRING + GET_PROPERTY_NOTES_STRING
    }
    
    class func getAddPropertyURL() -> String{
        return DEV_BASE_URL + API_STRING + ADD_PROPERTY_STRING
    }
    
    class func getAddTeamURL() -> String{
        return DEV_BASE_URL + API_STRING + ADD_TEAM
    }
    
    class func getAddScoutURL() -> String{
        return DEV_BASE_URL + API_STRING + ADD_SCOUT
    }
    
    class func getScoutURL() -> String{
        return DEV_BASE_URL + API_STRING + SCOUT_STRING
    }
    
    class func getTeamListURL() -> String{
        return DEV_BASE_URL + API_STRING + TEAM_STRING
    }
    
    class func getCurrentUserInfoURL() -> String{
        return DEV_BASE_URL + API_STRING + GET_CURRENT_USER_INFO
    }
    class func getAddNewListURL() -> String{
        return DEV_BASE_URL + API_STRING +  ADD_NEW_LIST_STRING
    }
    
    class func getAllMyListsURL() -> String{
        return DEV_BASE_URL + API_STRING + LIST_STRING
    }
    
    class func AddPropertyToListURL() -> String{
        return DEV_BASE_URL + ADD_TO_LIST_STRING
    }
    
    class func baseURL()->String {
        return DEV_BASE_URL
    }
    
    class func getClientID()->String {
        return CLIENT_ID_STRING
    }
    
    class func getClientSecret()->String {
        return CLIENT_SECRET_STRING
    }
    
    class func loginURL()->String {
        return DEV_BASE_URL + LOGIN_STRING
    }
    
    class func socialLoginVerifyURL()->String {
        return DEV_BASE_URL + SOCIAL_LOGIN_VERIFY_STRING
    }
    
    class func signUpURL()->String {
        print(DEV_BASE_URL + SIGN_UP_STRING)
        return DEV_BASE_URL + SIGN_UP_STRING
    }
    
    class func logoutURL()->String {
        return DEV_BASE_URL + LOGOUT_STRING
    }
    
    class func apiString()->String {
        return API_STRING
    }
    
    
    class func applyCouponURL()->String {
        return DEV_BASE_URL + API_STRING + COUPON_STRING
    }
    
}
