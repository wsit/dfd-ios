//
//  Constants.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 5/22/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//


import Foundation

struct K {
    struct ProductionServer {
        static let baseURL = "172.18.1.11:3001/api/v1"
    }
    
    struct APIParameterKey {
        
        static let username = "username"
        
        static let password = "password"
        static let email = "email"
        static let phone = "phone_number"
        static let firstName = "first_name"
        static let lastName = "last_name"
        
        static let grantType = "grant_type"
        static let clientID = "client_id"
        static let clientSecret = "client_secret"
        
        static let refreshToken = "refresh_token"
        
        //parameters for add to list
        static let propertyAddress = "property_address"
        static let latitude = "latitude"
        static let longitude = "longitude"
        static let list = "list"
        static let property = "property"
        
        //parameters for add new list
        static let name = "name"
        static let managerID = "manager_id"
        static let user = "user"
        static let scoutID = "id"
        static let memberID = "id"
        static let coupon = "coupon"
        
        
        
        
        //add property
        
    }
}

enum HTTPHeaderField: String {
    case authentication = "Authorization"
    case contentType = "Content-Type"
    case acceptType = "Accept"
    case acceptEncoding = "Accept-Encoding"
}

enum ContentType: String {
    case json = "application/json"
}
