//
//  APIRouter.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 5/22/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import Foundation
import Alamofire

enum APIRouter: URLRequestConvertible {
    
    case signup (email:String, password: String, phone: String, firstName: String, lastName :String)
    case login(grantType: String, email:String, password:String, clientID:String, clientSecret: String)
    case articles
    case article(id: Int)
    
    // MARK: - HTTPMethod
    private var method: HTTPMethod {
        switch self {
        
        case .signup:
            return .post
        case .login:
            return .post
        case .articles, .article:
            return .get
        }
    }
    
    // MARK: - Path
    private var path: String {
        switch self {
        case .signup:
            return "/public/user/register"
        case .login:
            return "/oauth/token"
        case .articles:
            return "/articles/all.json"
        case .article(let id):
            return "/article/\(id)"
        }
    }
    
    // MARK: - Parameters
    private var parameters: Parameters? {
        switch self {
        
        case .signup(let email, let password, let phone, let firstName, let lastName):
            return [K.APIParameterKey.email: email, K.APIParameterKey.password: password, K.APIParameterKey.phone: phone, K.APIParameterKey.firstName: firstName, K.APIParameterKey.lastName: lastName ]
        
        case .login(let grantType, let email, let password, let clientID, let clientSecret):
            return [K.APIParameterKey.grantType: grantType,K.APIParameterKey.email: email, K.APIParameterKey.password: password, K.APIParameterKey.clientID: clientID, K.APIParameterKey.clientSecret: clientSecret,]
        
        case .articles, .article:
            return nil
        }
    }
    
    // MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        let url = try K.ProductionServer.baseURL.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        
        // HTTP Method
        urlRequest.httpMethod = method.rawValue
        
        // Common Headers
        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.acceptType.rawValue)
        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.contentType.rawValue)
        
        // Parameters
        if let parameters = parameters {
            do {
                urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
            } catch {
                throw AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
            }
        }
        
        return urlRequest
    }
}

