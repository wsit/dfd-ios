//
//  APIClient.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 5/22/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import Foundation
import Alamofire

class APIClient {
  //  @discardableResult
//    private static func performRequest<T:Decodable>(route:APIRouter, decoder: JSONDecoder = JSONDecoder(), completion:@escaping (Result<T, Error>)->Void) -> DataRequest {
//        return AF.request(route)
//            .responseDecodable (decoder: decoder){ (response: DataResponse<T>) in
//                completion(response.result)
//        }
//    }
//    
//    static func login(grantType: String, email: String, password: String, clientID:String, clientSecret:String, completion:@escaping (Result<User, Error>)->Void) {
//        performRequest(route: APIRouter.login(grantType: grantType, email: email, password: password, clientID: clientID, clientSecret: clientSecret), completion: completion)
//    }
//    
//    static func signUp(email:String, password: String, phone: String, firstName: String, lastName :String, completion:@escaping (Result<RegisteredUserResponse, Error>)->Void) {
//        
//        performRequest(route: APIRouter.signup(email: email, password: password, phone: phone, firstName: firstName, lastName: lastName), completion: completion)
//    }
//
//    static func getArticles(completion:@escaping (Result<[Article]>)->Void) {
//        let jsonDecoder = JSONDecoder()
//        jsonDecoder.dateDecodingStrategy = .formatted(.articleDateFormatter)
//        performRequest(route: APIRouter.articles, decoder: jsonDecoder, completion: completion)
//    }
}
