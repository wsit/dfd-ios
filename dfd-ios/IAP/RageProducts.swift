//
//  RageProducts.swift
//  HouZes
//
//  Created by Mahadhi Hassan Chowdhury on 4/3/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//

import Foundation

public struct RageProducts {
    
    //public static let GirlfriendOfDrummerRage = "com.razeware.rageswift3.GirlfriendOfDrummerRage"
    
    public static let AddWallet1 = "com.ra.houzes_ios.AddWallet1"
    public static let AddWallet2 = "com.ra.houzes_ios.AddWallet2"
    public static let AddWallet3 = "com.ra.houzes_ios.AddWallet3"
    public static let AddWallet4 = "com.ra.houzes_ios.AddWallet4"
    public static let subscriptionSolo = "com.ra.houzes_ios.SubscriptionSolo"
    public static let subscriptionTeam = "com.ra.houzes_ios.SubscriptionTeam"
    
    
    fileprivate static let productIdentifiers: Set<ProductIdentifier> =
        [/*RageProducts.GirlfriendOfDrummerRage,*/
            RageProducts.AddWallet1]
    
    public static let store = IAPHelper(productIds: RageProducts.productIdentifiers)
    
    public static func isProductPurchased(_ productIdentifier: ProductIdentifier) -> Bool {
        if (productIdentifier == RageProducts.AddWallet1) {
            return false
        } else {
            return RageProducts.store.isProductPurchased(productIdentifier)
        }
    }
    
}

func resourceNameForProductIdentifier(_ productIdentifier: String) -> String? {
    return productIdentifier.components(separatedBy: ".").last
}
