//
//  RechargeForIAPViewController.swift
//  HouZes
//
//  Created by Mahadhi Hassan Chowdhury on 4/3/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//

import UIKit
import StoreKit
import Alamofire
import SwiftyJSON


class RechargeForIAPViewController: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var lblFirstTitle: UILabel!
    @IBOutlet weak var lblFirstSubTitle: UILabel!
    @IBOutlet weak var btnFirst: UIButton!
    
    @IBOutlet weak var secondView: UIView!
    @IBOutlet weak var lblSecondTitle: UILabel!
    @IBOutlet weak var lblSecondSubTitle: UILabel!
    @IBOutlet weak var btnSecond: UIButton!
    
    @IBOutlet weak var thirdView: UIView!
    @IBOutlet weak var lblThirdTitle: UILabel!
    @IBOutlet weak var lblThirdSubTitle: UILabel!
    @IBOutlet weak var btnThird: UIButton!
    
    @IBOutlet weak var fourthView: UIView!
    @IBOutlet weak var lblFourthTitle: UILabel!
    @IBOutlet weak var lblFourthSubTitle: UILabel!
    @IBOutlet weak var btnFourth: UIButton!
    
    
    var selectedProductIdentifier : String?
    var productIDs: [String] = [RageProducts.AddWallet1, RageProducts.AddWallet2, RageProducts.AddWallet3, RageProducts.AddWallet4]
    var productsArray: [SKProduct] = []
    var parentString : String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().restoreCompletedTransactions()
        //        PKIAPHandler.shared.setProductIds(ids: self.productIDs)
        //        PKIAPHandler.shared.fetchAvailableProducts { (products) in
        //
        //            self.productsArray = products
        //            print(self.productsArray)
        //
        //        }
        lblTitle.textColor  = UIColor(red:0.55, green:0.67, blue:0.72, alpha:1)
        firstView.backgroundColor = UIColor(red:0.93, green:0.96, blue:0.7, alpha:1)
        
        //FontAndColorConfigs.setDynamicLabelFontSize(labels: [lblFirstTitle], type: TextType.MainTitle.rawValue)
        FontAndColorConfigs.setDynamicLabelFontSize(labels: [lblFirstSubTitle, lblSecondSubTitle, lblThirdSubTitle,lblFourthSubTitle], type: TextType.ListSubTitle.rawValue)
        //FontAndColorConfigs.setDynamicButtonsFontSize(buttons: [], type: TextType.ListTitle.rawValue)
        
        fourthView.backgroundColor = UIColor(red:0.32, green:0.77, blue:0.94, alpha:1)
        btnFourth.backgroundColor = .white
        btnFourth.setTitleColor(UIColor(red:0.27, green:0.59, blue:0.71, alpha:1), for: .normal)
        lblFourthTitle.textColor = UIColor(red:0.06, green:0.34, blue:0.44, alpha:1)
        lblFourthSubTitle.textColor = UIColor(red:0.27, green:0.6, blue:0.71, alpha:1)
        
        thirdView.backgroundColor = UIColor(red:0.33, green:0.87, blue:0.88, alpha:1)
        btnThird.backgroundColor = .white
        btnThird.setTitleColor(UIColor(red:0.27, green:0.71, blue:0.71, alpha:1), for: .normal)
        lblThirdTitle.textColor = UIColor(red:0.06, green:0.44, blue:0.44, alpha:1)
        lblThirdSubTitle.textColor = UIColor(red:0.27, green:0.71, blue:0.71, alpha:1)
        
        secondView.backgroundColor = UIColor(red:0.43, green:0.93, blue:0.73, alpha:1)
        btnSecond.backgroundColor = .white
        btnSecond.setTitleColor(UIColor(red:0.27, green:0.71, blue:0.54, alpha:1), for: .normal)
        lblSecondTitle.textColor = UIColor(red:0.06, green:0.44, blue:0.29, alpha:1)
        lblSecondSubTitle.textColor = UIColor(red:0.27, green:0.71, blue:0.54, alpha:1)
        
        btnFirst.backgroundColor = .white
        btnFirst.setTitleColor(UIColor(red:0.67, green:0.71, blue:0.27, alpha:1), for: .normal)
        lblFirstTitle.textColor = UIColor(red:0.4, green:0.44, blue:0.06, alpha:1)
        lblFirstSubTitle.textColor = UIColor(red:0.67, green:0.71, blue:0.27, alpha:1)
        
    }
    
    @IBAction func backClicked(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func firstButtonClicked(_ sender: Any) {
        
        print("About to fetch the products");
        // We check that we are allow to make the purchase.
        if (SKPaymentQueue.canMakePayments())
        {
            let productID:NSSet = NSSet(object: RageProducts.AddWallet1)
            self.selectedProductIdentifier = RageProducts.AddWallet1
            let productsRequest:SKProductsRequest = SKProductsRequest(productIdentifiers: productID as! Set<String>);
            productsRequest.delegate = self;
            productsRequest.start();
            print("Fething Products");
        }else{
            print("can't make purchases");
        }
        //        for sproduct in self.productsArray{
        //
        //            if  sproduct.productIdentifier == RageProducts.AddWallet1{
        //                PKIAPHandler.shared.purchase(product: sproduct) { (alert, product, transaction) in
        //                    if let tran = transaction, let prod = product {
        //                        //use transaction details and purchased product as you want
        //                        print(tran.transactionIdentifier)
        //                        print(prod.productIdentifier)
        //                        print(alert.message)
        //                    }
        //
        //                }
        //            }
        //        }
        
    }
    
    @IBAction func secondButtonClicked(_ sender: Any) {
        print("About to fetch the products");
        // We check that we are allow to make the purchase.
        if (SKPaymentQueue.canMakePayments())
        {
            let productID:NSSet = NSSet(object: RageProducts.AddWallet2)
            self.selectedProductIdentifier = RageProducts.AddWallet2
            let productsRequest:SKProductsRequest = SKProductsRequest(productIdentifiers: productID as! Set<String>);
            productsRequest.delegate = self;
            productsRequest.start();
            print("Fething Products");
        }else{
            print("can't make purchases");
        }
        //        for sproduct in self.productsArray{
        //
        //            if  sproduct.productIdentifier == RageProducts.AddWallet2{
        //                PKIAPHandler.shared.purchase(product: sproduct) { (alert, product, transaction) in
        //
        //                    if let tran = transaction, let prod = product {
        //                        //use transaction details and purchased product as you want
        //                        print(tran.transactionIdentifier)
        //                        print(prod.productIdentifier)
        //                        print(alert.message)
        //                    }
        //                }
        //            }
        //        }
        
    }
    
    
    @IBAction func thirdButtonClicked(_ sender: Any) {
        print("About to fetch the products");
        // We check that we are allow to make the purchase.
        if (SKPaymentQueue.canMakePayments())
        {
            let productID:NSSet = NSSet(object: RageProducts.AddWallet3)
            self.selectedProductIdentifier = RageProducts.AddWallet3
            let productsRequest:SKProductsRequest = SKProductsRequest(productIdentifiers: productID as! Set<String>);
            productsRequest.delegate = self;
            productsRequest.start();
            print("Fething Products");
        }else{
            print("can't make purchases");
        }
        //        for sproduct in self.productsArray{
        //
        //            if  sproduct.productIdentifier == RageProducts.AddWallet3{
        //                PKIAPHandler.shared.purchase(product: sproduct) { (alert, product, transaction) in
        //                    if let tran = transaction, let prod = product {
        //                        //use transaction details and purchased product as you want
        //                        print(tran.transactionIdentifier)
        //                        print(prod.productIdentifier)
        //                        print(alert.message)
        //                    }
        //                    //Globals.shared.showWarnigMessage(alert.message)
        //                }
        //            }
        //        }
    }
    
    @IBAction func fourthButtonClicked(_ sender: Any) {
        print("About to fetch the products");
        // We check that we are allow to make the purchase.
        if (SKPaymentQueue.canMakePayments())
        {
            let productID:NSSet = NSSet(object: RageProducts.AddWallet4)
            self.selectedProductIdentifier = RageProducts.AddWallet4
            let productsRequest:SKProductsRequest = SKProductsRequest(productIdentifiers: productID as! Set<String>);
            productsRequest.delegate = self;
            productsRequest.start();
            print("Fething Products");
        }else{
            print("can't make purchases");
        }
        //        for sproduct in self.productsArray{
        //
        //            if  sproduct.productIdentifier == RageProducts.AddWallet4{
        //                PKIAPHandler.shared.purchase(product: sproduct) { (alert, product, transaction) in
        //                    if let tran = transaction, let prod = product {
        //                        //use transaction details and purchased product as you want
        //                        print(tran.transactionIdentifier)
        //                        print(prod.productIdentifier)
        //                        print(alert.message)
        //                    }
        //
        //                }
        //            }
        //        }
    }
    
}

extension RechargeForIAPViewController: SKProductsRequestDelegate{
    
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        print("got the request from Apple")
        let count : Int = response.products.count
        if (count>0) {
            let validProducts = response.products
            
            //let validProduct: SKProduct = response.products[0] as SKProduct
            for validProduct in validProducts{
                if (validProduct.productIdentifier == self.selectedProductIdentifier) {
                    print(validProduct.localizedTitle)
                    print(validProduct.localizedDescription)
                    print(validProduct.price)
                    buyProduct(product: validProduct);
                    
                } else {
                    print(validProduct.productIdentifier)
                }
            }
            
        } else {
            print("nothing")
        }
        
    }
    
    func request(_ request: SKRequest, didFailWithError error: Error) {
        
        print("Error Fetching product information");
    }
    
    // Helper Methods
    
    func buyProduct(product: SKProduct){
        print("Sending the Payment Request to Apple");
        let payment = SKPayment(product: product)
        SKPaymentQueue.default().add(payment);
        
    }
    
    
}

extension RechargeForIAPViewController: SKPaymentTransactionObserver{
    
    //If an error occurs, the code will go to this function
    
    func sendDataToServer(transaction: SKPaymentTransaction) {
        print(transaction.payment.productIdentifier)
        var price :Double = 0.0
        if transaction.payment.productIdentifier == RageProducts.AddWallet1{
            price = 10.99
            handleSendData(price: price, view: self.btnFirst)
            
        }else if transaction.payment.productIdentifier == RageProducts.AddWallet2{
            price = 15.99
            handleSendData(price: price, view: self.btnSecond)

        }else if transaction.payment.productIdentifier == RageProducts.AddWallet3{
            price = 20.99
            handleSendData(price: price, view: self.btnThird)

        }else if transaction.payment.productIdentifier == RageProducts.AddWallet4{
            price = 25.99
            handleSendData(price: price, view: self.btnFourth)

        }else{
            
        }
       
    }
    
    func handleSendData(price: Double, view: UIButton) {
        self.showSpinner(onView:view )
                var urlString = UrlManager.baseURL() + UrlManager.apiString() + "/apple-payment-gateway/"
                var parameters: Parameters = [
                    "amount" : price
                ]
                let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
                print("headers", headers)
                print("urlString", urlString)
                print("parameters", parameters)
                
                
        Alamofire.request(urlString, method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
                    switch(response.result) {
                    case.success(let data):
                        print("success",data)
                        self.removeSpinner()
                        //self.activityStopAnimating()
                        let statusCode = response.response?.statusCode
                        print(statusCode!)
                        let rechargeCardResponse = try? JSON(data: response.data!)
                        let status = rechargeCardResponse?["status"].boolValue
                        let message = rechargeCardResponse?["message"].stringValue
                        
                        if statusCode == 200{
                            if status == true{
                                self.dismiss(animated: true, completion: nil)
                                //
                                let url : String = UrlManager.getCurrentUserInfoURL()
                                self.getCurrentUserInfo(urlString: url)
                            }else{
                                AppConfigs.showSnacbar(message: message ?? "", textColor: .green)
                            }
        //                    if rechargeCardResponse?.status == true{
        //
        //                        DispatchQueue.main.async {
        //                            self.removeSpinner()
        //                            //AppConfigs.showSnacbar(message: (rechargeCardResponse?.message)!, textColor: .green)
        //
        //                            let alertController = UIAlertController(title: (rechargeCardResponse?.message)!, message:"", preferredStyle: .alert)
        //                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
        //
        //                                //                                    let url : String = UrlManager.getCurrentUserInfoURL()
        //                                //                                    self.getCurrentUserInfo(urlString: url)
        //                                if self.parentString == "Plan"{
        //                                    let url : String = UrlManager.getCurrentUserInfoURL()
        //                                    self.getCurrentUserInfo(urlString: url)
        //                                }else{
        //
        //                                    self.dismiss(animated: true, completion: nil)
        //
        //                                    let url : String = UrlManager.getCurrentUserInfoURL()
        //                                    self.getCurrentUserInfo(urlString: url)
        //
        //                                }
        //                            }
        //                            alertController.addAction(OKAction)
        //                            self.present(alertController, animated: true, completion: nil)
        //
        //                        }
        //                    }else{
        //                       // AppConfigs.showSnacbar(message: (rechargeCardResponse?.message)!, textColor: .green)
        //
        //                    }
                        }
                        else if statusCode == 401{
                            self.removeSpinner()
                            let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                            print("error with response status: \(String(describing: statusCode))")
                            //AppConfigs.showSnacbar(message: (rechargeCardResponse?.message)!, textColor: .green)
                        }
                        else{
                            self.removeSpinner()
                            print("error with response status: \(String(describing: statusCode))")
                            //AppConfigs.showSnacbar(message: (rechargeCardResponse?.message)!, textColor: .green)
                        }
                    case.failure(let error):
                        print("Not Success",error)
                        let statusCode = response.response?.statusCode
                        self.showMessageForServerError(code: statusCode ?? 500)
                        
                        //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                        self.removeSpinner()
                        self.removeSpinner()
                    }
                    self.removeSpinner()
                }
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error) {
        
        print(error.localizedDescription)
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        
        print("Received Payment Transaction Response from Apple");
        
        for transaction:AnyObject in transactions {
            if let trans:SKPaymentTransaction = transaction as? SKPaymentTransaction{
                switch trans.transactionState {
                    
                case .purchasing:
                    print("Purchasing state");
                case .purchased:
                    print("Product Purchased");
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    //trans.
                    self.sendDataToServer(transaction: transaction as! SKPaymentTransaction)
                    
                    break;
                case .failed:
                    print("Purchased Failed");
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    self.showAlert(message: trans.error?.localizedDescription ?? "Something went wrong")
                    break;
                case .restored:
                    print("Product Restored");
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                     self.sendDataToServer(transaction: transaction as! SKPaymentTransaction)
                    // SKPaymentQueue.default().restoreCompletedTransactions()
                    break
                //[self restoreTransaction:transaction];
                default:
                    break;
                }
            }
        }
        
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, removedTransactions transactions: [SKPaymentTransaction]) {
        
        print("The Payment was removedTransactions!")
       // SKPaymentQueue.default().remove(self)
    }
    
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        
        print("The Payment was successfull!")
        
    }
    
    
    func getCurrentUserInfo(urlString:String) {
        print(urlString)
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success",data)
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    
                    DispatchQueue.main.async {
                        
                        let currentUser = try? JSONDecoder().decode(CurrentUserInfoResponse.self, from: response.data!)
                        AppConfigs.saveCurrentUserInfo(user: currentUser!)
                        print(AppConfigs.getCurrentUserInfo().email!)
                        
                        if self.parentString == "Plan"{

                            let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
                            guard let vc = storyBoard.instantiateViewController(withIdentifier: "SMNavigationController") as? SMNavigationController  else
                            { return  }
                            vc.modalPresentationStyle = .fullScreen
                            self.present(vc, animated: true, completion: nil)
                            
                        }else{
                            print(self.parentString)
                        }
                    }
                }
                    
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                
                //AppConfigs.showSnacbar(message: error.localizedDescription , textColor: .red)
                self.removeSpinner()
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
    
}
