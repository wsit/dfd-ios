//
//  RealmManager.swift
//  HouZes
//
//  Created by Mahadhi Hassan Chowdhury on 2/9/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//

import Foundation
import RealmSwift


class RCoordinate: Object {
    
    @objc dynamic var lat = 0.0
    @objc dynamic var long = 0.0
    
    convenience init(lat: Double, long:Double) {
        self.init()
        self.lat = lat
        self.long = long
    }
}

class RMarkerCoordinate: Object {
    
    @objc dynamic var lat = 0.0
    @objc dynamic var long = 0.0
    
    convenience init(lat: Double, long:Double) {
        self.init()
        self.lat = lat
        self.long = long
    }
}


class RPolyline: NSObject {
    
     let coordinates = List<RCoordinate>()
    
    
    func addNewSpecimen(lastLocation: CLLocation) {
      let realm = try! Realm() // 1
        
      try! realm.write { // 2
        
        let coordinate = RCoordinate() // 3
          
        coordinate.lat = lastLocation.coordinate.latitude // 4
        coordinate.long = lastLocation.coordinate.longitude
          
        realm.add(coordinate) // 5
        //coordinate = coordinate // 6
      }
    }
}

