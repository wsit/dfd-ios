//
//  RealmService.swift
//  HouZes
//
//  Created by Mahadhi Hassan Chowdhury on 2/11/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//

import Foundation
import RealmSwift


class RealmPolylineService {
    
    private init() {}
    static let shared = RealmPolylineService()
    
    var realmPolyline = try! Realm()
    
    func create<T: Object>(_ object: T) {
        do {
            try realmPolyline.write {
                realmPolyline.add(object)
            }
        } catch {
            post(error)
        }
    }
    
    func update<T: Object>(_ object: T, with dictionary: [String: Any?]) {
        do {
            try realmPolyline.write {
                for (key, value) in dictionary {
                    object.setValue(value, forKey: key)
                }
            }
        } catch {
            post(error)
        }
    }
    
    func delete<T: Object>(_ object: T) {
        do {
            try realmPolyline.write {
                realmPolyline.delete(object)
            }
        } catch {
            post(error)
        }
    }
    
    func post(_ error: Error) {
        NotificationCenter.default.post(name: NSNotification.Name("RealmError"), object: error)
    }
    
    func observeRealmErrors(in vc: UIViewController, completion: @escaping (Error?) -> Void) {
        NotificationCenter.default.addObserver(forName: NSNotification.Name("RealmError"),
                                               object: nil,
                                               queue: nil) { (notification) in
                                                completion(notification.object as? Error)
        }
    }
    
    func stopObservingErrors(in vc: UIViewController) {
        NotificationCenter.default.removeObserver(vc, name: NSNotification.Name("RealmError"), object: nil)
    }
    
}
