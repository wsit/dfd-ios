//
//  SMNavigationController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 5/2/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit
import Foundation
import LNSideMenu
import AuthenticationServices


class SMNavigationController: LNSideMenuNavigationController {
    
    fileprivate var items:[String]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        // Using default side menu
        // items = ["Home", "Story", "Team", "Visited Place", "Profile"]
        //    initialSideMenu(.left)
        // Custom side menu
        initialCustomMenu(pos: .left)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    fileprivate func initialSideMenu(_ position: Position) {
        menu = LNSideMenu(sourceView: view, menuPosition: .left, items: items!)
        menu?.menuViewController?.menuBgColor = UIColor.black.withAlphaComponent(0.85)
        menu?.delegate = self
        menu?.underNavigationBar = true
        view.bringSubviewToFront(navigationBar)
    }
    
    fileprivate func initialCustomMenu(pos position: Position) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LeftMenuViewController") as! LeftMenuViewController
        vc.delegate = self
        menu = LNSideMenu(navigation: self, menuPosition: .left, customSideMenu: vc, size: .custom(UIScreen.main.bounds.width - 55))
        menu?.delegate = self
        menu?.enableDynamic = true
        // Moving down the menu view under navigation bar
        //    menu?.underNavigationBar = true
    }
    
    fileprivate func setContentVC(_ index: Int) {
        print("Did select item at index: \(index)")
        
        var nViewController: UIViewController? = nil
        
        switch index {
            
        case 0:
            print("Index----->", index)
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            nViewController =  storyBoard.instantiateViewController(withIdentifier: "HomeViewController")
            break
            
        case 1:
            print("Index----->", index)
            let storyBoard = UIStoryboard(name: "LoadHouzes", bundle: nil)
            nViewController =  storyBoard.instantiateViewController(withIdentifier: "LoadHouzesViewController")
            break
            
        case 2:
            print("Index----->", index)
            let storyBoard = UIStoryboard(name: "AddProperty", bundle: nil)
            nViewController =  storyBoard.instantiateViewController(withIdentifier: "AddPropertyViewController")
            break
            
        case 3:
            print("Index----->", index)
            let storyBoard = UIStoryboard(name: "MyStory", bundle: nil)
            nViewController =  storyBoard.instantiateViewController(withIdentifier: "StoryViewController")
            
            
            break
            
            //        case 2:
            //
            //            break
            
        case 4:
            print("Index----->", index)
            let transition = CATransition()
            transition.duration = 0.5
            transition.type = CATransitionType.push
            transition.subtype = CATransitionSubtype.fromRight
            transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
            view.window!.layer.add(transition, forKey: kCATransition)
            let storyBoard = UIStoryboard(name: "MyTeam", bundle: nil)
            nViewController =  storyBoard.instantiateViewController(withIdentifier: "TeamViewController")
            break
            
            //        case 5:
            //            print("Index----->", index)
            //            let storyBoard = UIStoryboard(name: "VisitedPlaces", bundle: nil)
            //            nViewController =  storyBoard.instantiateViewController(withIdentifier: "VisitedPlacesViewController")
            //
            //            break
            
        case 5:
            print("Index----->", index)
            let transition = CATransition()
            transition.duration = 0.5
            transition.type = CATransitionType.push
            transition.subtype = CATransitionSubtype.fromRight
            transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
            view.window!.layer.add(transition, forKey: kCATransition)
            
            let storyBoard = UIStoryboard(name: "MyProfile", bundle: nil)
            nViewController =  storyBoard.instantiateViewController(withIdentifier: "ProfileViewController")
            
            break
            
        case 6:
            
            print("Index----->", index)
            let storyBoard = UIStoryboard(name: "Settings", bundle: nil)
            nViewController =  storyBoard.instantiateViewController(withIdentifier: "SettingsViewController")
            break
            
            //        case 7:
            //            print("Index----->", index)
            //            let storyBoard = UIStoryboard(name: "MyStory", bundle: nil)
            //            nViewController =  storyBoard.instantiateViewController(withIdentifier: "StoryViewController")
            //            break
            
            
        case 1001:
            let settingsSB: UIStoryboard = UIStoryboard(name: "Settings", bundle: nil)
            //let vc = settingsSB.instantiateViewController(withIdentifier: "PackageViewController") as! PackageViewController
            //            vc.modalPresentationStyle = .fullScreen
            //            self.navigationController?.pushViewController(vc, animated: true)
            nViewController =  settingsSB.instantiateViewController(withIdentifier: "PackageViewController")
//            let alertController = UIAlertController(title: "Please go to web to manage your account.", message: "", preferredStyle: .alert)
//            let OKAction = UIAlertAction(title: "Ok", style: .default) { (alert) in
//
//
//            }
//            alertController.addAction(OKAction)
//            self.present(alertController, animated: true, completion: nil)
            
            break
            
            
                           
//            let transition = CATransition()
//            transition.duration = 0.5
//            transition.type = CATransitionType.push
//            transition.subtype = CATransitionSubtype.fromRight
//            transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
//            view.window!.layer.add(transition, forKey: kCATransition)
//
//            let settingsSB: UIStoryboard = UIStoryboard(name: "Settings", bundle: nil)
//            //let vc = settingsSB.instantiateViewController(withIdentifier: "PackageViewController") as! PackageViewController
//            //            vc.modalPresentationStyle = .fullScreen
//            //            self.navigationController?.pushViewController(vc, animated: true)
//            nViewController =  settingsSB.instantiateViewController(withIdentifier: "PackageViewController")
            
            
        default:
            print("in default")
            break
        }
        
        //        if let viewController = viewControllers.first , viewController is NextViewController {
        //            nViewController = storyboard?.instantiateViewController(withIdentifier: "ViewController")
        //        } else {
        //            nViewController = storyboard?.instantiateViewController(withIdentifier: "NextViewController")
        //        }
        if let viewController = nViewController {
            self.setContentViewController(viewController)
        }
        // Test moving up/down the menu view
        if let sm = menu, sm.isCustomMenu {
            menu?.underNavigationBar = false
        }
    }
}

extension SMNavigationController: LNSideMenuDelegate {
    func sideMenuWillOpen() {
        print("sideMenuWillOpen")
    }
    
    func sideMenuWillClose() {
        print("sideMenuWillClose")
    }
    
    func sideMenuDidClose() {
        print("sideMenuDidClose")
    }
    
    func sideMenuDidOpen() {
        print("sideMenuDidOpen")
    }
    
    func didSelectItemAtIndex(_ index: Int) {
        setContentVC(index)
    }
}

extension SMNavigationController: LeftMenuDelegate {
    func didSelectItemAtIndex(index idx: Int) {
        menu?.toggleMenu() { [unowned self] in
            self.setContentVC(idx)
        }
    }
}

