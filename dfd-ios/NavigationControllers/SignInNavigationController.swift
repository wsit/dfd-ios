//
//  SignInNavigationController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 5/13/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit

class SignInNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationBar.barTintColor = .white //UIColor(red:0.04, green:0.09, blue:0.14, alpha:1)
        self.navigationController?.navigationBar.prefersLargeTitles = true

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
