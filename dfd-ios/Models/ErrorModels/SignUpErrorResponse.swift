//
//  SignUpErrorResponse.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 8/3/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

//   let signUpErrorResponse = try? newJSONDecoder().decode(SignUpErrorResponse.self, from: jsonData)

import Foundation

// MARK: - SignUpErrorResponse
class SignUpErrorResponse: Codable {
    let email: [String]?
    
    init(email: [String]?) {
        self.email = email
    }
}
