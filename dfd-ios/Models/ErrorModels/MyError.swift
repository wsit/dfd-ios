//
//  MyError.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 8/7/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

//   let myError = try? newJSONDecoder().decode(MyError.self, from: jsonData)

import Foundation

// MARK: - MyError
class MyError: Codable {
    let detail: String?
    
    init(detail: String?) {
        self.detail = detail
    }
    
    
}

