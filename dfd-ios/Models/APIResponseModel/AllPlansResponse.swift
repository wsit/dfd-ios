//
//  AllPlansResponse.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 1/13/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//
// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let allPlansResponse = try? newJSONDecoder().decode(AllPlansResponse.self, from: jsonData)

import Foundation

// MARK: - AllPlansResponseElement
class AllPlansResponseElement: Codable {
    
    let id: Int?
       let planName: String?
    let planCost, planCoin: Double?
    let createdBy: Int?
       let updatedBy: Int?
       let rechargeCardResponseDescription, image, createdAt, updatedAt: String?

       enum CodingKeys: String, CodingKey {
           case id
           case planName = "plan_name"
           case planCost = "plan_cost"
           case planCoin = "plan_coin"
           case createdBy = "created_by"
           case updatedBy = "updated_by"
           case rechargeCardResponseDescription = "description"
           case image
           case createdAt = "created_at"
           case updatedAt = "updated_at"
       }

       init(id: Int?, planName: String?, planCost: Double?, planCoin: Double?, createdBy: Int?, updatedBy: Int?, rechargeCardResponseDescription: String?, image: String?, createdAt: String?, updatedAt: String?) {
           self.id = id
           self.planName = planName
           self.planCost = planCost
           self.planCoin = planCoin
           self.createdBy = createdBy
           self.updatedBy = updatedBy
           self.rechargeCardResponseDescription = rechargeCardResponseDescription
           self.image = image
           self.createdAt = createdAt
           self.updatedAt = updatedAt
       }
    
//    let id: Int?
//    let planName: String?
//    let planCost, planCoin: Double?
//    let  createdBy: Int?
//    let updatedBy: String?
//    let createdAt, updatedAt: String?
//
//    enum CodingKeys: String, CodingKey {
//        case id
//        case planName = "plan_name"
//        case planCost = "plan_cost"
//        case planCoin = "plan_coin"
//        case createdBy = "created_by"
//        case updatedBy = "updated_by"
//        case createdAt = "created_at"
//        case updatedAt = "updated_at"
//    }
//
//    init(id: Int?, planName: String?, planCost: Double?, planCoin: Double?, createdBy: Int?, updatedBy: String?, createdAt: String?, updatedAt: String?) {
//        self.id = id
//        self.planName = planName
//        self.planCost = planCost
//        self.planCoin = planCoin
//        self.createdBy = createdBy
//        self.updatedBy = updatedBy
//        self.createdAt = createdAt
//        self.updatedAt = updatedAt
//    }
}

typealias AllPlansResponse = [AllPlansResponseElement]
