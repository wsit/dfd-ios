//
//  PropertyDetailsResponse.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 12/19/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import Foundation
//   let propertyDetailsResponse = try? newJSONDecoder().decode(PropertyDetailsResponse.self, from: jsonData)

import Foundation

// MARK: - PropertyDetailsResponse
class PropertyDetailsResponse: Codable {
    let id, userList: Int?
    let street, city, state, zip: String?
    let cadAcct: String?
    let gmaTag: Int?
    let latitude, longitude: Double?
    let propertyTags: [PropertyTag]?
    let ownerInfo: [OwnerInfo]?
    let photos: [PropertyPhoto]?
    let notes: [Note]?
    let history: Int?
    let createdAt, updatedAt: String?
    let powerTraceRequestID: Int?
    let userListDetails: UserListDetails?

    enum CodingKeys: String, CodingKey {
        case id
        case userList = "user_list"
        case street, city, state, zip
        case cadAcct = "cad_acct"
        case gmaTag = "gma_tag"
        case latitude, longitude
        case propertyTags = "property_tags"
        case ownerInfo = "owner_info"
        case photos, notes, history
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case powerTraceRequestID = "power_trace_request_id"
        case userListDetails = "user_list_details"
    }

    init(id: Int?, userList: Int?, street: String?, city: String?, state: String?, zip: String?, cadAcct: String?, gmaTag: Int?, latitude: Double?, longitude: Double?, propertyTags: [PropertyTag]?, ownerInfo: [OwnerInfo]?, photos: [PropertyPhoto]?, notes: [Note]?, history: Int?, createdAt: String?, updatedAt: String?, powerTraceRequestID: Int?, userListDetails: UserListDetails?) {
        self.id = id
        self.userList = userList
        self.street = street
        self.city = city
        self.state = state
        self.zip = zip
        self.cadAcct = cadAcct
        self.gmaTag = gmaTag
        self.latitude = latitude
        self.longitude = longitude
        self.propertyTags = propertyTags
        self.ownerInfo = ownerInfo
        self.photos = photos
        self.notes = notes
        self.history = history
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.powerTraceRequestID = powerTraceRequestID
        self.userListDetails = userListDetails
    }
}

// MARK: - UserListDetails
class UserListDetails: Codable {
    let id: Int?
    let name: String?
    let user: Int?
    let createdAt, updatedAt: String?
    let leadsCount: Int?
    let fetchLatLong: Bool?

    enum CodingKeys: String, CodingKey {
        case id, name, user
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case leadsCount = "leads_count"
        case fetchLatLong = "fetch_lat_lng"
    }

    init(id: Int?, name: String?, user: Int?, createdAt: String?, updatedAt: String?, leadsCount: Int?,fetchLatLong: Bool?) {
        self.id = id
        self.name = name
        self.user = user
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.leadsCount = leadsCount
        self.fetchLatLong = fetchLatLong

    }
}

// MARK: - Note
//class Note: Codable {
//    let id: Int?
//    let title, notes, createdAt, updatedAt: String?
//    let user: String?
//    let property: Int?
//
//    init(id: Int?, title: String?, notes: String?, createdAt: String?, updatedAt: String?, user: String?, property: Int?) {
//        self.id = id
//        self.title = title
//        self.notes = notes
//        self.createdAt = createdAt
//        self.updatedAt = updatedAt
//        self.user = user
//        self.property = property
//    }
//}

// MARK: - OwnerInfo
class OwnerInfo: Codable {
    let fullName,fullAddress: String?
//    let estate, fullName, companyName, fullAddress, otherInfo: String?

//    let formattedName: FormattedName?
//    let formattedAddress: FormattedAddress?

    enum CodingKeys: String, CodingKey {
        //case estate
        case fullName = "full_name"
//        case companyName = "company_name"
        case fullAddress = "full_address"
//        case otherInfo = "other_info"
//        case formattedName = "formatted_name"
//        case formattedAddress = "formatted_address"
    }

    init(fullName: String?, fullAddress: String?){
        self.fullName = fullName
        self.fullAddress = fullAddress

    }
//
//    init(estate: String?, fullName: String?, companyName: String?, fullAddress: String?, otherInfo: String?, formattedName: FormattedName?, formattedAddress: FormattedAddress?) {
//        self.estate = estate
//        self.fullName = fullName
//        self.companyName = companyName
//        self.fullAddress = fullAddress
//        self.otherInfo = otherInfo
//        self.formattedName = formattedName
//        self.formattedAddress = formattedAddress
//    }
}

// MARK: - FormattedAddress
class FormattedAddress: Codable {
    let city, state: String?
    let street: Street?
    let zipCode: String?

    enum CodingKeys: String, CodingKey {
        case city, state, street
        case zipCode = "zip_code"
    }

    init(city: String?, state: String?, street: Street?, zipCode: String?) {
        self.city = city
        self.state = state
        self.street = street
        self.zipCode = zipCode
    }
}

// MARK: - Street
class Street: Codable {
    let streetName: String?
    let streetNumber: Int?
    let formattedFullStreetName: String?

    enum CodingKeys: String, CodingKey {
        case streetName = "street_name"
        case streetNumber = "street_number"
        case formattedFullStreetName = "formatted_full_street_name"
    }

    init(streetName: String?, streetNumber: Int?, formattedFullStreetName: String?) {
        self.streetName = streetName
        self.streetNumber = streetNumber
        self.formattedFullStreetName = formattedFullStreetName
    }
}

// MARK: - FormattedName
class FormattedName: Codable {
    let lastName, firstName, middleName, formattedFullName: String?

    enum CodingKeys: String, CodingKey {
        case lastName = "last_name"
        case firstName = "first_name"
        case middleName = "middle_name"
        case formattedFullName = "formatted_full_name"
    }

    init(lastName: String?, firstName: String?, middleName: String?, formattedFullName: String?) {
        self.lastName = lastName
        self.firstName = firstName
        self.middleName = middleName
        self.formattedFullName = formattedFullName
    }
}

// MARK: - Photo
//class Photo: Codable {
//    let id: Int?
//    let photoURL, createdAt, updatedAt, user: String?
//    let property: Int?
//
//    init(id: Int?, photoURL: String?, createdAt: String?, updatedAt: String?, user: String?, property: Int?) {
//        self.id = id
//        self.photoURL = photoURL
//        self.createdAt = createdAt
//        self.updatedAt = updatedAt
//        self.user = user
//        self.property = property
//    }
//}

// MARK: - PropertyTag
class PropertyTag: Codable {
    let id: Int?
    let name: String?
    let color: TagColor?
    let user: Int?
    let createdAt, updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case id, name, color, user
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }

    init(id: Int?, name: String?, color: TagColor?, user: Int?, createdAt: String?, updatedAt: String?) {
        self.id = id
        self.name = name
        self.color = color
        self.user = user
        self.createdAt = createdAt
        self.updatedAt = updatedAt
    }
}

// MARK: - Color
class TagColor: Codable {
    let colorName, colorCode: String?

    enum CodingKeys: String, CodingKey {
        case colorName = "color_name"
        case colorCode = "color_code"
    }

    init(colorName: String?, colorCode: String?) {
        self.colorName = colorName
        self.colorCode = colorCode
    }
}

