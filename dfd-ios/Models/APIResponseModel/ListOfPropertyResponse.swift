//
//  ListOfPropertyResponse.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 9/17/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

//   let listOfPropertyResponse = try? newJSONDecoder().decode(ListOfPropertyResponse.self, from: jsonData)

import Foundation

// MARK: - ListOfPropertyResponse
class ListOfPropertyResponse: Codable {
    let next, previous: String?
    let count: Int?
    let results: [ListedProperty]?
    
    init(next: String?, previous: String?, count: Int?, results: [ListedProperty]?) {
        self.next = next
        self.previous = previous
        self.count = count
        self.results = results
    }
}

// MARK: - Result
class Property: Codable {
    let id, photoCount, noteCount: Int?
    let propertyAddress, cadAcct: String?
    let latitude, longitude: String?
    let ownerInfo: [OwnerInfo]?
    let createdAt, updatedAt: String?
    let userList: Int?
    let tag,gmaTag: Int?
    let property: Int?
    
    enum CodingKeys: String, CodingKey {
        case id
        case photoCount = "photo_count"
        case noteCount = "note_count"
        case propertyAddress = "property_address"
        case cadAcct = "cad_acct"
        case gmaTag = "gma_tag"
        case latitude, longitude
        case ownerInfo = "owner_info"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case userList = "user_list"
        case tag, property
    }
    
    init(id: Int?, photoCount: Int?, noteCount: Int?, propertyAddress: String?, cadAcct: String?, gmaTag: Int?, latitude: String?, longitude: String?, ownerInfo: [OwnerInfo]?, createdAt: String?, updatedAt: String?, userList: Int?, tag: Int?, property: Int?) {
        self.id = id
        self.photoCount = photoCount
        self.noteCount = noteCount
        self.propertyAddress = propertyAddress
        self.cadAcct = cadAcct
        self.gmaTag = gmaTag
        self.latitude = latitude
        self.longitude = longitude
        self.ownerInfo = ownerInfo
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.userList = userList
        self.tag = tag
        self.property = property
    }
}
