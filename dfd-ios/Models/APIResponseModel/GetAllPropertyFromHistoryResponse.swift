//
//  GetAllPropertyFromHistoryResponse.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 11/5/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import Foundation
//   let getAllPropertyFromHistoryResponse = try? newJSONDecoder().decode(GetAllPropertyFromHistoryResponse.self, from: jsonData)

import Foundation

// MARK: - GetAllPropertyFromHistoryResponse
class GetAllPropertyFromHistoryResponse: Codable {
    let next, previous: String?
    let count: Int?
    let results: [ListedProperty]?

    init(next: String?, previous: String?, count: Int?, results: [ListedProperty]?) {
        self.next = next
        self.previous = previous
        self.count = count
        self.results = results
    }
}
