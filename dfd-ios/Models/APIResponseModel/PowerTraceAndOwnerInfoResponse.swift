//
//  PowerTraceAndOwnerInfoResponse.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 12/20/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

//   let ownerInfoAndPowerTraceResponse = try? newJSONDecoder().decode(OwnerInfoAndPowerTraceResponse.self, from: jsonData)

import Foundation

// MARK: - OwnerInfoAndPowerTraceResponse
class OwnerInfoAndPowerTraceResponse: Codable {
    let status: Bool?
    let data: OwnerInfoAndPowerTraceResponseData?
    let message: String?

    init(status: Bool?, data: OwnerInfoAndPowerTraceResponseData?, message: String?) {
        self.status = status
        self.data = data
        self.message = message
    }
}

// MARK: - OwnerInfoAndPowerTraceResponseData
class OwnerInfoAndPowerTraceResponseData: Codable {
    let fetchOwnershipInfo: FetchOwnershipInfo?
    let powerTrace: PowerTrace?
    let payment: Bool?
    let upgradeInfo: UpgradeInfo?

    enum CodingKeys: String, CodingKey {
        case fetchOwnershipInfo = "fetch_ownership_info"
        case powerTrace = "power_trace"
        case payment
        case upgradeInfo = "upgrade_info"
    }

    init(fetchOwnershipInfo: FetchOwnershipInfo?, powerTrace: PowerTrace?, payment: Bool?, upgradeInfo: UpgradeInfo?) {
        self.fetchOwnershipInfo = fetchOwnershipInfo
        self.powerTrace = powerTrace
        self.payment = payment
        self.upgradeInfo = upgradeInfo
    }
}

// MARK: - FetchOwnershipInfo
class FetchOwnershipInfo: Codable {
    let status: Bool?
    let data: [OwnerInfo]?
    let message: String?

    init(status: Bool?, data: [OwnerInfo]?, message: String?) {
        self.status = status
        self.data = data
        self.message = message
    }
}

// MARK: - Datum/OwnerInfo
//class Datum: Codable {
//    let fullName, fullAddress: String?
//    let formattedName: FormattedName?
//    let companyName, estate: JSONNull?
//    let formattedAddress: FormattedAddress?
//    let otherInfo: String?
//
//    enum CodingKeys: String, CodingKey {
//        case fullName = "full_name"
//        case fullAddress = "full_address"
//        case formattedName = "formatted_name"
//        case companyName = "company_name"
//        case estate
//        case formattedAddress = "formatted_address"
//        case otherInfo = "other_info"
//    }
//
//    init(fullName: String?, fullAddress: String?, formattedName: FormattedName?, companyName: JSONNull?, estate: JSONNull?, formattedAddress: FormattedAddress?, otherInfo: String?) {
//        self.fullName = fullName
//        self.fullAddress = fullAddress
//        self.formattedName = formattedName
//        self.companyName = companyName
//        self.estate = estate
//        self.formattedAddress = formattedAddress
//        self.otherInfo = otherInfo
//    }
//}

// MARK: - FormattedAddress
//class FormattedAddress: Codable {
//    let street: Street?
//    let city, state, zipCode: String?
//
//    enum CodingKeys: String, CodingKey {
//        case street, city, state
//        case zipCode = "zip_code"
//    }
//
//    init(street: Street?, city: String?, state: String?, zipCode: String?) {
//        self.street = street
//        self.city = city
//        self.state = state
//        self.zipCode = zipCode
//    }
//}
 
// MARK: - PowerTrace
class PowerTrace: Codable {
    let status: Bool?
    let data: PowerTraceData?
    let message: String?

    init(status: Bool?, data: PowerTraceData?, message: String?) {
        self.status = status
        self.data = data
        self.message = message
    }
}

// MARK: - PowerTraceData
class PowerTraceData: Codable {
    let address: String?
    let verifiedCellphones, social: String?
    let traceName, lastName: String?
    let associatedPhone: String?
    let clientLeadInfoID: Int?
    let cellphones, voip: String?
    let id: Int?
    let landline: String?
    let sourceID: Int?
    let verifiedLandline: String?
    let firstName, email: String?
    let personID: Int?
    let status: String?

    enum CodingKeys: String, CodingKey {
        case address
        case verifiedCellphones = "verified_cellphones"
        case social
        case traceName = "trace_name"
        case lastName = "last_name"
        case associatedPhone = "associated_phone"
        case clientLeadInfoID = "client_lead_info_id"
        case cellphones, voip, id, landline
        case sourceID = "source_id"
        case verifiedLandline = "verified_landline"
        case firstName = "first_name"
        case email
        case personID = "person_id"
        case status
    }

    init(address: String?, verifiedCellphones: String?, social: String?, traceName: String?, lastName: String?, associatedPhone: String?, clientLeadInfoID: Int?, cellphones: String?, voip: String?, id: Int?, landline: String?, sourceID: Int?, verifiedLandline: String?, firstName: String?, email: String?, personID: Int?, status: String?) {
        self.address = address
        self.verifiedCellphones = verifiedCellphones
        self.social = social
        self.traceName = traceName
        self.lastName = lastName
        self.associatedPhone = associatedPhone
        self.clientLeadInfoID = clientLeadInfoID
        self.cellphones = cellphones
        self.voip = voip
        self.id = id
        self.landline = landline
        self.sourceID = sourceID
        self.verifiedLandline = verifiedLandline
        self.firstName = firstName
        self.email = email
        self.personID = personID
        self.status = status
    }
}
