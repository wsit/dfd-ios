//
//  GetNeighbourResponse.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 1/7/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//
//   let getNeighbourResponse = try? newJSONDecoder().decode(GetNeighbourResponse.self, from: jsonData)

import Foundation

// MARK: - GetNeighbourResponseElement
class GetNeighbourResponseElement: Codable {
    let id: Int?
    let neighborAddress: String?
    let ownershipInfo: OwnershipInfoForNeighbour?
    let powerTrace: PowerTraceForNeighbour?
    let ownerStatus, powerTraceStatus, status: String?
    let property: Int?
    let latitude, longitude: Double?
    let requestedBy: Int?
    let createdAt, updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case id
        case neighborAddress = "neighbor_address"
        case ownershipInfo = "ownership_info"
        case powerTrace = "power_trace"
        case ownerStatus = "owner_status"
        case powerTraceStatus = "power_trace_status"
        case status, property, latitude, longitude
        case requestedBy = "requested_by"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }

    init(id: Int?, neighborAddress: String?, ownershipInfo: OwnershipInfoForNeighbour?, powerTrace: PowerTraceForNeighbour?, ownerStatus: String?, powerTraceStatus: String?, status: String?, property: Int?, latitude: Double?, longitude: Double?, requestedBy: Int?, createdAt: String?, updatedAt: String?) {
        self.id = id
        self.neighborAddress = neighborAddress
        self.ownershipInfo = ownershipInfo
        self.powerTrace = powerTrace
        self.ownerStatus = ownerStatus
        self.powerTraceStatus = powerTraceStatus
        self.status = status
        self.property = property
        self.latitude = latitude
        self.longitude = longitude
        self.requestedBy = requestedBy
        self.createdAt = createdAt
        self.updatedAt = updatedAt
    }
}

// MARK: - OwnershipInfo
class OwnershipInfoForNeighbour: Codable {
    let fullName, otherInfo, fullAddress: String?
    let formattedName: FormattedName?
    let formattedAddress: FormattedAddress?

    enum CodingKeys: String, CodingKey {
        case fullName = "full_name"
        case otherInfo = "other_info"
        case fullAddress = "full_address"
        case formattedName = "formatted_name"
        case formattedAddress = "formatted_address"
    }

    init(fullName: String?, otherInfo: String?, fullAddress: String?, formattedName: FormattedName?, formattedAddress: FormattedAddress?) {
        self.fullName = fullName
        self.otherInfo = otherInfo
        self.fullAddress = fullAddress
        self.formattedName = formattedName
        self.formattedAddress = formattedAddress
    }
}

// MARK: - PowerTrace
class PowerTraceForNeighbour: Codable {
    let id: Int?
    let voip: String?
    let email: String?
    let social: String?
    let status, address, landline, lastName: String?
    let personID, sourceID: Int?
    let cellphones: String?
    let firstName, traceName, associatedPhone, verifiedLandline: String?
    let clientLeadInfoID: Int?
    let verifiedCellphones: String?

    enum CodingKeys: String, CodingKey {
        case id, voip, email, social, status, address, landline
        case lastName = "last_name"
        case personID = "person_id"
        case sourceID = "source_id"
        case cellphones
        case firstName = "first_name"
        case traceName = "trace_name"
        case associatedPhone = "associated_phone"
        case verifiedLandline = "verified_landline"
        case clientLeadInfoID = "client_lead_info_id"
        case verifiedCellphones = "verified_cellphones"
    }

    init(id: Int?, voip: String?, email: String?, social: String?, status: String?, address: String?, landline: String?, lastName: String?, personID: Int?, sourceID: Int?, cellphones: String?, firstName: String?, traceName: String?, associatedPhone: String?, verifiedLandline: String?, clientLeadInfoID: Int?, verifiedCellphones: String?) {
        self.id = id
        self.voip = voip
        self.email = email
        self.social = social
        self.status = status
        self.address = address
        self.landline = landline
        self.lastName = lastName
        self.personID = personID
        self.sourceID = sourceID
        self.cellphones = cellphones
        self.firstName = firstName
        self.traceName = traceName
        self.associatedPhone = associatedPhone
        self.verifiedLandline = verifiedLandline
        self.clientLeadInfoID = clientLeadInfoID
        self.verifiedCellphones = verifiedCellphones
    }
}

typealias GetNeighbourResponse = [GetNeighbourResponseElement]
