//
//  ListClusterResponse.swift
//  HouZes
//
//  Created by Mahadhi Hassan Chowdhury on 1/28/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//

 //   let listClusterResponse = try? newJSONDecoder().decode(ListClusterResponse.self, from: jsonData)

import Foundation

// MARK: - ListClusterResponseElement
class ListClusterResponseElement: Codable {
    let id: Int?
    let latitude, longitude: Double?
    let propertyTags: [PropertyTag]?

    enum CodingKeys: String, CodingKey {
        case id, latitude, longitude
        case propertyTags = "property_tags"
    }

    init(id: Int?, latitude: Double?, longitude: Double?, propertyTags: [PropertyTag]?) {
        self.id = id
        self.latitude = latitude
        self.longitude = longitude
        self.propertyTags = propertyTags
    }
}

//// MARK: - PropertyTag
//class PropertyTag: Codable {
//    let id: String?
//
//    init(id: String?) {
//        self.id = id
//    }
//}

typealias ListClusterResponse = [ListClusterResponseElement]
