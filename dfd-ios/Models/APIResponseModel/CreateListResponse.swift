//
//  CreateListResponse.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 10/28/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

//   let createListResponse = try? newJSONDecoder().decode(CreateListResponse.self, from: jsonData)

import Foundation

// MARK: - CreateListResponse
class CreateListResponse: Codable {
    let status: Bool?
    let data: MyList?
    let message: String?

    init(status: Bool?, data: MyList?, message: String?) {
        self.status = status
        self.data = data
        self.message = message
    }
}

// MARK: - DataClass
class MyList: Codable {
    let id: Int?
    let name: String?
    let leadsCount: Int?
    let createdAt, updatedAt: String?
    let user: Int?

    enum CodingKeys: String, CodingKey {
        case id, name
        case leadsCount = "leads_count"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case user
    }

    init(id: Int?, name: String?, leadsCount: Int?, createdAt: String?, updatedAt: String?, user: Int?) {
        self.id = id
        self.name = name
        self.leadsCount = leadsCount
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.user = user
    }
}

