//
//  SocialSignInResponse.swift
//  HouZes
//
//  Created by Mahadhi Hassan Chowdhury on 3/27/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//
//   let socialSignInResponse = try? newJSONDecoder().decode(SocialSignInResponse.self, from: jsonData)

import Foundation

// MARK: - SocialSignInResponse
class SocialSignInResponse: Codable {
    var status: Bool?
    var data: LoginResponse?
    var message: String?

    init(status: Bool?, data: LoginResponse?, message: String?) {
        self.status = status
        self.data = data
        self.message = message
    }
}

// MARK: - DataClass
class SocialSignIn: Codable {
    var accessToken: String?
    var expiresIn: Int?
    var tokenType, scope, refreshToken: String?

    enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
        case expiresIn = "expires_in"
        case tokenType = "token_type"
        case scope
        case refreshToken = "refresh_token"
    }

    init(accessToken: String?, expiresIn: Int?, tokenType: String?, scope: String?, refreshToken: String?) {
        self.accessToken = accessToken
        self.expiresIn = expiresIn
        self.tokenType = tokenType
        self.scope = scope
        self.refreshToken = refreshToken
    }
}
