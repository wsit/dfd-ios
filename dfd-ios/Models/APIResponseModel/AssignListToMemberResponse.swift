//
//  AssignListToMemberResponse.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 9/26/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

//   let assignListToMemberResponse = try? newJSONDecoder().decode(AssignListToMemberResponse.self, from: jsonData)

import Foundation

// MARK: - AssignListToMemberResponse
class AssignListToMemberResponse: Codable {
    let status: Bool?
    let data: DataClass?
    let message: String?
    
    init(status: Bool?, data: DataClass?, message: String?) {
        self.status = status
        self.data = data
        self.message = message
    }
}

// MARK: - DataClass
class DataClass: Codable {
    let id: Int?
    let createdAt, updatedAt: String?
    let list, member: Int?
    
    enum CodingKeys: String, CodingKey {
        case id
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case list, member
    }
    
    init(id: Int?, createdAt: String?, updatedAt: String?, list: Int?, member: Int?) {
        self.id = id
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.list = list
        self.member = member
    }
}
