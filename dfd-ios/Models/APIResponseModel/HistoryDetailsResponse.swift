//
//  HistoryDetailsResponse.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 11/4/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

//   let historyDetailsResponse = try? newJSONDecoder().decode(HistoryDetailsResponse.self, from: jsonData)

import Foundation

// MARK: - HistoryDetailsResponse
class HistoryDetailsResponse: Codable {
    let id: Int?
    let startPointLatitude, startPointLongitude, endPointLatitude, endPointLongitude: Double?
    let image: String?
    let startTime, endTime: String?
    let length: Double?
    let polylines: String? //[Polyline]?
    let user, propertyCount: Int?
    let property: [ListedProperty]?
    let createdAt, updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case id
        case startPointLatitude = "start_point_latitude"
        case startPointLongitude = "start_point_longitude"
        case endPointLatitude = "end_point_latitude"
        case endPointLongitude = "end_point_longitude"
        case image
        case startTime = "start_time"
        case endTime = "end_time"
        case polylines, length, user
        case propertyCount = "property_count"
        case property
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }

    init(id: Int?, startPointLatitude: Double?, startPointLongitude: Double?, endPointLatitude: Double?, endPointLongitude: Double?, image: String?, startTime: String?, endTime: String?, polylines: String?, length: Double?, user: Int?, propertyCount: Int?, property: [ListedProperty]?, createdAt: String?, updatedAt: String?) {
        self.id = id
        self.startPointLatitude = startPointLatitude
        self.startPointLongitude = startPointLongitude
        self.endPointLatitude = endPointLatitude
        self.endPointLongitude = endPointLongitude
        self.image = image
        self.startTime = startTime
        self.endTime = endTime
        self.polylines = polylines
        self.length = length
        self.user = user
        self.propertyCount = propertyCount
        self.property = property
        self.createdAt = createdAt
        self.updatedAt = updatedAt
    }
}
