//
//  DeleteTeamMemberResponse.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 10/30/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

//   let deleteTeamMemberResponse = try? newJSONDecoder().decode(DeleteTeamMemberResponse.self, from: jsonData)

import Foundation

// MARK: - DeleteTeamMemberResponse
class DeleteTeamMemberResponse: Codable {
    let message: String?
    let status: Bool?

    init(message: String?, status: Bool?) {
        self.message = message
        self.status = status
    }
}
