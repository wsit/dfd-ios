//
//  GetPropertyPhotosResponse.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 10/1/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

//   let getPropertyPhotosResponse = try? newJSONDecoder().decode(GetPropertyPhotosResponse.self, from: jsonData)

import Foundation

// MARK: - GetPropertyPhotosResponse
class GetPropertyPhotosResponse: Codable {
    let next, previous: String?
    let count: Int?
    let results: [PropertyPhoto]?
    
    init(next: String?, previous: String?, count: Int?, results: [PropertyPhoto]?) {
        self.next = next
        self.previous = previous
        self.count = count
        self.results = results
    }
}

// MARK: - Result
class PropertyPhoto: Codable {
    let id: Int?
    let photoURL, thumbPhotoUrl: String?
    let createdAt, updatedAt: String?
    let user, property: Int?
    
    enum CodingKeys: String, CodingKey {
        case id
        case photoURL = "photo_url"
        case thumbPhotoUrl = "thumb_photo_url"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case user, property
    }
    
    init(id: Int?, photoURL: String?,  thumbPhotoUrl: String?, createdAt: String?, updatedAt: String?, user: Int?, property: Int?) {
        self.id = id
        self.photoURL = photoURL
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.user = user
        self.property = property
        self.thumbPhotoUrl = thumbPhotoUrl
    }
}
