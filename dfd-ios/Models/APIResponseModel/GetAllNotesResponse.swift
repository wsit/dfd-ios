//
//  GetAllNotesResponse.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 9/3/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

//   let getAllNotesResponse = try? newJSONDecoder().decode(GetAllNotesResponse.self, from: jsonData)

import Foundation

// MARK: - GetAllNotesResponse
class GetAllNotesResponse: Codable {
    let count: Int?
    let next, previous: String?
    let results: [Note]?
    
    init(count: Int?, next: String?, previous: String?, results: [Note]?) {
        self.count = count
        self.next = next
        self.previous = previous
        self.results = results
    }
}

// MARK: - Result
class Note: Codable {
    let id: Int?
    let title, notes, createdAt, updatedAt: String?
    let user, property: Int?
    
    enum CodingKeys: String, CodingKey {
        case id, title, notes
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case user, property
    }
    
    init(id: Int?, title: String?, notes: String?, createdAt: String?, updatedAt: String?, user: Int?, property: Int?) {
        self.id = id
        self.title = title
        self.notes = notes
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.user = user
        self.property = property
    }
}
