//
//  AddMemberResponse.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 10/30/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import Foundation
//   let addMemberResponse = try? newJSONDecoder().decode(AddMemberResponse.self, from: jsonData)

import Foundation

// MARK: - AddMemberResponse
class AddMemberResponse: Codable {
    let status: Bool?
    let data: UnregisteredInvitation?
    let message: String?

    init(status: Bool?, data: UnregisteredInvitation?, message: String?) {
        self.status = status
        self.data = data
        self.message = message
    }
}
