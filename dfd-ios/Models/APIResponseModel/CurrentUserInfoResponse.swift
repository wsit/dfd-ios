 
 //   let currentUserInfoResponse = try? newJSONDecoder().decode(CurrentUserInfoResponse.self, from: jsonData)
 
 import Foundation
 
 // MARK: - CurrentUserInfoResponse
 class CurrentUserInfoResponse: Codable {
    let id: Int?
    let lastLogin, firstName, lastName, email: String?
    let phoneNumber: String?
    let invitedBy: Int?
    let photo: String?
    var photoThumb: String?
    let isActive, isAdmin, thinker: Bool?
    let upgradeInfo: UpgradeInfo?
    let createdAt, updatedAt: String?
    var defaultList: MList?
    
    enum CodingKeys: String, CodingKey {
        case id
        case lastLogin = "last_login"
        case firstName = "first_name"
        case lastName = "last_name"
        case email
        case phoneNumber = "phone_number"
        case invitedBy = "invited_by"
        case photo
        case photoThumb = "photo_thumb"
        case isActive = "is_active"
        case isAdmin = "is_admin"
        case thinker
        case upgradeInfo = "upgrade_info"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case defaultList = "default_list"

    }
    
    init(id: Int?, lastLogin: String?, firstName: String?, lastName: String?, email: String?, phoneNumber: String?, invitedBy: Int?, photo: String?, photoThumb:String, isActive: Bool?, isAdmin: Bool?, thinker: Bool?, upgradeInfo: UpgradeInfo?, createdAt: String?, updatedAt: String?, defaultList: MList?) {
        self.id = id
        self.lastLogin = lastLogin
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.phoneNumber = phoneNumber
        self.invitedBy = invitedBy
        self.thinker = thinker
        self.photo = photo
        self.photoThumb = photoThumb
        self.isActive = isActive
        self.isAdmin = isAdmin
        self.upgradeInfo = upgradeInfo
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.defaultList = defaultList
    }
    
 }
 
 // MARK: - UpgradeInfo
 class UpgradeInfo: Codable {
    let upgrade: Bool?
    let amount: Double?
    let discount: Double?
    let powerTrace, ownerInfo, mailWizard: Double?
    let isTeamInvitable: Bool?
    let plan: Plan?
    
    enum CodingKeys: String, CodingKey {
        case upgrade, amount,discount
        case powerTrace = "power_trace"
        case ownerInfo = "owner_info"
        case mailWizard = "mailer_wizard"
        case isTeamInvitable = "is_team_invitable"
        case plan
        
    }
    
    init(isTeamInvitable: Bool?,upgrade: Bool?, amount: Double?, discount:Double?, powerTrace: Double?, ownerInfo: Double?, mailWizard: Double?, plan: Plan?) {
        self.isTeamInvitable = isTeamInvitable
        self.upgrade = upgrade
        self.amount = amount
        self.discount = discount
        self.powerTrace = powerTrace
        self.ownerInfo = ownerInfo
        self.mailWizard = mailWizard
        self.plan = plan
        
    }
 }
 // MARK: - Plan
 class Plan: Codable {
    let id: Int?
    let planName: String?
    let planCost, planCoin : Double?
    let updatedBy, createdBy: Int?
    let createdAt, updatedAt: String?
    
    let planDescription: String?
    let image: String?
    
    
    enum CodingKeys: String, CodingKey {
        case id
        case planName = "plan_name"
        case planCost = "plan_cost"
        case planCoin = "plan_coin"
        case createdBy = "created_by"
        case updatedBy = "updated_by"
        case planDescription = "description"
        case image
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
    
    init(id: Int?, planName: String?, planCost: Double?, planCoin: Double?, createdBy: Int?, updatedBy: Int?, createdAt: String?, updatedAt: String?, planDescription: String, image: String) {
        self.id = id
        self.planName = planName
        self.planCost = planCost
        self.planCoin = planCoin
        self.createdBy = createdBy
        self.updatedBy = updatedBy
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.planDescription = planDescription
        self.image = image
    }
 }
 
 
 // MARK: - Encode/decode helpers
 // MARK: - Encode/decode helpers
 
 class JSONNull: Codable, Hashable {
    
    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }
    
    public var hashValue: Int {
        return 0
    }
    
    public init() {}
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
 }
 
 
 //import Foundation
 //
 //// MARK: - CurrentUserInfoResponse
 //class CurrentUserInfoResponse: Codable {
 ////    let id: Int?
 ////    let lastLogin: String?
 ////    let firstName, lastName, email, phoneNumber: String?
 ////    let invitedBy: Int?
 ////    let photo: String?
 ////    let isActive, isAdmin: Bool?
 ////    let upgradeInfo: UpgradeInfo?
 ////    let createdAt, updatedAt: String?
 ////
 ////    enum CodingKeys: String, CodingKey {
 ////        case id
 ////        case lastLogin = "last_login"
 ////        case firstName = "first_name"
 ////        case lastName = "last_name"
 ////        case email
 ////        case phoneNumber = "phone_number"
 ////        case invitedBy = "invited_by"
 ////        case photo
 ////        case isActive = "is_active"
 ////        case isAdmin = "is_admin"
 ////        case upgradeInfo = "upgrade_info"
 ////        case createdAt = "created_at"
 ////        case updatedAt = "updated_at"
 ////    }
 ////
 ////    init(id: Int?, lastLogin: String?, firstName: String?, lastName: String?, email: String?, phoneNumber: String?, invitedBy: Int?, photo: String?, isActive: Bool?, isAdmin: Bool?, upgradeInfo: UpgradeInfo?, createdAt: String?, updatedAt: String?) {
 ////        self.id = id
 ////        self.lastLogin = lastLogin
 ////        self.firstName = firstName
 ////        self.lastName = lastName
 ////        self.email = email
 ////        self.phoneNumber = phoneNumber
 ////        self.invitedBy = invitedBy
 ////        self.photo = photo
 ////        self.isActive = isActive
 ////        self.isAdmin = isAdmin
 ////        self.upgradeInfo = upgradeInfo
 ////        self.createdAt = createdAt
 ////        self.updatedAt = updatedAt
 ////    }
 //   let id: Int?
 //    let lastLogin, firstName, lastName, email: String?
 //    let phoneNumber: String?
 //    let invitedBy: Int?
 //    let photo: String?
 //    let isActive, isAdmin: Bool?
 //    let upgradeInfo: UpgradeInfo?
 //    let createdAt, updatedAt: String?
 //
 //    enum CodingKeys: String, CodingKey {
 //        case id
 //        case lastLogin = "last_login"
 //        case firstName = "first_name"
 //        case lastName = "last_name"
 //        case email
 //        case phoneNumber = "phone_number"
 //        case invitedBy = "invited_by"
 //        case photo
 //        case isActive = "is_active"
 //        case isAdmin = "is_admin"
 //        case upgradeInfo = "upgrade_info"
 //        case createdAt = "created_at"
 //        case updatedAt = "updated_at"
 //    }
 //
 //    init(id: Int?, lastLogin: String?, firstName: String?, lastName: String?, email: String?, phoneNumber: String?, invitedBy: Int?, photo: String?, isActive: Bool?, isAdmin: Bool?, upgradeInfo: UpgradeInfo?, createdAt: String?, updatedAt: String?) {
 //        self.id = id
 //        self.lastLogin = lastLogin
 //        self.firstName = firstName
 //        self.lastName = lastName
 //        self.email = email
 //        self.phoneNumber = phoneNumber
 //        self.invitedBy = invitedBy
 //        self.photo = photo
 //        self.isActive = isActive
 //        self.isAdmin = isAdmin
 //        self.upgradeInfo = upgradeInfo
 //        self.createdAt = createdAt
 //        self.updatedAt = updatedAt
 //    }
 //}
 //
 //
 //
 //// MARK: - UpgradeInfo
 //class UpgradeInfo: Codable {
 //    let upgrade: Bool?
 //    let amount: Int?
 //    let powerTrace, ownerInfo: Double?
 //
 //    enum CodingKeys: String, CodingKey {
 //        case upgrade, amount
 //        case powerTrace = "power_trace"
 //        case ownerInfo = "owner_info"
 //    }
 //
 //    init(upgrade: Bool?, amount: Int?, powerTrace: Double?, ownerInfo: Double?) {
 //        self.upgrade = upgrade
 //        self.amount = amount
 //        self.powerTrace = powerTrace
 //        self.ownerInfo = ownerInfo
 //    }
 //}
 ////    let id: Int?
 ////    let password: String?
 ////    let lastLogin: String?
 ////    let firstName, lastName, email, phoneNumber, photo: String?
 ////    let invitedBy: Int?
 ////    let createdAt, updatedAt: String?
 ////    let isActive, isAdmin: Bool?
 ////
 ////    enum CodingKeys: String, CodingKey {
 ////        case id, password
 ////        case lastLogin = "last_login"
 ////        case firstName = "first_name"
 ////        case lastName = "last_name"
 ////        case email
 ////        case photo
 ////        case phoneNumber = "phone_number"
 ////        case invitedBy = "invited_by"
 ////        case createdAt = "created_at"
 ////        case updatedAt = "updated_at"
 ////        case isActive = "is_active"
 ////        case isAdmin = "is_admin"
 ////    }
 ////
 ////    init(id: Int?, password: String?, lastLogin: String?, firstName: String?, lastName: String?, email: String?, photo: String, phoneNumber: String?, invitedBy: Int?, createdAt: String?, updatedAt: String?, isActive: Bool?, isAdmin: Bool?) {
 ////        self.id = id
 ////        self.password = password
 ////        self.lastLogin = lastLogin
 ////        self.firstName = firstName
 ////        self.lastName = lastName
 ////        self.email = email
 ////        self.photo = photo
 ////        self.phoneNumber = phoneNumber
 ////        self.invitedBy = invitedBy
 ////        self.createdAt = createdAt
 ////        self.updatedAt = updatedAt
 ////        self.isActive = isActive
 ////        self.isAdmin = isAdmin
 ////    }
 ////}
