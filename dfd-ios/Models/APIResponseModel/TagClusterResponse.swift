//
//  TagClusterResponse.swift
//  HouZes
//
//  Created by Mahadhi Hassan Chowdhury on 1/30/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//

//   let tagClusterResponse = try? newJSONDecoder().decode(TagClusterResponse.self, from: jsonData)

import Foundation

// MARK: - TagClusterResponse
class TagClusterResponse: Codable {
    let next: String?
    let previous: String?
    let count: Int?
    let results: [TagClusterResponseElement]?

    init(next: String?, previous: String?, count: Int?, results: [TagClusterResponseElement]?) {
        self.next = next
        self.previous = previous
        self.count = count
        self.results = results
    }
}

// MARK: - Result
class TagClusterResponseElement: Codable {
    let id: Int?
    let latitude, longitude: Double?
    let propertyTags: [PropertyTag]?

    enum CodingKeys: String, CodingKey {
        case id, latitude, longitude
        case propertyTags = "property_tags"
    }

    init(id: Int?, latitude: Double?, longitude: Double?, propertyTags: [PropertyTag]?) {
        self.id = id
        self.latitude = latitude
        self.longitude = longitude
        self.propertyTags = propertyTags
    }
}
