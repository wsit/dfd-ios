//
//  MyTeamResponse.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 8/26/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//
//   let myTeamResponse = try? newJSONDecoder().decode(MyTeamResponse.self, from: jsonData)

import Foundation

// MARK: - MyTeamResponse
class MyTeamResponse: Codable {//CurrentUserInfoResponse
    let users: [UnregisteredInvitation]?
    let unregisteredInvitations: [UnregisteredInvitation]?
    
    enum CodingKeys: String, CodingKey {
        case users
        case unregisteredInvitations = "unregistered_invitations"
    }
    
    init(users: [UnregisteredInvitation]?, unregisteredInvitations: [UnregisteredInvitation]?) {
        self.users = users
        self.unregisteredInvitations = unregisteredInvitations
    }
}

// MARK: - UnregisteredInvitation
class UnregisteredInvitation: Codable {
    let id: Int?
    let email: String?
    let status: Int?
    let invitationKey: String?
    let firstName, lastName, photo, phoneNumber: String?
    let createdAt, updatedAt: String?
    let user: Int?
    let invitedBy: Int?
    
    let isActive, isAdmin: Bool?
    
    enum CodingKeys: String, CodingKey {
        case id, email, status
        case invitationKey = "invitation_key"
        case firstName = "first_name"
        case lastName = "last_name"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case user
        case photo
        case phoneNumber = "phone_number"
        case invitedBy = "invited_by"
        case isActive = "is_active"
        case isAdmin = "is_admin"
    }
    
    init(id: Int?, email: String?, status: Int?, invitationKey: String?, firstName: String?, lastName: String?, createdAt: String?, updatedAt: String?, user: Int?, photo:String, phoneNumber: String, invitedBy: Int, isActive: Bool?, isAdmin: Bool?) {
        self.id = id
        self.email = email
        self.status = status
        self.invitationKey = invitationKey
        self.firstName = firstName
        self.lastName = lastName
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.user = user
        self.photo = photo
        self.phoneNumber = phoneNumber
        self.invitedBy = invitedBy
        self.isActive = isActive
        self.isAdmin = isAdmin
    }
}


// MARK: - User
class User: Codable {
    let id: Int?
    let password: String?
    let lastLogin: String?
    let firstName, lastName, email, phoneNumber: String?
    let invitedBy: Int?
    let createdAt, updatedAt: String?
    let isActive, isAdmin: Bool?
    
    enum CodingKeys: String, CodingKey {
        case id, password
        case lastLogin = "last_login"
        case firstName = "first_name"
        case lastName = "last_name"
        case email
        case phoneNumber = "phone_number"
        case invitedBy = "invited_by"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case isActive = "is_active"
        case isAdmin = "is_admin"
    }
    
    init(id: Int?, password: String?, lastLogin: String?, firstName: String?, lastName: String?, email: String?, phoneNumber: String?, invitedBy: Int?, createdAt: String?, updatedAt: String?, isActive: Bool?, isAdmin: Bool?) {
        self.id = id
        self.password = password
        self.lastLogin = lastLogin
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.phoneNumber = phoneNumber
        self.invitedBy = invitedBy
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.isActive = isActive
        self.isAdmin = isAdmin
    }
}
