//
//  GetAllLists.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 8/6/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

//   let getAllListsResponse = try? newJSONDecoder().decode(GetAllListsResponse.self, from: jsonData)

import Foundation

// MARK: - GetAllListsResponse
class GetAllListsResponse: Codable {
    let count: Int?
    let next, previous: String?
    let results: [MList]?
    
    init(count: Int?, next: String?, previous: String?, results: [MList]?) {
        self.count = count
        self.next = next
        self.previous = previous
        self.results = results
    }
}

// MARK: - Result
class MList: Codable {
    let id: Int?
    let name: String?
    let leadsCount: Int?
    let createdAt, updatedAt: String?
    let user: Int?
    var fetchLatLng, isDefault: Bool?

    
    enum CodingKeys: String, CodingKey {
        case id, name
        case leadsCount = "leads_count"
        case createdAt = "created_at"
        case fetchLatLng = "fetch_lat_lng"
        case isDefault = "is_default"
        case updatedAt = "updated_at"
        case user
    }
    
    init(id: Int?, name: String?, leadsCount: Int?, fetchLatLng: Bool?, isDefault: Bool?, createdAt: String?, updatedAt: String?, user: Int?) {
        self.id = id
        self.name = name
        self.leadsCount = leadsCount
        self.fetchLatLng = fetchLatLng
        self.isDefault = isDefault
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.user = user
    }
//    init(id: Int?, name: String?, leadsCount: Int?, createdAt: String?, updatedAt: String?, user: Int?) {
//        self.id = id
//        self.name = name
//        self.leadsCount = leadsCount
//        self.createdAt = createdAt
//        self.updatedAt = updatedAt
//        self.user = user
//    }
}


