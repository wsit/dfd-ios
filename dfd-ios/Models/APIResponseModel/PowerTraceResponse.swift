//
//  PowerTraceResponse.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 12/20/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

//   let powerTraceResponse = try? newJSONDecoder().decode(PowerTraceResponse.self, from: jsonData)

import Foundation

// MARK: - PowerTraceResponse
class PowerTraceResponse: Codable {
    let status: Bool?
    let message: String?
    let data: [PowerTraceDataInfo]?

    init(status: Bool?, message: String?, data: [PowerTraceDataInfo]?) {
        self.status = status
        self.message = message
        self.data = data
    }
}

// MARK: - Datum
class PowerTraceDataInfo: Codable {
    let address, verifiedCellphones, social, traceName: String?
    let lastName, associatedPhone: String?
    let clientLeadInfoID: Int?
    let cellphones, voip: String?
    let id: Int?
    let landline: String?
    let sourceID: Int?
    let verifiedLandline, firstName, email: String?
    let personID: Int?
    let status: String?

    enum CodingKeys: String, CodingKey {
        case address
        case verifiedCellphones = "verified_cellphones"
        case social
        case traceName = "trace_name"
        case lastName = "last_name"
        case associatedPhone = "associated_phone"
        case clientLeadInfoID = "client_lead_info_id"
        case cellphones, voip, id, landline
        case sourceID = "source_id"
        case verifiedLandline = "verified_landline"
        case firstName = "first_name"
        case email
        case personID = "person_id"
        case status
    }

    init(address: String?, verifiedCellphones: String?, social: String?, traceName: String?, lastName: String?, associatedPhone: String?, clientLeadInfoID: Int?, cellphones: String?, voip: String?, id: Int?, landline: String?, sourceID: Int?, verifiedLandline: String?, firstName: String?, email: String?, personID: Int?, status: String?) {
        self.address = address
        self.verifiedCellphones = verifiedCellphones
        self.social = social
        self.traceName = traceName
        self.lastName = lastName
        self.associatedPhone = associatedPhone
        self.clientLeadInfoID = clientLeadInfoID
        self.cellphones = cellphones
        self.voip = voip
        self.id = id
        self.landline = landline
        self.sourceID = sourceID
        self.verifiedLandline = verifiedLandline
        self.firstName = firstName
        self.email = email
        self.personID = personID
        self.status = status
    }
}
