//
//  GellAllTemplateResponse.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 1/10/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//

//   let getAllTemplateResponse = try? newJSONDecoder().decode(GetAllTemplateResponse.self, from: jsonData)

import Foundation

// MARK: - GetAllTemplateResponse
class GetAllTemplateResponse: Codable {
    let status: Bool?
    let message: String?
    let data: [MailTemplate]?

    init(status: Bool?, message: String?, data: [MailTemplate]?) {
        self.status = status
        self.message = message
        self.data = data
    }
}

// MARK: - Datum
class MailTemplate: Codable {
    let itemID, code, caption, datumDescription: String?
    let url, thumbURL: String?

    enum CodingKeys: String, CodingKey {
        case itemID = "item_id"
        case code, caption
        case datumDescription = "description"
        case url
        case thumbURL = "thumbUrl"
    }

    init(itemID: String?, code: String?, caption: String?, datumDescription: String?, url: String?, thumbURL: String?) {
        self.itemID = itemID
        self.code = code
        self.caption = caption
        self.datumDescription = datumDescription
        self.url = url
        self.thumbURL = thumbURL
    }
}
