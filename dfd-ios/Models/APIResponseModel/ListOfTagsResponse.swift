//
//  ListOfTagsResponse.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 9/25/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//


//   let listOfTagsResponse = try? newJSONDecoder().decode(ListOfTagsResponse.self, from: jsonData)

import Foundation

// MARK: - ListOfTagsResponseElement
//class Tag: Codable {
//    let id: Int?
//    let name: String?
//    let color: TagColor?
//    let user: Int?
//    let createdAt, updatedAt: String?
//
//    enum CodingKeys: String, CodingKey {
//        case id, name, color, user
//        case createdAt = "created_at"
//        case updatedAt = "updated_at"
//    }
//
//    init(id: Int?, name: String?, color: TagColor?, user: Int?, createdAt: String?, updatedAt: String?) {
//        self.id = id
//        self.name = name
//        self.color = color
//        self.user = user
//        self.createdAt = createdAt
//        self.updatedAt = updatedAt
//    }
//}


// MARK: - Color
//class TagColor: Codable {
//    let colorName, colorCode: String?
//    
//    enum CodingKeys: String, CodingKey {
//        case colorName = "color_name"
//        case colorCode = "color_code"
//    }
//    
//    init(colorName: String?, colorCode: String?) {
//        self.colorName = colorName
//        self.colorCode = colorCode
//    }
//}

 var globalSelectedTag : PropertyTag?
typealias ListOfTagsResponse = [PropertyTag]
