//
//  StartDrivingResponse.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 10/16/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import Foundation
//   let startDrivingResponse = try? newJSONDecoder().decode(StartDrivingResponse.self, from: jsonData)


// MARK: - StartDrivingResponse
class DrivingResponse: Codable {
    let status: Bool?
    let data: DrivingInfo?//let data: DrivingInfo?
    let message: String?

    init(status: Bool?, data: DrivingInfo?, message: String?) {
        self.status = status
        self.data = data
        self.message = message
    }
}

// MARK: - DataClass
class DrivingInfo: Codable {
    let id: Int?
    let startPointLatitude, startPointLongitude: Double?
    let endPointLatitude, endPointLongitude: Double?
    let image: String?
    let startTime, endTime, polylines: String?
    let length:Double?
    let user, propertyCount: Int?
    let createdAt, updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case id
        case startPointLatitude = "start_point_latitude"
        case startPointLongitude = "start_point_longitude"
        case endPointLatitude = "end_point_latitude"
        case endPointLongitude = "end_point_longitude"
        case image
        case startTime = "start_time"
        case endTime = "end_time"
        case polylines, length, user
        case propertyCount = "property_count"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }

    init(id: Int?, startPointLatitude: Double?, startPointLongitude: Double?, endPointLatitude: Double?, endPointLongitude: Double?, image: String?, startTime: String?, endTime: String?, polylines: String?, length: Double?, user: Int?, propertyCount: Int?, createdAt: String?, updatedAt: String?) {
        self.id = id
        self.startPointLatitude = startPointLatitude
        self.startPointLongitude = startPointLongitude
        self.endPointLatitude = endPointLatitude
        self.endPointLongitude = endPointLongitude
        self.image = image
        self.startTime = startTime
        self.endTime = endTime
        self.polylines = polylines
        self.length = length
        self.user = user
        self.propertyCount = propertyCount
        self.createdAt = createdAt
        self.updatedAt = updatedAt
    }
}

// MARK: - Polyline
class Polyline: Codable {
    let latitude, longitude: Double?
    let position: Int?

    init(latitude: Double?, longitude: Double?, position: Int?) {
        self.latitude = latitude
        self.longitude = longitude
        self.position = position
    }
}

class SelectedNeighbour: Codable {
    let latitude, longitude: Double?
    let address: String?
    let street: String?
    let city : String?
    let state: String?
    let zip: String?

    init(latitude: Double?, longitude: Double?, address: String?, street: String?, city : String?, state: String?, zip: String?) {
        self.latitude = latitude
        self.longitude = longitude
        self.address = address
        self.street = street
        self.city = city
        self.state = state
        self.zip = zip
        
    }
}
