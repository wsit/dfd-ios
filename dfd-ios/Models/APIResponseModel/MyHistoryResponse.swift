//
//  MyHistoryResponse.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 10/18/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

//   let myHistoryResponse = try? newJSONDecoder().decode(MyHistoryResponse.self, from: jsonData)

import Foundation

// MARK: - MyHistoryResponse
class MyHistoryResponse: Codable {
    let next, previous: String?
    let count: Int?
    let results: [MyDrive]?

    init(next: String?, previous: String?, count: Int?, results: [MyDrive]?) {
        self.next = next
        self.previous = previous
        self.count = count
        self.results = results
    }
}

// MARK: - Result
class MyDrive: Codable {
    let id: Int?
    let startPointLatitude, startPointLongitude, endPointLatitude, endPointLongitude: Double?
    let image: String?
    let startTime, endTime, polylines: String?
    let length: Double?
    let user, propertyCount: Int?
    let createdAt, updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case id
        case startPointLatitude = "start_point_latitude"
        case startPointLongitude = "start_point_longitude"
        case endPointLatitude = "end_point_latitude"
        case endPointLongitude = "end_point_longitude"
        case image
        case startTime = "start_time"
        case endTime = "end_time"
        case polylines, length, user
        case propertyCount = "property_count"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }

    init(id: Int?, startPointLatitude: Double?, startPointLongitude: Double?, endPointLatitude: Double?, endPointLongitude: Double?, image: String?, startTime: String?, endTime: String?, polylines: String?, length: Double?, user: Int?, propertyCount: Int?, createdAt: String?, updatedAt: String?) {
        self.id = id
        self.startPointLatitude = startPointLatitude
        self.startPointLongitude = startPointLongitude
        self.endPointLatitude = endPointLatitude
        self.endPointLongitude = endPointLongitude
        self.image = image
        self.startTime = startTime
        self.endTime = endTime
        self.polylines = polylines
        self.length = length
        self.user = user
        self.propertyCount = propertyCount
        self.createdAt = createdAt
        self.updatedAt = updatedAt
    }
}
