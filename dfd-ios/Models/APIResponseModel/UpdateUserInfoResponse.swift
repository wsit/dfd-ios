//
//  UpdateUserInfoResponse.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 1/2/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let updateUserInfoResponse = try? newJSONDecoder().decode(UpdateUserInfoResponse.self, from: jsonData)

import Foundation
import SwiftyJSON

// MARK: - UpdateUserInfoResponse
class UpdateUserInfoResponse: Codable {
    let status: Bool?
    let data: CurrentUserInfoResponse?
    let message: String?
//    
//    init(json: [String: Any]) {
//        let status = json["status"] as? String
//        let message = json["message"] as? String
//        let data = json["data"] as? [String: Any]
//        let user = CurrentUserInfoUpdatedResponse(json: data!)
//    }
        init(status: Bool?, data: CurrentUserInfoResponse?, message: String?) {
            self.status = status
            self.data = data
            self.message = message
        }
    //
    //
//    required init(from decoder: Decoder) throws {
//            let values = try decoder.container(keyedBy: CodingKeys.self)
//            status = try values.decodeIfPresent(Bool.self, forKey: .status)
//            data = try values.decodeIfPresent(CurrentUserInfoResponse.self, forKey: .data)
//            message = try values.decodeIfPresent(String.self, forKey: .message)
//        }
    //    init(json: JSON) {
    //        self.status = json["status"].boolValue
    //        self.data = json["data"].map(CurrentUserInfoResponse.self)
    //        self.message = json["message"].stringValue
    //    }
}
//
//class CurrentUserInfoUpdatedResponse: Codable {
//    let id: Int?
//    let lastLogin, firstName, lastName, email: String?
//    let phoneNumber: String?
//    let invitedBy: Int?
//    let photo: String?
//    let isActive, isAdmin: Bool?
//    let upgradeInfo: UpgradeInfo?
//    let createdAt, updatedAt: String?
//
////    enum CodingKeys: String, CodingKey {
////        case id
////        case lastLogin = "last_login"
////        case firstName = "first_name"
////        case lastName = "last_name"
////        case email
////        case phoneNumber = "phone_number"
////        case invitedBy = "invited_by"
////        case photo
////        case isActive = "is_active"
////        case isAdmin = "is_admin"
////        case upgradeInfo = "upgrade_info"
////        case createdAt = "created_at"
////        case updatedAt = "updated_at"
////    }
//
//    init(json: [String: Any]) {
//        let id = json["id"] as? Int
//        let lastLogin = json["last_login"] as? String
//        let firstName = json["first_name"] as? String
//        let lastName = json["last_name"] as? String
//        let email = json["email"] as? String
//        let phoneNumber = json["phone_number"] as? String
//        let invitedBy = json["invited_by"] as? Int
//        let photo = json["photo"] as? String
//        let status = json["is_active"] as? String
//        let isActive = json["is_admin"] as? Bool
//        let isAdmin = json["upgrade_info"] as? Bool
//        let createdAt = json["created_at"] as? String
//        let updatedAt = json["updated_at"] as? String
//        let upgradeInfo = json["upgrade_info"] as? [String: Any]
//
//
//    }
//}
////    init(id: Int?, lastLogin: String?, firstName: String?, lastName: String?, email: String?, phoneNumber: String?, invitedBy: Int?, photo: String?, isActive: Bool?, isAdmin: Bool?, upgradeInfo: UpgradeInfo?, createdAt: String?, updatedAt: String?) {
////        self.id = id
////        self.lastLogin = lastLogin
////        self.firstName = firstName
////        self.lastName = lastName
////        self.email = email
////        self.phoneNumber = phoneNumber
////        self.invitedBy = invitedBy
////        self.photo = photo
////        self.isActive = isActive
////        self.isAdmin = isAdmin
////        self.upgradeInfo = upgradeInfo
////        self.createdAt = createdAt
////        self.updatedAt = updatedAt
////    }
//
//    init(json: [String: Any]) {
//        let status = json["status"] as? String
//        let message = json["message"] as? String
//    }
//}

