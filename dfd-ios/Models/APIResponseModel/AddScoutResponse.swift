//
//  AddScoutResponse.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 10/30/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//
//   let addScoutResponse = try? newJSONDecoder().decode(AddScoutResponse.self, from: jsonData)

import Foundation

// MARK: - AddScoutResponse
class AddScoutResponse: Codable {
    let status: Bool?
    let data: Scout?
    let message: String?

    init(status: Bool?, data: Scout?, message: String?) {
        self.status = status
        self.data = data
        self.message = message
    }
}
