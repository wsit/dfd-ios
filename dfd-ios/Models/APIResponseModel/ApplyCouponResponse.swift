//
//  ApplyCouponResponse.swift
//  HouZes
//
//  Created by Mahadhi Hassan Chowdhury on 7/14/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//

import Foundation
//   let couponResponse = try? newJSONDecoder().decode(CouponResponse.self, from: jsonData)

import Foundation

// MARK: - CouponResponse
class ApplyCouponResponse: Codable {
    var status: Bool?
    var data: CurrentUserInfoResponse?
    var message: String?
    
    init(status: Bool?, data: CurrentUserInfoResponse?, message: String?) {
        self.status = status
        self.data = data
        self.message = message
    }
}
