
//   let signUpResponse = try? newJSONDecoder().decode(SignUpResponse.self, from: jsonData)

import Foundation

// MARK: - SignUpResponse
class SignUpResponse: Codable {
    let status: Bool?
    let data: SignedUpUser?
    let message: String?

    init(status: Bool?, data: SignedUpUser?, message: String?) {
        self.status = status
        self.data = data
        self.message = message
    }
}

// MARK: - DataClass
class SignedUpUser: Codable {
    let id: Int?
    let password, lastLogin, firstName, lastName: String?
    let email, phoneNumber, invitedBy, photo: String?
    let createdAt, updatedAt: String?
    let isActive, isAdmin: Bool?

    enum CodingKeys: String, CodingKey {
        case id, password
        case lastLogin = "last_login"
        case firstName = "first_name"
        case lastName = "last_name"
        case email
        case phoneNumber = "phone_number"
        case invitedBy = "invited_by"
        case photo
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case isActive = "is_active"
        case isAdmin = "is_admin"
    }

    init(id: Int?, password: String?, lastLogin: String?, firstName: String?, lastName: String?, email: String?, phoneNumber: String?, invitedBy: String?, photo: String?, createdAt: String?, updatedAt: String?, isActive: Bool?, isAdmin: Bool?) {
        self.id = id
        self.password = password
        self.lastLogin = lastLogin
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.phoneNumber = phoneNumber
        self.invitedBy = invitedBy
        self.photo = photo
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.isActive = isActive
        self.isAdmin = isAdmin
    }
}
