//
//  AddedToListResponse.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 10/28/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//
//   let addedToListResponse = try? newJSONDecoder().decode(AddedToListResponse.self, from: jsonData)

import Foundation

// MARK: - AddedToListResponse
class AddedToListResponse: Codable {
    let status: Bool?
    let data: ListedProperty?
    let message: String?

    init(status: Bool?, data: ListedProperty?, message: String?) {
        self.status = status
        self.data = data
        self.message = message
    }
}

// MARK: - DataClass
class ListedProperty: Codable {
   var id, userList: Int?
    let street, city, state, zip: String?
    let cadAcct: String?
    let gmaTag: Int?
    let latitude, longitude: Double?
    let propertyTags: [PropertyTag]?
    let ownerInfo: [OwnerInfo]?
    let photoCount, noteCount: Int?
    let createdAt, updatedAt: String?
    let powerTraceRequestID: Int?
    let history: Int?


    enum CodingKeys: String, CodingKey {
        case id
        case userList = "user_list"
        case street, city, state, zip
        case cadAcct = "cad_acct"
        case gmaTag = "gma_tag"
        case latitude, longitude
        case propertyTags = "property_tags"
        case ownerInfo = "owner_info"
        case powerTraceRequestID = "power_trace_request_id"
        case photoCount = "photo_count"
        case noteCount = "note_count"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case history = "history"
    }

    init(id: Int?, userList: Int?, street: String?, city: String?, state: String?, zip: String?, cadAcct: String?, gmaTag: Int?, latitude: Double?, longitude: Double?, propertyTags: [PropertyTag]?, ownerInfo: [OwnerInfo]?, photoCount: Int?, noteCount: Int?, createdAt: String?, updatedAt: String?, powerTraceRequestID: Int, history: Int?) {
        self.id = id
        self.userList = userList
        self.street = street
        self.city = city
        self.state = state
        self.zip = zip
        self.cadAcct = cadAcct
        self.gmaTag = gmaTag
        self.latitude = latitude
        self.longitude = longitude
        self.propertyTags = propertyTags
        self.ownerInfo = ownerInfo
        self.photoCount = photoCount
        self.noteCount = noteCount
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.powerTraceRequestID = powerTraceRequestID
        self.history = history

    }
}

//class PropertyTag: Codable {
//    let id: Int?
//
//    init(id: Int?) {
//        self.id = id
//    }
//}
