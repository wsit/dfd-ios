//
//  MailWizardSubscriptionTypeResponse.swift
//  HouZes
//
//  Created by Mahadhi Hassan Chowdhury on 1/20/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//
// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let mailWizardSubcriptionTypeResponse = try? newJSONDecoder().decode(MailWizardSubcriptionTypeResponse.self, from: jsonData)

import Foundation

// MARK: - MailWizardSubcriptionTypeResponseElement
class MailWizardSubcriptionTypeResponseElement: Codable {
    let id: Int?
    let typeName: String?
    let daysInterval: Int?
    let createdAt, updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case id
        case typeName = "type_name"
        case daysInterval = "days_interval"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }

    init(id: Int?, typeName: String?, daysInterval: Int?, createdAt: String?, updatedAt: String?) {
        self.id = id
        self.typeName = typeName
        self.daysInterval = daysInterval
        self.createdAt = createdAt
        self.updatedAt = updatedAt
    }
}

typealias MailWizardSubcriptionTypeResponse = [MailWizardSubcriptionTypeResponseElement]
