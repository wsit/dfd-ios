//
//  RechargeCardResponse.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 1/14/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//

//   let rechargeCardResponse = try? newJSONDecoder().decode(RechargeCardResponse.self, from: jsonData)

import Foundation

// MARK: - RechargeCardResponse
class RechargeCardResponse: Codable {
    let status: Bool?
    let data: RechargeInfo?
    let message: String?

    init(status: Bool?, data: RechargeInfo?, message: String?) {
        self.status = status
        self.data = data
        self.message = message
    }
}

// MARK: - DataClass
class RechargeInfo: Codable {
    let isTeamInvitable: Bool?
    let amount, powerTrace: Double?
    let ownerInfo: Double?
    let mailerWizard: Double?
    let plan: Plan?

    enum CodingKeys: String, CodingKey {
        case isTeamInvitable = "is_team_invitable"
        case amount
        case powerTrace = "power_trace"
        case ownerInfo = "owner_info"
        case mailerWizard = "mailer_wizard"
        case plan
    }

    init(isTeamInvitable: Bool?, amount: Double?, powerTrace: Double?, ownerInfo: Double?, mailerWizard: Double?, plan: Plan?) {
        self.isTeamInvitable = isTeamInvitable
        self.amount = amount
        self.powerTrace = powerTrace
        self.ownerInfo = ownerInfo
        self.mailerWizard = mailerWizard
        self.plan = plan
    }
}
//
//// MARK: - Plan
//class Plan: Codable {
//    let id: Int?
//    let planName: String?
//    let planCost, planCoin, createdBy: Int?
//    let updatedBy: JSONNull?
//    let planDescription: String?
//    let image: JSONNull?
//    let createdAt, updatedAt: String?
//
//    enum CodingKeys: String, CodingKey {
//        case id
//        case planName = "plan_name"
//        case planCost = "plan_cost"
//        case planCoin = "plan_coin"
//        case createdBy = "created_by"
//        case updatedBy = "updated_by"
//        case planDescription = "description"
//        case image
//        case createdAt = "created_at"
//        case updatedAt = "updated_at"
//    }
//
//    init(id: Int?, planName: String?, planCost: Int?, planCoin: Int?, createdBy: Int?, updatedBy: JSONNull?, planDescription: String?, image: JSONNull?, createdAt: String?, updatedAt: String?) {
//        self.id = id
//        self.planName = planName
//        self.planCost = planCost
//        self.planCoin = planCoin
//        self.createdBy = createdBy
//        self.updatedBy = updatedBy
//        self.planDescription = planDescription
//        self.image = image
//        self.createdAt = createdAt
//        self.updatedAt = updatedAt
//    }
//}
