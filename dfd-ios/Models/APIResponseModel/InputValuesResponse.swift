//
//  InputValuesResponse.swift
//  HouZes
//
//  Created by Mahadhi Hassan Chowdhury on 2/3/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//

 //   let inputValuesResponse = try? newJSONDecoder().decode(InputValuesResponse.self, from: jsonData)

import Foundation

// MARK: - InputValuesResponse
class InputValuesResponse : Codable {
    var status: Bool?
    var data: InputData?
    var message: String?

    init(status: Bool?, data: InputData?, message: String?) {
        self.status = status
        self.data = data
        self.message = message
    }
}

// MARK: - DataClass
class InputData: Codable {
    var id: Int?
    var firstName, lastName, email: String?
    var companyName, website, phoneNo: String?
    var addressStreet, addressCity, addressState, addressZip: String?
    var createdAt, updatedAt: String?
    var user: Int?
    var logo, coverPhoto, agentLicense: String?

    enum CodingKeys: String, CodingKey {
        case id
        case firstName = "first_name"
        case lastName = "last_name"
        case email
        case companyName = "company_name"
        case website
        case phoneNo = "phone_no"
        case addressStreet = "address_street"
        case addressCity = "address_city"
        case addressState = "address_state"
        case addressZip = "address_zip"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case user
        case logo
        case coverPhoto = "cover_photo"
        case agentLicense = "agent_license"
 
        
    }

    init(id: Int?, firstName: String?, lastName: String?, email: String?, companyName: String?, website: String?, phoneNo: String?, addressStreet: String?, addressCity: String?, addressState: String?, addressZip: String?, createdAt: String?, updatedAt: String?, user: Int?, logo: String?, coverPhoto:String?, agentLicense: String?) {
        self.id = id
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.companyName = companyName
        self.website = website
        self.phoneNo = phoneNo
        self.addressStreet = addressStreet
        self.addressCity = addressCity
        self.addressState = addressState
        self.addressZip = addressZip
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.user = user
        self.logo = logo
        self.coverPhoto = coverPhoto
        self.agentLicense = agentLicense
    }
}
