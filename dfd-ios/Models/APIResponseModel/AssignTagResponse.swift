//
//  AssignTagResponse.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 10/29/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

//   let assignTagResponse = try? newJSONDecoder().decode(AssignTagResponse.self, from: jsonData)

import Foundation

// MARK: - AssignTagResponse
class AssignTagResponse: Codable {
    let status: Bool?
    let data: TaggedProperty?
    let message: String?

    init(status: Bool?, data: TaggedProperty?, message: String?) {
        self.status = status
        self.data = data
        self.message = message
    }
}

// MARK: - DataClass
class TaggedProperty: Codable {
    let id, userList: Int?
    let street, city, state, zip: String?
    let cadAcct: String?
    let gmaTag: Int?
    let latitude, longitude: Double?
    let propertyTags: [PropertyTag]?
    let photoCount, noteCount: Int?
    let createdAt, updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case id
        case userList = "user_list"
        case street, city, state, zip
        case cadAcct = "cad_acct"
        case gmaTag = "gma_tag"
        case latitude, longitude
        case propertyTags = "property_tags"
        case photoCount = "photo_count"
        case noteCount = "note_count"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }

    init(id: Int?, userList: Int?, street: String?, city: String?, state: String?, zip: String?, cadAcct: String?, gmaTag: Int?, latitude: Double?, longitude: Double?, propertyTags: [PropertyTag]?, photoCount: Int?, noteCount: Int?, createdAt: String?, updatedAt: String?) {
        self.id = id
        self.userList = userList
        self.street = street
        self.city = city
        self.state = state
        self.zip = zip
        self.cadAcct = cadAcct
        self.gmaTag = gmaTag
        self.latitude = latitude
        self.longitude = longitude
        self.propertyTags = propertyTags
        self.photoCount = photoCount
        self.noteCount = noteCount
        self.createdAt = createdAt
        self.updatedAt = updatedAt
    }
}
