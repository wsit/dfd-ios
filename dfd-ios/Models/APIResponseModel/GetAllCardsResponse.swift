//
//  GetAllCardsResponse.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 1/14/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//

//   let allCardsResponse = try? newJSONDecoder().decode(AllCardsResponse.self, from: jsonData)

import Foundation

// MARK: - AllCardsResponseElement
class AllCardsResponseElement: Codable {
    let id: Int?
    let cardName: String?
    let cardNumber, cardCode, expDate: String?
    let isSave: Bool?
    let createdAt, updatedAt: String?
    let user: Int?

    enum CodingKeys: String, CodingKey {
        case id
        case cardName = "card_name"
        case cardNumber = "card_number"
        case cardCode = "card_code"
        case expDate = "exp_date"
        case isSave = "is_save"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case user
    }

    init(id: Int?, cardName: String?, cardNumber: String?, cardCode: String?, expDate: String?, isSave: Bool?, createdAt: String?, updatedAt: String?, user: Int?) {
        self.id = id
        self.cardName = cardName
        self.cardNumber = cardNumber
        self.cardCode = cardCode
        self.expDate = expDate
        self.isSave = isSave
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.user = user
    }
}

typealias AllCardsResponse = [AllCardsResponseElement]
