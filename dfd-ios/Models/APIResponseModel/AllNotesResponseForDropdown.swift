//
//  AllNotesResponseForDropdown.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 9/17/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

   //let allNotesResponseForDropdown = try? newJSONDecoder().decode(AllNotesResponseForDropdown.self, from: jsonData)

import Foundation

// MARK: - AllNotesResponseForDropdownElement
class AllListsResponseForDropdownElement: Codable {
    let id: Int?
    let name: String?
    let leadsCount: Int?
    let createdAt, updatedAt: String?
    let user: Int?
    
    enum CodingKeys: String, CodingKey {
        case id, name
        case leadsCount = "leads_count"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case user
    }
    
    init(id: Int?, name: String?, leadsCount: Int?, createdAt: String?, updatedAt: String?, user: Int?) {
        self.id = id
        self.name = name
        self.leadsCount = leadsCount
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.user = user
    }
}

typealias AllListsResponseForDropdown = [MList]
