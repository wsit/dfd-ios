//
//  AddPropertyResponse.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 9/3/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

//   let addPropertyResponse = try? newJSONDecoder().decode(AddPropertyResponse.self, from: jsonData)

import Foundation

// MARK: - AddPropertyResponse
class AddPropertyResponse: Codable {
    let id: Int?
    let cadAcct, gmaTag, propertyAddress: String?
    let ownerInfo: OwnerInfo?
    let googlePlaceID, createdAt: String?
    let  lat, lon: Double?
    let updatedAt: String?
    let propertyTags: [String]?
    
    enum CodingKeys: String, CodingKey {
        case id
        case cadAcct = "cad_acct"
        case gmaTag = "gma_tag"
        case propertyAddress = "property_address"
        case ownerInfo = "owner_info"
        case googlePlaceID = "google_place_id"
        case lat, lon
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case propertyTags = "property_tags"
    }
    
    init(id: Int?, cadAcct: String?, gmaTag: String?, propertyAddress: String?, ownerInfo: OwnerInfo?, googlePlaceID: String?, lat: Double?, lon: Double?, createdAt: String?, updatedAt: String?, propertyTags: [String]?) {
        self.id = id
        self.cadAcct = cadAcct
        self.gmaTag = gmaTag
        self.propertyAddress = propertyAddress
        self.ownerInfo = ownerInfo
        self.googlePlaceID = googlePlaceID
        self.lat = lat
        self.lon = lon
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.propertyTags = propertyTags
    }
}

// MARK: - OwnerInfo
//class OwnerInfo: Codable {
//    let ownerName, ownerAddress: String?
//    
//    init(ownerName: String?, ownerAddress: String?) {
//        self.ownerName = ownerName
//        self.ownerAddress = ownerAddress
//    }
//}
