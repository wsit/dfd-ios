//
//  MyScoutResponse.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 8/27/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

//   let myScoutResponse = try? newJSONDecoder().decode(MyScoutResponse.self, from: jsonData)

import Foundation

// MARK: - MyScoutResponse
class MyScoutResponse: Codable {
    let count: Int?
    let next, previous: String?
    let results: [Scout]?
    
    init(count: Int?, next: String?, previous: String?, results: [Scout]?) {
        self.count = count
        self.next = next
        self.previous = previous
        self.results = results
    }
}

// MARK: - Result
class Scout: Codable {
    let id: Int?
    let firstName, lastName: String?
    let url: String?
    let createdAt, updatedAt: String?
    let managerID: Int?
    
    enum CodingKeys: String, CodingKey {
        case id
        case firstName = "first_name"
        case lastName = "last_name"
        case url
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case managerID = "manager_id"
    }
    
    init(id: Int?, firstName: String?, lastName: String?, url: String?, createdAt: String?, updatedAt: String?, managerID: Int?) {
        self.id = id
        self.firstName = firstName
        self.lastName = lastName
        self.url = url
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.managerID = managerID
    }
}
