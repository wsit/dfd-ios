//
//  LoginResponse.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 5/24/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let loginResponse = try? newJSONDecoder().decode(LoginResponse.self, from: jsonData)

import Foundation
import UIKit


//   let loginResponse = try? newJSONDecoder().decode(LoginResponse.self, from: jsonData)
// MARK: - LoginResponse
class LoginResponse: Codable {
    let accessToken: String?
    let expiresIn: Int?
    let tokenType, scope, refreshToken: String?
    let error, errorDescription: String?

    enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
        case expiresIn = "expires_in"
        case tokenType = "token_type"
        case scope
        case refreshToken = "refresh_token"
        case error
        case errorDescription = "error_description"
    }
    
    init(accessToken: String?, expiresIn: Int?, tokenType: String?, scope: String?, refreshToken: String?,error: String?, errorDescription: String?) {
        self.accessToken = accessToken
        self.expiresIn = expiresIn
        self.tokenType = tokenType
        self.scope = scope
        self.refreshToken = refreshToken
        self.error = error
        self.errorDescription = errorDescription
    }
}

//
//// MARK: - LoginResponse
//class LoginResponse: Codable {
//    let statusCode: Int?
//    let message: String?
//    let data: DataClass?
//
//    init(statusCode: Int?, message: String?, data: DataClass?) {
//        self.statusCode = statusCode
//        self.message = message
//        self.data = data
//    }
//}
//
//// MARK: - DataClass
//class DataClass: Codable {
//    let client: Client?
//    let user: User?
//    let accessToken, refreshToken, accessTokenExpiresAt, refreshTokenExpiresAt: String?
//    let scope: Bool?
//
//    enum CodingKeys: String, CodingKey {
//        case client, user
//        case accessToken = "access_token"
//        case refreshToken = "refresh_token"
//        case accessTokenExpiresAt, refreshTokenExpiresAt, scope
//    }
//
//    init(client: Client?, user: User?, accessToken: String?, refreshToken: String?, accessTokenExpiresAt: String?, refreshTokenExpiresAt: String?, scope: Bool?) {
//        self.client = client
//        self.user = user
//        self.accessToken = accessToken
//        self.refreshToken = refreshToken
//        self.accessTokenExpiresAt = accessTokenExpiresAt
//        self.refreshTokenExpiresAt = refreshTokenExpiresAt
//        self.scope = scope
//    }
//}
//
//// MARK: - Client
//class Client: Codable {
//    let id: Int?
//    let clientID, scope: String?
//    let grants: [String]?
//    let redirectUris: [JSONNull?]?
//
//    enum CodingKeys: String, CodingKey {
//        case id
//        case clientID = "client_id"
//        case scope, grants, redirectUris
//    }
//
//    init(id: Int?, clientID: String?, scope: String?, grants: [String]?, redirectUris: [JSONNull?]?) {
//        self.id = id
//        self.clientID = clientID
//        self.scope = scope
//        self.grants = grants
//        self.redirectUris = redirectUris
//    }
//}
//
//// MARK: - User
//class User: Codable {
//    let id: Int?
//    let email, password, scope: String?
//
//    init(id: Int?, email: String?, password: String?, scope: String?) {
//        self.id = id
//        self.email = email
//        self.password = password
//        self.scope = scope
//    }
//}
//
//// MARK: - Encode/decode helpers
//
//class JSONNull: Codable, Hashable {
//
//    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
//        return true
//    }
//
//    public var hashValue: Int {
//        return 0
//    }
//
//    public init() {}
//
//    public required init(from decoder: Decoder) throws {
//        let container = try decoder.singleValueContainer()
//        if !container.decodeNil() {
//            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
//        }
//    }
//
//    public func encode(to encoder: Encoder) throws {
//        var container = encoder.singleValueContainer()
//        try container.encodeNil()
//    }
//}
