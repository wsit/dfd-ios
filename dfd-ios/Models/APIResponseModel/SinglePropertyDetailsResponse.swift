//
//  SinglePropertyDetailsResponse.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 10/29/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

//   let singlePropertyDetailsResponse = try? newJSONDecoder().decode(SinglePropertyDetailsResponse.self, from: jsonData)

import Foundation

// MARK: - SinglePropertyDetailsResponse
class SinglePropertyDetailsResponse: Codable {
    let id, userList: Int?
    let street, city, state, zip: String?
    let cadAcct: String?
    let gmaTag: Int?
    let latitude, longitude: Double?
    let propertyTags: [PropertyTagWithColor]?
    let ownerInfo: [OwnerInfo]?
    let photos: [PropertyPhoto]?
    let notes: [ProperyNote]?
    let createdAt, updatedAt: String?
    let powerTraceRequestID:Int?

    enum CodingKeys: String, CodingKey {
        case id
        case userList = "user_list"
        case street, city, state, zip
        case cadAcct = "cad_acct"
        case gmaTag = "gma_tag"
        case latitude, longitude
        case propertyTags = "property_tags"
        case ownerInfo = "owner_info"
        case photos, notes
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case powerTraceRequestID = "power_trace_request_id"

    }

    init(id: Int?, userList: Int?, street: String?, city: String?, state: String?, zip: String?, cadAcct: String?, gmaTag: Int?, latitude: Double?, longitude: Double?, propertyTags: [PropertyTagWithColor]?, ownerInfo: [OwnerInfo]?, photos: [PropertyPhoto]?, notes: [ProperyNote]?, createdAt: String?, updatedAt: String?, powerTraceRequestID:Int?) {
        self.id = id
        self.userList = userList
        self.street = street
        self.city = city
        self.state = state
        self.zip = zip
        self.cadAcct = cadAcct
        self.gmaTag = gmaTag
        self.latitude = latitude
        self.longitude = longitude
        self.propertyTags = propertyTags
        self.ownerInfo = ownerInfo
        self.photos = photos
        self.notes = notes
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.powerTraceRequestID = powerTraceRequestID
    }
}

// MARK: - Note
class ProperyNote: Codable {
    let id: Int?
    let title, notes, createdAt, updatedAt: String?
    let user, property: Int?

    enum CodingKeys: String, CodingKey {
        case id, title, notes
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case user, property
    }

    init(id: Int?, title: String?, notes: String?, createdAt: String?, updatedAt: String?, user: Int?, property: Int?) {
        self.id = id
        self.title = title
        self.notes = notes
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.user = user
        self.property = property
    }
}

//// MARK: - Photo
//class Photo: Codable {
//    let id: Int?
//    let photoURL, thumbPhotoURL: String?
//    let createdAt, updatedAt: String?
//    let user, property: Int?
//
//    enum CodingKeys: String, CodingKey {
//        case id
//        case photoURL = "photo_url"
//        case thumbPhotoURL = "thumb_photo_url"
//        case createdAt = "created_at"
//        case updatedAt = "updated_at"
//        case user, property
//    }
//
//    init(id: Int?, photoURL: String?, thumbPhotoURL: String?, createdAt: String?, updatedAt: String?, user: Int?, property: Int?) {
//        self.id = id
//        self.photoURL = photoURL
//        self.thumbPhotoURL = thumbPhotoURL
//        self.createdAt = createdAt
//        self.updatedAt = updatedAt
//        self.user = user
//        self.property = property
//    }
//}

// MARK: - PropertyTag
class PropertyTagWithColor: Codable {
    let id: Int?
    let name: String?
    let color: Color?
    let user: Int?
    let createdAt, updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case id, name, color, user
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }

    init(id: Int?, name: String?, color: Color?, user: Int?, createdAt: String?, updatedAt: String?) {
        self.id = id
        self.name = name
        self.color = color
        self.user = user
        self.createdAt = createdAt
        self.updatedAt = updatedAt
    }
}

// MARK: - Color
class Color: Codable {
    let colorName, colorCode: String?

    enum CodingKeys: String, CodingKey {
        case colorName = "color_name"
        case colorCode = "color_code"
    }

    init(colorName: String?, colorCode: String?) {
        self.colorName = colorName
        self.colorCode = colorCode
    }
}
