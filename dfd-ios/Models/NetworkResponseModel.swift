//
//  NetworkResponseModel.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 5/23/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import Foundation

struct NetworkResponseModel : Codable {
   
    let statusCode : Int?
    let message : String?
    let data : Data?
    
    enum CodingKeys: String, CodingKey {
        
        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(Data.self, forKey: .data)
    }
    
}
/*
 Using Codeable Mapping
 let url = URL(string: "http://www.stackoverflow.com")
 let task = URLSession.shared.dataTask(with: url!) { (data, response, error) in
 let jsonDecoder = JSONDecoder()
 let responseModel = try jsonDecoder.decode(Json4Swift_Base.self, from: data!)
 
 }
 task.resume()
 
 Using ObjectMapper
 // Convert JSON String to Model
 let responseModel = Mapper<Json4Swift_Base>().map(JSONString: JSONString)
 
 // Create JSON String from Model
 let JSONString = Mapper().toJSONString(responseModel, prettyPrint: true)
 
 Using Dictionary Mapping
 let url = URL(string: "http://www.json4swift.com")
 let task = URLSession.shared.dataTask(with: url!) { (data, response, error) in
 
 let someDictionaryFromJSON = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String: Any]
 let json4Swift_Base = Json4Swift_Base(someDictionaryFromJSON)
 
 }
 task.resume()
 */
