//
//  LocationWithIndexPosition.swift
//  HouZes
//
//  Created by Mahadhi Hassan Chowdhury on 3/11/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//

import Foundation

class LocationWithIndexPosition: NSObject {
    
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    var position: Int = 0
     
}



extension Sequence {
    func group<U: Hashable>(by key: (Iterator.Element) -> U) -> [U:[Iterator.Element]] {
        return Dictionary.init(grouping: self, by: key)
    }
}
