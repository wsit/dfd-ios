//
//  SelectedProperty.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 8/6/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import Foundation

class SelectedProperty: NSObject {
    
    var propertyAddress: String = ""
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    var propertyID: Int = 0
    var street: String = ""
    var city : String = ""
    var state: String = ""
    var zip: String = ""
    var country :String = ""
}

//   let place = try? newJSONDecoder().decode(Place.self, from: jsonData)


// MARK: - Place
class TappedPlace: NSObject {
    var fullAddress: String = ""
    var street: String = ""
    var city : String = ""
    var state: String = ""
    var zip: String = ""
    var cadAcct: String = ""
    var gmaTag: Int = 0
    var latitude : Double = 0.0
    var longitude: Double = 0.0
    var country :String = ""
    
}
