//
//  MailWizardInputFields.swift
//  HouZes
//
//  Created by Mahadhi Hassan Chowdhury on 2/3/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//

 //   let mailWizardInputField = try? newJSONDecoder().decode(MailWizardInputField.self, from: jsonData)

import Foundation

// MARK: - MailWizardInputField
class MailWizardInputField: NSObject {
    var first_name, last_name, email,phone_no, address_street: String?
    var address_city, address_state: String?
    var address_zip: Int?
    var agent_license_number, website, company_name : String?

    
}
 
