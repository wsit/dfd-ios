//
//  UISegmentedControl.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 11/25/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//
import UIKit
import Foundation
class SquareSegmentedControl: UISegmentedControl {
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = 0
    }
}
