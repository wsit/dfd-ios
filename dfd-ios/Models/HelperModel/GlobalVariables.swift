//
//  GlobalVariables.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 10/24/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import Foundation
import GoogleMaps

//global variables for driving

var USA_LAT : Double = 40.710704
var USA_LONG : Double = -74.005972

var activeUserIDs: [Int] = []
var users: [MarkerDetails] = []
var userDict : [[Int:MarkerDetails]] = []
var markerArray : [GMSMarker] = []
var routeLocationsAfterStop: [CLLocation] = []

var totalCoveredDistance: Double = 0.0
var drivingDetails: DrivingInfo?
var globalDriveType : String = ""

//

var globalDrivingInfo : DrivingInfo?
var globalSelectedProperty = SelectedProperty()
var globalTappedPlace = TappedPlace()
var globalSelectedPropertyFromList : ListedProperty? //property after added to list

var globalPropertyList: [ListedProperty] = []
var globalPropertyListForMap: [ListClusterResponseElement] = []


var globalSelectedHistory: MyDrive?
var globalIsListedProperty : Bool = false
var globalIsPropertyDetailsAvailable : Bool = false
var globalLastAddedListName : String = ""
var globalSelectedList : MList?

var globalIsFilterSelected: Bool = false
var globalSelectedMembersID : [Int] = []
var globalAllTags: [PropertyTag] = []

var isUpgradePopUpShowed = false
var isResumeGlobal: Bool = false
var counterForProjection : Int = 0

var noOfTaggedProperty: Int = 0
var totalMiles: Double = 0.0
var duration: Double = 0.0
var isGlobalDriveNowSelected: Bool = false
var isGlobalPauseSelected: Bool = false
var myLastLocationGlobal: CLLocation?

var globalMapType: Int = 1
//---------------------------Start Variables for handle Timer in Home---------
var seconds = 0 //This variable will hold a starting value of seconds. It could be any amount above 0.
var timer = Timer()
var isTimerRunning = false //This will be used to make sure only one timer is created at a time.
var resumeTapped = false
//---------------------------End Variables for handle Timer in Home---------

var routeLocations: [CLLocation] = []

//---------------------------Start Variables for handle pause resume---------
var globalAllPaths: [GMSMutablePath] = []
var currentPauseIndex: Int = 0
var routeLocationsWithIndex: [LocationWithIndexPosition] = []
var routeLocationPaths: [GMSMutablePath] = []


//---------------------------End Variables for handle pause resume---------


struct Asset {
    let coin: String
    let amount: Int
}

let assets = [
    Asset(coin: "BTC", amount: 12),
    Asset(coin: "ETH", amount: 15),
    Asset(coin: "BTC", amount: 30),
]
