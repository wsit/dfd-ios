//
//  NearbyUsersLocationResponseFromSocket.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 7/10/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//
//   let nearbyUsersLocationResponseFromSocket = try? newJSONDecoder().decode(NearbyUsersLocationResponseFromSocket.self, from: jsonData)

import Foundation

// MARK: - NearbyUsersLocationResponseFromSocketElement
class NearbyUserLocationResponseFromSocket: NSObject, Codable {
    let createdAt, nearbyUsersLocationResponseFromSocketCreatedAt: String?
    let id, isDriving: Int?
    let latitude, longitude, updatedAt, nearbyUsersLocationResponseFromSocketUpdatedAt: String?
    let userID: Int?
    
    enum CodingKeys: String, CodingKey {
        case createdAt
        case nearbyUsersLocationResponseFromSocketCreatedAt = "created_at"
        case id
        case isDriving = "is_driving"
        case latitude, longitude, updatedAt
        case nearbyUsersLocationResponseFromSocketUpdatedAt = "updated_at"
        case userID = "user_id"
    }
    
    init(createdAt: String?, nearbyUsersLocationResponseFromSocketCreatedAt: String?, id: Int?, isDriving: Int?, latitude: String?, longitude: String?, updatedAt: String?, nearbyUsersLocationResponseFromSocketUpdatedAt: String?, userID: Int?) {
        self.createdAt = createdAt
        self.nearbyUsersLocationResponseFromSocketCreatedAt = nearbyUsersLocationResponseFromSocketCreatedAt
        self.id = id
        self.isDriving = isDriving
        self.latitude = latitude
        self.longitude = longitude
        self.updatedAt = updatedAt
        self.nearbyUsersLocationResponseFromSocketUpdatedAt = nearbyUsersLocationResponseFromSocketUpdatedAt
        self.userID = userID
    }
}

//typealias NearbyUsers = [NearbyUserLocationResponseFromSocket]
