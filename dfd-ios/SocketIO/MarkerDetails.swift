//
//  MarkerDetails.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 7/11/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import Foundation
import GoogleMaps
class MarkerDetails: NSObject {
    
    var id : Int = 0
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    var angle: String = ""
    
}
