//
//  SocketIOManager.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 7/9/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import Foundation
import SocketIO
import GoogleMaps

protocol LocationReceiverDelegate {
    func didReceiveResponse(nearByUser: [String: Any])
}

protocol UserDisconnectedDelegate {
    func didDisconnectedAUser(disconnectedUserID: Int)
}

let manager = SocketManager(socketURL: URL(string: "https://socket.houzes.com")!, config: [.log(true),  .connectParams(["token" : AppConfigs.getCurrentUserInfo().id ?? 0])])
//let manager = SocketManager(socketURL: URL(string: "https://socket-dev.ratoolkit.com")!, config: [.log(true), .reconnectAttempts(5),   .connectParams(["token" : AppConfigs.getCurrentUserInfo().id ?? 0])])

//https://192.168.1.8:3000
//"https://socket-dev.ratoolkit.com"
class SocketIOManager: NSObject {
    
    var locationReceiverDelegate: LocationReceiverDelegate?
    var userDisconnectedDelegate : UserDisconnectedDelegate?
    
    static let shared = SocketIOManager()
    
    //"https://socket-dev.ratoolkit.com"  http://192.168.1.51:3000
    
    var socket : SocketIOClient!
    
    var socketConnectionStatus : SocketIOStatus?
    
    
    override init() {
        super.init()
        
        socket = manager.defaultSocket
        socketConnectionStatus = socket.status
        
        //NotificationCenter.default.addObserver(self, selector: #selector(self.checkSocketConnectionStatus(notification:)), name: Notification.Name("socketConnectionStatus"), object: nil)
        
        self.connectSocket() 
    }
    
    
    @objc func checkSocketConnectionStatus(notification: Notification) {
        //print("Value of notification : ", notification.object ?? "")
        
       // print("checkSocketConnectionStatus")
        
        let socketConnectionStatus = socket.status
        
        switch socketConnectionStatus {
        case SocketIOStatus.connected:
            print("socket connected")
        case SocketIOStatus.connecting:
            print("socket connecting")
        case SocketIOStatus.disconnected:
            print("socket disconnected")
            socket.connect()
        case SocketIOStatus.notConnected:
            print("socket not connected")
            socket.connect()
        }
    }
    
    func connectSocket() {
        
        //        socket.connect(timeoutAfter: 120.0) {
        //            print("socket connection timeout")
        //
        //        }
        
        socket.on(clientEvent: .connect) {data, ack in
           // print(data)
            print("socket connected")
        }
        
        
        
        self.socket.on("connected") { ( dataArray, ack)  in
            print("dataArray",dataArray)
            
            NotificationCenter.default.post(name:NSNotification.Name("socketUserConnectedWithValidToken"),object: nil)
            
        }
        
        self.socket.on("user::disconnected") { ( data, ack)  in
            print("user::disconnected",data[0])
            
            let id = data[0] as! String
            var idInt : Int = 0
            
            print("user::disconnected", id)
            if id is String {
                print("String type")
                idInt = Int(id) ?? 0
            }
            
            print(idInt)
            if idInt > 0{
                self.userDisconnectedDelegate?.didDisconnectedAUser(disconnectedUserID: idInt )
            }
            //            let disconnectedData = data as! Array<Dictionary<String, Any>>
            //            let data = data[0]
            //            print("user::disconnected", data)
            //            print("user::disconnected.count-->", data.count)
            //            self.userDisconnectedDelegate?.didDisconnectedAUser(disconnectedUser: data)
        }
        
        self.socket.on("error") { (data, ack) in
            print("error-->", data)
            print(self.socket.status)
        }
        
        self.socket.on("location::share.error") { (data, ack) in
          //  print("location::share.error-->", data)
        }
        
        self.socket.on("location::share.success") { (data, ack) in
           // print("location::share.success", data[0])
        }
        
        self.socket.on("location::receive") { (data, ack) in
         //   print(data)
            
            let receivedData = data as! Array<Dictionary<String, Any>>
           // print("receivedData---->", receivedData)
            let data = receivedData[0]
            
           // print("location::receive", data)
            
          //  print("nearByUsers?.count-->", data.count)
            
            self.locationReceiverDelegate?.didReceiveResponse(nearByUser: data)
            
        }
        
        self.socket.on("location::receive\(AppConfigs.getCurrentUserInfo().invitedBy ?? AppConfigs.getCurrentUserInfo().id ?? 0)") { (data, ack) in
            print(data)
            
            let receivedData = data as! Array<Dictionary<String, Any>>
          //  print("receivedData---->", receivedData)
            let data = receivedData[0]
          //  print("location::receive", data)
         //   print("nearByUsers?.count-->", data.count)
            self.locationReceiverDelegate?.didReceiveResponse(nearByUser: data)
            
        }
    }
    
    func establishConnection() {
        print("in connect socket")
        socket.connect()
    }
    
    func disconnectSocket() {
        socket.disconnect()
        print("in disconnect socket")
    }
    
    func stoppedDriving() {
        socket.emit("driver::leave")
    }
    
    
    func updateCurrentLocation(myLocation: CLLocation, driveType:String) {
        
        if driveType == "currentLocation"{
//            print(myLocation.coordinate.latitude)
//            print(myLocation.coordinate.longitude)
//            print(myLocation.course)
//            print(AppConfigs.getSavedAccessToken())
            
            let data : Dictionary<String,Any> = [
                "latitude": myLocation.coordinate.latitude,
                "longitude":  myLocation.coordinate.longitude,
                "angle": myLocation.course,
                "is_driving" : true
            ]
           // print(data)
            
            socket.emit("location::update", data)
        }else{
            
//            print(myLocation.coordinate.latitude)
//            print(myLocation.coordinate.longitude)
//            print(myLocation.course)
//            print(AppConfigs.getSavedAccessToken())
            
            var invitedBy: Int = 0
            if AppConfigs.getCurrentUserInfo().invitedBy == nil || invitedBy == 0{
                invitedBy = AppConfigs.getCurrentUserInfo().id ?? 0
               // print(invitedBy)
            }else{
              //  print(invitedBy)
            }
            
            if AppConfigs.getCurrentUserInfo().id != nil{
                let data : Dictionary<String,Any> = [
                    "latitude": myLocation.coordinate.latitude,
                    "longitude":  myLocation.coordinate.longitude,
                    "angle": myLocation.course,
                    "is_driving" : true,
                    "drive_type" : driveType,
                    "user_id" : AppConfigs.getCurrentUserInfo().id ?? 0,
                    "parent" : invitedBy
                ]
              //  print(data)
                
                //        let jsonData = try! JSONSerialization.data(withJSONObject: data)
                //        print(jsonData)
                //        let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)
                //        print((String(describing: jsonString!)))
                //socket.emit("location::share", "\(String(describing: jsonString!))")
                
                socket.emit("location::share", data)
            }else{
                print("Current User id nil")
            }
            
        }
        
    } 
}
