//
//  NearbyByUser.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 7/10/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import Foundation
class NearByUser {
    var createdAt: String = ""
    var nearbyUsersLocationResponseFromSocketCreatedAt: String = ""
    var id : Int = 0
    var isDriving: Int = 0
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    var updatedAt: String = ""
    var nearbyUsersLocationResponseFromSocketUpdatedAt: String = ""
    var userID: Int = 0
    var angle: String = ""
    
    
}
/*
 {
 "id":11,
 "user_id":4,
 "latitude":23.83261263370514,
 "longitude":90.41737854480743,
 "is_driving":true,
 "angle":50,
 "created_at":"2019-07-10T13:02:55.925Z",
 "updated_at":"2019-07-11T10:30:31.382Z",
 "createdAt":"2019-07-10T13:02:55.925Z",
 "updatedAt":"2019-07-11T10:30:51.119Z",
 "user":{
 "id":4,
 "email":"vikram@workspaceit.com",
 "first_name":"Pronob",
 "last_name":"Halder",
 "phone_number":"123"
 }
 }
 */
