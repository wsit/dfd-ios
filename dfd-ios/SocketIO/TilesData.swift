//
//  TilesData.swift
//  HouZes
//
//  Created by Mahadhi Hassan Chowdhury on 2/11/20.
//  Copyright © 2020 Workspace IT. All rights reserved.
//

import Foundation

class TilesData: NSObject, Codable {
    
    var seconds : Int = 0
    var distanceInMile: Double = 0.0
    var noOfPropertiesAddred: Int =  0
    
}
