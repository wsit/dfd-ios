//
//  ViewController.swift
//  dfd-ios
//
//  Created by Mahadhi Hassan Chowdhury on 5/1/19.
//  Copyright © 2019 Workspace IT. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit
import Alamofire
import SwiftyJSON
import AuthenticationServices

class WelcomeScreenViewController: UIViewController  , GIDSignInDelegate, ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding{
    
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
                return self.view.window!

    }
    
    
    @IBOutlet weak var btnFbLogin: UIButton!
    //@IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var signInButton: GIDSignInButton!
    @IBOutlet weak var btnEmail: UIButton!
    @IBOutlet weak var btnGoogle: UIButton!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblSubTitle: UILabel!
    //@IBOutlet weak var btnNewSignIn: UIButton!
    @IBOutlet weak var AppleSignInView: UIStackView!

    let img1 = UIImage(named: "11")
    let img2 = UIImage(named: "22")
    let img3 = UIImage(named: "33")
    
    var index = 0
    let animationDuration: TimeInterval = 0.25
    let switchingInterval: TimeInterval = 3
    
    
    var imageArray = [UIImage]()   //Uimage(named: "1"), Uimage(named: "2"), Uimage(named: "3")
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageArray = [img1!, img2!, img3!]
        animateImageView()
        
        self.setUpNavigationBar()
        
        FontAndColorConfigs.setDynamicButtonsFontSize(buttons: [btnFbLogin, btnEmail, btnGoogle], type: TextType.ListTitle.rawValue)
        //self.btnNewSignIn.backgroundColor = self.buttonBackgroundColor
        
        btnFbLogin.backgroundColor =   self.fbButtonBackgroundColor
        GIDSignIn.sharedInstance()?.delegate = self
        GIDSignIn.sharedInstance().presentingViewController = self
        
        if GIDSignIn.sharedInstance()?.currentUser != nil{
            print("Google User Found", GIDSignIn.sharedInstance()?.currentUser! as Any)
        }
        
        if #available(iOS 13.0, *) {
            let appleButton = ASAuthorizationAppleIDButton(type: .continue, style: .black)
            appleButton.translatesAutoresizingMaskIntoConstraints = true
            
            appleButton.addTarget(self, action: #selector(handleLogInWithAppleID), for: .touchUpInside)
            
            AppleSignInView.addArrangedSubview(appleButton)
            NSLayoutConstraint.activate([
                // appleButton.centerYAnchor.constraint(equalTo: AppleSignInView.centerYAnchor, constant: -70),
                appleButton.leadingAnchor.constraint(equalTo: AppleSignInView.leadingAnchor, constant: 0),
                appleButton.trailingAnchor.constraint(equalTo: AppleSignInView.trailingAnchor, constant: 0),
                appleButton.heightAnchor.constraint(equalToConstant: 48)
                ,appleButton.widthAnchor.constraint(equalTo: AppleSignInView.widthAnchor, multiplier: 1.0)
            ])
        } else {
            // Fallback on earlier versions
            self.showAlert(message: "Available from iOS 13. Please upgrade your OS or try other options.")
        }

    }
    
    
    
    @objc func handleLogInWithAppleID() {
        if #available(iOS 13.0, *) {
            let request = ASAuthorizationAppleIDProvider().createRequest()

            request.requestedScopes = [.fullName, .email]
            
            let controller = ASAuthorizationController(authorizationRequests: [request])
            
            controller.delegate = self
            controller.presentationContextProvider = self
            
            controller.performRequests()
        } else {
            // Fallback on earlier versions
        }
    }
    
    func animateImageView() {
        CATransaction.begin()
        
        CATransaction.setAnimationDuration(animationDuration)
        CATransaction.setCompletionBlock {
            let time = DispatchTime.now() + 3.0
            DispatchQueue.main.asyncAfter(deadline: time) {
                self.animateImageView()
            }
        }
        
        let transition = CATransition()
        
        transition.timingFunction = CAMediaTimingFunction.init(name: .easeInEaseOut)
        transition.type = .push
        transition.subtype = .fromRight
        
        imgView.layer.add(transition, forKey: kCATransition)
        imgView.image = imageArray[index]
        
        CATransaction.commit()
        
        index = index < imageArray.count - 1 ? index + 1 : 0
    }
    
    @IBAction func btnSignUpClicked(_ sender: Any) {
        
        //        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        //        guard let registerVC = storyBoard.instantiateViewController(withIdentifier: "RegistrationNavigationController") as? RegistrationNavigationController  else
        //        { return  }
        //        registerVC.modalPresentationStyle = .fullScreen
        //        present(registerVC, animated: true, completion: nil)
    }
    
    @IBAction func btnSignInClicked(_ sender: Any) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        guard let signInVC = storyBoard.instantiateViewController(withIdentifier: "SignInNavigationController") as? SignInNavigationController  else
        { return  }
        signInVC.modalPresentationStyle = .fullScreen
        present(signInVC, animated: true, completion: nil)
        
    }
    
    @IBAction func newSignInClicked(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        guard let signInVC = storyBoard.instantiateViewController(withIdentifier: "SignInNavigationController") as? SignInNavigationController  else
        { return  }
        signInVC.modalPresentationStyle = .fullScreen
        present(signInVC, animated: true, completion: nil)
    }
    
    @IBAction func googleSignInClicked(_ sender: Any) {
        // GIDSignIn.sharedInstance().signIn()
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        guard let signInVC = storyBoard.instantiateViewController(withIdentifier: "PolicyPpoUpViewController") as? PolicyPpoUpViewController  else
        { return  }
        signInVC.modalPresentationStyle = .overCurrentContext
        signInVC.type = "google"
        present(signInVC, animated: true, completion: nil)
        
    }
    
    @IBAction func backAction(_ sender: Any) {
        AppConfigs.setRootViewController()
    }
    
    @IBAction func googleImageClicked(_ sender: Any) {
        // GIDSignIn.sharedInstance().signIn()
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        guard let signInVC = storyBoard.instantiateViewController(withIdentifier: "PolicyPpoUpViewController") as? PolicyPpoUpViewController  else
        { return  }
        signInVC.modalPresentationStyle = .overCurrentContext
        signInVC.type = "google"
        present(signInVC, animated: true, completion: nil)
        
        
    }
    
    @IBAction func fbLoginClicked(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        guard let signInVC = storyBoard.instantiateViewController(withIdentifier: "PolicyPpoUpViewController") as? PolicyPpoUpViewController  else
        { return  }
        signInVC.modalPresentationStyle = .overCurrentContext
        signInVC.type = "fb"
        present(signInVC, animated: true, completion: nil)
        
        // loginWithFb() //to do
        
    }
    
    @IBAction func fbImageClicked(_ sender: Any) {
        //loginWithFb() //to do
        
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        guard let signInVC = storyBoard.instantiateViewController(withIdentifier: "PolicyPpoUpViewController") as? PolicyPpoUpViewController  else
        { return  }
        signInVC.modalPresentationStyle = .overCurrentContext
        signInVC.type = "fb"
        present(signInVC, animated: true, completion: nil)
        
    }
    
    
    func loginWithFb() {
        
        let loginManager = LoginManager()
        //UIApplication.shared.statusBarStyle = .default  // remove this line if not required
        loginManager.logIn(permissions: ["public_profile", "email"], from: self) { (loginResult, error) in
            
            print(loginResult!)
            
            if AccessToken.current != nil{
                AppConfigs.registerSocialUser(view: self.view, vc: self, type: "fb") { (response,responseCode, status, error)  in
                    
                    if responseCode == 200{
                        AppConfigs.saveAccessToken(token: (response?.accessToken)!)
                        AppConfigs.saveRefreshToken(token: (response?.refreshToken)!)
                        
                        let url : String = UrlManager.getCurrentUserInfoURL()
                        self.getCurrentUserInfo(urlString: url)
                        
                    }else{
                        if response != nil{
                            
                            AppConfigs.showSnacbar(message: (response?.errorDescription)!, textColor: .red)
                        }else{
                            
                            AppConfigs.showSnacbar(message: error!, textColor: .red)
                        }
                    }
                }
            }
        }
    }
    
    // Stop the UIActivityIndicatorView animation that was started when the user
    // pressed the Sign In button
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error?) {
        //myActivityIndicator.stopAnimating()
    }
    
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
                print("The user has not signed in before or they have since signed out.")
            } else {
                print("\(error.localizedDescription)")
            }
            return
        }
        let email = user.profile.email
        // ...
        
        print("\(String(describing: email))")
        
        AppConfigs.registerSocialUser(view: self.view, vc: self, type: "google") { (response,responseCode, status, error)  in
            
            if responseCode == 200{
                AppConfigs.saveAccessToken(token: (response?.accessToken)!)
                AppConfigs.saveRefreshToken(token: (response?.refreshToken)!)
                
                let url : String = UrlManager.getCurrentUserInfoURL()
                self.getCurrentUserInfo(urlString: url)
                
            }else{
                if response != nil{
                    
                    AppConfigs.showSnacbar(message: (response?.errorDescription)!, textColor: .red)
                }else{
                    
                    AppConfigs.showSnacbar(message: error!, textColor: .red)
                }
            }
        }
    }
    
    
    func getCurrentUserInfo(urlString:String) {
        print(urlString)
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success",data)
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    
                    DispatchQueue.main.async {
                        let currentUser = try? JSONDecoder().decode(CurrentUserInfoResponse.self, from: response.data!)
                        AppConfigs.saveCurrentUserInfo(user: currentUser!)
                        print(AppConfigs.getCurrentUserInfo().email!)
                        
                        //                        SocketIOManager.shared.establishConnection()
                        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
                        guard let vc = storyBoard.instantiateViewController(withIdentifier: "SMNavigationController") as? SMNavigationController  else
                        { return  }
                        vc.modalPresentationStyle = .fullScreen
                        self.present(vc, animated: true, completion: nil)
                        // UIApplication.shared.keyWindow?.rootViewController = vc
                    }
                }
                    
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    //AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
                let code = response.response?.statusCode
                if code == 500{
                    AppConfigs.showSnacbar(message: ErrorMessage.c500.rawValue, textColor: .red)
                }
                else if code == 502{
                    AppConfigs.showSnacbar(message: ErrorMessage.c502.rawValue, textColor: .red)

                }else{
                    AppConfigs.showSnacbar(message: ErrorMessage.c502.rawValue, textColor: .red)
                }
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
    }
    
    
    func callNetworkAPIForAppleLogin(_ email:String,_ code: String, _ userId:String, _ identityToken:String) {
        // Call your backend to exchange an API token with the code.
        let urlString : String = "https://api.houzes.com/auth/apple/jwt/?user_id=\(userId)"
        print(urlString)
        let headers: HTTPHeaders = ["Authorization": "Bearer" + " " + AppConfigs.getSavedAccessToken()]
        print(headers)
        Alamofire.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.httpBody, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case.success(let data):
                
                print("success",data)
                let statusCode = response.response?.statusCode
                if statusCode == 200{
                    
                    
                    DispatchQueue.main.async {
                        do {
                            let json = try JSON(data: response.data!)
                            let status = json["status"].boolValue
                            let data = json["data"].stringValue
                            let message = json["message"].stringValue
                            
                            print(status, data, message)
                            var parameters: Parameters = [:]
                            
                            parameters = [
                                "code": code,
                                "user_id": userId,
                                "email": email,
                                "jwt_token" : data,
                                "type": "UP"
                            ]
                            print(parameters)
                            
                            ///auth/apple/jwt/?user_id=123
                            AppConfigs.registerAppleUser(view: self.view, vc: self, parameters: parameters) { (data, code, error) in
                                
                                if code == 200{
                                    do{
                                        let json = try JSON(data: data!)
                                        let status = json["status"].boolValue
                                        print(status)
                                        let message = json["message"].stringValue
                                        print(message)
                                        
                                        if status == true{
                                            let dataValue = json["data"]
                                            let access_token = dataValue["access_token"].stringValue
                                            let token_type = dataValue["token_type"].stringValue
                                            let refresh_token = dataValue["refresh_token"].stringValue
                                            print("access_token --> \(access_token) ")
                                            //self.showAlert(message: message)
                                            AppConfigs.saveAccessToken(token: access_token)
                                            AppConfigs.saveRefreshToken(token: refresh_token)
                                            
                                            let url : String = UrlManager.getCurrentUserInfoURL()
                                            self.getCurrentUserInfo(urlString: url)
                                            
                                        }else{
                                            self.showAlert(message: message)
                                        }
                                        
                                    }catch let error{
                                        self.showAlert(message: error.localizedDescription)
                                    }
                                }else if code == 9803{
                                    self.showAlert(message: "Undefined", title: "Error!")
                                }else if code == 500{
                                    //AppConfigs.showSnacbar(message: ErrorMessage.c500.rawValue, textColor: .red)
                                    self.showAlert(message: ErrorMessage.c500.rawValue)
                                }
                                else if code == 502{
                                    self.showAlert(message: ErrorMessage.c502.rawValue)
                                    
                                    //AppConfigs.showSnacbar(message: ErrorMessage.c502.rawValue, textColor: .red)
                                    
                                }else{
                                    self.showAlert(message: ErrorMessage.c502.rawValue)
                                    
                                    //AppConfigs.showSnacbar(message: ErrorMessage.c502.rawValue, textColor: .red)
                                }
                            }
                        
                        }catch let error{
                            print(error.localizedDescription)
                        }
                    }
                }
                    
                else if statusCode == 401{
                    self.removeSpinner()
                    let myError = try? JSONDecoder().decode(MyError.self, from: response.data!)
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode)) \(String(describing: myError?.detail))", textColor: .red)
                }
                    
                else{
                    self.removeSpinner()
                    print("error with response status: \(String(describing: statusCode))")
                    AppConfigs.showSnacbar(message: "error with response status: \(String(describing: statusCode))", textColor: .red)
                }
                
            case.failure(let error):
                print("Not Success",error)
                self.removeSpinner()
                let statusCode = response.response?.statusCode
                self.showMessageForServerError(code: statusCode ?? 500)
                //AppConfigs.showSnacbar(message: error.localizedDescription, textColor: .red)
                
            }
            self.removeSpinner()//self.view.activityStopAnimating()
        }
        
        
    }
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        switch authorization.credential {
            
        case let appleIDCredential as ASAuthorizationAppleIDCredential:
            
            let userIdentifier = appleIDCredential.user
            //appleIDCredential.
            
            let defaults = UserDefaults.standard
            defaults.set(userIdentifier, forKey: "userIdentifier1")
            print("userIdentifier---.", userIdentifier)
            print("userIdentifier---.", appleIDCredential.email ?? "")
            print("userIdentifier---.", appleIDCredential.fullName ?? "")
            
            //self.showAlert(message: "userIdentifier---.\(userIdentifier), \(appleIDCredential.email), \(appleIDCredential.fullName) ")
            
            if appleIDCredential.authorizationCode != nil{
                
                let authCode = String(data: appleIDCredential.authorizationCode!, encoding: .utf8)
                print("authCode:",authCode!)
                print(appleIDCredential.identityToken!)
                
                let identityToken = String(data: appleIDCredential.identityToken!, encoding: .utf8)
                print("identityToken",identityToken!)
                
                self.callNetworkAPIForAppleLogin(appleIDCredential.email ?? "", authCode!, userIdentifier, identityToken!)
                
            }
             
            break
        default:
            break
        }
        
        
    }
    
}

